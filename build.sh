#!/bin/bash

APP_VERSION=$(date +'%d.%m.%y')
export APP_VERSION
working_dir=$(pwd)

#Build repo
hemtt --ci build --release

#Check if our repos are checked out if not then check them out
if [ ! -d '/home/bwi/public_html/test/@TCore_BWI' ]
then
  /home/bwi/scripts/checkout -m \©Core_BWI
fi

if [ ! -d '/home/bwi/public_html/test/@TMain_BWI' ]
then
    /home/bwi/scripts/checkout -m \©Main_BWI
fi

#Clean up bisign files in both Core and Main
rm /home/bwi/public_html/test/@TCore_BWI/addons/*.bisign
rm /home/bwi/public_html/test/@TMain_BWI/addons/*.bisign
#Clean up zsync files in dir
rm /home/bwi/public_html/test/@TCore_BWI/addons/*.zsync
rm /home/bwi/public_html/test/@TMain_BWI/addons/*.zsync

# First move+copy all Main and Core mods + filesQ
#chown -R bwi:bwi ./releases/$APP_VERSION/@bwi
mv ./releases/$APP_VERSION/@bwi/addons/*_main.pbo /home/bwi/public_html/test/@TMain_BWI/addons/
mv ./releases/$APP_VERSION/@bwi/addons/*_main.pbo.bwi-$APP_VERSION.bisign /home/bwi/public_html/test/@TMain_BWI/addons/
cp ./releases/$APP_VERSION/@bwi/bwi.paa /home/bwi/public_html/test/@TMain_BWI/
cp ./releases/$APP_VERSION/@bwi/mod.cpp /home/bwi/public_html/test/@TMain_BWI/
cp ./releases/$APP_VERSION/@bwi/mod.cpp /home/bwi/public_html/test/@TCore_BWI/
cp ./releases/$APP_VERSION/@bwi/bwi.paa /home/bwi/public_html/test/@TCore_BWI/
mv ./releases/$APP_VERSION/@bwi/addons/*.pbo /home/bwi/public_html/test/@TCore_BWI/addons/
mv ./releases/$APP_VERSION/@bwi/addons/*.bisign /home/bwi/public_html/test/@TCore_BWI/addons/

#Clean up old key from key dir
rm /home/bwi/public_html/test/@TServer_CoreKeys/bwi_*.bikey

#Copy key to key dir
cp keys/*.bikey /home/bwi/public_html/test/@TServer_CoreKeys/

#Build test repo
/home/bwi/scripts/buildtest

#Curl to game server
