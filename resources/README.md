This folder contained a collection of resources that are useful to or required for the development of various bwi_addons.

Each of the resources is detailed below:

- ```[DEV]ConstructionCompositions``` Mission file containing compositions for bwi_construction.

## Reporting Issues
Any issues with the BWI Addons should be reported on our [issue tracker](https://gitlab.com/blackwatchint/bwi_addons/issues). Please make sure to include the following information:
- Description of the issue.
- Steps to reproduce.
- Where the issue occurred (singleplayer/local multiplayer/dedicated server).
- The template or mission that the issue occurred on.
- Link to the RPT file if a crash or script error occurred. [How do I find my RPT file?](https://community.bistudio.com/wiki/Crash_Files#Arma_3)

## License
These addons are licensed under the [Arma Public License Share-Alike](https://www.bistudio.com/community/licenses/arma-public-license-share-alike).

<a rel="license" href="https://www.bistudio.com/community/licenses/arma-public-license-share-alike" target="_blank" ><img src="https://www.bistudio.com/assets/img/licenses/APL-SA.png" width="100"></a>