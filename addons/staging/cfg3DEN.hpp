class EventHandlers
{
    class bwi_staging
    {
        onMapOpened =  "call bwi_staging_fnc_refreshMapMarkers;";
        onMissionLoad = "call bwi_staging_fnc_checkMissionConfig;";
		onMissionSave = "call bwi_staging_fnc_checkMissionConfig;";
    };
};