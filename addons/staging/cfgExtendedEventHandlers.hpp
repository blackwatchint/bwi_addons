class Extended_PreInit_EventHandlers {
    class bwi_staging_settings {
        init = "call bwi_staging_fnc_initCBASettings;";
    };
};

class Extended_PostInit_EventHandlers {
    class bwi_staging_init {
        init = "call bwi_staging_fnc_initStaging;";
    };
};