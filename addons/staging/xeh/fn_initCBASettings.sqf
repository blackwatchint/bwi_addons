// Define category strings for the CBA settings.
private _categoryLabelStaging = "BWI Staging";
private _categoryLabelAreas = "Areas";
private _categoryLabelMarkers = "Markers";
private _categoryLabelRespawn = "Respawn";
private _categoryLabelOther = "Other";

// Load staging values. Already returned in CBA format of [values, labels, default].
private _stagingAreaValues = call bwi_staging_fnc_getStagingSettingValues;

// Initialize CBA settings.
// Primary & secondary area settings.
[
    "bwi_staging_primaryAreaBlufor",
    "LIST",
    ["BLUFOR Primary Staging", "Select the primary staging area for BLUFOR players."],
    [_categoryLabelStaging, _categoryLabelAreas],
    _stagingAreaValues,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
        [west, 0, _value] call bwi_staging_fnc_stagingSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_staging_secondaryAreaBlufor",
    "LIST",
    ["BLUFOR Secondary Staging", "Select the secondary staging area for BLUFOR players."],
    [_categoryLabelStaging, _categoryLabelAreas],
    _stagingAreaValues,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
        [west, 1, _value] call bwi_staging_fnc_stagingSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_staging_primaryAreaOpfor",
    "LIST",
    ["OPFOR Primary Staging", "Select the primary staging area for OPFOR players."],
    [_categoryLabelStaging, _categoryLabelAreas],
    _stagingAreaValues,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
        [east, 0, _value] call bwi_staging_fnc_stagingSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_staging_secondaryAreaOpfor",
    "LIST",
    ["OPFOR Secondary Staging", "Select the secondary staging area for OPFOR players."],
    [_categoryLabelStaging, _categoryLabelAreas],
    _stagingAreaValues,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
        [east, 1, _value] call bwi_staging_fnc_stagingSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_staging_primaryAreaIndep",
    "LIST",
    ["INDEP Primary Staging", "Select the primary staging area for INDEP players."],
    [_categoryLabelStaging, _categoryLabelAreas],
    _stagingAreaValues,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
        [resistance, 0, _value] call bwi_staging_fnc_stagingSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_staging_secondaryAreaIndep",
    "LIST",
    ["INDEP Secondary Staging", "Select the secondary staging area for INDEP players."],
    [_categoryLabelStaging, _categoryLabelAreas],
    _stagingAreaValues,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
        [resistance, 1, _value] call bwi_staging_fnc_stagingSettingChanged;
    }
] call CBA_settings_fnc_init;


// Marker settings.
[
    "bwi_staging_showAllMarkers",
    "CHECKBOX",
    ["Show All Markers", "Enables the placement of markers for all available staging areas. Applies only when in the editor."],
    [_categoryLabelStaging, _categoryLabelMarkers],
    false,
    true
] call CBA_settings_fnc_init;

[
    "bwi_staging_showActiveMarkers",
    "CHECKBOX",
    ["Show Active Markers", "Enables the placement of markers for active staging areas."],
    [_categoryLabelStaging, _categoryLabelMarkers],
    true,
    true
] call CBA_settings_fnc_init;

[
    "bwi_staging_showMarkersIn3DEN",
    "CHECKBOX",
    ["Show in 3DEN", "Enables the display of staging markers in 3DEN's map view. Note that markers are only refreshed on map open/close."],
    [_categoryLabelStaging, _categoryLabelMarkers],
    true,
    false // Not-synced.
] call CBA_settings_fnc_init;


// Respawn settings.
[
    "bwi_staging_reinforcementCheckRadius",
    "SLIDER",
    ["Reinforcement Check Radius", "Radius from the staging flagpole checked for waiting reinforcements."],
    [_categoryLabelStaging, _categoryLabelRespawn],
    [10, 100, 25, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_staging_notificationSubscribers",
    "EDITBOX",
    ["Notification Subscribers", "List of units that will receive notifications about players waiting at staging areas."],
    [_categoryLabelStaging, _categoryLabelRespawn],
    "",
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
		
        if ( (str player) in (_value splitString ",") ) then {
	        bwi_staging_isNotificationSubscriber = true;
        } else {
            bwi_staging_isNotificationSubscriber = false;
        };
    }
] call CBA_settings_fnc_init;

[
    "bwi_staging_notificationFrequency",
    "SLIDER",
    ["Notification Frequency", "Number of seconds between notifications about players waiting at staging areas."],
    [_categoryLabelStaging, _categoryLabelRespawn],
    [10, 300, 120, 0],
    false // Not-synced.
] call CBA_settings_fnc_init;


// Other settings.
[
    "bwi_staging_returnTimeLimit",
    "SLIDER",
    ["Return Time Limit", "The amount of time in seconds that a player is permitted to return to spawn."],
    [_categoryLabelStaging, _categoryLabelOther],
    [-1, 600, 300, 0],
    true
] call CBA_settings_fnc_init;