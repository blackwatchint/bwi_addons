// Declare global to check if staging is active.
bwi_staging_isEnabled = false;

// Check for CfgStaging areas first...
private _stagingAreasCfg = missionConfigFile >> "CfgStagingAreas";

if ( isClass _stagingAreasCfg ) then {
	// Staging is active.
	bwi_staging_isEnabled = true;

	// Run on the server only.
	if ( isServer ) then {
		["CfgStagingAreas found in missionConfigFile, initializing bwi_staging..."] call bwi_common_fnc_log;

		// Get the staging teleport data from the config.
		private _teleporters = getArray (_stagingAreasCfg >> "teleporters");

		// Create spawn flagpoles.
		{
			private _position = getPosATL (missionNamespace getVariable _x);
			private _rotation = getDir (missionNamespace getVariable _x); 

			private _flagpole = [_position, _rotation] call bwi_staging_fnc_createSpawnFlagpole;

			// Create a bwi_utilities corpse cleanup zone.
			if ( isClass (configFile >> "CfgPatches" >> "bwi_utilities") ) then {
				[_flagpole] call bwi_utilities_fnc_addCorpseCleanupZone;
			};

			// Note - We explicitly omit a spawn event handler as it will be unreliable!
		} forEach _teleporters;

		// Initialize the COP variables and broadcast.
		if ( isNil "bwi_staging_outpostActiveBlufor" && isNil "bwi_staging_outpostObjectBlufor" ) then {
			missionNamespace setVariable ["bwi_staging_outpostActiveBlufor", "NONE", true];
			missionNamespace setVariable ["bwi_staging_outpostObjectBlufor", objNull, true];
		};
		if ( isNil "bwi_staging_outpostActiveOpfor" && isNil "bwi_staging_outpostObjectOpfor" ) then {
			missionNamespace setVariable ["bwi_staging_outpostActiveOpfor", "NONE", true];
			missionNamespace setVariable ["bwi_staging_outpostObjectOpfor", objNull, true];
		};
		if ( isNil "bwi_staging_outpostActiveIndep" && isNil "bwi_staging_outpostObjectIndep" ) then {
			missionNamespace setVariable ["bwi_staging_outpostActiveIndep", "NONE", true];
			missionNamespace setVariable ["bwi_staging_outpostObjectIndep", objNull, true];
		};
	};

	// Register event and per frame handlers.
	call bwi_staging_fnc_registerOutpostHandlers;
	call bwi_staging_fnc_registerNotificationHandler;
};