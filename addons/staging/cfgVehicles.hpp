/**
 * Add ACE self-interaction actions for managing the display of
 * reinforcement notifications by BWI Staging.
 */
class Man;
class CAManBase: Man {
    class ACE_SelfActions {

        class BWI_Staging {
            displayName = "Reinforcements";
            condition = "bwi_staging_isEnabled && bwi_staging_isNotificationSubscriber";
            icon = "\bwi_staging\data\icons\reinforcements.paa";

            class BWI_Staging_RequestReport {
                displayName = "Request Report";
                condition = "true";
                statement = "[_player] call bwi_staging_fnc_displayReinforcementReport;";
                showDisabled = 0;
                priority = 0.1;
                icon = "\bwi_staging\data\icons\report.paa";
                exceptions[] = {"notOnMap"};
            };

            class BWI_Staging_PauseNotifications {
                displayName = "Pause Notifications";
                condition = "!bwi_staging_pauseNotifications";
                statement = "bwi_staging_pauseNotifications = true;";
                showDisabled = 0;
                priority = 0.1;
                icon = "\bwi_staging\data\icons\pause.paa";
                exceptions[] = {"notOnMap"};
            };

            class BWI_Staging_ResumeNotifications {
                displayName = "Resume Notifications";
                condition = "bwi_staging_pauseNotifications";
                statement = "bwi_staging_pauseNotifications = false;";
                showDisabled = 0;
                priority = 0.1;
                icon = "\bwi_staging\data\icons\resume.paa";
                exceptions[] = {"notOnMap"};
            };
        };
    };
};


/**
 * The flag for abandoning a built COP.
 */
class Flag_White_F;
class BWI_Staging_OutpostFlag: Flag_White_F
{
    scope = 1;
    scopeCurator = 1;
    displayName = "BWI Staging Outpost Flag";

    class EventHandlers
    {
        //class CBA_Extended_EventHandlers;
        class CBA_Extended_EventHandlers: CBA_Extended_EventHandlers {};
    };

    class ACE_Actions 
    {
        class ACE_MainActions 
        {
            displayName = "Interactions";
            distance = 6;
            condition = "true";
            statement = "true";
            icon = "";

            class BWI_Staging_AbandonOutpost 
            {
                displayName = "Abandon Outpost";
                condition = "true";
                statement = "[_player] call bwi_staging_fnc_abandonOutpost;";
                priority = 1;
                icon = "";
                showDisabled = 0;
            };
        };
    };
};