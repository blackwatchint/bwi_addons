/**
 * We can not reference the marker or positions by missionNamespace variables,
 * as they do not exist in 3DEN! For example the following will not work (where
 * myCustomMarker and myTeleporter are named entities placed in 3DEN):
 *
 *		myCustomMarker setMarkerColor 'ColorRed';
 *		myCustomMarker setMarkerPos getPos myTeleporter;
 *
 * However, the following will work as it does not reference a named entity:
 *
 *		createMarker ["myCustomMarker", [123,456]];
 *		"myCustomMarker" setMarkerColor 'ColorRed';
 *		"myCustomMarker" setMarkerPos [789,012];
 */

// Run on the server only.
if ( isServer ) then {
	// Get the staging areas from the mission config.
	private _stagingAreaCfgs = "true" configClasses (missionConfigFile >> "CfgStagingAreas");

	// There are staging areas to iterate through.
	if ( count _stagingAreaCfgs > 0 ) then {
		{
			private _stagingId = configName (_x);
			private _markerId = "bwi_staging_marker_" + _stagingId;
			private _position = getArray (_x >> "position");
			private _name = getText (_x >> "name");
			private _isActiveStaging = false;

			// Marker doesn't exist, so create it.
			if ( getMarkerType _markerId == "" ) then {
				createMarker [_markerId, _position];
				_markerId setMarkerText _name;
				_markerId setMarkerShape 'ICON';
				_markerId setMarkerType 'mil_start';
				_markerId setMarkerSize [0.75, 0.75];
			};

			// Update marker colour according to settings.
			_markerId setMarkerColor 'ColorCIV';

			if ( _stagingId == bwi_staging_primaryAreaBlufor || _stagingId == bwi_staging_secondaryAreaBlufor ) then {
				_markerId setMarkerColor 'ColorWEST';
				_isActiveStaging = true;
			};

			if ( _stagingId == bwi_staging_primaryAreaOpfor || _stagingId == bwi_staging_secondaryAreaOpfor ) then {
				_markerId setMarkerColor 'ColorEAST';
				_isActiveStaging = true;
			};

			if ( _stagingId == bwi_staging_primaryAreaIndep || _stagingId == bwi_staging_secondaryAreaIndep ) then {
				_markerId setMarkerColor 'ColorGUER';
				_isActiveStaging = true;
			};

			// Show and hide the marker according to settings.
			if ( ( bwi_staging_showMarkersIn3DEN && is3DEN ) || !is3DEN ) then {
				if ( ( bwi_staging_showAllMarkers && is3DEN ) || ( bwi_staging_showActiveMarkers && _isActiveStaging ) ) then {
					_markerId setMarkerAlpha 1;
				} else {
					_markerId setMarkerAlpha 0;
				};
			} else {
				_markerId setMarkerAlpha 0;
			};
		} forEach _stagingAreaCfgs;
	};
};