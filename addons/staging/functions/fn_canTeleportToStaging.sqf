params ["_side", "_index"];

private _result = false;

// Check the result conditions according to side and index.
switch ( _side ) do {
	case west: {
		switch ( _index ) do {
			case 0: { _result = ( bwi_staging_primaryAreaBlufor   != "NONE" ); };
			case 1: { _result = ( bwi_staging_secondaryAreaBlufor != "NONE" ); };
			case 2: { _result = ( bwi_staging_outpostActiveBlufor == "COP" && !isNull bwi_staging_outpostObjectBlufor ); };
			case 3: { _result = ( bwi_staging_outpostActiveBlufor == "VOP" && alive bwi_staging_outpostObjectBlufor   ); };
		};
	};
	case east: {
		switch ( _index ) do {
			case 0: { _result = ( bwi_staging_primaryAreaOpfor   != "NONE" ); };
			case 1: { _result = ( bwi_staging_secondaryAreaOpfor != "NONE" ); };
			case 2: { _result = ( bwi_staging_outpostActiveOpfor == "COP" && !isNull bwi_staging_outpostObjectOpfor ); };
			case 3: { _result = ( bwi_staging_outpostActiveOpfor == "VOP" && alive bwi_staging_outpostObjectOpfor   ); };
		}
	};
	case resistance: {
		switch ( _index ) do {
			case 0: { _result = ( bwi_staging_primaryAreaIndep   != "NONE" ); };
			case 1: { _result = ( bwi_staging_secondaryAreaIndep != "NONE" ); };
			case 2: { _result = ( bwi_staging_outpostActiveIndep == "COP" && !isNull bwi_staging_outpostObjectIndep ); };
			case 3: { _result = ( bwi_staging_outpostActiveIndep == "VOP" && alive bwi_staging_outpostObjectIndep   ); };
		}
	};
};

_result