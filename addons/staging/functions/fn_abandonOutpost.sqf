params ["_unit"];

[5, [_unit],
{
	(_this select 0) params ["_unit"]; // Don't ask why, ACE weirdness.

	// Reset the outpost variables.
	private _flagpole = [_unit] call bwi_staging_fnc_resetOutpost;

	// Replace the flagpole with an empty one.
	private _position = getPosATL _flagpole;
	private _rotation = getDir _flagpole;

	// Delete the old flagpole.
	deleteVehicle _flagpole;

	// Create the empty flagpole.
	private _obj = createVehicle ["FlagPole_EP1", _position, [], 0, "CAN_COLLIDE"];
	_obj setDir _rotation;

	// Angle to match terrain.
	private _objNorm = surfaceNormal (getPosATL _obj); 
	_obj setVectorUp _objNorm;

	// Display hint informing of abandonment.
	["Outpost Abandoned", "Reinforcements will no longer arrive at this outpost.", 1] call bwi_common_fnc_notification;
}, {}, "Abandoning Outpost", {true}, []] call ace_common_fnc_progressBar;