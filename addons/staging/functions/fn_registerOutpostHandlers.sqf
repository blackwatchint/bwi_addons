/**
 * Event handler that iterates over all objects when a
 * construction is completed, updating the outpost variables
 * so that they can be used when available.
 */
["bwi_construction_buildingCompleted",
	{
		private _crate = _this select 0;
		private _unit = _this select 1;
		private _objects = _this select 2;

		{
			// Found a staging flag, activate it.
			if ( typeOf _x == "BWI_Staging_OutpostFlag" ) then {
				[_unit, _x, false] call bwi_staging_fnc_activateOutpost;
			};
		} forEach _objects;
	}
] call CBA_fnc_addEventHandlerArgs;


/**
 * Event handler that checks spawned vehicles for the outpost
 * parameter, and assigns them as the active outpost when it
 * is present and true.
 */
["bwi_motorpool_spawnVehicle",
	{
		private _unit = player; // Can assume player, event is local.
		private _vehicle = _this select 0;
		private _isOutpost = _this select 6;

		if ( _isOutpost == 1 ) then {
			[_unit, _vehicle, true] call bwi_staging_fnc_activateOutpost;
		};
	}
] call CBA_fnc_addEventHandlerArgs;