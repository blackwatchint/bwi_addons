private _labels = ["None"];
private _values = ["NONE"];
private _default = 0;

// Get the staging data from the mission config.
private _stagingAreaCfgs = "true" configClasses (missionConfigFile >> "CfgStagingAreas");

// Format the staging options for CBA settings.
if ( count _stagingAreaCfgs > 0 ) then {
	{
		_labels pushBack getText ( _x >> "name" );
		_values pushBack configName ( _x );
	} forEach _stagingAreaCfgs;
};

[_values, _labels, _default]