params ["_side", "_index", "_stagingArea"];

// Run on the server only, when staging is enabled.
if ( isServer && bwi_staging_isEnabled ) then {
	// Refresh the staging markers.
	call bwi_staging_fnc_refreshMapMarkers;

	// Call the destroy staging event handler for this staging area.
	["bwi_staging_removeStaging", [_side, _index, _stagingArea]] call CBA_fnc_localEvent;

	if ( _stagingArea != "NONE" ) then {
		// Read the config for the selected staging area.
		private _stagingAreaCfg = missionConfigFile >> "CfgStagingAreas" >> _stagingArea;
		private _teleporter = missionNamespace getVariable (getText (_stagingAreaCfg >> "teleporter"));

		private _position = getPosATL _teleporter;
		private _rotation = getDir _teleporter;

		// Create the staging area flagpole.
		private _flagpole = [_position, _rotation, _side] call bwi_staging_fnc_createStagingFlagpole;

		// Create a bwi_construction exclusion zone.
		// No radii are passed so bwi_construction defaults are used.
		[_flagpole] call bwi_construction_fnc_addExclusionZone;

		// Create a bwi_utilities corpse cleanup zone.
		if ( isClass (configFile >> "CfgPatches" >> "bwi_utilities") ) then {
			[_flagpole] call bwi_utilities_fnc_addCorpseCleanupZone;
		};

		// Call the create staging event handler for this staging area.
		["bwi_staging_createStaging", [_side, _index, _stagingArea, _flagpole]] call CBA_fnc_localEvent;

		// Log the staging creation.
		["Creating staging flagpole %1...", _flagpole] call bwi_common_fnc_log;

		/**
		 * Removes the flagpole that has just been created when the
		 * removeStaging EH is triggered. This event also passes
		 * the side, index and flagpole objects as "static" arguments,
		 * so they can be compared with the called staging area to
		 * determine if it should be removed or not.
		 */
		["bwi_staging_removeStaging",
			{
				private _seekSide = _this select 0;
				private _seekIndex = _this select 1;

				private _side = _thisArgs select 0;
				private _index = _thisArgs select 1;
				private _flagpole = _thisArgs select 2;

				// Found the flagpole to remove.
				if ( _seekSide == _side && _seekIndex == _index ) then {
					// Log the staging removal.
					["Removing staging flagpole %1...", _flagpole] call bwi_common_fnc_log;

					// Remove the flagpole (also disables bwi_construction exclusion zone).
					removeAllActions _flagpole;
					deleteVehicle _flagpole;

					// Delete this event handler.
					[_thisType, _thisId] call CBA_fnc_removeEventHandler
				};
			},
			[_side, _index, _flagpole]
		] call CBA_fnc_addEventHandlerArgs;
	};
};