if ( is3DEN ) then {
	private _stagingAreasCfg = missionConfigFile >> "CfgStagingAreas";

	// Staging is enabled.
	if ( isClass _stagingAreasCfg ) then {
		private _allLogicNames = [3] call bwi_common_fnc_get3DENEntityNames;

		private _teleporters = getArray (_stagingAreasCfg >> "teleporters");

		// Check for missing logic entities.
		{
			if ( !(_x in _allLogicNames) ) then {
				["Spawn teleporter %1 does not exist.", _x] call bwi_common_fnc_log;
			};
		} forEach _teleporters;

		// Now check each staging area.
		private _stagingCfgs = "true" configClasses (_stagingAreasCfg);

		{
			private _name = getText (_x >> "name");
			private _position = getArray (_x >> "position");
			private _teleporter = getText (_x >> "teleporter");

			// Empty name.
			if ( _name == "" ) then {
				["Name for %1 is empty.", configName _x] call bwi_common_fnc_log;
			};

			// Invalid position.
			if ( count _position < 2 ) then {
				["Position for %1 is invalid.", configName _x] call bwi_common_fnc_log;
			};

			// Check for missing logic entity.
			if ( !(_teleporter in _allLogicNames) ) then {
				["Staging teleporter %2 for %1 does not exist.", configName _x, _teleporter] call bwi_common_fnc_log;
			};
		} forEach _stagingCfgs;
	};
};