params ["_side", "_index"];

private _stagingArea = "NONE";

// Find the correct staging area setting.
switch ( _side ) do {
	case west: {
		switch ( _index ) do {
			case 0: { _stagingArea = bwi_staging_primaryAreaBlufor;   };
			case 1: { _stagingArea = bwi_staging_secondaryAreaBlufor; };
			case 2;
			case 3: { _stagingArea = bwi_staging_outpostActiveBlufor; };
		}
	};
	case east: {
		switch ( _index ) do {
			case 0: { _stagingArea = bwi_staging_primaryAreaOpfor;   };
			case 1: { _stagingArea = bwi_staging_secondaryAreaOpfor; };
			case 2;
			case 3: { _stagingArea = bwi_staging_outpostActiveOpfor; };
		}
	};
	case resistance: {
		switch ( _index ) do {
			case 0: { _stagingArea = bwi_staging_primaryAreaIndep;   };
			case 1: { _stagingArea = bwi_staging_secondaryAreaIndep; };
			case 2;
			case 3: { _stagingArea = bwi_staging_outpostActiveIndep; };
		}
	};
};

// A valid staging area is available.
if ( _stagingArea != "NONE" ) then {
	private _name = "";
	private _destination = objNull;
	private _canTeleport = true;

	// Set the name and destination according to staging index.
	// Note we do _index && _stagingArea comparisons to ensure correct selection.
	switch true do {
		// Primary and secondary staging.
		case ( _index == 0 && _stagingArea != "NONE" );
		case ( _index == 1 && _stagingArea != "NONE" ): {
			// Get the staging name and destination from the config.
			private _stagingAreaCfg = missionConfigFile >> "CfgStagingAreas" >> _stagingArea;

			_name = getText (_stagingAreaCfg >> "name");
			_destination = missionNamespace getVariable (getText (_stagingAreaCfg >> "teleporter"));
		};

		// Combat and vehicle outposts.
		case ( _index == 2 && _stagingArea == "COP" );
		case ( _index == 3 && _stagingArea == "VOP" ): {
			_name = "Combat Outpost";

			// Load the destination from the public variable.
			switch ( _side ) do {
				case west: 		 { _destination = bwi_staging_outpostObjectBlufor; };
				case east: 		 { _destination = bwi_staging_outpostObjectOpfor;  };
				case resistance: { _destination = bwi_staging_outpostObjectIndep;  };
			};

			// Validate and rename vehicle outposts.
			if ( _stagingArea == "VOP" ) then {
				_name = "Vehicle Outpost";

				// Check vehicle specific conditions not covered by canTeleportToStaging.
				if ( _canTeleport && _destination emptyPositions "cargo" < 1 ) then {
					_canTeleport = false;
					["Outpost Unavailable", "Vehicle is full...", 1, true] call bwi_common_fnc_notification;
				};

				if ( _canTeleport && _destination isKindOf "Plane" && !(backpack player isKindOf "B_Parachute") ) then {
					_canTeleport = false;
					["Pancake Warning", "You require a parachute!", 1, true] call bwi_common_fnc_notification;
				};
			};
		};
	};

	// Destination valid.
	if ( _canTeleport ) then {
		// Inform any listeners that the teleport is starting. Reversed is set to false.
		["bwi_staging_preTeleportToStaging", [player, _destination, false]] call CBA_fnc_localEvent;

		// Teleport according to destination type.
		if ( _stagingArea == "VOP" ) then {
			[_destination, _name] call bwi_common_fnc_teleportToVehicle;
		} else {
			private _position = getPosATL _destination;
			private _rotation = getDir _destination;

			[_position, _rotation, _name] call bwi_common_fnc_teleportToPosition;
		};

		// Inform any listeners that the teleport is completed. Reversed is set to false.
		["bwi_staging_postTeleportToStaging", [player, _destination, false]] call CBA_fnc_localEvent;

		// Save the deploy time variable on the player.
		player setVariable ["bwi_staging_deployTime", CBA_missionTime];
	};
// Staging not set, display error notification.
} else {
	["Staging Unavailable", "Please wait for Zeus to set a staging area...", 0, true] call bwi_common_fnc_notification;
};