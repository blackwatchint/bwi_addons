params ["_unit"];

private _result = true;

// Only check return time limit if it is not -1.
if ( bwi_staging_returnTimeLimit >= 0 ) then {
	_result = ((_unit getVariable['bwi_staging_deployTime', 0]) > (CBA_missionTime - bwi_staging_returnTimeLimit));
};

// Return the result.
_result