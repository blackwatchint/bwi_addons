params ["_unit"];

private _object = objNull;

// Reset the outpost variables according to side.
switch ( side _unit ) do {
	case west: {
		_object = bwi_staging_outpostObjectBlufor;

		missionNamespace setVariable ["bwi_staging_outpostActiveBlufor", "NONE", true];
		missionNamespace setVariable ["bwi_staging_outpostObjectBlufor", objNull, true];
	};
	case east: {
		_object = bwi_staging_outpostObjectOpfor;

		missionNamespace setVariable ["bwi_staging_outpostActiveOpfor", "NONE", true];
		missionNamespace setVariable ["bwi_staging_outpostObjectOpfor", objNull, true];
	};
	case resistance: {
		_object = bwi_staging_outpostObjectIndep;

		missionNamespace setVariable ["bwi_staging_outpostActiveIndep", "NONE", true];
		missionNamespace setVariable ["bwi_staging_outpostObjectIndep", objNull, true];
	};
};

// Return the object.
_object