params ["_unit", "_object", ["_isVehicle", false]];

private _type = "COP";

if ( _isVehicle ) then {
	_type = "VOP";
};

// Update outpost variables according to side.
switch ( side _unit ) do {
	case west: {
		missionNamespace setVariable ["bwi_staging_outpostActiveBlufor", _type, true];
		missionNamespace setVariable ["bwi_staging_outpostObjectBlufor", _object, true];
	};
	case east: {
		missionNamespace setVariable ["bwi_staging_outpostActiveOpfor", _type, true];
		missionNamespace setVariable ["bwi_staging_outpostObjectOpfor", _object, true];
	};
	case resistance: {
		missionNamespace setVariable ["bwi_staging_outpostActiveIndep", _type, true];
		missionNamespace setVariable ["bwi_staging_outpostObjectIndep", _object, true];
	};
};