// Exit on Server/HC.
if ( !hasInterface ) exitWith {};

// Initialize main globals.
if ( isNil "bwi_staging_isNotificationSubscriber" ) then {
	bwi_staging_isNotificationSubscriber = false;
};

if ( isNil "bwi_staging_pauseNotifications" ) then {
	bwi_staging_pauseNotifications = false;
};


/**
 * PFH that displays a hint notifying subscribed users of
 * the number of players waiting within range of each of
 * the active staging areas. Runs every x seconds as defined
 * in the client's settings.
 */
private _notificationPFHID = [
	{
		// Exit if staging is disabled.
		if ( !bwi_staging_isEnabled ) exitWith {};

		// Exit if not a notification subscriber.
		if ( !bwi_staging_isNotificationSubscriber ) exitWith {};

		// Exit if notifications paused.
		if ( bwi_staging_pauseNotifications ) exitWith {};

		// Exit if any required parameters are not yet initialized.
		if ( isNil "bwi_staging_primaryAreaBlufor" || isNil "bwi_staging_secondaryAreaBlufor" || isNil "bwi_staging_outpostActiveBlufor" ) exitWith {};
		if ( isNil "bwi_staging_primaryAreaOpfor"  || isNil "bwi_staging_secondaryAreaOpfor"  || isNil "bwi_staging_outpostActiveOpfor"  ) exitWith {};
		if ( isNil "bwi_staging_primaryAreaIndep"  || isNil "bwi_staging_secondaryAreaIndep"  || isNil "bwi_staging_outpostActiveIndep"  ) exitWith {};

		// Display the report.
		[player] call bwi_staging_fnc_displayReinforcementReport;
	},
	bwi_staging_notificationFrequency // Run according to the frequency setting.
] call CBA_fnc_addPerFrameHandler;