params ["_position", "_rotation"];

private _flagpole = objNull;

// Run on the server only.
if ( isServer ) then {
	// Create the flagpole object.
	_flagpole = createVehicle ["Flag_White_F", _position, [], 0, "CAN_COLLIDE"];
	_flagpole setDir _rotation;
	_flagpole allowDamage false;
	_flagpole setFlagTexture "\bwi_staging\data\flag_bwi.paa";

	// Add the primary staging action.
	[_flagpole, [
		"<t color='#18d500'>Deploy to Primary Staging</t>",
		{ [playerSide, 0] call bwi_staging_fnc_teleportToStaging; },
		nil, 1.3, false, true, "",
		"[playerSide, 0] call bwi_staging_fnc_canTeleportToStaging" // Omit ;.
	]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP

	// Add the secondary staging action.
	[_flagpole, [
		"<t color='#0083d5'>Deploy to Secondary Staging</t>", 
		{ [playerSide, 1] call bwi_staging_fnc_teleportToStaging; },
		nil, 1.2, false, true, "",
		"[playerSide, 1] call bwi_staging_fnc_canTeleportToStaging" // Omit ;.
	]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP

	// Add the deploy to COP action.
	[_flagpole, [
		"<t color='#7100d5'>Deploy to Combat Outpost</t>", 
		{ [playerSide, 2] call bwi_staging_fnc_teleportToStaging; },
		nil, 1.1, false, true, "",
		"[playerSide, 2] call bwi_staging_fnc_canTeleportToStaging" // Omit ;.
	]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP

	// Add the deploy to VOP action.
	[_flagpole, [
		"<t color='#7100d5'>Deploy to Vehicle Outpost</t>", 
		{ [playerSide, 3] call bwi_staging_fnc_teleportToStaging; },
		nil, 1.1, false, true, "",
		"[playerSide, 3] call bwi_staging_fnc_canTeleportToStaging" // Omit ;.
	]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP
};

// Return the flagpole.
_flagpole