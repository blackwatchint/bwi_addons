params ["_side"];

// Get the spawn teleport data from the config.
private _teleporters = getArray (missionConfigFile >> "CfgStagingAreas" >> "teleporters");
private _sideId = 0;

// Find correct sideId for multiple teleporters.
if ( count _teleporters > 1 ) then {
	_sideId = [_side] call bwi_common_fnc_sideToIndex;
};

// Fetch the teleporter position.
private _teleporter = missionNamespace getVariable (_teleporters select _sideId);
private _position = getPosATL _teleporter;
private _rotation = getDir _teleporter;

// Inform any listeners that the teleport is starting. Reversed is set to true.
["bwi_staging_preTeleportToStaging", [player, _teleporter, true]] call CBA_fnc_localEvent;

// Perform the teleportation.
[_position, _rotation, "Spawn"] call bwi_common_fnc_teleportToPosition;

// Inform any listeners that the teleport is completed. Reversed is set to true.
["bwi_staging_postTeleportToStaging", [player, _teleporter, true]] call CBA_fnc_localEvent;