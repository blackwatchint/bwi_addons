params ["_unit"];

private _stagingPrimary = "NONE";
private _stagingSecondary = "NONE";
private _stagingOutpost = "NONE";
private _message = "";

// Get the staging settings by side.
switch ( side _unit ) do {
	case west: {
		_stagingPrimary = bwi_staging_primaryAreaBlufor;
		_stagingSecondary = bwi_staging_secondaryAreaBlufor;
		_stagingOutpost = bwi_staging_outpostActiveBlufor;
	};
	case east: {
		_stagingPrimary = bwi_staging_primaryAreaOpfor;
		_stagingSecondary = bwi_staging_secondaryAreaOpfor;
		_stagingOutpost = bwi_staging_outpostActiveOpfor;
	};
	case resistance: {
		_stagingPrimary = bwi_staging_primaryAreaIndep;
		_stagingSecondary = bwi_staging_secondaryAreaIndep;
		_stagingOutpost = bwi_staging_outpostActiveIndep;
	};
};

// Check each staging area individually.
if ( _stagingPrimary != "NONE" ) then {
	// Get the staging teleport data from the config.
	private _stagingAreaCfg = missionConfigFile >> "CfgStagingAreas" >> _stagingPrimary;
	private _flagpole = missionNamespace getVariable (getText (_stagingAreaCfg >> "teleporter"));

	// Count the players near the teleport position and add to the message.
	private _playerCount = [getPosATL _flagpole, bwi_staging_reinforcementCheckRadius, side _unit, true] call bwi_common_fnc_countPlayersNear;

	if ( _playerCount > 0 ) then {
		_message = _message + format ["Primary Staging: <t color='#18d500'>%1</t><br/>", _playerCount];
	};
};

if ( _stagingSecondary != "NONE" ) then {
	// Get the staging teleport data from the config.
	private _stagingAreaCfg = missionConfigFile >> "CfgStagingAreas" >> _stagingSecondary;
	private _flagpole = missionNamespace getVariable (getText (_stagingAreaCfg >> "teleporter"));

	// Count the players near the teleport position and add to the message.
	private _playerCount = [getPosATL _flagpole, bwi_staging_reinforcementCheckRadius, side _unit, true] call bwi_common_fnc_countPlayersNear;

	if ( _playerCount > 0 ) then {
		_message = _message + format ["Secondary Staging: <t color='#0083d5'>%1</t><br/>", _playerCount];
	};
};

if ( _stagingOutpost != "NONE" ) then {
	private _outpostObject = objNull;
	private _outpostTitle = "";
	private _playerCount = 0;

	// Get the outpost object according to side.
		switch ( side _unit ) do {
			case west: 		 { _outpostObject = bwi_staging_outpostObjectBlufor; };
			case east: 		 { _outpostObject = bwi_staging_outpostObjectOpfor;  };
			case resistance: { _outpostObject = bwi_staging_outpostObjectIndep;  };
		};

	// If the outpost exists, count the players according to outpost type.
	if ( !isNull _outpostObject && alive _outpostObject ) then {
		switch ( _stagingOutpost ) do {
			case "COP": {
				_outpostTitle = "Combat Outpost";
				_playerCount = [getPosATL _outpostObject, bwi_staging_reinforcementCheckRadius, side _unit, true] call bwi_common_fnc_countPlayersNear;
			};

			case "VOP": {
				_outpostTitle = "Vehicle Outpost";
				_playerCount = [_outpostObject, "cargo", side _unit, true] call bwi_common_fnc_countPlayersIn;
			};
		};
	};

	// Add to the message.
	if ( _playerCount > 0 ) then {
		_message = _message + format ["%2: <t color='#7100d5'>%1</t><br/>", _playerCount, _outpostTitle];
	};
};

// There is a report to display.
if ( _message != "" ) then {
	["Reinforcement Report", _message, 3, true] call bwi_common_fnc_notification;
};