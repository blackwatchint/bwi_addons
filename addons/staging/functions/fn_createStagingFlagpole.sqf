params ["_position", "_rotation", ["_side", west]];

private _flagpole = objNull;

// Run on the server only.
if ( isServer ) then {
	// Choose the flagpole object based on side.
	private _classname = "Flag_White_F";
	switch ( _side ) do {
		case west:	 	 { _classname = "Flag_Blue_F"; };
		case east: 		 { _classname = "Flag_Red_F"; };
		case resistance: { _classname = "Flag_Green_F"; };
	};

	// Create the flagpole object.
	_flagpole = createVehicle [_classname, _position, [], 0, "CAN_COLLIDE"];
	_flagpole setDir _rotation;
	_flagpole allowDamage false;

	// Set the flag texture according to the faction.
	private _flagTexture = [_side] call bwi_armory_fnc_getFlagFromFaction;

	if ( _flagTexture != "" ) then {
		_flagpole setFlagTexture _flagTexture;
	};

	// Add the return action.
	[_flagpole, [
		"<t color='#d500bf'>Return to Spawn</t>",
		{ [playerSide] call bwi_staging_fnc_teleportToSpawn; },
		nil, 1.0, false, true, "",
		"[_this] call bwi_staging_fnc_canTeleportToSpawn" // Omit ;.
	]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP
};

// Return the flagpole.
_flagpole