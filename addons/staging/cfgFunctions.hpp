class bwi_staging
{
	class functions
	{
		file = "\bwi_staging\functions";
		class refreshMapMarkers{};

		class getStagingSettingValues{};
		class stagingSettingChanged{};

		class createSpawnFlagpole{};
		class createStagingFlagpole{};

		class teleportToSpawn{};
		class teleportToStaging{};
		class canTeleportToSpawn{};
		class canTeleportToStaging{};
		
		class activateOutpost{};
		class resetOutpost{};
		class abandonOutpost{};
		class registerOutpostHandlers{};
		
		class registerNotificationHandler{};
		class displayReinforcementReport{};

		class checkMissionConfig{};
	};

	class xeh
	{
		file = "\bwi_staging\xeh";
		class initCBASettings{};
		class initStaging{};
	};
};