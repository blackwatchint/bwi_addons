class CfgPatches 
{
    class bwi_staging 
    {
        requiredVersion = 1;
        authors[] = { "Fourjays" };
        author = "Black Watch International";
        url = "http://blackwatch-int.com";
        version = 1.1;
        versionStr = "1.1.0";
        versionAr[] = {1,1,0};
        requiredAddons[] = {
            "A3_UI_F", 
            "cba_settings", 
            "ace_common", 
            "ace_interaction", 
            "bwi_common",
            "bwi_armory",
            "bwi_construction"
        };
        units[] = { };
    };
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgRemoteExec
{
	#include <cfgRemoteExec.hpp>
};

class CBA_Extended_EventHandlers; // Required here.
class CfgVehicles 
{
	#include <cfgVehicles.hpp>
};

class Cfg3DEN
{
    #include <cfg3DEN.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>