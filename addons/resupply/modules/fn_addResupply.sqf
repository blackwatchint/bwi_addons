#include "\bwi_common\modules\module_header.sqf"

// Run the server only.
if ( isServer  ) then {
	if ( count _units > 0 ) then {
		private _locationData = [];
		
		// Get the module settings.
		private _dialogMode = _logic getVariable ["DialogMode", 1];
		private _locationEntities = _logic getVariable ["LocationEntities", ""];
		private _locationLabel = _logic getVariable ["LocationLabel", ""];

		// Dialog is to use the module defined location data and settings.
		if ( _dialogMode == 1 ) then {
			// Split the entities into an array.
			_locationEntities = _locationEntities splitString ",";
	
			// Organise the entities into an array with their group label.
			if ( count _locationEntities > 0 ) then {
				private _entities = [];

				{
					private _entity = missionNamespace getVariable [_x, objNull];
					
					// Don't include null entities.
					if ( !isNull _entity ) then {
						_entities pushBack _entity;
					};
				} forEach _locationEntities;

				_locationData pushBack [_entities, _locationLabel];
			};
		};

		// Add resupply to all sync'd objects with the specified locationData.
		{
			if ( !(_x isKindOf "Man") ) then {
				[_x, _locationData] call bwi_resupply_fnc_addAction;
			};
		} forEach _units;
	};
};

#include "\bwi_common\modules\module_footer.sqf"