// Initialize the non-public globals for the GUI.
bwi_resupply_gui_locationData = [];

// Run on the server only.
if ( isServer ) then {
	// Initialize the location variables and broadcast.
	 // Note - number of empty child arrays must equal number of staging areas.
	if ( isNil "bwi_resupply_locationDataBlufor" ) then {
		missionNamespace setVariable ["bwi_resupply_locationDataBlufor", [[],[]], true];
	};
	if ( isNil "bwi_resupply_locationDataOpfor" ) then {
		missionNamespace setVariable ["bwi_resupply_locationDataOpfor", [[],[]], true];
	};
	if ( isNil "bwi_resupply_locationDataIndep" ) then {
		missionNamespace setVariable ["bwi_resupply_locationDataIndep", [[],[]], true];
	};
	if ( isNil "bwi_resupply_locationDataCivil" ) then {
		missionNamespace setVariable ["bwi_resupply_locationDataCivil", [[],[]], true];
	};

	// Register the handlers for activation/deactivation of staging areas.
	call bwi_resupply_fnc_registerStagingHandlers
};