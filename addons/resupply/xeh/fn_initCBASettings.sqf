// Define category strings for the CBA settings.
private _categoryLabelResupply = "BWI Resupply";
//private _categoryLabelFunds = "Funds";
private _categoryLabelRestrictions = "Restrictions";


// Initialize CBA settings.
// Advanced settings.
[
    "bwi_resupply_restrictAccessTo",
    "EDITBOX",
    ["Restrict Access To", "List of units that can access the resupply dialog to spawn supplies. Leave blank for unrestricted access."],
    [_categoryLabelResupply, _categoryLabelRestrictions],
    "",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;