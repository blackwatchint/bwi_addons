class BwiGui_RscText;
class BwiGui_RscStructuredText;
class BwiGui_RscPicture;
class BwiGui_RscButton;
class BwiGui_RscXListBox;
class BwiGui_RscTree;

class BwiDialogResupply
{
	idd = 6900;
	movingenable = 0;
	
	class ControlsBackground
	{
		class ResupplyBackground: BwiGui_RscPicture
		{
			idc = 6900;
			text = "\bwi_resupply\gui\data\resupply_bg.paa";
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = -3 * GUI_GRID_H + GUI_GRID_Y;
			w = 38.5 * GUI_GRID_W;
			h = 30 * GUI_GRID_H;
		};
	};
	

	class Controls
	{
		class imgFaction: BwiGui_RscPicture
		{
			idc = 6901;
			text = "";

			x = 34.6 * GUI_GRID_W + GUI_GRID_X;
			y = -2.45 * GUI_GRID_H + GUI_GRID_Y;
			w = 4.35 * GUI_GRID_W;
			h = 3.62 * GUI_GRID_H;
		};

		class lblSide: BwiGui_RscText
		{
			idc = 6902;
			text = "Side:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class xlistSide: BwiGui_RscXListBox
		{
			idc = 6903;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.275,0.463,0.694,1};
			
			onLoad = "[ctrlParent (_this select 0), [6903]] call bwi_armory_fnc_refreshSidesList;";
			onLBSelChanged = "[ctrlParent (_this select 0), [6903, 6905]] call bwi_armory_fnc_refreshFactionsTree;";
		};

		class lblFaction: BwiGui_RscText
		{
			idc = 6904;
			text = "Faction:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvFaction: BwiGui_RscTree
		{
			idc = 6905;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 16 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6903, 6905]] call bwi_armory_fnc_refreshFactionsTree;";
			onTreeSelChanged = "[ctrlParent (_this select 0), [6903, 6905, 6909]] call bwi_resupply_fnc_refreshSuppliesTree;[ctrlParent (_this select 0), [6901, 6905]] call bwi_armory_fnc_refreshFactionImage;";
		};

		class lblLocation: BwiGui_RscText
		{
			idc = 6906;
			text = "Location:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class xlistLocation: BwiGui_RscXListBox
		{
			idc = 6907;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.275,0.463,0.694,1};
			
			onLoad = "[ctrlParent (_this select 0), [6907]] call bwi_resupply_fnc_refreshLocationsList;";
		};

		class lblSupply: BwiGui_RscText
		{
			idc = 6908;
			text = "Resupply:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvSupply: BwiGui_RscTree
		{
			idc = 6909;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 14 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6903, 6905, 6909]] call bwi_resupply_fnc_refreshSuppliesTree;";
			onTreeSelChanged = "[ctrlParent (_this select 0), [6909, 6910, 6911, 6912]] call bwi_resupply_fnc_refreshDetailsText;";
		};

		class lblCargoText: BwiGui_RscStructuredText
		{
			idc = 6910;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 7 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};
		};

		class lblQuantityText: BwiGui_RscStructuredText
		{
			idc = 6911;
			x = 28.5 * GUI_GRID_W + GUI_GRID_X;
			y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};
		};

		class lblCompatibleText: BwiGui_RscStructuredText
		{
			idc = 6912;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};

			onLoad = "[ctrlParent (_this select 0), [6909, 6910, 6911, 6912]] call bwi_resupply_fnc_refreshDetailsText;"; // Called on last one only so it handles both!
		};

		class lblErrorText: BwiGui_RscStructuredText
		{
			idc = 6913;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.7 * GUI_GRID_H + GUI_GRID_Y;
			w = 16.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorText[] = {1,0,0,1};
		};

		class btnCancel: BwiGui_RscButton
		{
			idc = 6914;
			text = "Close";
			x = 25 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			onMouseButtonClick = "closeDialog 2;";
		};

		class btnSelect: BwiGui_RscButton
		{
			idc = 6915;
			text = "Request";
			x = 31.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			
			onMouseButtonClick = "[ctrlParent (_this select 0), [6905, 6907, 6909, 6913]] call bwi_resupply_fnc_selectSupplyGUI;";
		};
	};
};