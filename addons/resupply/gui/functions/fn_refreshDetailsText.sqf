/**
 * ctrlIDCs: tvSupply, lblCargoText, lblQuantityText, lblCompatibleText
 */
params ["_display", "_ctrlIDCs"];

private _tvSupply        	  = _display displayctrl (_ctrlIDCs select 0);
private _lblCargoText    	  = _display displayctrl (_ctrlIDCs select 1);
private _lblQuantityText      = _display displayctrl (_ctrlIDCs select 2);
private _lblCompatibleText    = _display displayctrl (_ctrlIDCs select 3);

// Set the default values.
private _supplyCargo = "-";
private _supplyQuantity = "-";
private _supplyCompatible = "-";

// Load selected crate's config data.
private _supplyData = _tvSupply tvData (tvCurSel _tvSupply);
private _supplyClasses = _supplyData splitString "/";

// Process supplies only, not categories.
if ( count _supplyClasses >= 2 ) then {
	// Get the supply's CfgPlayableSupplies and CfgVehicles configs entries.
	private _supplyCfg = configFile >> "CfgPlayableSupplies" >> _supplyClasses select 0 >> _supplyClasses select 1;
	private _supplyClassname = getText (_supplyCfg >> "classname");
	private _crateCfg = configFile >> "CfgVehicles" >> _supplyClassname;

	// Get compatible weapons and cargo space values from their configs.
	_supplyCargo = getNumber (_crateCfg >> "ace_cargo_size");
	_supplyCompatible = getArray (_supplyCfg >> "compatible");

	// Merge the compatible weapons array into a string.
	if ( count _supplyCompatible >= 1 ) then {
		_supplyCompatible = _supplyCompatible joinString ", ";
	} else {
		_supplyCompatible = "-";
	};

	// Iterate through all TransportX children and add up the item quantities.
	private _transportMagazines = "true" configClasses (_crateCfg >> "TransportMagazines");
	private _transportWeapons = "true" configClasses (_crateCfg >> "TransportWeapons");
	private _transportItems = "true" configClasses (_crateCfg >> "TransportItems");
	private _transportAll = _transportMagazines + _transportWeapons + _transportItems;

	_supplyQuantity = 0;
	{
		_supplyQuantity = _supplyQuantity + getNumber (_x >> "count");
	} forEach _transportAll;
};

// Apply the text to the detail panels.
_lblCargoText ctrlSetStructuredText parseText format["Cargo Size: %1", _supplyCargo];
_lblQuantityText ctrlSetStructuredText parseText format["Items: %1", _supplyQuantity];
_lblCompatibleText ctrlSetStructuredText parseText format["For: %1", _supplyCompatible];