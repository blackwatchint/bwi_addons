/**
 * ctrlIDCs: xListLocation
 */
params ["_display", "_ctrlIDCs"];

private _xlistLocation = _display displayctrl (_ctrlIDCs select 0);

lbClear _xlistLocation;

// Add each defined location to the list.
// Note we set the value to the array's index.
{
	if ( count _x > 0 ) then {
		private _listId = _xlistLocation lbAdd (_x select 1);
		_xlistLocation lbSetValue [_listId, _forEachIndex];
	};
} forEach bwi_resupply_gui_locationData;

// Set current to 0.
_xlistLocation lbSetCurSel 0;