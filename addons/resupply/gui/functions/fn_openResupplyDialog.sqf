params [["_locationData", []]];

// If no location data is specified, we are operating in staging mode.
if ( count _locationData == 0 ) then {
	// Set the location data array according to side.
	switch ( playerSide ) do {
		case west: 		 { _locationData = bwi_resupply_locationDataBlufor;};
		case east: 		 { _locationData = bwi_resupply_locationDataOpfor; };
		case resistance: { _locationData = bwi_resupply_locationDataIndep; };
		case civilian:   { _locationData = bwi_resupply_locationDataCivil; };
	};
};

// Set the dialog instance global.
bwi_resupply_gui_locationData = _locationData;

// Open the dialog.
createDialog 'BwiDialogResupply';