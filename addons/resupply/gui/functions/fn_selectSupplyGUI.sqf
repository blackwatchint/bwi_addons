/**
 * ctrlIDCs: tvFaction, xlistLocation, tvSupply, lblErrorText
 */
params ["_display", "_ctrlIDCs"];

if( !local player ) exitWith {};

private _tvFaction 		= _display displayctrl (_ctrlIDCs select 0);
private _xlistLocation  = _display displayctrl (_ctrlIDCs select 1);
private _tvSupply 		= _display displayctrl (_ctrlIDCs select 2);
private _lblErrorText   = _display displayctrl (_ctrlIDCs select 3);

// Prepare for error messages.
private _error = false;
private _errorMsg = "";

// Get currently selected values from the GUI.
private _locationIndex = _xlistLocation lbValue (lbCurSel _xlistLocation);
private _locationData = bwi_resupply_gui_locationData select _locationIndex;
private _factionData = _tvFaction tvData (tvCurSel _tvFaction);
private _factionClasses = _factionData splitString "/";
private _supplyData = _tvSupply tvData (tvCurSel _tvSupply);
private _supplyClasses = _supplyData splitString "/";

// Error check the selections!
if ( count _supplyClasses < 2 ) then {
	_error = true;
	_errorMsg = "Please select a supply!";
};

if ( count _factionClasses < 2 ) then {
	_error = true;
	_errorMsg = "Please select a faction!";
};

// No errors so far.
if ( !_error ) then {
	// Get the CfgPlayableSupplies and CfgPlayableFactions entries.
	private _supplyCfg = configFile >> "CfgPlayableSupplies" >> _supplyClasses select 0 >> _supplyClasses select 1;
	private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;

	// Get supply crate values from the config.
	private _supplyName = _tvSupply tvText (tvCurSel _tvSupply);
	private _supplyClassname = getText (_supplyCfg >> "classname");
	private _supplySize = [_supplyClassname] call bwi_common_fnc_getVehicleSize;
	private _supplyRadius = _supplySize / 2;

	// Get the textures to use for this crate 
	private _vehicleCfg = configFile >> "CfgVehicles" >> _supplyClassname;
	private _supplyTextures = getArray (_factionCfg >> "resupplyTextures");
	private _supplyTextureIndexes = getArray (_supplyCfg >> "textureIndexes");

	// Get the location data.
	private _allLocations = _locationData select 0;
	private _locationName = _locationData select 1;

	// Find a free spawn position for the crate.
	private _locationPos = [];
	private _locationDir = 0;
	private _locationFound = false;
	{
		_locationPos = getPosATL _x;
		_locationDir = getDir _x;

		// Find nearby crates/vehicle spares. We use a custom function to account for ATL/ASL conversions.
		private _cratesNear = [_locationPos, _supplyRadius, ["ReammoBox_F","Items_base_F","StaticWeapon","ACE_RepairItem_base"]] call bwi_common_fnc_countObjectsNear;

		// Exit when a location is found.
		if ( _cratesNear == 0 ) exitWith {
			_locationFound = true;
		};
	} forEach _allLocations;

	// Location has been found, check for other blockages.
	if ( _locationFound ) then {
		// Check for players or vehicles in the way.
		private _playersNear = [_locationPos, _supplyRadius, sideEmpty, true] call bwi_common_fnc_countPlayersNear;
		private _vehiclesNear = [_locationPos, _supplyRadius, false] call bwi_common_fnc_countVehiclesNear;

		if ( (_playersNear + _vehiclesNear) > 0 ) then {
			_error = true;
			_errorMsg = "Players or vehicles are in the way.";
		};
	// No location found, error out.
	} else {
		_error = true;
		_errorMsg = "No space for more supplies.";
	};

	// Still no errors so far.
	if ( !_error ) then {
		// Spawn and fill the supply crate.
		private _vehicle = [
			_locationPos,
			_locationDir,
			_supplyClassname,
			_supplyTextures,
			_supplyTextureIndexes
		] call bwi_resupply_fnc_spawnSupply;

		// Inform the user of selection.
		private _message = format ["%1<br/>to %2", _supplyName, _locationName];
		["Supplies Delivered", _message, 4] call bwi_common_fnc_notification;

		// Clear any error messages.
		_lblErrorText ctrlSetStructuredText parseText "";
	};
};

// Errors.
if ( _error ) then {
	_lblErrorText ctrlSetStructuredText parseText format["<t color='#ff1111'>%1</t>", _errorMsg];
};

false