/**
 * ctrlIDCs: xListSide, tvFaction, tvSupply
 */
params ["_display", "_ctrlIDCs"];

private _xlistSide 		= _display displayctrl (_ctrlIDCs select 0);
private _tvFaction 		= _display displayctrl (_ctrlIDCs select 1);
private _tvSupply 		= _display displayctrl (_ctrlIDCs select 2);

tvClear _tvSupply;

// Load selected faction's supply data.
private _side = lbCurSel _xlistSide;
private _factionData = _tvFaction tvData (tvCurSel _tvFaction);
private _factionClasses = _factionData splitString "/";

if ( count _factionClasses < 2 ) exitWith {}; // Handle un-selectable parent factions gracefully.

// Fetch the supply categories from CfgPlayableSupplies.
private _categoryCfgs = "true" configClasses (configFile >> "CfgPlayableSupplies");

// Load selected faction's supply filter data and create the filter.
private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;
private _allowedSupplies = getArray (_factionCfg >> "allowedSupplies");

// Independently filter for weapons, recon and special forces resupply.
private _allowedSuppliesRecon = [];
private _allowedSuppliesSpecial = [];
private _allowedSuppliesWeapons = [];

if ( bwi_armory_unlockReconElements ) then {
	_allowedSuppliesRecon = getArray (_factionCfg >> "allowedSuppliesRecon");
	_allowedSupplies = _allowedSupplies + _allowedSuppliesRecon;
};
if ( bwi_armory_unlockSpecialElements ) then {
	_allowedSuppliesSpecial = getArray (_factionCfg >> "allowedSuppliesSpecial");
	_allowedSupplies = _allowedSupplies + _allowedSuppliesSpecial;
};
if ( bwi_armory_unlockWeaponsElements ) then {
	_allowedSuppliesWeapons = getArray (_factionCfg >> "allowedSuppliesWeapons");
	_allowedSupplies = _allowedSupplies + _allowedSuppliesWeapons;
};

private _supplyFilter = format ["(configName(_x) in %1)", _allowedSupplies];

// Build categories/supplies tree.
{
	// Fetch the supplies according to those permitted for this faction.
	private _supplyCfgs = _supplyFilter configClasses (_x);

	// Don't render if there's no supplies.
	if ( count _supplyCfgs > 0 ) then {
		private _parentId = _tvSupply tvAdd [[], getText (_x >> "name")];
		private _parentData = configName (_x);

		{
			// Generate name and data values from config.
			private _childClassname = getText (_x >> "classname");
			private _childName = getText (configFile >> "CfgVehicles" >> _childClassname >> "displayName");
			private _childData = format["%1/%2", _parentData, configName (_x)];

			// Add supply child entry.
			private _childId = _tvSupply tvAdd [[_parentId], _childName];
			_tvSupply tvSetData [[_parentId, _childId], _childData];

			// Determine if supply is for recon, special or weapons.
			private _isReconResupply   = ( configName (_x) in _allowedSuppliesRecon );
			private _isSpecialResupply = ( configName (_x) in _allowedSuppliesSpecial );
			private _isWeaponsResupply = ( configName (_x) in _allowedSuppliesWeapons );

			// Add supply child icon according to intended elements.
			private _childIcon = "\bwi_data\data\icon_element_r.paa";

			if ( _isReconResupply && !_isSpecialResupply ) then { _childIcon = "\bwi_data\data\icon_element_lr.paa"; };
			if ( !_isReconResupply && _isSpecialResupply ) then { _childIcon = "\bwi_data\data\icon_element_sf.paa"; };
			if ( _isReconResupply && _isSpecialResupply )  then { _childIcon = "\bwi_data\data\icon_element_lrsf.paa"; };
			if ( _isWeaponsResupply ) 					   then { _childIcon = "\bwi_data\data\icon_element_w.paa"; };

			_tvSupply tvSetPicture [[_parentId, _childId], _childIcon];
		} forEach _supplyCfgs;

		// Sort the child tree.
		_tvSupply tvSort [[_parentId], false];
	};
} forEach _categoryCfgs;