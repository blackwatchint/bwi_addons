#include <defines.hpp>

class CfgPatches
{
	class bwi_resupply
	{
		requiredVersion = 1.0;
		authors[] = {"Fourjays"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
	    version = 5.1;
	    versionStr = "5.1.0";
	    versionAr[] = {5,1,0};
		requiredAddons[] = {
			"A3_Modules_F", 
			"A3_UI_F", 
			"cba_settings", 
			"bwi_common",
			"bwi_data",
            "bwi_armory"
		};
		units[] = {};
	};
};

class CfgFactionClasses
{
	#include <cfgFactionClasses.hpp>
};

class CfgVehicleClasses
{
	#include <cfgVehicleClasses.hpp>
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgRemoteExec
{
	#include <cfgRemoteExec.hpp>
};

class CfgVehicles
{
	#include <cfgVehicles.hpp>
};

class Cfg3DEN
{
    #include <cfg3DEN.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>

// Common GUI Definitions
#include <\common\gui\macros.hpp> // Absolute path

// GUI Definitions
#include <gui\resupply.hpp>