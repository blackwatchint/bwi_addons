class bwi_resupply
{
	class functions
	{
		file = "\bwi_resupply\functions";

		class addAction{};

		class spawnSupply{};

		class registerStagingHandlers{};

		class checkMissionConfig{};
	};

	class gui_functions
	{
		file = "\bwi_resupply\gui\functions";
		class openResupplyDialog{};
		class refreshLocationsList{};
		class refreshSuppliesTree{};
		class refreshDetailsText{};
		class selectSupplyGUI{}; 
	};

	class xeh
	{
		file = "\bwi_resupply\xeh";
		class initCBASettings{};
		class initResupply{};
	};
};

class bwi_resupply_module
{
	class modules
	{
		file = "\bwi_resupply\modules";
		class addResupply{};
	};
};