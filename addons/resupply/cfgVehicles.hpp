class Logic;
class Module_F: Logic
{
	class ArgumentsBaseUnits
	{
		class Units;
	};
	class ModuleDescription;
};


/**
 * Adds a BWI Resupply interface to any synchronised objects.
 * This module has two modes. Staging mode will add the UI and
 * link itself to locations registered through BWI Staging.
 * Manual mode will add the UI with the options specified
 * by this module.
 */ 
class BWI_Resupply_ModuleAddResupply: Module_F
{
	scope = 2;
	scopeCurator = 1;
	displayName = "Add Resupply";
	icon = "\bwi_resupply\data\icon_resupply_w.paa";
	category = "BWI_Resupply";
	function = "bwi_resupply_module_fnc_addResupply";
	functionPriority = 1;
	isGlobal = 0;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class Arguments: ArgumentsBaseUnits
	{
		class DialogMode {
			displayName = "Dialog Mode";
			description = "Manual will use the data below for spawn locations on this instance only. Staging will use locations defined by CfgStagingAreas in the mission description.ext.";
			typeName = "NUMBER";
			class Values {
				class STAGING	{ name = "Staging"; value = 0; };
				class MODULE	{ name = "Manual";	value = 1; default = 1; };
			};
		};

		class LocationEntities {
			displayName = "Location Entities";
			description = "List of named objects used to define the spawn positions used by this dialog. Ignored in Function mode.";
			typeName = "STRING";
			defaultValue = "";
		};

		class LocationLabel {
			displayName = "Location Label";
			description = "A user-friendly name for the group of locations defined above. Ignored in Function mode.";
			typeName = "STRING";
			defaultValue = "";
		};
	};

	class ModuleDescription: ModuleDescription
	{
		description = "Enables usage of BWI Resupply on synchronised object.";
		sync[] = {};
	};
};


/**
 * Base crate inherited by all other resupply crates.
 */
class NATO_Box_Base;
class BWI_Resupply_CrateBase: NATO_Box_Base {
	scope = 0;
	vehicleClass = "BWI_Resupply";
	displayName = "Resupply Crate";
	model = "\A3\weapons_F\AmmoBoxes\AmmoBox_F";
	icon = "iconCrateAmmo";
	maximumLoad = 0;
	ace_cargo_size = 1;
	
	hiddenSelections[] = {"Camo_Signs", "Camo"};
};


/**
 * Base crate used for airborne resupply. This base crate can
 * be used and referenced directly as there are no texture
 * components.
 */
class B_supplyCrate_F;
class Bwi_Resupply_CrateAirBase: B_supplyCrate_F {
	scope = 0;
	vehicleClass = "BWI_Resupply";
	displayName = "Airborne Resupply";
	model = "\A3\Weapons_F\Ammoboxes\Supplydrop.p3d";
	icon = "iconCrateAmmo";
	maximumLoad = 0;
	ace_cargo_size = 8;
	
	class TransportBackpacks {
		//Force empty.
	};
};


/**
 * Base crates with icon locations setup to match the textured
 * labels as follows:
 *   [Small1] [Small2]
 *   [Long1]  [Medium2]
 *   [Small3] [Small4]
 *            [Medium1]
 */
class BWI_Resupply_CrateBaseSmall1: BWI_Resupply_CrateBase {
	hiddenSelections[] = {"Camo_Signs", "Camo"};

	class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
	};
};

class BWI_Resupply_CrateBaseSmall2: BWI_Resupply_CrateBase {
	hiddenSelections[] = {"Camo_Signs", "Camo"};

    class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
	};
};

class BWI_Resupply_CrateBaseSmall3: BWI_Resupply_CrateBase {
	hiddenSelections[] = {"Camo_Signs", "Camo"};

    class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
	};
};

class BWI_Resupply_CrateBaseSmall4: BWI_Resupply_CrateBase {
	hiddenSelections[] = {"Camo_Signs", "Camo"};

    class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
	};
};

class BWI_Resupply_CrateBaseMedium1: BWI_Resupply_CrateBase
{
	model = "\A3\weapons_F\AmmoBoxes\WpnsBox_large_F";
	ace_cargo_size = 3;

	hiddenSelections[] = {"Camo_Signs", "Camo"};
};

class BWI_Resupply_CrateBaseMedium2: BWI_Resupply_CrateBase
{
    model = "\A3\weapons_F\AmmoBoxes\WpnsBox_F";
	ace_cargo_size = 2;

	hiddenSelections[] = {"Camo_Signs", "Camo"};
};

class BWI_Resupply_CrateBaseLong1: BWI_Resupply_CrateBase
{
    model = "\A3\weapons_F\AmmoBoxes\WpnsBox_long_F";
	ace_cargo_size = 2;
	
	hiddenSelections[] = {"Camo_Signs", "Camo"};
};