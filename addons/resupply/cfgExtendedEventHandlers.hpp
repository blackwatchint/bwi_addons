class Extended_PreInit_EventHandlers {
    class bwi_resupply_settings {
        init = "call bwi_resupply_fnc_initCBASettings;";
    };
};

class Extended_PostInit_EventHandlers {
    class bwi_resupply_init {
        init = "call bwi_resupply_fnc_initResupply;";
    };
};