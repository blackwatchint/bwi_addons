if ( is3DEN ) then {
	private _stagingAreasCfg = missionConfigFile >> "CfgStagingAreas";

	// Staging is enabled.
	if ( isClass _stagingAreasCfg ) then {
		private _allLogicNames = [3] call bwi_common_fnc_get3DENEntityNames;

		// Now check each staging area.
		private _stagingCfgs = "true" configClasses (_stagingAreasCfg);

		{
			private _resupplyLocations = getArray (_x >> "resupplyLocations");

			// Check for missing logic entities.
			{
				// Location missing.
				if ( !(_x in _allLogicNames) ) then {
					["Resupply location %1 does not exist.", _x] call bwi_common_fnc_log;
				};
			} forEach _resupplyLocations;
		} forEach _stagingCfgs;
	};
};