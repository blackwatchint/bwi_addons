params ["_addToObject", "_locationData"];

private _actionCondition = "true";

// Add the action condition if restricted access is enabled.
private _restrictAccessTo = bwi_resupply_restrictAccessTo splitString ",";
if ( count _restrictAccessTo > 0 ) then {
	_actionCondition = format["(str _this) in %1", _restrictAccessTo];
};

// Add the action and broadcast.
[_addToObject, [
	"<t color='#8ed500'>Resupply</t>", 
	{ (_this select 3) call bwi_resupply_fnc_openResupplyDialog; }, 
	[_locationData], 1.3, false, true, "", _actionCondition
]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP