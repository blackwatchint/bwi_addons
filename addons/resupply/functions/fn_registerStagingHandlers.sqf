/**
 * Event handler that creates resupply spawn locations
 * from CfgStagingAreas data for the given staging area.
 */
["bwi_staging_createStaging",
	{
		params ["_side", "_index", "_stagingArea", "_flagpole"];

		// Log the creation.
		["Creating resupply locations for %1...", _stagingArea] call bwi_common_fnc_log;

		private _stagingAreaCfg = missionConfigFile >> "CfgStagingAreas" >> _stagingArea;
		private _resupplyLocations = getArray (_stagingAreaCfg >> "resupplyLocations");
		private _locationLabel = getText (_stagingAreaCfg >> "name"); // Use staging area name for the group of spawn points.

		// Iterate over the resupply positions, adding them to an array for this staging area's index.
		private _entities = [];
		{
			private _entity = missionNamespace getVariable _x;
			_entities pushBack _entity;
		} forEach _resupplyLocations;

		private _data = [_entities, _locationLabel];

		// Overwrite the data for this side's staging index and broadcast.
		switch ( _side ) do {
			case west: 		 { 
				bwi_resupply_locationDataBlufor set [_index, _data];
				publicVariable "bwi_resupply_locationDataBlufor";
			};
			case east: 		 {
				bwi_resupply_locationDataOpfor set [_index, _data];
				publicVariable "bwi_resupply_locationDataOpfor";
			};
			case resistance: {
				bwi_resupply_locationDataIndep set [_index, _data];
				publicVariable "bwi_resupply_locationDataIndep";
			};
			case civilian:   {
				bwi_resupply_locationDataCivil set [_index, _data];
				publicVariable "bwi_resupply_locationDataCivil";
			};
		};

		// Add the resupply action to the flagpole (empty array for staging).
		[_flagpole, []] call bwi_resupply_fnc_addAction;
	}
] call CBA_fnc_addEventHandlerArgs;


/**
 * Event handler that removes resupply spawn locations when 
 * staging areas are set to "NONE". Changes to staging areas 
 * that aren't "NONE" will be handled by the createStaging 
 * handler and are ignored to reduce network broadcasting.
 */
["bwi_staging_removeStaging",
	{
		params ["_side", "_index", "_stagingArea"];

		if ( _stagingArea == "NONE" ) then { 
			// Overwrite the data for this side's staging index and broadcast.
			switch ( _side ) do {
				case west: 		 { 
					bwi_resupply_locationDataBlufor set [_index, []];
					publicVariable "bwi_resupply_locationDataBlufor";
				};
				case east: 		 {
					bwi_resupply_locationDataOpfor set [_index, []];
					publicVariable "bwi_resupply_locationDataOpfor";
				};
				case resistance: {
					bwi_resupply_locationDataIndep set [_index, []];
					publicVariable "bwi_resupply_locationDataIndep";
				};
				case civilian:   {
					bwi_resupply_locationDataCivil set [_index, []];
					publicVariable "bwi_resupply_locationDataCivil";
				};
			};
		};
	}
] call CBA_fnc_addEventHandlerArgs;