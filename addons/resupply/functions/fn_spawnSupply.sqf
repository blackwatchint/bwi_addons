params ["_position", "_direction", "_classname", ["_textureData", []], ["_textureIndexes", []]];

// Create the supply crate.
private _crate = createVehicle [_classname, _position, [], 0, "CAN_COLLIDE"];
_crate setDir _direction;

// Disable damage on the supply crate.
_crate allowDamage false;

// Set the crate textures according to the passed data.
if ( count _textureIndexes > 0 ) then {
	{
		// Skip -1 indexes and non-local crates (just in-case).
		if ( _x >= 0 && local _crate ) then {
			// Only process if textureData has a matching index.
			if ( _x < count _textureData ) then {
				// Set the crate texture.
				private _texture = _textureData select _x;
				_crate setObjectTextureGlobal [_forEachIndex, _texture];
			};
		};
	} forEach _textureIndexes;
};

// Return the crate.
_crate