// Note - There must be no trailing space between the count and classname.
#define MACRO_ADDWEAPON(COUNT,WEAPON) class _xx_##WEAPON { weapon = #WEAPON; count = COUNT; }
#define MACRO_ADDITEM(COUNT,ITEM) class _xx_##ITEM { name = #ITEM; count = COUNT; }
#define MACRO_ADDMAGAZINE(COUNT,MAGAZINE) class _xx_##MAGAZINE { magazine = #MAGAZINE; count = COUNT; }
#define MACRO_ADDBACKPACK(COUNT,BACKPACK) class _xx_##BACKPACK { backpack = #BACKPACK; count = COUNT; }