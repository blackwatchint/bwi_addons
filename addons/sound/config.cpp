class CfgPatches 
{
	class bwi_sound
	{
		name = "bwi_sound";
		requiredVersion = 1.0;
		author = "Black Watch International";
		authors[] = {"0mega"};
		url = "http://blackwatch-int.com";
		version = 1.0;
		versionStr = "1.0.0";
		versionAr[] = {1,0,0};
		requiredAddons[] = {
			
		};
		units[] = {};
	};
};


// Placeholder for future configurations.