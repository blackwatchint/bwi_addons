class BWA3_G36A1 : Rifle_Base_F 
{
	class Single: Mode_SemiAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_g36_shot_soundset","jsrs_g36_shell_soundset","jsrs_5x56mm_reflector_2"};
		};
		class SilencedSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_g36_shot_silenced_soundset","jsrs_g36_shell_soundset","jsrs_5x56mm_SD_reflector_2"};
		};
	};
	class FullAuto: Mode_FullAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_g36_shot_soundset","jsrs_g36_shell_soundset","jsrs_5x56mm_reflector_2"};
		};
		class SilencedSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_g36_shot_silenced_soundset","jsrs_g36_shell_soundset","jsrs_5x56mm_SD_reflector_2"};
		};
	};
};
	
class BWA3_G38 : Rifle_Base_F 
{
	class Single: Mode_SemiAuto	
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_spar_shot_soundset","jsrs_spar_shell_soundset","jsrs_5x56mm_reflector_1"};
		};
		class SilencedSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_spar_shot_silenced_soundset","jsrs_spar_shell_soundset","jsrs_5x56mm_sd_reflector_1"};
		};
	};
	class FullAuto: Mode_FullAuto	
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_spar_shot_soundset","jsrs_spar_shell_soundset","jsrs_5x56mm_reflector_1"};
		};
		class SilencedSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_spar_shot_silenced_soundset","jsrs_spar_shell_soundset","jsrs_5x56mm_sd_reflector_1"};
		};
	};
};
	
class BWA3_MG3: Rifle_Long_Base_F 
{
	class burst: Mode_FullAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_mg42_shot_soundset","jsrs_mg42_shell_soundset","jsrs_7x62mm_reflector_1"};
		};
	};
};

class BWA3_MG4: Rifle_Long_Base_F 
{
	class manual: Mode_FullAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[]= {"jsrs_m249_shot_soundset","BWA3_MG4_Tail_SoundSet","BWA3_MG4_InteriorTail_SoundSet"};
		};
	};
};

class BWA3_MG5: Rifle_Long_Base_F 
{
	class manual: Mode_FullAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[]= {"jsrs_m240_shot_soundset","BWA3_MG5_Tail_SoundSet","BWA3_MG5_InteriorTail_SoundSet"};
		};
	};
};

class BWA3_G28: Rifle_Long_Base_F 
{
	class Single: Mode_SemiAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_mar10_shot_soundset","jsrs_mar10_shell_soundset","jsrs_7x62mm_reflector_1"};
		};
	};
};

class BWA3_G27: BWA3_G28 
{
	class FullAuto: Mode_FullAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_mar10_shot_soundset","jsrs_mar10_shell_soundset","jsrs_7x62mm_reflector_1"};
		};
	};
};

class BWA3_G82: Rifle_Long_Base_F 
{
	class Single: Mode_SemiAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_m320r_shot_soundset","jsrs_m320r_shell_soundset","jsrs_12x7mm_reflector_1"};
		};
	};
};

class BWA3_G29: Rifle_Long_Base_F 
{
	class Single: Mode_SemiAuto 
	{
		class BaseSoundModeType;
		class StandardSound: BaseSoundModeType 
		{
			soundSetShot[] = {"jsrs_rifle_shake_soundset","jsrs_awm_shot_soundset","jsrs_awm_shell_soundset","jsrs_12x7mm_reflector_1"};
		};
	};
};