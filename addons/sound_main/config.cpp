class CfgPatches 
{
	class bwi_sound_main
	{
		name = "bwi_sound_main";
		requiredVersion = 1.0;
		author = "Black Watch International";
		authors[] = {"0mega"};
		url = "http://blackwatch-int.com";
		version = 1.0;
		versionStr = "1.0.0";
		versionAr[] = {1,0,0};
		requiredAddons[] = {
			"JSRS_SOUNDMOD_Weapons",
			"BWA3_G36",
			"BWA3_G38",
			"BWA3_MG3",
			"BWA3_MG4",
			"BWA3_MG5",
			"BWA3_G28",
			"BWA3_G29",
			"BWA3_G82"
		};
		units[] = {};
	};
};

class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;

class CfgWeapons {
	#include <cfgWeapons.hpp>
};