class CfgPatches
{
	class bwi_utilities
	{
		requiredVersion = 1.0;
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
		authors[] = {"Fourjays", "RedBery", "Dhorkiy", "0mega", "Jessar"};
		version = 2.1;
		versionStr = "2.1.0";
		versionAr[] = {2,1,0};
		requiredAddons[] = {
			"A3_Modules_F",
            "A3_UI_F", 
            "cba_settings",
			"ace_arsenal",
			"ace_zeus",
			"bwi_common",
			"bwi_motorpool"
		};
		units[] = {
			"BWI_Utilities_ModuleAddObjectsToCurator",
			"BWI_Utilities_ModuleAddVehiclesToCurator",
			"BWI_Utilities_ModuleRemoveObjectsFromCurator",
			"BWI_Utilities_ModuleRemoveVehiclesFromCurator",
			"BWI_Utilities_ModuleToggleZeusVisibility",
			"BWI_Utilities_ModuleTeleportZeus",
			"BWI_Utilities_ModuleRefundVehicle",
			"BWI_Utilities_ModuleAddArsenal",
			"BWI_Utilities_ModulePreventHCTransfer",
			"BWI_Utilities_ModuleAIForceHoldPosition",
			"BWI_Utilities_ModuleCheckClientFPS",
			"BWI_Utilities_ModuleCheckServerFPS",
			"BWI_Utilities_ModuleDamageEngine",
			"BWI_Utilities_ModuleDamageFuel",
			"BWI_Utilities_ModuleDamageMainRotor",
			"BWI_Utilities_ModuleDamageTailRotor",
			"BWI_Utilities_ModuleDamageAvionics",
			"BWI_Utilities_ModuleFullRepair"
		};
	};
};

class CfgFactionClasses
{
	#include <cfgFactionClasses.hpp>
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgRemoteExec
{
	#include <cfgRemoteExec.hpp>
};

class CfgVehicles
{
	#include <cfgVehicles.hpp>
};

class CfgRespawnTemplates
{
	#include <cfgRespawnTemplates.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>