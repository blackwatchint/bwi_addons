class bwi_utilities
{
	class functions
	{
		file = "\bwi_utilities\functions";

		class addToCurator{};
		class removeFromCurator{};

		class clearVehicleCargo{};
		class findExemptVehicleCargo{};

		class addArsenalAction{};
		class addQuickRespawnAction{};
		class addTeleportAction{};

		class quickRespawn{};
		class setRespawnTimer{};

		class addCorpseCleanupZone{};
		class removeCorpse{};

		class disablePlayer{};

		class requestFPSLocal{};
		class displayFPSCurator{};

		class damageVehicle{};
		class repairVehicle{};
		class refundAsset{};
		
		class registerCorpseCleanupHandlers{};
		class registerMotorpoolClearVehicleHandler{};
		class registerZeusClearVehicleHandler{};
		class registerZeusRCShortcutHandler{};
	};

	class xeh
	{
		file = "\bwi_utilities\xeh";

		class initCBAKeybinds{};
		class initCBASettings{};
		class initUtilities{};
	};
};

class bwi_utilities_module
{
	class module
	{
		file = "\bwi_utilities\modules";

		class addObjectsToCurator{};
		class addVehiclesToCurator{};
		class removeObjectsFromCurator{};
		class removeVehiclesFromCurator{};

		class teleportZeus{};
		class toggleZeusVisibility{};

		class addArsenal{};
		class addQuickRespawn{};
		class addTeleportTo{};

		class preventHCTransfer{};
		class aiForceHoldPosition{};

		class endMission{};

		class checkClientFPS{};
		class checkServerFPS{};

		class damageAvionics{};
		class damageEngine{};
		class damageFuel{};
		class damageMainRotor{};
		class damageTailRotor{};
		class repairDamage{};
		class refundVehicle{};
	};
};