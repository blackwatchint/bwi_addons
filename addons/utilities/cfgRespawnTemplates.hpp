class bwi_respawnBehaviour
{
	// Call function on death.
	onPlayerKilled = "bwi_utilities_fnc_setRespawnTimer";

	// Default respawn delay (see https://community.bistudio.com/wiki/Description.ext)
	respawnDelay = 60;

	// Disable respawn at start (see https://community.bistudio.com/wiki/Description.ext)
	respawnOnStart = -1;
};