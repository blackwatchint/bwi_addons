// Define category strings for the CBA settings.
private _categoryLabelUtilities = "BWI Utilities";
private _categoryLabelInventories = "Vehicle Inventories";
private _categoryLabelCorpses = "Corpse Cleanup";

// Initialize CBA settings.
// Vehicle Inventory settings.
[
    "bwi_utilities_clearVehicleCargo",
    "CHECKBOX",
    ["Clear Vehicle Inventories", "When enabled, the inventories of all vehicles will be cleared of their contents."],
    [_categoryLabelUtilities, _categoryLabelInventories],
    true,
    true
] call CBA_settings_fnc_init;

[
    "bwi_utilities_ignoreCargoItems",
    "EDITBOX",
    ["Ignore Item Classes", "List of classnames that will be skipped when clearing vehicle inventories."],
    [_categoryLabelUtilities, _categoryLabelInventories],
    "",
    true
] call CBA_settings_fnc_init;


// Corpse Cleanup settings.
[
    "bwi_utilities_corpseCleanupRadius",
    "SLIDER",
    ["Removal Radius", "Radius around spawn and staging flagpoles within which player bodies will be removed. Set to 0 to disable."],
    [_categoryLabelUtilities, _categoryLabelCorpses],
    [0, 500, 100, 0],
    true
] call CBA_settings_fnc_init;