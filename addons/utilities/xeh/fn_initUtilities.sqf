// Initialize global variables.
if ( isNil "bwi_utilities_corpseCleanupZones" ) then {
	bwi_utilities_corpseCleanupZones = [];
};

// Register the body cleanup handlers for both client and server.
// Note that this function handles client/server distinctions itself.
call bwi_utilities_fnc_registerCorpseCleanupHandlers;


// Run on the server only.
if ( isServer ) then {
	// Clear inventory of any editor-placed vehicles.
	{
		[_x] call bwi_utilities_fnc_clearVehicleCargo;
	} forEach vehicles;
};


// Run for clients only.
if ( hasInterface ) then {
	// Initialize global variables.
	if ( isNil "bwi_utilities_rcsModifierPressed" ) then {
		bwi_utilities_rcsModifierPressed = false;
	};

	// Register the motorpool clear vehicle handler for clients.
	call bwi_utilities_fnc_registerMotorpoolClearVehicleHandler;

	/**
	* Waits for the curator logics to be assigned before 
	* registering event handlers that must be run within 
	* a curator only context.
	*/
	[
		{
			(({ !isNull getAssignedCuratorUnit _x } count allCurators) > 0)
		},
		{
			// Iterate over all curator logics, to find the local player.
			{
				if ( (getAssignedCuratorUnit _x) == player ) then {
					// Register the clear vehicle handler for curator.
					[_x] call bwi_utilities_fnc_registerZeusClearVehicleHandler;

					// Register the RC Shortcut for this curator instance.
					[_x] call bwi_utilities_fnc_registerZeusRCShortcutHandler;
				};
			} forEach allCurators;
		}
	] call CBA_fnc_waitUntilAndExecute;
};
