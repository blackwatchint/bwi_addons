// Include DIK codes.
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

// Define category strings for the CBA settings.
private _categoryLabelUtilities = "BWI Utilities";

// Initialize CBA keybinds.
[
    _categoryLabelUtilities,
    "bwi_utilities_curatorRCShortcut",
    ["Zeus RC Shortcut Modifier", "Double-clicking a unit while holding this key will start Zeus Remote Control."],
    {
        bwi_utilities_rcsModifierPressed = true;
        false
    },
    {
        bwi_utilities_rcsModifierPressed = false;
        false
    },
    [DIK_LCONTROL, [false, false, false]]
] call CBA_fnc_addKeybind;