class Extended_PreInit_EventHandlers {
    class bwi_utilities_settings {
        init = "call bwi_utilities_fnc_initCBASettings;";
    };
};

class Extended_PostInit_EventHandlers {
    class bwi_utilities_init {
        init = "call bwi_utilities_fnc_initUtilities;";
    };

    class bwi_utilities_keybinds {
        init = "call bwi_utilities_fnc_initCBAKeybinds;";
    };
};