params ["_unit"];

// Exit for non-local, just in case.
if (!local _unit) exitWith {};

// Get the cost of the vehicle and which side it belongs to.
_cost = _unit getVariable ["bwi_motorpool_spawnCost", 0];
_side = _unit getVariable "bwi_motorpool_vehicleSide";

// Credit the funds.
[_cost, _side, true] call bwi_motorpool_fnc_creditFunds;

// Remove the vehicle.
deleteVehicle _unit;