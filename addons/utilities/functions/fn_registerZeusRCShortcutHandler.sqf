params ["_curator"];

/**
 * Registers an event handler for creating a shortcut
 * to open remote control in Zeus. When a unit is double 
 * clicked, and the modifier key (configured through CBA 
 * keybinds) is pressed, this handler will call BI's
 * module function and simulate the process of a module 
 * being placed in order to do so.
 */
_curator addEventHandler ["CuratorObjectDoubleClicked",
	{
		params["_curator", "_clickedObject"];

		// If the modifier key is down, continue.
		if ( bwi_utilities_rcsModifierPressed ) then {
			// Set bis_fnc_curatorObjectPlaced_mouseOver to the RC target.
			// When a module is placed over an object in Zeus, this variable is set to it.
			// We set it manually so we can call the BI module function directly.
			// Without this, the module code will not detect the unit underneath it.
			missionNamespace setVariable ["bis_fnc_curatorObjectPlaced_mouseOver", ["OBJECT", _clickedObject]]; 

			// Create a fake logic to pass to BIS_fnc_moduleRemoteControl.
			// We do this as it is literally the module code, so requires a logic.
			private _center = createCenter sideLogic;
			private _group = createGroup _center;
			private _logic = _group createUnit ["LOGIC", getPos _clickedObject, [], 0, "NONE"];
			_logic setName "rcModule";

			// Call BI's module function for starting RC.
			[_logic, [], true] call BIS_fnc_moduleRemoteControl;

			// Prevent the unit details dialog from opening.
			closeDialog 1;
			true
		};

		false
	}
];