params ["_unit"];

// Exit for non-local, just in case.
if (!local _unit) exitWith {};

// Repair the vehicle.
_unit setDamage [0, true];

// Inform the driver/pilot of the repair.
["Vehicle Repaired", "Zeus repaired this vehicle.", 2] call bwi_common_fnc_notification;