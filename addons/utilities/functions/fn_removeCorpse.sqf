params ["_corpse", ["_unit", objNull]];

/**
 * Wait 1 second before moving and/or removing the corpse.
 */
[
	{
		params ["_corpse", "_unit"];

		if (isNull _corpse) exitWith {};

		// Move corpse so it is hidden.
		_corpse setPos [0,0,-5000];
		
		// Check if unit or corpse was a Zeus.
		private _isZeus = false;
		if ( !isNull _unit ) then {
			_isZeus = _unit getVariable ["isZeus", false];
		} else {
			_isZeus = _corpse getVariable ["isZeus", false];
		};		

		// Delete the corpse if it wasn't a Zeus.
		if ( !_isZeus ) then {
			deleteVehicle _corpse;
		};
	}, 
	[_corpse, _unit], 1
] call CBA_fnc_WaitAndExecute;