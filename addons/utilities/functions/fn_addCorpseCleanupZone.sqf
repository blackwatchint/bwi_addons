params ["_object"];

// Run on the server only.
if ( isServer ) then {
	// Ensure array is initialized due to pushBack.
	if ( isNil "bwi_utilities_corpseCleanupZones" ) then {
		bwi_utilities_corpseCleanupZones = [];
	};

	// Add the zone to the array.
	bwi_utilities_corpseCleanupZones pushBackUnique _object;

	// Broadcast for all clients.
	publicVariable "bwi_utilities_corpseCleanupZones";
};