params ["_objects"];

if ( isServer ) then {
	{
		_x addCuratorEditableObjects [_objects, true];
	} foreach allCurators;
};