/**
 * Register an event handler for clearing the cargo of
 * vehicles spawned through the bwi_motorpool.
 */
["bwi_motorpool_spawnVehicle",
	{
		params ["_vehicle"];

		if ( local _vehicle ) then {
			[_vehicle] call bwi_utilities_fnc_clearVehicleCargo;
		};
	}
] call CBA_fnc_addEventHandlerArgs;