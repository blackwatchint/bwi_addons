params ["_unit"];

// Set the quick respawn flag on the unit.
_unit setVariable ["bwi_utilities_quickRespawn", true];

// Kill the unit.
_unit setDamage 1;