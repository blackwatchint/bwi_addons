params["_cargo"];

private _exemptClasses = bwi_utilities_ignoreCargoItems splitString ",";

private _cargoItems = _cargo select 0;
private _cargoCounts = _cargo select 1;
private _foundItems = [];

// If the specified item is in the exempt list, add it to an array of found items.
{
	if ( _x in _exemptClasses ) then {
		_foundItems pushBack [_x, _cargoCounts select _forEachIndex];
	};
} forEach _cargoItems;

// Return the result.
_foundItems