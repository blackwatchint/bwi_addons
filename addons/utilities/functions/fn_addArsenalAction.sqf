params ["_addToObject"];

// Add the action and broadcast.
[_addToObject, [
	"<t color='#d54500'>ACE Arsenal</t>",
	{ [_this select 0, _this select 1, true] call ace_arsenal_fnc_openBox; }, 
	nil, 1.6, false
]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP