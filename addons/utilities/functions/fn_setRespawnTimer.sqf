params["_unit", "_killer", "_respawn", "_respawnDelay"];

if (  !isDedicated && local player && player == _unit ) then {
	private _respawnTime = 0;

	// Check for isZeus or quickRespawn flags.
	private _isQuickRespawn = _unit getVariable ["bwi_utilities_quickRespawn", false];
	private _isZeus = _unit getVariable ["isZeus", false];


	// If Zeus or a quick respawn, set the timer to 5 seconds.
	if ( _isZeus || _isQuickRespawn ) then {
		_respawnTime = 5;

		// Clear the quickRespawn flag.
		_unit setVariable ["bwi_utilities_quickRespawn", false];
	// Otherwise process as a wave-based respawn.
	} else {
		// Fetch and round the server time.
		private _serverTime = CBA_missionTime;
		_serverTime = floor _serverTime;

		// Divide serverTime into chunks (waves) and get the last wave.
		// Note respawnDelay is the mission respawn time setting.
		private _timeSinceLastWave = _serverTime mod _respawnDelay;
		_timeSinceLastWave = floor _timeSinceLastWave;

		// Find when the next wave will occur.
		private _timeUntilNextWave = _respawnDelay - _timeSinceLastWave;

		// If the next wave is under a minute away, relegate to the following wave.
		if ( _timeUntilNextWave < 60 ) then {
			_respawnTime = _timeUntilNextWave + _respawnDelay;
		// Otherwise go with the next wave.
		} else {
			_respawnTime = _timeUntilNextWave;
		};
	};

	// Set the respawn time.
	setPlayerRespawnTime _respawnTime;
};