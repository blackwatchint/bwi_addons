params["_vehicle"];

// Only run when the setting is enabled.
if ( bwi_utilities_clearVehicleCargo ) then {
	if( ( _vehicle isKindOf "landVehicle" ) or ( _vehicle isKindOf "Air" ) or ( _vehicle isKindOf "Ship" ) ) then {
		// Get a list of exempted items found so they can be returned.
		private _returnWeaps = [getWeaponCargo _vehicle] call bwi_utilities_fnc_findExemptVehicleCargo;
		private _returnMags = [getMagazineCargo _vehicle] call bwi_utilities_fnc_findExemptVehicleCargo;
		private _returnItems = [getItemCargo _vehicle] call bwi_utilities_fnc_findExemptVehicleCargo;
		private _returnBacks = [getBackpackCargo _vehicle] call bwi_utilities_fnc_findExemptVehicleCargo;

		// Clear the cargo.
		clearWeaponCargoGlobal _vehicle;
		clearMagazineCargoGlobal _vehicle;
		clearItemCargoGlobal _vehicle;
		clearBackpackCargoGlobal _vehicle;

		// Return the exempt items to the inventory.
		if ( count _returnWeaps > 0 ) then {
			{
				_vehicle addWeaponCargoGlobal _x;
			} forEach _returnWeaps;
		};

		if ( count _returnMags > 0 ) then {
			{
				_vehicle addMagazineCargoGlobal _x;
			} forEach _returnMags;
		};

		if ( count _returnItems > 0 ) then {
			{
				_vehicle addItemCargoGlobal _x;
			} forEach _returnItems;
		};

		if ( count _returnBacks > 0 ) then {
			{
				_vehicle addBackpackCargoGlobal _x;
			} forEach _returnBacks;
		};
	};
};