params ["_addToObject", "_position", "_direction", "_name", ["_script", nil]];

// Add the action and broadcast.
[_addToObject, [
	format ["Teleport to %1", _name],
	{
		(_this select 3) params ["_teleportArguments", ["_script", nil]];
		_teleportArguments call bwi_common_fnc_teleportToPosition;
		if (!isNil "_script") then
		{
			[player, _teleportArguments] call _script;
		};		
	}, 
	[[_position,_direction,_name],_script], 1.5, false
]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP