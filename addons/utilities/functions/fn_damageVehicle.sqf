params ["_unit", "_module", "_damage"];

// Exit for non-local, just in case.
if (!local _unit) exitWith {};

// Get the unit's hitpoint configs.
private _hitpointCfgs = [(configFile >> "CfgVehicles" >> typeOf _unit >> "HitPoints"), 0, true] call BIS_fnc_returnChildren;

// Iterate over the hitpoint configs, looking for the target hitpoints.
{
    _hitpoint = configName _x;

    switch (_module) do {
        case "Engine": {
            if ( _hitpoint in ["HitEngine","HitEngine2","HitEngine3"] ) then {
                _unit setHitPointDamage [_hitpoint, _damage, true];
            };
        };

        case "Fuel": {
            if ( _hitpoint in ["HitFuel","HitFuel2","HitFuelL","HitFuelR"] ) then {
                _unit setHitPointDamage [_hitpoint, _damage, true];
            };
        };  

        case "Main_Rotor": {
            if ( _hitpoint in ["HitHRotor"] ) then {
                _unit setHitPointDamage [_hitpoint, _damage, true];
            };
        };  

        case "Tail_Rotor": {
            if ( _hitpoint in ["HitVRotor"] ) then {
                _unit setHitPointDamage [_hitpoint, _damage, true];
            };
        };  

        case "Avionics": {
            if ( _hitpoint in ["HitAvionics"] ) then {
                _unit setHitPointDamage [_hitpoint, _damage, true];
            };
        };  
    };


} forEach _hitpointCfgs;

// Warn the driver/pilot of the hit.
["Vehicle Damaged", "Zeus damaged this vehicle.", 1] call bwi_common_fnc_notification;