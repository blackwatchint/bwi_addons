params ["_curator"];

/**
 * Registers an event handler for clearing a the cargo 
 * (inventory) of vehicles placed by Zeus.
 */
_curator addEventHandler ["CuratorObjectPlaced",
	{
		params["_curator", "_placedObject"];

		[_placedObject] call bwi_utilities_fnc_clearVehicleCargo;
	}
];