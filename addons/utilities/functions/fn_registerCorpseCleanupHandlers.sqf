// Register server-only handlers.
if ( isServer ) then {
	/**
	 * Removes the player corpse on disconnect if it is within
	 * range of a cleanup location.
	 */
	addMissionEventHandler ["HandleDisconnect",
		{ 
			params ["_corpse"];
			{
				// Note we use exitWith so the loop exits.
				if ( !isNull _x && _corpse distance _x < bwi_utilities_corpseCleanupRadius ) exitWith {
					[_corpse] call bwi_utilities_fnc_removeCorpse;
				};
			} forEach bwi_utilities_corpseCleanupZones;
		}
	];
};


// Register client-only handlers.
if ( hasInterface ) then {
	/**
	 * Removes the player corpse on respawn if it is within
	 * range of a cleanup location.
	 */
	player addEventHandler ["Respawn",
		{
			params ["_unit", "_corpse"];
			{
				// Note we use exitWith so the loop exits.
				if ( !isNull _x && _corpse distance _x < bwi_utilities_corpseCleanupRadius ) exitWith {
					[_corpse, _unit] call bwi_utilities_fnc_removeCorpse;
				};
			} forEach bwi_utilities_corpseCleanupZones;
		}
	];
};