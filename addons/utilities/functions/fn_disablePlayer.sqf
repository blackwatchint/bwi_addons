// Quit on server and HCs.
if ( !hasInterface ) exitWith {};

// Disable simulation and damage.
player enableSimulation false;
player allowDamage false; 

// Remove all magazines.
{player removeMagazine _x} forEach magazines player;

// Empty the currently loaded magazines.
player setAmmo [primaryWeapon player, 0];
player setAmmo [handgunWeapon player, 0];
player setAmmo [secondaryWeapon player, 0];

// Player is in a vehicle, handle vehicle scenarios.
if ( vehicle player != player ) then {
    (vehicle player) setVehicleAmmo 0;
    (vehicle player) enableSimulation false;
    (vehicle player) allowDamage false;

    // Remove all vehicle weapons.
    removeAllWeapons vehicle player;

    // Clear turret magazines. // TODO - Cleanup if not necessary.
    /*{
        if ( vehicle player turretUnit _x == player ) then {
            private _turret = _x;

            {
                vehicle player removeMagazinesTurret [_x, _turret];
            } forEach (vehicle player magazinesTurret _x);
        };
    } forEach [[-1]] + allTurrets (vehicle player); // Include -1 (driver) manually.*/
};