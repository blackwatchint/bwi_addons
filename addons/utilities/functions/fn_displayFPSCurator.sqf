params ["_fps", "_unit"];

// Get the responder's name.
private _name = "Server";
if (!isNull _unit) then {
	_name = name _unit;
};

// Output the message to curator.
[objNull, format["%1 FPS: %2", _name, _fps]] call bis_fnc_showCuratorFeedbackMessage;