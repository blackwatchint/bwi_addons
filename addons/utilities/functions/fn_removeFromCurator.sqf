params ["_objects"];

if ( isServer ) then {
	{
		_x removeCuratorEditableObjects [_objects, true];
	} foreach allCurators;
};