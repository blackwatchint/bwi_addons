#include "\bwi_common\modules\module_header.sqf"

// Teleport the Zeus player to the module position.
private _position = getPos _logic;
player setPos _position;

// Display Zeus message and delete module.
_curatorMsg = format ["Zeus teleported to %1,%2", round (_position select 0), round (_position select 1)];
_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"