#include "\bwi_common\modules\module_header.sqf"

// Find the objects to remove.
private _objectsToRemove = [];
_objectsToRemove = _objectsToRemove + allMissionObjects "Static";
_objectsToRemove = _objectsToRemove + allMissionObjects "Thing";

// Remote the remove objects.
[_objectsToRemove] remoteExec ["bwi_utilities_fnc_removeFromCurator", 2]; // Server

// Display Zeus message and delete module.
_curatorMsg = format["Removed %1 objects from Zeus", count _objectsToRemove];
_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"