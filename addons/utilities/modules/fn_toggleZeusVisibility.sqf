#include "\bwi_common\modules\module_header.sqf"

private _unit = player;

// Hidden, so make visible, vulnerable.
if ( isObjectHidden _unit ) then {
	[_unit, false] remoteExec ["hideObjectGlobal", 2]; //Server, JIP safe

	_unit setCaptive false;
	_unit allowDamage true;

	_curatorMsg = "Zeus visible";
// Visible, so make hidden, invulnerable.
} else {
	[_unit, true] remoteExec ["hideObjectGlobal", 2]; //Server, JIP safe
	
	_unit setCaptive true;
	_unit allowDamage false;

	_curatorMsg = "Zeus invisible";
};

_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"