#include "\bwi_common\modules\module_header.sqf"

private _destinationName = _logic getVariable "DestinationName";
private _destinationDir = _logic getVariable "LookDirection";
private _destinationPos = getPosATL _logic;
private _executedScript = _logic getVariable "ExecutedScript";
private _scriptIsFile = _logic getVariable "ScriptIsFile";
if (_scriptIsFile) then
{
	_executedScript = preprocessFile _executedScript;
};
_executedScript = compileFinal _executedScript;

// Run on the server only.
if ( isServer  ) then {
	if ( count _units > 0 ) then {
		// Add teleport action to all sync'd units.
		{
			if ( !(_x isKindOf "Man") ) then  {
				[_x, _destinationPos, _destinationDir, _destinationName, _executedScript] call bwi_utilities_fnc_addTeleportAction;
			};
		} forEach _units;
	};
};

#include "\bwi_common\modules\module_footer.sqf"