#include "\bwi_common\modules\module_header.sqf"

// Find the objects to add.
private _objectsToAdd = allMissionObjects "AllVehicles";

// Remote the add objects.
[_objectsToAdd] remoteExec ["bwi_utilities_fnc_addToCurator", 2]; // Server

// Display Zeus message and delete module.
_curatorMsg = format["Added %1 vehicles to Zeus", count _objectsToAdd];
_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"