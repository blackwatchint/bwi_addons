#include "\bwi_common\modules\module_header.sqf"

// Find the objects to remove.
private _objectsToRemove = allMissionObjects "AllVehicles";

// Remote the add objects.
[_objectsToRemove] remoteExec ["bwi_utilities_fnc_removeFromCurator", 2]; // Server

// Display Zeus message and delete module.
_curatorMsg = format["Removed %1 vehicles from Zeus", count _objectsToRemove];
_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"