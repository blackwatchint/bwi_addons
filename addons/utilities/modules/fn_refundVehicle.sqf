#include "\bwi_common\modules\module_header.sqf"

if ( count _units > 0 ) then {
    {
        // Module works with all vehicles.
        if ( ( _x isKindOf "landVehicle" ) || ( _x isKindOf "Air" ) || ( _x isKindOf "Ship" ) ) then {
			
			_playerCrew = ({ isPlayer _x } count (crew _x));
			if ( _playerCrew == 0 ) then {
				// Call refundAsset on target unit.
				[_x] remoteExecCall ["bwi_utilities_fnc_refundAsset", _x, false]; // Target, no-JIP.
            
				_curatorMsg = "Refunding vehicle...";
			} else {
				_curatorMsg = "Cannot refund vehicle with players inside";
			}
        } else {
            _curatorMsg = "Can only be used on vehicles";
        }
    } forEach _units;
} else {
    _curatorMsg = "No object found";
};

_deleteOnExit = true; // Delete if Zeus

#include "\bwi_common\modules\module_footer.sqf"