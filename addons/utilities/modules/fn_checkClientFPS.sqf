#include "\bwi_common\modules\module_header.sqf"

private _thisClientID = clientOwner;

if (count _units > 0) then {
	// Can only check a single unit.
	private _unit = _units select 0;

	if ( isPlayer _unit ) then {
		_curatorMsg = format ["Checking FPS for %1...", name _unit];

		// RemoteExec the FPS check on the target unit. Pass it this client ID so it knows where to return the result.
		[_thisClientID, _unit] remoteExecCall ["bwi_utilities_fnc_requestFPSLocal", _unit, false]; // Specific player, no-JIP
	} else {
		_curatorMsg = "Target is not a player";
	};
} else {
	_curatorMsg = "No object found";
};

_deleteOnExit = true; // Delete if Zeus

#include "\bwi_common\modules\module_footer.sqf"