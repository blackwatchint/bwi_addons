#include "\bwi_common\modules\module_header.sqf"

// Run on zeus or server only.
if ( _isCurator || isServer ) then {
	if ( count _units > 0 ) then {
		// Add the quick respawn option to all sync'd objects.
		{
			if ( !(_x isKindOf "Man") ) then {
				[_x] call bwi_utilities_fnc_addQuickRespawnAction;
				_curatorMsg = "Added Quick Respawn";
			} else {
				_curatorMsg = "Can only be added to objects";
			};
		} forEach _units;
	} else {
		_curatorMsg = "No object found";
	};
};

_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"