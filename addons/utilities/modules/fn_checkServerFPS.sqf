#include "\bwi_common\modules\module_header.sqf"

private _thisClientID = clientOwner;

// RemoteExec the FPS check on the server. Pass it this client ID so it knows where to return the result.
[_thisClientID, objNull] remoteExecCall ["bwi_utilities_fnc_requestFPSLocal", 2, false]; // Server, no-JIP

// Display Zeus message and delete module.
_curatorMsg = "Checking FPS for the server...";
_deleteOnExit = true;

#include "\bwi_common\modules\module_footer.sqf"