#include "\bwi_common\modules\module_header.sqf"

// Run on Zeus, or the server only.
if ( _isCurator || isServer  ) then {
	if ( count _units > 0 ) then {
		
		private _linkedGroups = [];
		// Toggle hold status on sync'd groups.
		{
			// Check if the object is in a group, and check that we haven't already applied the hold module to this group.
			if ( !isNull (group _x) && !((group _x) in _linkedGroups) ) then {
				
				// Variables for hold logic, used later to check if we need to disable VCOM.
				private _holdMode = 0;
				private _stanceMode = "AUTO";
				private _releaseMode = 0;
				private _enemyMode = "NONE";
				
				// Loop through units in group.
				{
					// Make sure not to apply logic to any players (in case of an Player-AI mixed group)
					if ( _x isKindOf "Man" && !(isPlayer _x) ) then {
						
						// If Zeus and unit isn't local, try to return locality before executing.
						if ( _isCurator && !(local _x) ) then {
							[_x, player] call bwi_common_fnc_transferUnitLocality;
						};
						
						// Set the HC blacklist variable if it isn't already set.
						private _isBlacklisted = _x getVariable ["acex_headless_blacklist", false];
						if ( !_isBlacklisted ) then {
							_x setVariable ["acex_headless_blacklist", true, true]; 
						};
						
						// Server uses module logic variables.
						if ( !_isCurator && isServer ) then {
							_holdMode = _logic getVariable ["HoldMode", 0];
							_stanceMode = _logic getVariable ["StanceMode", "AUTO"];
							_releaseMode = _logic getVariable ["ReleaseMode", 0];
							_enemyMode = _logic getVariable ["EnemyMode", "NONE"];

						// Zeus uses auto-cycling.
						} else {
							_holdMode = (group _x) getVariable ["bwi_utilities_currentForceHoldMode", 0];

							// Cycle hold mode.
							switch ( _holdMode ) do {
								case 2: { _holdMode = 0; };
								case 1: { _holdMode = 2; };
								case 0: { _holdMode = 1; };
								default { _holdMode = 0; };
							};
						};
						
						// Hold mode has changed.
						if ( _holdMode != (group _x) getVariable ["bwi_utilities_currentForceHoldMode", 0] ) then {
							switch ( _holdMode ) do {
								// Auto - Enable move and path.
								case 0: {
									[_x, "MOVE"] remoteExec ["enableAI", _x]; //Where object local, no JIP
									[_x, "PATH"] remoteExec ["enableAI", _x]; //Where object local, no JIP
									_curatorMsg = "Group no longer holding";
								};
								// Position - Enable move, disable path.
								case 1: {
									[_x, "MOVE"] remoteExec ["enableAI", _x]; //Where object local, no JIP
									[_x, "PATH"] remoteExec ["disableAI", _x]; //Where object local, no JIP
									_curatorMsg = "Group holding position";
								};
								// Direction - Enable path, disable move.
								case 2: {
									[_x, "MOVE"] remoteExec ["disableAI", _x]; //Where object local, no JIP
									[_x, "PATH"] remoteExec ["enableAI", _x]; //Where object local, no JIP
									_curatorMsg = "Group holding direction";
								};
							};
						};
						
						// Stance has changed.
						if ( _stanceMode != unitPos _x  ) then {
							[_x, _stanceMode] remoteExec ["setUnitPos", _x]; //Where object local, no JIP
						};
					};
				} foreach units (group _x);
				
				// Update hold mode for group.
				(group _x) setVariable ["bwi_utilities_currentForceHoldMode", _holdMode, true];
				
				// Need to disable VCOM for group to hold properly, and enable it when group is no longer holding.
				switch ( _holdMode ) do {
					// No longer holding, enable VCOM.
					case 0: {
						(group _x) setVariable ["Vcm_Disable", false, true];
					};
					// Holding, disable VCOM.
					case 1;
					case 2: {
						(group _x) setVariable ["Vcm_Disable", true, true];
					};
				};
				
				// Release mode and enemy mode defined.
				if ( _releaseMode != 0 && _enemyMode != "NONE") then {
					// Create a trigger to release the unit.
					private _trigger = createTrigger ["EmptyDetector", (leader (group _x)), false];
					_trigger setVariable ["bwi_utilities_linkedGroup", (group _x)];
					_trigger setTriggerArea [_releaseMode, _releaseMode, 0, false];
					_trigger setTriggerActivation [_enemyMode, "PRESENT", false];
					_trigger setTriggerStatements ["this", "
						private _group = thisTrigger getVariable 'bwi_utilities_linkedGroup';
						{
							[_x, 'MOVE'] remoteExec ['enableAI', _x];
							[_x, 'PATH'] remoteExec ['enableAI', _x];
							[_x, 'AUTO'] remoteExec ['setUnitPos', _x]
						} foreach units _group;
						_group setVariable ['Vcm_Disable', false, true];
					", ""];
				};
				
				/* Add this group to the appliedGroups array to prevent applying the above logic to
				the same group twice, if module is synced to more than one member of the same group. */
				_linkedGroups pushBack (group _x);
				
			} else {
				_curatorMsg = "Can only be used on groups";
			};
		} forEach _units;
	} else {
		_curatorMsg = "No object found";
	};
};

_deleteOnExit = true; // Delete if Zeus

#include "\bwi_common\modules\module_footer.sqf"