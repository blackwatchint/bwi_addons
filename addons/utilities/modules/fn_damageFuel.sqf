#include "\bwi_common\modules\module_header.sqf"

if ( count _units > 0 ) then {
    {
        // Module works with all vehicles.
        if ( ( _x isKindOf "landVehicle" ) || ( _x isKindOf "Air" ) || ( _x isKindOf "Ship" ) ) then {
            // Call damageVehicle on target unit.
            [_x, "Fuel", 0.65] remoteExecCall ["bwi_utilities_fnc_damageVehicle", _x, false]; // Target, no-JIP.
            
            _curatorMsg = "Damaging fuel...";
        } else {
            _curatorMsg = "Can only be used on vehicles";
        }
    } forEach _units;
} else {
    _curatorMsg = "No object found";
};

_deleteOnExit = true; // Delete if Zeus

#include "\bwi_common\modules\module_footer.sqf"