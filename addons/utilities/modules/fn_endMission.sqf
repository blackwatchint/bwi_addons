params ["_logic","_units","_activated"];

// Call the default end mission module.
// Note - must go first so dialog can be opened!
[_logic,_units,_activated] call BIS_fnc_moduleEndMission;

//Exit if logic null (might be if cancelled).
if ( isNull _logic ) exitwith {};

// Disable player simulation.
[] remoteExec ["bwi_utilities_fnc_disablePlayer", 0, true];