#include "\bwi_common\modules\module_header.sqf"

if ( count _units > 0 ) then {
    {
        // Module only works with aircraft.
        if (  (_x isKindOf "Helicopter") ) then {
            // Call damageVehicle on target unit.
            [_x, "Main_Rotor", 1.0] remoteExecCall ["bwi_utilities_fnc_damageVehicle", _x, false]; // Target, no-JIP.
            
            _curatorMsg = "Damaging main rotor...";
        } else {
            _curatorMsg = "Can only be used on helicopters";
        }
    } forEach _units;
} else {
    _curatorMsg = "No object found";
};

_deleteOnExit = true; // Delete if Zeus

#include "\bwi_common\modules\module_footer.sqf"