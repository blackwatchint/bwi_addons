#include "\bwi_common\modules\module_header.sqf"

// Only run on the server or a curator.
if ( isServer || _isCurator ) then {
	if ( count _units > 0 ) then {
		// Set the variable on all sync'd objects.
		{
			// Check if the unit has a "crew". Returns true for any unit-based objects (including logics).
			if ( count (crew _x) > 0 && !(_x isKindOf "Logic") ) then {
				// If unit isn't local to Zeus, try to regain control first.
				if ( _isCurator && !(local _x) ) then {
					[_x, player] call bwi_common_fnc_transferUnitLocality;
				};

				// Attempt to set the blacklist variable.
				// ACEX checks within groups and vehicles, so don't need to "propagate" further.
				_x setVariable ["acex_headless_blacklist", true, true]; 
				_curatorMsg = "Unit flagged as local-only";
			} else {
				_curatorMsg = "Can only be used on units";
			};
		} forEach _units;
	} else {
		_curatorMsg = "No object found";
	};
};

_deleteOnExit = true; // Delete if Zeus

#include "\bwi_common\modules\module_footer.sqf"