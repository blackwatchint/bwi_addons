class Logic;
class Module_F: Logic
{
	class ArgumentsBaseUnits
	{
		class Units;
	};
	class ModuleDescription;
};


/**
 * Adds all static objects to Zeus when dropped.
 */
class BWI_Utilities_ModuleAddObjectsToCurator: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Add Objects to Zeus";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_addObjectsToCurator";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Add all objects to Zeus.";
		sync[] = {};
	};
};


/**
 * Adds all vehicles and units to Zeus when dropped.
 */
class BWI_Utilities_ModuleAddVehiclesToCurator: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Add Vehicles to Zeus";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_addVehiclesToCurator";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Add all vehicles to Zeus.";
		sync[] = {};
	};
};


/**
 * Removes all static objects from Zeus when dropped.
 */
class BWI_Utilities_ModuleRemoveObjectsFromCurator: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Remove Objects from Zeus";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_removeObjectsFromCurator";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Remove all objects from Zeus.";
		sync[] = {};
	};
};


/**
 * Removes all vehicles and units from Zeus when dropped.
 */
class BWI_Utilities_ModuleRemoveVehiclesFromCurator: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Remove Vehicles from Zeus";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_removeVehiclesFromCurator";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Remove all vehicles from Zeus.";
		sync[] = {};
	};
};


/**
 * Hides the Zeus player when dropped.
 */
class BWI_Utilities_ModuleToggleZeusVisibility: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Zeus Visibility";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_toggleZeusVisibility";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Toggles the current Zeus' visibility.";
		sync[] = {};
	};
};


/**
 * Teleports the Zeus to the given position when dropped.
 */
class BWI_Utilities_ModuleTeleportZeus: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Zeus Teleport";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_teleportZeus";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Teleports the current Zeus.";
		sync[] = {};
	};
};


/**
 * Adds a quick respawn option to the object this module
 * is synced to. Also sets the respawn timer to 5 seconds
 * for this respawn only. Requires the BWI respawn
 * template to correctly function.
 */
class BWI_Utilities_ModuleAddQuickRespawn: Module_F
{
	scope = 2;
	scopeCurator = 1;
	displayName = "Add Quick Respawn";
	icon = "\bwi_utilities\data\icon_respawn_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_addQuickRespawn";
	functionPriority = 1;
	isGlobal = 0; // Server-only
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class ModuleDescription: ModuleDescription
	{
		description = "Adds a quick respawn option to the object.";
		sync[] = {};
	};
};


/**
 * Adds an (ACE) Arsenal to all synchronised objects.
 */
class BWI_Utilities_ModuleAddArsenal: Module_F
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Add Arsenal";
	icon = "\bwi_utilities\data\icon_va_w.paa";
	category = "BWI_Arsenal";
	function = "bwi_utilities_module_fnc_addArsenal";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Adds an arsenal to the object.";
		sync[] = {};
	};
};


/**
 * Prevents transfer of the synchronised unit to the 
 * ACEX headless client.
 */
class BWI_Utilities_ModulePreventHCTransfer: Module_F
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Prevent HC Transfer";
	icon = "\bwi_utilities\data\icon_hc_w.paa";
	category = "BWI_Headless";
	function = "bwi_utilities_module_fnc_preventHCTransfer";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Prevents synchronised units from being transferred to the ACEX headless client.";
		sync[] = {};
	};
};


/**
 * Forces synchronised units to hold a specific position by
 * disabling the relevant AI behaviour. In editor mode the
 * behaviour can be configured. In Zeus mode it cycles between 
 * three settings.
 */
class BWI_Utilities_ModuleAIForceHoldPosition: Module_F
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Hold Position";
	icon = "\bwi_utilities\data\icon_ai_w.paa";
	category = "BWI_AI";
	function = "bwi_utilities_module_fnc_aiForceHoldPosition";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class Arguments: ArgumentsBaseUnits
	{
		class HoldMode {
			displayName = "Hold";
			description = "Set the hold mode. Auto allows free movement. Position allows rotation only. Direction allows minimal rotation.";
			typeName = "NUMBER";
			class Values {
				class AUTO		{ name = "Auto"; 		value = 0; default = 1; };
				class POSITION	{ name = "Position"; 	value = 1; };
				class DIRECTION	{ name = "Direction";	value = 2; };
			};
		};

		class StanceMode {
			displayName = "Stance";
			description = "Set the unit stance. Auto allows automatic stance selection.";
			typeName = "STRING";
			class Values {
				class AUTO		{ name = "Auto";	value = "AUTO"; default = 1; };
				class STAND		{ name = "Stand"; 	value = "UP"; };
				class CROUCH	{ name = "Crouch"; 	value = "MIDDLE"; };
				class PRONE		{ name = "Prone"; 	value = "DOWN"; };
			};
		};

		class ReleaseMode {
			displayName = "Released At";
			description = "When enemy are within this range, movement is allowed again.";
			typeName = "NUMBER";
			class Values {
				class NONE	{ name = "Disabled";	value = 0; default = 1; };
				class 5M	{ name = "5m";			value = 5; };
				class 10M	{ name = "10m"; 		value = 10; };
				class 25M	{ name = "25m"; 		value = 25; };
				class 50M	{ name = "50m"; 		value = 50; };
				class 100M	{ name = "100m"; 		value = 100; };
				class 250M	{ name = "250m"; 		value = 250; };
			};
		};

		class EnemyMode {
			displayName = "Released By";
			description = "Which side can release the unit.";
			typeName = "STRING";
			class Values {
				class NONE	{ name = "None";		value = "NONE"; default = 1; };
				class WEST	{ name = "BLUFOR";		value = "WEST"; };
				class EAST	{ name = "OPFOR";		value = "EAST"; };
				class GUER	{ name = "INDEP";		value = "GUER" };
				class CIV	{ name = "CIV"; 		value = "CIV"; };
				class ANY	{ name = "ANY"; 		value = "ANY"; };
			};
		};
	};

	class ModuleDescription: ModuleDescription
	{
		description = "Forces synchronised units to hold a fixed position. Can be released via Zeus or trigger.";
		sync[] = {};
	};
};


/* 
 * Adds a teleport action to all synced objects. The
 * teleport action teleports player to the location
 * the module is placed at.
 */
class BWI_Utilities_ModuleAddTeleportTo: Module_F
{
	scope = 2;
	scopeCurator = 1;
	displayName = "Add Teleporter";
	icon = "\bwi_utilities\data\icon_deploy_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_addTeleportTo";
	functionPriority = 1;
	isGlobal = 0; // Server-only
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	
	class Arguments: ArgumentsBaseUnits
	{
		class DestinationName {
			displayName = "Destination name";
			description = "Defines the name of the destination for the teleporter. Will change the teleport action title accordingly.";
			defaultValue = "Somewhere";
		};
		class LookDirection {
			displayName = "Look direction";
			description = "Defines the direction the player will be looking in after being teleported. Accepted intput is in degrees from -360 to 360.";
			typeName = "NUMBER";
		};
		class ExecutedScript {
			displayName = "Script to execute";
			description = "Defines a script to execute on teleport action. Script is executed locally on the computer that started the action.";
		};
		class ScriptIsFile {
			displayName = "Read script as file path";
			description = "Set this to Enabled if the script to execute line is a file path and set to Disabled if it is a script of itself.";
			typeName = "BOOL";
			defaultValue = "false";
		};
	};

	class ModuleDescription: ModuleDescription
	{
		description = "Add a teleport action to all synced object. Teleport destination is module location.";
		sync[] = {};
	};
};


/**
 * Overwrite the default "End Scenario" module with a custom
 * function that disables player and vehicle movement and 
 * removes all ammunition before the mission is ended.
 */
class ModuleEndMission_F : Module_F
{
    function = "bwi_utilities_module_fnc_endMission";
};


/**
 * Checks the FPS of a client and displays the result as a
 * curator message.
 */
class BWI_Utilities_ModuleCheckClientFPS: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Check Client FPS";
	icon = "\bwi_utilities\data\icon_perf_w.paa";
	category = "BWI_Performance";
	function = "bwi_utilities_module_fnc_checkClientFPS";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Checks FPS of a player.";
		sync[] = {};
	};
};


/**
 * Checks the FPS of the server and displays the result
 * as a curator message.
 */
class BWI_Utilities_ModuleCheckServerFPS: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Check Server FPS";
	icon = "\bwi_utilities\data\icon_perf_w.paa";
	category = "BWI_Performance";
	function = "bwi_utilities_module_fnc_checkServerFPS";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Checks FPS of the server.";
		sync[] = {};
	};
};


/**
 * Damages a vehicle's engine on-demand.
 */
class BWI_Utilities_ModuleDamageEngine: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Damage Engine";
	icon = "\bwi_utilities\data\icon_spanner_w.paa";
	category = "BWI_Damage";
	function = "bwi_utilities_module_fnc_damageEngine";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Damages a vehicle's engine.";
		sync[] = {};
	};
};


/**
 * Damages a vehicle's fuel tank on-demand.
 */
class BWI_Utilities_ModuleDamageFuel: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Damage Fuel Tank";
	icon = "\bwi_utilities\data\icon_spanner_w.paa";
	category = "BWI_Damage";
	function = "bwi_utilities_module_fnc_damageFuel";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Damages a vehicle's fuel tank.";
		sync[] = {};
	};
};


/**
 * Damages a helicopter's main rotor on-demand.
 */
class BWI_Utilities_ModuleDamageMainRotor: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Damage Main Rotor";
	icon = "\bwi_utilities\data\icon_spanner_w.paa";
	category = "BWI_Damage";
	function = "bwi_utilities_module_fnc_damageMainRotor";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Damages a helicopter's main rotor.";
		sync[] = {};
	};
};


/**
 * Damages a helicopter's tail rotor on-demand.
 */
class BWI_Utilities_ModuleDamageTailRotor: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Damage Tail Rotor";
	icon = "\bwi_utilities\data\icon_spanner_w.paa";
	category = "BWI_Damage";
	function = "bwi_utilities_module_fnc_damageTailRotor";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Damages a helicopter's tail rotor.";
		sync[] = {};
	};
};


/**
 * Damages an aircraft's avionics on-demand.
 */
class BWI_Utilities_ModuleDamageAvionics: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Damage Avionics";
	icon = "\bwi_utilities\data\icon_spanner_w.paa";
	category = "BWI_Damage";
	function = "bwi_utilities_module_fnc_damageAvionics";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Damages an aircraft's avionics.";
		sync[] = {};
	};
};


/**
 * Fully repairs a vehicle on-demand.
 */
class BWI_Utilities_ModuleFullRepair: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Full Repair";
	icon = "\bwi_utilities\data\icon_spanner_w.paa";
	category = "BWI_Damage";
	function = "bwi_utilities_module_fnc_repairDamage";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Repairs all damage to a vehicle.";
		sync[] = {};
	};
};


/**
 * Refunds the vehicle's motorpool cost when dropped on a vehicle.
 */
class BWI_Utilities_ModuleRefundVehicle: Module_F
{
	scope = 1;
	scopeCurator = 2;
	displayName = "Refund Vehicle";
	icon = "\bwi_utilities\data\icon_uze_w.paa";
	category = "BWI_Utilities";
	function = "bwi_utilities_module_fnc_refundVehicle";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver to function

	class ModuleDescription: ModuleDescription
	{
		description = "Refunds a vehicle.";
		sync[] = {};
	};
};