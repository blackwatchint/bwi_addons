class Functions
{
	mode = 2;
	jip = 1;
	class bwi_utilities_fnc_addToCurator 	  { allowedTargets = 2; };
	class bwi_utilities_fnc_removeFromCurator { allowedTargets = 2; };
	class bwi_utilities_fnc_disablePlayer 	  { allowedTargets = 0; };
	class bwi_utilities_fnc_requestFPSLocal   { allowedTargets = 0; jip = 0; };
	class bwi_utilities_fnc_displayFPSCurator { allowedTargets = 0; jip = 0; };
	class bwi_utilities_fnc_damageVehicle	  { allowedTargets = 0; jip = 0; };
	class bwi_utilities_fnc_repairVehicle	  { allowedTargets = 0; jip = 0; };
	class bwi_utilities_fnc_refundVehicle	  { allowedTargets = 0; jip = 0; };
};

class Commands
{
	mode = 2;
	jip = 1;

	class addAction 	   { allowedTargets = 0; };
	class hideObjectGlobal { allowedTargets = 2; };
	class setGroupOwner    { allowedTargets = 2; };
	class enableAI 		   { allowedTargets = 0; };
	class disableAI 	   { allowedTargets = 0; };
	class setUnitPos 	   { allowedTargets = 0; };
};