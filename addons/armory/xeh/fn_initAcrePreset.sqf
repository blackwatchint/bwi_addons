/**
 * List of ACRE radio classes to receive configured presets.
 * Each item is an array consisting of the classname, the 
 * first channel ID and the last channel ID.
 */
private _presetRadios = [
	["ACRE_PRC77", 1, 1],
	["ACRE_PRC148", 1, 16], 
	["ACRE_PRC152", 1, 16], 
	["ACRE_PRC117F", 1, 16],
	["ACRE_SEM52SL", 1, 12],
	["ACRE_SEM70", 11, 11]
];


/**
 * Wait until the bwi_armory radio settings have been set 
 * before attempting to preset the radios. This is to make 
 * sure the setting has been defined and broadcast, so it  
 * does not attempt to use the default values instead.
 */
[
	{
		(
			!isNil "bwi_armory_radioChannelNames" && 
			!isNil "bwi_armory_radioFrequencyStart" && 
			!isNil "bwi_armory_radioFrequencySpacing"
		)
	},
	{
		params ["_presetRadios"];

		// Get the channel and frequency settings.
		private _presetChannels = bwi_armory_radioChannelNames splitString ",";
		private _frequencyStart = bwi_armory_radioFrequencyStart;
		private _frequencyIncrement = bwi_armory_radioFrequencySpacing;

		// Iterate over the radios and apply the preset data.
		["Initializing ACRE Preset Data"] call bwi_common_fnc_log;
		{
			private _radioType = _x select 0;
			private _startChannel = _x select 1;
			private _maxChannels = _x select 2;
			private _isSingleChannel = ( _x select 1 == _x select 2 );
			private _hasChannelNames = _x select 3;

			// Create a new preset.
			[_radioType, "default", "bwi"] call acre_api_fnc_copyPreset;

			// Iterate through the channels, setting the name and frequency.
			{
				private _name = _x;
				private _channel = _startChannel + _forEachIndex;
				private _frequency = _frequencyStart + ( _frequencyIncrement * _forEachIndex );

				// Single channel radios exit the loop after the first channel.
				if ( _isSingleChannel && _forEachIndex > 0 ) exitWith {};

				// Don't preset more channels than available.
				if ( _forEachIndex == _maxChannels ) exitWith {};

				// Set the preset channel fields.
				[_radioType, "bwi", _channel, "name", _name] call acre_api_fnc_setPresetChannelField;		
				[_radioType, "bwi", _channel, "frequencyRX", _frequency] call acre_api_fnc_setPresetChannelField;
				[_radioType, "bwi", _channel, "frequencyTX", _frequency] call acre_api_fnc_setPresetChannelField;

				// Log the preset for diagnostic purposes.
				["%1 channel %3 (%2) set to %4 MHz", _radioType, _name, _channel, _frequency] call bwi_common_fnc_log;
			} forEach _presetChannels;

			// Save the new preset data.
			[_radioType, "bwi"] call acre_api_fnc_setPreset;
		} forEach _presetRadios;
	},
	[_presetRadios]
] call CBA_fnc_waitUntilAndExecute;