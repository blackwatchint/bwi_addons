// Define category strings for the CBA settings.
private _categoryLabelArmory = "BWI Armory";
private _categoryLabelFactions = "Factions";
private _categoryLabelLanguages = "Languages (editor only)";
private _categoryLabelRadio = "Radio (editor only)";
private _categoryLabelElements = "Elements";
private _categoryLabelAdvanced = "Testing & Training (advanced use only)";

// Load faction values. Already returned in CBA format of [values, labels, default].
private _factionValuesBlufor = [0] call bwi_armory_fnc_getFactionSettingValues;
private _factionValuesOpfor  = [1] call bwi_armory_fnc_getFactionSettingValues;
private _factionValuesIndep  = [2] call bwi_armory_fnc_getFactionSettingValues;
private _factionValuesCivil  = [3] call bwi_armory_fnc_getFactionSettingValues;


// Initialize CBA settings.
// Loadout settings.
[
    "bwi_armory_playerFactionBlufor",
    "LIST",
    ["BLUFOR Faction", "Select the loadout to apply to BLUFOR players."],
    [_categoryLabelArmory, _categoryLabelFactions],
    _factionValuesBlufor,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
		[west, _value] call bwi_armory_fnc_factionSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_armory_playerFactionOpfor",
    "LIST",
    ["OPFOR Faction", "Select the loadout to apply to OPFOR players."],
    [_categoryLabelArmory, _categoryLabelFactions],
    _factionValuesOpfor,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
		[east, _value] call bwi_armory_fnc_factionSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_armory_playerFactionIndep",
    "LIST",
    ["INDEP Faction", "Select the loadout to apply to INDEPENDENT players."],
    [_categoryLabelArmory, _categoryLabelFactions],
    _factionValuesIndep,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
		[resistance, _value] call bwi_armory_fnc_factionSettingChanged;
    }
] call CBA_settings_fnc_init;

[
    "bwi_armory_playerFactionCivil",
    "LIST",
    ["CIVIL Faction", "Select the loadout to apply to CIVILIAN players."],
    [_categoryLabelArmory, _categoryLabelFactions],
    _factionValuesCivil,
    true,
    {  
        // Ran locally on each client.
        params ["_value"];
		[civilian, _value] call bwi_armory_fnc_factionSettingChanged;
    }
] call CBA_settings_fnc_init;


// Language settings.
[
    "bwi_armory_babelLanguageBlufor",
    "EDITBOX",
    ["BLUFOR Language", "Name of the language applied for BLUFOR players."],
    [_categoryLabelArmory, _categoryLabelLanguages],
    "BLUFOR",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_armory_babelLanguageOpfor",
    "EDITBOX",
    ["OPFOR Language", "Name of the language applied for OPFOR players."],
    [_categoryLabelArmory, _categoryLabelLanguages],
    "OPFOR",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_armory_babelLanguageIndep",
    "EDITBOX",
    ["INDEP Language", "Name of the language applied for INDEPENDENT players."],
    [_categoryLabelArmory, _categoryLabelLanguages],
    "INDEPENDENT",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_armory_babelLanguageCivil",
    "EDITBOX",
    ["CIVIL Language", "Name of the language applied for CIVILIAN players."],
    [_categoryLabelArmory, _categoryLabelLanguages],
    "CIVILIAN",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;


// Radio settings.
[
    "bwi_armory_radioChannelNames",
    "EDITBOX",
    ["Preset Channel Names", "List of named preset radios channels to configure. Custom frequencies apply only to these channels."],
    [_categoryLabelArmory, _categoryLabelRadio],
    "",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_armory_radioFrequencyStart",
    "SLIDER",
    ["Preset Frequency Start", "The starting frequency for preset radio channels listed above."],
    [_categoryLabelArmory, _categoryLabelRadio],
    [46.00, 65.00, 60, 2],
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_armory_radioFrequencySpacing",
    "SLIDER",
    ["Preset Frequency Spacing", "The spacing between channel frequencys for preset radio channels listed above."],
    [_categoryLabelArmory, _categoryLabelRadio],
    [0.05, 1.00, 0.05, 2],
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;


// Element settings.
[
    "bwi_armory_unlockWeaponsElements",
    "CHECKBOX",
    ["Unlock Weapons Team Roles", "Unlocks the weapon team elements and roles."],
    [_categoryLabelArmory, _categoryLabelElements],
    false,
    true
] call CBA_settings_fnc_init;

[
    "bwi_armory_unlockReconElements",
    "CHECKBOX",
    ["Unlock Light Recon Roles", "Unlocks the light recon elements and roles."],
    [_categoryLabelArmory, _categoryLabelElements],
    false,
    true
] call CBA_settings_fnc_init;

[
    "bwi_armory_unlockSpecialElements",
    "CHECKBOX",
    ["Unlock Special Forces Roles", "Unlocks the special forces elements and roles."],
    [_categoryLabelArmory, _categoryLabelElements],
    false,
    true
] call CBA_settings_fnc_init;


// Advanced settings.
[
    "bwi_armory_unlockAllFactions",
    "CHECKBOX",
    ["Unlock All Factions", "Unlocks all factions for selection despite above settings. Leave unchecked."],
    [_categoryLabelArmory, _categoryLabelAdvanced],
    false,
    true
] call CBA_settings_fnc_init;