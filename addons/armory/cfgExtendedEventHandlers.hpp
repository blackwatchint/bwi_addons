class Extended_PreInit_EventHandlers {
    class bwi_armory_settings {
        init = "call bwi_armory_fnc_initCBASettings;";
    };
};
class Extended_PostInit_EventHandlers {
    class bwi_armory_acre {
        init = "call bwi_armory_fnc_initAcrePreset;";
    };
};