class CfgPatches
{
	class bwi_armory
	{
		requiredVersion = 1.0;
		authors[] = {"Fourjays", "Ifu", "Abel", "aDi", "RedBery"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
	    version = 5.1;
	    versionStr = "5.1.0";
	    versionAr[] = {5,1,0};
		requiredAddons[] = {
			"A3_Modules_F", 
			"A3_UI_F", 
			"cba_settings",
			"ace_advanced_fatigue",
			"ace_explosives",
			"ace_medical_treatment",
			"ace_repair",
			"bwi_common",
			"bwi_data"
		};
		units[] = {
			"BWI_Armory_ModuleAddArmory"
		};
	};
};

class Cfg3DEN {
	#include <cfgEden.hpp>
};

class CfgFactionClasses
{
	#include <cfgFactionClasses.hpp>
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgRemoteExec
{
	#include <cfgRemoteExec.hpp>
};

class CfgVehicles
{
	#include <cfgVehicles.hpp>
};

class CfgRespawnTemplates
{
	#include <cfgRespawnTemplates.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>

// Common GUI Definitions
#include <\common\gui\macros.hpp> // Absolute path

// GUI Definitions
#include <gui\armory.hpp>
#include <gui\accessories.hpp>