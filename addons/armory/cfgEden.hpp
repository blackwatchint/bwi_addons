class Object {
	class AttributeCategories {
		class ace_attributes {
			class Attributes {
				delete ace_advanced_fatigue_performanceFactor;
				delete ace_isEngineer;
				delete ace_isEOD;
				delete ace_isMedic;
			};
		};
	};
};