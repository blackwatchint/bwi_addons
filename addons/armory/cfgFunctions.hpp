class bwi_armory
{
	class functions
	{
		file = "\bwi_armory\functions";

		class addAction{};

		class addToUniform{};
		class addToVest{};
		class addToBackpack{};

		class addRandomUniform{};
		class addRandomVest{};
		class addRandomHeadgear{};
		class addRandomBackpack{};
		class addRandomWeapon{};
		class addRandomGoggles{};

		class initLoadout{};
		class respawnLoadout{};
		class processLoadout{};
		class processAccessories{};

		class setUnitVars{};
		class setUnitCallsign{};
		class setAcreBabel{};
		class setAcreRadios{};
		class registerBFTHandler{};
		class registerChannelHandler{};

		class getFactionSettingValues{};
		class factionSettingChanged{};

		class getFlagFromFaction{};
	};

	class gui_functions
	{
		file = "\bwi_armory\gui\functions";
		class openArmoryDialog{};
		class refreshSidesList{};
		class refreshFactionsTree{};
		class refreshFactionImage{};
		class refreshRolesTree{};
		class refreshSlotText{};
		class selectLoadoutGUI{};

		class openAccessoriesDialog{};
		class refreshAccessoriesTree{};
		class selectAccessoriesGUI{};
	};

	class xeh
	{
		file = "\bwi_armory\xeh";
		class initCBASettings{};
		class initAcrePreset{};
	};
};

class bwi_armory_module
{
	class modules
	{
		file = "\bwi_armory\modules";
		class addArmory{};
	};
};