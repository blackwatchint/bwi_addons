class bwi_reloadLoadoutOnRespawn
{
	// Call the respawnLoadout function. Same parameters as onPlayerRespawn.sqf file.
	onPlayerRespawn = "bwi_armory_fnc_respawnLoadout";

	// Disable respawn at start (see https://community.bistudio.com/wiki/Description.ext)
	respawnOnStart = -1;
};