params ["_unit", "_classes"];

// Guarantee that backpack seed is unique by passing a seed ID.
private _randomBackpack = [_classes, 4] call bwi_common_fnc_selectRandomOnce;

// Add the uniform and return the classname.
_unit addBackpack _randomBackpack;
_randomBackpack