params ["_side"];

private _faction = "";
private _flag = "";

// Find the player's faction.
switch ( _side ) do {
	case west:	 	 { _faction = bwi_armory_playerFactionBlufor;};
	case east: 		 { _faction = bwi_armory_playerFactionOpfor; };
	case resistance: { _faction = bwi_armory_playerFactionIndep; };
	case civilian:   { _faction = bwi_armory_playerFactionCivil; };
};

// Load the flag texture path from CfgPlayableFactions.
if ( _faction != "" ) then {
	private _factionClasses = _faction splitString "/";
	
	_flag = getText (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> "flag");
};

// Return the flag.
_flag