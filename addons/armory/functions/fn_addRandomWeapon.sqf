params ["_unit", "_classes"];

// Guarantee that weapon seed is unique by passing a seed ID.
private _randomWeapon = [_classes, 5] call bwi_common_fnc_selectRandomOnce;

// Add the uniform and return the classname.
_unit addWeapon _randomWeapon;
_randomWeapon