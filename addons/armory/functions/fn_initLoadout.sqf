// Quit on dedicated server and HCs.
if ( !hasInterface ) exitWith { };

/**
* Waits for a player to fully load before applying a loadout. 
* Will use the faction defined by CBA settings and the role 
* defined by the unit's slot ID string. The isAI parameter
* will skip the internal player != unit check so it can run
* on local AI units for loadout testing purposes.
*/ 
[
	{
		(
			!isNull player && player == player && time > 0 && 
			(
				!isNil "bwi_armory_playerFactionBlufor" && 
				!isNil "bwi_armory_playerFactionOpFor" && 
				!isNil "bwi_armory_playerFactionIndep" && 
				!isNil "bwi_armory_playerFactionCivil"
			)
		)
	},
	{
		params [["_unit", player], ["_isDebugAI", false]];

		// Quit if the unit is not the player and debug AI is false.
		if ( _unit != player && !_isDebugAI ) exitWith {};

		// Get the unit side.
		private _side = [side _unit] call bwi_common_fnc_sideToIndex;

		// Derive the unit faction from the settings.
		private _faction = "";
		switch ( side _unit ) do {
			case west: 		 { _faction = bwi_armory_playerFactionBlufor; };
			case east: 		 { _faction = bwi_armory_playerFactionOpfor; };
			case resistance: { _faction = bwi_armory_playerFactionIndep; };
			case civilian:   { _faction = bwi_armory_playerFactionCivil; };
		};

		// Read the role from the slot ID array.
		private _slotIdArr = [_unit] call bwi_common_fnc_getSlotIdArray;
		private _role = format ["%1/%2", _slotIdArr select 1, _slotIdArr select 3];

		// Quit if faction or role are still unspecified or badly formatted.
		if ( _faction == "" || _role == "" || count (_faction splitString "/") != 2 || count (_role splitString "/") != 2) exitWith {
			["Could not apply loadout for player %3. Bad faction (%1) or role (%2).", _faction, _role, _unit] call bwi_common_fnc_log;
		};

		// Process the loadout for the player.
		[_unit, _side, _faction, _role] call bwi_armory_fnc_processLoadout;
	},
	_this // Pass parameters through as-is.
] call CBA_fnc_waitUntilAndExecute;