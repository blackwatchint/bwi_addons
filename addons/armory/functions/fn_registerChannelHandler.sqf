params ["_unit", "_factionData", "_roleData"];

// Remove existing event handlers.
private _teleportEHID = _unit getVariable ["bwi_armory_teleportEHID", nil];
if ( !isNil "_teleportEHID" ) then {
	["bwi_staging_postTeleportToStaging", _teleportEHID] call CBA_fnc_removeEventHandler;
};

/**
 * Changes the available chat and marker channels according
 * to presence of BFT when going through the teleporter.
 * If teleporting back to the spawn area, all channels are
 * enabled regardless of BFT setting.
 */
_teleportEHID = ["bwi_staging_postTeleportToStaging",
	{
		params ["_unit", "_position", "_isReverseDirection"];

		private _isZeus = _unit getVariable ["isZeus", false];
		private _hasBFT = _unit getVariable ["bwi_armory_hasBFT", false];

		// Enable global chat for Zeus only.
		0 enableChannel _isZeus;

		// Enable side chat if BFT enabled or when at spawn.
		if ( _hasBFT || _isReverseDirection ) then {
			// Log the change.
			["Side channel state change to true for %1.", _unit] call bwi_common_fnc_log;

			// Set the channels.
			1 enableChannel true;
			2 enableChannel true;
			setCurrentChannel 1;
		} else {
			// Log the change.
			["Side channel state change to false for %1.", _unit] call bwi_common_fnc_log;
			
			// Set the channels.
			1 enableChannel false;
			2 enableChannel false;
			setCurrentChannel 3;
		};
	}
] call CBA_fnc_addEventHandler;

// Save the handler to a variable.
_unit setVariable ["bwi_armory_teleportEHID", _teleportEHID];