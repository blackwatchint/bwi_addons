params ["_unit", "_factionData", "_roleData"];

// Remove existing PFH.
private _bftPFHID = _unit getVariable ["bwi_armory_bftPFHID", nil];
if ( !isNil "_bftPFHID" ) then {
	[_bftPFHID] call CBA_fnc_removePerFrameHandler;
};


/**
 * Changes the group and unit BFT state according to the presence
 * of an ACE MicroDAGR. Checks each frame for a change in the
 * BFT state and updates the appropriate ACE variables accordingly.
 */
_bftPFHID = [
	{
		(_this select 0) params ["_unit"];
		private _instance = _this select 1;

		// Exit and remove the PFH when dead.
		if ( !(alive _unit) ) exitWith {
			[_instance] call CBA_fnc_removePerFrameHandler;
		};

		// Check current and correct BFT states match.
		private _currentHasBFT = _unit getVariable ["bwi_armory_hasBFT", nil];
		private _correctHasBFT = ("ACE_microDAGR" in (items _unit) || "ACE_DAGR" in (items _unit));

		// If first run, force a refresh by setting opposite.
		if ( isNil "_currentHasBFT" ) then {
			_currentHasBFT = !_correctHasBFT;
		};

		// BFT state has changed.
		if ( !(_currentHasBFT isEqualTo _correctHasBFT) ) then {
			// Log the state change.
			["BFT state change to %2 for %1", _unit, _correctHasBFT] call bwi_common_fnc_log;

			// Update the ACE setting and hasBFT variable.
			if ( isPlayer _unit ) then { ace_map_BFT_Enabled = _correctHasBFT; };
			_unit setVariable ["bwi_armory_hasBFT", _correctHasBFT];

			// For leaders we also check the group state.
			if ( leader group _unit == _unit ) then {
				// Hide the group marker and broadcast. Note that we inverse the _correctHasBFT value.
				(group _unit) setVariable ["ace_map_hideBlueForceMarker", !_correctHasBFT, true];
			};
		};
	},
	ace_map_BFT_Interval, // Run at same frequency as BFT refresh rate.
	[_unit]
] call CBA_fnc_addPerFrameHandler;

// Save the handler to a variable.
_unit setVariable ["bwi_armory_bftPFHID", _bftPFHID];