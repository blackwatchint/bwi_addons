params["_unit", "_factionData", "_roleData"];

/**
 * Define macro to calculate the
 * channel for AN/PRC-343 radios.
 */
bwi_armory_macro_getPRC343Chn = {
	params ["_block", "_channel"];
	private _result = (((_block - 1) * 16) + _channel);
	_result
};

// Pointer to the configs.
private _factionClasses = _factionData splitString "/";
private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;

private _roleClasses = _roleData splitString "/";
private _roleCfg = configFile >> "CfgPlayableRoles" >> _roleClasses select 0 >> _roleClasses select 1;

// Get the radio configurations.
private _radioDevices = getArray (_factionCfg >> "acreRadioDevices");
private _radioChannels = getArray (_roleCfg >> "acreRadioChannels");

// Identify the correct radio configuration.
private _configuredRadios = [];

{
	private _device = _radioDevices select _forEachIndex;
	private _channel = _radioChannels select _forEachIndex;

	// If the device is not "NONE", check for a defined channel.
	if ( _device != "NONE" ) then {
		// If the corresponding channel is over 0, a channel is defined.
		if ( _channel > 0 ) then {
			// Add the device to the inventory.
			if ( _device in ["ACRE_PRC117F", "ACRE_PRC77", "ACRE_SEM70"] ) then {
				[_unit, _device, 1] call bwi_armory_fnc_addToBackpack;
			} else {
				[_unit, _device, 1] call bwi_armory_fnc_addToUniform;
			};

			// Calculate the correct channel number for 343 radios.
			if ( _device == "ACRE_PRC343" ) then {
				private _slotIdArr = [_unit] call bwi_common_fnc_getSlotIdArray;
				private _elementNum = _slotIdArr select 2;

				// Use the channel as the block, and the element number as the channel.
				_channel = [_channel, _elementNum] call bwi_armory_macro_getPRC343Chn;
			};

			// Save for ACRE initilization.
			_configuredRadios pushBack [_device, _channel];
		};
	};
} forEach _radioDevices;


/**
 * Set the radios channel, spatial and PTT settings.
 * Must be run as a function within CBA_fnc_waitUntilAndExecute
 * after the acre API has initialized the radio with its ID!
 */ 
[
	{
		([] call acre_api_fnc_isInitialized)
	},
	{
		params ["_radios", "_unit"];
	
		// Don't bother if there's no radios.
		if ( count _radios > 0 ) then {
			private _rids = [];
			private _ears = ["LEFT", "RIGHT", "CENTER"];

			// Only process first three radios.
			_radios = _radios select [0,3];

			// List of all unconfigured radios in the inventory.
			// Used to ensure we configure only unique radios.
			private _unconfiguredRadios = [] call acre_api_fnc_getCurrentRadioList;

			// Iterate over the radios.
			{
				// Radio is in the inventory.
				if ( [_unit, _x select 0] call acre_api_fnc_hasKindOfRadio ) then {
					// Only apply settings for player units.
					if ( isPlayer _unit ) then {
						private _rid = "";
						private _type = _x select 0;

						// Find a unique unconfigured radio of the corresponding type.
						// We do this to so we always get a unique ID, unlike acre_api_fnc_getRadioByType.
						{
							private _foundRadio = toLower(_x) find toLower(_type);

							if ( _foundRadio > -1 ) exitWith {
      							_rid = _x;
      							_unconfiguredRadios deleteAt _forEachIndex;
							};
						} forEach _unconfiguredRadios;

						// A unique radio ID of the correct type has been found.
						if ( _rid != "" ) then {
							// Set the radio channel for multi-channel radios.
							if ( !(_type in ["ACRE_PRC77", "ACRE_SEM70"]) ) then {
								[_rid, _x select 1] call acre_api_fnc_setRadioChannel;
							};

							// Set the spatial setting.
							[_rid, _ears select _forEachIndex ] call acre_api_fnc_setRadioSpatial;

							// Save the radio ID for PTT assignment.
							_rids pushBack _rid;
						};
					// Otherwise log the configuration for debugging.
					} else {
						["AI %3 received radio %1 (Chn %2). Skipping configuration...", _x select 0, _x select 1, _unit] call bwi_common_fnc_log;
					};
				} else {
					["%1 radio for %3 is missing.", _x select 0, _x select 1, _unit] call bwi_common_fnc_log;
				};
			} forEach _radios;

			// Assign the radios PTT in order.
			[ _rids ] call acre_api_fnc_setMultiPushToTalkAssignment;
		};
	},
	[_configuredRadios, _unit]
] call CBA_fnc_waitUntilAndExecute;