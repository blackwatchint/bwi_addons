params["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];

// Only run for local player.
if ( !isDedicated && hasInterface && local player ) then {
	// If the respawn variables are set (they should be).
	if ( !isNil "bwi_armory_selectedSide" && !isNil "bwi_armory_selectedFaction" && !isNil "bwi_armory_selectedRole" ) then {
		private _side = bwi_armory_selectedSide;
		private _faction = bwi_armory_selectedFaction;
		private _role = bwi_armory_selectedRole;

		// Process the loadout for the player.
		[_newUnit, _side, _faction, _role] call bwi_armory_fnc_processLoadout;

		// Inform the user of respawn reloading.
		["Respawn Success", "Loadout automatically reloaded.", 2] call bwi_common_fnc_notification;

		// If the accessories respawn variable is set (it should be).
		if ( !isNil "bwi_armory_selectedAccessories" ) then {
			private _accessories = bwi_armory_selectedAccessories;

			// Process the accessories for the player.
			[
				_newUnit,
				_accessories select 0, 
				_accessories select 1,
				_accessories select 2,
				_accessories select 3,
				_accessories select 4,
				_accessories select 5
			] call bwi_armory_fnc_processAccessories;
		}; // Fail silently.
	// Otherwise, display an error.
	} else {
		["Respawn Error", "Loadout not automatically reloaded, please re-select from the Armory!", 0, false, 10] call bwi_common_fnc_notification;
	};
};