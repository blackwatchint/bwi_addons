params ["_unit", "_topRailData", "_sideRailData", "_bottomRailData", "_barrelData", "_chestpackData", "_headmountData"];

// Add each of the primary weapon items.
if ( _topRailData != "NONE" ) then {
	_unit addPrimaryWeaponItem _topRailData;
};

if ( _sideRailData != "NONE" ) then {
	_unit addPrimaryWeaponItem _sideRailData;
};

if ( _bottomRailData != "NONE" ) then {
	_unit addPrimaryWeaponItem _bottomRailData;
};

if ( _barrelData != "NONE" ) then {
	_unit addPrimaryWeaponItem _barrelData;
};

// Add the chestpack.
if ( _chestpackData != "NONE" ) then {
	// Add the chestpack using the backpack on chest mod.
	// Can add directly - pre-existing chestpacks are deleted.
	[_unit, _chestpackData] call zade_boc_fnc_addChestpack;
} else {
	// No chestpack selected, so remove existing.
	if ( [_unit] call zade_boc_fnc_chestpack != "" ) then {
		[_unit] call zade_boc_fnc_removeChestpack;
	};
};

// Add the headmount item.
if ( _headmountData != "NONE" ) then {
	_unit linkItem _headmountData;
};

/**
 * Wait a second then check the unit for any errant magazines
 * loaded in the weapon caused by accessory changes.
 */
[
	{
		params ["_unit"];

		private _loadedMagazine = currentMagazine _unit;
		if ( _loadedMagazine != "" ) then {
			_unit removePrimaryWeaponItem _loadedMagazine;
		};
	},
	[_unit]
	, 1
] call CBA_fnc_waitAndExecute;

// Set the selected accessories variable.
// Do not overwrite if applying to AI units.
if ( isPlayer _unit && hasInterface ) then {
	bwi_armory_selectedAccessories = [_topRailData, _sideRailData, _bottomRailData, _barrelData, _chestpackData, _headmountData];
};