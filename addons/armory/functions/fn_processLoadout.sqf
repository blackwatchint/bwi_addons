params ["_unit", "_sideData", "_factionData", "_roleData"];

// Log the processing.
["Processing loadout for %1 (%2 >> %3)", _unit, _factionData, _roleData] call bwi_common_fnc_log;

// Pointer to the config.
private _factionClasses = _factionData splitString "/";
private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;

// Load script names from the config.
private _uniformScript = getText (_factionCfg >> "uniformScript");
private _weaponScript  = getText (_factionCfg >> "weaponScript");
private _grenadeScript  = getText (_factionCfg >> "grenadeScript");
private _ammoScript    = getText (_factionCfg >> "ammoScript");
private _gearScript    = getText (_factionCfg >> "gearScript");

// Preprocess the script files.
private _uniformScriptPP = preprocessFileLineNumbers _uniformScript;
private _weaponScriptPP = preprocessFileLineNumbers _weaponScript;
private _grenadeScriptPP = preprocessFileLineNumbers _grenadeScript;
private _ammoScriptPP = preprocessFileLineNumbers _ammoScript;
private _gearScriptPP = preprocessFileLineNumbers _gearScript;

// Reformat role class names for script usage.
private _roleClasses = _roleData splitString "/";
private _role = toLower (_roleClasses joinString "_");

// Load faction year and type from the config.
private _year = getNumber (_factionCfg >> "year");
private _type = getText (_factionCfg >> "type");

// Call the faction script files.
[_unit, _role, _year, _type] call compile _uniformScriptPP;
[_unit, _role, _year, _type] call compile _weaponScriptPP;
[_unit, _role, _year, _type] call compile _grenadeScriptPP;
[_unit, _role, _year, _type] call compile _gearScriptPP;
[_unit, _role, _year, _type] call compile _ammoScriptPP;

// Call the setup functions to complete the process.
[_unit, _factionData, _roleData] call bwi_armory_fnc_setUnitVars;
[_unit, _factionData, _roleData] call bwi_armory_fnc_setUnitCallsign;
[_unit, _factionData, _roleData] call bwi_armory_fnc_setAcreBabel;
[_unit, _factionData, _roleData] call bwi_armory_fnc_setAcreRadios;
[_unit, _factionData, _roleData] call bwi_armory_fnc_registerBFTHandler;
[_unit, _factionData, _roleData] call bwi_armory_fnc_registerChannelHandler;

// Save the faction and role for respawn.
// Do not overwrite if applying to AI units.
if ( isPlayer _unit && hasInterface ) then {
	bwi_armory_selectedSide = _sideData;
	bwi_armory_selectedFaction = _factionData;
	bwi_armory_selectedRole = _roleData;
};
