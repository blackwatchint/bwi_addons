params ["_addToObject"];

// Add the action and broadcast.
[_addToObject, [
	"<t color='#d58200'>Armory</t>",
	{ call bwi_armory_fnc_openArmoryDialog; }, 
	nil, 1.5, false
]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP