params ["_unit", "_item", "_quantity"];

if ( _quantity > 0 ) then {
	private _canFit = _unit canAddItemToVest [_item, _quantity];

	if ( _canFit ) then {
		for "_i" from 1 to _quantity do { 
			_unit addItemToVest _item;
		};
	} else {
		["Could not add %2x %1 to vest for %3.", _item, _quantity, _unit] call bwi_common_fnc_log;
	};
};