params ["_side"];

private _labels = [];
private _values = [];
private _default = 0;

// Load factions from the config.
private _sideFilter = format ["getNumber(_x >> 'side') == %1", _side];
private _factionCfgs = _sideFilter configClasses (configFile >> "CfgPlayableFactions");

// Build the factions list.
{
    private _factionLabel = getText (_x >> "name");
    private _factionValue = configName (_x);

    // Load faction children from the config.
	private _loadoutCfgs = "true" configClasses (_x);

    {
        _labels pushBack format ["%1 (c. %3) %2", _factionLabel, getText (_x >> "name"), getNumber (_x >> "year")];
        _values pushBack format ["%1/%2", _factionValue, configName (_x)];
    } forEach _loadoutCfgs;
} forEach _factionCfgs;

[_values, _labels, _default]