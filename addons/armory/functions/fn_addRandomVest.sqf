params ["_unit", "_classes"];

// Guarantee that vest seed is unique by passing a seed ID.
private _randomVest = [_classes, 2] call bwi_common_fnc_selectRandomOnce;

// Add the uniform and return the classname.
_unit addVest _randomVest;
_randomVest