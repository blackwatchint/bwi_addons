params["_unit", "_factionData", "_roleData"];

// Initialize the babel languages.
if (isNil "bwi_armory_acreBabelInitialized") then {
	["blu", bwi_armory_babelLanguageBlufor] call acre_api_fnc_babelAddLanguageType;
	["opf", bwi_armory_babelLanguageOpfor] call acre_api_fnc_babelAddLanguageType;
	["ind", bwi_armory_babelLanguageIndep] call acre_api_fnc_babelAddLanguageType;
	["civ", bwi_armory_babelLanguageCivil] call acre_api_fnc_babelAddLanguageType;
	
	bwi_armory_acreBabelInitialized = true;
};

// Fetch the babel configuration for the role.
private _roleClasses = _roleData splitString "/";
private _cfgRole = configFile >> "CfgPlayableRoles" >> _roleClasses select 0 >> _roleClasses select 1;
private _isInterpreter = getNumber (_cfgRole >> "acreBabelInterpreter");

// Set spoken languages based on role and unit side.
if ( _isInterpreter == 1 ) then {
	["blu", "opf", "ind", "civ"] call acre_api_fnc_babelSetSpokenLanguages;
} else {
	switch ( side _unit ) do {
		case west:		 { ["blu"] call acre_api_fnc_babelSetSpokenLanguages;	};
		case east: 	  	 { ["opf"] call acre_api_fnc_babelSetSpokenLanguages;	};
		case resistance: { ["ind"] call acre_api_fnc_babelSetSpokenLanguages;	};
		case civilian:   { ["civ"] call acre_api_fnc_babelSetSpokenLanguages;	};
	};	
};

// Set starting language according to side.
switch ( side _unit ) do {
	case west: 		 { ["blu"] call acre_api_fnc_babelSetSpeakingLanguage; };
	case east: 	  	 { ["opf"] call acre_api_fnc_babelSetSpeakingLanguage; };
	case resistance: { ["ind"] call acre_api_fnc_babelSetSpeakingLanguage; };
	case civilian:   { ["civ"] call acre_api_fnc_babelSetSpeakingLanguage; };
};