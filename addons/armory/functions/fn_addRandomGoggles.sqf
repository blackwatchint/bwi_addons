params ["_unit", "_classes"];

// Guarantee that weapon seed is unique by passing a seed ID.
private _randomGoggles = [_classes, 6] call bwi_common_fnc_selectRandomOnce;

// Add the goggles and return the classname.
_unit addWeapon _randomGoggles;
_randomGoggles