params ["_unit", "_factionData", "_roleData"];

// Only run for group leader.
if ( leader group _unit == _unit ) then {
	private _groupCallsign = "";
	private _slotIdArr = [_unit] call bwi_common_fnc_getSlotIdArray;
	private _elementNum = _slotIdArr select 2;

	// Pointer to the config.
	private _roleClasses = _roleData splitString "/";
	private _cfgElement = configFile >> "CfgPlayableRoles" >> _roleClasses select 0;

	// Fetch element callsigns parameter.
	private _callsigns = getArray (_cfgElement >> "callsigns");

	// If group leader, check for a role-specific callsign.
	if ( leader group _unit == leader _unit  ) then {
		_groupCallsign = getText (_cfgElement >> _roleClasses select 1 >> "callsign");
	};
	
	// If still empty, get the callsign that matches the element number.
	if ( _groupCallsign == "" ) then {
		if ( _elementNum > count _callsigns ) then { _elementNum = 1; }; // Handle out of bounds callsigns gracefully.
		_groupCallsign = _callsigns select (_elementNum - 1); // Element starts at 1, callsigns at 0.
	};

	// If Use Multiple Side Callsigns is true, put a side-specific prefix in front of element.
	_groupCallsign = [_unit, _groupCallsign] call bwi_common_fnc_groupIdWithSide;
	
	// This callsign isn't the group's current callsign.
	if ( groupId group _unit != _groupCallsign  ) then {
		private _isUniqueCallsign = [_groupCallsign] call bwi_common_fnc_isUniqueGroupId;

		// Another group isn't using this callsign.
		if ( _isUniqueCallsign ) then {
			group _unit setGroupIdGlobal [_groupCallsign];
		// Duplicate callsign attempt, display notification.
		} else {
			private _message = format ["Callsign %1 is already in use. This role may not permit multiple elements, or you are in the wrong group or slot.", _groupCallsign];
			["Callsign Exists", _message, 0, true, 10] call bwi_common_fnc_notification;
		};
	};
};