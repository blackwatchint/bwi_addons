params ["_side", "_value"];

// Only run for matching player sides when the value has changed.
if ( playerSide == _side && !isNil "bwi_armory_selectedFaction" && _value != bwi_armory_selectedFaction ) then {
	// Reset the selectedFaction to re-initialize the armory GUI.
    bwi_armory_selectedFaction = nil;

	// Visually broadcast the setting change for visual clients.
	if ( hasInterface ) then {
		private _sideText = "UNKNOWN";
		private _sideColor = "#007f7f";

		switch ( _side ) do {
			case west: {
				_sideText = "BLUFOR";
				_sideColor = "#004c99";
			};

			case east: {
				_sideText = "OPFOR";
				_sideColor = "#7f0000";
			};

			case resistance: {
				_sideText = "INDEP";
				_sideColor = "#007f00";
			};

			case civilian: {
				_sideText = "CIVIL";
				_sideColor = "#66007f";
			};
		};

		// Get the faction name from the config.
		private _factionClasses = _value splitString "/";
		private _factionName = getText (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> "name");
		private _loadoutName  = getText (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1 >> "name");
		private _loadoutYear  = getNumber (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1 >> "year");

		// Display the message.
		private _message = format [
			"<t color='%2'>%1 Faction:</t><br/>%3 (c. %5)<br/>%4",
			_sideText,
			_sideColor,
			_factionName,
			_loadoutName,
			_loadoutYear
		];
		["Faction Changed", _message, 4] call bwi_common_fnc_notification;
	};
};