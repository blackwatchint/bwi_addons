params ["_unit", "_classes"];

// Guarantee that uniform seed is unique by passing a seed ID.
private _randomUniform = [_classes, 1] call bwi_common_fnc_selectRandomOnce;

// Add the uniform and return the classname.
_unit forceAddUniform _randomUniform;
_randomUniform