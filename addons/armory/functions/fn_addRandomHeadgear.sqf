params ["_unit", "_classes"];

// Guarantee that headgear seed is unique by passing a seed ID.
private _randomHeadgear = [_classes, 3] call bwi_common_fnc_selectRandomOnce;

// Add the uniform and return the classname.
_unit addHeadgear _randomHeadgear;
_randomHeadgear