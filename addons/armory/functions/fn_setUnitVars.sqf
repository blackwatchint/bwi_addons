params ["_unit", "_factionData", "_roleData"];

// Pointer to the config.
private _roleClasses =_roleData splitString "/";
private _roleCfg = configFile >> "CfgPlayableRoles" >> _roleClasses select 0 >> _roleClasses select 1;

// Assign unit variables based on config parameters.
_unit setVariable ["ACE_medical_medicClass", getNumber (_roleCfg >> "aceIsMedic"), true];
_unit setVariable ["ACE_isEngineer", 		 getNumber (_roleCfg >> "aceIsEngineer"), true];
_unit setVariable ["ACE_isEOD",  			 (getNumber (_roleCfg >> "aceIsEOD") == 1), true]; // Convert to boolean.
_unit setVariable ["ACE_GForceCoef",  		 getNumber (_roleCfg >> "aceGForceCoef")];
_unit setVariable ["ace_advanced_fatigue_performanceFactor", getNumber (_roleCfg >> "aceStaminaFactor")];

// Refresh the advanced fatigue stamina factor.
[_unit, objNull] call ace_advanced_fatigue_fnc_handlePlayerChanged;

// Set the isZeus flag only when the slot and role match too.
private _slotIdArr = [_unit] call bwi_common_fnc_getSlotIdArray;
private _slottedRole = toLower(_slotIdArr select 3);
private _selectedRole = toLower(_roleClasses select 1);

if ( _slottedRole == _selectedRole && getNumber (_roleCfg >> "isZeus") == 1 ) then {
	_unit setVariable ["isZeus", true];
} else {
	_unit setVariable ["isZeus", false];
};
