class Logic;
class Module_F: Logic
{
	class ArgumentsBaseUnits
	{
		class Units;
	};
	class ModuleDescription;
};


/**
 * Adds a BWI Armory interface to any synchronised objects.
 */ 
class BWI_Armory_ModuleAddArmory: Module_F
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Add Armory";
	icon = "\bwi_armory\data\icon_arm_w.paa";
	category = "BWI_Armory";
	function = "bwi_armory_module_fnc_addArmory";
	functionPriority = 1;
	isGlobal = 1;
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;
	curatorCanAttach = 1; // Required for curatorMouseOver.

	class ModuleDescription: ModuleDescription
	{
		description = "Enables usage of the BWI Armory on synchronised object.";
		sync[] = {};
	};
};