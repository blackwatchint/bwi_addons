/**
 * ctrlIDCs: xListSide, tvFaction, tvRole, lblErrorText
 */
params ["_display", "_ctrlIDCs", "_launchAccessoriesDialog"];

if( !local player ) exitWith {};

private _xlistSide    = _display displayctrl (_ctrlIDCs select 0);
private _tvFaction    = _display displayctrl (_ctrlIDCs select 1);
private _tvRole       = _display displayctrl (_ctrlIDCs select 2);
private _lblErrorText = _display displayctrl (_ctrlIDCs select 3);

// Prepare for error messages.
private _error = false;
private _errorMsg = "";

// Get currently selected values from the GUI.
private _sideData = lbCurSel _xlistSide;
private _factionData = _tvFaction tvData (tvCurSel _tvFaction);
private _factionClasses = _factionData splitString "/";
private _roleData = _tvRole tvData (tvCurSel _tvRole);
private _roleClasses = _roleData splitString "/";

// Error check the selections!
if ( count _factionClasses < 2 ) then {
	_error = true;
	_errorMsg = "Please select a faction!";
};

if ( count _roleClasses < 2 ) then {
	_error = true;
	_errorMsg = "Please select a role!";
};

// No errors.
if ( !_error ) then {
	// Pointers to the configs.
	private _factionGroupCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0;
	private _factionCfg 	 = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;
	private _elementCfg 	 = configFile >> "CfgPlayableRoles" >> _roleClasses select 0;
	private _roleCfg 		 = configFile >> "CfgPlayableRoles" >> _roleClasses select 0 >> _roleClasses select 1;

	// Load the relevant config data.
	private _factionGroupName   = getText (_factionGroupCfg >> "name");
	private _factionGroupSide   = getNumber (_factionGroupCfg >> "side");
	private _factionName   		= getText (_factionCfg >> "name");
	private _factionYear   		= getNumber (_factionCfg >> "year");
	private _elementName   		= getText (_elementCfg >> "name");
	private _roleName 	   		= getText (_roleCfg >> "name");
	private _roleCallsign 		= getText (_roleCfg >> "callsign");

	// Unit is always player.
	private _unit = player;
	
	// Add prefix to unique roles if Use Multiple Side Callsigns is enabled (e.g. Mechanic -> B-Mechanic)
	_roleCallsign = [_unit, _roleCallsign] call bwi_common_fnc_groupIdWithSide;
	
	// If a unique role callsign is specified, check the role is not already in use.
	if ( _roleCallsign != "" && leader group _unit == leader _unit ) then {
		// Check role is unique by checking against the groupId (callsign).
		private _isUniqueRole = [_roleCallsign] call bwi_common_fnc_isUniqueGroupID;

		// Role is not unique, and isn't the current group's callsign.
		if ( !_isUniqueRole && _roleCallsign != groupId group _unit ) then {
			_error = true;
			_errorMsg = "Unique role already in use!";
		};
	};

	// Still no errors.
	if ( !_error ) then {
		// Process the loadout for the player.
		[_unit, _sideData, _factionData, _roleData] call bwi_armory_fnc_processLoadout;

		// Inform the user of selection.
		private _message = format [
			"%1 (c. %3) %2<br/>%4 %5",
			_factionGroupName,
			_factionName,
			_factionYear,
			_elementName,
			_roleName
		];
		["Loadout Selected", _message, 2] call bwi_common_fnc_notification;

		// Clear any error messages.
		_lblErrorText ctrlSetStructuredText parseText "";

		// Close the dialog.
		closeDialog 1;

		// Launch accessories dialog accordingly.
		if ( _launchAccessoriesDialog ) then {
			call bwi_armory_fnc_openAccessoriesDialog;
		};
	};
};

// Errors.
if ( _error ) then {
	// Display any error messages.
	_lblErrorText ctrlSetStructuredText parseText format["<t color='#ff1111'>%1</t>", _errorMsg];
};

false