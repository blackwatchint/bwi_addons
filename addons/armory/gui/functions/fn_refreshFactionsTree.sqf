/**
 * ctrlIDCs: xListSide, tvFaction
 */
params ["_display", "_ctrlIDCs"];

private _xlistSide = _display displayctrl (_ctrlIDCs select 0);
private _tvFaction = _display displayctrl (_ctrlIDCs select 1);

tvClear _tvFaction;

// Set the default selected faction, unless factions are unlocked.
private _side = lbCurSel _xlistSide;
if ( isNil "bwi_armory_selectedFaction" ) then {
	// Load the default faction from the appropriate CBA setting variable.
	switch ( _side ) do {
		case 0: { bwi_armory_selectedFaction = bwi_armory_playerFactionBlufor; };
		case 1: { bwi_armory_selectedFaction = bwi_armory_playerFactionOpfor; };
		case 2: { bwi_armory_selectedFaction = bwi_armory_playerFactionIndep; };
		case 3: { bwi_armory_selectedFaction = bwi_armory_playerFactionCivil; };
	};
};

// Load factions from the config.
private _sideFilter = format ["getNumber(_x >> 'side') == %1", _side];
private _factionGroupCfgs = _sideFilter configClasses (configFile >> "CfgPlayableFactions");

// Build the faction tree.
{
	// Add faction parent entry.
	private _parentId = _tvFaction tvAdd [[], getText (_x >> "name")];
	private _parentData = configName (_x);

	_tvFaction tvSetData [[_parentId], _parentData];

	// Set parent entry icon.
	_tvFaction tvSetPicture [[_parentId], getText (_x >> "icon")];

	// Load faction children from the config.
	private _factionCfgs = "true" configClasses (_x);

	{
		// Generate name and data values from config.
		private _childName = format ["(c. %2) %1", getText (_x >> "name"), getNumber (_x >> "year")];
		private _childData = format ["%1/%2", _parentData, configName (_x)]; // Delimit with slash so underscore can be in classnames.

		// Add faction child entry.
		private _childId = _tvFaction tvAdd [[_parentId], _childName];
		_tvFaction tvSetData [[_parentId, _childId], _childData];

		// If the faction is selected, set the cursor and expand the parent.
		if ( bwi_armory_selectedFaction == _childData ) then {
			_tvFaction tvSetCurSel [_parentId, _childId];
			_tvFaction tvExpand [_parentId];
		};
	} forEach _factionCfgs;

	// Sort the child tree.
	_tvFaction tvSort [[_parentId], false];
} forEach _factionGroupCfgs;

// Sort the parent tree.
_tvFaction tvSort [[], false];

// Handle locking of the faction tree.
if ( bwi_armory_unlockAllFactions ) then {
	_tvFaction ctrlEnable true;
} else {
	_tvFaction ctrlEnable false;
};