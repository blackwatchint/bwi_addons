/**
 * ctrlIDCs: tvTree
 */
params ["_display", "_ctrlIDCs", "_slotId"];

// This function is called on multiple controls, so we handle the GUI dynamically.
private _tvTree = _display displayctrl (_ctrlIDCs select 0);

tvClear _tvTree;

// Set the default selected accessories as an array of empty values.
if ( isNil "bwi_armory_selectedAccessories" ) then {
	bwi_armory_selectedAccessories = ["", "", "", "", "", ""];
};

// Load filter information from configs.
private _factionClasses = bwi_armory_selectedFaction splitString "/";
private _roleClasses = bwi_armory_selectedRole splitString "/";
private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;
private _roleCfg	= configFile >> "CfgPlayableRoles" >> _roleClasses select 0 >> _roleClasses select 1;

// Pad allowedAccessories to ensure it equals 6 slots.
private _allowedAccessories = getArray (_roleCfg >> "allowedAccessories");
for "_i" from 1 to (6 - count _allowedAccessories) do {
	_allowedAccessories pushBack 0;
};

private _factionYear = getNumber (_factionCfg >> "year");
private _maxLevel = _allowedAccessories select _slotId;

// Load accessory groups from the config.
private _groupFilter = format ["getNumber(_x >> 'slot') == %1 && getNumber(_x >> 'level') <= %2", _slotId, _maxLevel];
private _accessoryGroupCfgs = _groupFilter configClasses (configFile >> "CfgPlayableAccessories");

// Add "None" to start the tree.
_tvTree tvAdd [[], "None"];
_tvTree tvSetData [[0], "NONE"];
_tvTree tvSetCurSel [0];

// Build the accessory tree.
{
	// Add group parent entry.
	private _parentId = _tvTree tvAdd [[], getText (_x >> "name")];

	// Load the filtering rules from the AccessoryGroup.
	private _filterOnClass = call compile (getText (_x >> "compatibleAgainst") + " player;");
	private _parentClassTree = getText (_x >> "parentClassTree");
	
	// Load child accessories from the config.
	private _accessoryFilter = format ["getNumber(_x >> 'year') <= %1 && ((count getArray(_x >> 'compatibleClasses') == 0) || ('%2' in getArray(_x >> 'compatibleClasses')))", _factionYear, _filterOnClass];
	private _accessoryCfgs = _accessoryFilter configClasses (_x);

	// There are accessories to add.
	if ( count _accessoryCfgs > 0 ) then {
		{
			// Generate name and data values from config.
			private _childName = getText (configFile >> _parentClassTree >> configName (_x) >> "displayName");
			private _childPicture = getText (configFile >> _parentClassTree >> configName (_x) >> "picture");
			private _childData = configName (_x);

			// Add accessory child entry.
			private _childId = _tvTree tvAdd [[_parentId], _childName];
			_tvTree tvSetPicture [[_parentId, _childId], _childPicture];
			_tvTree tvSetData [[_parentId, _childId], _childData];

			// If the accessory is currently equipped, set the cursor and expand the parent.
			if ( _childData == bwi_armory_selectedAccessories select _slotId ) then {
				_tvTree tvSetCurSel [_parentId, _childId];
				_tvTree tvExpand [_parentId];
			};
		} forEach _accessoryCfgs;

		// Sort the child tree.
		_tvTree tvSort [[_parentId], false];
	// No accessories to add.
	} else {
		// Remove empty parent.
		_tvTree tvDelete [_parentId];
	};
} forEach _accessoryGroupCfgs;