/**
 * ctrlIDCs: xListSide
 */
params ["_display", "_ctrlIDCs"];

private _xlistSide = _display displayctrl (_ctrlIDCs select 0);

lbClear _xlistSide;

// Populate the list.
_xlistSide lbAdd "BLUFOR";
_xlistSide lbAdd "OPFOR";
_xlistSide lbAdd "INDEP";
_xlistSide lbAdd "CIVIL";

// Default to player sides.
if ( isNil "bwi_armory_selectedSide" ) then {
	bwi_armory_selectedSide = [playerSide] call bwi_common_fnc_sideToIndex;
};

// Set selected side.
_xlistSide lbSetCurSel bwi_armory_selectedSide;