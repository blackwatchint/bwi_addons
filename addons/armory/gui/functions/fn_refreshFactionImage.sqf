/**
 * ctrlIDCs: lblFactionIcon, tvFaction
 */
params ["_display", "_ctrlIDCs"];

private _imgFaction = _display displayctrl (_ctrlIDCs select 0);
private _tvFaction 	    = _display displayctrl (_ctrlIDCs select 1);

// Load selected faction's data.
private _factionData = _tvFaction tvData (tvCurSel _tvFaction);
private _factionClasses = _factionData splitString "/";

// Change the faction icon.
_imgFaction ctrlSetText getText (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> "image");