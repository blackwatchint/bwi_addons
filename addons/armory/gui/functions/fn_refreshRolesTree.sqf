/**
 * ctrlIDCs: xListSide, tvFaction, tvRole
 */
params ["_display", "_ctrlIDCs"];

private _xlistSide = _display displayctrl (_ctrlIDCs select 0);
private _tvFaction = _display displayctrl (_ctrlIDCs select 1);
private _tvRole    = _display displayctrl (_ctrlIDCs select 2);

tvClear _tvRole;

// Set a default selected role.
if ( isNil "bwi_armory_selectedRole" ) then {
	private _slotIdArr = [player] call bwi_common_fnc_getSlotIdArray;
	bwi_armory_selectedRole = format ["%1/%2", _slotIdArr select 1, _slotIdArr select 3];
};

// Load selected faction's filter data.
private _side = lbCurSel _xlistSide;
private _factionData = _tvFaction tvData (tvCurSel _tvFaction);
private _factionClasses = _factionData splitString "/";

if ( count _factionClasses < 2 ) exitWith {}; // Handle un-selectable parent factions gracefully.

private _hiddenElements = getArray (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1 >> "hiddenElements");
private _hiddenRoles = getArray (configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1 >> "hiddenRoles");

// Independently filter out weapons, recon and special forces.
private _filterExtraElements = "";
if ( !bwi_armory_unlockReconElements ) then {
	_filterExtraElements = _filterExtraElements + "&& ( getNumber(_x >> 'isReconElement') == 0 ) ";
};

if ( !bwi_armory_unlockSpecialElements ) then {
	_filterExtraElements = _filterExtraElements + "&& ( getNumber(_x >> 'isSpecialElement') == 0 ) ";
};

if ( !bwi_armory_unlockWeaponsElements ) then {
	_filterExtraElements = _filterExtraElements + "&& ( getNumber(_x >> 'isWeaponsElement') == 0 ) ";
};

// Load elements from the config.
private _elementFilter = format ["!(configName(_x) in %1) && (%2 in getArray(_x >> 'sides')) %3", _hiddenElements, _side, _filterExtraElements];
private _elementCfgs = _elementFilter configClasses (configFile >> "CfgPlayableRoles");

// Build the element/roles tree.
{
	// Add role parent entry.
	private _parentId = _tvRole tvAdd [[], getText (_x >> "name")];
	private _parentData = configName (_x);

	// Load the role children from the config.
	private _rolesFilter = format ["!(configName(_x) in %1) && getNumber(_x >> 'isHidden') == 0", _hiddenRoles];
	private _roleCfgs = _rolesFilter configClasses (_x);

	{
		// Generate name and data values from config.
		private _childName = format["%1", getText (_x >> "name")];
		private _childData = format["%1/%2", _parentData, configName (_x)];

		// Add role child entry.
		private _childId = _tvRole tvAdd [[_parentId], _childName];
		_tvRole tvSetData [[_parentId, _childId], _childData];

		// If the role is selected, set the cursor and expand the parent.
		if ( bwi_armory_selectedRole == _childData ) then {
			_tvRole tvSetCurSel [_parentId, _childId];
			_tvRole tvExpand [_parentId];
		};
	} forEach _roleCfgs;
} forEach _elementCfgs;