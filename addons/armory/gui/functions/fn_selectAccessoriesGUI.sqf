/**
 * ctrlIDCs: tvTopRail, tvSideRail, tvBottomRail, tvBarrel, tvChestpack, tvHeadmount, lblErrorText
 */
params ["_display", "_ctrlIDCs"];

if( !local player ) exitWith {};

private _tvTopRail    = _display displayctrl (_ctrlIDCs select 0);
private _tvSideRail   = _display displayctrl (_ctrlIDCs select 1);
private _tvBottomRail = _display displayctrl (_ctrlIDCs select 2);
private _tvBarrel     = _display displayctrl (_ctrlIDCs select 3);
private _tvChestpack  = _display displayctrl (_ctrlIDCs select 4);
private _tvHeadmount  = _display displayctrl (_ctrlIDCs select 5);
private _lblErrorText = _display displayctrl (_ctrlIDCs select 6);

// Prepare for error messages.
private _error = false;
private _errorMsg = "";

// Get currently selected values from the GUI.
private _topRailData = _tvTopRail tvData (tvCurSel _tvTopRail);
private _sideRailData = _tvSideRail tvData (tvCurSel _tvSideRail);
private _bottomRailData = _tvBottomRail tvData (tvCurSel _tvBottomRail);
private _barrelData = _tvBarrel tvData (tvCurSel _tvBarrel);
private _chestpackData = _tvChestpack tvData (tvCurSel _tvChestpack);
private _headmountData = _tvHeadmount tvData (tvCurSel _tvHeadmount);

// Error check the selections!
if ( _topRailData == "" ) then {
	_error = true;
	_errorMsg = "Please select a top rail item!";
};

if ( _sideRailData == "" ) then {
	_error = true;
	_errorMsg = "Please select a side rail item!";
};

if ( _bottomRailData == "" ) then {
	_error = true;
	_errorMsg = "Please select a bottom rail item!";
};

if ( _barrelData == "" ) then {
	_error = true;
	_errorMsg = "Please select a barrel item!";
};

if ( _chestpackData == "" ) then {
	_error = true;
	_errorMsg = "Please select a chestpack!";
};

if ( _headmountData == "" ) then {
	_error = true;
	_errorMsg = "Please select a head mount!";
};

// No errors.
if ( !_error ) then {
	// Process the accessories for the player.
	[player, _topRailData, _sideRailData, _bottomRailData, _barrelData, _chestpackData, _headmountData] call bwi_armory_fnc_processAccessories;

	// Close the dialog.
	closeDialog 1;
// Errors.
} else {
	// Display any error messages.
	_lblErrorText ctrlSetStructuredText parseText format["<t color='#ff1111'>%1</t>", _errorMsg];
};

false