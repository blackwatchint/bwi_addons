/**
 * ctrlIDCs: lblSlotText
 */
params ["_display", "_ctrlIDCs"];

private _lblSlotText = _display displayctrl (_ctrlIDCs select 0);

// Get the player's slot array.
private _slotIdArr = [player] call bwi_common_fnc_getSlotIdArray;

// Identify the side first.
private _sideText = "";
switch ( _slotIdArr select 0 ) do {
	case "b": {
		_sideText = "BLUFOR";
	};
	case "o": {
		_sideText = "OPFOR";
	};
	case "i": {
		_sideText = "INDEP";
	};
	case "c": {
		_sideText = "CIVIL";
	};
};

// Get the element callsign from the config.
private _element = toUpper (_slotIdArr select 1);
private _elementNum = _slotIdArr select 2;

private _cfgCallsigns = getArray (configFile >> "CfgPlayableRoles" >> _element  >> "callsigns");

if ( _elementNum > count _cfgCallsigns ) then { _elementNum = 1; }; // Handle out of bounds callsigns gracefully.
private _elementText =  _cfgCallsigns select (_elementNum - 1); // Element starts at 1, callsigns at 0.

// Get the role name from the config.
private _role = toUpper (_slotIdArr select 3);
private _roleText = getText (configFile >> "CfgPlayableRoles" >> _element >> _role >> "name");

// Update the label text.
_lblSlotText ctrlSetStructuredText parseText format["Slotted: %1 / %2 / %3", _sideText, _elementText, _roleText];