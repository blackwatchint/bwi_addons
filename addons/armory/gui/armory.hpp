class BwiGui_RscText;
class BwiGui_RscStructuredText;
class BwiGui_RscPicture;
class BwiGui_RscButton;
class BwiGui_RscXListBox;
class BwiGui_RscTree;

class BwiDialogArmory
{
	idd = 6600;
	movingenable = 0;
	
	class ControlsBackground
	{
		class ArmoryBackground: BwiGui_RscPicture
		{
			idc = 6600;
			text = "\bwi_armory\gui\data\armory_bg.paa";
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = -3 * GUI_GRID_H + GUI_GRID_Y;
			w = 38.5 * GUI_GRID_W;
			h = 30 * GUI_GRID_H;
		};
	};

	class Controls
	{
		class imgFaction: BwiGui_RscPicture
		{
			idc = 6601;
			text = "";

			x = 34.6 * GUI_GRID_W + GUI_GRID_X;
			y = -2.45 * GUI_GRID_H + GUI_GRID_Y;
			w = 4.35 * GUI_GRID_W;
			h = 3.62 * GUI_GRID_H;
		};

		class lblSide: BwiGui_RscText
		{
			idc = 6602;
			text = "Side:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class xlistSide: BwiGui_RscXListBox
		{
			idc = 6603;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.275,0.463,0.694,1};
			
			onLoad = "[ctrlParent (_this select 0), [6603]] call bwi_armory_fnc_refreshSidesList;";
			onLBSelChanged = "[ctrlParent (_this select 0), [6603, 6605]] call bwi_armory_fnc_refreshFactionsTree;";
		};

		class lblFaction: BwiGui_RscText
		{
			idc = 6604;
			text = "Faction:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvFaction: BwiGui_RscTree
		{
			idc = 6605;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 16 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6603, 6605]] call bwi_armory_fnc_refreshFactionsTree;";
			onTreeSelChanged = "[ctrlParent (_this select 0), [6603, 6605, 6607]] call bwi_armory_fnc_refreshRolesTree;[ctrlParent (_this select 0), [6601, 6605]] call bwi_armory_fnc_refreshFactionImage;";
		};

		class lblRole: BwiGui_RscText
		{
			idc = 6606;
			text = "Role:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvRole: BwiGui_RscTree
		{
			idc = 6607;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 16 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6603, 6605, 6607]] call bwi_armory_fnc_refreshRolesTree;";
		};

		class lblSlotText: BwiGui_RscStructuredText
		{
			idc = 6608;
			x = 1.55 * GUI_GRID_W + GUI_GRID_X;
			y = 25.48 * GUI_GRID_H + GUI_GRID_Y;
			w = 36.8485 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6608]] call bwi_armory_fnc_refreshSlotText;";
		};

		class lblErrorText: BwiGui_RscStructuredText
		{
			idc = 6609;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.7 * GUI_GRID_H + GUI_GRID_Y;
			w = 12.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorText[] = {1,0,0,1};
		};

		class btnCancel: BwiGui_RscButton
		{
			idc = 6610;
			text = "Cancel";
			x = 14.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			onMouseButtonClick = "closeDialog 2;";
		};

		class btnSelect: BwiGui_RscButton
		{
			idc = 6611;
			text = "Select";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			
			onMouseButtonClick = "[ctrlParent (_this select 0), [6603, 6605, 6607, 6609], false] call bwi_armory_fnc_selectLoadoutGUI;";
		};

		class btnSelectCustomize: BwiGui_RscButton
		{
			idc = 6612;
			text = "Select & Customize";
			x = 27.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			
			onMouseButtonClick = "[ctrlParent (_this select 0), [6603, 6605, 6607, 6609], true] call bwi_armory_fnc_selectLoadoutGUI;";
		};		
	};
};