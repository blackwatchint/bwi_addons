class BwiGui_RscCombo;
class BwiGui_RscListbox;

class BwiDialogAccessories
{
	idd = 6700;
	movingenable = 0;
	
	class ControlsBackground
	{
		class AccessoriesBackground: BwiGui_RscPicture
		{
			idc = 6701;
			text = "\bwi_armory\gui\data\armory_bg.paa";
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = -3 * GUI_GRID_H + GUI_GRID_Y;
			w = 38.5 * GUI_GRID_W;
			h = 30 * GUI_GRID_H;
		};
	};


	class Controls
	{
		class lblTopRail: BwiGui_RscText
		{
			idc = 6704;
			text = "Top Rail:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvTopRail: BwiGui_RscTree
		{
			idc = 6705;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6705], 0] call bwi_armory_fnc_refreshAccessoriesTree;"; // IDC is defined!
		};

		class lblSideRail: BwiGui_RscText
		{
			idc = 6706;
			text = "Side Rail:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvSideRail: BwiGui_RscTree
		{
			idc = 6707;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6707], 1] call bwi_armory_fnc_refreshAccessoriesTree;"; // IDC is defined!
		};

		class lblBottomRail: BwiGui_RscText
		{
			idc = 6708;
			text = "Bottom Rail:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvBottomRail: BwiGui_RscTree
		{
			idc = 6709;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6709], 2] call bwi_armory_fnc_refreshAccessoriesTree;"; // IDC is defined!
		};

		class lblBarrel: BwiGui_RscText
		{
			idc = 6710;
			text = "Barrel:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvBarrel: BwiGui_RscTree
		{
			idc = 6711;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6711], 3] call bwi_armory_fnc_refreshAccessoriesTree;"; // IDC is defined!
		};

		class lblChestpack: BwiGui_RscText
		{
			idc = 6712;
			text = "Chestpack:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 15.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 6 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvChestpack: BwiGui_RscTree
		{
			idc = 6713;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6713], 4] call bwi_armory_fnc_refreshAccessoriesTree;"; // IDC is defined!
		};

		class lblHeadmount: BwiGui_RscText
		{
			idc = 6714;
			text = "Helmet Mount:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 15.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 6 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvHeadmount: BwiGui_RscTree
		{
			idc = 6715;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 5 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6715], 5] call bwi_armory_fnc_refreshAccessoriesTree;"; // IDC is defined!
		};

		class lblErrorText: BwiGui_RscStructuredText
		{
			idc = 6716;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.7 * GUI_GRID_H + GUI_GRID_Y;
			w = 12.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorText[] = {1,0,0,1};
		};

		class btnCancel: BwiGui_RscButton
		{
			idc = 6717;
			text = "Cancel";
			x = 22 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			onMouseButtonClick = "closeDialog 2;";
		};

		class btnSelect: BwiGui_RscButton
		{
			idc = 6718;
			text = "Select Accessories";
			x = 28.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			
			onMouseButtonClick = "[ctrlParent (_this select 0), [6705, 6707, 6709, 6711, 6713, 6715, 6716]] call bwi_armory_fnc_selectAccessoriesGUI;";
		};
	};
};