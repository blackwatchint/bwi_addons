class BWI_Staging_COPTents: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Camp COP";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 2;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

	class BWI_Construction_Build {
		class Point1 { distance = 2.86873; angle = 89.1467; timer = 10; label = "Flagpole"; };
		class Point2 { distance = 9.45097; angle = 130.017; timer = 1; };
		class Point6 { distance = 9.53304; angle = 49.77; timer = 1; };
		class Point5 { distance = 12.1089; angle = 70.2992; timer = 1; };
		class Point3 { distance = 12.1907; angle = 109.868; timer = 1; };
		class Point4 { distance = 13.5052; angle = 90.1533; timer = 1; };
	};

	class BWI_Construction_Area {
		class Area1 { distance = 3.33427; angle = 89.6979; radius = 2.75; };
		class Area2 { distance = 7.39065; angle = 90.1457; radius = 6; };
	};

	class BWI_Construction_Composition {
		class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.77182; angle = 88.0044; height = 0; orient = 0; disableDamage = 1; };
		class Object2 { type = "Land_Campfire_F"; distance = 7.36766; angle = 90.4462; height = 0; orient = 0; };
		class Object3 { type = "Land_TentA_F"; distance = 8.82421; angle = 54.9271; height = 0; orient = 90; };
		class Object4 { type = "Land_TentA_F"; distance = 8.87254; angle = 124.224; height = 0; orient = 270; };
		class Object5 { type = "Land_TentA_F"; distance = 11.1573; angle = 72.7724; height = 0; orient = 45; };
		class Object6 { type = "Land_TentA_F"; distance = 11.2119; angle = 107.348; height = 0; orient = 315; };
		class Object7 { type = "Land_TentA_F"; distance = 12.3909; angle = 90.1219; height = 0; orient = 0; };
	};
};
