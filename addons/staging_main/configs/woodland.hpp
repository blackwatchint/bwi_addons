class BWI_Staging_COPSandbagWoodWest: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Sandbag COP (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 4;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.15425; angle = 89.2338; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 7.07269; angle = 161.873; timer = 3; };
        class Point3 { distance = 13.4082; angle = 119.875; timer = 3; };
        class Point4 { distance = 11.7237; angle = 88.864; timer = 2; };
        class Point5 { distance = 13.5113; angle = 59.0911; timer = 3; };
        class Point6 { distance = 7.23192; angle = 17.4159; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.24585; angle = 90.0129; radius = 2.75; };
        class Area2 { distance = 7.2517; angle = 108.057; radius = 4.7; };
        class Area3 { distance = 7.4323; angle = 70.1282; radius = 4.7; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_short_green_F"; distance = 4.62977; angle = 147.547; height = 0; orient = 360; };
        class Object2 { type = "Land_BagFence_01_short_green_F"; distance = 4.74845; angle = 33.0209; height = 0; orient = 539.971; };
        class Object3 { type = "BWI_Staging_OutpostFlag"; distance = 2.91415; angle = 87.4984; height = 0; orient = 360; disableDamage = 1; };
        class Object4 { type = "Land_WoodenCrate_01_F"; distance = 6.56503; angle = 123.49; height = 0; orient = 450; };
        class Object5 { type = "Land_BagFence_01_round_green_F"; distance = 6.5876; angle = 151.115; height = 0; orient = 405.029; };
        class Object6 { type = "Land_BagFence_01_round_green_F"; distance = 6.77877; angle = 28.327; height = 0; orient = 675; };
        class Object7 { type = "Land_CamoNetVar_NATO"; distance = 7.1718; angle = 87.6041; height = 0; orient = 360; };
        class Object8 { type = "Land_CampingTable_F"; distance = 7.25826; angle = 88.19; height = -0.00259209; orient = 720; };
        class Object9 { type = "Land_CampingTable_F"; distance = 7.57799; angle = 73.2085; height = -0.00259209; orient = 719.999; };
        class Object10 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 7.65601; angle = 117.704; height = 0; orient = 450; };
        class Object11 { type = "Land_CampingChair_V2_F"; distance = 8.20296; angle = 92.8421; height = -1.90735e-006; orient = 691.503; };
        class Object12 { type = "Land_BagFence_01_long_green_F"; distance = 8.5222; angle = 138.645; height = 0; orient = 449.842; };
        class Object13 { type = "Land_BagFence_01_long_green_F"; distance = 8.62867; angle = 39.8392; height = 0; orient = 450; };
        class Object14 { type = "Land_BagFence_01_long_green_F"; distance = 10.5045; angle = 127.401; height = 0; orient = 449.996; };
        class Object15 { type = "Land_BagFence_01_long_green_F"; distance = 10.5954; angle = 51.0963; height = 0; orient = 629.971; };
        class Object16 { type = "Land_BagFence_01_long_green_F"; distance = 11.5008; angle = 97.5126; height = 0; orient = 360; };
        class Object17 { type = "Land_BagFence_01_long_green_F"; distance = 11.5233; angle = 83.1457; height = 0; orient = 360; };
        class Object18 { type = "Land_BagFence_01_short_green_F"; distance = 12.0088; angle = 108.756; height = 0; orient = 540.08; };
        class Object19 { type = "Land_BagFence_01_long_green_F"; distance = 12.0184; angle = 71.5897; height = 0; orient = 539.819; };
        class Object20 { type = "Land_BagFence_01_round_green_F"; distance = 12.2096; angle = 118.08; height = 0; orient = 495.025; };
        class Object21 { type = "Land_BagFence_01_round_green_F"; distance = 12.3191; angle = 60.7675; height = 0; orient = 585; };
    };
};


class BWI_Staging_COPSandbagWoodEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Sandbag COP (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 4;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.15425; angle = 89.2338; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 7.07269; angle = 161.873; timer = 3; };
        class Point3 { distance = 13.4082; angle = 119.875; timer = 3; };
        class Point4 { distance = 11.7237; angle = 88.864; timer = 2; };
        class Point5 { distance = 13.5113; angle = 59.0911; timer = 3; };
        class Point6 { distance = 7.23192; angle = 17.4159; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.24585; angle = 90.0129; radius = 2.75; };
        class Area2 { distance = 7.2517; angle = 108.057; radius = 4.7; };
        class Area3 { distance = 7.4323; angle = 70.1282; radius = 4.7; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_short_green_F"; distance = 4.62977; angle = 147.547; height = 0; orient = 360; };
        class Object2 { type = "Land_BagFence_01_short_green_F"; distance = 4.74845; angle = 33.0209; height = 0; orient = 539.971; };
        class Object3 { type = "BWI_Staging_OutpostFlag"; distance = 2.91415; angle = 87.4984; height = 0; orient = 360; disableDamage = 1; };
        class Object4 { type = "Land_WoodenCrate_01_F"; distance = 6.56503; angle = 123.49; height = 0; orient = 450; };
        class Object5 { type = "Land_BagFence_01_round_green_F"; distance = 6.5876; angle = 151.115; height = 0; orient = 405.029; };
        class Object6 { type = "Land_BagFence_01_round_green_F"; distance = 6.77877; angle = 28.327; height = 0; orient = 675; };
        class Object7 { type = "Land_CamoNetVar_EAST"; distance = 7.1718; angle = 87.6041; height = 0; orient = 360; };
        class Object8 { type = "Land_CampingTable_F"; distance = 7.25826; angle = 88.19; height = -0.00259209; orient = 720; };
        class Object9 { type = "Land_CampingTable_F"; distance = 7.57799; angle = 73.2085; height = -0.00259209; orient = 719.999; };
        class Object10 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 7.65601; angle = 117.704; height = 0; orient = 450; };
        class Object11 { type = "Land_CampingChair_V2_F"; distance = 8.20296; angle = 92.8421; height = -1.90735e-006; orient = 691.503; };
        class Object12 { type = "Land_BagFence_01_long_green_F"; distance = 8.5222; angle = 138.645; height = 0; orient = 449.842; };
        class Object13 { type = "Land_BagFence_01_long_green_F"; distance = 8.62867; angle = 39.8392; height = 0; orient = 450; };
        class Object14 { type = "Land_BagFence_01_long_green_F"; distance = 10.5045; angle = 127.401; height = 0; orient = 449.996; };
        class Object15 { type = "Land_BagFence_01_long_green_F"; distance = 10.5954; angle = 51.0963; height = 0; orient = 629.971; };
        class Object16 { type = "Land_BagFence_01_long_green_F"; distance = 11.5008; angle = 97.5126; height = 0; orient = 360; };
        class Object17 { type = "Land_BagFence_01_long_green_F"; distance = 11.5233; angle = 83.1457; height = 0; orient = 360; };
        class Object18 { type = "Land_BagFence_01_short_green_F"; distance = 12.0088; angle = 108.756; height = 0; orient = 540.08; };
        class Object19 { type = "Land_BagFence_01_long_green_F"; distance = 12.0184; angle = 71.5897; height = 0; orient = 539.819; };
        class Object20 { type = "Land_BagFence_01_round_green_F"; distance = 12.2096; angle = 118.08; height = 0; orient = 495.025; };
        class Object21 { type = "Land_BagFence_01_round_green_F"; distance = 12.3191; angle = 60.7675; height = 0; orient = 585; };
    };
};


class BWI_Staging_COPSandbagTallWoodWest: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Sandbag COP (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 5;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.34561; angle = 93.3714; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 7.45925; angle = 161.171; timer = 3; };
        class Point3 { distance = 13.7576; angle = 120.667; timer = 3; };
        class Point5 { distance = 13.521; angle = 60.7723; timer = 3; };
        class Point4 { distance = 11.9289; angle = 90.5089; timer = 2; };
        class Point6 { distance = 6.97742; angle = 19.8704; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.36364; angle = 90.4782; radius = 2.75; };
        class Area2 { distance = 7.58659; angle = 70.7598; radius = 4.7; };
        class Area3 { distance = 7.61525; angle = 112.373; radius = 4.7; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_end_green_F"; distance = 4.59741; angle = 37.4632; height = 0; orient = 179.998; };
        class Object2 { type = "Land_BagFence_01_end_green_F"; distance = 4.64877; angle = 144.387; height = 0; orient = 0.000189401; };
        class Object3 { type = "BWI_Staging_OutpostFlag"; distance = 3.17776; angle = 90.2113; height = 0; orient = 0.000189401; disableDamage = 1; };
        class Object4 { type = "Land_BagFence_01_end_green_F"; distance = 5.16357; angle = 33.1483; height = 0.682866; orient = 179.998; };
        class Object5 { type = "Land_BagFence_01_end_green_F"; distance = 5.31282; angle = 148.923; height = 0.698795; orient = 0.000189401; };
        class Object6 { type = "Land_BagFence_01_short_green_F"; distance = 5.62735; angle = 151.249; height = 0; orient = 0.000189401; };
        class Object7 { type = "Land_BagFence_01_short_green_F"; distance = 5.63003; angle = 29.5954; height = 0; orient = 359.971; };
        class Object8 { type = "Land_BagFence_01_short_green_F"; distance = 6.23722; angle = 26.7526; height = 0.682866; orient = 359.971; };
        class Object9 { type = "Land_BagFence_01_short_green_F"; distance = 6.32995; angle = 154.231; height = 0.695; orient = 0.000189401; };
        class Object10 { type = "Land_BagFence_01_corner_green_F"; distance = 6.8217; angle = 27.4537; height = 0; orient = 90.0002; };
        class Object11 { type = "Land_CampingTable_F"; distance = 6.98859; angle = 63.4341; height = -0.00259209; orient = 90.0002; };
        class Object12 { type = "Land_BagFence_01_corner_green_F"; distance = 7.02092; angle = 154.904; height = 0; orient = 180.029; };
        class Object13 { type = "Land_CamoNetVar_NATO"; distance = 7.37266; angle = 90.2998; height = 0; orient = 0.000189401; };
        class Object14 { type = "Land_BagFence_01_short_green_F"; distance = 7.68451; angle = 34.8296; height = 0; orient = 90.0002; };
        class Object15 { type = "Land_BagFence_01_long_green_F"; distance = 7.71131; angle = 35.3757; height = 0.699098; orient = 269.972; };
        class Object16 { type = "Land_BagFence_01_long_green_F"; distance = 7.86145; angle = 148.376; height = 0.693268; orient = 89.8417; };
        class Object17 { type = "Land_BagFence_01_long_green_F"; distance = 8.20295; angle = 144.766; height = 0; orient = 89.8417; };
        class Object18 { type = "Land_CampingTable_F"; distance = 8.78545; angle = 69.1601; height = -0.00259161; orient = 90.0001; };
        class Object19 { type = "Land_BagFence_01_long_green_F"; distance = 9.25356; angle = 46.9597; height = 0; orient = 269.972; };
        class Object20 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 9.22602; angle = 114.585; height = 0; orient = 90.0002; };
        class Object21 { type = "Land_BagFence_01_long_green_F"; distance = 9.59317; angle = 48.9438; height = 0.699098; orient = 89.9715; };
        class Object22 { type = "Land_BagFence_01_long_green_F"; distance = 9.76353; angle = 133.316; height = 0.693268; orient = 269.842; };
        class Object23 { type = "Land_CampingChair_V2_F"; distance = 10.0695; angle = 64.0053; height = 4.76837e-006; orient = 61.5073; };
        class Object24 { type = "Land_BagFence_01_long_green_F"; distance = 10.1401; angle = 131.498; height = 0; orient = 89.9959; };
        class Object25 { type = "Land_BagFence_01_long_green_F"; distance = 11.5018; angle = 56.8064; height = 0; orient = 269.817; };
        class Object26 { type = "Land_BagFence_01_long_green_F"; distance = 11.7147; angle = 83.8795; height = 0; orient = 0.000189401; };
        class Object27 { type = "Land_BagFence_01_long_green_F"; distance = 11.7348; angle = 98.1018; height = 0; orient = 0.000189401; };
        class Object28 { type = "Land_BagFence_01_short_green_F"; distance = 12.0177; angle = 123.948; height = 0; orient = 270.026; };
        class Object29 { type = "Land_BagFence_01_long_green_F"; distance = 11.9988; angle = 58.3758; height = 0.699098; orient = 269.972; };
        class Object30 { type = "Land_BagFence_01_long_green_F"; distance = 12.0292; angle = 123.921; height = 0.693268; orient = 269.842; };
        class Object31 { type = "Land_BagFence_01_long_green_F"; distance = 12.2677; angle = 71.1583; height = 0; orient = 179.819; };
        class Object32 { type = "Land_BagFence_01_end_green_F"; distance = 12.3558; angle = 70.2858; height = 0.682866; orient = 179.998; };
        class Object33 { type = "Land_BagFence_01_long_green_F"; distance = 12.4796; angle = 111.908; height = 0; orient = 180.081; };
        class Object34 { type = "Land_BagFence_01_end_green_F"; distance = 12.5192; angle = 112.463; height = 0.698795; orient = 0.000189401; };
        class Object35 { type = "Land_BagFence_01_corner_green_F"; distance = 12.8431; angle = 62.3862; height = 0; orient = 0.000189401; };
        class Object36 { type = "Land_BagFence_01_short_green_F"; distance = 12.8156; angle = 65.0089; height = 0.682866; orient = 359.971; };
        class Object37 { type = "Land_BagFence_01_corner_green_F"; distance = 12.9371; angle = 119.922; height = 0; orient = 270.026; };
        class Object38 { type = "Land_BagFence_01_short_green_F"; distance = 13.0107; angle = 117.133; height = 0.695; orient = 180; };
    };
};


class BWI_Staging_COPSandbagTallWoodEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Sandbag COP (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 5;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.34561; angle = 93.3714; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 7.45925; angle = 161.171; timer = 3; };
        class Point3 { distance = 13.7576; angle = 120.667; timer = 3; };
        class Point5 { distance = 13.521; angle = 60.7723; timer = 3; };
        class Point4 { distance = 11.9289; angle = 90.5089; timer = 2; };
        class Point6 { distance = 6.97742; angle = 19.8704; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.36364; angle = 90.4782; radius = 2.75; };
        class Area2 { distance = 7.58659; angle = 70.7598; radius = 4.7; };
        class Area3 { distance = 7.61525; angle = 112.373; radius = 4.7; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_end_green_F"; distance = 4.59741; angle = 37.4632; height = 0; orient = 179.998; };
        class Object2 { type = "Land_BagFence_01_end_green_F"; distance = 4.64877; angle = 144.387; height = 0; orient = 0.000189401; };
        class Object3 { type = "BWI_Staging_OutpostFlag"; distance = 3.17776; angle = 90.2113; height = 0; orient = 0.000189401; disableDamage = 1; };
        class Object4 { type = "Land_BagFence_01_end_green_F"; distance = 5.16357; angle = 33.1483; height = 0.682866; orient = 179.998; };
        class Object5 { type = "Land_BagFence_01_end_green_F"; distance = 5.31282; angle = 148.923; height = 0.698795; orient = 0.000189401; };
        class Object6 { type = "Land_BagFence_01_short_green_F"; distance = 5.62735; angle = 151.249; height = 0; orient = 0.000189401; };
        class Object7 { type = "Land_BagFence_01_short_green_F"; distance = 5.63003; angle = 29.5954; height = 0; orient = 359.971; };
        class Object8 { type = "Land_BagFence_01_short_green_F"; distance = 6.23722; angle = 26.7526; height = 0.682866; orient = 359.971; };
        class Object9 { type = "Land_BagFence_01_short_green_F"; distance = 6.32995; angle = 154.231; height = 0.695; orient = 0.000189401; };
        class Object10 { type = "Land_BagFence_01_corner_green_F"; distance = 6.8217; angle = 27.4537; height = 0; orient = 90.0002; };
        class Object11 { type = "Land_CampingTable_F"; distance = 6.98859; angle = 63.4341; height = -0.00259209; orient = 90.0002; };
        class Object12 { type = "Land_BagFence_01_corner_green_F"; distance = 7.02092; angle = 154.904; height = 0; orient = 180.029; };
        class Object13 { type = "Land_CamoNetVar_EAST"; distance = 7.37266; angle = 90.2998; height = 0; orient = 0.000189401; };
        class Object14 { type = "Land_BagFence_01_short_green_F"; distance = 7.68451; angle = 34.8296; height = 0; orient = 90.0002; };
        class Object15 { type = "Land_BagFence_01_long_green_F"; distance = 7.71131; angle = 35.3757; height = 0.699098; orient = 269.972; };
        class Object16 { type = "Land_BagFence_01_long_green_F"; distance = 7.86145; angle = 148.376; height = 0.693268; orient = 89.8417; };
        class Object17 { type = "Land_BagFence_01_long_green_F"; distance = 8.20295; angle = 144.766; height = 0; orient = 89.8417; };
        class Object18 { type = "Land_CampingTable_F"; distance = 8.78545; angle = 69.1601; height = -0.00259161; orient = 90.0001; };
        class Object19 { type = "Land_BagFence_01_long_green_F"; distance = 9.25356; angle = 46.9597; height = 0; orient = 269.972; };
        class Object20 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 9.22602; angle = 114.585; height = 0; orient = 90.0002; };
        class Object21 { type = "Land_BagFence_01_long_green_F"; distance = 9.59317; angle = 48.9438; height = 0.699098; orient = 89.9715; };
        class Object22 { type = "Land_BagFence_01_long_green_F"; distance = 9.76353; angle = 133.316; height = 0.693268; orient = 269.842; };
        class Object23 { type = "Land_CampingChair_V2_F"; distance = 10.0695; angle = 64.0053; height = 4.76837e-006; orient = 61.5073; };
        class Object24 { type = "Land_BagFence_01_long_green_F"; distance = 10.1401; angle = 131.498; height = 0; orient = 89.9959; };
        class Object25 { type = "Land_BagFence_01_long_green_F"; distance = 11.5018; angle = 56.8064; height = 0; orient = 269.817; };
        class Object26 { type = "Land_BagFence_01_long_green_F"; distance = 11.7147; angle = 83.8795; height = 0; orient = 0.000189401; };
        class Object27 { type = "Land_BagFence_01_long_green_F"; distance = 11.7348; angle = 98.1018; height = 0; orient = 0.000189401; };
        class Object28 { type = "Land_BagFence_01_short_green_F"; distance = 12.0177; angle = 123.948; height = 0; orient = 270.026; };
        class Object29 { type = "Land_BagFence_01_long_green_F"; distance = 11.9988; angle = 58.3758; height = 0.699098; orient = 269.972; };
        class Object30 { type = "Land_BagFence_01_long_green_F"; distance = 12.0292; angle = 123.921; height = 0.693268; orient = 269.842; };
        class Object31 { type = "Land_BagFence_01_long_green_F"; distance = 12.2677; angle = 71.1583; height = 0; orient = 179.819; };
        class Object32 { type = "Land_BagFence_01_end_green_F"; distance = 12.3558; angle = 70.2858; height = 0.682866; orient = 179.998; };
        class Object33 { type = "Land_BagFence_01_long_green_F"; distance = 12.4796; angle = 111.908; height = 0; orient = 180.081; };
        class Object34 { type = "Land_BagFence_01_end_green_F"; distance = 12.5192; angle = 112.463; height = 0.698795; orient = 0.000189401; };
        class Object35 { type = "Land_BagFence_01_corner_green_F"; distance = 12.8431; angle = 62.3862; height = 0; orient = 0.000189401; };
        class Object36 { type = "Land_BagFence_01_short_green_F"; distance = 12.8156; angle = 65.0089; height = 0.682866; orient = 359.971; };
        class Object37 { type = "Land_BagFence_01_corner_green_F"; distance = 12.9371; angle = 119.922; height = 0; orient = 270.026; };
        class Object38 { type = "Land_BagFence_01_short_green_F"; distance = 13.0107; angle = 117.133; height = 0.695; orient = 180; };
    };
};


class BWI_Staging_COPBarricadeWoodEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Barricade COP (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 5;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.38895; angle = 90.8666; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 7.56424; angle = 160.291; timer = 6; };
        class Point3 { distance = 14.0447; angle = 120.357; timer = 6; };
        class Point4 { distance = 12.3186; angle = 89.3811; timer = 4; };
        class Point5 { distance = 14.1156; angle = 60.3117; timer = 6; };
        class Point6 { distance = 7.51022; angle = 20.069; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.41145; angle = 88.7534; radius = 2.75; };
        class Area2 { distance = 7.88634; angle = 111.615; radius = 4.8; };
        class Area3 { distance = 8.01679; angle = 69.3298; radius = 4.8; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.2261; angle = 88.4042; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 4.9695; angle = 39.758; height = 0; orient = 540; };
        class Object3 { type = "Land_SandbagBarricade_01_half_F"; distance = 5.00817; angle = 140.876; height = 0; orient = 540; };
        class Object4 { type = "Land_SandbagBarricade_01_F"; distance = 6.54689; angle = 29.0081; height = 0; orient = 540; };
        class Object5 { type = "Land_SandbagBarricade_01_F"; distance = 6.59825; angle = 151.352; height = 0; orient = 540; };
        class Object6 { type = "Land_CamoNetVar_EAST"; distance = 7.42043; angle = 90.6711; height = 0; orient = 360; };
        class Object7 { type = "Land_CampingChair_V2_F"; distance = 7.56078; angle = 70.2322; height = -1.90735e-006; orient = 616.505; };
        class Object8 { type = "Land_CampingTable_F"; distance = 7.67059; angle = 62.1944; height = -0.00259256; orient = 450.002; };
        class Object9 { type = "Land_SandbagBarricade_01_F"; distance = 7.94153; angle = 34.7004; height = 0; orient = 450; };
        class Object10 { type = "Land_SandbagBarricade_01_F"; distance = 7.99026; angle = 145.531; height = 0; orient = 630; };
        class Object11 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 9.13223; angle = 112.413; height = 0; orient = 450; };
        class Object12 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.16569; angle = 44.5942; height = 0; orient = 450; };
        class Object13 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.21558; angle = 135.643; height = 0; orient = 630; };
        class Object14 { type = "Land_CampingTable_F"; distance = 9.44859; angle = 67.7472; height = -0.00259256; orient = 450.002; };
        class Object15 { type = "Land_SandbagBarricade_01_half_F"; distance = 10.5907; angle = 51.9949; height = 0; orient = 450; };
        class Object16 { type = "Land_SandbagBarricade_01_half_F"; distance = 10.6419; angle = 128.295; height = 0; orient = 630; };
        class Object17 { type = "Land_CampingChair_V2_F"; distance = 10.7495; angle = 63.0834; height = 4.76837e-006; orient = 421.507; };
        class Object18 { type = "Land_SandbagBarricade_01_F"; distance = 11.6192; angle = 90.1589; height = 0; orient = 360; };
        class Object19 { type = "Land_SandbagBarricade_01_half_F"; distance = 11.7738; angle = 80.7691; height = 0; orient = 360; };
        class Object20 { type = "Land_SandbagBarricade_01_half_F"; distance = 11.7842; angle = 99.509; height = 0; orient = 360; };
        class Object21 { type = "Land_SandbagBarricade_01_F"; distance = 12.1605; angle = 57.5772; height = 0; orient = 450; };
        class Object22 { type = "Land_SandbagBarricade_01_half_F"; distance = 12.2251; angle = 71.8806; height = 0; orient = 360; };
        class Object23 { type = "Land_SandbagBarricade_01_F"; distance = 12.2037; angle = 122.724; height = 0; orient = 630; };
        class Object24 { type = "Land_SandbagBarricade_01_half_F"; distance = 12.2514; angle = 108.444; height = 0; orient = 360; };
        class Object25 { type = "Land_SandbagBarricade_01_F"; distance = 12.9492; angle = 63.7717; height = 0; orient = 360; };
        class Object26 { type = "Land_SandbagBarricade_01_F"; distance = 12.9862; angle = 116.474; height = 0; orient = 360; };
    };
};


class BWI_Staging_COPBarricadeTallWoodEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Barricade COP (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 6;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.38968; angle = 90.8664; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 7.53559; angle = 160.238; timer = 7; };
        class Point3 { distance = 14.0944; angle = 120.469; timer = 8; };
        class Point4 { distance = 12.275; angle = 89.2296; timer = 4; };
        class Point5 { distance = 14.0987; angle = 60.4505; timer = 8; };
        class Point6 { distance = 7.54986; angle = 19.8764; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.41218; angle = 88.7537; radius = 2.75; };
        class Area2 { distance = 7.93437; angle = 110.904; radius = 4.8; };
        class Area3 { distance = 7.98708; angle = 69.9687; radius = 4.8; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.22684; angle = 88.4045; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 4.96996; angle = 39.7645; height = 0; orient = 540; };
        class Object3 { type = "Land_SandbagBarricade_01_half_F"; distance = 5.00863; angle = 140.87; height = 0; orient = 540; };
        class Object4 { type = "Land_SandbagBarricade_01_F"; distance = 6.54724; angle = 29.0137; height = 0; orient = 540; };
        class Object5 { type = "Land_SandbagBarricade_01_F"; distance = 6.5986; angle = 151.347; height = 0; orient = 540; };
        class Object6 { type = "Land_CamoNetVar_EAST"; distance = 7.42044; angle = 90.6786; height = 0; orient = 360; };
        class Object7 { type = "Land_BagFence_01_corner_green_F"; distance = 7.63962; angle = 31.7538; height = 0; orient = 450; };
        class Object8 { type = "Land_CampingTable_F"; distance = 7.67124; angle = 62.197; height = -0.00259256; orient = 450.002; };
        class Object9 { type = "Land_CampingChair_V2_F"; distance = 7.7861; angle = 54.8209; height = -1.90735e-006; orient = 466.503; };
        class Object10 { type = "Land_SandbagBarricade_01_F"; distance = 7.94201; angle = 34.7023; height = 0; orient = 450; };
        class Object11 { type = "Land_SandbagBarricade_01_F"; distance = 7.99068; angle = 145.526; height = 0; orient = 630; };
        class Object12 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 9.13291; angle = 112.411; height = 0; orient = 450; };
        class Object13 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.16672; angle = 44.5964; height = 0; orient = 450; };
        class Object14 { type = "Land_SandbagBarricade_01_F"; distance = 9.21609; angle = 135.638; height = 0; orient = 630; };
        class Object15 { type = "Land_CampingTable_F"; distance = 9.44926; angle = 67.7489; height = -0.00259352; orient = 450.001; };
        class Object16 { type = "Land_SandbagBarricade_01_F"; distance = 10.5914; angle = 51.9982; height = 0; orient = 450; };
        class Object17 { type = "Land_SandbagBarricade_01_hole_F"; distance = 10.6428; angle = 128.293; height = 0; orient = 630; };
        class Object18 { type = "Land_CampingChair_V2_F"; distance = 10.7501; angle = 63.0852; height = 3.33786e-006; orient = 421.508; };
        class Object19 { type = "Land_SandbagBarricade_01_F"; distance = 11.6199; angle = 90.1589; height = 0; orient = 360; };
        class Object20 { type = "Land_SandbagBarricade_01_half_F"; distance = 11.7745; angle = 80.7697; height = 0; orient = 360; };
        class Object21 { type = "Land_SandbagBarricade_01_half_F"; distance = 11.785; angle = 99.5084; height = 0; orient = 360; };
        class Object22 { type = "Land_SandbagBarricade_01_F"; distance = 12.1613; angle = 57.5823; height = 0; orient = 450; };
        class Object23 { type = "Land_SandbagBarricade_01_F"; distance = 12.2043; angle = 122.722; height = 0; orient = 630; };
        class Object24 { type = "Land_SandbagBarricade_01_hole_F"; distance = 12.226; angle = 71.8831; height = 0; orient = 360; };
        class Object25 { type = "Land_SandbagBarricade_01_hole_F"; distance = 12.2523; angle = 108.442; height = 0; orient = 360; };
        class Object26 { type = "Land_SandbagBarricade_01_F"; distance = 12.95; angle = 63.7721; height = 0; orient = 360; };
        class Object27 { type = "Land_SandbagBarricade_01_F"; distance = 12.9869; angle = 116.472; height = 0; orient = 360; };
    };
};


class BWI_Staging_COPHescoWoodWest: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "H-Barrier COP (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 6;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.24521; angle = 91.5577; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 8.51901; angle = 164.68; timer = 7; };
        class Point3 { distance = 13.6505; angle = 127.894; timer = 7; };
        class Point4 { distance = 11.0356; angle = 92.9522; timer = 4; };
        class Point5 { distance = 13.5511; angle = 54.5077; timer = 7; };
        class Point6 { distance = 8.556; angle = 16.455; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.41505; angle = 90.6963; radius = 2.75; };
        class Area2 { distance = 7.9177; angle = 120.173; radius = 4.8; };
        class Area3 { distance = 7.99329; angle = 59.7172; radius = 4.8; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.22911; angle = 90.4592; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.50114; angle = 144.587; height = 0; orient = 360; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.56336; angle = 35.0633; height = 0; orient = 360; };
        class Object4 { type = "Land_CampingTable_F"; distance = 6.95943; angle = 60.1783; height = -0.00259161; orient = 450; };
        class Object5 { type = "Land_CamoNetVar_NATO"; distance = 7.43905; angle = 93.6655; height = 0; orient = 360; };
        class Object6 { type = "Land_WoodenCrate_01_F"; distance = 7.60886; angle = 118.757; height = -4.76837e-007; orient = 360; };
        class Object7 { type = "Land_CampingTable_F"; distance = 7.97018; angle = 68.7539; height = -0.00259161; orient = 719.999; };
        class Object8 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 8.35139; angle = 126.564; height = 0; orient = 450; };
        class Object9 { type = "Land_CampingChair_V2_F"; distance = 8.73503; angle = 56.095; height = -1.90735e-006; orient = 451.987; };
        class Object10 { type = "Land_CampingChair_V2_F"; distance = 9.37077; angle = 65.4722; height = -1.90735e-006; orient = 391.501; };
        class Object11 { type = "Land_HBarrier_01_big_4_green_F"; distance = 9.70534; angle = 137.72; height = 0; orient = 450; };
        class Object12 { type = "Land_HBarrier_01_big_4_green_F"; distance = 9.74982; angle = 43.475; height = 0; orient = 450; };
        class Object13 { type = "Land_HBarrier_01_line_3_green_F"; distance = 10.2386; angle = 97.5056; height = 0; orient = 360; };
        class Object14 { type = "Land_HBarrier_01_line_5_green_F"; distance = 10.618; angle = 72.8341; height = 0; orient = 360.328; };
        class Object15 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.1242; angle = 114.811; height = 0; orient = 360; };
    };
};


class BWI_Staging_COPHescoWoodEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "H-Barrier COP (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.24521; angle = 91.5577; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 8.51901; angle = 164.68; timer = 7; };
        class Point3 { distance = 13.6505; angle = 127.894; timer = 7; };
        class Point4 { distance = 11.0356; angle = 92.9522; timer = 4; };
        class Point5 { distance = 13.5511; angle = 54.5077; timer = 7; };
        class Point6 { distance = 8.556; angle = 16.455; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.41505; angle = 90.6963; radius = 2.75; };
        class Area2 { distance = 7.9177; angle = 120.173; radius = 4.8; };
        class Area3 { distance = 7.99329; angle = 59.7172; radius = 4.8; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.22911; angle = 90.4592; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.50114; angle = 144.587; height = 0; orient = 360; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.56336; angle = 35.0633; height = 0; orient = 360; };
        class Object4 { type = "Land_CampingTable_F"; distance = 6.95943; angle = 60.1783; height = -0.00259161; orient = 450; };
        class Object5 { type = "Land_CamoNetVar_EAST"; distance = 7.43905; angle = 93.6655; height = 0; orient = 360; };
        class Object6 { type = "Land_WoodenCrate_01_F"; distance = 7.60886; angle = 118.757; height = -4.76837e-007; orient = 360; };
        class Object7 { type = "Land_CampingTable_F"; distance = 7.97018; angle = 68.7539; height = -0.00259161; orient = 719.999; };
        class Object8 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 8.35139; angle = 126.564; height = 0; orient = 450; };
        class Object9 { type = "Land_CampingChair_V2_F"; distance = 8.73503; angle = 56.095; height = -1.90735e-006; orient = 451.987; };
        class Object10 { type = "Land_CampingChair_V2_F"; distance = 9.37077; angle = 65.4722; height = -1.90735e-006; orient = 391.501; };
        class Object11 { type = "Land_HBarrier_01_big_4_green_F"; distance = 9.70534; angle = 137.72; height = 0; orient = 450; };
        class Object12 { type = "Land_HBarrier_01_big_4_green_F"; distance = 9.74982; angle = 43.475; height = 0; orient = 450; };
        class Object13 { type = "Land_HBarrier_01_line_3_green_F"; distance = 10.2386; angle = 97.5056; height = 0; orient = 360; };
        class Object14 { type = "Land_HBarrier_01_line_5_green_F"; distance = 10.618; angle = 72.8341; height = 0; orient = 360.328; };
        class Object15 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.1242; angle = 114.811; height = 0; orient = 360; };
    };
};


class BWI_Staging_COPHescoHeavyWoodWest: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Heavy H-Barrier COP (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 7;
    bwi_construction_tier = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.2457; angle = 91.5574; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 8.83812; angle = 166.783; timer = 7; };
        class Point3 { distance = 15.8821; angle = 123.67; timer = 8; };
        class Point4 { distance = 13.5483; angle = 90.8239; timer = 8; };
        class Point5 { distance = 15.794; angle = 57.7318; timer = 8; };
        class Point6 { distance = 9.04397; angle = 14.8308; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.41554; angle = 90.6962; radius = 2.75; };
        class Area2 { distance = 8.56762; angle = 67.7675; radius = 5.5; };
        class Area3 { distance = 8.60446; angle = 114.412; radius = 5.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.2296; angle = 90.4591; height = 0; orient = 0.000392844; disableDamage = 1; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.82131; angle = 146.792; height = 0; orient = 0.000392844; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.91514; angle = 32.7108; height = 0; orient = 0.000392844; };
        class Object4 { type = "Land_CamoNetVar_NATO"; distance = 7.4245; angle = 90.4032; height = 0; orient = 0.000392844; };
        class Object5 { type = "Land_CampingTable_F"; distance = 9.25518; angle = 65.1852; height = -0.00259209; orient = 90.0008; };
        class Object6 { type = "Land_CampingTable_F"; distance = 9.30648; angle = 74.5171; height = -0.00259209; orient = 0.000847211; };
        class Object7 { type = "Land_CampingChair_V2_F"; distance = 9.41116; angle = 56.4039; height = 3.33786e-006; orient = 91.9921; };
        class Object8 { type = "Land_HBarrier_01_big_4_green_F"; distance = 9.99554; angle = 139.211; height = 0; orient = 90.0004; };
        class Object9 { type = "Land_HBarrier_01_big_4_green_F"; distance = 10.0616; angle = 41.8181; height = 0; orient = 90.0004; };
        class Object10 { type = "Land_CampingChair_V2_F"; distance = 10.5898; angle = 60.7627; height = 4.76837e-006; orient = 61.5028; };
        class Object11 { type = "Land_WoodenCrate_01_F"; distance = 10.7047; angle = 114.109; height = 0; orient = 0.000450908; };
        class Object12 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 11.3436; angle = 119.283; height = 0; orient = 90.0004; };
        class Object13 { type = "Land_HBarrier_01_big_4_green_F"; distance = 12.8777; angle = 71.6087; height = 0; orient = 2.96293; };
        class Object14 { type = "Land_HBarrier_01_big_4_green_F"; distance = 12.9407; angle = 110.259; height = 0; orient = 0.000392844; };
    };
};


class BWI_Staging_COPHescoHeavyWoodEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Heavy H-Barrier COP (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionCOPKits";
	ace_cargo_size = 7;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_5.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.2457; angle = 91.5574; timer = 10; label = "Flagpole"; };
        class Point2 { distance = 8.83812; angle = 166.783; timer = 7; };
        class Point3 { distance = 15.8821; angle = 123.67; timer = 8; };
        class Point4 { distance = 13.5483; angle = 90.8239; timer = 8; };
        class Point5 { distance = 15.794; angle = 57.7318; timer = 8; };
        class Point6 { distance = 9.04397; angle = 14.8308; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.41554; angle = 90.6962; radius = 2.75; };
        class Area2 { distance = 8.56762; angle = 67.7675; radius = 5.5; };
        class Area3 { distance = 8.60446; angle = 114.412; radius = 5.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "BWI_Staging_OutpostFlag"; distance = 3.2296; angle = 90.4591; height = 0; orient = 0.000392844; disableDamage = 1; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.82131; angle = 146.792; height = 0; orient = 0.000392844; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 5.91514; angle = 32.7108; height = 0; orient = 0.000392844; };
        class Object4 { type = "Land_CamoNetVar_EAST"; distance = 7.4245; angle = 90.4032; height = 0; orient = 0.000392844; };
        class Object5 { type = "Land_CampingTable_F"; distance = 9.25518; angle = 65.1852; height = -0.00259209; orient = 90.0008; };
        class Object6 { type = "Land_CampingTable_F"; distance = 9.30648; angle = 74.5171; height = -0.00259209; orient = 0.000847211; };
        class Object7 { type = "Land_CampingChair_V2_F"; distance = 9.41116; angle = 56.4039; height = 3.33786e-006; orient = 91.9921; };
        class Object8 { type = "Land_HBarrier_01_big_4_green_F"; distance = 9.99554; angle = 139.211; height = 0; orient = 90.0004; };
        class Object9 { type = "Land_HBarrier_01_big_4_green_F"; distance = 10.0616; angle = 41.8181; height = 0; orient = 90.0004; };
        class Object10 { type = "Land_CampingChair_V2_F"; distance = 10.5898; angle = 60.7627; height = 4.76837e-006; orient = 61.5028; };
        class Object11 { type = "Land_WoodenCrate_01_F"; distance = 10.7047; angle = 114.109; height = 0; orient = 0.000450908; };
        class Object12 { type = "Land_WoodenCrate_01_stack_x5_F"; distance = 11.3436; angle = 119.283; height = 0; orient = 90.0004; };
        class Object13 { type = "Land_HBarrier_01_big_4_green_F"; distance = 12.8777; angle = 71.6087; height = 0; orient = 2.96293; };
        class Object14 { type = "Land_HBarrier_01_big_4_green_F"; distance = 12.9407; angle = 110.259; height = 0; orient = 0.000392844; };
    };
};