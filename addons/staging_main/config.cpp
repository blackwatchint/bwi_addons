class CfgPatches 
{
    class bwi_staging_main
    {
        requiredVersion = 1;
        authors[] = { "Fourjays" };
        author = "Black Watch International";
        url = "http://blackwatch-int.com";
        version = 1.1;
        versionStr = "1.1.0";
        versionAr[] = {1,1,0};
        requiredAddons[] = {
            "bwi_staging",
            "bwi_construction_main"
        };
        units[] = {
            // Deployable COPs - Neutral
            "BWI_Staging_COPTents",

            // Deployable COPs - Desert
            "BWI_Staging_COPSandbagDesertWest",
            "BWI_Staging_COPSandbagDesertEast",
            "BWI_Staging_COPSandbagTallDesertWest",
            "BWI_Staging_COPSandbagTallDesertEast",
            "BWI_Staging_COPBarricadeDesertEast",
            "BWI_Staging_COPBarricadeTallDesertEast",
            "BWI_Staging_COPHescoDesertWest",
            "BWI_Staging_COPHescoDesertEast",
            "BWI_Staging_COPHescoHeavyDesertWest",
            "BWI_Staging_COPHescoHeavyDesertEast",

            // Deployable COPs - Woodland
            "BWI_Staging_COPSandbagWoodWest",
            "BWI_Staging_COPSandbagWoodEast",
            "BWI_Staging_COPSandbagTallWoodWest",
            "BWI_Staging_COPSandbagTallWoodEast",
            "BWI_Staging_COPBarricadeWoodEast",
            "BWI_Staging_COPBarricadeTallWoodEast",
            "BWI_Staging_COPHescoWoodWest",
            "BWI_Staging_COPHescoWoodEast",
            "BWI_Staging_COPHescoHeavyWoodWest",
            "BWI_Staging_COPHescoHeavyWoodEast"
        };
    };
};

class CfgEditorSubcategories {
    #include <cfgEditorSubcategories.hpp>
};

class CfgVehicles 
{
	#include <cfgVehicles.hpp>
};