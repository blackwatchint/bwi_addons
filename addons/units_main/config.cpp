class CfgPatches
{
    class bwi_units_main
	{
        requiredVersion = 1;
        authors[] = {"Andrew"};
        authorURL = "http://blackwatch-int.com";
        author = "Black Watch International";
        version = 2.0;
		versionStr = "2.0.0";
		versionAr[] = {2,0,0};
        requiredAddons[] =
		{
			"rhsgref_c_a29",
			"rhsgref_a29",
			"rhsusf_c_melb",
			"rhsusf_melb"
        };
        units[] = 
		{
			"BWI_A29_ION",
			"BWI_MELB_OH6M_ION",
			"BWI_MELB_MH6M_ION",
			"BWI_MELB_AH6M_ION"
		};
    };
};

class CfgVehicles
{
    #include <cfgVehicles.hpp>
};