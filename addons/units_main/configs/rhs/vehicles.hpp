// A-29 Super Tucano
class RHSGREF_A29_Base: Plane_Fighter_03_base_F
{
	class textureSources: textureSources
	{
		class ION 
		{
			author = "Andrew";
			displayName = "ION";
			textures[] =
			{
				"\bwi_units_main\data\st_fuselage_ion_co.paa",
				"\bwi_units_main\data\st_wingsandstabs_ion_co.paa",
				"\bwi_units_main\data\st_everythingelse_ion_co.paa"
			};
		};
	};
};
class BWI_A29_ION: RHSGREF_A29_Base 
{
	scope = 2;
	scopeCurator = 1;
	author = "Retexture by Andrew, vehicle by RHS";
	displayName = "A-29B Super Tucano";
	side = 1;
	faction = "rhs_faction_usaf";
	crew = "rhsusf_airforce_pilot";
	availableForSupportTypes[]=
	{
		"CAS_Bombing"
	};
	textureList[]=
	{
		"ION",
		1
	};
};


// Mission Enhanced Little Bird (MELB)
class RHS_MELB_base: Helicopter_Base_H
{
	class textureSources: textureSources
	{
		class USArmy
		{
			author = "RHS";
			displayName = "Army";
			textures[] =
			{
				"rhsusf\addons\rhsusf_melb\data\melb_ext_co.paa",
				"rhsusf\addons\rhsusf_melb\data\decals\SN\blank_ca.paa"
			};
		};
		class ION
		{
			author = "Andrew";
			displayName = "ION";
			textures[] =
			{
				"\bwi_units_main\data\melb_ext_ion_co.paa",
				"rhsusf\addons\rhsusf_melb\data\decals\SN\blank_ca.paa"
			};
		};
	};
};