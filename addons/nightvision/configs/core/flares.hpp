class FlareCore;
class FlareBase;


class Flare_82mm_AMOS_White: FlareCore
{
	timeToLive = 80;
	brightness = 150;
	intensity  = 1000000;
};

class F_40mm_White: FlareBase
{
	timeToLive = 80;
	brightness = 150;
	intensity  = 1000000;
};

class F_20mm_White: FlareBase
{
	timeToLive = 80;
	brightness = 150;
	intensity  = 1000000;
};

class F_Signal_Green: FlareBase
{
	timeToLive = 80;
	brightness = 150;
	intensity  = 1000000;
};