class ItemCore;
class InventoryOpticsItem_Base_F;  


class optic_Arco: ItemCore
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class ARCO2scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class optic_Hamr: ItemCore
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class Hamr2scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class optic_MRCO: ItemCore
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class MRCOscope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};