//class ItemCore; // Included in optics.hpp first.
class InventoryFlashLightItem_Base_F;

// Vanila flashlight
class acc_flashlight: ItemCore
{
	class ItemInfo : InventoryFlashLightItem_Base_F
	{
		class FlashLight
		{
			color[] = {180,160,130};
			daylight = 0;
			direction = "flash";
			flaremaxdistance = 200;
			flaresize = 0.8;

			position = "flash dir";
			scale[] = {0};
			size = 1;
			useflare = 1;

			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};
};