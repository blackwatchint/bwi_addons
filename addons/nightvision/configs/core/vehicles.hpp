class Car;
class Tank;
class Helicopter;
class Plane;
class Ship;
class Boat_F;
class APC_Wheeled_01_base_F;
class I_APC_tracked_03_base_F;


/**
 * CARS
 */
class Car_F: Car {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class SUV_01_base_F : Car_F  {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class Hatchback_01_base_F: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class Offroad_01_base_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Right : Left { };
	};
};
class Offroad_02_base_F : Car_F { // Jeep Wrangler
	class Reflectors {
		class Light_1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Light_2 : Light_1 { };
	};
};
class LSV_01_base_F: Car_F { // Polaris DAGOR
	class Reflectors {
		class Light_1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Light_2 : Light_1 { };
	};
};
class LSV_02_base_F: Car_F { // LSV Mk II
	class Reflectors {
		class Light_1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Light_2 : Light_1 { };
	};
};
class Wheeled_APC_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};

class Quadbike_01_base_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * TRUCKS
 */
class Truck_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class Van_01_base_F: Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,0.5,110,10)
		};
		class Left2 : Left { };
		class Left3 : Left { };
		class Right : Left { };
		class Right1: Right { };
		class Right2 : Right { };
	};
};
class Van_02_base_F : Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,0.5,110,10)
		};
		class Left2 : Left { };
		class Left3 : Left { };
		class Right : Left { };
		class Right1: Right { };
		class Right2 : Right { };
	};
};
class Truck_01_base_F: Truck_F {  // HEMETT
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class Truck_02_base_F: Truck_F { // Kamaz
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class Truck_03_base_F: Truck_F { // Typhoon
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * MRAPs
 */
class MRAP_01_base_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class MRAP_02_base_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class MRAP_03_base_F: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * ARMOR
 */
class Tank_F : Tank {
	class Left {
			color[] = COLOR_ORANGE;
		MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
	};
	class Right : Left { };
};
class LT_01_base_F: Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class I_APC_tracked_03_cannon_F : I_APC_tracked_03_base_F
{
	class Reflectors;
	class Left;
	class Right;
};
class Warrior_IFV_Base_F : I_APC_tracked_03_cannon_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class APC_Tracked_03_base_F : Tank_F
{
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.0,110,10)
		};
		class Right : Left { };
	};
};


/**
 * HELICOPTERS
 */
class Helicopter_Base_F: Helicopter {
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Helicopter_Base_H: Helicopter_Base_F {
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Light_01_base_F: Helicopter_Base_H {
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Light_01_armed_F: Heli_Light_01_base_F {
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Light_02_base_F: Helicopter_Base_H {
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Transport_01_base_F: Helicopter_Base_H {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Transport_02_base_F: Helicopter_Base_H {
	class Reflectors {
		class Light {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Transport_03_base_F: Helicopter_Base_H {
	class Reflectors {
		class Light_L {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class Heli_Transport_04_base_F : Helicopter_Base_H {
	class Reflectors {
		class Light_1 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};