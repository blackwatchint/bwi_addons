#include <defines.hpp>
class CfgPatches {
	class bwi_nightvision {
		requiredVersion = 1;
		author = "Black Watch International";
		authorURL = "http://blackwatch-int.com";
		authors[] = {"Fourjays", "0mega", "Diva"};
		version = 1.3;
		versionStr = "1.3.0";
		versionAr[] = {1,3,0};
        requiredAddons[] = {
			"A3_Weapons_F",
			"ace_nightvision",
			"ace_cargo",
			"A3_Air_F"
		};
		units[] = {};
	};
};

class CfgWeapons {
	#include <cfgWeapons.hpp>
};

class CfgAmmo {
	#include <cfgAmmo.hpp>
};

class CfgVehicles {
	#include <cfgVehicles.hpp>
};