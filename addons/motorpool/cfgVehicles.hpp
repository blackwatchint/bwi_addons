class Logic;
class Module_F: Logic
{
	class ArgumentsBaseUnits
	{
		class Units;
	};
	class ModuleDescription;
};


/**
 * Adds a BWI Motorpool interface to any synchronised objects.
 * This module has two modes. Staging mode will add the UI and
 * link itself to locations registered through BWI Staging.
 * Manual mode will add the UI with the options specified
 * by this module.
 */ 
class BWI_Motorpool_ModuleAddMotorpool: Module_F
{
	scope = 2;
	scopeCurator = 1;
	displayName = "Add Motorpool";
	icon = "\bwi_motorpool\data\icon_motor_w.paa";
	category = "BWI_Motorpool";
	function = "bwi_motorpool_module_fnc_addMotorpool";
	functionPriority = 1;
	isGlobal = 0; // Server-only
	isTriggerActivated = 0;
	isDisposable = 0;
	is3DEN = 0;

	class Arguments: ArgumentsBaseUnits
	{
		class DialogMode {
			displayName = "Dialog Mode";
			description = "Manual will use the data below for spawn locations on this instance only. Staging will use locations defined by CfgStagingAreas in the mission description.ext.";
			typeName = "NUMBER";
			class Values {
				class STAGING	{ name = "Staging"; value = 0; };
				class MODULE	{ name = "Manual";	value = 1; default = 1; };
			};
		};

		class LocationEntities {
			displayName = "Location Entities";
			description = "List of named objects used to define the spawn positions available from this dialog. Ignored in Function mode.";
			typeName = "STRING";
			defaultValue = "";
		};

		class LocationLabels {
			displayName = "Location Labels";
			description = "List of user-friendly names for each of the locations defined above. Ignored in Function mode.";
			typeName = "STRING";
			defaultValue = "";
		};

		class VehicleType {
			displayName = "Vehicle Type";
			description = "Restrict the type of vehicles that can be spawned from this dialog. Ignored in Function mode.";
			typeName = "NUMBER";
			class Values {
				class ANY 		 { name = "Any"; 				  value = 0; default = 1; };
				class GROUND	 { name = "Ground";				  value = 1; };
				class HELIPAD 	 { name = "Air (Rotary)"; 		  value = 2; };
				class HANGAR	 { name = "Air (Fixed)"; 		  value = 3; };
				class APRON	 	 { name = "Air (Fixed, Large)";   value = 4; };
				class CARRIER	 { name = "Air (Fixed, Carrier)"; value = 5; };
				class SEA		 { name = "Sea"; 				  value = 6; };
			};
		};

		class EngineOn {
			displayName = "Engine";
			description = "Set whether vehicles spawn with their engine running or not. Ignored in Function mode.";
			typeName = "NUMBER";
			class Values {
				class OFF	{ name = "Off"; value = 0; default = 1; };
				class ON	{ name = "On";	value = 1; };
			};
		};
	};

	class ModuleDescription: ModuleDescription
	{
		description = "Enables usage of BWI Motorpool on synchronised object.";
		sync[] = {};
	};
};