/**
 * ctrlIDCs: xlistLocation, tvVehicle, lblErrorText
 */
params ["_display", "_ctrlIDCs"];

if( !local player ) exitWith {};

private _xlistLocation  = _display displayctrl (_ctrlIDCs select 0);
private _tvVehicle 		= _display displayctrl (_ctrlIDCs select 1);
private _lblErrorText   = _display displayctrl (_ctrlIDCs select 2);

// Prepare for error messages.
private _error = false;
private _errorMsg = "";

// Get currently selected values from the GUI.
private _locationIndex = _xlistLocation lbValue (lbCurSel _xlistLocation);
private _locationData = bwi_motorpool_gui_locationData select _locationIndex;
private _vehicleData = _tvVehicle tvData (tvCurSel _tvVehicle);
private _vehicleClasses = _vehicleData splitString "/";

// Error check the selections!
if ( count _vehicleClasses < 2 ) then {
	_error = true;
	_errorMsg = "Please select a vehicle!";
};

// No errors so far.
if ( !_error ) then {
	// Get the vehicle's CfgPlayableVehicles entry.
	private _motorpoolCfg = configFile >> "CfgPlayableVehicles" >> _vehicleClasses select 0 >> _vehicleClasses select 1;

	// Get remaining funds by player side.
	private _remainingFunds = [playerSide] call bwi_motorpool_fnc_getRemainingFunds;

	// Vehicle cost always 0 when funds disabled.
	if ( _remainingFunds == -1 ) then {
		_vehicleCost = 0;
	};

	// Get vehicle values from the config.
	private _vehicleName = _tvVehicle tvText (tvCurSel _tvVehicle);
	private _vehicleCost = getNumber (_motorpoolCfg >> "cost");
	private _vehicleClassname = getText (_motorpoolCfg >> "classname");
	private _vehicleTextureData = getArray (_motorpoolCfg >> "textureData");
	private _vehicleAnimationData = getArray (_motorpoolCfg >> "animationData");
	private _vehiclePylonLoadout = getArray (_motorpoolCfg >> "pylonLoadout");
	private _vehiclePylonTurrets = getArray (_motorpoolCfg >> "pylonTurrets");
	private _vehicleLoadInCargo = getArray (_motorpoolCfg >> "loadInCargo");
	private _vehicleIsOutpost = getNumber (_motorpoolCfg >> "isOutpost");
	private _vehicleSize = [_vehicleClassname] call bwi_common_fnc_getVehicleSize;
	private _vehicleRadius = _vehicleSize / 2;

	// Get location values from the config.
	private _locationPos = getPosATL (_locationData select 0);
	private _locationDir = getDir (_locationData select 0);
	private _locationName = _locationData select 1;
	private _locationEngine = _locationData select 3;

	// Check for players in the way.
	private _playersNear = [_locationPos, _vehicleRadius, sideEmpty, true] call bwi_common_fnc_countPlayersNear;
	if ( _playersNear > 0 ) then {
		_error = true;
		_errorMsg = "Players are in the way.";
	};

	// Still no errors so far.
	if ( !_error ) then {
		// Attempt to clear empty vehicles in the way.
		// Returns an array - true/false for clear yes/no, and the refunds accrued.
		private _clearVehiclesResult = [_locationPos, _vehicleRadius] call bwi_motorpool_fnc_clearVehicleSpawn;
		private _isPositionClear = _clearVehiclesResult select 0;
		private _totalRefunds = _clearVehiclesResult select 1;

		// Check position is clear.
		if ( !_ispositionClear ) then {
			_error = true;
			_errorMsg = "Non-refundable vehicles are in the way.";
		};

		// Still no errors.
		if ( !_error ) then {
			// Check for vehicle expense.
			if ( _remainingFunds >= 0  && _vehicleCost > _remainingFunds ) then {
				_error = true;
				_errorMsg = "Not enough credits.";
			};

			// No errors at all, good to spawn.
			if ( !_error ) then {
				/**
				* Spawn the vehicle after a 1 second delay. This ensures 
				* any vehicles that need to be cleared have time to be 
				* deleted before the new vehicle is spawned.
				*/
				[
					{
						// Pass the parameters as-is to spawn the vehicle.
						private _vehicle = _this call bwi_motorpool_fnc_spawnVehicle;

						// Save the cost and time to the vehicle for handling refunds.
						_vehicle setVariable ["bwi_motorpool_spawnTime", CBA_missionTime, true];
						_vehicle setVariable ["bwi_motorpool_spawnCost", (_this select 9), true];
						_vehicle setVariable ["bwi_motorpool_vehicleSide", playerSide, true];

						// Debit the funds and broadcast.
						[(_this select 9), playerSide, true] call bwi_motorpool_fnc_debitFunds;
					},
					[
						_locationPos,
						_locationDir,
						_vehicleClassname, 
						_vehicleTextureData, 
						_vehicleAnimationData, 
						_vehiclePylonLoadout, 
						_vehiclePylonTurrets,
						_vehicleLoadInCargo,
						_vehicleIsOutpost,
						_vehicleCost, 
						_locationEngine
					], 1
				] call CBA_fnc_waitAndExecute;

				// Inform the user of selection and fund changes.
				private _message = format ["%1<br/>to %2", _vehicleName, _locationName];

				if ( _remainingFunds >= 0 ) then {
					_message = _message + format["<br/>Deducted: <t color='#d51f00'>%1 Funds</t>", _vehicleCost];

					if ( _totalRefunds > 0 ) then {
						private _netCost = _totalRefunds - _vehicleCost;

						private _netColor = "#d51f00";
						if ( _netCost > 0 ) then { _netColor = "#7bd500"; };

						_message = _message + format["<br/>Refunded: <t color='#7bd500'>%1 Funds</t><br/>Net Change: <t color='%3'>%2 Funds</t>", _totalRefunds, _netCost, _netColor];
					};
				};
				["Vehicle Delivered", _message, 2] call bwi_common_fnc_notification;

				// Clear any error messages.
				_lblErrorText ctrlSetStructuredText parseText "";
			};

		};
	};
};

// Errors.
if ( _error ) then {
	_lblErrorText ctrlSetStructuredText parseText format["<t color='#ff1111'>%1</t>", _errorMsg];
};

false