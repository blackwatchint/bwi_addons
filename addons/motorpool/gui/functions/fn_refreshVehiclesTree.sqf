/**
 * ctrlIDCs: xListSide, tvFaction, xlistLocation, tvVehicle
 */
params ["_display", "_ctrlIDCs"];

private _xlistSide 		= _display displayctrl (_ctrlIDCs select 0);
private _tvFaction 		= _display displayctrl (_ctrlIDCs select 1);
private _xlistLocation  = _display displayctrl (_ctrlIDCs select 2);
private _tvVehicle 		= _display displayctrl (_ctrlIDCs select 3);

tvClear _tvVehicle;

// Load selected faction's motorpool data.
private _side = lbCurSel _xlistSide;
private _factionData = _tvFaction tvData (tvCurSel _tvFaction);
private _factionClasses = _factionData splitString "/";

if ( count _factionClasses < 2 ) exitWith {}; // Handle un-selectable parent factions gracefully.

// Filter and fetch the vehicle categories from CfgPlayableVehicles.
private _locationIndex = _xlistLocation lbValue (lbCurSel _xlistLocation);
private _locationData = bwi_motorpool_gui_locationData select _locationIndex;

private _typeFilter = format ["%1 == 0 || getNumber(_x >> 'type') == %1", _locationData select 2]; // 0 allows all category types.
private _categoryCfgs = _typeFilter configClasses (configFile >> "CfgPlayableVehicles");

// Get the maxLevel value according to the player side.
private _maxLevel = 1;
switch ( playerSide ) do {
	case west: 		 { _maxLevel = bwi_motorpool_maxLevelBlufor;};
	case east: 		 { _maxLevel = bwi_motorpool_maxLevelOpfor; };
	case resistance: { _maxLevel = bwi_motorpool_maxLevelIndep; };
	case civilian:   { _maxLevel = bwi_motorpool_maxLevelCivil; };
};

// Load selected faction's vehicle filter data and create the filter.
private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;
private _allowedVehicles = getArray (_factionCfg >> "allowedVehicles");

// Independently filter for recon and special forces vehicles.
private _allowedVehiclesRecon = [];
private _allowedVehiclesSpecial = [];
private _allowedVehiclesWeapons = [];

if ( bwi_armory_unlockReconElements ) then {
	_allowedVehiclesRecon = getArray (_factionCfg >> "allowedVehiclesRecon");
	_allowedVehicles = _allowedVehicles + _allowedVehiclesRecon;
};
if ( bwi_armory_unlockSpecialElements ) then {
	_allowedVehiclesSpecial = getArray (_factionCfg >> "allowedVehiclesSpecial");
	_allowedVehicles = _allowedVehicles + _allowedVehiclesSpecial;
};
if ( bwi_armory_unlockWeaponsElements ) then {
	_allowedVehiclesWeapons = getArray (_factionCfg >> "allowedVehiclesWeapons");
	_allowedVehicles = _allowedVehicles + _allowedVehiclesWeapons;
};

private _vehicleFilter = format ["(configName(_x) in %1) && (getNumber(_x >> 'level') <= %2)", _allowedVehicles, _maxLevel];

// Build categories/vehicles tree.
{
	// Fetch the vehicles according to those permitted for this faction.
	private _vehicleCfgs = _vehicleFilter configClasses (_x);

	// Don't render if there's no vehicles.
	if ( count _vehicleCfgs > 0 ) then {
		private _parentId = _tvVehicle tvAdd [[], getText (_x >> "name")];
		private _parentData = configName (_x);

		{
			// Generate name and data values from config.
			private _childName = format["%1", getText (_x >> "name")];
			private _childData = format["%1/%2", _parentData, configName (_x)];

			// If name is empty, fetch displayName from CfgVehicles.
			private _childClassname = getText (_x >> "classname");
			if ( _childName == "" ) then {
				_childName = getText (configFile >> "CfgVehicles" >> _childClassname >> "displayName");
			};

			// Add vehicle child entry.
			private _childId = _tvVehicle tvAdd [[_parentId], _childName];
			_tvVehicle tvSetData [[_parentId, _childId], _childData];

			// Determine if vehicle is for recon, special or weapons.
			private _isReconVehicle = ( configName (_x) in _allowedVehiclesRecon );
			private _isSpecialVehicle = ( configName (_x) in _allowedVehiclesSpecial );
			private _isWeaponsVehicle = ( configName (_x) in _allowedVehiclesWeapons );

			// Add vehicle child icon according to intended elements.
			private _childIcon = format["\bwi_data\data\icon_class_%1.paa", getNumber (_x >> "level")];

			if ( _isReconVehicle && !_isSpecialVehicle ) then { _childIcon = "\bwi_data\data\icon_element_lr.paa"; };
			if ( !_isReconVehicle && _isSpecialVehicle ) then { _childIcon = "\bwi_data\data\icon_element_sf.paa"; };
			if ( _isReconVehicle && _isSpecialVehicle )  then { _childIcon = "\bwi_data\data\icon_element_lrsf.paa"; };
			if ( _isWeaponsVehicle ) 					 then { _childIcon = "\bwi_data\data\icon_element_w.paa"; };

			_tvVehicle tvSetPicture [[_parentId, _childId], _childIcon];
		} forEach _vehicleCfgs;

		// Sort the child tree.
		_tvVehicle tvSort [[_parentId], false];
	};
} forEach _categoryCfgs;