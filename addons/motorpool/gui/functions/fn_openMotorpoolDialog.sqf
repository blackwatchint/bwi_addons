params [["_locationData", []]];

// If no location data is specified, we are operating in staging mode.
if ( count _locationData == 0 ) then {
	private _stagingData = [];

	// Set the location data array according to side.
	switch ( playerSide ) do {
		case west: 		 { _stagingData = bwi_motorpool_locationDataBlufor;};
		case east: 		 { _stagingData = bwi_motorpool_locationDataOpfor; };
		case resistance: { _stagingData = bwi_motorpool_locationDataIndep; };
		case civilian:   { _stagingData = bwi_motorpool_locationDataCivil; };
	};

	// Staging data is split by staging index, so merge into a single array.
	// We count 0 because some indexes may be empty and should be excluded!
	// We add instead of pushBack as it merges them at the same depth.
	{
  		if ( count _x > 0 ) then { _locationData = _locationData + _x; };
	} forEach _stagingData;
};

// Set the dialog instance global.
bwi_motorpool_gui_locationData = _locationData;

// Open the dialog.
createDialog 'BwiDialogMotorpool';