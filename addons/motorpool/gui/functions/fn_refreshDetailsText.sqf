/**
 * ctrlIDCs: tvVehicle, lblCostText, lblCargoText, lblCrewSeatText, lblPassSeatText
 */
params ["_display", "_ctrlIDCs"];

private _tvVehicle       = _display displayctrl (_ctrlIDCs select 0);
private _lblCostText     = _display displayctrl (_ctrlIDCs select 1);
private _lblCargoText    = _display displayctrl (_ctrlIDCs select 2);
private _lblCrewSeatText = _display displayctrl (_ctrlIDCs select 3);
private _lblPassSeatText = _display displayctrl (_ctrlIDCs select 4);

// Set the default values.
private _vehicleCost = "-";
private _vehicleCargo = "-";
private _vehicleCrewSeats = "-";
private _vehiclePassSeats = "-";

// Load selected vehicle's config data.
private _vehicleData = _tvVehicle tvData (tvCurSel _tvVehicle);
private _vehicleClasses = _vehicleData splitString "/";

// Get the total funds remaining.
private _remainingFunds = [playerSide] call bwi_motorpool_fnc_getRemainingFunds;

// Process vehicles only, not categories.
if ( count _vehicleClasses >= 2 ) then {
	// Get the vehicle's CfgPlayableVehicles and CfgVehicles configs entries.
	private _motorpoolCfg = configFile >> "CfgPlayableVehicles" >> _vehicleClasses select 0 >> _vehicleClasses select 1;
	private _vehicleClassname = getText (_motorpoolCfg >> "classname");
	private _vehicleCfg = configFile >> "CfgVehicles" >> _vehicleClassname;

	// Get cost and cargo space from their configs.
	_vehicleCost = getNumber (_motorpoolCfg >> "cost");
	_vehicleCargo = getNumber (_vehicleCfg >> "ace_cargo_space");

	// Read the crew and passenger count from CfgPlayableVehicles.
	_vehicleCrewSeats = getNumber (_motorpoolCfg >> "crew");
	_vehiclePassSeats = getNumber (_motorpoolCfg >> "passengers");

	// If either the crew or passenger count are -1, read from CfgVehicles.
	if ( _vehicleCrewSeats < 0 ) then {
		_vehicleCrewSeats = [_vehicleClassname, false] call BIS_fnc_crewCount;
	};
	if ( _vehiclePassSeats < 0 ) then {
		_vehiclePassSeats = ([_vehicleClassname, true] call BIS_fnc_crewCount) - _vehicleCrewSeats;
	};

	// If the vehicle is too expensive, make the cost text red.
	if (_remainingFunds >= 0  && _vehicleCost > _remainingFunds) then {
		_vehicleCost = format["<t color='#ff1111'>%1</t>", _vehicleCost];
	};
};

// If remainingFunds is -1, set both to -.
if ( _remainingFunds == -1 ) then {
	_vehicleCost = "-";
	_remainingFunds = "-";
};

// Apply the text to the detail panels.
_lblCostText ctrlSetStructuredText parseText format["Cost: %1/%2", _vehicleCost, _remainingFunds];
_lblCargoText ctrlSetStructuredText parseText format["Cargo Space: %1", _vehicleCargo];
_lblCrewSeatText ctrlSetStructuredText parseText format["Crew: %1", _vehicleCrewSeats];
_lblPassSeatText ctrlSetStructuredText parseText format["Passengers: %1", _vehiclePassSeats];