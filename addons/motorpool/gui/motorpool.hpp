class BwiGui_RscText;
class BwiGui_RscStructuredText;
class BwiGui_RscPicture;
class BwiGui_RscButton;
class BwiGui_RscXListBox;
class BwiGui_RscTree;

class BwiDialogMotorpool
{
	idd = 6800;
	movingenable = 0;
	
	class ControlsBackground
	{
		class MotorpoolBackground: BwiGui_RscPicture
		{
			idc = 6800;
			text = "\bwi_motorpool\gui\data\motorpool_bg.paa";
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = -3 * GUI_GRID_H + GUI_GRID_Y;
			w = 38.5 * GUI_GRID_W;
			h = 30 * GUI_GRID_H;
		};
	};
	

	class Controls
	{
		class imgFaction: BwiGui_RscPicture
		{
			idc = 6801;
			text = "";

			x = 34.6 * GUI_GRID_W + GUI_GRID_X;
			y = -2.45 * GUI_GRID_H + GUI_GRID_Y;
			w = 4.35 * GUI_GRID_W;
			h = 3.62 * GUI_GRID_H;
		};

		class lblSide: BwiGui_RscText
		{
			idc = 6802;
			text = "Side:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class xlistSide: BwiGui_RscXListBox
		{
			idc = 6803;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.275,0.463,0.694,1};
			
			onLoad = "[ctrlParent (_this select 0), [6803]] call bwi_armory_fnc_refreshSidesList;";
			onLBSelChanged = "[ctrlParent (_this select 0), [6803, 6805]] call bwi_armory_fnc_refreshFactionsTree;";
		};

		class lblFaction: BwiGui_RscText
		{
			idc = 6804;
			text = "Faction:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvFaction: BwiGui_RscTree
		{
			idc = 6805;
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 16 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6803, 6805]] call bwi_armory_fnc_refreshFactionsTree;";
			onTreeSelChanged = "[ctrlParent (_this select 0), [6803, 6805, 6807, 6809]] call bwi_motorpool_fnc_refreshVehiclesTree;[ctrlParent (_this select 0), [6801, 6805]] call bwi_armory_fnc_refreshFactionImage;";
		};

		class lblLocation: BwiGui_RscText
		{
			idc = 6806;
			text = "Location:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class xlistLocation: BwiGui_RscXListBox
		{
			idc = 6807;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.275,0.463,0.694,1};
			
			onLoad = "[ctrlParent (_this select 0), [6807]] call bwi_motorpool_fnc_refreshLocationsList;";
			onLBSelChanged = "[ctrlParent (_this select 0), [6803, 6805, 6807, 6809]] call bwi_motorpool_fnc_refreshVehiclesTree; [ctrlParent (_this select 0), [6809, 6810, 6811, 6812, 6813]] call bwi_motorpool_fnc_refreshDetailsText;";
		};

		class lblVehicle: BwiGui_RscText
		{
			idc = 6808;
			text = "Vehicle:";
			x = 21 * GUI_GRID_W + GUI_GRID_X;
			y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 2.5 * GUI_GRID_H;
			colorText[] = {1,1,1,1};
		};

		class tvVehicle: BwiGui_RscTree
		{
			idc = 6809;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 15 * GUI_GRID_W;
			h = 14 * GUI_GRID_H;

			onLoad = "[ctrlParent (_this select 0), [6803, 6805, 6807, 6809]] call bwi_motorpool_fnc_refreshVehiclesTree;";
			onTreeSelChanged = "[ctrlParent (_this select 0), [6809, 6810, 6811, 6812, 6813]] call bwi_motorpool_fnc_refreshDetailsText;";
		};

		class lblCostText: BwiGui_RscStructuredText
		{
			idc = 6810;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 7 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};
		};

		class lblCargoText: BwiGui_RscStructuredText
		{
			idc = 6811;
			x = 28.5 * GUI_GRID_W + GUI_GRID_X;
			y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};
		};

		class lblCrewSeatText: BwiGui_RscStructuredText
		{
			idc = 6812;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 7 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};
		};

		class lblPassSeatText: BwiGui_RscStructuredText
		{
			idc = 6813;
			x = 28.5 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.1,0.1,0.1,1};

			onLoad = "[ctrlParent (_this select 0), [6809, 6810, 6811, 6812, 6813]] call bwi_motorpool_fnc_refreshDetailsText;"; // Called on last one only so it handles all four!
		};

		class lblErrorText: BwiGui_RscStructuredText
		{
			idc = 6814;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.7 * GUI_GRID_H + GUI_GRID_Y;
			w = 16.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorText[] = {1,0,0,1};
		};

		class btnCancel: BwiGui_RscButton
		{
			idc = 6815;
			text = "Close";
			x = 25 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			onMouseButtonClick = "closeDialog 2;";
		};

		class btnSelect: BwiGui_RscButton
		{
			idc = 6816;
			text = "Request";
			x = 31.5 * GUI_GRID_W + GUI_GRID_X;
			y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			
			onMouseButtonClick = "[ctrlParent (_this select 0), [6807, 6809, 6814]] call bwi_motorpool_fnc_selectVehicleGUI;";
		};
	};
};