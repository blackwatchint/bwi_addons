#include "\bwi_common\modules\module_header.sqf"

// Run the server only.
if ( isServer  ) then {
	if ( count _units > 0 ) then {
		private _locationData = [];
		
		// Get the module settings.
		private _dialogMode = _logic getVariable ["DialogMode", 1];
		private _locationEntities = _logic getVariable ["LocationEntities", ""];
		private _locationLabels = _logic getVariable ["LocationLabels", ""];
		private _vehicleType = _logic getVariable ["VehicleType", 0];
		private _engineOn = _logic getVariable ["EngineOn", false];

		// Dialog is to use the module defined location data and settings.
		if ( _dialogMode == 1 ) then {
			// Split the entities and labels into an array.
			_locationEntities = _locationEntities splitString ",";
			_locationLabels = _locationLabels splitString ",";
	
			if ( count _locationEntities > 0 ) then {
				// Ensure each entity has a matching label and vehicle type array.
				{
					private _entity = missionNamespace getVariable [_x, objNull];
					private _label = "";

					// Don't process null entities.
					if ( !isNull _entity ) then {
						// Handle missing labels, or use the specified label.
						if ( _forEachIndex >= count _locationLabels ) then {
							_label = format["Location %1", _forEachIndex + 1];
						} else {
							_label = _locationLabels select _forEachIndex;
						};

						_locationData pushBack [_entity, _label, _vehicleType, _engineOn];
					};
				} forEach _locationEntities;
			};
		};

		// Add motorpool to all sync'd objects with the specified locationData.
		{
			if ( !(_x isKindOf "Man") ) then {
				[_x, _locationData] call bwi_motorpool_fnc_addAction;
			};
		} forEach _units;
	};
};

#include "\bwi_common\modules\module_footer.sqf"