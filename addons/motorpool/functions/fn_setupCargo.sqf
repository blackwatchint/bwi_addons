params ["_vehicle", "_cargo"];

// Load each item into the vehicle cargo space.
{
	// Attempt to load the item through ACE.
	private _isLoaded = [_x, _vehicle] call ace_cargo_fnc_loadItem;

	// Log an error if it couldn't be loaded.
	if ( !_isLoaded ) then {
		["Could not load %2 into %1. Insufficient cargo space?", _vehicle, _x] call bwi_common_fnc_log;
	};
} forEach _cargo;