/**
 * Event handler that creates motorpool spawn locations
 * from CfgStagingAreas data for the given staging area.
 */
["bwi_staging_createStaging",
	{
		params ["_side", "_index", "_stagingArea", "_flagpole"];

		// Log the creation.
		["Creating motorpool locations for %1...", _stagingArea] call bwi_common_fnc_log;

		private _stagingAreaCfg = missionConfigFile >> "CfgStagingAreas" >> _stagingArea;
		private _motorpoolLocations = getArray (_stagingAreaCfg >> "motorpoolLocations");
		private _motorpoolLabels = getArray (_stagingAreaCfg >> "motorpoolLabels");
		private _motorpoolTypes = getArray (_stagingAreaCfg >> "motorpoolTypes");

		// Log mismatches on motorpool configs before quitting.
		if ( (count _motorpoolLocations != count _motorpoolLabels) || (count _motorpoolLocations != count _motorpoolTypes) ) exitWith {
			["ERROR! Mismatch in motorpool definition for %1.", _stagingArea] call bwi_common_fnc_log;
		};

		// Iterate over the motorpool positions, adding them to an array for this staging area's index.
		private _data = [];
		{
			private _entity = missionNamespace getVariable _x;
			private _label = _motorpoolLabels select _forEachIndex;
			private _type = _motorpoolTypes select _forEachIndex;

			private _location = [_entity, _label, _type, 0]; // Engine on always 0 via this method.
			_data pushBack _location;
		} forEach _motorpoolLocations;

		// Overwrite the data for this side's staging index and broadcast.
		switch ( _side ) do {
			case west: 		 { 
				bwi_motorpool_locationDataBlufor set [_index, _data];
				publicVariable "bwi_motorpool_locationDataBlufor";
			};
			case east: 		 {
				bwi_motorpool_locationDataOpfor set [_index, _data];
				publicVariable "bwi_motorpool_locationDataOpfor";
			};
			case resistance: {
				bwi_motorpool_locationDataIndep set [_index, _data];
				publicVariable "bwi_motorpool_locationDataIndep";
			};
			case civilian:   {
				bwi_motorpool_locationDataCivil set [_index, _data];
				publicVariable "bwi_motorpool_locationDataCivil";
			};
		};

		// Add the motorpool action to the flagpole (empty array for staging).
		[_flagpole, []] call bwi_motorpool_fnc_addAction;
	}
] call CBA_fnc_addEventHandlerArgs;


/**
 * Event handler that removes motorpool spawn locations when 
 * staging areas are set to "NONE". Changes to staging areas 
 * that aren't "NONE" will be handled by the createStaging 
 * handler and are ignored to reduce network broadcasting.
 */
["bwi_staging_removeStaging",
	{
		params ["_side", "_index", "_stagingArea"];

		if ( _stagingArea == "NONE" ) then { 
			// Overwrite the data for this side's staging index and broadcast.
			switch ( _side ) do {
				case west: 		 { 
					bwi_motorpool_locationDataBlufor set [_index, []];
					publicVariable "bwi_motorpool_locationDataBlufor";
				};
				case east: 		 {
					bwi_motorpool_locationDataOpfor set [_index, []];
					publicVariable "bwi_motorpool_locationDataOpfor";
				};
				case resistance: {
					bwi_motorpool_locationDataIndep set [_index, []];
					publicVariable "bwi_motorpool_locationDataIndep";
				};
				case civilian:   {
					bwi_motorpool_locationDataCivil set [_index, []];
					publicVariable "bwi_motorpool_locationDataCivil";
				};
			};
		};
	}
] call CBA_fnc_addEventHandlerArgs;