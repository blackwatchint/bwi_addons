params ["_spentFunds", "_side", ["_broadcast", true]];

// Update and broadcast the new spent funds according to the setting.
switch ( _side ) do {
	case west: 		 { missionNamespace setVariable ["bwi_motorpool_spentFundsBlufor", _spentFunds, _broadcast]; };
	case east: 		 { missionNamespace setVariable ["bwi_motorpool_spentFundsOpfor",  _spentFunds, _broadcast]; };
	case resistance: { missionNamespace setVariable ["bwi_motorpool_spentFundsIndep",  _spentFunds, _broadcast]; };
	case civilian:   { missionNamespace setVariable ["bwi_motorpool_spentFundsCivil",  _spentFunds, _broadcast]; };
};