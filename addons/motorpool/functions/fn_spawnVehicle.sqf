params ["_position", "_direction", "_classname", ["_textureData", []], ["_animationData", []], ["_pylonLoadout", []], ["_pylonTurrets", []], ["_loadInCargo", []], ["_isOutpost", 0], ["_vehicleCost", 0], ["_engineOn", 0]];

// Create the vehicle. We setPosATL to ensure boats are spawned correctly.
private _vehicle = createVehicle [_classname, _position, [], 0, "CAN_COLLIDE"];
_vehicle setPosATL _position;
_vehicle setDir _direction;

// Handle the "crew" for UAVs.
private _isUAV = getNumber (configFile >> "CfgVehicles" >> _classname >> "isUav");
if ( _isUAV == 1 ) then {
   createVehicleCrew _vehicle; 	
};

// Initialize vehicle textures and animations.
switch true do {
	case ( count _textureData > 0 && count _animationData > 0 ): {
		[_vehicle, _textureData, _animationData] call BIS_fnc_initVehicle;
	};

	case ( count _textureData > 0 && count _animationData == 0 ): {
		[_vehicle, _textureData, false] call BIS_fnc_initVehicle;
	};

	case ( count _textureData == 0 && count _animationData > 0 ): {
		[_vehicle, false, _animationData] call BIS_fnc_initVehicle;
	};
};

// Setup the pylons as specified.
if ( count _pylonLoadout > 0 ) then {
	{
		// Set turret to ID specified in matching array.
		private _turret = [];
		if ( count _pylonTurrets >= count _pylonLoadout ) then {
			_turret = [_pylonTurrets select _forEachIndex];

			// If the turret is -1, change it to an empty array.
			if ( (_turret select 0) == -1 ) then { _turret = []; };
		};

		// Configure this pylon.
		[_vehicle, _forEachIndex, _x, _turret] call bwi_motorpool_fnc_configurePylon;
	} forEach _pylonLoadout;
};

// Handle setup of aircraft and boat specific features.
switch true do {
	case ( _vehicle isKindOf "Air" ): {
		[_vehicle] call bwi_motorpool_fnc_setupFRIES;
		[_vehicle] call bwi_motorpool_fnc_setupServicing;
	};

	case ( _vehicle isKindOf "Ship" ): {
		[_vehicle] call bwi_motorpool_fnc_setupServicing;
	};
};

// Handle loading of any cargo.
if ( count _loadInCargo > 0 ) then {
	[_vehicle, _loadInCargo] call bwi_motorpool_fnc_setupCargo;
};

// Turn the engine on.
if ( _engineOn == 1 ) then {
	_vehicle engineOn true;
};

// Call an event handler for creation of this vehicle.
[
	"bwi_motorpool_spawnVehicle",
	[
		_vehicle,
		_textureData,
		_animationData,
		_pylonLoadout,
		_pylonTurrets,
		_loadInCargo,
		_isOutpost,
		_vehicleCost,
		_engineOn
	]
] call CBA_fnc_localEvent;

// Return the vehicle.
_vehicle