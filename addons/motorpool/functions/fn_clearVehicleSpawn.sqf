params ["_position", ["_range", 10]];

private _result = true;
private _refunds = 0;
private _side = any;

// When over water we have to switch to ASL for nearestObjects.
if ( surfaceIsWater _position ) then { _position = ATLToASL _position; };

// Find the vehicles near the position.
private _nearVehicles = nearestObjects [_position, ["LandVehicle", "Air", "Ship", "Slingload_base_F"], _range];

if ( count _nearVehicles > 0 ) then {
	{
		// Only delete vehicles with no player crew.
		private _playerCrew = ({ isPlayer _x } count (crew _x));

		if ( _playerCrew == 0 ) then {
			// Get vehicle spawn time and cost.
			private _vehicleSpawnTime = _x getVariable ["bwi_motorpool_spawnTime", 0];
			private _vehicleRefundCost = _x getVariable ["bwi_motorpool_spawnCost", 0];
			_side = _x getVariable ["bwi_motorpool_vehicleSide", 0];

			// Vehicle can be refunded.
			if ( bwi_motorpool_refundTimeLimit < 0 || _vehicleSpawnTime > (CBA_missionTime - bwi_motorpool_refundTimeLimit) ) then {
				_refunds = _refunds + _vehicleRefundCost;
				deleteVehicle _x;
			// Vehicle can not be refunded.
			} else {
				_result = false;
			};
		// Vehicle can not be refunded.
		} else {
			_result = false;
		};
	} forEach _nearVehicles;
};


// Credit the funds and broadcast.
[_refunds, _side, true] call bwi_motorpool_fnc_creditFunds;

// Return the result as an array.
[_result, _refunds]