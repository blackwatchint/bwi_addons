params ["_funds", "_side", ["_broadcast", true]];

// Fetch the spent funds.
private _spentFunds = [_side] call bwi_motorpool_fnc_getSpentFunds;

// Debit the spent funds by adding to them.
_spentFunds = _spentFunds + _funds;

// Broadcast the update if applicable.
[_spentFunds, _side, _broadcast] call bwi_motorpool_fnc_setSpentFunds;