params ["_vehicle"];

// Action only displayed for pilot when engine is off and aircraft stationary.
private _actionCondition = format[
	"(driver _target == _this) && !(isEngineOn _target) && (speed _target < 1) && (getPosATL _target) distance %1 < %2 && !(_target getVariable ['bwi_motorpool_serviceInProgress', false])",
	getPosATL _vehicle,
	bwi_motorpool_servicingRange
];

// Add the actions and broadcast.
[_vehicle, [
	"<t color='#73d500'>Repair, Refuel, Rearm</t>", 
	{ [_this select 0] call bwi_motorpool_fnc_serviceVehicle; },
	[_locationData],
	10, false, true, "",
	_actionCondition
]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP