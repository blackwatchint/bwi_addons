params ["_side"];

// Get the total funds according to the side.
private _totalFunds = 0;
switch ( _side ) do {
	case west: 		 { _totalFunds = round bwi_motorpool_totalFundsBlufor;};
	case east: 		 { _totalFunds = round bwi_motorpool_totalFundsOpfor; };
	case resistance: { _totalFunds = round bwi_motorpool_totalFundsIndep; };
	case civilian:   { _totalFunds = round bwi_motorpool_totalFundsCivil; };
};

// If funds are enabled, return the total funds less the spent funds.
if ( _totalFunds >= 0 ) then {
	private _spentFunds = [_side] call bwi_motorpool_fnc_getSpentFunds;
	_totalFunds = _totalFunds - _spentFunds;
};

_totalFunds