params ["_vehicle"];

// Initialize the state tracking global.
bwi_motorpool_serviceState = [
	[_vehicle] call bwi_common_fnc_getVehicleDamage,
	[_vehicle] call bwi_common_fnc_getVehicleFuel,
	[_vehicle] call bwi_common_fnc_getVehicleAmmo
];

// Force pilot weapon to default to prevent weapon burst sequences from continuing during rearm.
_vehicle selectWeaponTurret [ _vehicle currentWeaponTurret [-1], [-1] ];

/**
 * Runs every few seconds on a vehicle, increasing the 
 * fuel and ammunition, and decreasing the damage in 
 * steps. Each step generates a report for the pilot 
 * informing them of progress.
 *
 * Note we manually track the expected progress through
 * a global variable rather than checking the state
 * directly on each run.This is to avoid a situation 
 * where an update of 0.12 to 0.14 may not provide a 
 * measurable change in state when re-checked.
 */
_servicePFHID = [
	{
		(_this select 0) params ["_vehicle"];
		private _instance = _this select 1;

		private _damageState = bwi_motorpool_serviceState select 0;
		private _fuelState   = bwi_motorpool_serviceState select 1;
		private _ammoState   = bwi_motorpool_serviceState select 2;

		private _localTurrets = true;
		private _serviceComplete = false;
		private _serviceCancelled = false;

		// Set a global flag to track in progress state.
		private _inProgress = _vehicle getVariable ["bwi_motorpool_serviceInProgress", false];
		if ( !_inProgress ) then {
			_vehicle setVariable ["bwi_motorpool_serviceInProgress", true, true]; // Broadcast.
		};

		// Engine off and not moving.
		if ( !(isEngineOn _vehicle)  && (speed _vehicle < 1) ) then {
			// Repair the vehicle in increments.
			if ( _damageState > 0 ) then {
				_damageState = (_damageState - bwi_motorpool_repairRate) max 0;
				_damageState = _damageState min 0.9; // Prevent accidental explosions.

				_vehicle setDamage _damageState;

				if ( _damageState == 0 ) then {
					[_vehicle, 0] call bwi_common_fnc_setVehicleHitpoints;
				};
			};

			// Refuel the vehicle in increments (after repairing).
			if ( _fuelState < 1 && _damageState == 0 ) then {
				_fuelState = (_fuelState + bwi_motorpool_refuelRate) min 1;

				_vehicle setFuel _fuelState;
			};

			// Rearm the vehicle in increments (after refueling and repairing).
			if ( _ammoState < 1 && _fuelState == 1 && _damageState == 0 ) then {
				_ammoState = (_ammoState + bwi_motorpool_rearmRate) min 1;
				
				_vehicle setVehicleAmmo _ammoState;

				// Detect non-local turrets.
				{
					if ( !(_vehicle turretLocal _x) ) exitWith {
						_localTurrets = false;
					};
				} forEach allTurrets _vehicle;
			};

			// Update the state tracking global.
			bwi_motorpool_serviceState = [_damageState, _fuelState, _ammoState];

			// Servicing completed.
			if ( _damageState <= 0 && _fuelState >= 1 && _ammoState >= 1 ) then {
				_serviceComplete = true;

				// Remove the PFH.
				[_instance] call CBA_fnc_removePerFrameHandler;

				// Update tracking variable.
				_vehicle setVariable ["bwi_motorpool_serviceInProgress", false, true]; // Broadcast.
			};
		// Engine on or moving.
		} else {
			_serviceCancelled = true;

			// Remove the PFH.
			[_instance] call CBA_fnc_removePerFrameHandler;

			// Update tracking variable.
			_vehicle setVariable ["bwi_motorpool_serviceInProgress", false, true]; // Broadcast.
		};


		// Build a servicing report.
		private _message = "";
		private _timer = bwi_motorpool_servicingInterval;

		private _damageColor = "#4ad500";
		private _fuelColor = "#4ad500";
		private _ammoColor = "#4ad500";

		if ( _damageState > 0 ) then { _damageColor = "#d58700"; };
		if ( _fuelState < 1 ) then { _fuelColor = "#d58700"; };
		if ( _ammoState < 1 ) then { _ammoColor = "#d58700"; };
		if ( !_localTurrets ) then { _ammoColor = "#d51200"; };

		_message = format ["%1Repair: <t color='%2'>%3%4</t><br/>", _message, _damageColor, floor (100 - (_damageState * 100)), "%"];
		_message = format ["%1Refuel: <t color='%2'>%3%4</t><br/>", _message, _fuelColor, floor (_fuelState * 100), "%"];
		_message = format ["%1Rearm: <t color='%2'>%3%4</t><br/>", _message, _ammoColor, floor (_ammoState * 100), "%"];

		if ( !_localTurrets ) then {
			_message = format["%1<t color='#d51200'>Dismount crew to complete rearm!</t><br/>", _message];
		};

		if ( _serviceCancelled ) then {
			_message = format["%1<t color='#d51200'>Servicing Cancelled!</t>", _message];
			_timer = 0; // Resets notification display time to default.
		};

		if ( _serviceComplete ) then {
			_message = format["%1<t color='#4ad500'>Servicing Complete!</t>", _message];
			_timer = 0; // Resets notification display time to default.
		};

		// Output the servicing report. Note we set priority with a custom timer.
		["Servicing Report", _message, 3, true, _timer] call bwi_common_fnc_notification;
	},
	bwi_motorpool_servicingInterval,
	[_vehicle]
] call CBA_fnc_addPerFrameHandler;