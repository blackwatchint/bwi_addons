params ["_vehicle"];

// Check helicopters for FRIES support.
if ( _vehicle isKindOf "Helicopter" ) then {
	private _canFRIES = getNumber (configFile >> "CfgVehicles" >> typeOf _vehicle >> "ace_fastroping_enabled");

	if ( _canFRIES == 2 ) then {
		// Add FRIES.
		[_vehicle] remoteExecCall ["ace_fastroping_fnc_equipFRIES", 2, false]; // Server, no-JIP.
	};
};