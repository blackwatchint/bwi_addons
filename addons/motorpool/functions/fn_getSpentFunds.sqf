params ["_side"];

// Get the spent funds according to player side.
private _spentFunds = 0;
switch ( _side ) do {
	case west: 		 { _spentFunds = bwi_motorpool_spentFundsBlufor;};
	case east: 		 { _spentFunds = bwi_motorpool_spentFundsOpfor; };
	case resistance: { _spentFunds = bwi_motorpool_spentFundsIndep; };
	case civilian:   { _spentFunds = bwi_motorpool_spentFundsCivil; };
};

_spentFunds