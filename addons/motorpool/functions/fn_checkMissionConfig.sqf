if ( is3DEN ) then {
	private _stagingAreasCfg = missionConfigFile >> "CfgStagingAreas";

	// Staging is enabled.
	if ( isClass _stagingAreasCfg ) then {
		private _allLogicNames = [3] call bwi_common_fnc_get3DENEntityNames;

		// Now check each staging area.
		private _stagingCfgs = "true" configClasses (_stagingAreasCfg);

		{
			private _motorpoolLocations = getArray (_x >> "motorpoolLocations");
			private _motorpoolLabels = getArray (_x >> "motorpoolLabels");
			private _motorpoolTypes = getArray (_x >> "motorpoolTypes");

			// Check for mismatched locations, labels and types.
			if ( (count _motorpoolLocations != count _motorpoolLabels) ) then {
				["Mismatch in motorpool locations and labels for %1.", configName _x] call bwi_common_fnc_log;
			};

			if ( (count _motorpoolLocations != count _motorpoolTypes) ) then {
				["Mismatch in motorpool locations and types for %1.", configName _x] call bwi_common_fnc_log;
			};

			// Check for missing logic entities.
			{
				if ( !(_x in _allLogicNames) ) then {
					["Motorpool location %1 does not exist.", _x] call bwi_common_fnc_log;
				};
			} forEach _motorpoolLocations;
		} forEach _stagingCfgs;
	};
};
