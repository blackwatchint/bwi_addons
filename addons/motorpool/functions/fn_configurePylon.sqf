params ["_aircraft", "_pylonIndex", "_newPylonMagazine", "_newPylonTurret"];

/**
 * Credit to ACE for solving the "empty pylons" issue in 
 * ace_pylons_fnc_configurePylons. Unfortunately we can't
 * call their function directly as it uses a hard-coded 
 * progress bar.
 *
 * The gist of the fix is that we check the pylon for an
 * existing magazine weapon and if it is the last one, we 
 * remove it after the pylon loadout is changed.
 */
private _removePylonWeapon = "";
private _currentPylonMagazine = (getPylonMagazines _aircraft) select _pylonIndex;

// Log the pylon change.
["Pylon %1 changing %2 to %3 with turret %4", _pylonIndex, _currentPylonMagazine, _newPylonMagazine, _newPylonTurret] call bwi_common_fnc_log;

// Current pylon isn't empty and is changing.
if ( _currentPylonMagazine != "" && _currentPylonMagazine != _newPylonMagazine ) then {
	// Find this pylon's magazine's weapon.
	private _allPylonWeapons = (getPylonMagazines _aircraft) apply { 
		getText (configFile >> "CfgMagazines" >> _x >> "pylonWeapon") 
	}; 
    private _currentPylonWeapon = _allPylonWeapons select _pylonIndex;
	private _newPylonWeapon = getText (configFile >> "CfgMagazines" >> _newPylonMagazine >> "pylonWeapon");

	// If the current pylon's weapon is the last one and not required by the new pylon weapon, set it for removal.
	if ( ({_x == _currentPylonWeapon} count _allPylonWeapons) == 1 && _currentPylonWeapon != _newPylonWeapon ) then { 
		_removePylonWeapon = _currentPylonWeapon; 
	}; 
};

// Set the aircraft's pylon magazine for this pylon index.
_aircraft setPylonLoadout [(_pylonIndex + 1), _newPylonMagazine, false, _newPylonTurret];

// If there is an old pylon weapon to remove.
if ( _removePylonWeapon != "" ) then {
	// Log the turret removal.
	["Pylon %1 removing unused %2", _pylonIndex, _removePylonWeapon] call bwi_common_fnc_log;

	// Remove the weapon on both turret indexes.
	{
		if ( _aircraft turretLocal _x ) then {
			_aircraft removeWeaponTurret [_removePylonWeapon, _x];
		};
	} forEach [[-1], [0]];
};
