params ["_funds", "_side", ["_broadcast", true]];

// Fetch the spent funds.
private _spentFunds = [_side] call bwi_motorpool_fnc_getSpentFunds;

// Credit the spent funds by subtracting from them.
_spentFunds = _spentFunds - _funds;

// Broadcast the update if applicable.
[_spentFunds, _side, _broadcast] call bwi_motorpool_fnc_setSpentFunds;
