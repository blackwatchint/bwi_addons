params ["_addToObject", "_locationData"];

private _actionCondition = "true";

// Add the action condition if restricted access is enabled.
private _restrictAccessTo = bwi_motorpool_restrictAccessTo splitString ",";
if ( count _restrictAccessTo > 0 ) then {
	_actionCondition = format["(str _this) in %1", _restrictAccessTo];
};

// Add the action and broadcast.
[_addToObject, [
	"<t color='#d5c600'>Motorpool</t>", 
	{ (_this select 3) call bwi_motorpool_fnc_openMotorpoolDialog; }, 
	[_locationData], 1.4, false, true, "", _actionCondition
]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP