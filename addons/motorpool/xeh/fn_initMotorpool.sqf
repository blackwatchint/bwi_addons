// Initialize the non-public globals for the GUI.
bwi_motorpool_gui_locationData = [];

// Run on the server only.
if ( isServer ) then {
	// Initialize the location variables and broadcast.
	 // Note - number of empty child arrays must equal number of staging areas.
	if ( isNil "bwi_motorpool_locationDataBlufor" ) then {
		missionNamespace setVariable ["bwi_motorpool_locationDataBlufor", [[],[]], true];
	};
	if ( isNil "bwi_motorpool_locationDataOpfor" ) then {
		missionNamespace setVariable ["bwi_motorpool_locationDataOpfor", [[],[]], true];
	};
	if ( isNil "bwi_motorpool_locationDataIndep" ) then {
		missionNamespace setVariable ["bwi_motorpool_locationDataIndep", [[],[]], true];
	};
	if ( isNil "bwi_motorpool_locationDataCivil" ) then {
		missionNamespace setVariable ["bwi_motorpool_locationDataCivil", [[],[]], true];
	};

	// Initialize the spent funds variables and broadcast.
	if ( isNil "bwi_motorpool_spentFundsBlufor" ) then {
		missionNamespace setVariable ["bwi_motorpool_spentFundsBlufor", 0, true];
	};
	if ( isNil "bwi_motorpool_spentFundsOpfor" ) then {
		missionNamespace setVariable ["bwi_motorpool_spentFundsOpfor", 0, true];
	};
	if ( isNil "bwi_motorpool_spentFundsIndep" ) then {
		missionNamespace setVariable ["bwi_motorpool_spentFundsIndep", 0, true];
	};
	if ( isNil "bwi_motorpool_spentFundsCivil" ) then {
		missionNamespace setVariable ["bwi_motorpool_spentFundsCivil", 0, true];
	};

	// Register the handlers for activation/deactivation of staging areas.
	call bwi_motorpool_fnc_registerStagingHandlers
};