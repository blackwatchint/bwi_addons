// Define category strings for the CBA settings.
private _categoryLabelMotorpool = "BWI Motorpool";
private _categoryLabelLevels = "Classes"; // Note - Class(es) makes more UX sense. Level(s) used in code to avoid reserved word conflicts.
private _categoryLabelFunds = "Funds";
private _categoryLabelRestrictions = "Restrictions";
private _categoryLabelServicing = "Aircraft Servicing";


// Initialize CBA settings.
// Class settings.
[
    "bwi_motorpool_maxLevelBlufor",
    "LIST",
    ["BLUFOR Maximum Vehicle Class", "The maximum class of vehicles BLUFOR is permitted to spawn. Vehicles are classified according to their capabilities of dealing with threats."],
    [_categoryLabelMotorpool, _categoryLabelLevels],
    [
        [1,2,3,4,5],
        ["Class I (Utility)","Class II (Anti-Car)","Class III (Anti-APC/SPAA)", "Class IV (Anti-IFV)", "Class V (Anti-MBT)"],
        0
    ],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_maxLevelOpfor",
    "LIST",
    ["OPFOR Maximum Vehicle Class", "The maximum class of vehicles OPFOR is permitted to spawn. Vehicles are classified according to their capabilities of dealing with threats."],
    [_categoryLabelMotorpool, _categoryLabelLevels],
    [
        [1,2,3,4,5],
        ["Class I (Utility)","Class II (Anti-Car)","Class III (Anti-APC/SPAA)", "Class IV (Anti-IFV)", "Class V (Anti-MBT)"],
        0
    ],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_maxLevelIndep",
    "LIST",
    ["INDEP Maximum Vehicle Class", "The maximum class of vehicles INDEP is permitted to spawn. Vehicles are classified according to their capabilities of dealing with threats."],
    [_categoryLabelMotorpool, _categoryLabelLevels],
    [
        [1,2,3,4,5],
        ["Class I (Utility)","Class II (Anti-Car)","Class III (Anti-APC/SPAA)", "Class IV (Anti-IFV)", "Class V (Anti-MBT)"],
        0
    ],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_maxLevelCivil",
    "LIST",
    ["CIVIL Maximum Vehicle Class", "The maximum class of vehicles CIVIL is permitted to spawn. Vehicles are classified according to their capabilities of dealing with threats."],
    [_categoryLabelMotorpool, _categoryLabelLevels],
    [
        [1,2,3,4,5],
        ["Class I (Utility)","Class II (Anti-Car)","Class III (Anti-APC/SPAA)", "Class IV (Anti-IFV)", "Class V (Anti-MBT)"],
        0
    ],
    true
] call CBA_settings_fnc_init;


// Credit settings.
[
    "bwi_motorpool_totalFundsBlufor",
    "SLIDER",
    ["BLUFOR Total Funds", "The funds available to BLUFOR for spawning vehicles. To refund simply add the cost to this total. Set to -1 for infinite funds."],
    [_categoryLabelMotorpool, _categoryLabelFunds],
    [-1, 2000, 500, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_totalFundsOpfor",
    "SLIDER",
    ["OPFOR Total Funds", "The funds available to OPFOR for spawning vehicles. To refund simply add the cost to this total. Set to -1 for infinite funds."],
    [_categoryLabelMotorpool, _categoryLabelFunds],
    [-1, 2000, 500, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_totalFundsIndep",
    "SLIDER",
    ["INDEP Total Funds", "The funds available to INDEP for spawning vehicles. To refund simply add the cost to this total. Set to -1 for infinite funds."],
    [_categoryLabelMotorpool, _categoryLabelFunds],
    [-1, 2000, 500, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_totalFundsCivil",
    "SLIDER",
    ["CIVIL Total Funds", "The funds available to CIVIL for spawning vehicles. To refund simply add the cost to this total. Set to -1 for infinite funds."],
    [_categoryLabelMotorpool, _categoryLabelFunds],
    [-1, 2000, 500, 0],
    true
] call CBA_settings_fnc_init;


// Advanced settings.
[
    "bwi_motorpool_restrictAccessTo",
    "EDITBOX",
    ["Restrict Access To", "List of units that can access the motorpool dialog to spawn vehicles. Leave blank for unrestricted access."],
    [_categoryLabelMotorpool, _categoryLabelRestrictions],
    "",
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_refundTimeLimit",
    "SLIDER",
    ["Refund Time Limit", "The amount of time in seconds that a vehicle can be returned for a refund. Set to -1 for infinite returns."],
    [_categoryLabelMotorpool, _categoryLabelRestrictions],
    [-1, 300, 180, 0],
    true
] call CBA_settings_fnc_init;


// Servicing settings.
[
    "bwi_motorpool_servicingRange",
    "SLIDER",
    ["Service Range", "Radius from the vehicle's spawn point that servicing is available within."],
    [_categoryLabelMotorpool, _categoryLabelServicing],
    [0, 100, 50, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_servicingInterval",
    "SLIDER",
    ["Service Interval", "Interval between servicing runs in seconds."],
    [_categoryLabelMotorpool, _categoryLabelServicing],
    [1, 30, 3, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_refuelRate",
    "SLIDER",
    ["Refuel Rate", "The amount of fuel delivered to the vehicle at each servicing run."],
    [_categoryLabelMotorpool, _categoryLabelServicing],
    [0, 1, 0.1, 2],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_repairRate",
    "SLIDER",
    ["Repair Rate", "The amount of damage removed from the vehicle at each servicing run."],
    [_categoryLabelMotorpool, _categoryLabelServicing],
    [0, 1, 0.1, 2],
    true
] call CBA_settings_fnc_init;

[
    "bwi_motorpool_rearmRate",
    "SLIDER",
    ["Rearm Rate", "The amount of ammunition delivered to the vehicle at each servicing run."],
    [_categoryLabelMotorpool, _categoryLabelServicing],
    [0, 1, 0.1, 2],
    true
] call CBA_settings_fnc_init;
