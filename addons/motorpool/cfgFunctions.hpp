class bwi_motorpool
{
	class functions
	{
		file = "\bwi_motorpool\functions";

		class addAction{};

		class getRemainingFunds{};
		class getSpentFunds{};
		class setSpentFunds{};
		class debitFunds{};
		class creditFunds{};

		class clearVehicleSpawn{};
		class spawnVehicle{};

		class configurePylon{};
		class serviceVehicle{};
		
		class setupCargo{};
		class setupFRIES{};
		class setupServicing{};

		class registerStagingHandlers{};

		class checkMissionConfig{};
	};

	class gui_functions
	{
		file = "\bwi_motorpool\gui\functions";
		class openMotorpoolDialog{};
		class refreshLocationsList{};
		class refreshVehiclesTree{};
		class refreshDetailsText{};
		class selectVehicleGUI{}; 
	};

	class xeh
	{
		file = "\bwi_motorpool\xeh";
		class initCBASettings{};
		class initMotorpool{};
	};
};

class bwi_motorpool_module
{
	class modules
	{
		file = "\bwi_motorpool\modules";
		class addMotorpool{};
	};
};