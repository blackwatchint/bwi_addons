class Extended_PreInit_EventHandlers {
    class bwi_motorpool_settings {
        init = "call bwi_motorpool_fnc_initCBASettings;";
    };
};

class Extended_PostInit_EventHandlers {
    class bwi_motorpool_init {
        init = "call bwi_motorpool_fnc_initMotorpool;";
    };
};