/**
 * Register event handlers that create an emergency medical action
 * This is only ran on the server, so others will not listen to the events
 */

// Add to staging flags
["bwi_staging_createStaging",
	{
		params ["_side", "_index", "_stagingArea", "_flagpole"];
		
		["Adding emergency treat medic action to %1", _flagpole] call bwi_common_fnc_log;
		// Add the action and broadcast.
		[_flagpole, [
		"<t color='#d50000'>Emergency treat medic</t>", 
		{ 
			params ["_target", "_caller"];
			private _nearestTarget = ([_target, allUnits, bwi_medical_emergencyMedicMaxRange, {(_x getVariable ["ACE_medical_hemorrhage", 0]) > 0 && _x getVariable ['ACE_medical_medicClass', 0] != 0}] call CBA_fnc_getNearest) select 0; 
			["%1 starting emergency treatment of %2", _caller, _nearestTarget] call bwi_common_fnc_log;
			["Emergency treating", bwi_medical_emergencyMedicTreatmentTime, {(_this select 0) call bwi_medical_fnc_canEmergencyTreatMedic}, {(_this select 0) call bwi_medical_fnc_onSuccessEmergencyTreatMedic}, {}, [_caller, _target, _nearestTarget]] call CBA_fnc_progressBar;
		}, 
		[], 1.8, false, true, "",
		"[_this, _target] call bwi_medical_fnc_canEmergencyTreatMedic"
		]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP
	}
] call CBA_fnc_addEventHandlerArgs;

// Add to outpost flags
["bwi_construction_buildingCompleted",
	{
		params ["_crate", "_unit", "_objects"];

		{
			// Found a staging flag, activate it.
			if ( typeOf _x == "BWI_Staging_OutpostFlag" ) then {
				["Adding emergency treat medic action to %1", _x] call bwi_common_fnc_log;
				// Add the action and broadcast.
				[_x, [
				"<t color='#d50000'>Emergency treat medic</t>", 
				{ 
					params ["_target", "_caller"];
					private _nearestTarget = ([_target, allUnits, bwi_medical_emergencyMedicMaxRange, {(_x getVariable ["ACE_medical_hemorrhage", 0]) > 0 && _x getVariable ['ACE_medical_medicClass', 0] != 0}] call CBA_fnc_getNearest) select 0; 
					["%1 starting emergency treatment of %2", _caller, _nearestTarget] call bwi_common_fnc_log;
					["Emergency treating", bwi_medical_emergencyMedicTreatmentTime, {(_this select 0) call bwi_medical_fnc_canEmergencyTreatMedic}, {(_this select 0) call bwi_medical_fnc_onSuccessEmergencyTreatMedic}, {}, [_caller, _target, _nearestTarget]] call CBA_fnc_progressBar;
				}, 
				[], 1.8, false, true, "",
				"[_this, _target] call bwi_medical_fnc_canEmergencyTreatMedic"
				]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP
			};
		} forEach _objects;
	}
] call CBA_fnc_addEventHandlerArgs;

// Add to outpost vehicles
["bwi_motorpool_spawnVehicle",
	{
		private _vehicle = _this select 0;
		private _isOutpost = _this select 6;

		if ( _isOutpost == 1 ) then {
			["Adding emergency treat medic action to %1", _vehicle] call bwi_common_fnc_log;
			// Add the action and broadcast.
			[_vehicle, [
			"<t color='#d50000'>Emergency treat medic</t>", 
			{ 
				params ["_target", "_caller"];
				private _nearestTarget = ([_target, allUnits, bwi_medical_emergencyMedicMaxRange, {(_x getVariable ["ACE_medical_hemorrhage", 0]) > 0 && _x getVariable ['ACE_medical_medicClass', 0] != 0}] call CBA_fnc_getNearest) select 0; 
				["%1 starting emergency treatment of %2", _caller, _nearestTarget] call bwi_common_fnc_log;
				["Emergency treating", bwi_medical_emergencyMedicTreatmentTime, {(_this select 0) call bwi_medical_fnc_canEmergencyTreatMedic}, {(_this select 0) call bwi_medical_fnc_onSuccessEmergencyTreatMedic}, {}, [_caller, _target, _nearestTarget]] call CBA_fnc_progressBar;
			}, 
			[], 1.8, false, true, "",
			"[_this, _target] call bwi_medical_fnc_canEmergencyTreatMedic"
			]] remoteExec ["addAction", [0,-2] select isDedicated, true]; // Players, JIP
		};
	}
] call CBA_fnc_addEventHandlerArgs;