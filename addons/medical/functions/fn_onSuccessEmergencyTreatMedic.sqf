params ["_callingPlayer", "_rangeObject", "_healingTarget"];

["%1 successfully emergency treated %2", _callingPlayer, _healingTarget] call bwi_common_fnc_log;
[_callingPlayer, _healingTarget] call ace_medical_treatment_fnc_fullHeal;