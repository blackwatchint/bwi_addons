params ["_medic", "_patient"];

private _originalSetting = ace_medical_treatment_advancedDiagnose;
// If we have the setting without IDing CA and we are a medic, we enable IDing CA
if (ace_medical_treatment_advancedDiagnose == 1 && _medic call ace_medical_treatment_fnc_isMedic) then
{
	ace_medical_treatment_advancedDiagnose = 2;
};
// Original function call and reset
_this call ace_medical_treatment_fnc_checkResponse;
ace_medical_treatment_advancedDiagnose = _originalSetting;