/*
 * Function called to check if emergency medic system can be used.
 * Will be called without a target if one still needs to be chosen (for action).
 * Will be called with target if one is already being treated.
*/

params ["_caller", "_target", "_healingTarget"];

if (_caller getVariable ['ACE_medical_medicClass', 0] != 0) exitWith {false};

private _hasValidTarget = false;
if (isNil "_healingTarget") then 
{
	// A valid target is someone who is hurt, within range, and a medic
	_hasValidTarget = count ([_target, allUnits, bwi_medical_emergencyMedicMaxRange, {(_x getVariable ["ACE_medical_hemorrhage", 0]) > 0 && _x getVariable ['ACE_medical_medicClass', 0] != 0}] call CBA_fnc_getNearest) > 0;
}
else
{
	_hasValidTarget = (_healingTarget distance _target) <= bwi_medical_emergencyMedicMaxRange;
};

_hasValidTarget