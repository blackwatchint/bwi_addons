// Modeled after ACE behavior in fnc_splintLocal.sqf
params ["_fractureArray", "_patient"];

_patient setVariable ["ace_medical_fractures", _fractureArray, true];
[_patient] call ace_medical_engine_fnc_updateDamageEffects;