params ["_args", "_elapsedTime", "_totalTime"];
_args params ["_medic", "_patient"];

// Original function
if (!(_this call ace_medical_treatment_fnc_surgicalKitProgress)) exitWith {false};

// Both settings disabled, so no need to run
if (!bwi_medical_surgeryFractureHeal) exitWith {true};

private _isBodypartDamaged = [0,0,0,0,0,0];
private _fractureArrayChanged = false;
private _fractureArray = +(_patient getVariable ["ace_medical_fractures", [0,0,0,0,0,0]]);

// Check if a body part is concidered damaged (not fully stitched)
// A bodypart with bandage is not stitched, so it is still damaged
{
    _x params ["_ID", "_bodyPart", "_amountOf"];
    
    // Wound id 2 is bruise (index in wound config in ACE_Medical_Injuries.hpp as parsed through fnc_parseConfigForInjuries.sqf)
    if (_ID != 2) then
    {
        _isBodypartDamaged set [_bodyPart, 1];
    };
}foreach (_patient getVariable ["ace_medical_bandagedWounds",[]]);
// A bodypart with open wound of amount > 0 is not stitched (nor bandaged), so it is still damaged
{
    _x params ["_ID", "_bodyPart", "_amountOf"];
    private _ClassID = floor(_ID / 10); //ID = ClassID * 10 + SizeID
    
    // Wound id 2 is bruise (index in wound config in ACE_Medical_Injuries.hpp as parsed through fnc_parseConfigForInjuries.sqf)
    if (_amountOf != 0 && _ClassID != 2) then
    {
        _isBodypartDamaged set [_bodyPart, 1];
    };
}foreach (_patient getVariable ["ace_medical_openWounds",[]]);

// Should probably be a normal loop instead, but this whole function is kindof un-optimized. Shouldn't run that much though so whatever.
{
    // No longer damaged, check trauma and fracture and remove if needed
    if (_x == 0) then
    {
        if (_fractureArray select _forEachIndex != 0) then
        {
            _fractureArray set [_forEachIndex, 0];
            _fractureArrayChanged = true;
        };
    };
}foreach _isBodypartDamaged;
if (_fractureArrayChanged && bwi_medical_surgeryFractureHeal) then
{
    ["bwi_medical_evt_updateFracturesLocal", [_fractureArray, _patient], _patient] call CBA_fnc_targetEvent;
};

true