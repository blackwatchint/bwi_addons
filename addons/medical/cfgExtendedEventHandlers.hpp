class Extended_PreInit_EventHandlers {
    class bwi_medical_settings {
        init = "call bwi_medical_fnc_initCBASettings;";
    };
};

class Extended_PostInit_EventHandlers {
    class bwi_medical_init {
        init = "call bwi_medical_fnc_initMedical;";
    };
};