class bwi_medical 
{
    class functions 
    {
        file = "\bwi_medical\functions";
        class aceWrapperSurgeryProgress{};
        class aceWrapperResponseSuccess{};
        class updateFracturesLocal{};

        class canEmergencyTreatMedic{};
        class onSuccessEmergencyTreatMedic{};
        class registerEmergencyTreatMedic{};
    };

    class xeh
    {
        file = "\bwi_medical\xeh";
        class initCBASettings{};
        class initMedical{};
    };
};