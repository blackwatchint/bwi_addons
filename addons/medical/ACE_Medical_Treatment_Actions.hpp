class ace_medical_treatment_Actions
{
    class FieldDressing;
    class SurgicalKit: FieldDressing
    {
        callbackProgress="bwi_medical_fnc_aceWrapperSurgeryProgress";
    };
    class CheckPulse;
    class CheckResponse: CheckPulse {
        callbackSuccess ="bwi_medical_fnc_aceWrapperResponseSuccess";
    };
};