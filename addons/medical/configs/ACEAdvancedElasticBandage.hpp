class ElasticBandage: fieldDressing {
    class Abrasion {
        effectiveness = 2.5;
        reopeningChance = 0.15;
        reopeningMinDelay = 240;
        reopeningMaxDelay = 360;
    };
    class AbrasionMinor: Abrasion {};
    class AbrasionMedium: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.2;
    };
    class AbrasionLarge: Abrasion {
        effectiveness = 1.25;
        reopeningChance = 0.25;
    };
    class Avulsion: Abrasion {
        effectiveness = 2.5;
        reopeningChance = 0.95;
        reopeningMinDelay = 60;
        reopeningMaxDelay = 180;
    };
    class AvulsionMinor: Avulsion {};
    class AvulsionMedium: Avulsion {
        effectiveness = 1.7;
        reopeningChance = 1;
    };
    class AvulsionLarge: Avulsion {
        effectiveness = 1.25;
        reopeningChance = 1;
    };
    class Contusion: Abrasion {
        effectiveness = 1;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class ContusionMinor: Contusion {};
    class ContusionMedium: Contusion {};
    class ContusionLarge: Contusion {};
    class Crush: Abrasion {
        effectiveness = 2.5;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class CrushMinor: Crush {};
    class CrushMedium: Crush {
        effectiveness = 1.7;
    };
    class CrushLarge: Crush {
        effectiveness = 1.25;
    };
    class Cut: Abrasion {
        effectiveness = 2.5;
        reopeningChance = 0.75;
        reopeningMinDelay = 180;
        reopeningMaxDelay = 300;
    };
    class CutMinor: Cut {};
    class CutMedium: Cut {
        effectiveness = 1.7;
        reopeningChance = 0.8;
    };
    class CutLarge: Cut {
        effectiveness = 1.25;
        reopeningChance = 0.85;
    };
    class Laceration: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.75;
        reopeningMinDelay = 120;
        reopeningMaxDelay = 240;
    };
    class LacerationMinor: Laceration {};
    class LacerationMedium: Laceration {
        effectiveness = 1.25;
        reopeningChance = 0.8;
    };
    class LacerationLarge: Laceration {
        effectiveness = 1.1;
        reopeningChance = 0.85;
    };
    class VelocityWound: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.95;
        reopeningMinDelay = 60;
        reopeningMaxDelay = 180;
    };
    class VelocityWoundMinor: VelocityWound {};
    class VelocityWoundMedium: VelocityWound {
        effectiveness = 1.25;
        reopeningChance = 1;
    };
    class VelocityWoundLarge: VelocityWound {
        effectiveness = 1.1;
        reopeningChance = 1;
    };
    class PunctureWound: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.95;
        reopeningMinDelay = 60;
        reopeningMaxDelay = 180;
    };
    class PunctureWoundMinor: PunctureWound {};
    class PunctureWoundMedium: PunctureWound {
        effectiveness = 1.25;
        reopeningChance = 1;
    };
    class PunctureWoundLarge: PunctureWound {
        effectiveness = 1.1;
        reopeningChance = 1;
    };
};