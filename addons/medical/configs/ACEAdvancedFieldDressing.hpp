class FieldDressing {
    class Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0;
        reopeningMaxDelay = 100;
        reopeningMinDelay = 100;
    };
    class AbrasionMinor: Abrasion {};
    class AbrasionMedium: Abrasion {
        effectiveness = 1.25;
    };
    class AbrasionLarge: Abrasion {
        effectiveness = 1.1;
    };
    class Avulsion: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.9;
        reopeningMinDelay = 180;
        reopeningMaxDelay = 300;
    };
    class AvulsionMinor: Avulsion {};
    class AvulsionMedium: Avulsion {
        reopeningChance = 1;
        effectiveness = 1.25;
    };
    class AvulsionLarge: Avulsion {
        reopeningChance = 1;
        effectiveness = 1.1;
    };
    class Contusion: Abrasion {
        effectiveness = 1;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class ContusionMinor: Contusion {};
    class ContusionMedium: Contusion {};
    class ContusionLarge: Contusion {};
    class Crush: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class CrushMinor: Crush {};
    class CrushMedium: Crush {
        effectiveness = 0.8;
    };
    class CrushLarge: Crush {
        effectiveness = 0.6;
    };
    class Cut: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.5;
        reopeningMinDelay = 540;
        reopeningMaxDelay = 660;
    };
    class CutMinor: Cut {};
    class CutMedium: Cut {
        effectiveness = 1.25;
        reopeningChance = 0.6;
    };
    class CutLarge: Cut {
        effectiveness = 1.1;
        reopeningChance = 0.65;
    };
    class Laceration: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.5;
        reopeningMinDelay = 420;
        reopeningMaxDelay = 540;
    };
    class LacerationMinor: Laceration {};
    class LacerationMedium: Laceration {
        effectiveness = 0.8;
        reopeningChance = 0.6;
    };
    class LacerationLarge: Laceration {
        effectiveness = 0.6;
        reopeningChance = 0.65;
    };
    class VelocityWound: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.9;
        reopeningMinDelay = 60;
        reopeningMaxDelay = 180;
    };
    class VelocityWoundMinor: VelocityWound {};
    class VelocityWoundMedium: VelocityWound {
        effectiveness = 0.8;
        reopeningChance = 1;
    };
    class VelocityWoundLarge: VelocityWound {
        effectiveness = 0.6;
        reopeningChance = 1;
    };
    class PunctureWound: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.9;
        reopeningMinDelay = 180;
        reopeningMaxDelay = 300;
    };
    class PunctureWoundMinor: PunctureWound {};
    class PunctureWoundMedium: PunctureWound {
        effectiveness = 0.8;
        reopeningChance = 1;
        reopeningMinDelay = 180;
        reopeningMaxDelay = 300;
    };
    class PunctureWoundLarge: PunctureWound {
        effectiveness = 0.6;
        reopeningChance = 1;
        reopeningMinDelay = 180;
        reopeningMaxDelay = 300;
    };
};