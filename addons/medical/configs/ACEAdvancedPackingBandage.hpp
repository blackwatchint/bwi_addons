class PackingBandage: fieldDressing {
    class Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class AbrasionMinor: Abrasion {};
    class AbrasionMedium: Abrasion {
        effectiveness = 1.25;
    };
    class AbrasionLarge: Abrasion {
        effectiveness = 1.1;
    };
    class Avulsion: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.45;
        reopeningMinDelay = 480;
        reopeningMaxDelay = 720;
    };
    class AvulsionMinor: Avulsion {};
    class AvulsionMedium: Avulsion {
        effectiveness = 0.8;
        reopeningChance = 0.5;
    };
    class AvulsionLarge: Avulsion {
        effectiveness = 0.6;
        reopeningChance = 0.55;
    };
    class Contusion: Abrasion {
        effectiveness = 1;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class ContusionMinor: Contusion {};
    class ContusionMedium: Contusion {};
    class ContusionLarge: Contusion {};
    class Crush: Abrasion {
        effectiveness = 0.6;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class CrushMinor: Crush {};
    class CrushMedium: Crush {
        effectiveness = 0.4;
    };
    class CrushLarge: Crush {
        effectiveness = 0.3;
    };
    class Cut: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.15;
        reopeningMinDelay = 720;
        reopeningMaxDelay = 960;
    };
    class CutMinor: Cut {};
    class CutMedium: Cut {
        effectiveness = 1.25;
        reopeningChance = 0.2;
    };
    class CutLarge: Cut {
        effectiveness = 1.1;
        reopeningChance = 0.25;
    };
    class Laceration: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.15;
        reopeningMinDelay = 600;
        reopeningMaxDelay = 840;
    };
    class LacerationMinor: Laceration {};
    class LacerationMedium: Laceration {
        effectiveness = 0.8;
        reopeningChance = 0.2;
    };
    class LacerationLarge: Laceration {
        effectiveness = 0.6;
        reopeningChance = 0.25;
    };
    class VelocityWound: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.55;
        reopeningMinDelay = 480;
        reopeningMaxDelay = 720;
    };
    class VelocityWoundMinor: VelocityWound {};
    class VelocityWoundMedium: VelocityWound {
        effectiveness = 0.8;
        reopeningChance = 0.6;
    };
    class VelocityWoundLarge: VelocityWound {
        effectiveness = 0.6;
        reopeningChance = 0.65;
    };
    class PunctureWound: Abrasion {
        effectiveness = 1.1;
        reopeningChance = 0.35;
        reopeningMinDelay = 600;
        reopeningMaxDelay = 840;
    };
    class PunctureWoundMinor: PunctureWound {};
    class PunctureWoundMedium: PunctureWound {
        effectiveness = 0.8;
        reopeningChance = 0.4;
    };
    class PunctureWoundLarge: PunctureWound {
        effectiveness = 0.6;
        reopeningChance = 0.45;
    };
};