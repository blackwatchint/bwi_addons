class QuikClot: fieldDressing {
    class Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class AbrasionMinor: Abrasion {};
    class AbrasionMedium: Abrasion {
        effectiveness = 1.25;
    };
    class AbrasionLarge: Abrasion {
        effectiveness = 1.1;
    };
    class Avulsion: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.65;
        reopeningMinDelay = 840;
        reopeningMaxDelay = 1320;
    };
    class AvulsionMinor: Avulsion {};
    class AvulsionMedium: Avulsion {
        effectiveness = 1.25;
        reopeningChance = 0.7;
    };
    class AvulsionLarge: Avulsion {
        effectiveness = 1.1;
        reopeningChance = 0.75;
    };
    class Contusion: Abrasion {
        effectiveness = 1;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class ContusionMinor: Contusion {};
    class ContusionMedium: Contusion {};
    class ContusionLarge: Contusion {};
    class Crush: Abrasion {
        effectiveness = 0.6;
        reopeningChance = 0;
        reopeningMinDelay = 100;
        reopeningMaxDelay = 100;
    };
    class CrushMinor: Crush {};
    class CrushMedium: Crush {
        effectiveness = 0.4;
    };
    class CrushLarge: Crush {
        effectiveness = 0.3;
    };
    class Cut: Abrasion {
        effectiveness = 2.5;
        reopeningChance = 0.35;
        reopeningMinDelay = 1200;
        reopeningMaxDelay = 1680;
    };
    class CutMinor: Cut {};
    class CutMedium: Cut {
        effectiveness = 1.7;
        reopeningChance = 0.4;
    };
    class CutLarge: Cut {
        effectiveness = 1.25;
        reopeningChance = 0.45;
    };
    class Laceration: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.35;
        reopeningMinDelay = 1080;
        reopeningMaxDelay = 1560;
    };
    class LacerationMinor: Laceration {};
    class LacerationMedium: Laceration {
        effectiveness = 1.25;
        reopeningChance = 0.4;
    };
    class LacerationLarge: Laceration {
        effectiveness = 1.1;
        reopeningChance = 0.45;
    };
    class VelocityWound: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.75;
        reopeningMinDelay = 840;
        reopeningMaxDelay = 1320;
    };
    class VelocityWoundMinor: VelocityWound {};
    class VelocityWoundMedium: VelocityWound {
        effectiveness = 1.25;
        reopeningChance = 0.8;
    };
    class VelocityWoundLarge: VelocityWound {
        effectiveness = 1.1;
        reopeningChance = 0.85;
    };
    class PunctureWound: Abrasion {
        effectiveness = 1.7;
        reopeningChance = 0.55;
        reopeningMinDelay = 960;
        reopeningMaxDelay = 1440;
    };
    class PunctureWoundMinor: PunctureWound {};
    class PunctureWoundMedium: PunctureWound {
        effectiveness = 1.25;
        reopeningChance = 0.6;
    };
    class PunctureWoundLarge: PunctureWound {
        effectiveness = 1.1;
        reopeningChance = 0.65;
    };
};