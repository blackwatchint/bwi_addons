## Uncounsciousness
### Current setting
* Chance of wakeup 15% and epi boost 5x. Means without epi there is a <4% chance they have not woken up within 5 minutes with epi this is the chance wihtin 1 minute with the 5 minute chance being 0.00000874767%.

### Causes
* New wound to chest or head with damage higher than pentration threshold gives chance of Critical Injury KO.
* New wound(s) which cause pain higher than pain unconscious gives chance of Critical Injury KO.
* Critical Injury KO chance succeeds if head damage (total) is more than 1/2 of threshold setting or body damage is more than threshold setting or 10% chance if total pain is more than pain unconscious
* Overdose of medication leads to Critical Vitals KO.
* Heartrate under 30 but over 20 has a chance to lead to Critical Vitals KO.
* Bloodloss (active rate) higher than blood loss KO threshold (currently 0.5) leads to Critical Vitals KO.

### Exit conditions
* Needs stable vitals which includes not loosing too much blood (active rate), not too low BP, not too low (40) HR.
* Every 15 seconds when the above is true a check is performed.
* This time for checks is divided through epi boost setting if epi is active in the patient's system.
* Every check has a chance set by the setting of waking up the patient.

### Effects
* If the unit is AI and AI KO setting is disabled it will cause death for the unit.

## Cardiac Arrest
### Causes
* Fatal vitals which includes HR under 20 or over 220, very high BP, very low BP, and Class IV hemorrhage (less than 3.6L left).
* If set in the settings (no Fatal injury set to always) Fatal Injury (see relevant chapter)

### Effects
* If there is no reset of the timer (successful CPR) the unit dies after the time from the setting.

### Exit conditions
* CPR can be performed and takes 15 seconds to perform and has a sucess chance determined by the setting. (if the unit still had the causes of CA they will go in again, but timer will be reset)
* If the blood volume goes below bleedout values (less than 3.6L left) the unit dies. (so watch out giving CPR with open wounds as you might push out enough blood to kill them, although I did not test if that's possible)

## Fractures
### Causes
* Certain wound types (cause fracture == 1 in config). Currently crush and velocity.
* Only on arms and legs.
* Chance can be change in settings.
* Received a wound with more damage than threshold (currently 0.5), of correct type, on applicable limb, and chance roll succeeded.

### Effects
* Visible on the medical UI as broken bone.
* Causes limp if on leg.
* Causes aim sway if on arm (more if both arms).

### Remedies
* Splint works as partial fix (with current fractures setting).
* Partial heal on leg solves limp, but blocks sprinting.
* Partial heal on arm halves (+-) effect of aim sway.
* Surgery fully removes the effect (! This is custom, not from ACE !)

## Fatal injuries
### Causes
#### Large hit damage source setting or Either
* A single wound to the head with damage over the head damage threshold.
* A single wound to the torso with damage over the organ damage threshold combined with a random roll over the heart hit chance (currently 0.05).
#### Sum of trauma or Either
* Total damage of all wounds to torso or head is higher than damage threshold multiplied by 1.25 or 1.5 respectively causes exponential chance.
* If clear trauma on bandage is set to true, bandaged wounds are not included in this total damage value (current plan is to instead clear this on surgery in BWI addon).

### Effects
* Depends on player/AI fatal injuries setting. Either causes death or CA.

## Limping
!!! TODO !!!
### Causes
* 

### Effects
* 

### Remedies
* 

## Pain
!!! TODO !!!
### Causes
* 

### Effects
*

### Remedies
*
