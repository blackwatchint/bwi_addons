class ace_medical_treatment
{
    class Bandaging
    {
        //Packing, Quikclot, and Elastic have dependencies on FieldDressing
        #include <configs\ACEAdvancedFieldDressing.hpp>
        #include <configs\ACEAdvancedPackingBandage.hpp>
        #include <configs\ACEAdvancedElasticBandage.hpp>
        #include <configs\ACEAdvancedQuikClot.hpp>
    };
};