// Define category strings for the CBA settings.
private _categoryLabelMedical = "BWI Medical";
private _categoryLabelEmergencyMedic = "Emergency Medic";
private _categoryLabelAceExpansion = "ACE Expansion";


// Initialize CBA settings.
// Advanced settings.
[
    "bwi_medical_emergencyMedicMaxRange",
    "SLIDER",
    ["Maximum Range", "The maximum from the flagpole within which wounded medics are healed."],
    [_categoryLabelMedical, _categoryLabelEmergencyMedic],
    [5, 50, 20, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_medical_emergencyMedicTreatmentTime",
    "SLIDER",
    ["Treatment Time", "The time it takes to heal someone with the emergency system."],
    [_categoryLabelMedical, _categoryLabelEmergencyMedic],
    [5, 120, 30, 0],
    true
] call CBA_settings_fnc_init;

[
    "bwi_medical_surgeryFractureHeal",
    "CHECKBOX",
    ["Surgery Heals Fracture", "Surgery will fully heal a fracture returning to the effect before the fracture occured. Should be used in combination with ACE Splint Heals But Can't Run."],
    [_categoryLabelMedical, _categoryLabelAceExpansion],
    [true],
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;
