["bwi_medical_evt_updateFracturesLocal", bwi_medical_fnc_updateFracturesLocal] call CBA_fnc_addEventHandler;


// Run on the server only.
if ( isServer ) then {
	// Register the handlers for activation of staging areas.
	call bwi_medical_fnc_registerEmergencyTreatMedic;
};