class CfgPatches {
	class bwi_medical {
		requiredVersion = 1;
		authors[] = { "RedBery", "Fourjays"};
		authorURL = "http://blackwatch-int.com";
		author = "Black Watch International";
		version = 2.1;
		versionStr = "2.1.0";
		versionAr[] = {2,1,0};
		requiredAddons[] = {
			"ace_medical",
			"ace_medical_treatment",
			"ace_medical_damage"
		};
		units[] = {};
	};
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgWeapons
{
	#include <cfgWeapons.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>

// ACE Medical Configuration
#include <ACE_Medical_Treatment.hpp>
#include <ACE_Medical_Treatment_Actions.hpp>