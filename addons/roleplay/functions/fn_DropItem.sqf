/*
    Drops a Ace interactable weapons container
    similar to the Roleplay item in inventory.

    Arguments:
    0: Roleplay item to be dropped <Object>
    1: Player


    Returns:
    <NONE>
*/

params ["_item", "_player"];

createVehicle [_item, position _player, [], 0, "CAN_COLLIDE"];
player removeItem _item;