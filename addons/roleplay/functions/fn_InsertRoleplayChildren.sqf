params ['_target', '_player', '_actionParams'];

private _actionItems = [];
private _actions = [];
//Get unique RP items in inventory
{
    _config = (configFile >> 'CfgWeapons' >> _x);
    if (inheritsFrom _config == (configFile >> 'CfgWeapons' >> 'BWI_RoleplayBaseItem') ) then {
        _actionItems pushBackUnique _x;
    };
}forEach (items _player);

{
    _config = (configFile >> 'CfgWeapons' >> _x);
    private _actionName = 'Carefully place ' + (_x);
    private _displayName = 'Carefully place ' + ([_config, 'displayName', 'Roleplay Item'] call BIS_fnc_returnConfigEntry);
    private _statement = {[_this select 2, _this select 1] call BWI_roleplay_fnc_DropItem;};
    private _newAction = [_actionName, _displayName, '',_statement, {true}, {}, _x] call ace_interact_menu_fnc_createAction;

    _actions pushBack [_newAction, [], _target];
} forEach _actionItems;

_actions