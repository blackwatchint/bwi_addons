class Item_Base_F;
class Man;

class CAManBase: Man {
    class ACE_SelfActions {
        class ACE_Equipment {
            class BWI_Action_DropItem {
                displayName = "Drop Item";
                condition = "true";
                statment = "";
                showDisabled = 0;
                priority = 2.5;
                insertChildren = "_this call BWI_roleplay_fnc_InsertRoleplayChildren";
            };
        };
    };
};

class BWI_RoleplayBaseVehicle: Item_Base_F {

    //Make CBA EH comp
    class EventHandlers {
        class CBA_Extended_EventHandlers: CBA_Extended_EventHandlers {};
    };

    // Set General RP item values
    editorCategory = "EdCat_BwiRoleplay";
    editorSubcategory = "EdSubcat_BwiItems";
    //Remove weapon box when empty
    forceSupply = 1;
    scope = 0;
    scopeCurator = 0;

    // Set ACE dragging and Carrying, these can be disabled on a per item basis
    ace_dragging_canDrag = 1;  // Can be dragged (0-no, 1-yes)
    ace_dragging_dragPosition[] = {0, 1.2, 0};  // Offset of the model from the body while dragging (same as attachTo)
    ace_dragging_dragDirection = 0;  // Model direction while dragging (same as setDir after attachTo)
    ace_dragging_canCarry = 1;  // Can be carried (0-no, 1-yes)
    ace_dragging_carryPosition[] = {0, 1.2, 1};  // Offset of the model from the body while dragging (same as attachTo)
    ace_dragging_carryDirection = 0;  // Model direction while dragging (same as setDir after attachTo)

    icon = "\bwi_roleplay\data\icon_loadout_w.paa";
};

#include <configs\vehicles\BWI_Camera.hpp>
#include <configs\vehicles\BWI_Documents.hpp>
//#include <configs/vehicles/BWI_Explosives.hpp>
#include <configs\vehicles\BWI_FlightRecorder.hpp>
#include <configs\vehicles\BWI_Intel.hpp>
#include <configs\vehicles\BWI_LongRangeRadio.hpp>
#include <configs\vehicles\BWI_Microphone.hpp>
#include <configs\vehicles\BWI_Money.hpp>
#include <configs\vehicles\BWI_PhoneOld.hpp>
#include <configs\vehicles\BWI_PhoneSmart.hpp>
#include <configs\vehicles\BWI_Suitcase.hpp>
#include <configs\vehicles\BWI_Tablet.hpp>
