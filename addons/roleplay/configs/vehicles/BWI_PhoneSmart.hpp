class BWI_Item_PhoneSmart: BWI_RoleplayBaseVehicle {
    displayName = "Smartphone";
    model = "\A3\Structures_F\Items\Electronics\MobilePhone_smart_F.p3d";
    scope = 2;
    ace_dragging_canDrag = 0;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/smartphone-screen_18079#
    icon = "\bwi_roleplay\data\smartphone-screen.paa";
    class TransportItems {
        class BWI_PhoneSmart {
            name = "BWI_PhoneSmart";
            count = 1;
        };
    };
};