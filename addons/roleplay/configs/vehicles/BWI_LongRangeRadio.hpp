class BWI_Item_LongRangeRadio: BWI_RoleplayBaseVehicle {
    displayName = "Long Range Radio";
    model = "\A3\Structures_F\Items\Electronics\PortableLongRangeRadio_F.p3d";
    scope = 2;
    ace_dragging_canDrag = 0;
    scopeCurator = 2;
    
    //Credit https://www.flaticon.com/free-icon/military-radio_1516#
    icon = "\bwi_roleplay\data\longRangeRadio.paa";
    class TransportItems {
        class BWI_LongRangeRadio {
            name = "BWI_LongRangeRadio";
            count = 1;
        };
    };
};