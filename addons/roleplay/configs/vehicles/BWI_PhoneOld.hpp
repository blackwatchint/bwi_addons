class BWI_Item_PhoneOld: BWI_RoleplayBaseVehicle {
    displayName = "Old Phone";
    model = "\A3\Structures_F\Items\Electronics\MobilePhone_old_F.p3d";
    scope = 2;
    ace_dragging_canDrag = 0;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/telephone_684812#
    icon = "\bwi_roleplay\data\telephone.paa";
    class TransportItems {
        class BWI_PhoneOld {
            name = "BWI_PhoneOld";
            count = 1;
        };
    };
};