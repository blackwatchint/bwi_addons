class BWI_Item_Explosives: BWI_RoleplayBaseVehicle {
    displayName = "Explosives";
    model = "\CA\misc2\explosive\explosive.p3d";
    scope = 2;
    scopeCurator = 2;
    
    //Credit https://www.flaticon.com/free-icon/explosion_71300#
    icon = "\bwi_roleplay\data\explosion.paa";
    class TransportItems {
        class BWI_Explosives {
            name = "BWI_Explosives";
            count = 1;
        };
    };
};