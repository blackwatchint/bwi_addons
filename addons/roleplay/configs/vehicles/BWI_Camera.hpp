class BWI_Item_Camera: BWI_RoleplayBaseVehicle {
    displayName = "Camera";
    model = "\A3\Structures_F_Heli\Items\Electronics\Camera_01_F.p3d";
    scope = 2;
    scopeCurator = 2;
    ace_dragging_canDrag = 0;

    //Credit https://www.flaticon.com/free-icon/photo-camera_149098#
    icon = "\bwi_roleplay\data\photo-camera.paa";
    class TransportItems {
        class BWI_Camera {
            name = "BWI_Camera";
            count = 1;
        };
    };
};