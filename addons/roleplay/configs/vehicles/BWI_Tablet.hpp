class BWI_Item_Tablet: BWI_RoleplayBaseVehicle {
    displayName = "Tablet";
    model = "\A3\Props_F_Exp_A\Military\Equipment\Tablet_02_F.p3d";
    scope = 2;
    scopeCurator = 2;
    ace_dragging_canDrag = 0;

    // Credit https://www.flaticon.com/free-icon/tablet_319#
    icon = "\bwi_roleplay\data\tablet.paa";
    class TransportItems {

        class BWI_Tablet {
            name = "BWI_Tablet";
            count = 1;
        };
    };
};