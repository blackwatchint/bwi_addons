class BWI_Item_Money: BWI_RoleplayBaseVehicle {
    displayName = "Money";
    model = "\A3\Structures_F\Items\Valuables\Money_F.p3d";
    scope = 2;
    ace_dragging_canDrag = 0;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/dollar-symbol_126179#
    icon = "\bwi_roleplay\data\dollar-symbol.paa";
    class TransportItems {
        class BWI_Money {
            name = "BWI_Money";
            count = 1;
        };
    };
};