class BWI_Item_Documents: BWI_RoleplayBaseVehicle {
    displayName = "Documents";
    model = "\A3\Structures_F_EPC\Items\Documents\Document_01_F.p3d";
    scope = 2;
    scopeCurator = 2;
    ace_dragging_canDrag = 0;

    //Credit https://www.flaticon.com/free-icon/contract_757257#
    icon = "\bwi_roleplay\data\contract.paa";
    class TransportWeapons {
        class BWI_Documents {
            weapon = "BWI_Documents";
            count = 1;
        };
    };
};