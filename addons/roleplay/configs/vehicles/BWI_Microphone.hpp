class BWI_Item_Microphone: BWI_RoleplayBaseVehicle {
    displayName = "Microphone";
    model = "\ca\Misc_E\Mikrofon_ws_EP1.p3d";
    scope = 2;
    ace_dragging_canDrag = 0;
    scopeCurator = 2;
    
    //Credit https://www.flaticon.com/free-icon/microphone_263052#
    icon = "\bwi_roleplay\data\microphone.paa";
    class TransportItems {
        class BWI_Money {
            name = "BWI_Microphone";
            count = 1;
        };
    };
};