class BWI_Item_FlightRecorder: BWI_RoleplayBaseVehicle {
    displayName = "Flight Recorder";
    model = "\rhsafrf\addons\rhs_air\misc\rhs_flightrecorder.p3d";
    scope = 2;
    scopeCurator = 2;
    
    //Credit https://www.flaticon.com/free-icon/rec-button_99720
    icon = "\bwi_roleplay\data\rec-button.paa";
    class TransportItems {
        class BWI_FlightRecorder {
            name = "BWI_FlightRecorder";
            count = 1;
        };
    };
};