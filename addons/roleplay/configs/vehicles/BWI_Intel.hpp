class BWI_Item_Intel: BWI_RoleplayBaseVehicle {
    displayName = "Intel";
    model = "\A3\Structures_F\Items\Documents\File1_F.p3d";
    scope = 2;
    ace_dragging_canDrag = 0;
    scopeCurator = 2;
    
    //Credit https://www.flaticon.com/free-icon/contract_757257#
    icon = "\bwi_roleplay\data\contract.paa";
    class TransportItems {
        class BWI_Intel {
            name = "BWI_Intel";
            count = 1;
        };
    };
};