class NATO_Box_Base;
class BWI_Suitcase: NATO_Box_Base {
    displayName = "Suitcase";
    model = "\A3\Structures_F\Items\Luggage\Suitcase_F.p3d";
    scope = 2;
    scopeCurator = 2;
    ace_cargo_size = 1;
    forceSupply = 0; //Do not delete when empty

    // Credit https://www.flaticon.com/free-icon/briefcase_123846
    icon = "\bwi_roleplay\data\briefcase.paa";
    maximumLoad = 250;
    editorCategory = "EdCat_BwiRoleplay";
    editorSubcategory = "EdSubcat_BwiItems";

    // Set ACE dragging and Carrying, these can be disabled on a per item basis
    ace_dragging_canDrag = 1;  // Can be dragged (0-no, 1-yes)
    ace_dragging_dragPosition[] = {0, 1.2, 0};  // Offset of the model from the body while dragging (same as attachTo)
    ace_dragging_dragDirection = 0;  // Model direction while dragging (same as setDir after attachTo)
    ace_dragging_canCarry = 1;  // Can be carried (0-no, 1-yes)
    ace_dragging_carryPosition[] = {0, 1.2, 1};  // Offset of the model from the body while dragging (same as attachTo)
    ace_dragging_carryDirection = 0;  // Model direction while dragging (same as setDir after attachTo)
    class TransportItems {

    };
};