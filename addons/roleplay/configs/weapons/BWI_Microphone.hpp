class BWI_Microphone: BWI_RoleplayBaseItem {
    displayName = "Microphone";
    model = "\ca\Misc_E\Mikrofon_ws_EP1.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/microphone_263052#
    picture = "\bwi_roleplay\data\microphone.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 7; //Set weight 10 = ~400g
        };
};