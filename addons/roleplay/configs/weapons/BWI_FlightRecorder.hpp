class BWI_FlightRecorder: BWI_RoleplayBaseItem {
    displayName = "Flight Recorder";
    model = "\rhsafrf\addons\rhs_air\misc\rhs_flightrecorder.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/rec-button_99720
    picture = "\bwi_roleplay\data\rec-button.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 125; //Set weight 10 = ~400g
        };
};