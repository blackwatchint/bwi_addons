class BWI_LongRangeRadio: BWI_RoleplayBaseItem {
    displayName = "Long Range Radio";
    model = "\A3\Structures_F\Items\Electronics\PortableLongRangeRadio_F.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/military-radio_1516#
    picture = "\bwi_roleplay\data\longRangeRadio.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 5; //Set weight 10 = ~400g
        };
};