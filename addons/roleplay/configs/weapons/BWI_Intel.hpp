class BWI_Intel: BWI_RoleplayBaseItem {
    displayName = "Intel";
    model = "\A3\Structures_F\Items\Documents\File1_F.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/contract_757257#
    picture = "\bwi_roleplay\data\contract.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 1; //Set weight 10 = ~400g
        };
};