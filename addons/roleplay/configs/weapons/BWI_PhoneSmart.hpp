class BWI_PhoneSmart:  BWI_RoleplayBaseItem {
    displayName = "Smartphone";
    model = "\A3\Structures_F\Items\Electronics\MobilePhone_smart_F.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/smartphone-screen_18079#
    picture = "\bwi_roleplay\data\smartphone-screen.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 3; //Set weight 10 = ~400g
        };
};