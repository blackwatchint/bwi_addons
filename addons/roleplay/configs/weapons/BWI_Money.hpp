class BWI_Money: BWI_RoleplayBaseItem {
    displayName = "Money";
    model = "\A3\Structures_F\Items\Valuables\Money_F.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/dollar-symbol_126179#
    picture = "\bwi_roleplay\data\dollar-symbol.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 4; //Set weight 10 = ~400g
        };
};