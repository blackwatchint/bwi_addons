class BWI_PhoneOld:  BWI_RoleplayBaseItem {
    displayName = "Old Phone";
    model = "\A3\Structures_F\Items\Electronics\MobilePhone_old_F.p3d";
    scope = 2;
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/telephone_684812#
    picture = "\bwi_roleplay\data\telephone.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 3; //Set weight 10 = ~400g
        };
};