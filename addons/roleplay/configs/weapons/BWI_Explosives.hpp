class BWI_Explosives: BWI_RoleplayBaseItem {
    displayName = "Explosives";
    scope = 2;
    model = "\CA\misc2\explosive\explosive.p3d";
    scopeCurator = 2;


    //Credit https://www.flaticon.com/free-icon/explosion_71300#
    picture = "\bwi_roleplay\data\explosion.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 25; //Set weight 10 = ~400g
        };
};