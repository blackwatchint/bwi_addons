class BWI_Camera: BWI_RoleplayBaseItem {
    displayName = "Camera";
    scope = 2;
    model = "\A3\Structures_F_Heli\Items\Electronics\Camera_01_F.p3d";
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/photo-camera_149098#
    picture = "\bwi_roleplay\data\photo-camera.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 15; //Set weight 10 = ~400g
        };
};