class BWI_Tablet:  BWI_RoleplayBaseItem {

    displayName = "Tablet";
    model = "\A3\Props_F_Exp_A\Military\Equipment\Tablet_02_F.p3d";
    scope = 2;
    scopeCurator = 2;

    // Credit https://www.flaticon.com/free-icon/tablet_319#
    picture = "\bwi_roleplay\data\tablet.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
        mass = 15; //Set weight 10 = ~400g
    };
};