class BWI_Documents: BWI_RoleplayBaseItem {
    displayName = "Documents";
    scope = 2;
    model = "\A3\Structures_F_EPC\Items\Documents\Document_01_F.p3d";
    scopeCurator = 2;

    //Credit https://www.flaticon.com/free-icon/contract_757257#
    picture = "\bwi_roleplay\data\contract.paa";
    class ItemInfo: CBA_MiscItem_ItemInfo {
            mass = 3; //Set weight 10 = ~400g
        };
};