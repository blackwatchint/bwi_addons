//Base Classes
class ACE_ItemCore;
class CBA_MiscItem_ItemInfo;

//Base Class with defaults
class BWI_RoleplayBaseItem: ACE_ItemCore {
    allowedSlots[]= {701,801,901};
    scope = 0;
    scopeCurator = 0;

    picture = "\bwi_roleplay\data\bwi.paa";

    class WeaponSlotsInfo {
            mass = 10;
        };
};

#include <configs\weapons\BWI_Camera.hpp>
#include <configs\weapons\BWI_Documents.hpp>
//#include <configs/weapons/BWI_Explosives.hpp>
#include <configs\weapons\BWI_FlightRecorder.hpp>
#include <configs\weapons\BWI_Intel.hpp>
#include <configs\weapons\BWI_LongRangeRadio.hpp>
#include <configs\weapons\BWI_Microphone.hpp>
#include <configs\weapons\BWI_Money.hpp>
#include <configs\weapons\BWI_PhoneOld.hpp>
#include <configs\weapons\BWI_PhoneSmart.hpp>
#include <configs\weapons\BWI_Tablet.hpp>
