class CfgPatches {
	class bwi_roleplay {
		requiredVersion = 1.0;
		authors[] = { "Dhorkiy" };
		authorURL = "http://blackwatch-int.com";
		author = "Black Watch International";
		version = 1.0;
		versionStr = "1.0";
		versionAr[] = {1,0};
		requiredAddons[] = {
            "ace_common",
            "ace_interaction",
            "ace_cargo",
            "3DEN"
		};
		units[] = {
		"BWI_Item_Camera",
		"BWI_Item_Documents",
		//"BWI_Item_Explosives",
		"BWI_Item_FlightRecorder",
		"BWI_Item_Intel",
		"BWI_Item_LongRangeRadio",
		"BWI_Item_Microphone",
		"BWI_Item_Money",
		"BWI_Item_PhoneOld",
		"BWI_Item_PhoneSmart",
		"BWI_Suitcase",
		"BWI_Item_Tablet"
		};
	};
};

//Ref for CBA XEH
class CfgEditorCategories {
    #include <cfgEditorCategories.hpp>
};

class CfgEditorSubcategories {
    #include <cfgEditorSubcategories.hpp>
};

class CBA_Extended_EventHandlers;
class CfgVehicles {
	#include <cfgVehicles.hpp>
};

class CfgWeapons {
    #include <cfgWeapons.hpp>
};
class cfgFunctions {
    #include <cfgFunctions.hpp>
};

class Extended_Init_EventHandlers {
    class BWI_RoleplayBaseVehicle {
        class bwi_roleplay {
            init = "_this call ace_dragging_fnc_initObject";
        };
    };
};