class UK3CB_BAF_MAN_HX58_Base;


/**
 * CARS
 */
class UK3CB_BAF_LandRover_Base: Car_F {	 // Land Rover and all variants
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_BAF_Jackal_Base: Car_F { // Code affects the Coyote as well
	class Reflectors : Reflectors
	{
		class Left : Left
		{
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_BAF_Panther_Base: MRAP_01_base_F { // Panther MRAP
	class Reflectors {
		class Left1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_BAF_Husky_Base: MRAP_01_base_F { // Husky TSV MRAP
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * TRUCKS
 */
class UK3CB_BAF_MAN_HX60_Base: Truck_01_base_F {
	class Reflectors {
		class Left1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_BAF_MAN_HX58_Transport_Base: UK3CB_BAF_MAN_HX58_Base {
	class Reflectors {
		class Left1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_BAF_MAN_HX60_Transport_Base: UK3CB_BAF_MAN_HX60_Base {
	class Reflectors {
		class Left1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * APCs
 */
class UK3CB_BAF_FV423_Mk3_Base : APC_Tracked_01_base_F { // Might be unused?
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_BAF_FV432_Mk3_Base : APC_Tracked_01_base_F { // Bulldog
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * IFVs
 */
class UK3CB_BAF_Warrior_A3_Base : APC_Tracked_03_base_F { // Warrior
	/* Inherits light configs directly from APC_Tracked_03_base_F.
	 * Will always throw inheritance errors if you try to change light configs here.
	 */
};


/**
 * HELICOPTERS
 */
class UK3CB_BAF_Apache_base: Heli_Attack_01_base_F {
	class Reflectors
	{
		class Middle {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class UK3CB_BAF_Merlin_Base: Heli_Transport_02_base_F {
	class Reflectors {
		class Landing_Light_Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class UK3CB_BAF_Wildcat_Base: Heli_light_03_base_F {
	class Reflectors {
		class Landing_Light {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};


/**
 * BOATS
 */
class UK3CB_BAF_RHIB_Base: Boat_Armed_01_base_F {
	class Reflectors {
		class Spotlight {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(50,75,2.5,110,10)
		};
	};
};