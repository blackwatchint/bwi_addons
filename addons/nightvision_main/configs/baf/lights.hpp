
// 3CB BAF Main rifle flashlight
class UK3CB_BAF_LLM_Flashlight_Tan: ItemCore
{
	class ItemInfo : InventoryFlashLightItem_Base_F
	{
		class FlashLight
		{
			color[] = {190, 130, 95};
			
			daylight = 0;
			direction = "flash";
			flaremaxdistance = 200;
			flaresize = 0.8;	

			position = "flash dir";
			scale[] = {0};
			size = 1;
			useflare = 1;
			
			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};
};

// 3CB BAF Glock flashlight
class UK3CB_BAF_Flashlight_L131A1: ItemCore
{
	class ItemInfo : InventoryFlashLightItem_Base_F
	{
		class FlashLight
		{
			color[] = {255, 255, 213};
			
			daylight = 0;
			direction = "flash";
			flaremaxdistance = 200;
			flaresize = 0.8;
			
			position = "flash dir";
			scale[] = {0};
			size = 1;
			useflare = 1;
			
			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};

};