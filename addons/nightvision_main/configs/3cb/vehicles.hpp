class rhs_uaz_open_Base;
class rhsgref_BRDM2 : Wheeled_APC_F
{
	class Reflectors;
	class Left;
	class Left2;
	class Right;
	class Right2;
};
class I_Plane_Fighter_03_AA_F;
class I_Plane_Fighter_03_CAS_F;
class I_Plane_Fighter_03_dynamicLoadout_F;
class rhs_mi8amt_base : RHS_Mi8_base
{
	class Reflectors;
	class Left;
	class Right;
};


/**
 * CARS
 */
class UK3CB_Motorbike_Base: Quadbike_01_base_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(50,80,2.5,60,10)
		};
	};
};
class UK3CB_Datsun_Base: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Datsun_Closed: UK3CB_Datsun_Base {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Datsun_Open: UK3CB_Datsun_Base {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Sedan_Base: Car_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
	};
};
class UK3CB_SUV_Base: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Skoda_Base: Car_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
	};
};
class UK3CB_S1203_BASE: Car_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
	};
};
class UK3CB_Gaz24: Car_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
	};
};
class UK3CB_Hilux_Base: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_LandRover_Base: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_GAZ_Vodnik_Base: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Spotlight {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(50,75,2.5,110,10)
		};
	};
};
class UK3CB_Hatchback: UK3CB_Sedan_Base {
	class Reflectors {
		class Left1 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Lada_Base: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Golf: Car_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
	};
};
class UK3CB_BTR40: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_Willys_Jeep_Base: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_M151_Jeep_Base: Car_F {
	class Reflectors {
		class LightCarHeadL01 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * TRUCKS
 */
class UK3CB_Ural_Base: Truck_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_Ikarus_Base: Truck_02_base_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
	};
};
class UK3CB_MAZ_543_SCUD: Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_V3S_Base: Truck_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_M939_Base: Truck_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_MTVR_Base: Truck_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * MRAPs
 */
class UK3CB_MaxxPro_Base: MRAP_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * APCs
 */
class UK3CB_BRDM2: rhsgref_BRDM2 {
	// Inherits lights from RHS BRDM.
};
class UK3CB_MTLB_PKT: APC_Tracked_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_AAV: APC_Tracked_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * IFVs
 */
class UK3CB_T34: MBT_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * MBTs
 */
class UK3CB_T55: MBT_02_base_F {
	class Reflectors {
		class DriverLight {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class UK3CB_T72A: MBT_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class UK3CB_M60a3: Tank_F { // M60a1 inherits from this
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * HELICOPTERS
 */
class UK3CB_UH1H_M240_Base: Helicopter_Base_H {
	class Reflectors {
		class Front {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,5e-2,200,25)
		};
	};
};
class UK3CB_Mi8AMT_Base : rhs_mi8amt_base {
	class Reflectors : Reflectors {
		class Left : Left{
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,5e-2,200,25)
		};
		class Right: Left { };
	};
};


/**
 * BOATS
 */
class UK3CB_RHIB: Boat_Armed_01_base_F {
	class Reflectors {
		class Spotlight {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(50,75,2.5,110,10)
		};
	};
};