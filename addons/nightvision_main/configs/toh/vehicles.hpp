/**
 * HELICOPTERS
 */
class JK_UH1H_base: Helicopter_Base_H { // UH-1H, UH-1D
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class JK_AB212_base: Helicopter_Base_H { // UH-1N
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class JK_Heli_Medium_01_base_F: Helicopter_Base_H { // CH-146
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};