// Soft orange tinted headlights.
// Good for older vehicles, and vehicles with incandescent lights.
#define COLOR_ORANGE {190,130,95}

// Soft blue-white tinted headlights.
// Good for helicopters, spotlights, or vehicles with modern LED lights.
#define COLOR_BLUE {70, 75, 100}

#define MACRO_LIGHTVALUESVARS(INNERANGLE,OUTERANGLE,INTENSITY,START,HARDLIMITSTART) \
ambient[] = {5,5,5}; \
innerAngle = INNERANGLE; \
outerAngle = OUTERANGLE; \
intensity = INTENSITY; \
coneFadeCoef = 2; \
class Attenuation \
{ \
	start = START; \
	constant = 0; \
	linear = 0; \
	quadratic = 4; \
	hardLimitStart = HARDLIMITSTART; \
	hardLimitEnd = START; \
};