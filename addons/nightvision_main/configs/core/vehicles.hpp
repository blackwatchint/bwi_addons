class Car;
class Car_F: Car
{
	class Reflectors;
	class Left;
	class Right;
};
class Quadbike_01_base_F;
class Offroad_01_base_F;

class Truck_F;
class Truck_01_base_F;
class Truck_02_base_F;
class Truck_02_Ammo_base_F;
class O_Truck_02_covered_F;
class Truck_03_base_F;

class MRAP_01_base_F;
class MRAP_02_base_F;

class Tank_F;
class Wheeled_APC_F: Car_F
{
	class Reflectors;
};
class APC_Tracked_01_base_F;
class APC_Tracked_02_base_F;
class APC_Tracked_03_base_F;

class MBT_01_base_F;
class MBT_02_base_F;
class MBT_01_arty_base_F;

class Helicopter_Base_F;
class Helicopter_Base_H;
class Heli_Transport_02_base_F;
class Heli_Attack_01_base_F;
class Heli_Attack_02_base_F;
class Heli_Light_01_base_F;
class Heli_Light_02_base_F;
class Heli_light_03_base_F;

class Plane_Base_F;
class Plane_CAS_01_base_F;
class Plane_Fighter_03_base_F;
class O_Plane_CAS_02_F;

class Boat_Armed_01_base_F;