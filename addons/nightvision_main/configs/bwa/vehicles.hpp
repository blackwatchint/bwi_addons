/**
 * CARS
 */
class BWA3_Eagle_base : Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};

class BWA3_Dingo2_base : Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * IFVs
 */
class BWA3_Puma_base : Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * MBTs
 */
class BWA3_Leopard_base : Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
