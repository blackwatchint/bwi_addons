class BWA3_acc_LLM01_irlaser;
class BWA3_acc_VarioRay_irlaser;

// BWA GER LLM01
class BWA3_acc_LLM01_flash: BWA3_acc_LLM01_irlaser
{
	class ItemInfo : InventoryFlashLightItem_Base_F
	{
		class FlashLight
		{
			color[] = {180,156,120};
			
			daylight = 0;
			direction = "flash";
			flaremaxdistance = 200;
			flaresize = 0.8;

			position = "flash dir";
			scale[] = {0};
			size = 1;
			useflare = 1;

			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};
};

// BWA GER Vario Ray
class BWA3_acc_VarioRay_flash: BWA3_acc_VarioRay_irlaser
{
	class ItemInfo : InventoryFlashLightItem_Base_F
	{
		class FlashLight
		{
			color[] = {180,156,120};
			
			daylight = 0;
			direction = "flash";
			flaremaxdistance = 200;
			flaresize = 0.8;

			position = "flash dir";
			scale[] = {0};
			size = 1;
			useflare = 1;

			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};
};