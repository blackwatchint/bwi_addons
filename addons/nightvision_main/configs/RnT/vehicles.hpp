/**
 * CARS
 */
class Redd_Tank_LKW_leicht_gl_Wolf_Base: Car_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * TRUCKS
 */
class rnt_lkw_5t_mil_gl_kat_i_base: Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
	};
};
class rnt_lkw_7t_mil_gl_kat_i_base: Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
	};
};
class rnt_lkw_10t_mil_gl_kat_i_base: Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1.5,110,10)
		};
	};
};


/**
 * APCs
 */
class Redd_Tank_Fuchs_1A4_Base: Wheeled_APC_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rnt_sppz_2a2_luchs_Base: Wheeled_APC_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class Redd_Tank_Wiesel_1A2_TOW_base: Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class Redd_Tank_Wiesel_1A4_MK20_base: Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * SPAA
 */
class Redd_Tank_Gepard_1A2_base: Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * IFVs
 */
class Redd_Marder_1A5_base: Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_2 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left_3 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};