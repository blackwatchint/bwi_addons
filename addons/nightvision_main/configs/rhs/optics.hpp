class rhsusf_acc_sniper_base;


class rhsusf_acc_ACOG: rhsusf_acc_sniper_base
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class elcan_scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_ACOG_3d: rhsusf_acc_ACOG
{
    class ItemInfo: ItemInfo
    {
        class OpticsModes: OpticsModes
        {
            class elcan_scope: elcan_scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_ACOG_pip: rhsusf_acc_ACOG
{
    class ItemInfo: ItemInfo
    {
        class OpticsModes: OpticsModes
        {
            class elcan_scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_ELCAN: rhsusf_acc_sniper_base
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class elcan_scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_elcan_3d : rhsusf_acc_ELCAN
{
    class ItemInfo: ItemInfo
    {
        class OpticsModes: OpticsModes
        {
            class elcan_scope: elcan_scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_ELCAN_pip: rhsusf_acc_ELCAN
{
    class ItemInfo: ItemInfo
    {
        class OpticsModes: OpticsModes
        {
            class elcan_scope: elcan_scope
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_SpecterDR: ItemCore // Deprecated, replaced by rhsusf_acc_su230_base?
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class Elcan_x4
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_SpecterDR_3d: rhsusf_acc_SpecterDR // Deprecated, replaced by rhsusf_acc_su230_base_3d?
{
    class ItemInfo: ItemInfo
    {
        class OpticsModes: OpticsModes
        {
            class Elcan_x4: Elcan_x4
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};


class rhsusf_acc_su230_base: ItemCore
{
    class ItemInfo: InventoryOpticsItem_Base_F
    {
        class OpticsModes
        {
            class Elcan_x4
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};

class rhsusf_acc_su230_base_3d: rhsusf_acc_su230_base
{
    class ItemInfo: ItemInfo
    {
        class OpticsModes: OpticsModes
        {
            class Elcan_x4: Elcan_x4
            {
                visionMode[] = {"Normal"};
            };
        };
    };
};