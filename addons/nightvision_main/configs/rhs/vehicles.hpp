class rhsusf_hmmwe_base;
class RHS_UH60_Base;
class RHS_C130J_Base;
class RHS_AH1Z_base;
class RHS_UH1_Base;
class RHS_AH64_base;
class RHS_Su25SM_vvs;


/**
 * CARS
 */
class rhsusf_mrzr_base : MRAP_01_base_F{
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,50,10)
		};
		class RSvetla : LSvetla { };
	};
};
class rhsusf_m998_w_2dr : rhsusf_hmmwe_base {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,10,110,10)
		};
	};
};
class rhsusf_m1151_base : MRAP_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,10,110,10)
		};
	};
};
class rhsusf_m113tank_base : APC_Tracked_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhsusf_Cougar_base : MRAP_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_tigr_base : MRAP_02_base_F { // GAZ-22011 TIGR
	class Reflectors {
			// 'Short lights' turned on via scrollwheel option.
			class LSvetla {
				color[] = COLOR_ORANGE;
				MACRO_LIGHTVALUESVARS(80,140,1,60,10)
			};
			class RSvetla: LSvetla { };
			class CSvetla: LSvetla { };
			class Left : LSvetla { };
			class Left2: Left { };
			class Left3: Left { };
			class Right : RSvetla { };
			class Right2: Right {  };
			class Right3: Right { };
			
			// 'Long Lights' turned on via scrollwheel option.
			class Long_Left {
				color[] = COLOR_ORANGE;
				MACRO_LIGHTVALUESVARS(40,70,2.5,150,10)
			};
			class Long_Left2: Long_Left { };
			class Long_Right: Long_Left { };
			class Long_Right2: Long_Right { };
			
			// 'Searchlight' rear-facing light turned on via scrollwheel option.
			class Searchlight: LSvetla {
				color[] = COLOR_ORANGE;
				MACRO_LIGHTVALUESVARS(40,60,2,110,10)
			};
		};
};
class RHS_UAZ_Base : Offroad_01_base_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
		class Left2: LSvetla { };
		class Right2: RSvetla { };
		class Long_Left : LSvetla { };
		class Long_Left2: Long_Left { };
		class Long_Right: Long_Left { };
		class Long_Right2: Long_Right { };
	};
};


/**
 * TRUCKS
 */
class RHS_Ural_BaseTurret: Truck_F {
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
		class Left2: LSvetla { };
		class Right2: RSvetla { };
		class Long_Left : LSvetla { };
		class Long_Left2: Long_Left { };
		class Long_Right: Long_Left { };
		class Long_Right2: Long_Right { };
	};
};
class rhsusf_fmtv_base : Truck_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhsusf_HEMTT_A4_base : Truck_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhsusf_himars_base : Truck_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_truck : Truck_F { // GAZ-66
	class Reflectors {
		class LSvetla {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class RSvetla: LSvetla { };
		class Left2: LSvetla { };
		class Right2: RSvetla { };
		class Long_Left : LSvetla { };
		class Long_Left2: Long_Left { };
		class Long_Right: Long_Left { };
		class Long_Right2: Long_Right { };
	};
};
class rhs_kamaz5350 : O_Truck_02_covered_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_zil131_base : Truck_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_kraz255_base: O_Truck_02_covered_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * MRAPs
 */
class rhsusf_caiman_base : Truck_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhsusf_RG33L_base : MRAP_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhsusf_RG33_base : MRAP_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhsusf_M1239_base : MRAP_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * APCs
 */
class rhsusf_stryker_base : Wheeled_APC_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,1,110,10)
		}; 
	};
};
class rhsusf_M1117_base : Wheeled_APC_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Long_Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_btr_base : Wheeled_APC_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhs_btr60_base : rhs_btr_base {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhsgref_BRDM2 : Wheeled_APC_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left2 : Left { };
		class Right : Left { };
		class Right2 : Right { };
	};
};


/**
 * IFVs
 */
class RHS_M2A2_Base : APC_Tracked_03_base_F{
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhs_bmp1tank_base : Tank_F{
	class Reflectors {
		class Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Driver_FG125_Cover_Flare : Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_bmp3tank_base : Tank_F{
	class Reflectors {
		class Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Driver_FG125_Cover_Flare : Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_bmd_base : Tank_F{
	class Reflectors {
		class Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Driver_FG125_Cover_Flare : Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_a3spruttank_base : Tank_F{ // BMD-4
	class Reflectors {
		class Driver_FG125_Cover{
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Driver_FG125_Cover_Flare : Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};


/**
 * MBTs
 */
class rhsusf_m1a1tank_base : MBT_01_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhsusf_m109tank_base : MBT_01_arty_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhs_a3t72tank_base : Tank_F {
	class Reflectors{
		class Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Driver_FG125_Cover_Flare : Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_tank_base : Tank_F { // T-80
	class Reflectors{
		class Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Driver_FG125_Cover_Flare : Driver_FG125_Cover {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
	};
};
class rhs_2s3tank_base : Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
		class Left2 : Left { };
		class Right2 : Right { };
	};
};
class rhs_2s1tank_base : Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Left2 : Left { };
	};
};
class rhs_pts_base : APC_Tracked_02_base_F{
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhs_zsutank_base: APC_Tracked_02_base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};
class rhs_t14_base: Tank_F {
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
	};
};


/**
 * HELICOPTERS
 */
class RHS_UH60M_base: RHS_UH60_Base {
	class Reflectors {
		class Middle {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class RHS_CH_47F_base: Heli_Transport_02_base_F {
	class Reflectors {
		class Middle {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class RHS_AH64D: RHS_AH64_base{
	class Reflectors {
		class Middle {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class RHS_MELB_base: Helicopter_Base_H{
	class Reflectors {
		class Light {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class RHS_AH1Z : RHS_AH1Z_base {
	class Reflectors {
		class Middle {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class rhsusf_CH53E_USMC : Helicopter_Base_H {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class RHS_UH1Y_base : RHS_UH1_base {
	class Reflectors {
		class Middle {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class rhs_uh1h_base : Helicopter_Base_H {
	class Reflectors {
		class Front {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class RHS_Mi24_base : Heli_Attack_02_base_F{
	class Reflectors {
		class Light {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
		class Light_Flare : Light { };
	};
};
class RHS_Mi8_base : Heli_Light_02_base_F{
	class Reflectors {
		class LSvetla {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
		class RSvetla : LSvetla { };
	};
};
class RHS_Ka52_base : Heli_Attack_02_base_F{
	class Reflectors {
		class LSvetla {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
		class RSvetla : LSvetla { };
	};
};
class rhs_mi28_base : Heli_Attack_02_base_F{
	class Reflectors {
		class LightL {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
		class LightR : LightL { };
	};
};


/**
 * FIXED WING
 */
class rhs_an2_base: Plane_Base_F {
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(25,50,2.5,300,10)
		};
		class Right : Left { };
	};
};