class rhsusf_acc_anpeq15 : acc_pointer_IR
{
	class ItemInfo;
};

// RHS US ANPEQ-15, most RHS US flashlights inherit from this light (includes WMX, M952V)
class rhsusf_acc_anpeq15_light: rhsusf_acc_anpeq15
{
	class ItemInfo : ItemInfo
	{
		class FlashLight
		{
			// Fix colors "25 22 20" to standard 0-255
			color[] = {250, 220, 200};

			daylight = 0;
			direction = "flash";
			flaremaxdistance = 200;
			flaresize = 0.8;

			position = "flash dir";
			scale[] = {0};
			size = 1;
			useflare = 1;
			
			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};
};

// RHS RUS 2DP, 2DP (RIS) inherits from this
class rhs_acc_2dpZenit : acc_flashlight
{
	class ItemInfo : ItemInfo
	{
		class FlashLight
		{
			// Fix colors to standard 0-255
			color[] = {190, 130, 95};

			flaremaxdistance = 200;
			flaresize = 0.8;

			MACRO_LIGHTVALUESVARS(10,35,2.5,80,10)
		};
	};
};