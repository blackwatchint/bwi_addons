

/**
 * CARS
 */
class ffaa_et_vamtac_base : MRAP_01_base_F { // Vamtac (Humvee)
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
	};
};
class ffaa_et_vamtac_ume : ffaa_et_vamtac_base { // Vamtac (UME)
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
	};
};
class ffaa_et_anibal : Car_F { // Anibal (Land Rover)
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
		class Left2 : Left { };
		class Right2 : Right { };
	};
};
class ffaa_et_lince_base : MRAP_01_base_F { // Lince LMV
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
		class Left2 : Left { };
		class Right2 : Right { };
	};
};


/**
 * TRUCKS
 */
class ffaa_m250_base : Truck_03_base_F { // M250 Truck
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Left2 : Left { };
	};
};
class ffaa_et_pegaso_base : Truck_03_base_F { // Pegaso 7226
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Left2: Left { };
	};
};


/**
 * MRAPs
 */
class ffaa_et_rg31_base : MRAP_01_base_F { // RG31 Mk.5E Nyala
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Left_s {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Right : Left { };
		class Right_s : Left_s { };
	};
};

class ffaa_et_rg31_samson;
class ffaa_et_rg31_rollers : ffaa_et_rg31_samson { // RG31 Mk.5E MK II
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Left_s {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		};
		class Front : Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(40,70,2.5,110,10)
		 };
		class FrontLeft : Front { };
		class FrontRight : Front { };
		class CenterLeft : Front { };
		class CenterRight : Front { };
		class Right : Left { };
		class Right_s : Left_s { };
	};
};

/**
 * APCs
 */
class ffaa_et_toa : APC_Tracked_02_base_F { // M113
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
		class Left2 : Left { };
		class Right2 : Right { };
	};
};


/**
 * IFVs
 */
class ffaa_et_pizarro : APC_Tracked_03_base_F { // Pizarro ICV
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
	};
};


/**
 * MBTs
 */
class ffaa_et_leopardo_base : Tank_F { // Leaprd MBT
	class Reflectors {
		class Left {
			color[] = COLOR_ORANGE;
			MACRO_LIGHTVALUESVARS(75,100,1,75,10)
		};
		class Right : Left { };
	};
};


/**
 * HELICOPTERS
 */
class ffaa_ec135_base : Heli_Light_01_base_F { // Eurocopter 135 T2
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class ffaa_famet_cougar_base { // AS532UL Cougar
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};
class ffaa_nh90_base { // NH90 TTH
	class Reflectors {
		class Right {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(80,130,3,200,25)
		};
	};
};


/**
 * FIXED WING
 */
class ffaa_av8b2_base : Plane_Base_F { // C130-J
	class Reflectors {
		class Light_1 {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(25,50,2.5,300,10)
		};
	};
};
class ffaa_ea_hercules_base: Plane_Base_F { // C130-J
	class Reflectors {
		class Left {
			color[] = COLOR_BLUE;
			MACRO_LIGHTVALUESVARS(25,50,2.5,300,10)
		};
		class Right : Left { };
		class Left2 : Left { };
		class Right2 : Right { };
	};
};