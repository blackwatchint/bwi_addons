#include <configs\core\optics.hpp>
#include <configs\rhs\optics.hpp>

#include <configs\core\lights.hpp>
#include <configs\baf\lights.hpp>
#include <configs\bwa\lights.hpp>
#include <configs\rhs\lights.hpp>
#include <configs\rhs\helmets.hpp>