#include <defines.hpp>
class CfgPatches {
	class bwi_nightvision_main {
		requiredVersion = 1;
		author = "Black Watch International";
		authorURL = "http://blackwatch-int.com";
		authors[] = {"Fourjays", "0mega", "Andrew"};
		version = 1.3;
		versionStr = "1.3.0";
		versionAr[] = {1,3,0};
        requiredAddons[] = {
			"A3_Weapons_F",
			"ace_nightvision",
			"ace_cargo",
			"bwi_nightvision",
            "rhsusf_main",
            "rhsusf_c_weapons",
			"rhsusf_weapons",
			"rhsusf_weapons2",
			"rhsusf_weapons3",
			"uk3cb_baf_weapons_Accessories",
			"uk3cb_baf_vehicles_warrior_a3",
            "rhsusf_infantry",
			"bwa3_tiger",
			"bwa3_leopard2",
			"bwa3_eagle",
			"bwa3_puma",
			"rhsusf_a2port_air",
			"rhsusf_a2port_air2",
			"rhsusf_a2port_armor",
			"rhsusf_a2port_car",
			"rhsusf_c_rg33",
			"rhsusf_c_stryker",
			"rhsgref_main",
			"rhsgref_c_troops",
			"rhsgref_c_a2port_armor",
			"ffaa_ea_hercules",
			"ffaa_ec135",
			"ffaa_et_ch47",
			"ffaa_et_anibal",
			"ffaa_et_cougar",
			"ffaa_et_leopardo",
			"ffaa_et_lince",
			"ffaa_et_pegaso",
			"ffaa_et_pizarro",
			"ffaa_et_rg31",
			"ffaa_et_searcher",
			"ffaa_et_tigre",
			"ffaa_et_toa",
			"ffaa_et_vamtac",
			"ffaa_husky",
			"ffaa_nh90",
			"ffaa_husky",
            "jk_medium_helicopter"
        };
		units[] = {};
	};
};

class CfgWeapons {
	#include <cfgWeapons.hpp>
};

class CfgAmmo {
	#include <cfgAmmo.hpp>
};
class CfgVehicles {
    #include <cfgVehicles.hpp>
};