class rhsusf_acc_leupoldmk4: Accessory {
	year = 1990;

	compatibleClasses[] = {
		"rhs_weap_m14ebrri",
		"rhs_weap_m24sws",
		"rhs_weap_m40a5",
        "rhs_weap_svdp_wd_npz",
        "rhs_weap_svdp_npz",
        "rhs_weap_svds_npz",
        "rhs_weap_vss_npz",
        "rhs_weap_vss_grip_npz",
        "rhs_weap_t5000"
	};
};

class rhsusf_acc_leupoldmk4_d: Accessory {
	year = 1990;

	compatibleClasses[] = {
		"rhs_weap_m24sws_d",
		"rhs_weap_m40a5_d"
	};
};

class rhsusf_acc_leupoldmk4_wd: Accessory {
	year = 1990;

	compatibleClasses[] = {
		"rhs_weap_m24sws_wd",
		"rhs_weap_m40a5_wd"
	};
};

class bwa3_optic_pmii_shortdotcc: Accessory {
	year = 2012;

	compatibleClasses[] = {
		"BWA3_G27",
		"BWA3_G27_tan",
		"BWA3_G28",
		"BWA3_G28_Patrol"
	};
};

class uk3cb_optic_artel_m14: Accessory {
	year = 1969;

	compatibleClasses[] = {
		"UK3CB_M21"
	};
};

class uk3cb_optic_accupoint_g3: Accessory {
	year = 2001;

	compatibleClasses[] = {
		"UK3CB_PSG1A1"
	};
};