// Define top rail categories.
class OpticsX1: AccessoryGroup
{
    name = "1x Optics";
    slot = SLOT_TOP;
    level = OPTIC_1X;

    #include <toprail\optics_x1.hpp>
};

class OpticsX2: AccessoryGroup
{
    name = "2x Optics";
    slot = SLOT_TOP;
    level = OPTIC_2X;

    #include <toprail\optics_x2.hpp>
};

class OpticsX3: AccessoryGroup
{
    name = "3x Optics";
    slot = SLOT_TOP;
    level = OPTIC_3X;

    #include <toprail\optics_x3.hpp>
};

class OpticsX4: AccessoryGroup
{
    name = "4x Optics";
    slot = SLOT_TOP;
    level = OPTIC_4X;

    #include <toprail\optics_x4.hpp>
};

class OpticsX6: AccessoryGroup
{
    name = "6x Optics";
    slot = SLOT_TOP;
    level = OPTIC_6X;

    #include <toprail\optics_x6.hpp>
};

class OpticsX8: AccessoryGroup
{
    name = "8x Optics";
    slot = SLOT_TOP;
    level = OPTIC_8X;

    #include <toprail\optics_x8.hpp>
};

class OpticsX10: AccessoryGroup
{
    name = "10x Optics";
    slot = SLOT_TOP;
    level = OPTIC_8X;

    #include <toprail\optics_x10.hpp>
};

class OpticsX15: AccessoryGroup
{
    name = "15x Optics";
    slot = SLOT_TOP;
    level = OPTIC_8X;

    #include <toprail\optics_x15.hpp>
};

class OpticsX20: AccessoryGroup
{
    name = "20x Optics";
    slot = SLOT_TOP;
    level = OPTIC_8X;

    #include <toprail\optics_x20.hpp>
};

class OpticsX25: AccessoryGroup
{
    name = "25x Optics";
    slot = SLOT_TOP;
    level = OPTIC_8X;

    #include <toprail\optics_x25.hpp>
};


// Define side rail categories.
class Lights: AccessoryGroup
{
    name = "Lights";
    slot = SLOT_SIDE;
    level = RAIL_LIGHT;

    #include <siderail\lights.hpp>
};

class Lasers: AccessoryGroup
{
    name = "Lasers";
    slot = SLOT_SIDE;
    level = RAIL_LASER;

    #include <siderail\lasers.hpp>
};

class Combos: AccessoryGroup
{
    name = "Light/Laser Combos";
    slot = SLOT_SIDE;
    level = RAIL_COMBO;

    #include <siderail\combos.hpp>
};


// Define bottom rail categories.
class Grips: AccessoryGroup
{
    name = "Grips";
    slot = SLOT_BOTTOM;
    level = REST_GRIP;

    #include <bottomrail\grips.hpp>
};

class Bipods: AccessoryGroup
{
    name = "Bipods";
    slot = SLOT_BOTTOM;
    level = REST_BIPOD;

    #include <bottomrail\bipods.hpp>
};


// Define barrel categories.
class FlashSuppressors: AccessoryGroup
{
    name = "Flash Suppressors";
    slot = SLOT_BARREL;
    level = BARREL_FLASH;

    #include <barrel\suppressors_flash.hpp>
};

class SoundSuppressors: AccessoryGroup
{
    name = "Sound Suppressors";
    slot = SLOT_BARREL;
    level = BARREL_SOUND;

    #include <barrel\suppressors_sound.hpp>
};


// Define chestpack categories.
class Parachutes: ChestpackGroup
{
    name = "Parachutes";
    slot = CHESTPACK;
    level = PARACHUTE;

    parentClassTree = "CfgVehicles";

    #include <chestpack\parachutes.hpp> 
};


// Define headmount categories.
class NightVisionG1: HeadmountGroup
{
    name = "Night Vision (Gen I)";
    slot = HEADMOUNT;
    level = NIGHTVISION;

    #include <headmount\nightvision_g1.hpp> 
};

class NightVisionG2: HeadmountGroup
{
    name = "Night Vision (Gen II)";
    slot = HEADMOUNT;
    level = NIGHTVISION;

    #include <headmount\nightvision_g2.hpp> 
};

class NightVisionG3: HeadmountGroup
{
    name = "Night Vision (Gen III)";
    slot = HEADMOUNT;
    level = NIGHTVISION;

    #include <headmount\nightvision_g3.hpp> 
};

class NightVisionG4: HeadmountGroup
{
    name = "Night Vision (Gen IV)";
    slot = HEADMOUNT;
    level = NIGHTVISION;

    #include <headmount\nightvision_g4.hpp> 
};