class UK3CB_ANPVS7: Accessory {
    year = 1985;

    compatibleClasses[] = {
		"rhs_booniehat2_marpatd",
        "rhs_booniehat2_marpatwd",
        "UK3CB_CW_US_B_EARLY_H_BoonieHat_ERDL_01",
        "UK3CB_CW_US_B_EARLY_H_BoonieHat_ERDL_02"
	};
};