class Tan_Infantry: FactionGroup
{
	name = "Horizon Islands Defence Force";
	side = BLUFOR;

	flag  = "\bwi_data_main\data\flag_tan.paa";
    icon  = "\bwi_data_main\data\icon_s_tan.paa";
    image = "\bwi_data_main\data\icon_l_tan.paa";

	#include <1986\faction.hpp>
	#include <2007\faction.hpp>
};