class Woodland_2007: Faction
{
	name = "ERDL / Woodland";
	year = 2007;
	type = MARINES;

	uniformScript = "\bwi_data_main\configs\factions\tan_marines\2007\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tan_marines\2007\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tan_marines\2007\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tan_marines\2007\ammo.sqf";

	hiddenElements[] = {"SCU", "HAT", "UWC", "IFV", "AAA"};
	hiddenRoles[]	 = {"UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_g.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_w.hpp>
	#include <resupply.hpp>
};