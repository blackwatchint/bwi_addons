class Woodland_1986: Faction
{
	name = "ERDL / Woodland";
	year = 1986;
	type = MARINES;

	uniformScript = "\bwi_data_main\configs\factions\tan_marines\1986\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tan_marines\1986\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tan_marines\1986\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tan_marines\1986\ammo.sqf";

	hiddenElements[] = {"SCU", "MAT", "HAT", "UWC", "IFV", "AAA", "FWC"};
	hiddenRoles[]	 = {"LDG", "GRE", "HIR", "UAV"};

	acreRadioDevices[] = {"NONE", "NONE", "ACRE_SEM52SL", "ACRE_PRC77"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_g.paa",
		"\bwi_resupply_main\data\usa_signs_1.paa",
		"\bwi_resupply_main\data\usa_signs_2.paa",
		"\bwi_resupply_main\data\usa_signs_3.paa"
	};

	#include <motorpool_w.hpp>
	#include <resupply.hpp>
};