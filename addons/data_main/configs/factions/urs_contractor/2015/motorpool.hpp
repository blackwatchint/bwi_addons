allowedVehicles[] = {
	"GAZVodnik_URS",
	"GAZVodnik_MEV_URS",
	"GAZVodnik_PKT_URS",
	"Ural4320_URS",
	"Ural4320_O_URS",
	"Ural4320_Recover_URS",
	"Ural4320_Fuel_URS",
	"Ural4320_Repair_URS",
	"Ural4320_Ammo_URS",
	"Ural4320_ZU23_URS",
	"Ural4320_D30A_URS",
	"Ural4320_SBCOP_URS_W",
	"Ural4320_BBCOP_URS_W",
	"BRDM2_URS",
	"BRDM2_GPMG_URS",
	"BRDM2_HMG_URS",
	"BRDM2_ATGM_URS",
	"BMP2_URS",
	"T72BM_URS",
	"T72B_URS",
	"KA60_URS",
	"Ka60_HE_URS",
	"Ka60_AP_URS"
};

allowedVehiclesRecon[] = {
	"Quadbike",
	"AssaultBoat",
	"RHIB_Small",
	"RHIB_MG"
};

allowedVehiclesSpecial[] = {
	/* None */
};