// Ursus Corporation
class Urs_Contractor: FactionGroup
{
	name = "Ursus Corporation";
	side = OPFOR;
	
	flag  = "\bwi_data_main\data\flag_urs.paa";
    icon  = "\bwi_data_main\data\icon_s_urs.paa";
    image = "\bwi_data_main\data\icon_l_urs.paa";

	#include <2015\faction.hpp>
};