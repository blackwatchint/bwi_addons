// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Uniform.
[_unit, ["UK3CB_CHC_C_U_CIT_01", "UK3CB_CHC_C_U_ACTIVIST_01", "UK3CB_CHC_C_U_COACH_01", "UK3CB_CHC_C_U_PROF_01"]] call bwi_armory_fnc_addRandomUniform;

// Helmet.
[_unit, ["UK3CB_H_Worker_Cap_01", "UK3CB_H_Villager_Cap_01", "UK3CB_H_Ushanka_Cap_01", "UK3CB_H_Profiteer_Cap_01"]] call bwi_armory_fnc_addRandomHeadgear;

// Vest.
// No vest.