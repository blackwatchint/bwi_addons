class Russian: Faction
{
    name = "Russian";
    year = 2020;
    type = REGULARS;

    uniformScript = "\bwi_data_main\configs\factions\civilians\russian\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\civilians\russian\weapons.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\civilians\russian\ammo.sqf";
};