// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Uniform.
[_unit, ["U_C_Man_casual_6_F", "U_C_Man_casual_5_F", "U_C_Man_casual_4_F"]] call bwi_armory_fnc_addRandomUniform;

// Helmet.
[_unit, ["H_Bandanna_surfer", "H_Bandanna_surfer_blk", "H_Bandanna_surfer_grn"]] call bwi_armory_fnc_addRandomHeadgear;

// Vest.
// No vest.