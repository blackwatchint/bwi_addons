// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Uniform.
[_unit, ["U_BG_Guerilla2_2", "U_BG_Guerilla2_1", "U_BG_Guerilla2_3", "U_BG_Guerilla3_1", "U_BG_Guerrilla_6_1"]] call bwi_armory_fnc_addRandomUniform;

// Helmet.
[_unit, ["H_Hat_blue", "H_Hat_brown", "H_Hat_checker", "H_Hat_grey", "H_Hat_tan"]] call bwi_armory_fnc_addRandomHeadgear;

// Vest.
// No vest.