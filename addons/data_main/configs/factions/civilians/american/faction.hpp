class American: Faction
{
    name = "American";
    year = 2020;
    type = REGULARS;

    uniformScript = "\bwi_data_main\configs\factions\civilians\american\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\civilians\american\weapons.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\civilians\american\ammo.sqf";
};