class Desert_2007: Faction
{
    name = "Flora / Desert";
    year = 2007;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\rus_airborne\2007\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\rus_airborne\2007\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\rus_airborne\2007\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\rus_airborne\2007\ammo.sqf";

    hiddenElements[] = {"HAT", "UWC"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\rus_color_k.paa",
        "\bwi_resupply_main\data\rus_signs_1.paa",
        "\bwi_resupply_main\data\rus_signs_2.paa",
        "\bwi_resupply_main\data\rus_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2007: Faction
{
    name = "Flora / Woodland";
    year = 2007;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\rus_airborne\2007\uniform.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\rus_airborne\2007\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\rus_airborne\2007\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\rus_airborne\2007\ammo.sqf";

    hiddenElements[] = {"HAT", "UWC"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\rus_color_k.paa",
        "\bwi_resupply_main\data\rus_signs_1.paa",
        "\bwi_resupply_main\data\rus_signs_2.paa",
        "\bwi_resupply_main\data\rus_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};