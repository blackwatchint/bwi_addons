class Desert_2019: Faction
{
    name = "EMR / Desert";
    year = 2019;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\rus_airborne\2019\uniform_d.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\rus_airborne\2019\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\rus_airborne\2019\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\rus_airborne\2019\ammo.sqf";

    hiddenElements[] = {"HAT", "UWC"};
    hiddenRoles[]    = {"HIR", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\rus_color_k.paa",
        "\bwi_resupply_main\data\rus_signs_1.paa",
        "\bwi_resupply_main\data\rus_signs_2.paa",
        "\bwi_resupply_main\data\rus_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2019: Faction
{
    name = "EMR / Woodland";
    year = 2019;
    type = AIRBORNE;

    uniformScript = "\bwi_data_main\configs\factions\rus_airborne\2019\uniform_w.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\rus_airborne\2019\weapons.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\rus_airborne\2019\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\rus_airborne\2019\ammo.sqf";

    hiddenElements[] = {"HAT", "UWC"};
    hiddenRoles[]    = {"HIR", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\rus_color_k.paa",
        "\bwi_resupply_main\data\rus_signs_1.paa",
        "\bwi_resupply_main\data\rus_signs_2.paa",
        "\bwi_resupply_main\data\rus_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};