class Sov_Airborne: FactionGroup
{
	name = "Soviet Airborne";
	side = OPFOR;
	
	flag  = "\bwi_data_main\data\flag_ussr.paa";
    icon  = "\bwi_data_main\data\icon_s_ussr.paa";
    image = "\bwi_data_main\data\icon_l_ussr.paa";

    #include <1986\faction.hpp>
};

class Rus_Airborne: FactionGroup
{
    name = "Russian Airborne";
    side = OPFOR;
	
	flag  = "\bwi_data_main\data\flag_rus.paa";
    icon  = "\bwi_data_main\data\icon_s_rus.paa";
    image = "\bwi_data_main\data\icon_l_rus.paa";

    #include <2007\faction.hpp>
    #include <2019\faction.hpp>
};