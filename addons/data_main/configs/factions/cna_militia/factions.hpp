class CNA_Militia: FactionGroup
{
    name = "Chernarus Nationalist Army";
    side = INDEP;
	
	flag  = "\bwi_data_main\data\flag_cna.paa";
    icon  = "\bwi_data_main\data\icon_s_cna.paa";
    image = "\bwi_data_main\data\icon_l_cna.paa";

    #include <2008\faction.hpp>
};