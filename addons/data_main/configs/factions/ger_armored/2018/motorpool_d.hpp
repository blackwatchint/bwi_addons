allowedVehicles[] = {
	"GLWolf_D",
	"GLWolf_Medical_D",
	"LKW5T_D",
	"LKW7T_D",
	"LKW5T_Fuel_D",
	"LKW7T_Ammo_D",
	"LKW10T_Repair_D",
	"LKW5T_SBCOP_D",
	"LKW7T_SBCOP_D",
	"LKW5T_HBCOP_D",
	"LKW7T_HBCOP_D",
	"EagleIV_D",
	"EagleIV_FLW_D",
	"Dingo2_MG3_D",
	"Dingo2_M2_D",
	"Dingo2_GMW_D",
	"Wiesel1A2_TOW_D",
	"Wiesel1A2_Mk20_D",
	"Fuchs1A4_D",
	"Fuchs1A4_Medical_D",
	"Fuchs1A4_HAT_D",
	"Marder1A5_D",
	"Puma_D",
	"Leopard2_D",
	"Gepard1A2_D",
    "UH1D_GER_18",
    "UH1D_MG_GER_18",
	"UH_Tiger",
	"EF2000_CAS00_GER",
	"NH90NFH_ESP",
	"NH90NFH_MG_ESP"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat",
	"SDV"
};