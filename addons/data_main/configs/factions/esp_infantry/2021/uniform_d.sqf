// Parameters passed.
params ["_unit", "_role", "_year", "_type"];

// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;

// Remove goggles on special forces and scuba roles.
if ( (_role select [0,3]) in ["spf", "scu"] ) then {
	removeGoggles _unit;
};


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "ffaa_brilat_CombatUniform_item_d"; };

	case "rwc_pil";
	case "rwt_pil": { _unit forceAddUniform "ffaa_famet_uniforme_item_d"; };
	
	case "fwc_pil": { _unit forceAddUniform "ffaa_pilot_harri_uniforme_item"; };
	case "fwt_pil": { _unit forceAddUniform "ffaa_pilot_her_uniforme_item_d"; };
};


// Helmet.
switch ( _role ) do {
	default { [_unit, [ "ffaa_casco_marte_04_mod_1_d",
						"ffaa_casco_marte_04_mod_2_d",
						"ffaa_casco_marte_04_mod_3_d"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addHeadgear "ffaa_brilat_casco_d"; };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_lat";
	case "spf_sap";
	case "spf_eod";
	case "spf_bre";
	case "spf_dmr";
	case "spf_rif": { [_unit, [ "ffaa_moe_casco_02_1_d",
								"ffaa_moe_casco_02_2_d"]] call bwi_armory_fnc_addRandomHeadgear; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addHeadgear "ffaa_brilat_casco_tripulacion"; };

	case "rwc_pil": { _unit addHeadgear "ffaa_casco_famet_crew"; };
	case "rwt_pil": { _unit addHeadgear "ffaa_casco_famet_piloto"; };
	
	case "fwc_pil": { _unit addHeadgear "H_PilotHelmetFighter_B"; };
	case "fwt_pil": { _unit addHeadgear "ffaa_casco_hercules_piloto"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "ffaa_brilat_chaleco_01_ds"; };
	
	case "med_cpm": { _unit addVest "ffaa_brilat_chaleco_04_ds"; };
	
	case "sqd_ldg";
	case "sqd_lhg";
	case "sqd_gre": { _unit addVest "ffaa_brilat_chaleco_03_ds"; };
	case "sqd_lmg";
	case "sqd_ahg";
	case "sqd_amg": { _unit addVest "ffaa_brilat_chaleco_02_ds"; };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_rif";
	case "lrc_dmr": { _unit addVest "ffaa_brilat_chaleco_01_ds"; };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lat";
	case "spf_lmg";
	case "spf_sap";
	case "spf_eod";
	case "spf_bre";
	case "spf_rif";
	case "spf_dmr": { _unit addVest "ffaa_et_moe_peco_01_mtp"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addVest "ffaa_brilat_chaleco_01_ds"; };

	case "rwc_pil";
	case "rwt_pil": { _unit addVest "V_TacVest_khk"; };
	
	case "fwc_pil";
	case "fwt_pil": { _unit addVest "UK3CB_V_Pilot_Vest"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "ffaa_brilat_mochila_asalto_arida"; };
	
	case "plt_ldg";
	case "plt_ldr";
	case "log_sgt";
	case "tac_fac": { _unit addBackpack "ffaa_brilat_mochila_viaje_arida"; };
	
	case "med_cpm": { _unit addBackpack "ffaa_brilat_mochila_viaje_arida"; };
	
	case "sqd_dem": { _unit addBackpack "ffaa_brilat_mochila_viaje_arida"; };
	case "eng_def": { _unit addBackpack "B_Carryall_cbr"; };
	
	case "mat_ldr";
	case "mat_gun": { _unit addBackpack "B_Carryall_cbr"; };
	
	case "aat_ldr";
	case "aat_gun": { /* Backpack weapon */ };
	
	case "mmg_ldr";
	case "mmg_gun": { _unit addBackpack "ffaa_brilat_mochila_viaje_arida"; };
	
	case "sqd_lmg";
	case "sqd_lhg";
	case "sqd_amg";
	case "sqd_ahg": { _unit addBackpack "ffaa_brilat_mochila_viaje_arida"; };
	case "sqd_sap": { _unit addBackpack "ffaa_brilat_mochila_viaje_arida"; };
	
	case "how_ldr";
	case "how_gun"; { /* No backpack */ };
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": { /* Backpack weapon */ };

	case "lrc_ldr";
	case "lrc_lmg";
	case "lrc_lat";
	case "lrc_hir";
	case "lrc_dmr";
	case "lrc_rif": { _unit addBackpack "ffaa_brilat_mochila_ligera_arida"; };
	case "spc_uav": { /* Backpack weapon */ };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_rif";
	case "spf_lat";
	case "spf_sap": { _unit addBackpack "ffaa_brilat_mochila_arida"; };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { /* No backpack */ };

	case "rwc_pil";
	case "rwt_pil": { /* No backpack */ };
	case "fwc_pil";
	case "fwt_pil": { _unit addBackpack "B_Parachute"; };
};