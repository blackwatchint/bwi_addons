// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary ammo.
switch ( _role ) do {
	default {
		[_unit, "ffaa_556x45_g36", 5] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_g36_tracer_green", 4] call bwi_armory_fnc_addToVest;
	};
	
	case "sqd_bre";
	case "sqd_lhg";
	case "sqd_ahg": {
		[_unit, "ffaa_556x45_g36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_g36_tracer_green", 3] call bwi_armory_fnc_addToVest;
	};

	case "sqd_lmg": {
		[_unit, "ffaa_556x45_mg4", 1] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_mg4", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_gun": {
		[_unit, "UK3CB_MG3_100rnd_762x51_R", 1] call bwi_armory_fnc_addToVest;
		[_unit, "UK3CB_MG3_250rnd_762x51_RT", 2] call bwi_armory_fnc_addToBackpack;
	};

	case "how_ldr";
	case "how_gun";
	case "mot_ldr";
	case "mot_gun";
	case "dat_ldr";
	case "dat_gun";
	case "dgg_ldr";
	case "dgg_gun": {
		[_unit, "ffaa_556x45_g36", 3] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_g36_tracer_green", 2] call bwi_armory_fnc_addToVest;
	};

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": {
		[_unit, "ffaa_556x45_g36", 4] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_g36_tracer_green", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_lmg": {
		[_unit, "ffaa_556x45_mg4", 1] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_mg4", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "lrc_dmr": {
		[_unit, "ffaa_20Rnd_762x51_hk417", 5] call bwi_armory_fnc_addToBackpack;
		[_unit, "ffaa_20Rnd_762x51_hk417_Tracer_red", 4] call bwi_armory_fnc_addToBackpack;
	};

	case "spf_ldr": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 2] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 4] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_ldg";
	case "spf_bre";
	case "spf_sap";
	case "spf_eod";
	case "spf_rif": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 2] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_lat": {
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 3] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_lmg": {
		[_unit, "ffaa_556x45_mg4", 1] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_mg4", 4] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_dmr": {
		[_unit, "ffaa_762x51_accuracy", 6] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_762x51_accuracy", 10] call bwi_armory_fnc_addToBackpack;
	};

	case "scu_ldr";
	case "scu_bre";
	case "scu_lat";
	case "scu_sap";
	case "scu_rif"; 
	case "scu_lmg";
	case "scu_dmr": { };

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": {
		[_unit, "ffaa_556x45_g36", 2] call bwi_armory_fnc_addToVest;
		[_unit, "ffaa_556x45_g36_tracer_green", 2] call bwi_armory_fnc_addToVest;
	};

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": {
		[_unit, "ffaa_9x19_ump", 5] call bwi_armory_fnc_addToVest;
	};
	case "uwc_pil": { /* No primary */ };
};


// Secondary ammo.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg": {
		[_unit, "ffaa_9x19_pistola", 3] call bwi_armory_fnc_addToBackpack;
	};

	case "fwc_pil";
	case "fwt_pil";
	case "uwc_pil": {
		[_unit, "ffaa_9x19_pistola", 3] call bwi_armory_fnc_addToVest;
	};

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		[_unit, "ffaa_9x19_pistola", 2] call bwi_armory_fnc_addToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sqd_amg": {
		[_unit, "ffaa_556x45_mg4", 6] call bwi_armory_fnc_addToBackpack;
	};

	case "mmg_ldr": {
		[_unit, "UK3CB_MG3_250rnd_762x51_RT", 2] call bwi_armory_fnc_addToBackpack;
	};
	
	case "mat_gun": {
		[_unit, "ffaa_mag_c100_biv", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "mat_ldr": {
		[_unit, "ffaa_mag_c100_abk", 1] call bwi_armory_fnc_addToBackpack;
		[_unit, "ffaa_mag_c100", 1] call bwi_armory_fnc_addToBackpack;
	};
};


// Other ammo.
switch ( _role ) do {
	case "sqd_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToVest;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	case "spf_bre": {
		[_unit, "rhsusf_8Rnd_00Buck", 3] call bwi_armory_fnc_addToBackpack;
		[_unit, "rhsusf_8Rnd_Slug", 2] call bwi_armory_fnc_addToBackpack;
	};
	
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat": {
		[_unit, "ffaa_mag_c90", 1] call bwi_armory_fnc_addToBackpack;
	};
};