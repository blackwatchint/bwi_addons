class Desert_2021: Faction
{
	name = "M09 Pattern / Desert";
	year = 2021;
	type = INFANTRY;

	uniformScript = "\bwi_data_main\configs\factions\esp_infantry\2021\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\esp_infantry\2021\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\esp_infantry\2021\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\esp_infantry\2021\ammo.sqf";

	hiddenElements[] = {"SCU", "HAT"};
	hiddenRoles[]	 = {"LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_t.paa",
		"\bwi_resupply_main\data\usd_signs_1.paa",
		"\bwi_resupply_main\data\usd_signs_2.paa",
		"\bwi_resupply_main\data\usd_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_d.hpp>
};
class Woodland_2021: Faction
{
	name = "M09 Pattern / Woodland";
	year = 2021;
	type = INFANTRY;

	uniformScript = "\bwi_data_main\configs\factions\esp_infantry\2021\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\esp_infantry\2021\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\esp_infantry\2021\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\esp_infantry\2021\ammo.sqf";

	hiddenElements[] = {"SCU", "HAT"};
	hiddenRoles[]	 = {"LHG", "AHG"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_g.paa",
		"\bwi_resupply_main\data\usd_signs_1.paa",
		"\bwi_resupply_main\data\usd_signs_2.paa",
		"\bwi_resupply_main\data\usd_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};