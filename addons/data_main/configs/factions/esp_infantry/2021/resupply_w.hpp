allowedSupplies[] = {
	"Mag_30Rnd_G36E",
	"Box_200Rnd_MG4_ESP",
	"Mag_8Rnd_Buck",
	"Mag_8Rnd_Slug",
	
	"Gren_Hand_Alhambra",
	"Gren_Hand_WS_ANM8",
	"Gren_Hand_CS_M18",
	"Gren_40mm_HE_M441",
	"Gren_40mm_WS_M714",
	"Gren_40mm_CS_M713",
	"Gren_40mm_WF_M585",
	"Gren_40mm_CF_M661",
	
	"AT_Tube_C90",
	"Medical_Bandages",
	"Medical_Medication",
	"Medical_Blood",
	"Medical_Bodybags",
	"Explosive_M112",
	"Explosive_M183",
	"Explosive_M6SLAM",
	"Explosive_M26",
	"Explosive_M15",
	"Explosive_M18A1",
	"Explosive_PMR3M",
	"Explosive_PMR3F",
	
	"DMG_M2_Low",
	"DMG_M2_High",
	"Searchlight",

	"Spare_Track",
	"Spare_Wheel",
	"Jerry_Can",
	"Towing_Ropes",

	"BaseWallWood",
	"BaseRampartWood",
	"BaseRampartCornerWood",
	"BaseRampartInvCornerWood",
	"BaseRampartBunkerWood",
	"BaseWallCargoWood",
	"BaseWallCornerBunkerWood",
	"BaseWallCornerCargoWood",
	"BaseSideEntranceWood",
	"BaseMainEntranceBunkerWood",
	"BaseMainEntranceCargoWood",
	"BaseFuelBladderWood",
	"BaseHelipadWood",
	"BaseCargoHouseWood",
	"BaseCargoHQWood",
	"BaseAmmoDumpWoodEast",
	"BaseRampartAmmoDumpWoodEast",
	"BaseVehicleResupplyWoodEast",
	"MortarSandbagWood",
	"BunkerSandbagWood",
	"BunkerSandbagLargeWood",
	"SquadSandbagWoodEast",
	"SquadSandbagTallWoodEast",
	"CheckpointSandbagWoodEast",
    "SquadHescoWoodEast",
	"VehicleHescoWood",
	"MortarHescoWood",
	"MortarHescoWireWood",
	"TowerHescoWood",
	"BunkerHescoWood",
	"VehicleHescoCamoWoodEast",
	"CheckpointHescoWoodEast",
	"CheckpointHescoHeavyWoodEast",
	"VehicleTrenchWoodland",
	"SquadTrench90WoodEast",
	"SquadTrench180WoodEast",
	"SquadTrench360WoodEast",
	"VehicleTrenchCamoWoodlandEast",
	"VehicleTrenchLargeWoodlandEast",
	"PortableLightKit",
	"TallLightKit",
	"Shelter2",
	"Shelter4",
	"TankTrap4",
	"TankTrap8"
};

allowedSuppliesWeapons[] = {
	"Box_250Rnd_MG3",
	"AT_1Rnd_C100AT",
	"AT_1Rnd_C100BIV",
	"AT_1Rnd_C100ABK"
};

allowedSuppliesRecon[] = {
	"Mag_30Rnd_HK417"
};

allowedSuppliesSpecial[] = {
	"Mag_30Rnd_M855A1",
	"Mag_30Rnd_Mk262",
	"Mag_30Rnd_Mk318",
	"Mag_10Rnd_L96A1"
};
