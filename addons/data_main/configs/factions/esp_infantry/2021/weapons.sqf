// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { _unit addWeapon "ffaa_armas_hkg36e_tir"; };

	case "mmg_gun": { _unit addWeapon "UK3CB_MG3_KWS_B"; };

	case "plt_ldg";
	case "sqd_ldg";
	case "sqd_gre": { _unit addWeapon "ffaa_armas_hkag36e_tir"; };
	
	case "sqd_lmg": { _unit addWeapon "ffaa_armas_mg4"; };

	case "lrc_ldr";
	case "lrc_hir": { _unit addWeapon "ffaa_armas_hkag36k_tir"; };
	case "lrc_lat";
	case "lrc_rif": { _unit addWeapon "ffaa_armas_hkg36k_normal"; };
	case "lrc_lmg": { _unit addWeapon "ffaa_armas_mg4"; };
	case "lrc_dmr": { _unit addWeapon "ffaa_armas_hk417A2_long_blk"; };

	case "spf_ldg": { _unit addWeapon "rhs_weap_hk416d145_m320"; };
	case "spf_ldr";
	case "spf_bre";
	case "spf_lat";
	case "spf_sap";
	case "spf_eod";
	case "spf_rif": { _unit addWeapon "rhs_weap_hk416d10"; };
	case "spf_lmg": { _unit addWeapon "ffaa_armas_mg4"; };
	case "spf_dmr": { _unit addWeapon "ffaa_armas_aw"; }; 

	case "mbt_cmd";
	case "mbt_gun";
	case "mbt_drv";
	case "ifv_cmd";
	case "ifv_gun";
	case "ifv_drv";
	case "apc_cmd";
	case "apc_gun";
	case "apc_drv";
	case "art_cmd";
	case "art_gun";
	case "art_drv";
	case "aaa_cmd";
	case "aaa_gun";
	case "aaa_drv": { _unit addWeapon "ffaa_armas_hkg36k"; };

	case "fwc_pil";
	case "fwt_pil": { /* No primary */ };
	case "rwc_pil";
	case "rwt_pil": { _unit addWeapon "ffaa_armas_ump"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "ffaa_armas_usp"; };

	case "spf_ldr";
	case "spf_ldg";
	case "spf_lmg";
	case "spf_dmr";
	case "spf_lat";
	case "spf_sap";
	case "spf_rif": {
		_unit addWeapon "ffaa_armas_usp";
		_unit addHandgunItem "muzzle_snds_l";
	};
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat";
	case "spf_ldr";
	case "spf_lat": { _unit addWeapon "ffaa_armas_c90"; };

	case "mat_gun": {
		_unit addWeapon "ffaa_armas_c100";
		_unit addSecondaryWeaponItem "ffaa_optic_vosel";
	};
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "ffaa_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "ffaa_spike_tripode_Bag"; };
	case "dgg_ldr": { _unit addBackpack "ffaa_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "ffaa_lag40_tripode_Bag"; };
	case "aat_ldr": { _unit addBackpack "ffaa_Tripod_Bag"; };
	case "aat_gun": { _unit addBackpack "ffaa_mistral_tripode_Bag"; };
	
	case "spc_uav": { _unit addBackpack "B_UAV_01_backpack_F"; };
	
	case "sqd_bre";
	case "spf_bre": { [_unit, "ffaa_armas_sdass", 1] call bwi_armory_fnc_addToBackpack; };
};
