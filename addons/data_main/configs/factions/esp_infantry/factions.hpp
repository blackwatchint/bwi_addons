// Spanish Army (BLUFOR)
class ESP_Infantry: FactionGroup
{
	name = "Spanish Armed Forces";
	side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_esp.paa";
    icon  = "\bwi_data_main\data\icon_s_esp.paa";
    image = "\bwi_data_main\data\icon_l_esp.paa";

	#include <2021\faction.hpp>
};