allowedVehicles[] = {
	"LandRoverSoft_W",
	"LandRoverHard_W",
	"LandRoverSnatch_W",
	"LandRoverMedical_W",
	"LandRoverWMIK_GPMG_W",
	"LandRoverWMIK_HMG_W",
	"LandRoverWMIK_HAT_W",
	"LandRoverWMIK_GMG_W",
	"MANHX60_W",
	"MANHX58_W",
	"MANHX60_FB_W",
	"MANHX58_FB_W",
	"MANHX60_CO_W",
	"MANHX58_CO_W",
	"MANHX60_Fuel_W",
	"MANHX58_Fuel_W",
	"MANHX60_Repair_W",
	"MANHX58_Repair_W",
	"MANHX60_SBCOP_W",
	"MANHX58_SBCOP_W",
	"MANHX60_HBCOP_W",
	"MANHX58_HBCOP_W",
	"MANHX60_L118_W",
	"PantherCLV_RWS_W",
	"Jackal_HMG_W",
	"Coyote_HMG_W",
	"Jackal_GMG_W",
	"Coyote_GMG_W",
	"HuskyP_GPMG_W",
	"HuskyP_HMG_W",
	"HuskyP_GMG_W",
	"FV432_GPMG_W",
	"FV432_RWS_W",
	"FV510_W",
	"FV510N_W",
	"FV510C_W",
	"FV510CN_W",
	"ApacheAH1",
	"AV8B_GR9_CAS00",
	"AV8B_GR9_CAP00",
	"MQ9R_CAS",
	"MQ9R_GBU",
	"MQ9R_AGM"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"Canoe",
	"AssaultBoat",
	"RHIB_Small",
	"RHIB_ORC_GPMG",
	"RHIB_ORC_HMG",
	"SDV"
};