class MUJ_Insurgents: FactionGroup
{
	name = "Mujahideen";
	side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_muj.paa";
    icon  = "\bwi_data_main\data\icon_s_muj.paa";
    image = "\bwi_data_main\data\icon_l_muj.paa";

	#include <1986\faction.hpp>
};