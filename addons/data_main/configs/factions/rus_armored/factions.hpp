class Sov_Armored: FactionGroup
{
	name = "Soviet Motor Rifles";
	side = OPFOR;
	
	flag  = "\bwi_data_main\data\flag_ussr.paa";
    icon  = "\bwi_data_main\data\icon_s_ussr.paa";
    image = "\bwi_data_main\data\icon_l_ussr.paa";

    #include <1986\faction.hpp>
};

class Rus_Armored: FactionGroup
{
	name = "Russian Motor Rifles";
	side = OPFOR;
	
	flag  = "\bwi_data_main\data\flag_rus.paa";
    icon  = "\bwi_data_main\data\icon_s_rus.paa";
    image = "\bwi_data_main\data\icon_l_rus.paa";

	#include <2001\faction.hpp>
	#include <2013\faction.hpp>
};