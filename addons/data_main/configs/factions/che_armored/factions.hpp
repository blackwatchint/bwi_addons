class CHE_Armored: FactionGroup
{
    name = "ChDKZ (Chedaki)";
    side = OPFOR;
	
	flag  = "\bwi_data_main\data\flag_che.paa";
    icon  = "\bwi_data_main\data\icon_s_che.paa";
    image = "\bwi_data_main\data\icon_l_che.paa";

    #include <2009\faction.hpp>
};