class CDF_Airborne: FactionGroup
{
	name = "Chernarus Airborne";
	side = BLUFOR;

	flag  = "\bwi_data_main\data\flag_cdf.paa";
    icon  = "\bwi_data_main\data\icon_s_cdf.paa";
    image = "\bwi_data_main\data\icon_l_cdf.paa";

	#include <2009\faction.hpp>
	#include <2018\faction.hpp>
};