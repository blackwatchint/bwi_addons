// Parameters passed.
params ["_unit", "_role", "_year", "_type"];


// Primary weapon.
switch ( _role ) do {
	default { [_unit, [ "UK3CB_G3A3",
						"UK3CB_G3A3V"]] call bwi_armory_fnc_addRandomWeapon; };

	case "sqd_lmg": { _unit addWeapon "UK3CB_G3SG1"; };

	case "mmg_gun": { _unit addWeapon "UK3CB_MG3"; };

	case "lrc_ldr";
	case "lrc_hir";
	case "lrc_lat";
	case "lrc_rif": { _unit addWeapon "UK3CB_G3A3V"; };
	case "lrc_lmg": { _unit addWeapon "UK3CB_G3SG1"; };
	case "lrc_dmr": { _unit addWeapon "UK3CB_SVD_OLD"; };
};


// Secondary weapon.
switch ( _role ) do {
	case "plt_ldr";
	case "plt_ldg";
	case "log_sgt";
	case "med_cpm";
	case "sqd_ldr";
	case "sqd_ldg";
	case "fwc_pil";
	case "fwt_pil": { _unit addWeapon "rhs_weap_makarov_pm"; };
};


// Launcher.
switch ( _role ) do {
	case "plt_ldr";
	case "sqd_ldr";
	case "sqd_lat";
	case "lrc_lat": { _unit addWeapon "rhs_weap_rpg26"; };

	case "mat_gun": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";
	};
	case "aat_gun": { _unit addWeapon "rhs_weap_igla"; };
};


// Backpack weapon.
switch ( _role ) do {
	case "mot_ldr": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "mot_gun": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "dat_ldr": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "dat_gun": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "dgg_ldr": { _unit addBackpack "RHS_AGS30_Tripod_Bag"; };
	case "dgg_gun": { _unit addBackpack "RHS_AGS30_Gun_Bag"; };
};