allowedVehicles[] = {
	"UAZ_UN",
	"UAZ_O_UN",
	"UAZ_DSHKM_UN",
	"UAZ_AGS30_UN",
	"UAZ_SPG9_UN",
	"BTR40_UN",
	"BTR40_DSHKM_UN",
	"Ural4320_UN",
	"Ural4320_O_UN",
	"Ural4320_Recover_UN",
	"Ural4320_Fuel_UN",
	"Ural4320_Repair_UN",
	"Ural4320_Ammo_UN",
	"Ural4320_D30A_UN",
	"Ural4320_SBCOP_UN_W",
	"Ural4320_HBCOP_UN_W",
	"BRDM2_GPMG_UN",
	"BRDM2_HMG_UN",
	"BRM1K_UN",
	"BTR70_UN",
	"ZSU234_UN",
	"BMP1_UN",
	"T72A_UN",
	"T72B_UN",
	"Mi24V_UN",
	"Mi8_UN",
	"C130J_UN",
	"C130J_Cargo_UN"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat"
};