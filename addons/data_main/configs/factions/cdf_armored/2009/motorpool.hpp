allowedVehicles[] = {
	"UAZ_CDF",
	"UAZ_O_CDF",
	"UAZ_DSHKM_CDF",
	"UAZ_AGS30_CDF",
	"UAZ_SPG9_CDF",
	"BTR40_CDF",
	"BTR40_DSHKM_CDF",
	"GAZ66_CDF",
	"GAZ66_O_CDF",
	"GAZ66_FB_CDF",
	"GAZ66_O_FB_CDF",
	"GAZ66_Ammo_CDF",
	"GAZ66_Repair_CDF",
	"GAZ66_ZU23_CDF",
	"Ural4320_CDF",
	"Ural4320_O_CDF",
	"Ural4320_Recover_CDF",
	"Ural4320_Fuel_CDF",
	"Ural4320_Repair_CDF",
	"Ural4320_Ammo_CDF",
	"Ural4320_ZU23_CDF",
	"Ural4320_D30A_CDF",
	"Ural4320_SBCOP_CDF_W",
	"Ural4320_BBCOP_CDF_W",
	"BM21_CDF",
	"BRDM2_CDF",
	"BRDM2_GPMG_CDF",
	"BRDM2_HMG_CDF",
	"BRDM2_ATGM_CDF",
	"BRM1K_CDF",
	"BTR70_CDF",
	"ZSU234_CDF",
	"BMP1D_CDF",
	"BMP2E_CDF",
	"T72B84_CDF",
	"T72B85_CDF",
	"Mi24D_CDF",
	"Su25_CAS00_CDF",
	"MiG29S_Multi9090_CDF",
	"MiG29S_CAP90_CDF"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat"
};