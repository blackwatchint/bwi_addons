class Desert_2009: Faction
{
	name = "Arid DPM / Desert";
	year = 2009;
	type = INFANTRY;

	uniformScript = "\bwi_data_main\configs\factions\tka_infantry\2009\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tka_infantry\2009\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tka_infantry\2009\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tka_infantry\2009\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "HAT"};
	hiddenRoles[]	 = {"HIR", "UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\rus_color_k.paa",
		"\bwi_resupply_main\data\tkru_signs_1.paa",
		"\bwi_resupply_main\data\tkru_signs_2.paa",
		"\bwi_resupply_main\data\tkru_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_d.hpp>
};
