class Woodland_2019: Faction
{
	name = "Wood DPM / Woodland";
	year = 2019;
	type = INFANTRY;

	uniformScript = "\bwi_data_main\configs\factions\tka_infantry\2019\uniform_w.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tka_infantry\2019\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tka_infantry\2019\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tka_infantry\2019\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "HAT"};
	hiddenRoles[]	 = {"UAV"};

	acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\usa_color_g.paa",
		"\bwi_resupply_main\data\tkus_signs_1.paa",
		"\bwi_resupply_main\data\tkus_signs_2.paa",
		"\bwi_resupply_main\data\tkus_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_w.hpp>
};