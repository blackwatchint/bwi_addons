class Desert_1986: Faction
{
	name = "TKA Olive / Desert";
	year = 1986;
	type = INFANTRY;

	uniformScript = "\bwi_data_main\configs\factions\tka_infantry\1986\uniform_d.sqf";
	weaponScript  = "\bwi_data_main\configs\factions\tka_infantry\1986\weapons.sqf";
	grenadeScript = "\bwi_data_main\configs\factions\tka_infantry\1986\grenades.sqf";
	ammoScript	  = "\bwi_data_main\configs\factions\tka_infantry\1986\ammo.sqf";

	hiddenElements[] = {"UWC", "SCU", "HAT", "DGG"};
	hiddenRoles[]	 = {"HIR", "UAV", "LDG", "GRE", "LHG", "AHG"};

	acreRadioDevices[] = {"NONE", "NONE", "ACRE_SEM52SL", "ACRE_PRC77"};

	resupplyTextures[] = {
		"\bwi_resupply_main\data\rus_color_k.paa",
		"\bwi_resupply_main\data\tkru_signs_1.paa",
		"\bwi_resupply_main\data\tkru_signs_2.paa",
		"\bwi_resupply_main\data\tkru_signs_3.paa"
	};

	#include <motorpool.hpp>
	#include <resupply_d.hpp>
};