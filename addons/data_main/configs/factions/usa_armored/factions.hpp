class USA_Armored: FactionGroup
{
	name = "US Army Armored";
	side = BLUFOR;
	
	flag  = "\bwi_data_main\data\flag_usa.paa";
    icon  = "\bwi_data_main\data\icon_s_usa.paa";
    image = "\bwi_data_main\data\icon_l_usa.paa";

    #include <1986\faction.hpp>
	#include <2004\faction.hpp>
	#include <2015\faction.hpp>
};