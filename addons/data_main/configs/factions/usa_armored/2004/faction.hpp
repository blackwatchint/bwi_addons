class Desert_2004: Faction
{
    name = "UCP / Desert";
    year = 2004;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\usa_armored\2004\uniform_d.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\usa_armored\2004\weapons_d.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\usa_armored\2004\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\usa_armored\2004\ammo_d.sqf";

    hiddenElements[] = {"FWT"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\usa_color_t.paa",
        "\bwi_resupply_main\data\usd_signs_1.paa",
        "\bwi_resupply_main\data\usd_signs_2.paa",
        "\bwi_resupply_main\data\usd_signs_3.paa"
    };

    #include <motorpool_d.hpp>
    #include <resupply_d.hpp>
};


class Woodland_2004: Faction
{
    name = "UCP / Woodland";
    year = 2004;
    type = ARMORED;

    uniformScript = "\bwi_data_main\configs\factions\usa_armored\2004\uniform_w.sqf";
    weaponScript  = "\bwi_data_main\configs\factions\usa_armored\2004\weapons_w.sqf";
    grenadeScript = "\bwi_data_main\configs\factions\usa_armored\2004\grenades.sqf";
    ammoScript    = "\bwi_data_main\configs\factions\usa_armored\2004\ammo_w.sqf";

    hiddenElements[] = {"FWT"};
    hiddenRoles[]    = {"HIR", "UAV", "LHG", "AHG"};

    acreRadioDevices[] = {"ACRE_PRC343", "ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"};

    resupplyTextures[] = {
        "\bwi_resupply_main\data\usa_color_g.paa",
        "\bwi_resupply_main\data\usa_signs_1.paa",
        "\bwi_resupply_main\data\usa_signs_2.paa",
        "\bwi_resupply_main\data\usa_signs_3.paa"
    };

    #include <motorpool_w.hpp>
    #include <resupply_w.hpp>
};