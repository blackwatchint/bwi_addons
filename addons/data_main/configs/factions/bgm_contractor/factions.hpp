// Bloodgod Mercenaries
class BGM_Contractor: FactionGroup
{
	name = "Bloodgod Mercenaries";
	side = INDEP;

	flag  = "\bwi_data_main\data\flag_bgm.paa";
    icon  = "\bwi_data_main\data\icon_s_bgm.paa";
    image = "\bwi_data_main\data\icon_l_bgm.paa";

	#include <2002\faction.hpp>
	#include <2019\faction.hpp>
};