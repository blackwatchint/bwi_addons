allowedVehicles[] = {
	"M1025A1_O",
	"M1097A1_2DHT_O",
	"M1097A1_2DFT_O",
	"M1025A1_M2_O",
	"M1151A1_4D_O",
	"M1152A1_2D_O",
	"M1152A1_2D_FB_O",
	"M1152A1_2D_RSV_O",
	"M1151A1_PKM_GPK_O",
	"M1025A1_M2_GPOK_O",
	"M1151A1_Mk19_GPK_O",
	"GAZ66_G",
	"GAZ66_FB_G",
	"GAZ66_O_G",
	"GAZ66_O_FB_G",
	"GAZ66_Ammo_G",
	"GAZ66_SBCOP_G",
	"Ural375D_G",
	"Ural375D_O_G",
	"Ural375D_Recover_G",
	"Ural375D_Fuel_G",
	"Ural375D_SBCOP_G",
    "Ural375D_D30J_G",
	"PTSM_G",
	"BRDM2_G",
	"BRDM2_GPMG_G",
	"BRDM2_HMG_G",
	"BRDM2_ATGM_G",
	"BTR60_G",
	"BMP1D_G",
	"BRM1K_G",
	"ZSU234_G",
	"T72S",
	"2S1_G",
	"HT48",
	"Mi24G"
};

allowedVehiclesRecon[] = {
	"Quadbike"
};

allowedVehiclesSpecial[] = {
	"Quadbike",
	"AssaultBoat"
};