class Ser_Infantry: FactionGroup
{
    name = "Serbian Brigade";
    side = INDEP;
	
	flag  = "\bwi_data_main\data\flag_ser.paa";
    icon  = "\bwi_data_main\data\icon_s_ser.paa";
    image = "\bwi_data_main\data\icon_l_ser.paa";

    #include <2014\faction.hpp>
};