/**
 * Canoes
 */
class Canoe: Vehicle { classname = "rhsgref_hidf_canoe"; level = UTILITY; cost = 4; };


/**
 * Inflatable Boats
 */
class AssaultBoat: Vehicle { classname = "B_Boat_Transport_01_F"; level = UTILITY; cost = 16; };


/**
 * RHIBs
 */
class RHIB_Small: Vehicle { classname = "I_C_Boat_Transport_02_F"; level = UTILITY; cost = 20; name = "RHIB (Small)"; };
class Duarry_Supercat: Vehicle { classname = "ffaa_ar_supercat"; level = UTILITY; cost = 16; };
class Zodiac_Hurriance_S: Vehicle { classname = "ffaa_ar_zodiac_hurricane"; level = UTILITY; cost = 20; };
class Zodiac_Hurriance_L: Vehicle { classname = "ffaa_ar_zodiac_hurricane_long"; level = UTILITY; cost = 22; };