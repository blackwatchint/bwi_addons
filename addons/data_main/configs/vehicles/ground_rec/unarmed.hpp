/**
 * BRDM-2
 */
class BRDM2_MSV_D: Vehicle { classname = "rhsgref_BRDM2UM_msv"; level = UTILITY; cost = 36; textureData[] = {"3tone",1}; };
class BRDM2_MSV_W: Vehicle { classname = "rhsgref_BRDM2UM_msv"; level = UTILITY; cost = 36; };

class BRDM2_VDV_D: Vehicle { classname = "rhsgref_BRDM2UM_vdv"; level = UTILITY; cost = 36; textureData[] = {"3tone",1}; };
class BRDM2_VDV_W: Vehicle { classname = "rhsgref_BRDM2UM_vdv"; level = UTILITY; cost = 36; };

class BRDM2_VMF_D: Vehicle { classname = "rhsgref_BRDM2UM_vmf"; level = UTILITY; cost = 36; textureData[] = {"3tone",1}; };
class BRDM2_VMF_W: Vehicle { classname = "rhsgref_BRDM2UM_vmf"; level = UTILITY; cost = 36; };

class BRDM2_G: Vehicle { classname = "rhsgref_BRDM2UM"; level = UTILITY; cost = 36; textureData[] = {"olive",1}; };

class BRDM2_K: Vehicle { classname = "rhsgref_BRDM2UM"; level = UTILITY; cost = 36; textureData[] = {"khaki",1}; };

class BRDM2_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRDM2_UM"; level = UTILITY; cost = 36; };

class BRDM2_CDF: Vehicle    { classname = "UK3CB_I_BRDM2_UM_CDF"; level = UTILITY; cost = 36; };
class BRDM2_CDF_UN: Vehicle { classname = "UK3CB_UN_B_BRDM2_UM";  level = UTILITY; cost = 36; };
class BRDM2_CHE: Vehicle    { classname = "rhsgref_BRDM2UM_ins";  level = UTILITY; cost = 36; };

class BRDM2_INS_W: Vehicle { classname = "UK3CB_I_G_BRDM2_UM";   level = UTILITY; cost = 36; };
class BRDM2_INS_D: Vehicle { classname = "UK3CB_TKM_O_BRDM2_UM"; level = UTILITY; cost = 36; };

class BRDM2_TKA: Vehicle   { classname = "UK3CB_TKA_O_BRDM2_UM"; level = UTILITY; cost = 36; };

class BRDM2_URS: Vehicle { classname = "UK3CB_CW_SOV_O_LATE_BRDM2_UM"; level = UTILITY; cost = 36; textureData[] = {"KHK",1}; };