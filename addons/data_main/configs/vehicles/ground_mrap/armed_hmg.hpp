/**
 * Caiman MRAPs
 */
class M1220_M2_D: Vehicle    { classname = "rhsusf_M1220_M2_usarmy_d";  	 level = ANTI_APC; cost = 62; };
class M1220_M2_W: Vehicle    { classname = "rhsusf_M1220_M2_usarmy_wd"; 	 level = ANTI_APC; cost = 62; };
class M1220_CROWS_M2_D: Vehicle { classname = "rhsusf_M1220_M153_M2_usarmy_d";  level = ANTI_APC; cost = 80; };
class M1220_CROWS_M2_W: Vehicle { classname = "rhsusf_M1220_M153_M2_usarmy_wd"; level = ANTI_APC; cost = 80; };
class M1230_M2_D: Vehicle    { classname = "rhsusf_M1230_M2_usarmy_d";  	 level = ANTI_APC; cost = 62; };
class M1230_M2_W: Vehicle    { classname = "rhsusf_M1230_M2_usarmy_wd"; 	 level = ANTI_APC; cost = 62; };


/**
 * RG-33 MRAPs (US Army Variants)
 */
class M1232_M2_D: Vehicle   { classname = "rhsusf_M1232_M2_usarmy_d";	level = ANTI_APC; cost = 72; };
class M1232_M2_W: Vehicle   { classname = "rhsusf_M1232_M2_usarmy_wd";	level = ANTI_APC; cost = 72; };
class M1237_M2_D: Vehicle   { classname = "rhsusf_M1237_M2_usarmy_d";	level = ANTI_APC; cost = 82; };
class M1237_M2_W: Vehicle   { classname = "rhsusf_M1237_M2_usarmy_wd";	level = ANTI_APC; cost = 82; };

class M1238A1_CROWS_M2_D: Vehicle { classname = "rhsusf_M1238A1_M2_socom_d";			level = ANTI_APC; cost = 86; };
class M1238A1_CROWS_M2_W: Vehicle { classname = "rhsusf_M1238A1_M2_socom_wd";			level = ANTI_APC; cost = 86; }; 

class M1239_CROWS_M2_D: Vehicle { classname = "rhsusf_M1239_M2_socom_d";				level = ANTI_APC; cost = 64; };
class M1239_CROWS_M2_W: Vehicle { classname = "rhsusf_M1239_M2_socom_wd";				level = ANTI_APC; cost = 64; };
class M1239_Deploy_CROWS_M2_D: Vehicle { classname = "rhsusf_M1239_M2_Deploy_socom_d";	level = ANTI_APC; cost = 64; };
class M1239_Deploy_CROWS_M2_W: Vehicle { classname = "rhsusf_M1239_M2_Deploy_socom_wd";	level = ANTI_APC; cost = 64; };


/**
 * RG-33 MRAPs (US Marines Variants)
 */
class M1232_MC_M2_D: Vehicle   { classname = "rhsusf_M1232_MC_M2_usmc_d";  level = ANTI_APC; cost = 72; };
class M1232_MC_M2_W: Vehicle   { classname = "rhsusf_M1232_MC_M2_usmc_wd"; level = ANTI_APC; cost = 72; };


/**
 * M-ATV MRAPs (US Army Variants)
 */
class M1240_M240_D: Vehicle  { classname = "rhsusf_m1240a1_m240_usarmy_d";   level = ANTI_APC; cost = 44; };
class M1240_M240_W: Vehicle  { classname = "rhsusf_m1240a1_m240_usarmy_wd";  level = ANTI_APC; cost = 44; };
class M1240_M2_D: Vehicle    { classname = "rhsusf_m1240a1_m2_usarmy_d";  	 level = ANTI_APC; cost = 50; };
class M1240_M2_W: Vehicle    { classname = "rhsusf_m1240a1_m2_usarmy_wd"; 	 level = ANTI_APC; cost = 50; };
class M1240_CROWS_M2_D: Vehicle { classname = "rhsusf_m1240a1_m2crows_usarmy_d";  level = ANTI_APC; cost = 64; animationData[] = {"hide_qnet",0,"hide_qnetwindow",1}; };
class M1240_CROWS_M2_W: Vehicle { classname = "rhsusf_m1240a1_m2crows_usarmy_wd"; level = ANTI_APC; cost = 64; animationData[] = {"hide_qnet",0,"hide_qnetwindow",1}; };


/**
 * CAT MRAPs (US Marines Variants)
 */
class CGRCAT1A2_M2_D: Vehicle   { classname = "rhsusf_CGRCAT1A2_M2_usmc_d";  level = ANTI_APC; cost = 62; };
class CGRCAT1A2_M2_W: Vehicle   { classname = "rhsusf_CGRCAT1A2_M2_usmc_wd"; level = ANTI_APC; cost = 62; };


/**
 * Maxxpro MRAPs (US Army Variants)
 */
class MaxxPro_M2_D: Vehicle { classname = "UK3CB_B_MaxxPro_M2_US";   level = ANTI_APC; cost = 62; };


/**
 * Panther CLV
 */
class PantherCLV_RWS_D: Vehicle { classname = "UK3CB_BAF_Panther_GPMG_Sand_A";  level = ANTI_APC; cost = 62; };
class PantherCLV_RWS_W: Vehicle { classname = "UK3CB_BAF_Panther_GPMG_Green_A"; level = ANTI_APC; cost = 62; };


/**
 * Jackal/Coyote
 */
class Jackal_HMG_D: Vehicle { classname = "UK3CB_BAF_Jackal2_L111A1_D";  level = ANTI_CAR; cost = 40; };
class Jackal_HMG_W: Vehicle { classname = "UK3CB_BAF_Jackal2_L111A1_W";  level = ANTI_CAR; cost = 40; };
class Coyote_HMG_D: Vehicle { classname = "UK3CB_BAF_Coyote_Passenger_L111A1_D";  level = ANTI_CAR; cost = 48; };
class Coyote_HMG_W: Vehicle { classname = "UK3CB_BAF_Coyote_Passenger_L111A1_W";  level = ANTI_CAR; cost = 48; };


/**
 * Husky TSV
 */
class HuskyP_GPMG_D: Vehicle { classname = "UK3CB_BAF_Husky_Passenger_GPMG_Sand";  level = ANTI_APC; cost = 40; };
class HuskyP_GPMG_W: Vehicle { classname = "UK3CB_BAF_Husky_Passenger_GPMG_Green"; level = ANTI_APC; cost = 40; };
class HuskyP_HMG_D:  Vehicle { classname = "UK3CB_BAF_Husky_Passenger_HMG_Sand";   level = ANTI_APC; cost = 48; };
class HuskyP_HMG_W:  Vehicle { classname = "UK3CB_BAF_Husky_Passenger_HMG_Green";  level = ANTI_APC; cost = 48; };


/**
 * Eagle IV
 */
class EagleIV_FLW_D: Vehicle { classname = "BWA3_Eagle_FLW100_Tropen"; level = ANTI_APC; cost = 64; animationData[] = {"hide_rope",1,"backpack_back",1,"backpack_top",1,"tarnrolle_1",0}; };
class EagleIV_FLW_W: Vehicle { classname = "BWA3_Eagle_FLW100_Fleck";  level = ANTI_APC; cost = 64; animationData[] = {"hide_rope",1,"backpack_back",1,"backpack_top",1,"tarnrolle_1",0}; };


/**
 * Dingo
 */
class Dingo2_MG3_D: Vehicle { classname = "BWA3_Dingo2_FLW100_MG3_Tropen"; level = ANTI_APC; cost = 68; }; 
class Dingo2_MG3_W: Vehicle { classname = "BWA3_Dingo2_FLW100_MG3_Fleck"; level = ANTI_APC; cost = 68; };
class Dingo2_M2_D: Vehicle { classname = "BWA3_Dingo2_FLW200_M2_Tropen"; level = ANTI_APC; cost = 80; };
class Dingo2_M2_W: Vehicle { classname = "BWA3_Dingo2_FLW200_M2_Fleck"; level = ANTI_APC; cost = 80; };


/**
 * RG-31 Mk. 5E Nyala
 */
class RG31_M2_CROWS_ESP: Vehicle 			{ classname = "ffaa_et_rg31_samson";   level = ANTI_APC; cost = 64; };
class RG31_M2_CROWS_SPARKII_ESP: Vehicle 	{ classname = "ffaa_et_rg31_rollers";   level = ANTI_APC; cost = 66; };