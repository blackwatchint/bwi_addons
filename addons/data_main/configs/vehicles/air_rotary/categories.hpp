class RotaryTransport: Category {
    name = "Rotary Transport";
    type = HELIPAD;

	#include <unarmed.hpp>
};

class RotaryCAS: Category {
    name = "Rotary CAS";
    type = HELIPAD;

	#include <armed.hpp>
};