class FixedTransport: Category {
    name = "Fixed-Wing Transport";
    type = APRON;

	#include <unarmed.hpp>
};

class FixedCAS: Category {
    name = "Fixed-Wing CAS";
    type = HANGAR;

	#include <armed_cas.hpp>
    #include <armed_cvw.hpp> // Include for ground too.
};

class FixedCVW: Category {
    name = "Carrier-Based CAS";
    type = CARRIER;

	#include <armed_cvw.hpp>
};