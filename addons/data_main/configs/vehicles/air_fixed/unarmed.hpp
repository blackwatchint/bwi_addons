/**
 * C-130 (1956-)
 */
class C130: Vehicle		  { classname = "UK3CB_CW_US_B_EARLY_C130J"; name = "C-130J"; level = UTILITY; cost = 40; isOutpost = 1; };
class C130_Cargo: Vehicle { classname = "UK3CB_CW_US_B_EARLY_C130J"; name = "C-130J (Cargo)"; level = UTILITY; cost = 40; animationData[] = {"hide_cargo",1}; };


/**
 * C-130J (1999-)
 */
class C130J: Vehicle 	   { classname = "RHS_C130J"; name = "C-130J"; level = UTILITY; cost = 40; isOutpost = 1; };
class C130J_Cargo: Vehicle { classname = "RHS_C130J"; name = "C-130J (Cargo)"; level = UTILITY; cost = 40; animationData[] = {"hide_cargo",1}; };

class C130J_USA: Vehicle 	   { classname = "USAF_C130J"; 		 name = "C-130J"; 		  level = UTILITY; cost = 40; isOutpost = 1; };
class C130J_Cargo_USA: Vehicle { classname = "USAF_C130J_Cargo"; name = "C-130J (Cargo)"; level = UTILITY; cost = 40; };

class C130J_UN: Vehicle		  { classname = "UK3CB_UN_I_C130J"; name = "C-130J"; level = UTILITY; cost = 40; };
class C130J_Cargo_UN: Vehicle { classname = "UK3CB_UN_I_C130J"; name = "C-130J (Cargo)"; level = UTILITY; cost = 40; animationData[] = {"hide_cargo",1}; };

class C130J_TKA: Vehicle	   { classname = "UK3CB_TKA_O_C130J"; name = "C-130J";			level = UTILITY; cost = 40; isOutpost = 1; };
class C130J_Cargo_TKA: Vehicle { classname = "UK3CB_TKA_O_C130J"; name = "C-130J (Cargo)";	level = UTILITY; cost = 40; animationData[] = {"hide_cargo",1}; };

class C130J_ESP: Vehicle	   { classname = "ffaa_ea_hercules"; 		name = "C-130J";			level = UTILITY; cost = 40; isOutpost = 1; };
class C130J_Cargo_ESP: Vehicle { classname = "ffaa_ea_hercules_cargo"; 	name = "C-130J (Cargo)";	level = UTILITY; cost = 40; };

/**
 * C-17 Globemaster III (1995-)
 */
class C17_Airborne: Vehicle	{ classname = "USAF_C17"; name = "C-17 Globemaster III"; level = UTILITY; cost = 80; isOutpost = 1; };
class C17_Armored: Vehicle	{ classname = "USAF_C17"; name = "C-17 Globemaster III"; level = UTILITY; cost = 80; isOutpost = 0; };


/**
 * Hercules C3 (1979-)
 */
class C130C3: Vehicle 		{ classname = "UK3CB_BAF_Hercules_C3_DDPM"; name = "Hercules C3"; level = UTILITY; cost = 40; isOutpost = 1; };
class C130C3_Cargo: Vehicle { classname = "UK3CB_BAF_Hercules_C3_DDPM"; name = "Hercules C3 (Cargo)"; level = UTILITY; cost = 40; animationData[] = {"hide_cargo",1}; };


/**
 * Hercules C4 (2014-)
 */
class C130C4: Vehicle 		{ classname = "UK3CB_BAF_Hercules_C4_DDPM"; name = "Hercules C4"; level = UTILITY; cost = 40; isOutpost = 1; };
class C130C4_Cargo: Vehicle { classname = "UK3CB_BAF_Hercules_C4_DDPM"; name = "Hercules C4 (Cargo)"; level = UTILITY; cost = 40; animationData[] = {"hide_cargo",1}; };


/**
 * Antonov An-2 (1950-)
 */
class AN2_SOV: Vehicle { classname = "UK3CB_CW_SOV_O_EARLY_Antonov_AN2"; name = "Antonov An-2"; level = UTILITY; cost = 20; isOutpost = 1; };

class AN2_CDF: Vehicle { classname = "RHS_AN2_B"; name = "Antonov An-2"; level = UTILITY; cost = 20; isOutpost = 1; };

class AN2_TKA: Vehicle { classname = "UK3CB_TKA_O_Antonov_AN2"; name = "Antonov An-2"; level = UTILITY; cost = 20; isOutpost = 1; };


/**
 * Antonov An-12 (1959-)
 */
class AN12_RUS: Vehicle { classname = "pook_AN12B_OPFOR";  name = "Antonov An-12B"; textureData[] = {"pook_AN12_TEX_RUS3",1}; level = UTILITY; cost = 40; isOutpost = 1; };


/**
 * Antonov An-24 (1959-)
 */
class AN24_CDF: Vehicle { classname = "pook_AN24_BLUFOR"; name = "Antonov An-24"; textureData[] = {"pook_AN24_ZZZ_GRN",1}; level = UTILITY; cost = 40; isOutpost = 1; };


/**
 * Ilyushin Il-18
 */
class IL18_SOV: Vehicle { classname = "pook_IL18_OPFOR"; name = "Ilyushin Il-18"; textureData[] = {"pook_IL18_75598",1}; level = UTILITY; cost = 40; isOutpost = 1; };


/**
 * O-3A (Horizon Islands 1972-)
 */
class O3A_TAN: Vehicle { classname = "rhsgred_hidf_cessna_o3a"; name = "Cessna O-3A"; level = UTILITY; cost = 12; };