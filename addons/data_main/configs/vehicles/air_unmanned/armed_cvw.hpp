/**
 * RQ-170 (US Navy)
 */
class RQ170_GBU: Vehicle { 
	classname = "B_UAV_05_F";  
	level = ANTI_APC; 
	cost = 82; 

	name = "RQ-170 (GBU)";

	pylonLoadout[] = {
		"PylonMissile_Bomb_GBU12_x1",
		"PylonMissile_Bomb_GBU12_x1"
	};
	pylonTurrets[] = {0,0};
};
class RQ170_SDB: Vehicle { 
	classname = "B_UAV_05_F";  
	level = ANTI_APC; 
	cost = 90; 

	name = "RQ-170 (SDB)";

	pylonLoadout[] = {
		"PylonRack_Bomb_SDB_x4",
		"PylonRack_Bomb_SDB_x4"
	};
	pylonTurrets[] = {0,0};
};
class RQ170_AGM: Vehicle { 
	classname = "B_UAV_05_F";  
	level = ANTI_APC; 
	cost = 90; 

	name = "RQ-170 (AGM)";

	pylonLoadout[] = {
		"PylonMissile_Missile_AGM_02_x2",
		"PylonMissile_Missile_AGM_02_x2"
	};
	pylonTurrets[] = {0,0};
};
class RQ170_HARM: Vehicle { 
	classname = "B_UAV_05_F";  
	level = ANTI_APC; 
	cost = 82; 

	name = "RQ-170 (HARM)";

	pylonLoadout[] = {
		"PylonMissile_Missile_HARM_INT_x1",
		"PylonMissile_Missile_HARM_INT_x1"
	};
	pylonTurrets[] = {0,0};
};