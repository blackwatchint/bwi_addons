class MBT: Element
{
    name = "Armor Crew (MBT)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Kodiak 1", "Kodiak 2", "Kodiak 3", "Kodiak 4", "Kodiak 5", "Kodiak 6"};

    #include <roles.hpp> // Shared roles
};

class IFV: Element
{
    name = "Armor Crew (IFV)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Rhino 1", "Rhino 2", "Rhino 3", "Rhino 4", "Rhino 5", "Rhino 6"};

    #include <roles.hpp> // Shared roles
};

class APC: Element
{
    name = "Armor Crew (APC)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Ibex 1", "Ibex 2", "Ibex 3", "Ibex 4", "Ibex 5", "Ibex 6"}; // Or Bobcat?

    #include <roles.hpp> // Shared roles
};

class ART: Element
{
    name = "Armor Crew (Artillery)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Bison 1", "Bison 2", "Bison 3", "Bison 4", "Bison 5", "Bison 6"};

    #include <roles.hpp> // Shared roles
};

class AAA: Element
{
    name = "Armor Crew (Anti-Air)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Mantis 1", "Mantis 2", "Mantis 3", "Mantis 4", "Mantis 5", "Mantis 6"};

    #include <roles.hpp> // Shared roles
};