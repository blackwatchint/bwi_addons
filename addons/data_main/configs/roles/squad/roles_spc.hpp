class EOD: Role
{
    name = "Explosive Ordnance Disposal";

    aceIsEOD = 1;

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};

class UAV: Role
{
    name = "UAV Operator";

    acreRadioChannels[] = {1,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};