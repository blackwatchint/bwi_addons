class SQD: Element
{
    name = "Squad (Regulars)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot"};

    #include <roles_sqd.hpp>
};

class SPC: Element
{
    name = "Squad (Specialists)";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot"};

    #include <roles_spc.hpp>
};