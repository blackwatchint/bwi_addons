class LDR: Role
{
    name = "Leader";
    showOnCommcard = 1;

    acreRadioChannels[] = {2,0,1,0};

    allowedAccessories[] = {3,3,1,1};
};

class GUN: Role
{
    name = "Gunner";

    acreRadioChannels[] = {2,0,0,0};

    allowedAccessories[] = {3,3,1,1};
};
