class MMG: Element
{
    name = "Medium Machine Gun Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Sabre 1", "Sabre 2", "Sabre 3"};

    isWeaponsElement = 1;

    #include <roles_mmg.hpp> // Unique role, has bipods and optics.
};

class MAT: Element
{
    name = "Medium Anti-Tank Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Trident 1", "Trident 2", "Trident 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};

class HAT: Element
{
    name = "Heavy Anti-Tank Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Lancer 1", "Lancer 2", "Lancer 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};

class AAT: Element
{
    name = "Anti-Air Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Archer 1", "Archer 2", "Archer 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};

class MOT: Element
{
    name = "Mortar Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Mailman 1", "Mailman 2", "Mailman 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};

class HOW: Element
{
    name = "Howitzer Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Postbox 1", "Postbox 2", "Postbox 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};

class DAT: Element
{
    name = "Deployed Anti-Tank Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Halberd 1", "Halberd 2", "Halberd 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};

class DGG: Element
{
    name = "Deployed Grenade Gun Team";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Hammer 1", "Hammer 2", "Hammer 3"};

    isWeaponsElement = 1;

    #include <roles.hpp> // Shared roles
};