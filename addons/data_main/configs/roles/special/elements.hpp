class LRC: Element
{
    name = "Light Recon";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Recon 1", "Recon 2", "Recon 3"};

    isReconElement = 1;

    #include <roles_lrc.hpp>
};

class SPF: Element
{
    name = "Special Forces";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Spartan 1", "Spartan 2", "Spartan 3"};

    isSpecialElement = 1;

    #include <roles_spf.hpp>
};

class SCU: Element
{
    name = "Scuba Forces";
    sides[] = {BLUFOR, OPFOR, INDEP};
    callsigns[] = {"Viking 1", "Viking 2", "Viking 3"};

    isSpecialElement = 1;

    #include <roles_scu.hpp>
};