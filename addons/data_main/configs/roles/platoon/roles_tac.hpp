class JTC: Role
{
    name = "Joint Terminal Attack Controller";
    showOnCommcard = 1;

    acreRadioChannels[] = {0,4,1,0};

    allowedAccessories[] = {2,3,1,1};
};

class FAC: Role
{
    name = "Forward Air Controller";
    showOnCommcard = 1;

    acreRadioChannels[] = {0,4,0,3};

    allowedAccessories[] = {2,3,1,1};
};