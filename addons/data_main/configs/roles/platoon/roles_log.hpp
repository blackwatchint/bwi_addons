class SGT: Role
{
    name = "Sergeant";
    showOnCommcard = 1;

    aceIsEngineer = 2;

    acreRadioChannels[] = {0,2,1,0};

    allowedAccessories[] = {2,3,1,1};
};