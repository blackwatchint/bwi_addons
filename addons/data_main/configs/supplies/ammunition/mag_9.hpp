class Mag_20Rnd_SP5: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_SP5";
	compatible[] = {"AS Val", "VSS Vintorez"};
	textureIndexes[] = {2,0};
};

class Mag_20Rnd_SP6: Supply {
	classname = "BWI_Resupply_Mag_20Rnd_SP6";
	compatible[] = {"AS Val", "VSS Vintorez"};
	textureIndexes[] = {2,0};
};