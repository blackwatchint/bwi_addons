class Box_100Rnd_M80: Supply {
	classname = "BWI_Resupply_Box_100Rnd_M80";
	compatible[] = {"M240", "FN MAG", "L7A2"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_M80A1: Supply {
	classname = "BWI_Resupply_Box_100Rnd_M80A1";
	compatible[] = {"M240", "L7A2"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_M61AP: Supply {
	classname = "BWI_Resupply_Box_100Rnd_M61AP";
	compatible[] = {"M240", "L7A2"};
	textureIndexes[] = {1,0};
};

class Box_50Rnd_M80_SP: Supply {
	classname = "BWI_Resupply_Box_50Rnd_M80_SP";
	compatible[] = {"M240", "L7A2"};
	textureIndexes[] = {1,0};
};

class Box_50Rnd_M80A1_SP: Supply {
	classname = "BWI_Resupply_Box_50Rnd_M80A1_SP";
	compatible[] = {"M240", "L7A2"};
	textureIndexes[] = {1,0};
};

class Box_50Rnd_M61AP_SP: Supply {
	classname = "BWI_Resupply_Box_50Rnd_M61AP_SP";
	compatible[] = {"M240", "L7A2"};
	textureIndexes[] = {1,0};
};

class Box_120Rnd_MG5: Supply {
	classname = "BWI_Resupply_Box_120Rnd_MG5";
	compatible[] = {"MG5"};
	textureIndexes[] = {1,0};
};

class Box_120Rnd_MG3: Supply {
	classname = "BWI_Resupply_Box_120Rnd_MG3";
	compatible[] = {"MG3"};
	textureIndexes[] = {1,0};
};

class Box_250Rnd_MG3: Supply {
	classname = "BWI_Resupply_Box_250Rnd_MG3";
	compatible[] = {"MG3"};
	textureIndexes[] = {1,0};
};

class Box_75Rnd_RPK: Supply {
	classname = "BWI_Resupply_Box_75Rnd_RPK";
	compatible[] = {"RPK"};
	textureIndexes[] = {1,0};
};

class Box_250Rnd_M30: Supply {
	classname = "BWI_Resupply_Box_250Rnd_M30";
	compatible[] = {"M84"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_57N323S: Supply {
	classname = "BWI_Resupply_Box_100Rnd_57N323S";
	compatible[] = {"PKM", "PKP"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_7N13: Supply {
	classname = "BWI_Resupply_Box_100Rnd_7N13";
	compatible[] = {"PKM", "PKP"};
	textureIndexes[] = {1,0};
};

class Box_100Rnd_M13: Supply {
	classname = "BWI_Resupply_Box_100Rnd_M13";
	compatible[] = {"M60"};
	textureIndexes[] = {1,0};
};

class Box_150Rnd_Negev: Supply {
	classname = "BWI_Resupply_Box_150Rnd_Negev";
	compatible[] = {"Negev"};
	textureIndexes[] = {1,0};
};

class Box_50Rnd_G3SG1: Supply {
	classname = "BWI_Resupply_Box_50Rnd_G3SG1";
	compatible[] = {"HK G3/SG1"};
	textureIndexes[] = {1,0};
};

class Box_30Rnd_BrenLMG: Supply {
	classname = "BWI_Resupply_Box_30Rnd_BrenLMG";
	compatible[] = {"Bren LMG"};
	textureIndexes[] = {1,0};
};