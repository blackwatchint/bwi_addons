class Deployable: Category {
    name = "Deployable Weapons";

    #include <dmg.hpp> // Deployable Machine Gun
    #include <dsl.hpp> // Deployable searlicht
    #include <dat.hpp> // Deployable anti tank

};