class DMG_M2_Low: Supply {
	classname = "B_G_HMG_02_F";
	compatible[] = {"Infantry"};
};

class DMG_M2_High: Supply {
	classname = "B_G_HMG_02_high_F";
	compatible[] = {"Infantry"};
};

class DMG_DShKM_Low: Supply {
	classname = "rhsgref_ins_DSHKM_Mini_TriPod";
	compatible[] = {"Infantry"};
};

class DMG_DShKM_High: Supply {
	classname = "rhsgref_ins_DSHKM";
	compatible[] = {"Infantry"};
};

class DMG_Kord_Low: Supply {
	classname = "rhs_KORD_MSV";
	compatible[] = {"Infantry"};
};

class DMG_Kord_High: Supply {
	classname = "rhs_KORD_high_MSV";
	compatible[] = {"Infantry"};
};

class DMG_MG3: Supply {
	classname = "rnt_mg3_static_ai";
	compatible[] = {"Infantry"};
};