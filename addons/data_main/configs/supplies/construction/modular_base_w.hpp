class BaseWallWood: Supply {
	classname = "BWI_Construction_BaseWallWood";
	compatible[] = {">1000m"};
};

class BaseRampartWood: Supply {
	classname = "BWI_Construction_BaseRampartWood";
	compatible[] = {">1000m"};
};

class BaseRampartCornerWood: Supply {
	classname = "BWI_Construction_BaseRampartCornerWood";
	compatible[] = {">1000m"};
};

class BaseRampartInvCornerWood: Supply {
	classname = "BWI_Construction_BaseRampartInvCornerWood";
	compatible[] = {">1000m"};
};

class BaseRampartBunkerWood: Supply {
	classname = "BWI_Construction_BaseRampartBunkerWood";
	compatible[] = {">1000m"};
};

class BaseWallCargoWood: Supply {
	classname = "BWI_Construction_BaseWallCargoWood";
	compatible[] = {">1000m"};
};

class BaseWallCornerBunkerWood: Supply {
	classname = "BWI_Construction_BaseWallCornerBunkerWood";
	compatible[] = {">1000m"};
};

class BaseWallCornerCargoWood: Supply {
	classname = "BWI_Construction_BaseWallCornerCargoWood";
	compatible[] = {">1000m"};
};

class BaseSideEntranceWood: Supply {
	classname = "BWI_Construction_BaseSideEntranceWood";
	compatible[] = {">1000m"};
};

class BaseMainEntranceBunkerWood: Supply {
	classname = "BWI_Construction_BaseMainEntranceBunkerWood";
	compatible[] = {">1000m"};
};

class BaseMainEntranceCargoWood: Supply {
	classname = "BWI_Construction_BaseMainEntranceCargoWood";
	compatible[] = {">1000m"};
};

class BaseAmmoDumpWoodWest: Supply {
	classname = "BWI_Construction_BaseAmmoDumpWoodWest";
	compatible[] = {">1000m"};
};

class BaseAmmoDumpWoodEast: Supply {
	classname = "BWI_Construction_BaseAmmoDumpWoodEast";
	compatible[] = {">1000m"};
};

class BaseRampartAmmoDumpWoodWest: Supply {
	classname = "BWI_Construction_BaseRampartAmmoDumpWoodWest";
	compatible[] = {">1000m"};
};

class BaseRampartAmmoDumpWoodEast: Supply {
	classname = "BWI_Construction_BaseRampartAmmoDumpWoodEast";
	compatible[] = {">1000m"};
};

class BaseVehicleResupplyWoodWest: Supply {
	classname = "BWI_Construction_BaseVehicleResupplyWoodWest";
	compatible[] = {">1000m"};
};

class BaseVehicleResupplyWoodEast: Supply {
	classname = "BWI_Construction_BaseVehicleResupplyWoodEast";
	compatible[] = {">1000m"};
};

class BaseFuelBladderWood: Supply {
	classname = "BWI_Construction_BaseFuelBladderWood";
	compatible[] = {">1000m"};
};

class BaseHelipadWood: Supply {
	classname = "BWI_Construction_BaseHelipadWood";
	compatible[] = {">1000m"};
};

class BaseCargoHouseWood: Supply {
	classname = "BWI_Construction_BaseCargoHouseWood";
	compatible[] = {">1000m"};
};

class BaseCargoHQWood: Supply {
	classname = "BWI_Construction_BaseCargoHQWood";
	compatible[] = {">1000m"};
};

class BaseCargoTowerWood: Supply {
	classname = "BWI_Construction_BaseCargoTowerWood";
	compatible[] = {">1000m"};
};