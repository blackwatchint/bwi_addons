class COPSandbagWoodWest: Supply {
	classname = "BWI_Staging_COPSandbagWoodWest";
	compatible[] = {">3000m"};
};

class COPSandbagWoodEast: Supply {
	classname = "BWI_Staging_COPSandbagWoodEast";
	compatible[] = {">3000m"};
};

class COPSandbagTallWoodWest: Supply {
	classname = "BWI_Staging_COPSandbagTallWoodWest";
	compatible[] = {">3000m"};
};

class COPSandbagTallWoodEast: Supply {
	classname = "BWI_Staging_COPSandbagTallWoodEast";
	compatible[] = {">3000m"};
};

class COPBarricadeWoodEast: Supply {
	classname = "BWI_Staging_COPBarricadeWoodEast";
	compatible[] = {">3000m"};
};

class COPBarricadeTallWoodEast: Supply {
	classname = "BWI_Staging_COPBarricadeTallWoodEast";
	compatible[] = {">3000m"};
};

class COPHescoWoodWest: Supply {
	classname = "BWI_Staging_COPHescoWoodWest";
	compatible[] = {">3000m"};
};

class COPHescoWoodEast: Supply {
	classname = "BWI_Staging_COPHescoWoodEast";
	compatible[] = {">3000m"};
};

class COPHescoHeavyWoodWest: Supply {
	classname = "BWI_Staging_COPHescoHeavyWoodWest";
	compatible[] = {">3000m"};
};

class COPHescoHeavyWoodEast: Supply {
	classname = "BWI_Staging_COPHescoHeavyWoodEast";
	compatible[] = {">3000m"};
};