class SquadBarricadeWoodEast: Supply {
	classname = "BWI_Construction_SquadBarricadeWoodEast";
	compatible[] = {">1000m"};
};

class SquadBarricadeWoodWest: Supply {
	classname = "BWI_Construction_SquadBarricadeWoodWest";
	compatible[] = {">1000m"};
};

class SquadBarricadeTallWoodEast: Supply {
	classname = "BWI_Construction_SquadBarricadeTallWoodEast";
	compatible[] = {">1000m"};
};

class SquadBarricadeTallWoodWest: Supply {
	classname = "BWI_Construction_SquadBarricadeTallWoodWest";
	compatible[] = {">1000m"};
};