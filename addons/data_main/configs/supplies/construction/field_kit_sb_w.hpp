class SquadSandbagWoodEast: Supply {
	classname = "BWI_Construction_SquadSandbagWoodEast";
	compatible[] = {">1000m"};
};

class SquadSandbagWoodWest: Supply {
	classname = "BWI_Construction_SquadSandbagWoodWest";
	compatible[] = {">1000m"};
};

class SquadSandbagTallWoodEast: Supply {
	classname = "BWI_Construction_SquadSandbagTallWoodEast";
	compatible[] = {">1000m"};
};

class SquadSandbagTallWoodWest: Supply {
	classname = "BWI_Construction_SquadSandbagTallWoodWest";
	compatible[] = {">1000m"};
};

class MortarSandbagWood: Supply {
	classname = "BWI_Construction_MortarSandbagWood";
	compatible[] = {">1000m"};
};

class BunkerSandbagWood: Supply {
	classname = "BWI_Construction_BunkerSandbagWood";
	compatible[] = {">1000m"};
};

class BunkerSandbagLargeWood: Supply {
	classname = "BWI_Construction_BunkerSandbagLargeWood";
	compatible[] = {">1000m"};
};

class CheckpointSandbagWoodWest: Supply {
	classname = "BWI_Construction_CheckpointSandbagWoodWest";
	compatible[] = {">1000m"};
};

class CheckpointSandbagWoodEast: Supply {
	classname = "BWI_Construction_CheckpointSandbagWoodEast";
	compatible[] = {">1000m"};
};