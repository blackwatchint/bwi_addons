class SquadTrench90DesertEast: Supply {
	classname = "BWI_Construction_SquadTrench90DesertEast";
	compatible[] = {">1000m"};
};

class SquadTrench90DesertWest: Supply {
	classname = "BWI_Construction_SquadTrench90DesertWest";
	compatible[] = {">1000m"};
};

class SquadTrench180DesertEast: Supply {
	classname = "BWI_Construction_SquadTrench180DesertEast";
	compatible[] = {">1000m"};
};

class SquadTrench180DesertWest: Supply {
	classname = "BWI_Construction_SquadTrench180DesertWest";
	compatible[] = {">1000m"};
};

class SquadTrench360DesertEast: Supply {
	classname = "BWI_Construction_SquadTrench360DesertEast";
	compatible[] = {">1000m"};
};

class SquadTrench360DesertWest: Supply {
	classname = "BWI_Construction_SquadTrench360DesertWest";
	compatible[] = {">1000m"};
};

class VehicleTrenchDesert: Supply {
	classname = "BWI_Construction_VehicleTrenchDesert";
	compatible[] = {">1000m"};
};

class VehicleTrenchCamoDesertEast: Supply {
	classname = "BWI_Construction_VehicleTrenchCamoDesertEast";
	compatible[] = {">1000m"};
};

class VehicleTrenchCamoDesertWest: Supply {
	classname = "BWI_Construction_VehicleTrenchCamoDesertWest";
	compatible[] = {">1000m"};
};

class VehicleTrenchLargeDesertEast: Supply {
	classname = "BWI_Construction_VehicleTrenchLargeDesertEast";
	compatible[] = {">1000m"};
};

class VehicleTrenchLargeDesertWest: Supply {
	classname = "BWI_Construction_VehicleTrenchLargeDesertWest";
	compatible[] = {">1000m"};
};