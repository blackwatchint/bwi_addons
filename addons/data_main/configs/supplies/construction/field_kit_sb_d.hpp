class SquadSandbagDesertEast: Supply {
	classname = "BWI_Construction_SquadSandbagDesertEast";
	compatible[] = {">1000m"};
};

class SquadSandbagDesertWest: Supply {
	classname = "BWI_Construction_SquadSandbagDesertWest";
	compatible[] = {">1000m"};
};

class SquadSandbagTallDesertEast: Supply {
	classname = "BWI_Construction_SquadSandbagTallDesertEast";
	compatible[] = {">1000m"};
};

class SquadSandbagTallDesertWest: Supply {
	classname = "BWI_Construction_SquadSandbagTallDesertWest";
	compatible[] = {">1000m"};
};

class MortarSandbagDesert: Supply {
	classname = "BWI_Construction_MortarSandbagDesert";
	compatible[] = {">1000m"};
};

class BunkerSandbagDesert: Supply {
	classname = "BWI_Construction_BunkerSandbagDesert";
	compatible[] = {">1000m"};
};

class BunkerSandbagLargeDesert: Supply {
	classname = "BWI_Construction_BunkerSandbagLargeDesert";
	compatible[] = {">1000m"};
};

class CheckpointSandbagDesertWest: Supply {
	classname = "BWI_Construction_CheckpointSandbagDesertWest";
	compatible[] = {">1000m"};
};

class CheckpointSandbagDesertEast: Supply {
	classname = "BWI_Construction_CheckpointSandbagDesertEast";
	compatible[] = {">1000m"};
};