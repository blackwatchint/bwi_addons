class BunkerBarricadeSmall: Supply {
	classname = "BWI_Construction_BunkerBarricadeSmall";
	compatible[] = {">1000m"};
};

class BunkerBarricadeMedium: Supply {
	classname = "BWI_Construction_BunkerBarricadeMedium";
	compatible[] = {">1000m"};
};

class BunkerBarricadeLarge: Supply {
	classname = "BWI_Construction_BunkerBarricadeLarge";
	compatible[] = {">1000m"};
};

class CheckpointBarricadeEast: Supply {
	classname = "BWI_Construction_CheckpointBarricadeEast";
	compatible[] = {">1000m"};
};