class SquadHescoWoodWest: Supply {
	classname = "BWI_Construction_SquadHescoWoodWest";
	compatible[] = {">1000m"};
};

class SquadHescoWoodEast: Supply {
	classname = "BWI_Construction_SquadHescoWoodEast";
	compatible[] = {">1000m"};
};

class VehicleHescoWood: Supply {
	classname = "BWI_Construction_VehicleHescoWood";
	compatible[] = {">1000m"};
};

class VehicleHescoCamoWoodWest: Supply {
	classname = "BWI_Construction_VehicleHescoCamoWoodWest";
	compatible[] = {">1000m"};
};

class VehicleHescoCamoWoodEast: Supply {
	classname = "BWI_Construction_VehicleHescoCamoWoodEast";
};

class MortarHescoWood: Supply {
	classname = "BWI_Construction_MortarHescoWood";
	compatible[] = {">1000m"};
};

class MortarHescoWireWood: Supply {
	classname = "BWI_Construction_MortarHescoWireWood";
	compatible[] = {">1000m"};
};

class TowerHescoWood: Supply {
	classname = "BWI_Construction_TowerHescoWood";
	compatible[] = {">1000m"};
};

class BunkerHescoWood: Supply {
	classname = "BWI_Construction_BunkerHescoWood";
	compatible[] = {">1000m"};
};

class CheckpointHescoWoodWest: Supply {
	classname = "BWI_Construction_CheckpointHescoWoodWest";
	compatible[] = {">1000m"};
};

class CheckpointHescoWoodEast: Supply {
	classname = "BWI_Construction_CheckpointHescoWoodEast";
	compatible[] = {">1000m"};
};

class CheckpointHescoHeavyWoodWest: Supply {
	classname = "BWI_Construction_CheckpointHescoHeavyWoodWest";
	compatible[] = {">1000m"};
};

class CheckpointHescoHeavyWoodEast: Supply {
	classname = "BWI_Construction_CheckpointHescoHeavyWoodEast";
	compatible[] = {">1000m"};
};