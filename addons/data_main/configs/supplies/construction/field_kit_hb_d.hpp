class SquadHescoDesertWest: Supply {
	classname = "BWI_Construction_SquadHescoDesertWest";
	compatible[] = {">1000m"};
};

class SquadHescoDesertEast: Supply {
	classname = "BWI_Construction_SquadHescoDesertEast";
	compatible[] = {">1000m"};
};

class VehicleHescoDesert: Supply {
	classname = "BWI_Construction_VehicleHescoDesert";
	compatible[] = {">1000m"};
};

class VehicleHescoCamoDesertWest: Supply {
	classname = "BWI_Construction_VehicleHescoCamoDesertWest";
	compatible[] = {">1000m"};
};

class VehicleHescoCamoDesertEast: Supply {
	classname = "BWI_Construction_VehicleHescoCamoDesertEast";
};

class MortarHescoDesert: Supply {
	classname = "BWI_Construction_MortarHescoDesert";
	compatible[] = {">1000m"};
};

class MortarHescoWireDesert: Supply {
	classname = "BWI_Construction_MortarHescoWireDesert";
	compatible[] = {">1000m"};
};

class TowerHescoDesert: Supply {
	classname = "BWI_Construction_TowerHescoDesert";
	compatible[] = {">1000m"};
};

class BunkerHescoDesert: Supply {
	classname = "BWI_Construction_BunkerHescoDesert";
	compatible[] = {">1000m"};
};

class CheckpointHescoDesertWest: Supply {
	classname = "BWI_Construction_CheckpointHescoDesertWest";
	compatible[] = {">1000m"};
};

class CheckpointHescoDesertEast: Supply {
	classname = "BWI_Construction_CheckpointHescoDesertEast";
	compatible[] = {">1000m"};
};

class CheckpointHescoHeavyDesertWest: Supply {
	classname = "BWI_Construction_CheckpointHescoHeavyDesertWest";
	compatible[] = {">1000m"};
};

class CheckpointHescoHeavyDesertEast: Supply {
	classname = "BWI_Construction_CheckpointHescoHeavyDesertEast";
	compatible[] = {">1000m"};
};