class Gren_Hand_HE_M67: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_M67";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_ANM8: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_ANM8";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_M18: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_M18";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_HE_DM51A1: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_DM51A1";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_DM25: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_DM25";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_DM32: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_DM32";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_L50A1: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_L50A1";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_L68: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_L68";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_HE_M84: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_M84";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_M83: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_M83";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_M83: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_M83";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_HE_RGD5: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_RGD5";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_WS_RDG2: Supply {
	classname = "BWI_Resupply_Gren_Hand_WS_RDG2";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_NSPD: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_NSPD";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_CS_NSPN: Supply {
	classname = "BWI_Resupply_Gren_Hand_CS_NSPN";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};

class Gren_Hand_Alhambra: Supply {
	classname = "BWI_Resupply_Gren_Hand_HE_Alhambra";
	compatible[] = {"Throwing"};
	textureIndexes[] = {2,0};
};