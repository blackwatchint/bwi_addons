class Medical_Bandages: Supply {
	classname = "BWI_Resupply_Medical_Bandages";
	textureIndexes[] = {3,0};
};

class Medical_Medication: Supply {
	classname = "BWI_Resupply_Medical_Medication";
	textureIndexes[] = {3,0};
};

class Medical_Bodybags: Supply {
	classname = "BWI_Resupply_Medical_Bodybags";
	textureIndexes[] = {3,0};
};

class Medical_Blood: Supply {
	classname = "BWI_Resupply_Medical_Blood";
	textureIndexes[] = {3,0};
};