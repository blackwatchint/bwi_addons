// Shared base classes.
class AccessoryGroup
{
    class Accessory
    {

    };
};

// Create extra groups for different parameters.
class ChestpackGroup: AccessoryGroup
{
    // Override default filter.
    compatibleAgainst = "uniform";
    parentClassTree = "CfgVehicles";
};

class HeadmountGroup: AccessoryGroup
{
    // Override default filter.
    compatibleAgainst = "headgear";
    parentClassTree = "CfgWeapons";
};

// Include the full configurations.
#include <configs\accessories\groups.hpp>