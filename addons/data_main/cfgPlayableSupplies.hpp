// Shared base classes.
class Category {
    class Supply;
};


// Include order determines order in resupply menu.
#include <configs\supplies\ammunition\categories.hpp>
#include <configs\supplies\grenade\categories.hpp>
#include <configs\supplies\launcher\categories.hpp>
#include <configs\supplies\explosive\categories.hpp>
#include <configs\supplies\medical\categories.hpp>
#include <configs\supplies\deployable\categories.hpp>
#include <configs\supplies\other\categories.hpp>
#include <configs\supplies\construction\categories.hpp>