if (!hasInterface) exitWith {};

["ace_firedPlayer", {
	params["_unit", "_weapon","_muzzle","_mode","_ammo","_magazine","_projectile"];

	// dont need to handle launcher shots
	if ((_muzzle != (primaryWeapon _unit)) && (_muzzle != (handgunWeapon _unit))) exitWith {};

	// using aces method to detect silencer here again
	private _silencer = switch (_weapon) do {
	    case (primaryWeapon _unit) : {(primaryWeaponItems _unit) select 0};
	    case (handgunWeapon _unit) : {(handgunItems _unit) select 0};
	    default {""};
	};

	// no silencer => exit
	if (_silencer == "") exitWith {};

	// using same syntax as ace here
	private _temperature = _unit getVariable [format ["ace_overheating_%1_temp", _weapon], 0];
	private _scaledTemperature = linearConversion [0, 1000, _temperature, 0, 1, true];
	
	// detect which weapon was shot
	private _isPrimaryWeapon = switch (_weapon) do {
	    case (primaryWeapon _unit) : {true};
	    case (handgunWeapon _unit) : {false};
	    default {false};
	};

	// Warn player (real life scenario would be seeing the silencer start glowing)
	if (_scaledTemperature > 0.4 && _scaledTemperature <= 0.5) then
	{
		hint "Silencer reaching dangerous temperatures";
	};
	
	// remove silencer if heat bigger than x
	if (_scaledTemperature > 0.5) then
	{
		hint "Silencer has melted";
		if (_isPrimaryWeapon) then
		{
			_unit removePrimaryWeaponItem _silencer;
		}
		else
		{
			_unit removeHandgunItem _silencer;
		};
	};
}] call CBA_fnc_addEventHandler;