class CfgPatches
{
	class bwi_overheating
	{
		requiredVersion = 1.0;
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
		authors[] = {"0mega"};
		version = 1.0;
		versionStr = "1.0.0";
		versionAr[] = {1,0,0};
		requiredAddons[] = {
			"ace_overheating"
		};
		units[] = {};
	};
};
class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

