#include <defines.hpp>

class CfgPatches {
    class bwi_fixes_main {
        requiredVersion = 1;
        authors[] = {"Redbery", "Fourjays", "0mega", "Dhorkiy", "Andrew", "Selier"};
        authorURL = "http://blackwatch-int.com";
        author = "Black Watch International";
        version = 2.8;
		versionStr = "2.8.0";
		versionAr[] = {2,8,0};
        requiredAddons[] = {
            "A3_Functions_F",
            "A3_Weapons_F",
            "cba_disposable",
            "ace_interaction",
            "bwa3_common",
            "bwa3_PzF3",
            "BWA3_Comp_ACE",
            "bwa3_dingo2",
            "rhs_main",
			"rhs_accessory",
			"rhs_c_pts",
			"rhsusf_main",
			"rhsusf_c_ch53",
			"rhssaf_main",
            "rhsgref_main",
            "rhsgref_c_vehicles_ret", // Required to load before RHS macros!
			"rhsgref_c_a29",
			"rhsgref_a29",
            "rhsusf_c_RG33L",
            "rhsusf_c_heavyweapons",
            "rhsusf_c_m1117",
            "uk3cb_baf_vehicles_coyote_jackal",
            "uk3cb_baf_vehicles_landrover",
            "uk3cb_factions_Vehicles_v3s",
            "uk3cb_factions_Vehicles_ural",
            "uk3cb_baf_vehicles_MAN",
            "fallujah_v1_0",
            "USAF_A10_C",
            "USAF_C17_C",
            "USAF_F22_C",
            "USAF_C130J_C",
            "USAF_F35A_C"
        };
        units[] = {};
    };
};

/**
 * No code should be included in the config.cpp, or any of the files linked directly (e.g. CfgVehicles.hpp).
 * All custom fixes should be located in a configs\mod\file.hpp.
 * The main fix should be documented with a comment, including description, date and issue numbers.
 * Additional code snippets should be documented with relevant issue number for tracking.
 *
 *  When in doubt look at the other fixes as examples.
 *  Date format is: YYYY-MM-DD
 */

// Vehicle display panels
#include <cfgMisc.hpp>

class DefaultEventhandlers;

class CfgVehicles {
    #include <cfgVehicles.hpp>
};

class CfgWeapons {
    #include <cfgWeapons.hpp>
};

class CfgMagazines {
    #include <cfgMagazines.hpp>
};

class CfgFunctions {
    #include <cfgFunctions.hpp>
};

class CfgWorlds {
    #include <cfgWorlds.hpp>
};

class CfgCloudlets {
    #include <cfgCloudlets.hpp>
};

class CfgGlasses {
	#include <cfgGlasses.hpp>
};


// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>

// CBA Disposable Launchers
#include <cfgDisposableLaunchers.hpp>