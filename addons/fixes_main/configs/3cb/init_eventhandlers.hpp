/**
 * Disables an init script on 3CB Vodniks that
 * randomizes vehicle camo, interfering with
 * BWI mods that are intended to set the vehicle camo.
 *
 * Date Added: 2019-11-15
 * BWI Issue:  #255, #512
 * Mod Issue:  N/A
 */
delete UK3CB_GAZ_Vodnik;
delete UK3CB_GAZ_Vodnik_Base;
delete UK3CB_GAZ_Vodnik_GMG;
delete UK3CB_GAZ_Vodnik_HMG;