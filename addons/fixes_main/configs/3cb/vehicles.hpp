/**
 * Sets refueling and rearming to use ACE instead of vanilla
 * on 3CB refuel and rearm vehicles.
 *
 * Date Added: 2019-04-07
 * BWI Issue:  #250
 * Mod Issue:  N/A
 */
class UK3CB_V3S_Closed;
class UK3CB_Ural_Base;
class UK3CB_V3S_Base;
class UK3CB_M939_Base;
class UK3CB_MTVR_Base;
class RHS_Ural_Fuel_MSV_01;
class rhsusf_m113_usarmy_supply;
class UK3CB_BAF_Coyote_Passenger_L111A1_D;
class UK3CB_BAF_Coyote_Passenger_L134A1_D;

class UK3CB_V3S_Reammo: UK3CB_V3S_Closed {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};
class UK3CB_Ural_Ammo_Base: UK3CB_Ural_Base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
    ace_refuel_fuelCapacity = 0;
};
class UK3CB_M939_Reammo: UK3CB_M939_Base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};
class UK3CB_MTVR_Reammo: UK3CB_MTVR_Base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};
class UK3CB_M113tank_supply: rhsusf_m113_usarmy_supply {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};

class UK3CB_V3S_Refuel: UK3CB_V3S_Base {
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class UK3CB_Ural_Fuel_Base: RHS_Ural_Fuel_MSV_01 {
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class UK3CB_M939_Refuel: UK3CB_M939_Base {
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class UK3CB_MTVR_Refuel: UK3CB_MTVR_Base {
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class UK3CB_BAF_Coyote_Logistics_L111A1_D : UK3CB_BAF_Coyote_Passenger_L111A1_D { // Coyote L HMG
	transportAmmo = 0;
	transportFuel = 0;
};
class UK3CB_BAF_Coyote_Logistics_L134A1_D : UK3CB_BAF_Coyote_Passenger_L134A1_D { // Coyote L GMG
	transportAmmo = 0;
	transportFuel = 0;
};


/**
 * Disables carry/drag on the machine gun nests.
 *
 * Date Added: 2020-02-29
 * BWI Issue:  #365
 * Mod Issue:  N/A
 */
class UK3CB_PKM_Nest: StaticMGWeapon {
    ace_dragging_canCarry = 0;
    ace_dragging_canDrag = 0;
};
class UK3CB_M240_Nest: StaticMGWeapon {
    ace_dragging_canCarry = 0;
    ace_dragging_canDrag = 0;
};


/**
 * Disables rearming on Jackals and Coyote 'P' variants.
 *
 * Date Added: 2020-07-03
 * BWI Issue:  #461
 * Mod Issue:  N/A
 */
class UK3CB_BAF_Coyote_L111A1_Base: UK3CB_BAF_Jackal_Base { // Coyote HMG baseclass
    ace_rearm_defaultSupply = 0;
};
class UK3CB_BAF_Coyote_L134A1_Base: UK3CB_BAF_Jackal_Base { // Coyote GMG baseclass
    ace_rearm_defaultSupply = 0;
};
class UK3CB_BAF_Jackal2_L111A1_Base: UK3CB_BAF_Coyote_L111A1_Base { // Jackal HMG baseclass
    ace_rearm_defaultSupply = 0;
};
class UK3CB_BAF_Jackal2_L134A1_Base: UK3CB_BAF_Coyote_L134A1_Base { // Jackal GMG baseclass
    ace_rearm_defaultSupply = 0;
};