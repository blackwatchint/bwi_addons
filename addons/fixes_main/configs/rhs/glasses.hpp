class G_Balaclava_blk;


/**
 * Hides specific facewear from player selection.
 *
 * Date Added: 2020-09-08
 * BWI Issue:  #481
 * Mod Issue:  N/A
 */
class rhssaf_veil_Green: G_Balaclava_blk
{
	scope = 1;
};
