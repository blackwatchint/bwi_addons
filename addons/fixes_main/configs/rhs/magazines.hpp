class RHS_48Rnd_40mm_MK19;


/**
 * Overrides the 40mm Mk19 M1001 Canister Magazines
 * to fire regular 40mm Mk19 M384 HE Rounds
 *
 * Date Added: 2021-01-27
 * BWI Issue:  #543
 * Mod Issue:  N/A
 */
class RHS_48Rnd_40mm_MK19_M1001: RHS_48Rnd_40mm_MK19
{
	ammo = "rhs_ammo_mk19m3_M384";
};
class RHS_96Rnd_40mm_MK19_M1001: RHS_48Rnd_40mm_MK19_M1001
{
	ammo = "rhs_ammo_mk19m3_M384";
};