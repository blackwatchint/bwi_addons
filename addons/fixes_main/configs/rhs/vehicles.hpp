class rhsusf_M1239_CROWS_base;
class rhsusf_M1239_CROWSMK19_base;
class rhsusf_m1152_base;
class rhssaf_m1152_olive;
class StaticCannon;
class rhsgref_ins_uniform_izlom;

/**
 * RPK-74M does not load magazines
 * Affected factions: CDF and ChDZK 
 * Temporary fix: Remove accessibility to automatic rifleman from affected factions
 * 
 * Date Added: 2022-04-01
 * BWI Issue:  #629
 * Mod Issue:  N/A
 */
class rhsgref_ins_arifleman_rpk: rhsgref_ins_uniform_izlom
{
	scope = 0;
};


/**
 * Fixes optics not being repairable by attaching
 * them to the related gun.
 *
 * Date Added: 2020-11-29
 * BWI Issue:  #482
 * Mod Issue:  N/A
 */
class RHS_M2A2_Base: APC_Tracked_03_base_F
{
	ace_repair_hitpointGroups[] = { {"HitGun", {"Hit_Optics_Gnr"}}, {"HitGunCom", {"Hit_Optics_Cdr_Peri"}} };
};

/**
 * Add GPS panel to the Super Tucano
 *
 * Date Added: 2020-07-12
 * BWI Issue:  #380
 * Mod Issue:  N/A
 */
class RHSGREF_A29_Base: Plane_Fighter_03_base_F {
	class Components: Components
	{
		class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft
		{
			class Components
			{
				class MinimapDisplay
				{
					componentType = "MinimapDisplayComponent";
					resource = "RscCustomInfoMiniMap";
				};
			};
		};
		class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight
		{
			class Components
			{
				class MinimapDisplay
				{
					componentType = "MinimapDisplayComponent";
					resource = "RscCustomInfoMiniMap";
				};
			};
		};
	}
};


/**
 * Increases clutch strength on some trucks to prevent the 
 * rapid gear swapping when climbing hills.
 *
 * Date Added: 2020-03-12
 * BWI Issue:  #392
 * Mod Issue:  N/A
 */
class rhsusf_fmtv_base: Truck_01_base_F {
	clutchStrength = 60;
};
class rhsusf_HEMTT_A4_base: Truck_01_base_F {
	clutchStrength = 60;
};


/**
 * Disables the artillery computer on specific artillery.
 *
 * Date Added: 2020-02-27
 * BWI Issue:  #354
 * Mod Issue:  N/A
 */
class RHS_M119_base : StaticCannon
{
	artilleryScanner = 0; 					// Disable artillery computer
	ace_artillerytables_showRangetable = 1; // Show the artillery rangetable for this vehicle
	ace_artillerytables_showGunLaying = 1;  // Show the AZIMUTH / ELEVATION numbers on the HUD for this vehicle
};
class rhs_d30_base : StaticCannon
{
	artilleryScanner = 0;
	ace_artillerytables_showRangetable = 1;
	ace_artillerytables_showGunLaying = 1;
};


/**
 * Sets refuling and rearming to use ACE instead of vanilla
 * on RHS refuel and rearm vehicles. Should be removed when
 * ACE compatibility is updated to cover these vehicles.
 *
 * Date Added: 2019-11-06
 * BWI Issue:  #325, #507
 * Mod Issue:  N/A
 */
class rhsusf_M1239_Deploy_base: rhsusf_M1239_CROWS_base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class rhsusf_M1239_DeployMK19_base: rhsusf_M1239_CROWSMK19_base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class rhsusf_m1152_rsv_usarmy_d: rhsusf_m1152_base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class rhsusf_m1152_rsv_usmc_d: rhsusf_m1152_base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class rhssaf_m1152_rsv_olive: rhssaf_m1152_olive {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};


/**
 * Hides the DUKE antenna actions on RHS vehicles as
 * they do nothing but spam messages when used.
 *
 * Date Added: 2017-07-30
 * BWI Issue:  #139, #530
 * Mod Issue:  N/A
 */
class rhsusf_RG33L_base: MRAP_01_base_F
{
	class EventHandlers: EventHandlers
	{
		class MurderTheDuke
		{
			init="_this select 0 animate ['DUKE_Hide',1,true]; _this select 0 removeWeaponTurret ['rhsusf_weap_duke',[-1]];";
		};
	};
	class Attributes
	{	
		class rhs_hideDUKE
		{
			defaultValue="1";
			expression="_this animate ['DUKE_Hide',_value,true];if(_value isEqualTo 1)then{_this removeWeaponTurret ['rhsusf_weap_duke',[0,0]]};";
			control = "CheckboxNumber";
			displayName = "hide DUKE antennas";
			property = "rhs_hideDUKE";
		};
	};
};

class rhsusf_M1117_base: Wheeled_APC_F
{
	class EventHandlers: EventHandlers
	{
		class MurderTheDuke
		{
			init="_this select 0 animate ['DUKE_Hide',1,true]; _this select 0 removeWeaponTurret ['rhsusf_weap_duke',[0,0]];";
		};
	};
	class Attributes
	{	
		class rhs_hideDUKE
		{
			defaultValue="1";
			expression="_this animate ['DUKE_Hide',_value,true];if(_value isEqualTo 1)then{_this removeWeaponTurret ['rhsusf_weap_duke',[0,0]]};";
			control = "CheckboxNumber";
			displayName = "hide DUKE antennas";
			property = "rhs_hideDUKE";
		};
	};
};

class rhsusf_stryker_base: Wheeled_APC_F
{
	class EventHandlers: EventHandlers
	{
		class MurderTheDuke
		{
			init="_this select 0 animate ['Hide_DUKE',1,true]; _this select 0 removeWeaponTurret ['rhsusf_weap_duke',[0,0]];";
		};
	};
	class Attributes
	{	
		class rhs_hideDUKE
		{
			defaultValue="1";
			expression="_this animate ['Hide_DUKE',_value,true];if(_value isEqualTo 1)then{_this removeWeaponTurret ['rhsusf_weap_duke',[0,0]]};";
			control = "CheckboxNumber";
			displayName = "hide DUKE antennas";
			property = "rhs_hideDUKE";
		};
	};
};

class rhsusf_caiman_base: Truck_01_base_F
{
	class EventHandlers: EventHandlers
	{
		class MurderTheDuke
		{
			init="_this select 0 animate ['DUKE_Hide',1,true]; _this select 0 removeWeaponTurret ['rhsusf_weap_duke',[0,0]];";
		};
	};
	class Attributes
	{	
		class rhs_hideDUKE
		{
			defaultValue="1";
			expression="_this animate ['DUKE_Hide',_value,true];if(_value isEqualTo 1)then{_this removeWeaponTurret ['rhsusf_weap_duke',[0,0]]};";
			control = "CheckboxNumber";
			displayName = "hide DUKE antennas";
			property = "rhs_hideDUKE";
		};
	};
};


/**
 * Modifies the UserActions code for opening/closing the ramp.
 * This disables the RHS attachment system that predates Vehicle-in-Vehicle.
 *
 * Date Added: 2020-08-12
 * BWI Issue:  #473
 * Mod Issue:  N/A
 */
class rhs_pts_base: APC_Tracked_02_base_F {
	class UserActions {
		class OpenRamp {
			statement = "this animateDoor ['ramp',1];this engineOn false;";
		};
		class CloseRamp: OpenRamp {
			statement = "this animateDoor ['ramp',0];";
		};
	};
};
class RHS_CH_47F_base: Heli_Transport_02_base_F {
	class UserActions {
		class OpenCargoDoor {
			statement = "this animateSource ['ramp_anim', 1];";
		};
		class CloseCargoDoor: OpenCargoDoor {
			statement = "this animateSource ['ramp_anim', 0];";
		};
		delete VehicleParadrop;
	};
};
class rhsusf_CH53E_USMC: Helicopter_Base_H {
	class UserActions {
		class RampOpen {
			statement = "this animate ['ramp_bottom',1];this animate ['ramp_top',1];";
		};
		class RampClose: RampOpen {
			statement = "this animate ['ramp_bottom',0];this animate ['ramp_top',0];";
		};
		delete VehicleParadrop;
	};
};
class RHS_C130J_Base: Plane_Base_F {
	class UserActions {
		class OpenRamp {
			statement = "this animateSource ['ramp',1];";
		};
		class CloseRamp: OpenRamp {
			statement = "this animateSource ['ramp',0];";
		};
		delete VehicleParadrop;
	};
};