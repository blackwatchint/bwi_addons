class Plane_Base_F;
class Plane_CAS_01_base_F;
class Plane_CAS_02_base_F;
class Plane_Fighter_01_Base_F;
class Plane_Fighter_02_Base_F;
class Plane_Fighter_03_base_F;
class Plane_Fighter_04_Base_F;
class UAV_02_dynamicLoadout_base_F;
class UAV_05_Base_F;


/**
 * Disables loadout configuration menu on FIR & JS planes by
 * deleting the UserAction class assosciated with it.
 * Prevents pilots from making unintended changes to loadouts.
 *
 * Date Added: 2019-09-06
 * BWI Issue:  #267
 * Mod Issue:  N/A
 */
// AMS is the loadout config menu for most FIR aircraft.
class FIR_A10A_Base: Plane_CAS_01_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_A10C_Base: Plane_CAS_01_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
// Classname is AV8B_Gui_Open for AV-8B instead of AMSOpen
class FIR_AV8B_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AV8B_Gui_Open;
	};
};
// Classname is AV8B_Gui_Open for AV-8B instead of AMSOpen
class FIR_AV8B_NA_Base: Plane_Fighter_03_base_F {
		class UserActions {
		delete AV8B_Gui_Open;
	};
};
// AV-8B GR7 does not inherit from AV-8B base. AV-8B GR9 inherits from AV-8B GR7.
class FIR_AV8B_GR7_Base: Plane_Fighter_03_base_F {
		class UserActions {
		delete AV8B_Gui_Open;
	};
};
class FIR_F14D_Base: Plane_Fighter_03_base_F {
		class UserActions {
		delete AMSOpen;
	};
};
// Special case, F-14A declares AMS separate from base class.
class FIR_F14A_Base: FIR_F14D_Base {
	class UserActions {
		delete AMSOpen;
	};
};
// Special case, F-14B declares AMS separate from base class.
class FIR_F14B_Base: FIR_F14D_Base {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_F15_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_F15D_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_F15E_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_F16_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_F16D_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_F35B_Armaverse_Base: Plane_Fighter_03_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
class FIR_Su25sm3_base: Plane_CAS_02_base_F {
	class UserActions {
		delete AMSOpen;
	};
};
// Classname is service_menu for F/A-18E Super Hornet instead of AMSOpen
class JS_JC_FA18E: Plane_Base_F {
	class UserActions {
		delete service_menu;
	};
};
// Classname is service_menu for F/A-18F Super Hornet instead of AMSOpen
class JS_JC_FA18F: Plane_Base_F {
	class UserActions {
		delete service_menu;
	};
};
// Classname is service_menu for Su-35 Flanker E instead of AMSOpen
class JS_JC_SU35: Plane_Base_F {
	class UserActions {
		delete service_menu;
	};
};


/**
 * Disables loadout configuration menu added by FIR to vanilla aircraft
 * by deleting the UserAction class assosciated with it.
 * Prevents pilots from making unintended changes to loadouts.
 *
 * Date Added: 2020-01-24
 * BWI Issue:  #344
 * Mod Issue:  N/A
 */
// F/A-181 Black Wasp II - Stealth variant inherits from this.
class B_Plane_Fighter_01_F : Plane_Fighter_01_Base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// To-201 Shikra - Stealth variant does not inherit this, hence the extra deletion.
class O_Plane_Fighter_02_F : Plane_Fighter_02_Base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};
class O_Plane_Fighter_02_Stealth_F : Plane_Fighter_02_Base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// L-159 ALCA
class Plane_Fighter_03_dynamicLoadout_base_F : Plane_Fighter_03_base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// JAS 39 Gripen
class I_Plane_Fighter_04_F : Plane_Fighter_04_Base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// A-10D Thunderbolt II
class Plane_CAS_01_dynamicLoadout_base_F : Plane_CAS_01_base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// Yak-130
class Plane_CAS_02_dynamicLoadout_base_F : Plane_CAS_02_base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// YABHON-R3
class B_UAV_02_dynamicLoadout_F : UAV_02_dynamicLoadout_base_F {
	class UserActions {
		delete AMS_LiteOpen;
	};
};

// UCAV Sentinel
class B_UAV_05_F : UAV_05_Base_F {
	class UserActions {
		delete AMSOpen; // Randomly named differently :')
	};
};