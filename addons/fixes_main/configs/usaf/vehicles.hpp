/**
 * Adds a wait to the init that sometimes
 * generate script errors due to race conditions.
 *
 * Date Added: 2021-09-29
 * BWI Issue:  #584
 * Mod Issue:  N/A
 */
class USAF_A10: Plane
{
	class Eventhandlers: DefaultEventhandlers
	{
		init="[{!isNull (_this select 0)}, {_this execVM ""\usaf_a10_c\scr\usaf_a10_init.sqf"";}, _this] call CBA_fnc_waitUntilAndExecute";
	};
};

class USAF_C17: Plane_Base_F
{
	class EventHandlers: EventHandlers
	{
		init="[{!isNull (_this select 0)}, {_this execVM ""\USAF_C17_C\scripts\init.sqf""; [(_this select 0)] spawn USAF_Cargo_fnc_addActions;}, _this] call CBA_fnc_waitUntilAndExecute";
	};
};

class USAF_F22_Heavy: Plane
{
	class Eventhandlers: DefaultEventhandlers
	{
		init="[{!isNull (_this select 0)}, {[_this select 0] execVM ""USAF_F22\scripts\init.sqf"";}, _this] call CBA_fnc_waitUntilAndExecute";
	};
};

class USAF_C130J_Base: Plane_Base_F
{
	class EventHandlers: DefaultEventhandlers
	{
		class USAF_C130
		{
			init="[{!isNull (_this select 0)}, {null = _this execVM '\USAF_C130J_C\scripts\init.sqf'; [(_this select 0)] spawn USAF_C130_fnc_OH10_check; [(_this select 0)] spawn USAF_Cargo_fnc_addActions;}, _this] call CBA_fnc_waitUntilAndExecute";
		};
	};
};

class USAF_F35A: Plane_Base_F
{
	class EventHandlers: DefaultEventhandlers
	{
		init="[{!isNull (_this select 0)}, {_this execVM '\USAF_F35A_C\scripts\init.sqf';}, _this] call CBA_fnc_waitUntilAndExecute"
	};
};