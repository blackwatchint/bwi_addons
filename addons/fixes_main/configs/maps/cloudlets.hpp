/**
 * Fixes the missing housedestructionsmoke definition needed 
 * for building destruction on Fallujah. The error also impacts
 * other maps when running with Fallujah.
 *
 * Date Added: 2019-04-11
 * BWI Issue:  #253
 * Mod Issue:  N/A
 */
class Default;
class HouseDestructionSmoke1;
class HouseDestructionSmoke : HouseDestructionSmoke1
{
	// Prevents "No entry 'bin\config.bin/CfgCloudlets.housedestructionsmoke'."
	// Intentionally empty, just cloning.
};