/**
 * Fixes the grid definition for Fallujah so that it 
 * uses 6-grid instead of 8-grid coordinates at the 
 * maximum zoom. Additionally fixes the coordinate 
 * start points to be bottom left instead of middle
 * left.
 *
 * Date Added: 2019-04-02
 * BWI Issue:  #253
 * Mod Issue:  N/A
 */
class fallujah: Utes {
	class Grid {
		offsetX = 0;
		offsetY = 10240;

		class Zoom1 {
			format = "XY";
			formatX = "000";
			formatY = "000";
			stepX = 100;
			stepY = -100;
			zoomMax = 0.05;
		};

		class Zoom2 {
			format = "XY";
			formatX = "00";
			formatY = "00";
			stepX = 1000;
			stepY = -1000;
			zoomMax = 0.5;
		};

		class Zoom3 {
			format = "XY";
			formatX = "0";
			formatY = "0";
			stepX = 10000;
			stepY = -10000;
			zoomMax = 1;
		};
	};
};


/**
 * Makes it snow instead of rain on Panthera (Winter)
 * Changes the particle effects and sound effects.
 *
 * Date Added: 2020-08-18
 * BWI Issue:  #321
 * Mod Issue:  N/A
 */
class winthera3 : Stratis 
{
	class RainParticles 
	{
		dropColor[] = {0.1,0.1,0.1,1};
		dropHeight = 0.05;
		dropSpeed = 0.8;
		dropWidth = 0.05;
		effectRadius = 13;
		lumSunBack = 0.1;
		lumSunFront = 0.1;
		maxRainDensity = 9;
		minRainDensity = 0.01;
		rainDropTexture = "hellanmaaw\hellanmaaw\data\hellanmaaw_snow_ca.paa";
		refractCoef = 0.5;
		refractSaturation = 0.3;
		rndDir = 0.58;
		rndSpeed = 0.5;
		texDropCount = 1;
		windCoef = 0.95;
	};
	class EnvSounds // Override JSRS EnvSounds inherited from Stratis
	{
		class Rain 
		{
			name = "Rain";
			sound[] = {"A3\sounds_f\ambient\winds\wind-synth-slow",0.0199526,1};
			soundNight[] = {"A3\sounds_f\ambient\winds\wind-synth-slow",0.03,1};
			volume = "(windy factor[0,1])";
		};
		class WindForestHigh
		{
			name = "Wind";
			random[] = {};
			sound[] = {"A3\sounds_f\ambient\winds\wind-synth-slow",0.0199526,1};
			volume = "forest*(windy factor[0,1])*(0.1+(hills factor[0,1])*0.9)-(night*0.25)";
		};
		class WindNoForestHigh
		{
			name = "Wind";
			random[] = {};
			sound[] = {"A3\sounds_f\ambient\winds\wind-synth-slow",0.0199526,1};
			volume = "(1-forest)*(windy factor[0,1])*(0.1+(hills factor[0,1])*0.9)-(night*0.25)";
		};
	};
};