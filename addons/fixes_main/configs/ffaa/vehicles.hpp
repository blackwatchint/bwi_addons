class ffaa_et_m250_base_blin;
class ffaa_et_pegaso_base;

/**
 * Sets refuling and rearming to use ACE instead of vanilla
 * on FFAA refuel and rearm vehicles. Should be removed when
 * ACE compatibility is added to cover these vehicles.
 *
 * Date Added: 2021-08-04
 * BWI Issue:  #586
 * Mod Issue:  N/A
 */
class ffaa_et_m250_combustible_blin: ffaa_et_m250_base_blin { //M250 (Fuel)
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class ffaa_et_m250_repara_municion_blin: ffaa_et_m250_base_blin { //M250 (Workshop and Ammo)
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
	ace_repair_canRepair = 1;
};
class ffaa_et_pegaso_combustible: ffaa_et_pegaso_base { //Pegaso 7226 (Fuel)
    transportFuel = 0;
    ace_refuel_fuelCargo = 3000;
};
class ffaa_et_pegaso_repara_municion: ffaa_et_pegaso_base { //Pegaso 7226 (Workshop and Ammo)
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
	ace_repair_canRepair = 1;
};