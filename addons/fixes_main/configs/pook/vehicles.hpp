/**
 * Omits randomization scripts that sometimes
 * generate script errors due to race conditions.
 *
 * Date Added: 2021-08-25
 * BWI Issue:  #582
 * Mod Issue:  N/A
 */
class pook_AN12B_Base: Plane_Base_F
{
	class EventHandlers : EventHandlers
	{
		init = "_this execVM '\pook_SovAF\scripts\BB.sqf'";
	};
};

class pook_AN24_Base: Plane_Base_F
{
	class EventHandlers : EventHandlers
	{
		init = "_this execVM '\pook_SovAF\scripts\BB.sqf'";
	};
};

class pook_IL18_Base: pook_AN12B_Base
{
	class EventHandlers : EventHandlers
	{
		init = "_this execVM '\pook_SovAF\scripts\BB.sqf'";
	};
};