/**
 * Adds a custom rearm function which fires on the
 * ACE event being triggered which detects if
 * (like BAF) the magazine should fill the inventory
 * before filling the turret and does this.
 *
 * Date Added: 2017-04-11
 * BWI Issue:  #107, #243, #457
 * Mod Issue:  N/A
 */
class baf_functions
{
	file = "\bwi_fixes_main\functions\baf";

	class initPostRearm{};
	class postRearm{};
};