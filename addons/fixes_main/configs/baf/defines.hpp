// #361 - BAF TFAR Interaction
/**
 * Macro which deletes the TFAR Intercom Interaction from a 
 * vehicle class.
 */ 
#define MACRO_DELETE_TFARINTERACTION \
        class ACE_SelfActions: ACE_SelfActions { \
            delete TFAR_IntercomChannel; \
        };