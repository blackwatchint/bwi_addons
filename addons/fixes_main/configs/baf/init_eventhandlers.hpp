/**
 * Disables an init script on BAF Land Rovers that
 * adds Javelins to the inventory, interfering with
 * BWI mods that are intended to clear the inventory.
 *
 * Date Added: 2017-04-11
 * BWI Issue:  #105
 * Mod Issue:  N/A
 */
delete UK3CB_BAF_LandRover_Base;