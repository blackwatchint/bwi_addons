/**
 * Disables the TFAR Intercom Interaction available
 * on all BAF vehicles despite TFAR not being present.
 * Previously handled in bwi_radio, moved here to
 * prevent inheritance issues.
 *
 * Date Added: 2020-02-28
 * BWI Issue:  #361
 * Mod Issue:  N/A
 */
// Remove TFAR interaction.
class UK3CB_BAF_LandRover_Base: Car_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_Jackal_Base: Car_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_FV432_Mk3_Base: APC_Tracked_01_base_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_Panther_Base: MRAP_01_base_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_Husky_Base: MRAP_01_base_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_RHIB_Base: Boat_Armed_01_base_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_Warrior_A3_Base: APC_Tracked_03_base_F
{
	MACRO_DELETE_TFARINTERACTION
};
class UK3CB_BAF_MAN_HX60_Base: Truck_01_base_F
{
	MACRO_DELETE_TFARINTERACTION
};

// #250 - 3CB Faction rearming/refueling.
class UK3CB_BAF_MAN_HX58_Base;
class UK3CB_BAF_MAN_HX58_Fuel_Base: UK3CB_BAF_MAN_HX58_Base {
    transportFuel = 0;
};
class UK3CB_BAF_MAN_HX60_Fuel_Base: UK3CB_BAF_MAN_HX60_Base {
    transportFuel = 0;
};
class UK3CB_BAF_MAN_HX60_Repair_Base: UK3CB_BAF_MAN_HX60_Base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};
class UK3CB_BAF_MAN_HX58_Repair_Base: UK3CB_BAF_MAN_HX58_Base {
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};

class UK3CB_BAF_Husky_HMG_Base;
class UK3CB_BAF_Husky_GPMG_Base;
class UK3CB_BAF_Husky_GMG_Base;
class UK3CB_BAF_Husky_Logistics_HMG_Sand: UK3CB_BAF_Husky_HMG_Base {
	transportFuel = 0;
	transportAmmo = 0;
};
class UK3CB_BAF_Husky_Logistics_GPMG_Sand: UK3CB_BAF_Husky_GPMG_Base {
	transportFuel = 0;
	transportAmmo = 0;
};
class UK3CB_BAF_Husky_Logistics_GMG_Sand: UK3CB_BAF_Husky_GMG_Base {
	transportFuel = 0;
	transportAmmo = 0;
};


/**
 * Disables the "Sling Helmet" and "Don Helmet" interactions
 * as they do not function correctly with respawn.
 *
 * Date Added: 2019-02-28
 * BWI Issue:  #362
 * Mod Issue:  N/A
 */
class CAManBase: Man {
	class ACE_SelfActions {
		class ACE_Equipment {
			delete UK3CB_Attach_Helmet;
			delete UK3CB_Detach_Helmet;
		};
	};
};