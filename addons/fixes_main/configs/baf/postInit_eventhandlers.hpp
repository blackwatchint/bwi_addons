/**
 * Adds a custom rearm function which fires on the
 * ACE event being triggered which detects if
 * (like BAF) the magazine should fill the inventory
 * before filling the turret and does this.
 *
 * Date Added: 2020-06-15
 * BWI Issue:  #243, #457
 * Mod Issue:  N/A
 */
class bwi_fixes_main_baf
{
	init = "call bwi_fixes_main_fnc_initPostRearm;";
};