// #253 - Fallujah Grid Fix
class Utes;

// #282 - Chernarus Winter Map Y-Axis Unflip
class CAWorld;

/**
 * Fixes the coordinate start points to the BOTTOM LEFT
 * instead of the TOP LEFT on Chernarus (Winter).
 *
 * Date Added: 2019-08-27
 * BWI Issue: #282
 * Mod Issue: N/A
 */
 class Chernarus_Winter: CAWorld {
	class Grid {
		offsetY = 15360;
		
		class Zoom1 {
			stepY = -100;
		};
		
		class Zoom2 {
			stepY = -1000;
		};
		
		class Zoom3 {
			stepY = -10000;
		};
	};
 };

// #282 - Alibad Winter Map Y-Axis Unflip
class Takistan;