// #139 - RHS DUKE
// #361 - BAF TFAR Interaction
class LandVehicle;
class AllVehicles;

class Car: LandVehicle 
{
	class ACE_SelfActions;
};
class Car_F: Car
{
    class EventHandlers;
	class ACE_Actions;
};
class MRAP_01_base_F: Car_F{};
class Tank: LandVehicle
{
	class ACE_SelfActions;
};
class Tank_F: Tank{};
class APC_Tracked_01_base_F: Tank_F{};

class APC_Tracked_03_base_F: Tank_F{};

class Ship: AllVehicles
{
	class ACE_SelfActions;
};
class Ship_F: Ship{};
class Boat_F: Ship_F{};
class Boat_Armed_01_base_F: Boat_F{};

class Truck_F: Car_F{};
class Truck_01_base_F: Truck_F{};
class Wheeled_APC_F: Car_F
{
    class EventHandlers;
};


// #380 Can't use GPS in A-29 Super Tucano
class Plane;
class Plane_Base_F: Plane
{
	class EventHandlers;
	class textureSources;
	class Components;
};
class Plane_Fighter_03_base_F: Plane_Base_F{};


// #362 - BAF Helmet Interactions
class Man;


// #365 - Disable MG Nest Carry/Drag
class StaticMGWeapon;


// #473 - Disable ramps on vehicles with ViV support
class APC_Tracked_02_base_F;
class Heli_Transport_02_base_F;
class Helicopter_Base_H;