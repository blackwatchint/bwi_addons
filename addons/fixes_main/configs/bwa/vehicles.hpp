/**
 * Fixes a hard crash when accessing 
 * ACE option to bend or unbend antennas
 * by removing the option from the menu.
 *
 * Date Added: 2022-01-24
 * BWI Issue:  #619
 * Mod Issue:  BWA
 */
class BWA3_Dingo2_base: Car_F {
    class ACE_Actions: ACE_Actions {
        delete BWA3_UnbendAntennas;
        delete BWA3_BendAntennas;
    }
}