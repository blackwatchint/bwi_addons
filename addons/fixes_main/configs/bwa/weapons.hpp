/**
 * Makes the Panzerfaust 3 reloadable, as it should be.
 * Because while Arma doesn't support "disposable tube
 * reusable trigger" launchers, it can be simulated by
 * making the weapon reloadable.
 *
 * Date Added: 2017-11-27
 * BWI Issue:  #169, #240, #330
 * Mod Issue:  N/A
 */
class BWA3_Pzf3: Launcher_Base_F {
    // Make visible everywhere.
    scope = 2;
    scopeArsenal = 2;

    // Remove proxy to reloadable tube.
    delete baseWeapon; 

    // Add reloadable animations and sounds.
    reloadAction = "ReloadRPG";
    reloadMagazineSound[] = {"A3\sounds_f\weapons\reloads\missile_reload",0.316228,1,20};
    reloadSound[] = {"",1,1};
    reloadTime = 0;

    // Add compatible magazines.
    magazines[] = {"BWA3_PzF3_Tandem","BWA3_PzF3_DM32"};
    
    // Rebalance weight.
    class WeaponSlotsInfo : WeaponSlotsInfo {
        mass = 200;
    };

    // Remove CBA disposable fired event to prevent semi-automatic fire.
    class EventHandlers {
        delete fired;
    };
};

// Hide the reloadable variants.
class BWA3_PzF3_Tandem_Loaded: BWA3_PzF3 {
    scope = 1;
    scopeArsenal = 1;
};
class BWA3_PzF3_Used: BWA3_PzF3 {
    scope = 1;
    scopeArsenal = 1;
};
class BWA3_Bunkerfaust: BWA3_PzF3 {
    scope = 1;
    scopeArsenal = 1;  
};
class BWA3_Bunkerfaust_Loaded: BWA3_Bunkerfaust {
    scope = 1;
    scopeArsenal = 1;
};