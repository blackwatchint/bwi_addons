params ["_vehicle", "_unit", "_turretPath", "_numMagazines", "_magazineClass", "_numRounds", "_pylon"]; 

private _magazineMax = 0; 
{ 
	private _magazineClassName = gettext (_x >> "magazine");  
	if ( _magazineClassName == _magazineClass) then 
	{ 
		_magazineMax  = getnumber (_x >> "count");  
	};    
} foreach ("true" configClasses (configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "TransportMagazines")); 
 
private _magazineCurrent = 0; 
if ( (((getMagazineCargo _vehicle) select 0) find _magazineClass) != -1) then 
{ 
	_magazineCurrent = ((getMagazineCargo _vehicle) select 1) select (((getMagazineCargo _vehicle) select 0) find _magazineClass); 
}; 

if ( _magazineMax > _magazineCurrent ) then
{ 
	_vehicle addMagazineCargoGlobal [_magazineClass, 1]; 
	_vehicle removeMagazinesTurret [_magazineClass, _turretPath]; // Remove magazine so we can keep grabbing ammo
}; 
if ( _magazineMax == _magazineCurrent && _magazineMax != 0 ) then // If this was the last magazine readd it to the turret to force reload
{ 
	_vehicle removeMagazinesTurret [_magazineClass, _turretPath]; 
	_vehicle addMagazineTurret [_magazineClass, _turretPath]; 
};
