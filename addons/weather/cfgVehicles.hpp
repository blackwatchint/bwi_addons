class Logic;
class Module_F: Logic
{
	class ArgumentsBaseUnits
	{
		class Units;
	};
	class ModuleDescription;
};

// TODO - Update modules to new standard!

 class bwi_ModuleStartSnowstorm: Module_F
 {
	 // Standard object definitions
	 scope = 2;
	 displayName = "Snowstorm";
	 icon = "\bwi_weather\data\icon_weather_w.paa";
	 category = "BWI_MODULE_WEATHER";
	 function = "bwi_fnc_snowstormModuleInit";
	 functionPriority = 1;
	 isGlobal = 1;
	 isTriggerActivated = 0;
	 isDisposable = 0;
	 is3DEN = 0;
 
	 // Module arguments
	 class Arguments: ArgumentsBaseUnits
	 {
		 //class Units: Units {};
		 class Intensity {
			 displayName = "Intensity";
			 description = "Intensity of the snowstorm";
			 typeName = "NUMBER"; // NUMBER/STRING/BOOL
			 class Values {
				   class LIGHT	{ name = "Light"; value = 0; default = 1;};  
				   class NORMAL	{ name = "Normal"; value = 1;};  
				   class STRONG { name = "Strong"; value = 2;};
			};
		 };
	 };
 
	 // Module description
	 class ModuleDescription: ModuleDescription
	 {
		 description = "Starts a snowstorm with the desired power.";
		 sync[] = {};
	 };
 };

 
 class bwi_ModuleStartSandstorm: Module_F
 {
	 // Standard object definitions
	 scope = 2;
	 displayName = "Sandstorm";
	 icon = "\bwi_weather\data\icon_weather_w.paa";
	 category = "BWI_MODULE_WEATHER";
	 function = "bwi_fnc_sandstormModuleInit";
	 functionPriority = 1;
	 isGlobal = 1;
	 isTriggerActivated = 0;
	 isDisposable = 0;
	 is3DEN = 0;
 
	 // Module arguments
	 class Arguments: ArgumentsBaseUnits
	 {
		 //class Units: Units {};
		 class Intensity {
			 displayName = "Intensity";
			 description = "Intensity of the sandstorm";
			 typeName = "NUMBER"; // NUMBER/STRING/BOOL
			 class Values {
				   class LIGHT	{ name = "Light"; value = 0; default = 1;};  
				   class NORMAL	{ name = "Normal"; value = 1;};  
				   class STRONG { name = "Strong"; value = 2;};
			};
		 };
	 };
 
	 // Module description
	 class ModuleDescription: ModuleDescription
	 {
		 description = "Starts a sandstorm with the desired power.";
		 sync[] = {};
	 };
 };