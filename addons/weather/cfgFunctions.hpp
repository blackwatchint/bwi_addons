class bwi
{
	class functionsSnowstorm
	{
		file = "\bwi_weather\functions\snowstorm";
		class clientHandleSFX{};
		class clientHandleSnowfall{};
		class serverHandleWeather{};
		class snowstormModuleInit{};
		class clientInitSnowstorm{};
	};
	class functionsSandstorm
	{
		file = "\bwi_weather\functions\sandstorm";
		class sandstormModuleInit{};
		class serverHandleSandstormWeather{};
		class clientInitSandstorm{};
		class clientHandleSandstorm{};
	};
};