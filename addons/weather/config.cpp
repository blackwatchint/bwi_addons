class CfgPatches
{
	class bwi_weather
	{
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Modules_F"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
		authors[] = {"0mega"};
		version = 1.0;
		versionStr = "1.0.0";
		versionAr[] = {1,0,0};
		units[] = {
			"bwi_ModuleStartSnowstorm",
			"bwi_ModuleStartSandstorm"
		};
	};
};

class CfgVideoOptions
{
	class Particles
	{
		class High
		{
			particlesHardLimit = 20000;
			particlesSoftLimit = 19000;
		};
		class Low
		{
			particlesHardLimit = 20000;
			particlesSoftLimit = 19000;
		};
		class Normal
		{
			particlesHardLimit = 20000;
			particlesSoftLimit = 19000;
		};
	};
};
class CfgFactionClasses
{
	#include <cfgFactionClasses.hpp>
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgRemoteExec
{
	#include <cfgRemoteExec.hpp>
};

class CfgVehicles
{
	#include <cfgVehicles.hpp>
};
class CfgSounds
{
	#include <cfgSounds.hpp>
};