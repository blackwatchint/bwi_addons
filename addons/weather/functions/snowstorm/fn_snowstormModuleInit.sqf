private _logic = param [0,objNull,[objNull]];
bwi_weather_SnowstormIntensity = _logic getVariable ["Intensity",1];
if (hasInterface) then
{
	[] call bwi_fnc_clientInitSnowstorm;
	player addEventHandler ["Respawn", {[
	{
		bwi_weather_isRespawn = true;
		[] call bwi_fnc_clientInitSnowstorm;
	},player, 2 ] call CBA_fnc_waitAndExecute; }];
};

if (!isServer) exitWith {};

//Server turns on snowstorm at start and broadcasts (stays relevant for JiP)
bwi_weather_IsSnowstormOn = true;
publicVariable "bwi_weather_IsSnowstormOn";
[] call bwi_fnc_serverHandleWeather;