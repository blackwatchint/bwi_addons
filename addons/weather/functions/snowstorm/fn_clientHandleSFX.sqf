private _pos_rotocol = [player,5+random 10, random 180] call BIS_fnc_relPos;
bwi_weather_sfxEffectObject = "Land_HelipadEmpty_F" createVehicleLocal [(_pos_rotocol select 0),(_pos_rotocol select 1),0];
bwi_weather_sfxEffect = "#particlesource" createVehicleLocal getposatl bwi_weather_sfxEffectObject;

bwi_weather_sfxEffect setParticleCircle [0.5, [3, -3, 1]];
bwi_weather_sfxEffect setParticleRandom [0, [2, 2, 0], [-2, 2, 0.25], 0, 0.25, [0, 0, 0, 0.1], 0, 0];
bwi_weather_sfxEffect setParticleParams [["\A3\data_f\cl_basic" , 1, 0, 1], "", "Billboard", 1, 2, [0, 0, 0], [-3, 3, 0.25], 15, 10.5, 7.9, 0.075, [2, 2, 3], [[1, 1, 1, 0.2], [1, 1, 1, 0.2], [1, 1, 1, 0.2]], [0.08], 1, 0, "", "",bwi_weather_sfxEffectObject];
bwi_weather_sfxEffect setDropInterval 1;

bwi_weather_sfxEffectHandle = [{
	if (!bwi_weather_IsSnowstormOn) exitWith {};
	
	if (!alive player) exitWith {[bwi_weather_sfxEffectHandle] call CBA_fnc_removePerFrameHandler;};

	bwi_weather_sfxEffect attachto [bwi_weather_sfxEffectObject,[0,0,0]];
	private _startPosition = getPosASL player;
	private _endPosition = [_startPosition select 0, _startPosition select 1, (_startPosition select 2 ) + 10];
	private _intersects = lineIntersectsSurfaces [_startPosition, _endPosition, player, objNull, false, 1, "GEOM","NONE"];
	private _isHouse = false;
	private _houseClass = "";

	if (!(count _intersects == 0)) then 
	{
		_houseClass = typeOf ((_intersects select 0) select 3);
		_isHouse = ((_intersects select 0) select 3) isKindOf "house";
	};

	if (_isHouse) then
	{
		bwi_weather_sfxEffect setDropInterval -1;
	}
	else
	{
		bwi_weather_sfxEffect setDropInterval 1;
	};
},0.5,[]] call CBA_fnc_addPerFrameHandler;

[{
	private _pos_rotocol = [player,5+random 10, random 359] call BIS_fnc_relPos;
	bwi_weather_sfxEffectObject setpos [_pos_rotocol select 0,_pos_rotocol select 1,0];
},1,[]] call CBA_fnc_addPerFrameHandler;