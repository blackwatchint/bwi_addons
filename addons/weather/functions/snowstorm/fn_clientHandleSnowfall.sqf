bwi_weather_snowfallEffect = "#particlesource" createVehicleLocal (getPos player);
bwi_weather_snowfallCircle = [10, [0,0, 0]];
bwi_weather_snowfallParams = [["\A3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 12, 0], "", "Billboard", 1, 4, [0, 0, 12], [0,0,-1], 1, 1, 0.1, 0.1, [0.3], [[1, 1, 1, 1]], [0.08], 1, 0, "", "", vehicle player,0,false,0];
bwi_weather_snowfallRandom = [1, [10, 10, 2], [4,4,0], 0, 0.5, [0, 0, 0, 0.1], 1, 1];

bwi_weather_snowfallEffect setParticleRandom bwi_weather_snowfallRandom;
bwi_weather_snowfallEffect setParticleCircle bwi_weather_snowfallCircle;
bwi_weather_snowfallEffect setParticleParams bwi_weather_snowfallParams;
bwi_weather_snowfallEffect setDropInterval ([0.03, 0.007, 0.001] select bwi_weather_SnowstormIntensity);

bwi_weather_snowfallEffectHandle = [{
	if (!bwi_weather_IsSnowstormOn) exitWith {};
	
	if (!alive player) exitWith { [bwi_weather_snowfallEffectHandle] call CBA_fnc_removePerFrameHandler;};

	private _startPosition = getPosASL player;
	private _endPosition = [_startPosition select 0, _startPosition select 1, (_startPosition select 2 ) + 10];
	private _intersects = lineIntersectsSurfaces [_startPosition, _endPosition, player, objNull, false, 1, "GEOM","NONE"];
	private _isHouse = false;
	private _houseClass = "";

	if (!(count _intersects == 0)) then 
	{
		_houseClass = typeOf ((_intersects select 0) select 3);
		_isHouse = ((_intersects select 0) select 3) isKindOf "house";
	};

	if (_isHouse) then
	{
		private _house = ((_intersects select 0) select 3);
		bwi_weather_snowfallEffect attachto [_house,[0, 0, 12]];

		private _houseCircle = + bwi_weather_snowfallCircle;
		private _houseParams = + bwi_weather_snowfallParams;
		private _houseHeightParam = + bwi_weather_snowfallParams select 5;
		
		_houseCircle set [0, ((bwi_weather_snowfallCircle select 0) + (sizeOf _houseClass)/2)];
		_houseHeightParam set [2,(bwi_weather_snowfallParams select 5 select 2) + 10];
		_houseParams set [5, _houseHeightParam];
		bwi_weather_snowfallEffect setParticleCircle _houseCircle;
		bwi_weather_snowfallEffect setParticleParams _houseParams;
	}
	else
	{
		// resets changes to on foot settings
		bwi_weather_snowfallEffect setParticleCircle bwi_weather_snowfallCircle;
		bwi_weather_snowfallEffect setParticleParams bwi_weather_snowfallParams;
		bwi_weather_snowfallEffect setParticleRandom bwi_weather_snowfallRandom;
		bwi_weather_snowfallEffect attachto [vehicle player,[0, 0, 12]];

		// sets for vehicles
		if (vehicle player != player) then
		{

			private _isAirVic = (vehicle player) isKindOf "Air";

			// aircraft at low speeds gets lower snow height (should cause snow below vic, but still falling normally)
			if (_isAirVic) then
			{
				private _airVehicleParams = + bwi_weather_snowfallParams;
				_airVehicleParams set [5, [0,0,7]];
				bwi_weather_snowfallEffect setParticleParams _airVehicleParams;
			};
			// sets for vics over certain velocity
			if (speed (vehicle player) > 35) then
			{
				private _vicParticleRandom = + bwi_weather_snowfallRandom;	
				_vicParticleRandom set [2 , [10, 10, 10]];
				bwi_weather_snowfallEffect setParticleRandom _vicParticleRandom;

				private _vicVehicleParams = + bwi_weather_snowfallParams;
				_vicVehicleParams set [5, [0,0,0]];
				bwi_weather_snowfallEffect setParticleParams _vicVehicleParams;
			};
		};
	};
},0.5,[]] call CBA_fnc_addPerFrameHandler;