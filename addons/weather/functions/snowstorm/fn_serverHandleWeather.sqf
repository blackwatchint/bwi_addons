//Set weather values
0 setfog [([0.1,0.3,0.4] select bwi_weather_SnowstormIntensity), 0, 0];
forceWeatherChange;
0 setOverCast ([0.2,0.4,0.69] select bwi_weather_SnowstormIntensity);
forceWeatherChange;
0 setRain 0;
forceWeatherChange;
setWind [0,0,true];
forceWeatherChange;

//Force weather to stay
999999 setfog [([0.1,0.3,0.4] select bwi_weather_SnowstormIntensity), 0, 0];
999999 setOverCast ([0.2,0.4,0.69] select bwi_weather_SnowstormIntensity);
999999 setRain 0;