if (!hasInterface) exitwith {};

//Wait until server has initialized and initialize client
[{!(isNil "bwi_weather_IsSnowstormOn")},{
	//No need to run if snowstorm is off
	if (!(bwi_weather_IsSnowstormOn)) exitwith {};
	//Did not run locally yet or is respawn
	if (isNil "bwi_weather_IsSnowstormOnLocal" || {!(isnil "bwi_weather_isRespawn") && bwi_weather_isRespawn}) then 
	{
		bwi_weather_IsSnowstormOnLocal = true;
		bwi_weather_isRespawn = false;
		//Local weather settings
		0 setOverCast ([0.2,0.4,0.69] select bwi_weather_SnowstormIntensity);
		forceWeatherChange;
		999999 setOverCast ([0.2,0.4,0.69] select bwi_weather_SnowstormIntensity);
		//Snow effects
		[] call bwi_fnc_clientHandleSnowfall;
		[] call bwi_fnc_clientHandleSFX;
	};
}] call CBA_fnc_waitUntilAndExecute;