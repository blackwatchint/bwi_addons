private _logic = param [0,objNull,[objNull]];
bwi_weather_SandstormIntensity = _logic getVariable ["Intensity",1];

if (isServer) then
{
	//Server turns on snowstorm at start and broadcasts (stays relevant for JiP)
	bwi_weather_IsSandstormOn = true;
	publicVariable "bwi_weather_IsSandstormOn";
	[] call bwi_fnc_serverHandleSandstormWeather;
};

if (hasInterface) then
{
	[] call bwi_fnc_clientInitSandstorm;
	player addEventHandler ["Respawn", {[
	{
		bwi_weather_isRespawn = true;
		[] call bwi_fnc_clientInitSandstorm;
	},player, 2 ] call CBA_fnc_waitAndExecute; }];
};