if (!hasInterface) exitwith {};

//Wait until server has initialized and initialize client
[{!(isNil "bwi_weather_IsSandstormOn")},{
	//No need to run if snowstorm is off
	if (!(bwi_weather_IsSandstormOn)) exitwith {};
	//Did not run locally yet or is respawn
	if (isNil "bwi_weather_IsSandstormOnLocal" || {!(isnil "bwi_weather_isRespawn") && bwi_weather_isRespawn}) then 
	{
		bwi_weather_IsSandstormOnLocal = true;
		bwi_weather_isRespawn = false;
		//Local weather settings
		0 setWindStr ([0.6,0.7,0.8] select bwi_weather_SandstormIntensity);
		forceWeatherChange;
		999999 setWindStr ([0.6,0.7,0.8] select bwi_weather_SandstormIntensity);
		0 setOverCast ([0.3,0.55,0.69] select bwi_weather_SandstormIntensity);
		forceWeatherChange;
		999999 setOverCast ([0.3,0.55,0.69] select bwi_weather_SandstormIntensity);
		//Snow effects
		[] call bwi_fnc_clientHandleSandstorm;
	};
}] call CBA_fnc_waitUntilAndExecute;