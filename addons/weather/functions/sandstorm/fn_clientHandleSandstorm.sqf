bwi_weather_colorCorrectionEffect = ppEffectCreate ["ColorCorrections", 1500];

bwi_weather_colorCorrectionEffect ppEffectAdjust  
[    
 1,     
 1,     
 0,     
 [1, 0.7, 0.0, 0.1],     
 [1, 0.7, 0, 0.5],     
 [0.299, 0.587, 0.114, 0],    
 [-1, -1, 0, 0, 0, 0, 0]    
];    
bwi_weather_colorCorrectionEffect ppEffectCommit 0;    
bwi_weather_colorCorrectionEffect ppEffectEnable true;
bwi_weather_isOrangeEffectActive = true;

bwi_weather_particleParamsOnFootLow 		= [["\A3\data_f\cl_basic", 1, 0, 1], "", "Billboard", 1, 20, [0, 0, 0], [0,0,0], 0, 10.15, 7.9, 0.01, [30, 20, 20], [[0.65, 0.5, 0.5, 0.1]], [0.08], 1, 0, "", "", bwi_weather_helipad];
bwi_weather_particleParamsOnFootHigh 		= [["\A3\data_f\cl_basic", 1, 0, 1], "", "Billboard", 1, 15, [0, 0, 0], [0,0,0], 0, 10.15, 7.9, 0.01, [25, 15, 15], [[0.65, 0.5, 0.5, 0.35]], [0.08], 1, 0, "", "", bwi_weather_helipad];
bwi_weather_particleParamsOnFootVeryHight	= [["\A3\data_f\cl_basic", 1, 0, 1], "", "Billboard", 1, 12, [0, 0, 0], [0,0,0], 0, 10.15, 7.9, 0.01, [20, 10, 10], [[0.65, 0.5, 0.5, 0.5]], [0.08], 1, 0, "", "", bwi_weather_helipad];

bwi_weather_particleRandomLow 		= [0, [100,0, 20], [1.5, 1.5, 2], 0.5, 0.1, [0, 0, 0, 0.05], 0, 0];
bwi_weather_particleRandomHigh		= [0, [100,0, 20], [2.5, 2.5, 3], 0.5, 0.1, [0, 0, 0, 0], 0, 0];
bwi_weather_particleRandomVeryHigh 	= [0, [100,0, 20], [3, 3, 3], 0.5, 0.1, [0, 0, 0, 0], 0, 0];

bwi_weather_sandstormFog = "#particlesource" createVehicleLocal (getpos player);  
//bwi_weather_sandstormFog setParticleCircle [7, [0, 0, 0]]; 
bwi_weather_sandstormFog setParticleRandom ([bwi_weather_particleRandomLow,bwi_weather_particleRandomHigh,bwi_weather_particleRandomVeryHigh] select bwi_weather_SandstormIntensity);
bwi_weather_sandstormFog setParticleParams ([bwi_weather_particleParamsOnFootLow,bwi_weather_particleParamsOnFootHigh,bwi_weather_particleParamsOnFootVeryHight] select bwi_weather_SandstormIntensity);
bwi_weather_sandstormFog setDropInterval ([0.08, 0.04, 0.01] select bwi_weather_SandstormIntensity);

0 setWindStr ([0.6,0.7,0.8] select bwi_weather_SandstormIntensity);

bwi_weather_sandstormEffectHandle =[{

	bwi_weather_wind = +wind;
	bwi_weather_wind set [0, (bwi_weather_wind select 0)*5]; 
	bwi_weather_wind set [1, (bwi_weather_wind select 1)*5]; 

	if ((windDir>315)||(windDir<45)) then 
	{
		bwi_weather_randset = [120,0,20];
	};
	if ((windDir>135)&&(windDir<225)) then 
	{
		bwi_weather_randset = [120,0,20];
	};
	if ((windDir>=45)&&(windDir<=135)) then 
	{
		bwi_weather_randset = [0,120,20];
	};
	if ((windDir>=225)&&(windDir<=315)) then 
	{
		bwi_weather_randset = [0,120,20];
	};

	bwi_weather_particleRandomLow 		set [1, bwi_weather_randset];
	bwi_weather_particleRandomHigh 		set [1, bwi_weather_randset];
	bwi_weather_particleRandomVeryHigh 	set [1, bwi_weather_randset];

	bwi_weather_particleParamsOnFootLow 		set [6, bwi_weather_wind];
	bwi_weather_particleParamsOnFootHigh		set [6, bwi_weather_wind];
	bwi_weather_particleParamsOnFootVeryHight	set [6, bwi_weather_wind];

	bwi_weather_sandstormFog setParticleRandom ([bwi_weather_particleRandomLow,bwi_weather_particleRandomHigh,bwi_weather_particleRandomVeryHigh] select bwi_weather_SandstormIntensity);
 	bwi_weather_sandstormFog setParticleParams ([bwi_weather_particleParamsOnFootLow,bwi_weather_particleParamsOnFootHigh,bwi_weather_particleParamsOnFootVeryHight] select bwi_weather_SandstormIntensity);

	private _newPos = player getpos [100,windDir-180];
	private _newrelpos = player worldToModel _newPos; 
	_newrelpos set [2, 0];
	
	private _startPosition = getPosASL player;
	private _endPosition = [_startPosition select 0, _startPosition select 1, (_startPosition select 2 ) + 10];

	private _intersects = lineIntersectsSurfaces [_startPosition, _endPosition, player, objNull, false, 1, "GEOM","NONE"];
	private _isHouse = false;
	private _houseClass = "";
	
	if (!(count _intersects == 0)) then 
	{
		_houseClass = typeOf ((_intersects select 0) select 3);
		_isHouse = ((_intersects select 0) select 3) isKindOf "house";
	};
	if (_isHouse && {bwi_weather_isOrangeEffectActive}) then
	{
		// Less orange
		bwi_weather_colorCorrectionEffect ppEffectAdjust 
		[
			1, 
			1, 
			0, 
			[0.7, 0.7, 0.0, 0.1],     
			[0.7, 0.7, 0, 0.5],  
			[0.299, 0.587, 0.114, 0],
			[-1, -1, 0, 0, 0, 0, 0]
		];
		bwi_weather_colorCorrectionEffect ppEffectCommit 3;
		bwi_weather_isOrangeEffectActive = false;
	};
	if (!(_isHouse) && {!(bwi_weather_isOrangeEffectActive)}) then
	{
		//return to orange values
		bwi_weather_colorCorrectionEffect ppEffectAdjust  
		[    
		1,     
		1,     
		0,     
		[1, 0.7, 0.0, 0.1],     
		[1, 0.7, 0, 0.5],     
		[0.299, 0.587, 0.114, 0],    
		[-1, -1, 0, 0, 0, 0, 0]    
		];    
		bwi_weather_colorCorrectionEffect ppEffectCommit 3;
		bwi_weather_isOrangeEffectActive = true;
	};

	if (vehicle player != player) then
	{
		private _isAirVic = (vehicle player) isKindOf "Air";

		if (speed (vehicle player) > 15) then
		{		
			if (_isAirVic) then
			{
				_newPos = player getpos [30,windDir-180];
				_newrelpos = player worldToModel _newPos; 
				_newrelpos set [2, 4];
			};

			_particleParamsInVic = + ([bwi_weather_particleParamsOnFootLow,bwi_weather_particleParamsOnFootHigh,bwi_weather_particleParamsOnFootVeryHight] select bwi_weather_SandstormIntensity);
			_particleParamsInVic set [6,[0,(speed (vehicle player)) + 20,0]];
			bwi_weather_sandstormFog setParticleParams _particleParamsInVic;			
		};
	};
	if (vehicle player != player) then {bwi_weather_sandstormFog attachto [vehicle player,_newrelpos];} else {bwi_weather_sandstormFog attachto [player,_newrelpos];};

},0.5,[]] call CBA_fnc_addPerFrameHandler;

