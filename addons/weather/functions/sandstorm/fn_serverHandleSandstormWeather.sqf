//Set weather values
0 setfog [([0.15,0.35,0.6] select bwi_weather_SandstormIntensity), 0, 0];
forceWeatherChange;
0 setOverCast ([0.3,0.55,0.69] select bwi_weather_SandstormIntensity);
forceWeatherChange;
0 setRain 0;
forceWeatherChange;
0 setWindStr ([0.6,0.7,0.8] select bwi_weather_SandstormIntensity);
forceWeatherChange;

//Force weather to stay
999999 setfog [([0.15,0.35,0.6] select bwi_weather_SandstormIntensity), 0, 0];
999999 setOverCast ([0.3,0.55,0.69] select bwi_weather_SandstormIntensity);
999999 setWindStr ([0.6,0.7,0.8] select bwi_weather_SandstormIntensity);
999999 setRain 0;