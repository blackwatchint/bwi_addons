class UK3CB_V3S_Closed;
class UK3CB_Ural_Base;
class UK3CB_Hilux_Open;


/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
 */
class UK3CB_Motorbike_Base: Quadbike_01_base_F {
    ace_cargo_space = 0;
    maximumLoad = 50;
};
class UK3CB_BTR40: Car_F {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_Datsun_Base: Car_F {
    ace_cargo_space = 4;
    maximumLoad = 100;
};
class UK3CB_Datsun_Pickup_PKM: UK3CB_Datsun_Base {
    ace_cargo_space = 1;
};
class UK3CB_GAZ_Vodnik_Base: Car_F {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_GAZ_Vodnik_MedEvac: UK3CB_GAZ_Vodnik_Base {
    ace_cargo_space = 3;
    maximumLoad = 100;
};
class UK3CB_Sedan_Base: Car_F {
    ace_cargo_space = 2;
    maximumLoad = 100;
};
class UK3CB_SUV_Base: Car_F {
    ace_cargo_space = 2;
    maximumLoad = 100;
};
// Hilux
class UK3CB_Hilux_Base: Car_F {
    ace_cargo_space = 4;
    maximumLoad = 100;
};
class UK3CB_Hilux_DSHKM: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Hilux_PKM: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Hilux_SPG9: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Hilux_ZU23: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Hilux_GMG: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Hilux_Rocket: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Hilux_Rocket_Arty: UK3CB_Hilux_Open {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
// Land Rovers
class UK3CB_LandRover_Base: Car_F {
    ace_cargo_space = 4;
    maximumLoad = 100;
};
class UK3CB_LandRover: UK3CB_LandRover_Base {
    ace_cargo_space = 4;
    maximumLoad = 100;
};
class UK3CB_LandRover_SF_AGS30: UK3CB_LandRover_Base {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_LandRover_SF_M2: UK3CB_LandRover_Base {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_LandRover_SPG9: UK3CB_LandRover_Base {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_LandRover_AGS30: UK3CB_LandRover_Base {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_LandRover_M2: UK3CB_LandRover_Base {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_MaxxPro_Base: MRAP_01_base_F {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_Tractor_Old: Car_F {
    ace_cargo_space = 1; //Spare wheel
    maximumLoad = 50;
};
class UK3CB_Gaz24: Car_F {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_Golf: Car_F {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_S1203_BASE: Car_F {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
class UK3CB_Willys_Jeep_Base: Car_F {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
class UK3CB_M151_Jeep_Base: Car_F {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
// Humvees
class UK3CB_M998_2DR_Base: rhsusf_m998_w_2dr {
    ace_cargo_space = 6;
    maximumLoad = 50;
};
class UK3CB_M998_4DR_Base: rhsusf_m998_w_4dr {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
class UK3CB_M1025_Unarmed_Base: rhsusf_m1025_w {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
class UK3CB_M1025_M2_Base: rhsusf_m1025_w_m2 {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_M1025_MK19_Base: rhsusf_m1025_w_mk19 {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_M1025_TOW_Base: rhsusf_m966_w {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
// TIGRs
class UK3CB_TIGR: rhs_tigr_3camo_vdv {
    ace_cargo_space = 1;
    maximumLoad = 200;
};
class UK3CB_TIGR_FFV: rhs_tigr_ffv_3camo_vdv {
    ace_cargo_space = 1;
    maximumLoad = 200;
};
class UK3CB_TIGR_STS: rhs_tigr_sts_3camo_vdv {
    ace_cargo_space = 1;
    maximumLoad = 200;
};
// UAZs
class UK3CB_UAZ_Closed_Base: RHS_UAZ_Base {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
class UK3CB_UAZ_Open_Base: rhs_uaz_open_Base {
    ace_cargo_space = 4;
    maximumLoad = 50;
};
class UK3CB_UAZ_DShKM_Base: RHS_UAZ_DShKM_Base {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_UAZ_AGS30_Base: RHS_UAZ_AGS30_Base {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_UAZ_SPG9_Base: RHS_UAZ_SPG9_Base {
    ace_cargo_space = 1;
    maximumLoad = 50;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
 * Recovery = 16 / 50
 */
class UK3CB_Ikarus_Base: Truck_02_base_F {
    ace_cargo_space = 6;
    maximumLoad = 50;
};
class UK3CB_Ural_Recovery_Base: UK3CB_Ural_Base {
    ace_cargo_space = 16;
	bwi_secondary_cargo_space = 3;
    maximumLoad = 50;
};
class UK3CB_Kamaz_Covered_Base: Truck_02_base_F {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_Kamaz_Open_Base: Truck_02_transport_base_F {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_Kamaz_Repair_Base: Truck_02_box_base_F {
    ace_cargo_space = 6;
    maximumLoad = 50;
};
class UK3CB_MAZ_543_SCUD: Truck_F {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
// V3S Praga Truck
class UK3CB_V3S_Base: Truck_02_base_F {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_V3S_Recovery: UK3CB_V3S_Base {
    ace_cargo_space = 16;
	bwi_secondary_cargo_space = 3;
    maximumLoad = 50;
};
class UK3CB_V3S_Reammo: UK3CB_V3S_Closed {
    ace_cargo_space = 3;
    maximumLoad = 50;
};
class UK3CB_V3S_Refuel: UK3CB_V3S_Base {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_V3S_Repair: UK3CB_V3S_Base {
    ace_cargo_space = 6;
    maximumLoad = 50;
};
class UK3CB_V3S_Zu23: UK3CB_V3S_Base {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_Ural_Ammo_Base: UK3CB_Ural_Base {
    ace_cargo_space = 3;
    maximumLoad = 50;
};
class UK3CB_Ural_Empty_Base: UK3CB_Ural_Base {
    ace_cargo_space = 1; //Spare wheel
    maximumLoad = 50;
};
class UK3CB_Ural_Repair_Base: UK3CB_Ural_Base {
    ace_cargo_space = 6;
    maximumLoad = 50;
};
// M939 5 Ton Truck
class UK3CB_M939_Base: Truck_02_base_F {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_M939_Closed: UK3CB_M939_Base {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_M939_Open: UK3CB_M939_Base {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_M939_Guntruck: UK3CB_M939_Base {
    ace_cargo_space = 8;
    maximumLoad = 50;
};
class UK3CB_M939_Recovery: UK3CB_M939_Base {
    ace_cargo_space = 16;
	bwi_secondary_cargo_space = 3;
    maximumLoad = 50;
};
class UK3CB_M939_Reammo: UK3CB_M939_Base {
    ace_cargo_space = 3;
    maximumLoad = 50;
};
class UK3CB_M939_Refuel: UK3CB_M939_Base {
    ace_cargo_space = 1;
    maximumLoad = 50;
};
class UK3CB_M939_Repair: UK3CB_M939_Base {
    ace_cargo_space = 6;
    maximumLoad = 50;
};
// MTVR (US Marines)
class UK3CB_MTVR_Base: Truck_02_base_F {
    ace_cargo_space = 16;
	bwi_secondary_cargo_space = 4;
    maximumLoad = 100;
};
class UK3CB_MTVR_Closed: UK3CB_MTVR_Base {
    ace_cargo_space = 8;
    maximumLoad = 100;
};
class UK3CB_MTVR_Open: UK3CB_MTVR_Base {
    ace_cargo_space = 8;
    maximumLoad = 100;
};
class UK3CB_MTVR_Recovery: UK3CB_MTVR_Base {
    ace_cargo_space = 4;
    maximumLoad = 100;
};
class UK3CB_MTVR_Reammo: UK3CB_MTVR_Base {
    ace_cargo_space = 3;
    maximumLoad = 100;
};
class UK3CB_MTVR_Refuel: UK3CB_MTVR_Base {
    ace_cargo_space = 1;
    maximumLoad = 100;
};
class UK3CB_MTVR_Repair: UK3CB_MTVR_Base {
    ace_cargo_space = 6;
    maximumLoad = 100;
};
// FMTV
class UK3CB_M977A4_Recovery_Base: rhsusf_M977A4_usarmy_wd {
    ace_cargo_space = 16;
	bwi_secondary_cargo_space = 6;
    maximumLoad = 100;
};


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
 */
// M113s
class UK3CB_M113tank_unarmed: rhsusf_m113_usarmy_unarmed {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_M2: rhsusf_m113tank_base {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_M2_90: rhsusf_m113_usarmy_M2_90 {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_M240: rhsusf_m113_usarmy_M240 {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_medical: rhsusf_m113_usarmy_medical {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_supply: rhsusf_m113_usarmy_supply {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_MK19: rhsusf_m113_usarmy_MK19 {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M113tank_MK19_90: rhsusf_m113_usarmy_MK19_90 {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_M1A1_Base: rhsusf_m1a1tank_base {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_M109_Base: rhsusf_m109tank_base {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
// BRDM-2s
class UK3CB_BRDM2_HQ: rhsgref_BRDM2_HQ {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_BRDM2_UM: rhsgref_BRDM2UM {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_BRDM2: rhsgref_BRDM2 {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
class UK3CB_BRDM2_ATGM: rhsgref_BRDM2_ATGM {
    ace_cargo_space = 2;
    maximumLoad = 50;
};
// BTRs
class UK3CB_BTR60_Base: rhs_btr60_base {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BTR70_Base: rhs_btr70_vmf {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BTR80_Base: rhs_btr80_msv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BTR80a_Base: rhs_btr80a_msv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
// BMDs
class UK3CB_BMD1: rhs_bmd1 {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMD1K: rhs_bmd1k {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMD1P: rhs_bmd1p {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMD1PK: rhs_bmd1pk {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMD1R: rhs_bmd1r {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMD2: rhs_bmd2 {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
// BMPs
class UK3CB_BRM1KTank_Base: rhs_brm1k_base {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMP1Tank_Base: rhs_bmp1tank_base {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMP2Tank_Base: rhs_bmp2_vdv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_BMP2KTank_Base: rhs_bmp2_vdv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
// ZSU-23-4
class UK3CB_ZsuTank_Base: rhs_zsutank_base {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
// T-Series
class UK3CB_T34: MBT_02_base_F {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T55: MBT_02_base_F {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T72A: MBT_02_base_F {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T72BA: rhs_t72ba_tv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T72BB: rhs_t72bb_tv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T72BC: rhs_t72bc_tv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T72BD: rhs_t72bd_tv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
class UK3CB_T72BE: rhs_t72be_tv {
    ace_cargo_space = 4;
    maximumLoad = 150;
};


/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */
class UK3CB_AH9_Base: Heli_Light_01_armed_base_F {
    ace_cargo_space = 0;
    maximumLoad = 25;
};
class UK3CB_UH1H_M240_Base: Helicopter_Base_H {
    ace_cargo_space = 3;
    maximumLoad = 25;
};
class UK3CB_AH1Z: RHS_AH1Z {
    ace_cargo_space = 0;
    maximumLoad = 25;
};
class UK3CB_AH64: RHS_AH64D_noradar {
    ace_cargo_space = 0;
    maximumLoad = 25;
};


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
 */
class UK3CB_Antonov_An2_Base: Plane_Base_F {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 6;
	maximumLoad = 75;
};
class UK3CB_Antonov_An2_Armed_Base: UK3CB_Antonov_An2_Base {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 6;
	maximumLoad = 75;
};


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
 */
class UK3CB_RHIB: Boat_Armed_01_base_F {
    ace_cargo_space = 4;
    maximumLoad = 150;
};
