class rhsusf_m113_usarmy_supply;
class rhsusf_m966_w;
class rhsusf_m1025_w_m2;
class rhsusf_m1025_w_mk19;
class rhsusf_m1151_base;
class rhssaf_m1152_olive;
class rhs_tigr_3camo_vdv;
class rhs_tigr_ffv_3camo_vdv;
class rhs_tigr_sts_3camo_vdv;
class rhs_uaz_open_Base;
class rhs_UAZ_AGS30_Base;
class rhs_UAZ_SPG9_Base;
class rhsusf_m113_usarmy_unarmed;
class rhsusf_m113_usarmy_M2_90;
class rhsusf_m113_usarmy_M240;
class rhsusf_m113_usarmy_medical;
class rhsusf_m113_usarmy_MK19;
class rhsusf_m113_usarmy_MK19_90;
class rhsusf_m1a1tank_base;
class rhsusf_m109tank_base;
class rhsgref_BRDM2_HQ;
class rhsgref_BRDM2UM;
class rhsgref_BRDM2_ATGM;
class rhs_btr60_base;
class rhs_btr70_vmf;
class rhs_btr80_msv;
class rhs_btr80a_msv;
class rhs_bmd1;
class rhs_bmd1k;
class rhs_bmd1p;
class rhs_bmd1pk;
class rhs_bmd1r;
class rhs_bmd2;
class rhs_brm1k_base;
class rhs_bmp1tank_base;
class rhs_bmp2_vdv;
class rhs_zsutank_base;
class rhs_t72ba_tv;
class rhs_t72bb_tv;
class rhs_t72bc_tv;
class rhs_t72bd_tv;
class rhs_t72be_tv;
class RHS_AH1Z;
class RHS_AH64D_noradar;
class RHS_UAZ_Base;
class rhsusf_hmmwe_base;
class rhsusf_caiman_base;
class rhsusf_caiman_GPK_base;
class rhsusf_RG33L_GPK_base;
class rhsusf_m1025_w;
class rhsusf_cougar_base;
class rhsusf_cougar_GPK_base;
class OTR21_Base;
class rhs_gaz66_vmf;
class rhs_gaz66o_vmf;
class rhs_gaz66_repair_base;
class rhs_gaz66_ap2_base;
class rhs_gaz66_zu23_base;
class RHS_Ural_BaseTurret;
class RHS_Ural_MSV_Base;
class RHS_Ural_Open_MSV_01;
class rhs_kraz255b1_base;
class rhsusf_M1078A1P2_fmtv_usarmy;
class rhsusf_M1078A1P2_B_fmtv_usarmy;
class rhsusf_M1078A1P2_B_M2_fmtv_usarmy;
class rhsusf_M1083A1P2_fmtv_usarmy;
class rhsusf_M1083A1P2_B_M2_fmtv_usarmy;
class rhsusf_M1083A1P2_B_fmtv_usarmy;
class rhsusf_M977A4_usarmy_wd;
class rhsusf_M977A4_BKIT_usarmy_wd;
class rhsusf_M977A4_BKIT_M2_usarmy_wd;
class rhs_zil131_open_base;
class rhs_mi28_base;
class rhsusf_M1239_CROWS_base;
class rhsusf_M1239_CROWSMK19_base;


/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
 */
class rhs_tigr_base: MRAP_02_base_F {
	maximumLoad = 200; // Extra space for racks.
	ace_cargo_space = 1; //Spare wheel.
};
class RHS_UAZ_DShKM_Base: RHS_UAZ_Base {
	ace_cargo_space = 1; //Spare wheel.
    maximumload = 50;
};
class rhsusf_rg33_base: MRAP_01_base_F {
	ace_cargo_space = 1; // Spare wheel.
    maximumload = 50;
};
class rhsusf_m998_w_2dr: rhsusf_hmmwe_base {
	ace_cargo_space = 6;
    maximumload = 50;
};
class rhsusf_m998_w_4dr: rhsusf_m998_w_2dr {
	ace_cargo_space = 4;
    maximumload = 50;
};
class rhsusf_m1152_base: rhsusf_m1151_base {
	ace_cargo_space = 6;
    maximumload = 50;
};
class rhssaf_m1152_rsv_olive: rhssaf_m1152_olive {
	ace_cargo_space = 1;
    maximumload = 50;
};
class rhsusf_m1152_rsv_usarmy_d: rhsusf_m1152_base {
	ace_cargo_space = 1;
    maximumload = 50;
};
class rhsusf_m1152_rsv_usmc_d: rhsusf_m1152_base {
	ace_cargo_space = 1;
    maximumload = 50;
};
class rhsusf_mrzr_base: MRAP_01_base_F {
	ace_cargo_space = 4;
    maximumload = 50;
};
class rhsusf_M1220_usarmy_d: rhsusf_caiman_base {
    ace_cargo_space = 1; // Spare wheel.
    maximumload = 100;
};
class rhsusf_M1220_M2_usarmy_d: rhsusf_caiman_GPK_base {
    ace_cargo_space = 1; // Spare wheel.
    maximumload = 100;
};
class rhsusf_M1230_M2_usarmy_d: rhsusf_caiman_GPK_base {
    ace_cargo_space = 1; // Spare wheel.
    maximumload = 100;
};
class rhsusf_M1230a1_usarmy_d: rhsusf_M1220_usarmy_d {
    ace_cargo_space = 2; // Spare wheel.
    maximumload = 100;
};
class rhsusf_M1232_usarmy_d: rhsusf_RG33L_GPK_base {
    ace_cargo_space = 2;
    maximumload = 100;
};
class rhsusf_M1232_M2_usarmy_d: rhsusf_RG33L_GPK_base {
    ace_cargo_space = 2;
    maximumload = 100;
};
class rhsusf_M1237_M2_usarmy_d: rhsusf_RG33L_GPK_base {
    ace_cargo_space = 2;
	maximumload = 100;
};
class rhsusf_RG33L_base: MRAP_01_base_F {
	ace_cargo_space = 4;
	maximumload = 100;
};
class rhsusf_m996_w: rhsusf_m1025_w {
	ace_cargo_space = 1; // Spare wheel.
	maximumload = 100;
};
class rhsusf_CGRCAT1A2_M2_usmc_d: rhsusf_cougar_GPK_base
{
	ace_cargo_space = 1; // Spare wheel.
	maximumload = 100;
};
class rhsusf_CGRCAT1A2_usmc_d: rhsusf_cougar_base
{
	ace_cargo_space = 1; // Spare wheel.
	maximumload = 100;
};
class rhsusf_M1165A1_GMV_SAG2_base: rhsusf_m1151_base
{
	ace_cargo_space = 2; 
	maximumload = 100;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
 * Recovery = 16 / 50
 */
class rhs_9k79: OTR21_Base {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 1;	
	maximumLoad = 50;
};
class rhs_gaz66_flat_vmf: rhs_gaz66_vmf {
	ace_cargo_space = 12;
	bwi_secondary_cargo_space = 3;
	maximumLoad = 50;
};
class rhs_gaz66o_flat_vmf: rhs_gaz66o_vmf {
	ace_cargo_space = 12;
	bwi_secondary_cargo_space = 3;
	maximumLoad = 50;
};
class rhs_gaz66_r142_base: rhs_gaz66_vmf { 
	ace_cargo_space = 4; //Cabin on rear
	maximumLoad = 50;
};
class rhs_gaz66_repair_vmf: rhs_gaz66_repair_base {
	ace_cargo_space = 6; //Cabin on rear
	maximumLoad = 50;
};
class rhs_gaz66_ap2_vmf: rhs_gaz66_ap2_base {
	ace_cargo_space = 4; //Cabin on rear
	maximumLoad = 50;
};
class rhs_gaz66_zu23_vmf: rhs_gaz66_zu23_base {
	ace_cargo_space = 1; //Spare wheel.	
	maximumLoad = 50;
};
class rhs_gaz66_ammo_base: rhs_gaz66_vmf {
	ace_cargo_space = 3; //Spare wheel.	
	maximumLoad = 50;
};
class RHS_BM21_MSV_01: RHS_Ural_BaseTurret {
	ace_cargo_space = 1; //Spare wheel.	
	maximumLoad = 50;
};
class RHS_Ural_Zu23_Base: RHS_Ural_BaseTurret {
	ace_cargo_space = 1; //Spare wheel.	
	maximumLoad = 50;
};
class RHS_Ural_Base: RHS_Ural_BaseTurret {
	maximumLoad = 50;
};
class RHS_Ural_Flat_MSV_01: RHS_Ural_MSV_Base {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 4;
};
class RHS_Ural_Open_Flat_MSV_01: RHS_Ural_Open_MSV_01 {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 4;
};
class RHS_Ural_Support_MSV_Base_01: RHS_Ural_MSV_Base {
	ace_cargo_space = 1; //Spare wheel.	
};
class RHS_Ural_Repair_MSV_01: RHS_Ural_Support_MSV_Base_01 {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class rhs_kraz255b1_cargo_base: rhs_kraz255b1_base {
	ace_cargo_space = 8;
};
class rhs_kraz255b1_flatbed_base: rhs_kraz255b1_base {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 4;
};
// M1078A1P2
class rhsusf_M1078A1P2_B_flatbed_fmtv_usarmy: rhsusf_M1078A1P2_B_fmtv_usarmy {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 3;
};
class rhsusf_M1078A1P2_flatbed_fmtv_usarmy: rhsusf_M1078A1P2_fmtv_usarmy {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 3;
};
class rhsusf_M1078A1P2_B_M2_flatbed_fmtv_usarmy: rhsusf_M1078A1P2_B_M2_fmtv_usarmy {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 3;
	maximumLoad = 50;
};
class rhsusf_M1078A1P2_B_CP_fmtv_usarmy: rhsusf_M1078A1P2_B_fmtv_usarmy {
	ace_cargo_space = 4;
	maximumLoad = 50;
};
// M1083A1P2
class rhsusf_M1083A1P2_flatbed_fmtv_usarmy: rhsusf_M1083A1P2_fmtv_usarmy {
	ace_cargo_space = 18;
	bwi_secondary_cargo_space = 6;
	maximumLoad = 50;
};
class rhsusf_M1083A1P2_B_flatbed_fmtv_usarmy: rhsusf_M1083A1P2_B_fmtv_usarmy {
	ace_cargo_space = 18;
	bwi_secondary_cargo_space = 6;
	maximumLoad = 50;
};
class rhsusf_M1083A1P2_B_M2_flatbed_fmtv_usarmy: rhsusf_M1083A1P2_B_M2_fmtv_usarmy {
	ace_cargo_space = 18;
	bwi_secondary_cargo_space = 6;
	maximumLoad = 50;
};
// M1084A1P2
class rhsusf_M1084A1P2_fmtv_usarmy: rhsusf_M1083A1P2_fmtv_usarmy {
	ace_cargo_space = 18;
	bwi_secondary_cargo_space = 4; //M1084A1R SOV inherits from this
	maximumLoad = 50;
};
class rhsusf_M1084A1P2_B_M2_fmtv_usarmy: rhsusf_M1083A1P2_B_M2_fmtv_usarmy {
	ace_cargo_space = 18;
	maximumLoad = 50;
};
// M1085A1P2
class rhsusf_M1085A1P2_B_Medical_fmtv_usarmy: rhsusf_M1083A1P2_B_fmtv_usarmy {
	ace_cargo_space = 4;
    maximumload = 50;
};
class rhsusf_HEMTT_A4_base: Truck_01_base_F {
	ace_cargo_space = 24;
	maximumLoad = 100;
};
class rhsusf_M978A4_usarmy_wd: rhsusf_M977A4_usarmy_wd {
	ace_cargo_space = 1;
	maximumLoad = 50;
};
class rhsusf_M978A4_BKIT_usarmy_wd: rhsusf_M977A4_usarmy_wd {
	ace_cargo_space = 1;
	maximumLoad = 50;
};
class rhsusf_M977A4_REPAIR_usarmy_wd: rhsusf_M977A4_usarmy_wd {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class rhsusf_M977A4_REPAIR_BKIT_usarmy_wd: rhsusf_M977A4_BKIT_usarmy_wd {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class rhsusf_M977A4_REPAIR_BKIT_M2_usarmy_wd: rhsusf_M977A4_BKIT_M2_usarmy_wd {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class rhsusf_M977A4_AMMO_usarmy_wd: rhsusf_M977A4_usarmy_wd {
	ace_cargo_space = 3;
	maximumLoad = 50;
};
class rhsusf_M977A4_AMMO_BKIT_usarmy_wd: rhsusf_M977A4_BKIT_usarmy_wd {
	ace_cargo_space = 3;
	maximumLoad = 50;
};
class rhsusf_M977A4_AMMO_BKIT_M2_usarmy_wd: rhsusf_M977A4_BKIT_M2_usarmy_wd {
	ace_cargo_space = 3;
	maximumLoad = 50;
};
class rhs_kamaz5350: O_Truck_02_covered_F {
	ace_cargo_space = 8;
	maximumLoad = 50;
};
class rhs_kamaz5350_open: rhs_kamaz5350 {
	ace_cargo_space = 8;
	maximumLoad = 50;
};
class rhs_kamaz5350_flatbed: rhs_kamaz5350_open {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 2;
	maximumLoad = 50;
};
class rhs_kamaz5350_flatbed_cover: rhs_kamaz5350_flatbed {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 2;
	maximumLoad = 50;
};
class rhsusf_himars_base: Truck_01_base_F {
	ace_cargo_space = 0;
	maximumLoad = 50;
};
class rhs_zil131_flatbed_base: rhs_zil131_open_base {
	ace_cargo_space = 14;
	bwi_secondary_cargo_space = 4;
};
class rhsusf_M1239_base: MRAP_01_base_F {
	ace_cargo_space = 8;
	bwi_secondary_cargo_space = 4;
	maximumLoad = 100;
};
class rhsusf_M1239_Deploy_base: rhsusf_M1239_CROWS_base {
	ace_cargo_space = 1; // Spare tire, vehicle has ammo/fuel
	maximumLoad = 100;
};
class rhsusf_M1239_DeployMK19_base: rhsusf_M1239_CROWSMK19_base {
	ace_cargo_space = 1; // Spare tire, vehicle has ammo/fuel
	maximumLoad = 100;
};


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
 */
class rhsusf_m113_tank_base: APC_Tracked_02_base_F {
	ace_cargo_space = 2; // Spare track.
	maximumLoad = 50;
};
class rhsusf_m113tank_base: APC_Tracked_02_base_F {
	ace_cargo_space = 2; // Spare track.
	maximumLoad = 50;
};
class rhsusf_stryker_base: Wheeled_APC_F {
	ace_cargo_space = 4;
	maximumLoad = 50;
};
class rhsgref_BRDM2: Wheeled_APC_F {
	ace_cargo_space = 2; // Spare wheel.
	maximumLoad = 50;
};
class rhs_pts_base: APC_Tracked_02_base_F { // Tracked, unarmed, amphibious
	ace_cargo_space = 8; // Spare track takes 2 space
	bwi_secondary_cargo_space = 4;
	maximumLoad = 50;
};


/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */
class RHS_MELB_base: Helicopter_Base_H {
	maximumLoad = 25;
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
};
class RHS_MELB_MH6M: RHS_MELB_base {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 2;
};
class RHS_MELB_H6M: RHS_MELB_base {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 2;
};
class RHS_AH1Z_base: Heli_Attack_01_base_F {
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
};
class RHS_AH64_base: Heli_Attack_01_base_F {
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
};
class RHS_CH_47F_base: Heli_Transport_02_base_F {
	ace_cargo_space = 8;
	bwi_secondary_cargo_space = 0;
	maximumLoad = 75;	
};
class RHS_CH_47F_cargo_base: RHS_CH_47F_base {
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 4;
	maximumLoad = 75;	
};
class RHS_Ka52_base: Heli_Attack_02_base_F {
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
};
class RHS_ka60_grey: O_Heli_Light_02_unarmed_F {
	ace_cargo_space = 3;
	maximumLoad = 25;
};
class RHS_Mi8_base: Heli_Light_02_base_F {
	ace_cargo_space = 8;	
	maximumLoad = 50;
};
class RHS_Mi24_Base: Heli_Attack_02_base_F {
	ace_cargo_space = 2;
	maximumLoad = 50;
};
class RHS_UH1_Base: Heli_light_03_base_F {
	ace_cargo_space = 4;
};
class RHS_UH60_Base: Heli_Transport_01_base_F {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class rhsusf_CH53E_USMC: Helicopter_Base_H {
	ace_cargo_space = 12;
	bwi_secondary_cargo_space = 0;
	maximumLoad = 75;
};
class rhsusf_CH53e_USMC_cargo: rhsusf_CH53E_USMC {
	ace_cargo_space = 20;
	bwi_secondary_cargo_space = 6;
	maximumLoad = 75;
};
class rhs_mi28n_base: rhs_mi28_base {
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
	maximumLoad = 25;
};
class rhs_uh1h_base: Helicopter_Base_H {
	ace_cargo_space = 4;
	maximumLoad = 25;
};


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
 */
class RHS_C130J_Base: Plane_Base_F {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 24;
	bwi_secondary_cargo_space = 0;
	maximumLoad = 150;
};
class RHS_C130J_Cargo_Base: RHS_C130J_Base {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 32;
	bwi_secondary_cargo_space = 8;
	maximumLoad = 150;
};
class RHS_TU95MS_base: Plane_Base_F {
	maximumLoad = 25;
	ace_cargo_space = 0;
	ace_cargo_hasCargo = 0;
};
class RHS_AN2_Base: Plane_Base_F {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 6;
	maximumLoad = 75;
};


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
 */
class RHS_Ship: Ship_F {
	maximumLoad = 150;
};
class rhsusf_mkvsoc: RHS_Ship {
 	ace_cargo_space = 6;
 	maximumLoad = 250;
};


/**
 * STATIC HOWITZERS
 * Standard = 7
 */
class RHS_M119_base: StaticCannon {
	ace_cargo_size = 7;
};

class rhs_d30_base: StaticCannon {
	ace_cargo_size = 7;
};
