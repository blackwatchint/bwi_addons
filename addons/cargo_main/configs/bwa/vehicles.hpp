/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
 */
class BWA3_Eagle_base: Car_F {
	ace_cargo_space = 6;
	maximumLoad = 100;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
 */


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
 */


/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */
class BWA3_Tiger_base: Helicopter_Base_F {
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
	maximumLoad = 25;
}; 


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
 */


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
 */
