class UK3CB_BAF_Jackal_Base;
class uk3cb_baf_man_hx58_base;
class uk3cb_baf_man_hx58_cargo_green_a;
class uk3cb_baf_man_hx58_cargo_sand_a;
class uk3cb_baf_man_hx60_base;
class uk3cb_baf_man_hx60_cargo_green_a;
class uk3cb_baf_man_hx60_cargo_sand_a;
class UK3CB_BAF_Merlin_HC3_Unarmed_Base;
class UK3CB_BAF_Husky_HMG_Base;
class UK3CB_BAF_Husky_GPMG_Base;
class UK3CB_BAF_Husky_GMG_Base;


/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
 */
class UK3CB_BAF_LandRover_Base: Car_F {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_LandRover_Soft_Base: UK3CB_BAF_LandRover_Base {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_Coyote_L111A1_Base: UK3CB_BAF_Jackal_Base {
	ace_cargo_space = 8;
	maximumLoad = 150;
};
class UK3CB_BAF_Coyote_L134A1_Base: UK3CB_BAF_Jackal_Base {
	ace_cargo_space = 8;
	maximumLoad = 150;
};
class UK3CB_BAF_Jackal2_L111A1_Base: UK3CB_BAF_Coyote_L111A1_Base {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_Jackal2_L134A1_Base: UK3CB_BAF_Coyote_L134A1_Base {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_UK3CB_BAF_LandRover_Panama_Base: UK3CB_BAF_LandRover_Base {
	ace_cargo_space = 1;
	maximumLoad = 0;
};
class UK3CB_BAF_Panther_Base: MRAP_01_base_F {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_Husky_Base: MRAP_01_base_F {
	ace_cargo_space = 1;
	maximumLoad = 100;
};
class UK3CB_BAF_Husky_Logistics_HMG_Sand: UK3CB_BAF_Husky_HMG_Base {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_Husky_Logistics_GPMG_Sand: UK3CB_BAF_Husky_GPMG_Base {
	ace_cargo_space = 4;
	maximumLoad = 100;
};
class UK3CB_BAF_Husky_Logistics_GMG_Sand: UK3CB_BAF_Husky_GMG_Base {
	ace_cargo_space = 4;
	maximumLoad = 100;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
 */
class UK3CB_BAF_MAN_Container_Base: CargoNet_01_ammo_base_F { // Container for HX60.
	ace_cargo_space = 24;
	maximumLoad = 100;
};
class UK3CB_BAF_MAN_HX58_Container_Green: UK3CB_BAF_MAN_Container_Base { // Container for HX58.
	ace_cargo_space = 32;
	maximumLoad = 100;
};
class uk3cb_baf_man_hx60_fuel_base: uk3cb_baf_man_hx60_base {
    ace_cargo_space = 1;
	maximumLoad = 50;
};
class uk3cb_baf_man_hx60_repair_base: uk3cb_baf_man_hx60_base {
    ace_cargo_space = 6;
	maximumLoad = 50;
};
class uk3cb_baf_man_hx60_cargo_base: uk3cb_baf_man_hx60_base {
    ace_cargo_space = 1; // Can load cargo containers. 1 cargo for space tire on back of cabin.
	maximumLoad = 50;
};
class uk3cb_baf_man_hx60_cargo_green_b: uk3cb_baf_man_hx60_cargo_green_a {
    ace_cargo_space = 1; // Can load cargo containers. 1 cargo for space tire on back of cabin.
	maximumLoad = 50;
};
class uk3cb_baf_man_hx60_cargo_sand_b: uk3cb_baf_man_hx60_cargo_sand_a {
    ace_cargo_space = 1; // Can load cargo containers. 1 cargo for space tire on back of cabin.
	maximumLoad = 50;
};
class uk3cb_baf_man_hx58_fuel_base: uk3cb_baf_man_hx58_base {
    ace_cargo_space = 1;
	maximumLoad = 50;
};
class uk3cb_baf_man_hx58_repair_base: uk3cb_baf_man_hx58_base {
    ace_cargo_space = 6;
	maximumLoad = 50;
};
class UK3CB_BAF_MAN_HX58_Transport_Base: UK3CB_BAF_MAN_HX58_Base {
	ace_cargo_space = 12;
	maximumLoad = 50;
};
class uk3cb_baf_man_hx58_cargo_base: uk3cb_baf_man_hx58_base {
    ace_cargo_space = 1; // Can load cargo containers. 1 cargo for space tire on back of cabin.
	maximumLoad = 50;
};
class uk3cb_baf_man_hx58_cargo_green_b: uk3cb_baf_man_hx58_cargo_green_a {
    ace_cargo_space = 1; // Can unload cargo container. 1 cargo for spare tire on back of cabin.
	maximumLoad = 50;
};
class uk3cb_baf_man_hx58_cargo_sand_b: uk3cb_baf_man_hx58_cargo_sand_a {
    ace_cargo_space = 1; // Can unload cargo container. 1 cargo for spare tire on back of cabin.
	maximumLoad = 50;
};


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
 */
class UK3CB_BAF_FV432_Mk3_Base: APC_Tracked_01_base_F {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class UK3CB_BAF_Warrior_A3_Base: APC_Tracked_03_base_F {
	ace_cargo_space = 4;
	maximumLoad = 50;
};


/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */
class UK3CB_BAF_Apache_base: Heli_Attack_01_base_F {
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
	maximumLoad = 25;
};
class UK3CB_BAF_Merlin_Base: Heli_Transport_02_base_F {
	ace_cargo_space = 10;
	maximumLoad = 75;
};
class UK3CB_BAF_Merlin_HC3_Cargo: UK3CB_BAF_Merlin_HC3_Unarmed_Base {
	ace_cargo_space = 16;
};
class UK3CB_BAF_Wildcat_Base: Heli_light_03_base_F {
	ace_cargo_space = 3;
	maximumLoad = 25;
};


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
 */


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
 */
class UK3CB_BAF_RHIB_Base: Boat_Armed_01_base_F{
   ace_cargo_space = 4;
   maximumLoad = 100;
};
