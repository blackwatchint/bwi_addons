class Redd_Tank_LKW_leicht_gl_Wolf_Base;
class Redd_Tank_Fuchs_1A4_Base;
class rnt_lkw_10t_mil_gl_kat_i_base;
class rnt_lkw_7t_mil_gl_kat_i_base;
class rnt_lkw_5t_mil_gl_kat_i_base;


/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
 */
class Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FueFu: Redd_Tank_LKW_leicht_gl_Wolf_Base {
	ace_cargo_space = 5;
	maximumLoad = 100;
};
class Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_San: Redd_Tank_LKW_leicht_gl_Wolf_Base {
	ace_cargo_space = 3;
	maximumLoad = 100;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
 */
// LKW Repair 10t
class rnt_lkw_10t_mil_gl_kat_i_repair_fleck: rnt_lkw_10t_mil_gl_kat_i_base {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
// LKW Ammo 7t
class rnt_lkw_7t_mil_gl_kat_i_mun_fleck : rnt_lkw_7t_mil_gl_kat_i_base {
	ace_cargo_space = 3;
	maximumLoad = 50;
};
// LKW Fuel 5t
class rnt_lkw_5t_mil_gl_kat_i_fuel_fleck : rnt_lkw_5t_mil_gl_kat_i_base {
	ace_cargo_space = 1;
	maximumLoad = 50;
};
// LKW Transport 7t
class rnt_lkw_7t_mil_gl_kat_i_transport_fleck : rnt_lkw_7t_mil_gl_kat_i_base {
	ace_cargo_space = 12;
	maximumLoad = 50;
};
// LKW Transport 5t
class rnt_lkw_5t_mil_gl_kat_i_transport_fleck : rnt_lkw_5t_mil_gl_kat_i_base {
	ace_cargo_space = 8;
	maximumLoad = 50;
};


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
 */
class Redd_Tank_Wiesel_1A2_TOW_base: Tank_F {
   ace_cargo_space = 0;
   maximumLoad = 50;
};
class Redd_Tank_Wiesel_1A4_MK20_base: Tank_F {
   ace_cargo_space = 0;
   maximumLoad = 50;
};
class Redd_Tank_Fuchs_1A4_Pi_Flecktarn: Redd_Tank_Fuchs_1A4_Base {
   ace_cargo_space = 1;
};
class Redd_Tank_Fuchs_1A4_San_Flecktarn: Redd_Tank_Fuchs_1A4_Base {
   ace_cargo_space = 2;
};


/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
 */


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
 */
