class ffaa_et_rg31_base;
class ffaa_m250_base;
class ffaa_et_pegaso_base;
class ffaa_ec135_base;
class ffaa_et_ch47_base;
class ffaa_ea_hercules_base;

/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
 */
class ffaa_et_anibal: Car_F { //Anibal
	maximumLoad = 100;
	ace_cargo_space = 6;
};
class ffaa_et_rg31_samson: ffaa_et_rg31_base { // RG31 Mk.5E Nyala
	maximumLoad = 100;
	ace_cargo_space = 2;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
 * Recovery = 16 / 50
 */
class ffaa_et_m250_base_blin: ffaa_m250_base { // M250 Truck (base class)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 8;
	maximumLoad = 50;
};
class ffaa_et_m250_recuperacion_blin: ffaa_et_m250_base_blin { // M250 (Recovery)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 16;
	bwi_secondary_cargo_space = 4;
	maximumLoad = 50;
};
class ffaa_et_m250_combustible_blin: ffaa_et_m250_base_blin { // M250 (Fuel)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 1;
	maximumLoad = 50;
};
class ffaa_et_m250_repara_municion_blin: ffaa_et_m250_base_blin { // M250 (Workshop and Ammo)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class ffaa_et_pegaso_carga_lona: ffaa_et_pegaso_base { // Pegaso 7226 (Transport)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 8;
	maximumLoad = 50;
};
class ffaa_et_pegaso_combustible: ffaa_et_pegaso_base { // Pegaso 7226 (Fuel)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 8;
	maximumLoad = 50;
};
class ffaa_et_pegaso_repara_municion: ffaa_et_pegaso_base { // Pegaso 7226 (Workshop and Ammo)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 6;
	maximumLoad = 50;
};


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
 */
// Inherited from core cargo

/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */
class ffaa_famet_ec135: ffaa_ec135_base { // Eurocopter 135 T2
	maximumLoad = 25;
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 2;
};
class ffaa_famet_cougar_base: Helicopter_Base_H { // AS532UL Cougar
	maximumLoad = 50;
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 6;
};
class ffaa_nh90_base: Helicopter_Base_H { // NH90 TTH
	maximumLoad = 75;
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 8;
};
class ffaa_famet_ch47_mg: ffaa_et_ch47_base { // CH-47D (Transport)
	maximumLoad = 75;
	ace_cargo_hasCargo = 10;
	ace_cargo_space = 0;
};
class ffaa_famet_ch47_mg_cargo: ffaa_et_ch47_base { // CH-47D (Cargo)
	maximumLoad = 75;
	ace_cargo_hasCargo = 16;
	ace_cargo_space = 0;
};
class ffaa_famet_tigre_base: Helicopter_Base_F { // Eurocopter Tiger 665
	maximumLoad = 25;
	ace_cargo_hasCargo = 0;
	ace_cargo_space = 0;
};


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
 */
class ffaa_ea_hercules: ffaa_ea_hercules_base { // C130-J (Transport)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 24;
	bwi_secondary_cargo_space = 0;
	maximumLoad = 150;
};
class ffaa_ea_hercules_cargo_base: ffaa_ea_hercules_base { // C130-J (Cargo)
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 32;
	bwi_secondary_cargo_space = 0;
	maximumLoad = 150;
};


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
 */
// Inherited from core cargo