
/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
 */
// UH-1H, UH-1D
class JK_UH1H_base: Helicopter_Base_H {
    ace_cargo_space = 4;
    maximumLoad = 25;
};

// UH-1N
class JK_AB212_base: Helicopter_Base_H {
    ace_cargo_space = 6;
    maximumLoad = 50;
};

// CH-146
class JK_Heli_Medium_01_base_F: Helicopter_Base_H {
    ace_cargo_space = 6;
    maximumLoad = 50;
};