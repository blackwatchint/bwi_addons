class CfgPatches {
	class bwi_cargo_main {
		requiredVersion = 1;
		authors[] = { "Fourjays", "RedBery", "Tebro", "Dhorkiy", "Andrew" };
		authorURL = "http://blackwatch-int.com";
		author = "Black Watch International";
		version = 2.5;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"ace_cargo",
			"bwi_cargo",
			"uk3cb_baf_vehicles_apache",
			"uk3cb_baf_vehicles_bulldog",
			"uk3cb_baf_vehicles_coyote_jackal",
			"uk3cb_baf_vehicles_merlin",
			"uk3cb_baf_vehicles_wildcat",
			"uk3cb_baf_vehicles_MAN",
			"uk3cb_baf_vehicles_panther",
			"uk3cb_baf_vehicles_rhib",
			"uk3cb_baf_vehicles_warrior_a3",
			"bwa3_tiger",
			"rhs_main",
			"rhs_c_heavyweapons",
			"rhsusf_c_heavyweapons",
			"rhsusf_main",
			"rhsgref_main",
			"rhsusf_c_fmtv",
			"ace_compat_rhs_usf3",
			"redd_tank_fuchs_1a4",
			"redd_tank_lkw_leicht_gl",
			"redd_tank_wiesel_1a2_tow",
			"redd_tank_wiesel_1a4_mk20",
			"uk3cb_factions_Vehicles_brdm",
			"uk3cb_factions_Vehicles_btr",
			"uk3cb_factions_Vehicles_common",
			"uk3cb_factions_Vehicles_hilux",
			"uk3cb_factions_Vehicles_ikarus",
			"uk3cb_factions_Vehicles_kamaz",
			"uk3cb_factions_Vehicles_lada",
			"uk3cb_factions_Vehicles_landrover",
			"uk3cb_factions_Vehicles_scud",
			"uk3cb_factions_Vehicles_maxxpro",
			"uk3cb_Factions_Vehicles_MH9",
			"uk3cb_factions_Vehicles_v3s",
			"uk3cb_factions_Vehicles_tractor",
			"uk3cb_factions_Vehicles_ural",
			"uk3cb_factions_Vehicles_volha",
			"uk3cb_factions_Vehicles_vwgolf",
			"uk3cb_factions_Vehicles_UH1H",
			"uk3cb_factions_Vehicles_rhib",
			"uk3cb_factions_Vehicles_vodnik",
			"ffaa_ea_hercules",
			"ffaa_ec135",
			"ffaa_et_ch47",
			"ffaa_et_anibal",
			"ffaa_et_cougar",
			"ffaa_et_leopardo",
			"ffaa_et_lince",
			"ffaa_et_pegaso",
			"ffaa_et_pizarro",
			"ffaa_et_rg31",
			"ffaa_et_searcher",
			"ffaa_et_tigre",
			"ffaa_et_toa",
			"ffaa_et_vamtac",
			"ffaa_husky",
			"ffaa_nh90",
			"ffaa_husky",
            "jk_medium_helicopter"
		};
		units[] = {};
	};
};

class CfgVehicles {
	#include <cfgVehicles.hpp>
};