params ["_crate", "_unit"];

[5, [_crate, _unit],
{
	(_this select 0) params ["_crate", "_unit"]; // Don't ask why, ACE weirdness.

	// Get the build point objects and delete them.
	private _buildPoints = _crate getVariable ["bwi_construction_buildPoints", []];
	{
		deleteVehicle _x;
	} forEach _buildPoints;

	// Load the composition config from the crate definition.
	private _compositionCfgs = "true" configClasses (configFile >> "CfgVehicles" >> (typeOf _crate) >> "BWI_Construction_Composition");

	// Store all objects in an array for passing to the EH.
	private _allObjects = [];

	// Iterate through the composition and build it.
	{
		// Convert the polar coordinates to a world position.
		private _objPos = [
			[
				getNumber (_x >> "distance"),
				getNumber (_x >> "angle"),
				getNumber (_x >> "height")
			], 
			getPosATL _crate, 
			getDir _crate
		] call bwi_common_fnc_polarToWorld;

		private _objDir = getNumber (_x >> "orient") + getDir _crate;

		private _classname = getText (_x  >> "type");
		private _disableDamage = getNumber (_x >> "disableDamage");
		private _disablePhysics = getNumber (_x >> "disablePhysics");


		// Create the object.
		private _newObj = createVehicle [_classname, _objPos, [], 0, "CAN_COLLIDE"];
		_newObj setPosATL _objPos;
		_newObj setDir _objDir;

		// Angle to match terrain.
		private _objNorm = surfaceNormal (getPos _newObj); 
		_newObj setVectorUp _objNorm;

		// Disable damage and physics if required.
		if ( _disableDamage == 1 ) then {
			_newObj allowDamage false;
		};

		if ( _disablePhysics == 1 ) then {
			_newObj enableSimulationGlobal false;
		};

		// Save to the array.
		_allObjects pushBack _newObj;
	} forEach _compositionCfgs;

	// Inform any listeners that the construction was completed.
	["bwi_construction_buildingCompleted", [_crate, _unit, _allObjects]] call CBA_fnc_localEvent;	

	// Update the build phase tracking variable.
	_crate setVariable ["bwi_construction_isBuilding", false, true];

	// Delete the crate (important it occur after the CBA event).
	deleteVehicle _crate;
}, 
{}, 
"Completing Construction", 
{
	// Check crate is alive and is in build phase each frame.
	alive (_this select 0 select 0) && ((_this select 0 select 0) getVariable ["bwi_construction_isBuilding", false])
}, []] call ace_common_fnc_progressBar;