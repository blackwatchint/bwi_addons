/**
 * Event handler that iterates over all objects when a
 * construction is completed, setting the texture of any
 * flag objects to match the player's bwi_armory faction.
 */
["bwi_construction_buildingCompleted",
	{
		private _crate = _this select 0;
		private _unit = _this select 1;
		private _objects = _this select 2;

		{
			// Set flag textures according to side on flags.
			if ( _x isKindOf "FlagCarrier" ) then {
				private _flagTexture = [side _unit] call bwi_armory_fnc_getFlagFromFaction;

				if ( _flagTexture != "" ) then {
					_x setFlagTexture _flagTexture;
				};
			};
		} forEach _objects;
	}
] call CBA_fnc_addEventHandlerArgs;