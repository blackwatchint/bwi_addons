params ["_object", ["_radii", []]];

// Run on the server only.
if ( isServer ) then {
	// Ensure array is initialized due to pushBack.
	if ( isNil "bwi_construction_exclusionZones" ) then {
		bwi_construction_exclusionZones = [];
	};

	// If radii is too short, use the default tiers.
	if ( count _radii < 3 ) then {
		_radii = [
			bwi_construction_exclusionRadiusTier1,
			bwi_construction_exclusionRadiusTier2,
			bwi_construction_exclusionRadiusTier3
		];
	};

	// Add the zone to the array.
	bwi_construction_exclusionZones pushBack [_object, _radii];

	// Broadcast for all clients.
	publicVariable "bwi_construction_exclusionZones";
};