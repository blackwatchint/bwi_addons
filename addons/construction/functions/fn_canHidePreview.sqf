params ["_crate", "_unit"];

private _result = true;

// Get the tracking variables from the crate.
private _previewPFHID = _crate getVariable ["bwi_construction_previewPFHID", -1];
private _isBuilding = _crate getVariable["bwi_construction_isBuilding", false]; 

// If a PFHID is not set, preview isn't currently displayed.
if ( _previewPFHID < 0 ) then {
	_result = false;
};

// Disable preview when a crate is in the building stage.
if ( _isBuilding ) then {
	_result = false;
};

_result