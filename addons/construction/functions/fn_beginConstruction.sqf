params ["_crate", "_unit"];

// Disable ACE dragging and carrying on this crate.
// Done instantly to prevent movement while progress bar runs.
_crate setVariable ["ace_dragging_canDrag", false, true];
_crate setVariable ["ace_dragging_canCarry", false, true];

[5, [_crate, _unit],
{
	(_this select 0) params ["_crate", "_unit"]; // Don't ask why, ACE weirdness.

	// Hide the 3D preview.
	[_crate, _unit] call bwi_construction_fnc_hidePreview;

	// Set a tracking variable to mark the crate as in the build phase.
	_crate setVariable ["bwi_construction_isBuilding", true, true];

	// Get the preview points from the config.
	private _crateCfg = configFile >> "CfgVehicles" >> (typeOf _crate);
	private _previewCfgs = "true" configClasses (_crateCfg >> "BWI_Construction_Build");
	private _buildPoints = [];

	{
		// Convert the polar coordinates to a world position.
		private _objPos = [
			[
				getNumber (_x >> "distance"), 
				getNumber (_x >> "angle"), 
				0
			], 
			getPosATL _crate,
			getDir _crate
		] call bwi_common_fnc_polarToWorld;

		// Create the construction flags.
		private _flag = createVehicle ["BWI_Construction_BuildPointFlag", _objPos, [], 0, "CAN_COLLIDE"];
		_flag setPosATL _objPos;
		_flag setDir parseNumber ((random 360) toFixed 0);
		_flag setObjectTextureGlobal [0, "#(argb,8,8,3)color(1,0.25,0,1.0,co)"];
		_flag allowDamage false;

		// Save a variable for tracking construction status on this point.
		_flag setVariable ["bwi_construction_buildStatus", 0, true];
		_flag setVariable ["bwi_construction_buildTimer", getNumber (_x >> "timer"), true];

		// Add to the build points array.
		_buildPoints pushBack _flag;
	} forEach _previewCfgs;	

	// Save the array of the flags to the crate.
	_crate setVariable ["bwi_construction_buildPoints", _buildPoints, true];
},
{
	// Progress stopped by user or conditions.
	(_this select 0) params ["_crate", "_unit"]; // Don't ask why, ACE weirdness.

	// Quit if crate gone.
	if ( !alive _crate ) exitWith {};

	/**
	 * Re-enable drag and carry if cancelled or user died.

	 * ErrorCode 1 = Cancelled
	 * ErrorCode 2 = Player Died
	 * ErrorCode 3 = Per-Frame Condition == False
	 * ErrorCode 4 = Unknown?
	 */
	if ( _this select 3 != 3 ) then {
		_crate setVariable ["ace_dragging_canDrag", true, true];
		_crate setVariable ["ace_dragging_canCarry", true, true];
	};
}, 
"Beginning Construction",
{
	// Check crate is alive and NOT in build phase each frame.
	alive (_this select 0 select 0) && !((_this select 0 select 0) getVariable ["bwi_construction_isBuilding", false])
}, []] call ace_common_fnc_progressBar;