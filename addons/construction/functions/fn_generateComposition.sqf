/**
 * This is a utility function for generating a set of class 
 * definitions that can be used with bwi_construction to recreate
 * a composition in-game through the completeConstruction method.
 *
 * As positions are intended to be recreated relative to another
 * object, object positions are converted from world coordinates
 * of (x,y) to polar coordinates of (distance,angle). In order to
 * be useful again, they must be converted back.
 *
 * A set of preview/build points are also generated, based off the
 * placement of a specific object (default FlagMarker_01_F). This
 * includes generation of a center point. Additional variables
 * previewLabel, previewIndex, and buildTimer can be set on 
 * objects in the editor for inclusion.
 *
 * Additionally, a set of points and radiuses for testing the 
 * placement area for obstacles and flatness, are also generated
 * from any placed triggers. Note that while a trigger can be any
 * shape or size, the resulting areas will be defined as a
 * circular position and radius only.
 *
 * In-game flags such as Enable Simulation and Enable Damage are
 * copied across to the resulting object composition.
 * 
 * The resulting classes are formatted and copied to the clipboard
 * for placement in a child of BWI_Construction_CrateBase.
 */
params ["_rootObj", "_range", ["_previewObjectClass", "FlagMarker_01_F"]];

// Init return/clipboard vars.
private _indexObjects = 1;
private _indexAreas = 1;
private _clipboardPoints = "";
private _clipboardAreas = "";
private _clipboardObjects = "";

// Declare CRLF and tab for formatting clipboard output.
private _crlf = toString [13,10]; 
private _tab = toString [9];

// Find all objects within the specified range.
private _nearObjs = nearestObjects [_rootObj, [], _range];

// Get root object information.
private _rootObjPos = getPosATL _rootObj;
private _rootObjDir = getDir _rootObj;


{
    // Don't process the root object.
    if ( _x != _rootObj ) then {
        // Get object information.
        private _objType = typeOf _x;
        private _objPos = getPosATL _x;
        private _objDir = getDir _x;

        // Get the polar coordinates.
        private _objPolar = [_objPos, _rootObjPos] call bwi_common_fnc_worldToPolar;
        private _objOrient = _rootObjDir + _objDir;

        
        switch true do {
            // Is a preview/build point object.
            case ( typeOf _x == _previewObjectClass ): {
                private _previewIndex = _x getVariable ["previewIndex", _forEachIndex];
                private _previewLabel = _x getVariable ["previewLabel", ""];
                private _previewTimer = _x getVariable ["buildTimer", 1];

                // Add flags for the preview point.
                private _flags = format["timer = %1; ", _previewTimer];

                if ( _previewLabel != "" ) then {
                    _flags = _flags + format['label = "%1"; ', _previewLabel];
                };

                // Generate the preview point class and add to the string.
                private _pointClass = format[
                    "class Point%1 { distance = %2; angle = %3; %4};", 
                    _previewIndex, _objPolar select 0, _objPolar select 1, _flags
                ];
                
                // Save to the clipboard string.
                _clipboardPoints = _clipboardPoints + _tab + _pointClass + _crlf;
            };

            // Is a trigger defining an area.
            case ( _x isKindOf "EmptyDetector" ): {
                private _area = triggerArea _x;
                private _radius = (_area select 0) max (_area select 1);

               // Generate the check point class and add to the string.
                private _areaClass = format[
                    "class Area%1 { distance = %2; angle = %3; radius = %4; };", 
                    _indexAreas, _objPolar select 0, _objPolar select 1, _radius
                ];

                // Save to the clipboard string.
                _clipboardAreas = _clipboardAreas + _tab + _areaClass + _crlf;
                _indexAreas = _indexAreas + 1;
            };

            // Is a composition object.
            default {
                // Generate damage and physics flags accordingly.
                private _flags = "";

                if ( !isDamageAllowed _x ) then {
                    _flags = _flags + "disableDamage = 1; ";
                };

                if ( !simulationEnabled _x ) then {
                    _flags = _flags + "disablePhysics = 1; ";
                };

                // Generate the composition object class and add to the string.
                private _objectClass = format[
                    'class Object%1 { type = "%2"; distance = %3; angle = %4; height = %5; orient = %6; %7};', 
                    _indexObjects, _objType, _objPolar select 0, _objPolar select 1, _objPolar select 2, _objOrient, _flags
                ];

                // Save to the clipboard string.
                _clipboardObjects = _clipboardObjects + _tab + _objectClass + _crlf;
                _indexObjects = _indexObjects + 1;
            };
        };
    };
} foreach _nearObjs;

// Copy to the clipboard as a finished structure.
copyToClipboard (
    "class BWI_Construction_Build {" + _crlf + _clipboardPoints + "};"
    + _crlf + _crlf +
    "class BWI_Construction_Area {" + _crlf + _clipboardAreas + "};"
    + _crlf + _crlf +
    "class BWI_Construction_Composition {" + _crlf + _clipboardObjects + "};"
);