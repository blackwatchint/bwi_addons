params ["_crate", "_unit"];

private _result = true;

// Check if it is in the build phase.
private _isBuilding = _crate getVariable["bwi_construction_isBuilding", false]; 
if ( !_isBuilding ) then {
	_result = false;
};

// Check for the required tool.
if ( !("BWI_Construction_Tool" in (items _unit)) ) then {
	_result = false;
};

_result