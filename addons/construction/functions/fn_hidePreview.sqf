params ["_crate", "_unit"];

// Get the CBA PFH handle from the crate variable.
private _previewPFHID = _crate getVariable ["bwi_construction_previewPFHID", -1];

// Remove the PFH and the PFHID tracking variable.
if ( _previewPFHID >= 0 ) then {
	[_previewPFHID] call CBA_fnc_removePerFrameHandler;
};

_crate setVariable ["bwi_construction_previewPFHID", nil];

// Update the preview status tracking variable.
_crate setVariable ["bwi_construction_previewOn", false];