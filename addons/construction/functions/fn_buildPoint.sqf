params ["_flag"];

// Get the build timer from the object variable.
private _timer = _flag getVariable ["bwi_construction_buildTimer", 20];

[_timer, [_flag],
{
	(_this select 0) params ["_flag"]; // Don't ask why, ACE weirdness.

	// Update the build status.
	_flag setVariable ["bwi_construction_buildStatus", 1, true];

	// Update the flag colour.
	_flag setObjectTextureGlobal [0, "#(argb,8,8,3)color(0.2,0.9,0,1.0,co)"];

}, {}, "Building", {true}, []] call ace_common_fnc_progressBar;

