params ["_crate", "_unit"];

// Create a PFH for rendering the 3D preview UI.
private _previewPFHID = [
	{
		(_this select 0) params ["_crate", "_unit"];

		private _cratePos = getPosATL _crate;
		private _crateDir = getDir _crate;

		// Hide the preview if player or crate cease to be.
		if ( !(alive _crate) || !(alive _unit) ) exitWith {
			[_crate, _unit] call bwi_construction_fnc_hidePreview;
		};

		// Hide the preview if the crate is below ground level.
		if ( (_cratePos select 2) < -0.1 ) exitWith {
			[_crate, _unit] call bwi_construction_fnc_hidePreview;
		};

		// Hide the preview if it enters build phase.
		if ( _crate getVariable ["bwi_construction_isBuilding", false] ) then {
			[_crate, _unit] call bwi_construction_fnc_hidePreview;
		};
		
		// Only draw when within 250m of the crate.
		if ( (_unit distance _crate) <= bwi_construction_previewViewDistance ) then {
			// Get the preview points from the config.
			private _crateCfg = configFile >> "CfgVehicles" >> (typeOf _crate);
			private _previewCfgs = "true" configClasses (_crateCfg >> "BWI_Construction_Build");

			// Check if the placement is good, getting the result as an array.
			private _isFlatEmpty = [_crate, _unit, true] call bwi_construction_fnc_canBeginConstruction;
			private _isFlat = _isFlatEmpty select 0;
			private _isEmpty = _isFlatEmpty select 1;
			private _isEquipped = _isFlatEmpty select 2;
			private _isExcluded = _isFlatEmpty select 3;

			// Render the status icon.
			private _icon = "";
			private _text = "";
			private _color = [1,1,1,0.7];

			// Set status icon and text according to placement state.
			switch true do {
				case ( _isFlat && _isEmpty && _isEquipped && !_isExcluded ): {
					_icon = "\a3\ui_f\data\igui\rscingameui\rscunitinfoairrtdfull\ico_insp_ok_3_ca.paa";
					_text = getText (_crateCfg >> "displayName");
					_color = bwi_construction_previewColorOK;
				};

				case ( !_isFlat && _isEmpty && _isEquipped && !_isExcluded ): {
					_icon = "\a3\ui_f\data\igui\rscingameui\rscunitinfoairrtdfull\ico_insp_damaged_3_ca.paa";
					_text = "Too Steep";
					_color = bwi_construction_previewColorError;
				};

				case ( _isFlat && !_isEmpty && _isEquipped && !_isExcluded ): {
					_icon = "\a3\ui_f\data\igui\rscingameui\rscunitinfoairrtdfull\ico_insp_damaged_3_ca.paa";
					_text = "Obstructed";
					_color = bwi_construction_previewColorError;
				};

				case ( !_isFlat && !_isEmpty && _isEquipped && !_isExcluded ): {
					_icon = "\a3\ui_f\data\igui\rscingameui\rscunitinfoairrtdfull\ico_insp_damaged_3_ca.paa";
					_text = "Too Steep, Obstructed";
					_color = bwi_construction_previewColorError;
				};

				case ( !_isEquipped ): {
					_icon = "\a3\ui_f\data\igui\rscingameui\rscunitinfoairrtdfull\ico_insp_damaged_3_ca.paa";
					_text = "Missing Equipment";
					_color = bwi_construction_previewColorWarning;
				};

				case ( _isExcluded ): {
					_icon = "\a3\ui_f\data\igui\rscingameui\rscunitinfoairrtdfull\ico_insp_damaged_3_ca.paa";
					_text = "Exclusion Zone";
					_color = bwi_construction_previewColorWarning;
				};
			};

			// Draw the finished icon just above the crate.
			private _iconPos =_cratePos vectorAdd [0,0,0.5];
			drawIcon3D [_icon, _color, _iconPos, 1, 1, 0, _text, 0, 0.025, "EtelkaMonospaceProBold"];

			// Iterate over the build points to draw the outline.
			{
				// Convert the polar coordinates to a world position.
				private _objPos = [
					[
						getNumber (_x >> "distance"), 
						getNumber (_x >> "angle"),
						0
					], 
					_cratePos, 
					_crateDir
				] call bwi_common_fnc_polarToWorld;

				private _text = "";
				private _color = bwi_construction_previewColorLine;
				private _icon = "\a3\ui_f\data\igui\cfg\cursors\mission_ca.paa";
				private _label = getText (_x >> "label");
				private _distance = _unit distance _objPos;

				// Ensure distance remains 3 chars long.
				switch true do {
					case (_distance >= 100): { _distance = _distance toFixed 0; };
					case (_distance >= 10):  { _distance = _distance toFixed 1; };
					default 				 { _distance = _distance toFixed 2; };
				};

				if ( _label != "" ) then {
					_text = format ["%1m (%2)", _distance, _label];
				} else {
					_text = format ["%1m", _distance];
				};

				// Draw the 3D icon for the point.
				drawIcon3D [_icon, _color, _objPos, 1, 1, 0, _text, 0, 0.025, "EtelkaMonospaceProBold"];

				// Draw the 3D line from this point to the next.
				private _nextIndex = _forEachIndex + 1;
				if ( _nextIndex >= count _previewCfgs ) then { _nextIndex = 0; };

				// Convert next objects polar coord to a position.
				private _nextObj = _previewCfgs select _nextIndex;
				private _nextObjPos = [
					[
						getNumber (_nextObj >> "distance"), 
						getNumber (_nextObj >> "angle"),
						0
					], 
					_cratePos, 
					_crateDir
				] call bwi_common_fnc_polarToWorld;

				// Repeat the lines at multiple heights.
				for "_i" from 1 to 3 do {
					private _height = _i * 0.1;
					private _lineStart = [_objPos select 0, _objPos select 1, _height];
					private _lineEnd = [_nextObjPos select 0, _nextObjPos select 1, _height];

					drawLine3D [_lineStart, _lineEnd, _color];
				};
			} forEach _previewCfgs;	
		};
	},
	0, // Run every frame.
	[_crate, _unit]
] call CBA_fnc_addPerFrameHandler;

// Save the PFHID to the crate for tracking.
_crate setVariable ["bwi_construction_previewPFHID", _previewPFHID];

