params ["_crate", "_unit", ["_resultAsArray", false]];

private _result = true;
private _isFlat = true;
private _isEmpty = true;
private _isEquipped = true;
private _isExcluded = false;

// Get the preview points from the config.
private _previewCfgs = "true" configClasses (configFile >> "CfgVehicles" >> (typeOf _crate) >> "BWI_Construction_Area");

// Get the construction tier from the config.
private _tierIndex = getNumber( configFile >> "CfgVehicles" >> (typeOf _crate) >> "bwi_construction_tier" ) - 1;

// Handle invalid construction tiers.
if ( _tierIndex < 0 || _tierIndex > 2 ) exitWith {
	_result = false;

	// Log the error.
	["Invalid tier for %1, must be 1, 2 or 3.", (typeOf _crate)] call bwi_common_fnc_log;
};

// Check if it is already in the build phase.
private _isBuilding = _crate getVariable["bwi_construction_isBuilding", false]; 
if ( _isBuilding ) then {
	_result = false;
};

// Check for the required tool.
if ( !("BWI_Construction_Tool" in (items _unit)) ) then {
	_result = false;
	_isEquipped = false;
};

// Check the preview areas only if result is still true.
if ( _result ) then {
	{
		// Convert the polar coordinates to a world position.
		private _objPos = [
			[
				getNumber (_x >> "distance"), 
				getNumber (_x >> "angle"), 
				0
			], 
			getPosATL _crate,
			getDir _crate
		] call bwi_common_fnc_polarToWorld;
		
		private _radius = getNumber (_x >> "radius");

		// Check gradient only as isFlatEmpty is unreliable for object detection.
		if ( (_objPos isFlatEmpty  [-1, -1, 0.3, _radius, -1, false, _crate] isEqualTo []) ) then {
			_result = false;
			_isFlat = false;
		};

		// Check for empty space.
		if ( !((nearestObjects [_objPos, ["All"], _radius]) isEqualTo []) ) then {
			_result = false;
			_isEmpty = false;
		};

		// Check if within an exclusion zone.
		{
			private _zoneCenter = _x select 0;
			private _zoneRadius = _x select 1 select _tierIndex;

			if ( !isNull _zoneCenter ) then {
				if ( _objPos distance (getPos _zoneCenter) < _zoneRadius ) then {
					_result = false;
					_isExcluded = true; // Inverted
				};
			};
		} forEach bwi_construction_exclusionZones;
	} forEach _previewCfgs;
};

// Return result as an array of results if requested.
if ( _resultAsArray ) then {
	_result = [_isFlat, _isEmpty, _isEquipped, _isExcluded];
};

_result