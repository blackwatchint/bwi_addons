params ["_crate", "_unit"];

[10, [_crate, _unit],
{
	(_this select 0) params ["_crate", "_unit"]; // Don't ask why, ACE weirdness.

	// Get the build point objects and delete them.
	private _buildPoints = _crate getVariable ["bwi_construction_buildPoints", []];
	{
		deleteVehicle _x;
	} forEach _buildPoints;

	// Clear the crate variable.
	_crate setVariable ["bwi_construction_buildPoints", [], true];

	// Enable ACE dragging and carrying on this crate.
	_crate setVariable ["ace_dragging_canDrag", true, true];
	_crate setVariable ["ace_dragging_canCarry", true, true];

	// Update the build phase tracking variable.
	_crate setVariable ["bwi_construction_isBuilding", false, true];
	
},
{},
"Cancelling Construction",
{
	// Check crate is alive and still in build phase each frame.
	alive (_this select 0 select 0) && ((_this select 0 select 0) getVariable ["bwi_construction_isBuilding", false])
}, []] call ace_common_fnc_progressBar;