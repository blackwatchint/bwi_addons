params ["_crate", "_unit"];

private _result = false;

// Have the required tool.
if ( "BWI_Construction_Tool" in (items _unit) ) then {
	// Check if it is in the build phase.
	private _isBuilding = _crate getVariable["bwi_construction_isBuilding", false]; 

	// Crate is in the building stage.
	if ( _isBuilding ) then {
		private _buildPoints = _crate getVariable ["bwi_construction_buildPoints", []];
		private _progress = 0;

		// Check each of the build points to see if they are completed.
		{
			if ( _x getVariable ['bwi_construction_buildStatus', 0] == 1 ) then {
				_progress = _progress + 1;
			};
		} forEach _buildPoints;

		// If all points are complete.
		if ( count _buildPoints == _progress ) then {
			_result = true;
		};
	};
};

_result