class CfgPatches 
{
    class bwi_construction
    {
        requiredVersion = 1;
        authors[] = { "Fourjays" };
        author = "Black Watch International";
        url = "http://blackwatch-int.com";
        version = 1.1;
        versionStr = "1.1.0";
        versionAr[] = {1,1,0};
        requiredAddons[] = {
            "A3_UI_F",
            "ace_common", 
            "ace_interaction",
            "ace_trenches",
            "bwi_common",
            "bwi_armory"
        };
        units[] = { };
    };
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgEditorCategories {
    #include <cfgEditorCategories.hpp>
};

class CfgEditorSubcategories {
    #include <cfgEditorSubcategories.hpp>
};

class CBA_Extended_EventHandlers; // Required here.
class CfgVehicles 
{
	#include <cfgVehicles.hpp>
};

class CfgWeapons
{
    #include <cfgWeapons.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>