/**
 * Clones the ACE Entrenching Tool under a different class
 * so it can be used without the attached ACE Trenches
 * functionality.
 */
class ACE_EntrenchingTool;
class BWI_Construction_Tool: ACE_EntrenchingTool {
    displayName = "Construction Tool";
};