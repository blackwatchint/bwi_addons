// Run on the server only.
if ( isServer ) then {
	// Initialize public variables and broadcast.
	if ( isNil "bwi_construction_exclusionZones" ) then {
		missionNamespace setVariable ["bwi_construction_exclusionZones", [], true];
	};
};

// Register event handlers.
call bwi_construction_fnc_registerFlagTextureHandler;