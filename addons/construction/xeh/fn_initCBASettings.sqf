// Define category strings for the CBA settings.
private _categoryLabelConstruction = "BWI Construction";
private _categoryLabelPreview = "3D Preview";
private _categoryLabelRestrictions = "Restrictions";

// Initialize CBA settings.
// Restriction settings.
[
    "bwi_construction_exclusionRadiusTier1",
    "SLIDER",
    ["Tier 1 Exclusion Radius", "Default radius for an exclusion zone within which tier 1 constructions are prohibited."],
    [_categoryLabelConstruction, _categoryLabelRestrictions],
    [500, 10000, 1000, 0],
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_construction_exclusionRadiusTier2",
    "SLIDER",
    ["Tier 2 Exclusion Radius", "Default radius for an exclusion zone within which tier 2 constructions are prohibited."],
    [_categoryLabelConstruction, _categoryLabelRestrictions],
    [500, 10000, 2000, 0],
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;

[
    "bwi_construction_exclusionRadiusTier3",
    "SLIDER",
    ["Tier 3 Exclusion Radius", "Default radius for an exclusion zone within which tier 3 constructions are prohibited."],
    [_categoryLabelConstruction, _categoryLabelRestrictions],
    [500, 10000, 3000, 0],
    true,
    {},
    true // Require mission restart.
] call CBA_settings_fnc_init;


// User interface settings.
[
    "bwi_construction_previewViewDistance",
    "SLIDER",
    ["Draw Range", "Range at which the 3D preview will no longer be displayed."],
    [_categoryLabelConstruction, _categoryLabelPreview],
    [10, 1000, 250, 0],
    false // Not-synced.
] call CBA_settings_fnc_init;

// Colour settings.
[
    "bwi_construction_previewColorOK",
    "COLOR",
    ["OK Color", "Color used when the construction position is good."],
    [_categoryLabelConstruction, _categoryLabelPreview],
    [0,1,0,0.7],
    false // Not-synced.
] call CBA_settings_fnc_init;

[
    "bwi_construction_previewColorError",
    "COLOR",
    ["Error Color", "Color used when the construction position is bad."],
    [_categoryLabelConstruction, _categoryLabelPreview],
    [1,0,0,0.7],
    false // Not-synced.
] call CBA_settings_fnc_init;

[
    "bwi_construction_previewColorWarning",
    "COLOR",
    ["Warning Color", "Color used when the construction is prohibited by other factors."],
    [_categoryLabelConstruction, _categoryLabelPreview],
    [1,0.5,0,0.7],
    false // Not-synced.
] call CBA_settings_fnc_init;

[
    "bwi_construction_previewColorLine",
    "COLOR",
    ["Line Color", "Color used for the construction perimeter outline."],
    [_categoryLabelConstruction, _categoryLabelPreview],
    [0,0,1,0.8],
    false // Not-synced.
] call CBA_settings_fnc_init;

