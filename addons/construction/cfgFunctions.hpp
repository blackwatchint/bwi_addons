class bwi_construction
{
	class functions
	{
		file = "\bwi_construction\functions";
		class generateComposition{};

		class addExclusionZone{};

		class canShowPreview{};
		class canHidePreview{};
		class showPreview{};
		class hidePreview{};

		class canBeginConstruction{};
		class beginConstruction{};

		class canCancelConstruction{};
		class cancelConstruction{};

		class canCompleteConstruction{};
		class completeConstruction{};

		class canBuildPoint{};
		class buildPoint{};

		class registerFlagTextureHandler{};
	};

	class xeh
	{
		file = "\bwi_construction\xeh";
		class initCBASettings{};
		class initConstruction{};
	};
};