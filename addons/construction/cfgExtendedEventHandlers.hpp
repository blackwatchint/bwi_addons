class Extended_PreInit_EventHandlers {
    class bwi_construction_settings {
        init = "call bwi_construction_fnc_initCBASettings;";
    };
};

class Extended_PostInit_EventHandlers {
    class bwi_construction_init {
        init = "call bwi_construction_fnc_initConstruction;";
    };
};