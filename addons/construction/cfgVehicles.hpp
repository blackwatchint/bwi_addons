/**
 * The flag for build points.
 */
class FlagMarker_01_F;
class BWI_Construction_BuildPointFlag: FlagMarker_01_F
{
    scope = 1;
    scopeCurator = 1;
    displayName = "Build Point Flag";

    class EventHandlers
    {
        //class CBA_Extended_EventHandlers;
        class CBA_Extended_EventHandlers: CBA_Extended_EventHandlers {};
    };

    class ACE_Actions 
    {
        class ACE_MainActions // Use this to make it top-level interaction.
        {
                displayName = "Build";
                distance = 6;
                condition = "[_target] call bwi_construction_fnc_canBuildPoint"; // Omit ;.
                statement = "[_target] call bwi_construction_fnc_buildPoint;";
                priority = 1;
                icon = "\bwi_construction\data\icons\build.paa";
                showDisabled = 0;
                
        };
    };
};


/**
 * Base crate inherited by all other construction crates.
 */
class ReammoBox_F;
class BWI_Construction_CrateBase: ReammoBox_F
{
	scope = 0;
    scopeCurator = 0;
	displayName = "Base Deployable";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionKits";

	model = "\A3\weapons_F\AmmoBoxes\AmmoBox_F";
	icon = "iconCrateSupp";

	maximumLoad = 0;
	ace_cargo_size = 4;
    ace_dragging_canDrag = 1;
    ace_dragging_canCarry = 1;

	// Not an index, must be 1, 2 or 3.
	bwi_construction_tier = 1;
	
    class TransportItems {};

    class ACE_Actions
    {
        class ACE_MainActions
        {
            distance = 6;

            class BWI_Construction_ShowPlacementPreview
            {
                displayName = "Show Preview";
                condition = "[_target, _player] call bwi_construction_fnc_canShowPreview"; // Omit ;.
                distance = 6;
                statement = "[_target, _player] call bwi_construction_fnc_showPreview;";
                priority = 1.245;
                icon = "\bwi_construction\data\icons\preview.paa";
                showDisabled = 0;
                exceptions[] = {};
            };

            class BWI_Construction_HidePlacementPreview
            {
                displayName = "Hide Preview";
                condition = "[_target, _player] call bwi_construction_fnc_canHidePreview"; // Omit ;.
                distance = 6;
                statement = "[_target, _player] call bwi_construction_fnc_hidePreview;";
                priority = 1.245;
                icon = "\bwi_construction\data\icons\preview.paa";
                showDisabled = 0;
                exceptions[] = {};
            };

            class BWI_Construction_BeginConstruction
            {
                displayName = "Begin Construction";
                condition = "[_target, _player] call bwi_construction_fnc_canBeginConstruction"; // Omit ;.
                distance = 6;
                statement = "[_target, _player] call bwi_construction_fnc_beginConstruction;";
                priority = 1.245;
                icon = "\bwi_construction\data\icons\build.paa";
                showDisabled = 0;
                exceptions[] = {};
            };

            class BWI_Construction_CancelConstruction
            {
                displayName = "Cancel Construction";
                condition = "[_target, _player] call bwi_construction_fnc_canCancelConstruction"; // Omit ;.
                distance = 6;
                statement = "[_target, _player] call bwi_construction_fnc_cancelConstruction;";
                priority = 1.245;
                icon = "\bwi_construction\data\icons\cancel.paa";
                showDisabled = 0;
                exceptions[] = {};
            };

            class BWI_Construction_CompleteConstruction
            {
                displayName = "Complete Construction";
                condition = "[_target, _player] call bwi_construction_fnc_canCompleteConstruction"; // Omit ;.
                distance = 6;
                statement = "[_target, _player] call bwi_construction_fnc_completeConstruction;";
                priority = 1.245;
                icon = "\bwi_construction\data\icons\confirm.paa";
                showDisabled = 0;
                exceptions[] = {};
            };
        };
    };
};


/**
 * Base crates with icon locations setup to match the textured
 * labels as follows:
 *   [Small1] [Small2]
 *   [Long1]  [Medium2]
 *   [Small3] [Small4]
 *            [Medium1]
 */
class BWI_Construction_CrateBaseSmall1: BWI_Construction_CrateBase
{
	hiddenSelections[] = {"Camo_Signs", "Camo"};

	class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
	};
};

class BWI_Construction_CrateBaseSmall2: BWI_Construction_CrateBase
{
	hiddenSelections[] = {"Camo_Signs", "Camo"};

    class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
	};
};

class BWI_Construction_CrateBaseSmall3: BWI_Construction_CrateBase
{
	hiddenSelections[] = {"Camo_Signs", "Camo"};

	class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
	};
};

class BWI_Construction_CrateBaseSmall4: BWI_Construction_CrateBase
{
	hiddenSelections[] = {"Camo_Signs", "Camo"};

	class AnimationSources
	{
		class Ammo_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class AmmoOrd_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
		class Grenades_source
		{
			source = "user";
			initPhase = 0;
			animPeriod = 1;
		};
		class Support_source
		{
			source = "user";
			initPhase = 1;
			animPeriod = 1;
		};
	};
};

class BWI_Construction_CrateBaseMedium1: BWI_Construction_CrateBase
{
	model = "\A3\weapons_F\AmmoBoxes\WpnsBox_large_F";

	hiddenSelections[] = {"Camo_Signs", "Camo"};
};

class BWI_Construction_CrateBaseMedium2: BWI_Construction_CrateBase
{
    model = "\A3\weapons_F\AmmoBoxes\WpnsBox_F";

	hiddenSelections[] = {"Camo_Signs", "Camo"};
};

class BWI_Construction_CrateBaseLong1: BWI_Construction_CrateBase
{
    model = "\A3\weapons_F\AmmoBoxes\WpnsBox_long_F";
	
	hiddenSelections[] = {"Camo_Signs", "Camo"};
};