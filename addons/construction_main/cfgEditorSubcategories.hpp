class EdSubcat_BwiConstructionModularBaseKits {
    displayName = "Modular Base Kits";
};

class EdSubcat_BwiConstructionSBFieldKits {
    displayName = "Field Kits (Sandbag)";
};

class EdSubcat_BwiConstructionBBFieldKits {
    displayName = "Field Kits (Barricade)";
};

class EdSubcat_BwiConstructionHBFieldKits {
    displayName = "Field Kits (H-Barrier)";
};

class EdSubcat_BwiConstructionTrenchKits {
    displayName = "Trench Kits";
};

class EdSubcat_BwiConstructionExtraKits {
    displayName = "Extra Kits";
};