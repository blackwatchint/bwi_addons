class CfgPatches 
{
    class bwi_construction_main
    {
        requiredVersion = 1;
        authors[] = { "Fourjays" };
        author = "Black Watch International";
        url = "http://blackwatch-int.com";
        version = 1.1;
        versionStr = "1.1.0";
        versionAr[] = {1,1,0};
        requiredAddons[] = {
            "bwi_construction"
        };
        units[] = {
            // Modular Base Kits - Desert
            "BWI_Construction_BaseWallDesert",
            "BWI_Construction_BaseRampartDesert",
            "BWI_Construction_BaseRampartCornerDesert",
            "BWI_Construction_BaseRampartInvCornerDesert",
            "BWI_Construction_BaseRampartBunkerDesert",
            "BWI_Construction_BaseWallCargoDesert",
            "BWI_Construction_BaseWallCornerBunkerDesert",
            "BWI_Construction_BaseWallCornerCargoDesert",
            "BWI_Construction_BaseSideEntranceDesert",
            "BWI_Construction_BaseMainEntranceBunkerDesert",
            "BWI_Construction_BaseMainEntranceCargoDesert",
            "BWI_Construction_BaseAmmoDumpDesertWest",
            "BWI_Construction_BaseAmmoDumpDesertEast",
            "BWI_Construction_BaseRampartAmmoDumpDesertWest",
            "BWI_Construction_BaseRampartAmmoDumpDesertEast",
            "BWI_Construction_BaseVehicleResupplyDesertWest",
            "BWI_Construction_BaseVehicleResupplyDesertEast",
            "BWI_Construction_BaseFuelBladderDesert",
            "BWI_Construction_BaseHelipadDesert",
            "BWI_Construction_BaseCargoHouseDesert",
            "BWI_Construction_BaseCargoHQDesert",
            "BWI_Construction_BaseCargoTowerDesert",

            // Modular Base Kits - Woodland
            "BWI_Construction_BaseWallWood",
            "BWI_Construction_BaseRampartWood",
            "BWI_Construction_BaseRampartCornerWood",
            "BWI_Construction_BaseRampartInvCornerWood",
            "BWI_Construction_BaseRampartBunkerWood",
            "BWI_Construction_BaseWallCargoWood",
            "BWI_Construction_BaseWallCornerBunkerWood",
            "BWI_Construction_BaseWallCornerCargoWood",
            "BWI_Construction_BaseSideEntranceWood",
            "BWI_Construction_BaseMainEntranceBunkerWood",
            "BWI_Construction_BaseMainEntranceCargoWood",
            "BWI_Construction_BaseAmmoDumpWoodWest",
            "BWI_Construction_BaseAmmoDumpWoodEast",
            "BWI_Construction_BaseRampartAmmoDumpWoodWest",
            "BWI_Construction_BaseRampartAmmoDumpWoodEast",
            "BWI_Construction_BaseVehicleResupplyWoodWest",
            "BWI_Construction_BaseVehicleResupplyWoodEast",
            "BWI_Construction_BaseFuelBladderWood",
            "BWI_Construction_BaseHelipadWood",
            "BWI_Construction_BaseCargoHouseWood",
            "BWI_Construction_BaseCargoHQWood",
            "BWI_Construction_BaseCargoTowerWood",

            // Field Kits - Desert
            "BWI_Construction_SquadSandbagDesertEast",
            "BWI_Construction_SquadSandbagDesertWest",
            "BWI_Construction_SquadSandbagTallDesertEast",
            "BWI_Construction_SquadSandbagTallDesertWest",
            "BWI_Construction_MortarSandbagDesert",
            "BWI_Construction_BunkerSandbagDesert",
            "BWI_Construction_BunkerSandbagLargeDesert",
            "BWI_Construction_CheckpointSandbagDesertWest",
            "BWI_Construction_CheckpointSandbagDesertEast",
            "BWI_Construction_SquadBarricadeDesertEast",
            "BWI_Construction_SquadBarricadeDesertWest",
            "BWI_Construction_SquadBarricadeTallDesertEast",
            "BWI_Construction_SquadBarricadeTallDesertWest",
            "BWI_Construction_SquadHescoDesertWest",
            "BWI_Construction_SquadHescoDesertEast",
            "BWI_Construction_VehicleHescoDesert",
            "BWI_Construction_VehicleHescoCamoDesertWest",
            "BWI_Construction_VehicleHescoCamoDesertEast",
            "BWI_Construction_MortarHescoDesert",
            "BWI_Construction_MortarHescoWireDesert",
            "BWI_Construction_TowerHescoDesert",
            "BWI_Construction_BunkerHescoDesert",
            "BWI_Construction_CheckpointHescoDesertWest",
            "BWI_Construction_CheckpointHescoDesertEast",
            "BWI_Construction_CheckpointHescoHeavyDesertWest",
            "BWI_Construction_CheckpointHescoHeavyDesertEast",

            // Field Kits - Woodland
            "BWI_Construction_SquadSandbagWoodEast",
            "BWI_Construction_SquadSandbagWoodWest",
            "BWI_Construction_SquadSandbagTallWoodEast",
            "BWI_Construction_SquadSandbagTallWoodWest",
            "BWI_Construction_MortarSandbagWood",
            "BWI_Construction_BunkerSandbagWood",
            "BWI_Construction_BunkerSandbagLargeWood",
            "BWI_Construction_CheckpointSandbagWoodWest",
            "BWI_Construction_CheckpointSandbagWoodEast",
            "BWI_Construction_SquadBarricadeWoodEast",
            "BWI_Construction_SquadBarricadeWoodWest",
            "BWI_Construction_SquadBarricadeTallWoodEast",
            "BWI_Construction_SquadBarricadeTallWoodWest",
            "BWI_Construction_SquadHescoWoodWest",
            "BWI_Construction_SquadHescoWoodEast",
            "BWI_Construction_VehicleHescoWood",
            "BWI_Construction_VehicleHescoCamoWoodWest",
            "BWI_Construction_VehicleHescoCamoWoodEast",
            "BWI_Construction_MortarHescoWood",
            "BWI_Construction_MortarHescoWireWood",
            "BWI_Construction_TowerHescoWood",
            "BWI_Construction_BunkerHescoWood",
            "BWI_Construction_CheckpointHescoWoodWest",
            "BWI_Construction_CheckpointHescoWoodEast",
            "BWI_Construction_CheckpointHescoHeavyWoodWest",
            "BWI_Construction_CheckpointHescoHeavyWoodEast",

            // Field Kits - Neutral
            "BWI_Construction_BunkerBarricadeSmall",
            "BWI_Construction_BunkerBarricadeMedium",
            "BWI_Construction_BunkerBarricadeLarge",
            "BWI_Construction_CheckpointBarricadeEast",

            // Trench Kits - Desert
            "BWI_Construction_SquadTrench90DesertEast",
            "BWI_Construction_SquadTrench90DesertWest",
            "BWI_Construction_SquadTrench180DesertEast",
            "BWI_Construction_SquadTrench180DesertWest",
            "BWI_Construction_SquadTrench360DesertEast",
            "BWI_Construction_SquadTrench360DesertWest",
            "BWI_Construction_VehicleTrenchDesert",
            "BWI_Construction_VehicleTrenchCamoDesertEast",
            "BWI_Construction_VehicleTrenchCamoDesertWest",
            "BWI_Construction_VehicleTrenchLargeDesertEast",
            "BWI_Construction_VehicleTrenchLargeDesertWest",

            // Trench Kits - Woodland
            "BWI_Construction_SquadTrench90WoodEast",
            "BWI_Construction_SquadTrench90WoodWest",
            "BWI_Construction_SquadTrench180WoodEast",
            "BWI_Construction_SquadTrench180WoodWest",
            "BWI_Construction_SquadTrench360WoodEast",
            "BWI_Construction_SquadTrench360WoodWest",
            "BWI_Construction_VehicleTrenchWoodland",
            "BWI_Construction_VehicleTrenchCamoWoodlandEast",
            "BWI_Construction_VehicleTrenchCamoWoodlandWest",
            "BWI_Construction_VehicleTrenchLargeWoodlandEast",
            "BWI_Construction_VehicleTrenchLargeWoodlandWest",

            // Extra Kits
            "BWI_Construction_PortableLightKit",
            "BWI_Construction_TallLightKit",
            "BWI_Construction_Shelter2",
            "BWI_Construction_Shelter4",
            "BWI_Construction_TankTrap4",
            "BWI_Construction_TankTrap8"
        };
    };
};

class CfgEditorSubcategories {
    #include <cfgEditorSubcategories.hpp>
};

class CfgVehicles 
{
	#include <cfgVehicles.hpp>
};