/**
 * Walls
 */
class BWI_Construction_BaseWallDesert: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Wall (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 8.50276; angle = 168.741; timer = 4; };
        class Point2 { distance = 9.11573; angle = 155.685; timer = 4; label = "To Front"; };
        class Point3 { distance = 8.94693; angle = 24.4775; timer = 4; label = "To Front"; };
        class Point4 { distance = 8.28262; angle = 11.0907; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 2.74789; angle = 59.2606; radius = 1.5; };
        class Area2 { distance = 2.96913; angle = 120.246; radius = 1.5; };
        class Area3 { distance = 4.71362; angle = 149.062; radius = 1.5; };
        class Area4 { distance = 4.73734; angle = 30.3961; radius = 1.5; };
        class Area5 { distance = 7.10269; angle = 20.9412; radius = 1.5; };
        class Area6 { distance = 7.13609; angle = 160.448; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_Big_F"; distance = 4.92701; angle = 33.1987; height = 0; orient = 361.831; };
        class Object2 { type = "Land_HBarrier_Big_F"; distance = 5.15174; angle = 147.577; height = 0; orient = 361.831; };
    };
};


class BWI_Construction_BaseRampartDesert: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Rampart Wall (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 8.16411; angle = 173.128; timer = 6; };
        class Point2 { distance = 9.04339; angle = 155.142; timer = 12; label = "To Front"; };
        class Point3 { distance = 8.97377; angle = 25.709; timer = 12; label = "To Front"; };
        class Point4 { distance = 8.32729; angle = 8.70121; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 2.76545; angle = 58.5088; radius = 1.5; };
        class Area2 { distance = 2.94598; angle = 119.609; radius = 1.5; };
        class Area3 { distance = 4.67742; angle = 148.848; radius = 1.5; };
        class Area4 { distance = 4.77008; angle = 30.1148; radius = 1.5; };
        class Area5 { distance = 7.09714; angle = 160.368; radius = 1.5; };
        class Area6 { distance = 7.1388; angle = 20.7989; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrierWall6_F"; distance = 4.46517; angle = 158.281; height = 0; orient = 1.29281; };
        class Object2 { type = "Land_HBarrierWall6_F"; distance = 4.48752; angle = 21.2168; height = 0; orient = 1.29281; };
    };
};


class BWI_Construction_BaseRampartCornerDesert: BWI_Construction_CrateBaseSmall4
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Rampart Corner Wall (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 12.1314; angle = -117.891; timer = 9; label = "To Front"; };
        class Point2 { distance = 6.34656; angle = -156.792; timer = 9; };
        class Point3 { distance = 8.02996; angle = 136.328; timer = 10; label = "To Front"; };
        class Point4 { distance = 6.43636; angle = 65.8928; timer = 9; };
        class Point5 { distance = 12.272; angle = 28.5872; timer = 9; label = "To Front"; };
        class Point6 { distance = 11.4173; angle = 16.4334; timer = 8; };
        class Point7 { distance = 4.10819; angle = 137.112; timer = 5; };
        class Point8 { distance = 11.0743; angle = -105.101; timer = 8; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 4.29207; angle = -166.889; radius = 1.5; };
        class Area2 { distance = 4.52288; angle = 78.4414; radius = 1.5; };
        class Area3 { distance = 4.54845; angle = 157.944; radius = 1.5; };
        class Area4 { distance = 4.59277; angle = 110.936; radius = 1.5; };
        class Area5 { distance = 5.58636; angle = -139.587; radius = 1.5; };
        class Area6 { distance = 5.64804; angle = 49.8462; radius = 1.5; };
        class Area7 { distance = 6.06515; angle = 135.457; radius = 1.5; };
        class Area8 { distance = 7.61701; angle = -122.98; radius = 1.5; };
        class Area9 { distance = 7.77583; angle = 34.0343; radius = 1.5; };
        class Area10 { distance = 10.0016; angle = -114.269; radius = 1.5; };
        class Area11 { distance = 10.2379; angle = 26.0353; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrierWall4_F"; distance = 3.51013; angle = 91.2594; height = 0; orient = 361.201; };
        class Object2 { type = "Land_HBarrierWall6_F"; distance = 3.83122; angle = 156.49; height = 0; orient = 631.201; };
        class Object3 { type = "Land_HBarrier_1_F"; distance = 6.05923; angle = 125.869; height = 1.1857; orient = 615; };
        class Object4 { type = "Land_HBarrierWall6_F"; distance = 7.56366; angle = -117.324; height = 0; orient = 631.201; };
        class Object5 { type = "Land_HBarrierWall6_F"; distance = 7.72731; angle = 27.4208; height = 0; orient = 361.201; };
    };
};


class BWI_Construction_BaseRampartInvCornerDesert: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Rampart Corner Wall, Inverse (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.75574; angle = 81.6494; timer = 10; label = "To Front"; };
        class Point2 { distance = 11.3324; angle = 86.8409; timer = 8; };
        class Point3 { distance = 18.3891; angle = 88.1193; timer = 8; label = "To Front"; };
        class Point4 { distance = 18.604; angle = 79.7467; timer = 8; };
        class Point5 { distance = 4.25037; angle = 29.5232; timer = 7; };
        class Point6 { distance = 12.9983; angle = 172.283; timer = 8; };
        class Point7 { distance = 13.7539; angle = 160.319; timer = 8; label = "To Front"; };
        class Point8 { distance = 7.57804; angle = 143.496; timer = 8; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.19254; angle = 94.5748; radius = 1.5; };
        class Area2 { distance = 4.06603; angle = 55.4006; radius = 1.5; };
        class Area3 { distance = 4.32309; angle = 132.95; radius = 1.5; };
        class Area4 { distance = 6.57801; angle = 70.8919; radius = 1.5; };
        class Area5 { distance = 6.60526; angle = 151.4; radius = 1.5; };
        class Area6 { distance = 9.02381; angle = 158.182; radius = 1.5; };
        class Area7 { distance = 9.17233; angle = 76.7187; radius = 1.5; };
        class Area8 { distance = 11.617; angle = 163.693; radius = 1.5; };
        class Area9 { distance = 11.7154; angle = 79.9026; radius = 1.5; };
        class Area10 { distance = 14.3965; angle = 81.7318; radius = 1.5; };
        class Area11 { distance = 17.0414; angle = 83.2023; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 3.03628; angle = 59.3665; height = -0.15; orient = 0.000180883; };
        class Object2 { type = "Land_HBarrier_1_F"; distance = 3.9813; angle = 41.6192; height = -0.15; orient = 90.0002; };
        class Object3 { type = "Land_HBarrierWall6_F"; distance = 4.08674; angle = 144.649; height = 0; orient = 1.20118; };
        class Object4 { type = "Land_HBarrier_1_F"; distance = 4.37939; angle = 68.2158; height = -0.15; orient = 90.0002; };
        class Object5 { type = "Land_HBarrier_1_F"; distance = 4.25705; angle = 65.9648; height = 1.13138; orient = 90.0002; };
        class Object6 { type = "Land_HBarrier_1_F"; distance = 5.10208; angle = 53.5364; height = -0.15; orient = 0.000180883; };
        class Object7 { type = "Land_HBarrierWall6_F"; distance = 9.29923; angle = 71.546; height = 0; orient = 271.201; };
        class Object8 { type = "Land_HBarrierWall4_F"; distance = 10.4902; angle = 166.938; height = 0; orient = 1.20118; };
        class Object9 { type = "Land_HBarrierWall4_F"; distance = 16.0487; angle = 79.6563; height = 0; orient = 271.201; };
    };
};


/**
 * Wall Bunkers
 */
class BWI_Construction_BaseRampartBunkerDesert: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Rampart Wall w/ Bunker (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 10.9002; angle = 141.516; timer = 4; };
        class Point2 { distance = 12.9162; angle = 131.938; timer = 8; label = "To Front"; };
        class Point3 { distance = 9.82822; angle = 91.6356; timer = 8; label = "Bunker"; };
        class Point4 { distance = 12.7098; angle = 49.5231; timer = 8; label = "To Front"; };
        class Point5 { distance = 10.8934; angle = 39.6241; timer = 4; };
        class Point6 { distance = 7.55556; angle = 61.6429; timer = 5; };
        class Point7 { distance = 4.82787; angle = 44.9549; timer = 5; };
        class Point8 { distance = 4.86502; angle = 136.543; timer = 5; };
        class Point9 { distance = 7.71447; angle = 117.026; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.02156; angle = 90.2811; radius = 4; };
        class Area2 { distance = 9.17696; angle = 63.3177; radius = 1.5; };
        class Area3 { distance = 9.34305; angle = 118.303; radius = 1.5; };
        class Area4 { distance = 10.8304; angle = 50.3697; radius = 1.5; };
        class Area5 { distance = 10.8474; angle = 130.965; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrierTower_F"; distance = 5.05128; angle = 90.0969; height = 0; orient = 539.913; };
        class Object2 { type = "Land_HBarrierWall4_F"; distance = 9.39912; angle = 51.597; height = 0; orient = 361.114; };
        class Object3 { type = "Land_HBarrierWall4_F"; distance = 9.53955; angle = 129.108; height = 0; orient = 361.114; };
    };
};


class BWI_Construction_BaseWallCargoDesert: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Wall w/ Cargo Post (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 12.66; angle = 130.04; timer = 4; };
        class Point2 { distance = 14.2624; angle = 124.525; timer = 4; label = "To Front"; };
        class Point3 { distance = 11.7276; angle = 89.443; timer = 12; label = "Tower"; };
        class Point4 { distance = 14.4606; angle = 54.606; timer = 4; label = "To Front"; };
        class Point5 { distance = 12.7028; angle = 48.99; timer = 4; };
        class Point6 { distance = 10.9177; angle = 60.9944; timer = 7; };
        class Point7 { distance = 5.36138; angle = 13.9122; timer = 7; };
        class Point8 { distance = 5.38257; angle = 167.75; timer = 7; };
        class Point9 { distance = 10.9511; angle = 118.957; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.13378; angle = 89.2862; radius = 5.5; };
        class Area2 { distance = 10.4938; angle = 96.2401; radius = 1.5; };
        class Area3 { distance = 10.6126; angle = 80.49; radius = 1.5; };
        class Area4 { distance = 10.998; angle = 109.485; radius = 1.5; };
        class Area5 { distance = 11.1085; angle = 68.5879; radius = 1.5; };
        class Area6 { distance = 12.2408; angle = 122.42; radius = 1.5; };
        class Area7 { distance = 12.6102; angle = 56.2367; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_Big_F"; distance = 6.80213; angle = 129.08; height = 0; orient = 631.831; };
        class Object2 { type = "Land_HBarrier_Big_F"; distance = 6.87592; angle = 52.4872; height = 0; orient = 631.831; };
        class Object3 { type = "Land_Cargo_Patrol_V3_F"; distance = 6.54884; angle = 90.1175; height = 4.76837e-007; orient = 540; };
        class Object4 { type = "Land_HBarrier_Big_F"; distance = 11.5744; angle = 110.946; height = 0; orient = 361.831; };
        class Object5 { type = "Land_HBarrier_Big_F"; distance = 11.5866; angle = 68.0356; height = 0; orient = 361.831; };
    };
};


class BWI_Construction_BaseWallCornerBunkerDesert: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Corner Wall w/ Bunker (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 11.3828; angle = -131.821; timer = 4; label = "To Front"; };
        class Point2 { distance = 7.79063; angle = -179.293; timer = 9; };
        class Point3 { distance = 11.0009; angle = 133.469; timer = 10; label = "To Front"; };
        class Point4 { distance = 8.20324; angle = 83.9609; timer = 9; label = "Bunker"; };
        class Point5 { distance = 12.1838; angle = 41.4297; timer = 4; label = "To Front"; };
        class Point6 { distance = 10.9132; angle = 32.8511; timer = 4; };
        class Point7 { distance = 5.90171; angle = 77.2354; timer = 4; };
        class Point8 { distance = 2.19329; angle = 55.797; timer = 4; };
        class Point9 { distance = 5.77262; angle = 163.68; timer = 4; };
        class Point10 { distance = 10.0875; angle = -122.473; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.10563; angle = 116.68; radius = 4; };
        class Area2 { distance = 6.30174; angle = 166.869; radius = 1.5; };
        class Area3 { distance = 6.3848; angle = -166.57; radius = 1.5; };
        class Area4 { distance = 7.06665; angle = 72.0364; radius = 1.5; };
        class Area5 { distance = 7.41735; angle = 146.32; radius = 1.5; };
        class Area6 { distance = 7.43759; angle = -145.141; radius = 1.5; };
        class Area7 { distance = 8.39487; angle = 53.604; radius = 1.5; };
        class Area8 { distance = 9.15335; angle = 133.317; radius = 1.5; };
        class Area9 { distance = 9.24784; angle = -131.004; radius = 1.5; };
        class Area10 { distance = 10.3656; angle = 41.7252; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrierTower_F"; distance = 4.11114; angle = 121.81; height = 0; orient = 540; };
        class Object2 { type = "Land_HBarrier_Big_F"; distance = 7.61321; angle = 150.292; height = 0; orient = 631.831; };
        class Object3 { type = "Land_HBarrier_Big_F"; distance = 7.9719; angle = -144.714; height = 0; orient = 631.831; };
        class Object4 { type = "Land_HBarrier_Big_F"; distance = 8.76265; angle = 53.1263; height = 0; orient = 361.831; };
    };
};


class BWI_Construction_BaseWallCornerCargoDesert: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Corner Wall w/ Cargo Post  (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 8.53202; angle = -130.88; timer = 4; label = "To Front"; };
        class Point2 { distance = 6.09943; angle = 161.498; timer = 4; };
        class Point3 { distance = 11.4563; angle = 119.047; timer = 12; label = "Tower"; };
        class Point4 { distance = 10.5859; angle = 74.2755; timer = 4; };
        class Point5 { distance = 15.0337; angle = 42.177; timer = 4; label = "To Front"; };
        class Point6 { distance = 13.7147; angle = 35.4371; timer = 4; };
        class Point7 { distance = 8.41166; angle = 70.5653; timer = 4; };
        class Point8 { distance = 3.29174; angle = 33.4096; timer = 12; };
        class Point9 { distance = 3.82645; angle = 152.961; timer = 4; };
        class Point10 { distance = 7.3209; angle = -117.761; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 4.23981; angle = 172.559; radius = 1.5; };
        class Area2 { distance = 4.65926; angle = -151.559; radius = 1.5; };
        class Area3 { distance = 5.3908; angle = 140.021; radius = 1.5; };
        class Area4 { distance = 5.42964; angle = 97.82; radius = 4; };
        class Area5 { distance = 6.4007; angle = -129.386; radius = 1.5; };
        class Area6 { distance = 7.42442; angle = 124.136; radius = 1.5; };
        class Area7 { distance = 8.88236; angle = 81.9879; radius = 1.5; };
        class Area8 { distance = 8.89896; angle = 100.121; radius = 1.5; };
        class Area9 { distance = 9.6854; angle = 116.182; radius = 1.5; };
        class Area10 { distance = 9.7033; angle = 64.4465; radius = 1.5; };
        class Area11 { distance = 11.2283; angle = 51.5161; radius = 1.5; };
        class Area12 { distance = 13.2165; angle = 42.5116; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_Big_F"; distance = 5.07329; angle = -150.859; height = 0; orient = 271.832; };
        class Object2 { type = "Land_Cargo_Patrol_V3_F"; distance = 5.26308; angle = 96.5471; height = 4.76837e-007; orient = 180; };
        class Object3 { type = "Land_HBarrier_Big_F"; distance = 7.46487; angle = 127.199; height = 0; orient = 271.832; };
        class Object4 { type = "Land_HBarrier_Big_F"; distance = 9.18702; angle = 97.155; height = 0; orient = 1.83162; };
        class Object5 { type = "Land_HBarrier_Big_F"; distance = 11.5932; angle = 51.9879; height = 0; orient = 1.83162; };
    };
};


/**
 * Entrances
 */
class BWI_Construction_BaseSideEntranceDesert: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Side Entrance (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.4668; angle = 77.3864; timer = 2; label = "Entrance"; };
        class Point2 { distance = 2.36828; angle = 145.968; timer = 3; };
        class Point3 { distance = 4.6357; angle = 116.641; timer = 3; };
        class Point4 { distance = 9.66687; angle = 156.495; timer = 3; };
        class Point5 { distance = 11.1262; angle = 143.523; timer = 6; label = "To Front"; };
        class Point6 { distance = 6.85953; angle = 89.6125; timer = 8; label = "Entrance"; };
        class Point7 { distance = 10.3539; angle = 42.0918; timer = 6; label = "To Front"; };
        class Point8 { distance = 8.83672; angle = 28.4102; timer = 3; };
        class Point9 { distance = 4.73532; angle = 58.4283; timer = 3; };
        class Point10 { distance = 3.02279; angle = 30.9899; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.79603; angle = 84.9007; radius = 3; };
        class Area2 { distance = 5.2304; angle = 78.667; radius = 1.5; };
        class Area3 { distance = 5.73509; angle = 111.622; radius = 1.5; };
        class Area4 { distance = 6.35788; angle = 54.3068; radius = 1.5; };
        class Area5 { distance = 6.97546; angle = 131.924; radius = 1.5; };
        class Area6 { distance = 8.20322; angle = 40.2946; radius = 1.5; };
        class Area7 { distance = 8.97112; angle = 144.929; radius = 1.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrierWall_corridor_F"; distance = 4.1085; angle = 87.1082; height = 0; orient = 90.0003; };
        class Object2 { type = "Land_HBarrier_1_F"; distance = 5.58438; angle = 120.895; height = -0.15; orient = 90.0003; };
        class Object3 { type = "Land_HBarrier_1_F"; distance = 6.75664; angle = 115.279; height = -0.15; orient = 90.0003; };
        class Object4 { type = "Land_HBarrier_1_F"; distance = 6.67253; angle = 114.59; height = 1.17946; orient = 90.0003; };
        class Object5 { type = "Land_HBarrierWall4_F"; distance = 6.95798; angle = 41.824; height = 0; orient = 1.20127; };
        class Object6 { type = "Land_HBarrierWall4_F"; distance = 7.70537; angle = 144.762; height = 0; orient = 1.20127; };
    };
};


class BWI_Construction_BaseMainEntranceBunkerDesert: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Main Entrance w/ Bunker (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.81659; angle = 162.727; timer = 5; label = "Gate"; };
        class Point2 { distance = 10.6889; angle = 169.388; timer = 6; };
        class Point3 { distance = 14.7403; angle = 135.615; timer = 6; label = "To Front"; };
        class Point4 { distance = 10.2417; angle = 93.1128; timer = 7; label = "Bunker"; };
        class Point5 { distance = 11.8965; angle = 59.596; timer = 7; label = "To Front"; };
        class Point6 { distance = 7.31501; angle = 34.2178; timer = 7; };
        class Point7 { distance = 1.65768; angle = 65.6734; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.13034; angle = 127.826; radius = 6; };
        class Area2 { distance = 7.41116; angle = 68.0419; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 3.07482; angle = 88.6578; height = 0; orient = 360; };
        class Object2 { type = "Land_BarGate_F"; distance = 4.86803; angle = 160.442; height = 0; orient = 360; disableDamage = 1; };
        class Object3 { type = "Land_HBarrierTower_F"; distance = 6.37441; angle = 64.3722; height = 0; orient = 540; };
        class Object4 { type = "FlagPole_EP1"; distance = 9.41399; angle = 171.589; height = 0; orient = 450; };
        class Object5 { type = "Land_HBarrier_Big_F"; distance = 11.4691; angle = 147.397; height = 0; orient = 631.831; };
    };
};


class BWI_Construction_BaseMainEntranceCargoDesert: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Main Entrance w/ Cargo Post (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 3.73424; angle = 160.634; timer = 5; label = "Gate"; };
        class Point2 { distance = 9.71256; angle = 166.596; timer = 4; };
        class Point3 { distance = 14.1979; angle = 131.63; timer = 4; label = "To Front"; };
        class Point4 { distance = 10.6937; angle = 95.7745; timer = 12; label = "Tower"; };
        class Point5 { distance = 12.9019; angle = 55.2385; timer = 4; };
        class Point6 { distance = 7.55846; angle = 15.7641; timer = 12; };
        class Point7 { distance = 2.24182; angle = 66.4081; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.65243; angle = 66.1056; radius = 5; };
        class Area2 { distance = 7.10202; angle = 122.39; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BarGate_F"; distance = 3.82515; angle = 155.183; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_HBarrier_Big_F"; distance = 6.23704; angle = 91.0026; height = 0; orient = 631.831; };
        class Object3 { type = "Land_Cargo_Patrol_V3_F"; distance = 6.72055; angle = 54.1983; height = 4.76837e-007; orient = 540; };
        class Object4 { type = "FlagPole_EP1"; distance = 8.54426; angle = 169.901; height = 0; orient = 450; };
        class Object5 { type = "Land_HBarrier_Big_F"; distance = 10.118; angle = 72.0272; height = 0; orient = 541.831; };
        class Object6 { type = "Land_HBarrier_Big_F"; distance = 10.7119; angle = 142.928; height = 0; orient = 631.831; };
    };
};


/**
 * Utilities
 */
class BWI_Construction_BaseAmmoDumpDesertWest: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Ammo Dump (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 7.48599; angle = 150.725; timer = 10; label = "Entrance"; };
        class Point2 { distance = 15.5743; angle = 113.848; timer = 10; };
        class Point3 { distance = 15.7321; angle = 65.3428; timer = 10; };
        class Point4 { distance = 6.16733; angle = 12.6311; timer = 16; };
        class Point5 { distance = 6.58062; angle = 168.085; timer = 14; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.4287; angle = 62.5361; radius = 4; };
        class Area2 { distance = 6.17801; angle = 107.537; radius = 5; };
        class Area3 { distance = 10.3065; angle = 75.4479; radius = 5; };
        class Area4 { distance = 11.4285; angle = 104.501; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 2.00707; angle = 101.842; height = 1.20382; orient = 707.002; };
        class Object2 { type = "Land_HBarrier_5_F"; distance = 2.58683; angle = 49.9348; height = 0; orient = 360; };
        class Object3 { type = "Land_HBarrier_5_F"; distance = 3.74432; angle = 31.5551; height = 1.24668; orient = 360; };
        class Object4 { type = "Land_HBarrierWall_corridor_F"; distance = 5.17155; angle = 136.467; height = 0; orient = 544.932; };
        class Object5 { type = "Land_HBarrier_1_F"; distance = 5.56749; angle = 19.6006; height = 0; orient = 627.653; };
        class Object6 { type = "Land_HBarrier_1_F"; distance = 5.8172; angle = 90.2573; height = 0; orient = 581.707; };
        class Object7 { type = "Land_HBarrier_1_F"; distance = 5.81821; angle = 90.3318; height = 1.17621; orient = 581.113; };
        class Object8 { type = "Land_HBarrier_3_F"; distance = 6.75381; angle = 38.5291; height = 0; orient = 631.428; };
        class Object9 { type = "Land_HBarrier_3_F"; distance = 6.83131; angle = 38.3259; height = 1.25692; orient = 450.346; };
        class Object10 { type = "Land_CamoNetVar_NATO_EP1"; distance = 9.98005; angle = 88.4328; height = 0.688056; orient = 719.31; };
        class Object11 { type = "Land_HBarrier_Big_F"; distance = 11.4686; angle = 118.23; height = 0; orient = 632.217; };
        class Object12 { type = "Land_HBarrier_Big_F"; distance = 11.5589; angle = 61.5155; height = 0; orient = 632.217; };
        class Object13 { type = "Land_HBarrier_Big_F"; distance = 13.5054; angle = 89.5184; height = 0; orient = 543.202; };
    };
};


class BWI_Construction_BaseAmmoDumpDesertEast: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Ammo Dump (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 7.48599; angle = 150.725; timer = 10; label = "Entrance"; };
        class Point2 { distance = 15.5743; angle = 113.848; timer = 10; };
        class Point3 { distance = 15.7321; angle = 65.3428; timer = 10; };
        class Point4 { distance = 6.16733; angle = 12.6311; timer = 16; };
        class Point5 { distance = 6.58062; angle = 168.085; timer = 14; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.4287; angle = 62.5361; radius = 4; };
        class Area2 { distance = 6.17801; angle = 107.537; radius = 5; };
        class Area3 { distance = 10.3065; angle = 75.4479; radius = 5; };
        class Area4 { distance = 11.4285; angle = 104.501; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 2.00707; angle = 101.842; height = 1.20382; orient = 707.002; };
        class Object2 { type = "Land_HBarrier_5_F"; distance = 2.58683; angle = 49.9348; height = 0; orient = 360; };
        class Object3 { type = "Land_HBarrier_5_F"; distance = 3.74432; angle = 31.5551; height = 1.24668; orient = 360; };
        class Object4 { type = "Land_HBarrierWall_corridor_F"; distance = 5.17155; angle = 136.467; height = 0; orient = 544.932; };
        class Object5 { type = "Land_HBarrier_1_F"; distance = 5.56749; angle = 19.6006; height = 0; orient = 627.653; };
        class Object6 { type = "Land_HBarrier_1_F"; distance = 5.8172; angle = 90.2573; height = 0; orient = 581.707; };
        class Object7 { type = "Land_HBarrier_1_F"; distance = 5.81821; angle = 90.3318; height = 1.17621; orient = 581.113; };
        class Object8 { type = "Land_HBarrier_3_F"; distance = 6.75381; angle = 38.5291; height = 0; orient = 631.428; };
        class Object9 { type = "Land_HBarrier_3_F"; distance = 6.83131; angle = 38.3259; height = 1.25692; orient = 450.346; };
        class Object10 { type = "Land_CamoNetVar_EAST_EP1"; distance = 9.98005; angle = 88.4328; height = 0.688056; orient = 719.31; };
        class Object11 { type = "Land_HBarrier_Big_F"; distance = 11.4686; angle = 118.23; height = 0; orient = 632.217; };
        class Object12 { type = "Land_HBarrier_Big_F"; distance = 11.5589; angle = 61.5155; height = 0; orient = 632.217; };
        class Object13 { type = "Land_HBarrier_Big_F"; distance = 13.5054; angle = 89.5184; height = 0; orient = 543.202; };
    };
};


class BWI_Construction_BaseRampartAmmoDumpDesertWest: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Rampart Wall w/ Ammo Dump (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.54793; angle = 94.1107; timer = 4; label = "Entrance"; };
        class Point2 { distance = 8.99059; angle = 169.639; timer = 5; };
        class Point3 { distance = 9.9111; angle = 153.515; timer = 8; label = "To Front"; };
        class Point4 { distance = 4.72787; angle = 116.749; timer = 6; };
        class Point5 { distance = 7.74415; angle = 105.757; timer = 6; };
        class Point6 { distance = 14.7641; angle = 125.027; timer = 8; };
        class Point7 { distance = 20.8814; angle = 95.9456; timer = 8; };
        class Point8 { distance = 15.7813; angle = 59.0973; timer = 8; };
        class Point9 { distance = 7.19792; angle = 64.2858; timer = 6; };
        class Point10 { distance = 5.02164; angle = 60.5111; timer = 4; };
        class Point11 { distance = 8.71614; angle = 29.1389; timer = 8; label = "To Front"; };
        class Point12 { distance = 7.83149; angle = 10.4769; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.6671; angle = 125.66; radius = 1.5; };
        class Area2 { distance = 4.44216; angle = 88.0914; radius = 3; };
        class Area3 { distance = 4.44292; angle = 36.9253; radius = 1.5; };
        class Area4 { distance = 5.47096; angle = 146.299; radius = 1.5; };
        class Area5 { distance = 6.71544; angle = 24.7389; radius = 1.5; };
        class Area6 { distance = 7.83061; angle = 157.473; radius = 1.5; };
        class Area7 { distance = 12.1728; angle = 81.6159; radius = 6; };
        class Area8 { distance = 14.0462; angle = 96.0201; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 3.75566; angle = 142.358; height = -0.15; orient = 1.75706; };
        class Object2 { type = "Land_HBarrierWall_corridor_F"; distance = 4.37319; angle = 88.5701; height = 0; orient = 275.195; };
        class Object3 { type = "Land_HBarrier_1_F"; distance = 4.68956; angle = 129.639; height = -0.15; orient = 90.0002; };
        class Object4 { type = "Land_HBarrier_1_F"; distance = 4.52968; angle = 128.007; height = 1.20467; orient = 1.56633; };
        class Object5 { type = "Land_HBarrierWall4_F"; distance = 5.4877; angle = 21.1113; height = 0; orient = 2.26588; };
        class Object6 { type = "Land_HBarrierWall4_F"; distance = 6.6615; angle = 160.792; height = 0; orient = 1.76832; };
        class Object7 { type = "Land_HBarrier_1_F"; distance = 10.0039; angle = 78.2606; height = 0; orient = 305.758; };
        class Object8 { type = "Land_HBarrier_1_F"; distance = 10.776; angle = 86.1873; height = 0; orient = 305.758; };
        class Object9 { type = "Land_HBarrier_Big_F"; distance = 11.4204; angle = 113.343; height = 0; orient = 217.009; };
        class Object10 { type = "Land_HBarrier_Big_F"; distance = 11.6451; angle = 65.2848; height = 0; orient = 307.316; };
        class Object11 { type = "Land_CamoNetVar_NATO_EP1"; distance = 12.8341; angle = 89.7809; height = 0.688056; orient = 34.4099; };
        class Object12 { type = "Land_HBarrier_Big_F"; distance = 16.5904; angle = 81.7738; height = 0; orient = 218.301; };
        class Object13 { type = "Land_HBarrier_Big_F"; distance = 17.3172; angle = 103.735; height = 0; orient = 307.316; };
    };
};


class BWI_Construction_BaseRampartAmmoDumpDesertEast: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Rampart Wall w/ Ammo Dump (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.54793; angle = 94.1107; timer = 4; label = "Entrance"; };
        class Point2 { distance = 8.99059; angle = 169.639; timer = 5; };
        class Point3 { distance = 9.9111; angle = 153.515; timer = 8; label = "To Front"; };
        class Point4 { distance = 4.72787; angle = 116.749; timer = 6; };
        class Point5 { distance = 7.74415; angle = 105.757; timer = 6; };
        class Point6 { distance = 14.7641; angle = 125.027; timer = 8; };
        class Point7 { distance = 20.8814; angle = 95.9456; timer = 8; };
        class Point8 { distance = 15.7813; angle = 59.0973; timer = 8; };
        class Point9 { distance = 7.19792; angle = 64.2858; timer = 6; };
        class Point10 { distance = 5.02164; angle = 60.5111; timer = 4; };
        class Point11 { distance = 8.71614; angle = 29.1389; timer = 8; label = "To Front"; };
        class Point12 { distance = 7.83149; angle = 10.4769; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.6671; angle = 125.66; radius = 1.5; };
        class Area2 { distance = 4.44216; angle = 88.0914; radius = 3; };
        class Area3 { distance = 4.44292; angle = 36.9253; radius = 1.5; };
        class Area4 { distance = 5.47096; angle = 146.299; radius = 1.5; };
        class Area5 { distance = 6.71544; angle = 24.7389; radius = 1.5; };
        class Area6 { distance = 7.83061; angle = 157.473; radius = 1.5; };
        class Area7 { distance = 12.1728; angle = 81.6159; radius = 6; };
        class Area8 { distance = 14.0462; angle = 96.0201; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 3.75566; angle = 142.358; height = -0.15; orient = 1.75706; };
        class Object2 { type = "Land_HBarrierWall_corridor_F"; distance = 4.37319; angle = 88.5701; height = 0; orient = 275.195; };
        class Object3 { type = "Land_HBarrier_1_F"; distance = 4.68956; angle = 129.639; height = -0.15; orient = 90.0002; };
        class Object4 { type = "Land_HBarrier_1_F"; distance = 4.52968; angle = 128.007; height = 1.20467; orient = 1.56633; };
        class Object5 { type = "Land_HBarrierWall4_F"; distance = 5.4877; angle = 21.1113; height = 0; orient = 2.26588; };
        class Object6 { type = "Land_HBarrierWall4_F"; distance = 6.6615; angle = 160.792; height = 0; orient = 1.76832; };
        class Object7 { type = "Land_HBarrier_1_F"; distance = 10.0039; angle = 78.2606; height = 0; orient = 305.758; };
        class Object8 { type = "Land_HBarrier_1_F"; distance = 10.776; angle = 86.1873; height = 0; orient = 305.758; };
        class Object9 { type = "Land_HBarrier_Big_F"; distance = 11.4204; angle = 113.343; height = 0; orient = 217.009; };
        class Object10 { type = "Land_HBarrier_Big_F"; distance = 11.6451; angle = 65.2848; height = 0; orient = 307.316; };
        class Object11 { type = "Land_CamoNetVar_EAST_EP1"; distance = 12.8341; angle = 89.7809; height = 0.688056; orient = 34.4099; };
        class Object12 { type = "Land_HBarrier_Big_F"; distance = 16.5904; angle = 81.7738; height = 0; orient = 218.301; };
        class Object13 { type = "Land_HBarrier_Big_F"; distance = 17.3172; angle = 103.735; height = 0; orient = 307.316; };
    };
};


class BWI_Construction_BaseVehicleResupplyDesertWest: BWI_Construction_CrateBaseSmall4
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Vehicle Resupply (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.91588; angle = 142.98; timer = 20; label = "Crate"; };
        class Point2 { distance = 5.94228; angle = 161.865; timer = 4; };
        class Point3 { distance = 8.77897; angle = 129.933; timer = 5; };
        class Point4 { distance = 8.70593; angle = 50.5411; timer = 5; };
        class Point5 { distance = 5.80576; angle = 18.5569; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.16328; angle = 58.3019; radius = 4; };
        class Area2 { distance = 5.32549; angle = 126.785; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_DrillAku_F"; distance = 3.42621; angle = 40.5902; height = 0.0334177; orient = 465.503; };
        class Object2 { type = "Land_DuctTape_F"; distance = 3.48038; angle = 38.668; height = 0.000596523; orient = 719.365; };
        class Object3 { type = "Land_HBarrier_3_F"; distance = 3.58557; angle = 89.5299; height = 0; orient = 450.09; };
        class Object4 { type = "Land_WeldingTrolley_01_F"; distance = 3.96839; angle = 39.8336; height = 0; orient = 419.965; };
        class Object5 { type = "Box_NATO_AmmoVeh_F"; distance = 4.01741; angle = 126.299; height = 0.0305424; orient = 374.999; };
        class Object6 { type = "Land_CanisterFuel_F"; distance = 4.38646; angle = 75.065; height = 2.0504e-005; orient = 719.991; };
        class Object7 { type = "Land_CanisterFuel_F"; distance = 4.84219; angle = 74.4754; height = 2.24113e-005; orient = 600.001; };
        class Object8 { type = "Land_GasTank_02_F"; distance = 4.813; angle = 41.9169; height = 3.91006e-005; orient = 479.991; };
        class Object9 { type = "Land_CanisterFuel_F"; distance = 5.07416; angle = 71.1836; height = 1.57356e-005; orient = 630.003; };
        class Object10 { type = "Land_GasTank_02_F"; distance = 5.18549; angle = 45.0925; height = 3.62396e-005; orient = 405; };
        class Object11 { type = "Land_CamoNet_NATO_EP1"; distance = 5.12103; angle = 92.2199; height = 0; orient = 360; };
        class Object12 { type = "Land_HBarrier_3_F"; distance = 5.93184; angle = 36.4551; height = 0; orient = 450.09; };
        class Object13 { type = "Land_HBarrier_3_F"; distance = 6.04547; angle = 143.544; height = 0; orient = 450.09; };
        class Object14 { type = "Land_FireExtinguisher_F"; distance = 6.12138; angle = 53.2654; height = 3.8147e-005; orient = 389.988; };
        class Object15 { type = "Land_HBarrier_5_F"; distance = 6.55003; angle = 65.6484; height = 0; orient = 540.089; };
        class Object16 { type = "Land_HBarrier_5_F"; distance = 6.62355; angle = 115.516; height = 0; orient = 540.089; };
    };
};


class BWI_Construction_BaseVehicleResupplyDesertEast: BWI_Construction_CrateBaseSmall4
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Vehicle Resupply (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.91588; angle = 142.98; timer = 20; label = "Crate"; };
        class Point2 { distance = 5.94228; angle = 161.865; timer = 4; };
        class Point3 { distance = 8.77897; angle = 129.933; timer = 5; };
        class Point4 { distance = 8.70593; angle = 50.5411; timer = 5; };
        class Point5 { distance = 5.80576; angle = 18.5569; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.16328; angle = 58.3019; radius = 4; };
        class Area2 { distance = 5.32549; angle = 126.785; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_DrillAku_F"; distance = 3.42621; angle = 40.5902; height = 0.0334177; orient = 465.503; };
        class Object2 { type = "Land_DuctTape_F"; distance = 3.48038; angle = 38.668; height = 0.000596523; orient = 719.365; };
        class Object3 { type = "Land_HBarrier_3_F"; distance = 3.58557; angle = 89.5299; height = 0; orient = 450.09; };
        class Object4 { type = "Land_WeldingTrolley_01_F"; distance = 3.96839; angle = 39.8336; height = 0; orient = 419.965; };
        class Object5 { type = "Box_East_AmmoVeh_F"; distance = 4.01741; angle = 126.299; height = 0.0305424; orient = 374.999; };
        class Object6 { type = "Land_CanisterFuel_F"; distance = 4.38646; angle = 75.065; height = 2.0504e-005; orient = 719.991; };
        class Object7 { type = "Land_CanisterFuel_F"; distance = 4.84219; angle = 74.4754; height = 2.24113e-005; orient = 600.001; };
        class Object8 { type = "Land_GasTank_02_F"; distance = 4.813; angle = 41.9169; height = 3.91006e-005; orient = 479.991; };
        class Object9 { type = "Land_CanisterFuel_F"; distance = 5.07416; angle = 71.1836; height = 1.57356e-005; orient = 630.003; };
        class Object10 { type = "Land_GasTank_02_F"; distance = 5.18549; angle = 45.0925; height = 3.62396e-005; orient = 405; };
        class Object11 { type = "Land_CamoNet_EAST_EP1"; distance = 5.12103; angle = 92.2199; height = 0; orient = 360; };
        class Object12 { type = "Land_HBarrier_3_F"; distance = 5.93184; angle = 36.4551; height = 0; orient = 450.09; };
        class Object13 { type = "Land_HBarrier_3_F"; distance = 6.04547; angle = 143.544; height = 0; orient = 450.09; };
        class Object14 { type = "Land_FireExtinguisher_F"; distance = 6.12138; angle = 53.2654; height = 3.8147e-005; orient = 389.988; };
        class Object15 { type = "Land_HBarrier_5_F"; distance = 6.55003; angle = 65.6484; height = 0; orient = 540.089; };
        class Object16 { type = "Land_HBarrier_5_F"; distance = 6.62355; angle = 115.516; height = 0; orient = 540.089; };
    };
};


class BWI_Construction_BaseFuelBladderDesert: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Fuel Bladder (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_1.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.15342; angle = 33.5817; timer = 30; label = "Hose"; };
        class Point2 { distance = 8.56426; angle = 164.055; timer = 8; };
        class Point3 { distance = 15.529; angle = 122.204; timer = 8; };
        class Point4 { distance = 15.2854; angle = 57.1204; timer = 8; };
        class Point5 { distance = 8.63085; angle = 15.6221; timer = 8; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.77889; angle = 72.0473; radius = 6; };
        class Area2 { distance = 7.85863; angle = 109.967; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "StorageBladder_01_fuel_sand_F"; distance = 6.60961; angle = 90.6878; height = 0.0631223; orient = 540; };
        class Object2 { type = "ContainmentArea_01_sand_F"; distance = 7.68267; angle = 90.2057; height = 0.0493279; orient = 540; };
        class Object3 { type = "Land_HBarrier_Big_F"; distance = 9.71458; angle = 41.7982; height = 0; orient = 630.939; };
        class Object4 { type = "Land_HBarrier_Big_F"; distance = 9.82061; angle = 138.365; height = 0; orient = 630.939; };
        class Object5 { type = "Land_HBarrier_Big_F"; distance = 12.7028; angle = 71.4449; height = 0.0631223; orient = 544.153; };
        class Object6 { type = "Land_HBarrier_Big_F"; distance = 12.8284; angle = 108.691; height = 0.0631223; orient = 540.939; };
    };
};



class BWI_Construction_BaseHelipadDesert: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Helipad (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 13.3903; angle = 171.523; timer = 12; };
        class Point2 { distance = 31.5983; angle = 114.497; timer = 12; };
        class Point3 { distance = 31.9641; angle = 64.5733; timer = 12; };
        class Point4 { distance = 14.1243; angle = 7.60754; timer = 12; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 11.1013; angle = 149.572; radius = 4; };
        class Area2 { distance = 11.5793; angle = 28.0323; radius = 4; };
        class Area3 { distance = 15.2735; angle = 88.7731; radius = 14; };
        class Area4 { distance = 26.7917; angle = 110.496; radius = 4; };
        class Area5 { distance = 26.8766; angle = 68.0424; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_5_F"; distance = 2.26722; angle = 92.7281; height = 0; orient = 360; };
        class Object2 { type = "PortableHelipadLight_01_red_F"; distance = 9.06766; angle = 89.8488; height = 0.0011735; orient = 450.008; };
        class Object3 { type = "Land_HBarrier_3_F"; distance = 9.29679; angle = 14.5523; height = 0; orient = 360; };
        class Object4 { type = "Land_HBarrier_3_F"; distance = 9.50706; angle = 164.864; height = 0; orient = 360; };
        class Object5 { type = "Land_HBarrier_3_F"; distance = 14.2311; angle = 153.236; height = 0; orient = 449.175; };
        class Object6 { type = "Land_HBarrier_3_F"; distance = 14.6172; angle = 24.1455; height = 0; orient = 449.993; };
        class Object7 { type = "HeliH"; distance = 15.243; angle = 89.8119; height = 0; orient = 360.187; disableDamage = 1; };
        class Object8 { type = "PortableHelipadLight_01_green_F"; distance = 16.4434; angle = 111.82; height = 0.0011735; orient = 450.01; };
        class Object9 { type = "PortableHelipadLight_01_green_F"; distance = 16.4524; angle = 67.9656; height = 0.00117397; orient = 450.006; };
        class Object10 { type = "Land_HBarrier_5_F"; distance = 19.7111; angle = 129.894; height = 0; orient = 631.164; };
        class Object11 { type = "Land_HBarrier_5_F"; distance = 20.3403; angle = 49.1755; height = 0; orient = 631.164; };
        class Object12 { type = "PortableHelipadLight_01_red_F"; distance = 21.2576; angle = 90.9963; height = 0.00117397; orient = 450.006; };
        class Object13 { type = "Land_HBarrier_3_F"; distance = 27.4651; angle = 61.2964; height = 0; orient = 450.771; };
        class Object14 { type = "Land_HBarrier_3_F"; distance = 27.5011; angle = 117.338; height = 0; orient = 449.175; };
        class Object15 { type = "Land_HBarrier_5_F"; distance = 28.3057; angle = 89.6531; height = 0; orient = 360; };
        class Object16 { type = "Land_HBarrier_3_F"; distance = 29.5835; angle = 106.788; height = 0; orient = 360; };
        class Object17 { type = "Land_HBarrier_3_F"; distance = 29.8341; angle = 71.5301; height = 0; orient = 360; };
    };
};


/**
 * Structures
 */
class BWI_Construction_BaseCargoHouseDesert: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Cargo House (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.11919; angle = 99.3853; timer = 12; label = "Entrance"; };
        class Point2 { distance = 5.58561; angle = 158.825; timer = 5; };
        class Point3 { distance = 10.5418; angle = 118.949; timer = 5; };
        class Point4 { distance = 10.5015; angle = 60.8988; timer = 5; };
        class Point5 { distance = 5.53091; angle = 22.0203; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.77572; angle = 105.973; radius = 4; };
        class Area2 { distance = 5.91633; angle = 74.8948; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_Cargo_House_V3_F"; distance = 3.10317; angle = 92.3401; height = 0; orient = 360; };
        class Object2 { type = "Land_HBarrier_5_F"; distance = 6.5318; angle = 131.905; height = 0; orient = 630.089; };
        class Object3 { type = "Land_HBarrier_5_F"; distance = 6.53748; angle = 48.4381; height = 0; orient = 630.089; };
        class Object4 { type = "Land_HBarrier_5_F"; distance = 8.75013; angle = 105.217; height = 0; orient = 540.089; };
        class Object5 { type = "Land_HBarrier_3_F"; distance = 8.77106; angle = 75.2729; height = 0; orient = 719.131; };
        class Object6 { type = "Land_HBarrier_1_F"; distance = 9.62748; angle = 61.8324; height = 0; orient = 719.131; };
    };
};


class BWI_Construction_BaseCargoHQDesert: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Cargo HQ (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 8;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.56676; angle = 92.7643; timer = 36; label = "Entrance"; };
        class Point2 { distance = 8.69232; angle = 170.54; timer = 6; };
        class Point3 { distance = 13.7523; angle = 128.877; timer = 7; };
        class Point4 { distance = 21.5193; angle = 113.442; timer = 8; };
        class Point5 { distance = 19.8673; angle = 91.8045; timer = 8; };
        class Point6 { distance = 21.4241; angle = 67.3721; timer = 8; };
        class Point7 { distance = 14.1182; angle = 54.0373; timer = 7; };
        class Point8 { distance = 8.3389; angle = 10.1412; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.94212; angle = 49.0427; radius = 4; };
        class Area2 { distance = 7.23748; angle = 132.798; radius = 4; };
        class Area3 { distance = 11.2973; angle = 90.4823; radius = 9; };
        class Area4 { distance = 16.767; angle = 74.1974; radius = 4; };
        class Area5 { distance = 17.0169; angle = 107.094; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_1_F"; distance = 5.49535; angle = 45.4401; height = 0; orient = 719.131; };
        class Object2 { type = "Land_HBarrier_3_F"; distance = 5.50129; angle = 24.9131; height = 0; orient = 719.131; };
        class Object3 { type = "Land_HBarrier_3_F"; distance = 5.98797; angle = 157.566; height = 0; orient = 719.131; };
        class Object4 { type = "Land_HBarrier_5_F"; distance = 8.59521; angle = 29.9455; height = 0; orient = 630.09; };
        class Object5 { type = "Land_HBarrier_5_F"; distance = 8.94113; angle = 151.409; height = 0; orient = 630.09; };
        class Object6 { type = "Land_Cargo_HQ_V3_F"; distance = 9.22123; angle = 90.6129; height = 4.76837e-007; orient = 630; };
        class Object7 { type = "Land_HBarrier_5_F"; distance = 12.4439; angle = 53.1333; height = 0; orient = 630.09; };
        class Object8 { type = "Land_HBarrier_5_F"; distance = 12.577; angle = 128.429; height = 0; orient = 630.09; };
        class Object9 { type = "Land_HBarrier_5_F"; distance = 17.2655; angle = 64.368; height = 0; orient = 630.09; };
        class Object10 { type = "Land_HBarrier_5_F"; distance = 17.3235; angle = 116.824; height = 0; orient = 630.09; };
        class Object11 { type = "Land_HBarrier_5_F"; distance = 19.0303; angle = 90.4333; height = 0; orient = 540.09; };
        class Object12 { type = "Land_HBarrier_5_F"; distance = 19.8242; angle = 74.0861; height = 0; orient = 540.09; };
        class Object13 { type = "Land_HBarrier_5_F"; distance = 19.8905; angle = 106.81; height = 0; orient = 540.09; };
    };
};


class BWI_Construction_BaseCargoTowerDesert: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Cargo Tower (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionModularBaseKits";
	ace_cargo_size = 10;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_2.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.21143; angle = 89.9873; timer = 30; label = "Entrance"; };
        class Point2 { distance = 6.46543; angle = 160.926; timer = 5; };
        class Point3 { distance = 12.1254; angle = 120.766; timer = 15; };
        class Point4 { distance = 19.8012; angle = 108.291; timer = 17; };
        class Point5 { distance = 20.0646; angle = 71.5754; timer = 17; };
        class Point6 { distance = 12.4173; angle = 58.8592; timer = 15; };
        class Point7 { distance = 6.68739; angle = 19.5056; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.03516; angle = 90.0408; radius = 6.5; };
        class Area2 { distance = 13.4707; angle = 90.4922; radius = 6.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_3_F"; distance = 4.79095; angle = 120.891; height = 0; orient = 540.09; };
        class Object2 { type = "Land_HBarrier_3_F"; distance = 4.8005; angle = 59.0547; height = 0; orient = 540.09; };
        class Object3 { type = "Land_HBarrier_Big_F"; distance = 8.17624; angle = 129.44; height = 0; orient = 631.884; };
        class Object4 { type = "Land_HBarrier_Big_F"; distance = 8.34741; angle = 50.625; height = 0; orient = 631.681; };
        class Object5 { type = "Land_HBarrier_Big_F"; distance = 15.5898; angle = 109.897; height = 0; orient = 631.065; };
        class Object6 { type = "Land_HBarrier_Big_F"; distance = 15.775; angle = 70.3938; height = 0; orient = 632.298; };
        class Object7 { type = "Land_Cargo_Tower_V3_F"; distance = 10.9989; angle = 87.8629; height = 0; orient = 630; };
        class Object8 { type = "Land_HBarrier_Big_F"; distance = 18.124; angle = 89.9186; height = 0; orient = 361.293; };
    };
};