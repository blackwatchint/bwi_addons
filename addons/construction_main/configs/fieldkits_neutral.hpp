/**
 * Barricade Field Kits
 */
class BWI_Construction_BunkerBarricadeSmall: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Bunker (Small)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 3.58529; angle = 141.394; timer = 2; };
        class Point2 { distance = 5.46422; angle = 121.643; timer = 2; };
        class Point3 { distance = 6.46341; angle = 100.986; timer = 2; label = "To Front"; };
        class Point4 { distance = 6.38221; angle = 79.2411; timer = 2; label = "To Front"; };
        class Point5 { distance = 5.47585; angle = 59.8717; timer = 2; };
        class Point6 { distance = 3.49286; angle = 39.1933; timer = 2; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.53214; angle = 89.1327; radius = 3; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_SandbagBarricade_01_F"; distance = 4.14024; angle = 58.5426; height = 0; orient = 449.956; };
        class Object2 { type = "Land_SandbagBarricade_01_F"; distance = 4.19524; angle = 121.663; height = 0; orient = 629.956; };
        class Object3 { type = "Land_SandbagBarricade_01_hole_F"; distance = 5.31785; angle = 73.3254; height = 0; orient = 404.956; };
        class Object4 { type = "Land_SandbagBarricade_01_hole_F"; distance = 5.34023; angle = 107.397; height = 0; orient = 674.956; };
        class Object5 { type = "Land_SandbagBarricade_01_hole_F"; distance = 5.74667; angle = 90.3116; height = 0; orient = 359.956; };
    };
};


class BWI_Construction_BunkerBarricadeMedium: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Bunker (Medium)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 3.13679; angle = 164.317; timer = 3; };
        class Point2 { distance = 6.87605; angle = 134.527; timer = 2; };
        class Point3 { distance = 5.46538; angle = 152.707; timer = 2; };
        class Point4 { distance = 7.29343; angle = 115.57; timer = 3; label = "To Front"; };
        class Point5 { distance = 7.215; angle = 67.3432; timer = 3; label = "To Front"; };
        class Point6 { distance = 6.69011; angle = 44.7886; timer = 2; };
        class Point7 { distance = 5.37689; angle = 29.4074; timer = 2; };
        class Point8 { distance = 3.07274; angle = 16.6539; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 4.15276; angle = 62.1966; radius = 3; };
        class Area2 { distance = 4.30114; angle = 119.011; radius = 3; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_SandbagBarricade_01_half_F"; distance = 2.34819; angle = 41.5143; height = 0; orient = 539.956; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 2.60025; angle = 143.233; height = 0; orient = 539.956; };
        class Object3 { type = "Land_SandbagBarricade_01_F"; distance = 3.97882; angle = 33.4365; height = 0; orient = 494.956; };
        class Object4 { type = "Land_SandbagBarricade_01_F"; distance = 4.20641; angle = 149.061; height = 0; orient = 584.956; };
        class Object5 { type = "Land_SandbagBarricade_01_hole_F"; distance = 5.46215; angle = 43.3465; height = 0; orient = 449.956; };
        class Object6 { type = "Land_SandbagBarricade_01_hole_F"; distance = 5.65946; angle = 138.801; height = 0; orient = 629.956; };
        class Object7 { type = "Land_SandbagBarricade_01_F"; distance = 5.92059; angle = 91.1341; height = 0; orient = 359.956; };
        class Object8 { type = "Land_SandbagBarricade_01_hole_F"; distance = 6.18629; angle = 73.1097; height = 0; orient = 359.956; };
        class Object9 { type = "Land_SandbagBarricade_01_hole_F"; distance = 6.2633; angle = 109.047; height = 0; orient = 359.956; };
        class Object10 { type = "Land_SandbagBarricade_01_F"; distance = 6.26796; angle = 57.9814; height = 0; orient = 404.956; };
        class Object11 { type = "Land_SandbagBarricade_01_F"; distance = 6.39805; angle = 124.316; height = 0; orient = 674.956; };
    };
};


class BWI_Construction_BunkerBarricadeLarge: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Bunker (Large)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.98118; angle = 162.317; timer = 1; };
        class Point2 { distance = 5.32541; angle = 151.713; timer = 4; };
        class Point3 { distance = 9.95696; angle = 119.012; timer = 4; };
        class Point4 { distance = 10.8841; angle = 106.506; timer = 5; label = "To Front"; };
        class Point5 { distance = 10.6878; angle = 72.6702; timer = 5; label = "To Front"; };
        class Point6 { distance = 9.84302; angle = 60.6616; timer = 4; };
        class Point7 { distance = 5.11808; angle = 26.0126; timer = 4; };
        class Point8 { distance = 3.03703; angle = 20.3404; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.87117; angle = 90.5956; radius = 5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_SandbagBarricade_01_half_F"; distance = 2.44471; angle = 39.936; height = 0; orient = 539.956; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 2.50295; angle = 141.722; height = 0; orient = 539.956; };
        class Object3 { type = "Land_SandbagBarricade_01_hole_F"; distance = 4.0639; angle = 32.9511; height = 0; orient = 494.956; };
        class Object4 { type = "Land_SandbagBarricade_01_hole_F"; distance = 4.12545; angle = 148.05; height = 0; orient = 584.956; };
        class Object5 { type = "Land_SandbagBarricade_01_F"; distance = 5.50674; angle = 43.1047; height = 0; orient = 449.956; };
        class Object6 { type = "Land_SandbagBarricade_01_F"; distance = 5.59486; angle = 137.582; height = 0; orient = 629.956; };
        class Object7 { type = "Land_SandbagBarricade_01_hole_F"; distance = 6.9637; angle = 54.7453; height = 0; orient = 449.956; };
        class Object8 { type = "Land_SandbagBarricade_01_hole_F"; distance = 7.03802; angle = 125.95; height = 0; orient = 629.956; };
        class Object9 { type = "Land_SandbagBarricade_01_F"; distance = 8.60734; angle = 62.2088; height = 0; orient = 449.956; };
        class Object10 { type = "Land_SandbagBarricade_01_F"; distance = 8.67575; angle = 118.485; height = 0; orient = 629.956; };
        class Object11 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.77951; angle = 69.7871; height = 0; orient = 404.956; };
        class Object12 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.80163; angle = 90.4881; height = 0; orient = 359.956; };
        class Object13 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.8079; angle = 111.103; height = 0; orient = 674.956; };
        class Object14 { type = "Land_SandbagBarricade_01_F"; distance = 9.98134; angle = 79.3552; height = 0; orient = 359.956; };
        class Object15 { type = "Land_SandbagBarricade_01_F"; distance = 9.99246; angle = 101.517; height = 0; orient = 359.956; };
    };
};


class BWI_Construction_CheckpointBarricadeEast: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Checkpoint (East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 8;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.29379; angle = 86.8334; timer = 5; label = "Entrance"; };
        class Point2 { distance = 10.3885; angle = 167.842; timer = 12; label = "Bunker"; };
        class Point3 { distance = 21.6174; angle = 128.919; timer = 1; };
        class Point4 { distance = 32.1533; angle = 111.577; timer = 6; };
        class Point5 { distance = 30.1321; angle = 89.9782; timer = 5; label = "Exit"; };
        class Point6 { distance = 31.3641; angle = 73.1978; timer = 12; label = "Bunker"; };
        class Point7 { distance = 20.0909; angle = 50.2961; timer = 1; };
        class Point8 { distance = 10.804; angle = 13.7229; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 5.8186; angle = 91.9621; radius = 4; };
        class Area2 { distance = 9.38655; angle = 140.234; radius = 4; };
        class Area3 { distance = 9.85344; angle = 52.1528; radius = 5; };
        class Area4 { distance = 16.2357; angle = 91.3175; radius = 8; };
        class Area5 { distance = 25.4651; angle = 105.585; radius = 5; };
        class Area6 { distance = 26.8923; angle = 91.1663; radius = 4; };
        class Area7 { distance = 27.2291; angle = 76.5023; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint"; distance = 4.48419; angle = 42.3786; height = 0; orient = 11.2726; };
        class Object2 { type = "Sign_Checkpoint"; distance = 5.32413; angle = 144.023; height = 0; orient = 352.114; };
        class Object3 { type = "Land_BarGate_F"; distance = 4.96771; angle = 91.2391; height = 0; orient = 0.000621047; disableDamage = 1; };
        class Object4 { type = "Land_SandbagBarricade_01_half_F"; distance = 6.37769; angle = 34.9798; height = 0; orient = 180.001; };
        class Object5 { type = "Land_SandbagBarricade_01_hole_F"; distance = 6.51303; angle = 149.763; height = 0; orient = 135.001; };
        class Object6 { type = "Land_SandbagBarricade_01_half_F"; distance = 6.95258; angle = 136.252; height = 0; orient = 90.0006; };
        class Object7 { type = "Land_SandbagBarricade_01_hole_F"; distance = 7.65738; angle = 159.903; height = 0; orient = 180.001; };
        class Object8 { type = "Fort_RazorWire"; distance = 7.81344; angle = 8.4919; height = 0; orient = 349.91; };
        class Object9 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.18599; angle = 27.2857; height = 0; orient = 180.001; };
        class Object10 { type = "Fort_RazorWire"; distance = 8.56515; angle = 174.809; height = 0; orient = 184.91; };
        class Object11 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.58003; angle = 125.775; height = 0; orient = 90.0006; };
        class Object12 { type = "Land_SandbagBarricade_01_F"; distance = 9.3448; angle = 159.426; height = 0; orient = 225.001; };
        class Object13 { type = "Land_CncBarrier_stripes_F"; distance = 9.44942; angle = 68.0257; height = 0; orient = 269.361; };
        class Object14 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.87006; angle = 26.4275; height = 0; orient = 135.001; };
        class Object15 { type = "Land_SandbagBarricade_01_half_F"; distance = 10.5621; angle = 152.69; height = 0; orient = 270.001; };
        class Object16 { type = "Land_SandbagBarricade_01_half_F"; distance = 11.2003; angle = 32.1301; height = 0; orient = 90.0006; };
        class Object17 { type = "Land_SandbagBarricade_01_half_F"; distance = 11.6961; angle = 143.946; height = 0; orient = 270.001; };
        class Object18 { type = "FlagPole_EP1"; distance = 11.1192; angle = 119.637; height = 0; orient = 270.001; };
        class Object19 { type = "Fort_RazorWire"; distance = 13.4048; angle = 25.7875; height = 0; orient = 274.91; };
        class Object20 { type = "Land_CncBarrier_stripes_F"; distance = 13.983; angle = 111.617; height = 0; orient = 85.1482; };
        class Object21 { type = "Fort_RazorWire"; distance = 13.9851; angle = 158.28; height = 0; orient = 79.9098; };
        class Object22 { type = "Land_CncBarrier_stripes_F"; distance = 14.9087; angle = 75.888; height = 0; orient = 272.574; };
        class Object23 { type = "Fort_RazorWire"; distance = 19.3662; angle = 135.534; height = 0; orient = 270.868; };
        class Object24 { type = "Land_CncBarrier_stripes_F"; distance = 19.6029; angle = 105.488; height = 0; orient = 92.574; };
        class Object25 { type = "Land_CncBarrier_stripes_F"; distance = 20.6751; angle = 79.9852; height = 0; orient = 265.148; };
        class Object26 { type = "Fort_RazorWire"; distance = 22.6754; angle = 55.6817; height = 0; orient = 270.868; };
        class Object27 { type = "FlagPole_EP1"; distance = 23.849; angle = 79.328; height = 0; orient = 90.0006; };
        class Object28 { type = "Land_CncBarrier_stripes_F"; distance = 24.6942; angle = 102.174; height = 0; orient = 89.3612; };
        class Object29 { type = "Land_SandbagBarricade_01_half_F"; distance = 25.7807; angle = 80.7753; height = 0; orient = 270.001; };
        class Object30 { type = "Land_SandbagBarricade_01_half_F"; distance = 26.7907; angle = 71.6537; height = 0; orient = 90.0006; };
        class Object31 { type = "Land_BarGate_F"; distance = 27.4515; angle = 91.951; height = 0; orient = 180.001; disableDamage = 1; };
        class Object32 { type = "Land_SandbagBarricade_01_half_F"; distance = 27.8646; angle = 81.531; height = 0; orient = 270.001; };
        class Object33 { type = "Land_SandbagBarricade_01_half_F"; distance = 28.4586; angle = 111.906; height = 0; orient = 270.001; };
        class Object34 { type = "Land_SandbagBarricade_01_half_F"; distance = 28.7949; angle = 72.9001; height = 0; orient = 90.0006; };
        class Object35 { type = "Land_SandbagBarricade_01_half_F"; distance = 29.267; angle = 102.599; height = 0; orient = 0.000621047; };
        class Object36 { type = "Land_SandbagBarricade_01_hole_F"; distance = 29.4645; angle = 80.8022; height = 0; orient = 315.001; };
        class Object37 { type = "Fort_RazorWire"; distance = 29.6364; angle = 65.8439; height = 0; orient = 259.91; };
        class Object38 { type = "Sign_Checkpoint"; distance = 29.6655; angle = 83.5462; height = 0; orient = 352.114; };
        class Object39 { type = "Land_SandbagBarricade_01_half_F"; distance = 29.6692; angle = 109.721; height = 0; orient = 315.001; };
        class Object40 { type = "Fort_RazorWire"; distance = 29.7185; angle = 116.034; height = 0; orient = 94.9098; };
        class Object41 { type = "Land_SandbagBarricade_01_half_F"; distance = 29.8012; angle = 106.465; height = 0; orient = 0.000621047; };
        class Object42 { type = "Sign_Checkpoint"; distance = 29.9223; angle = 98.1434; height = 0; orient = 11.2726; };
        class Object43 { type = "Land_SandbagBarricade_01_F"; distance = 30.1203; angle = 74.9285; height = 0; orient = 45.0007; };
        class Object44 { type = "Land_SandbagBarricade_01_hole_F"; distance = 30.3906; angle = 78.0851; height = 0; orient = 0.000663736; };
        class Object45 { type = "Fort_RazorWire"; distance = 32.5651; angle = 105.498; height = 0; orient = 169.91; };
        class Object46 { type = "Fort_RazorWire"; distance = 32.6075; angle = 76.642; height = 0; orient = 4.90977; };
    };
};