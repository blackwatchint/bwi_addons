/**
 * Trench Kits
 */
class BWI_Construction_SquadTrench90DesertEast: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.55861; angle = 20.2194; timer = 7; };
        class Point2 { distance = 5.02141; angle = 159.324; timer = 7; };
        class Point3 { distance = 9.33851; angle = 142.156; timer = 1; };
        class Point4 { distance = 13.1824; angle = 111.416; timer = 4; };
        class Point5 { distance = 13.3744; angle = 89.7448; timer = 4; label = "To Front"; };
        class Point6 { distance = 13.0881; angle = 66.2006; timer = 4; };
        class Point7 { distance = 8.66955; angle = 42.9425; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.38862; angle = 76.7159; radius = 5.5; };
        class Area2 { distance = 7.48088; angle = 104.975; radius = 5.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_rampart_EP1"; distance = 2.53492; angle = 87.2398; height = 0.1; orient = 359.956; };
        class Object2 { type = "Land_CamoNet_EAST_EP1"; distance = 5.82885; angle = 94.1626; height = 0; orient = 539.956; };
        class Object3 { type = "Land_fort_rampart_EP1"; distance = 12.0822; angle = 89.7152; height = -0.0999994; orient = 539.956; };
    };
};


class BWI_Construction_SquadTrench90DesertWest: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.55861; angle = 20.2194; timer = 7; };
        class Point2 { distance = 5.02141; angle = 159.324; timer = 7; };
        class Point3 { distance = 9.33851; angle = 142.156; timer = 1; };
        class Point4 { distance = 13.1824; angle = 111.416; timer = 4; };
        class Point5 { distance = 13.3744; angle = 89.7448; timer = 4; label = "To Front"; };
        class Point6 { distance = 13.0881; angle = 66.2006; timer = 4; };
        class Point7 { distance = 8.66955; angle = 42.9425; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.38862; angle = 76.7159; radius = 5.5; };
        class Area2 { distance = 7.48088; angle = 104.975; radius = 5.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_rampart_EP1"; distance = 2.53492; angle = 87.2398; height = 0.1; orient = 359.956; };
        class Object2 { type = "Land_CamoNet_NATO_EP1"; distance = 5.82885; angle = 94.1626; height = 0; orient = 539.956; };
        class Object3 { type = "Land_fort_rampart_EP1"; distance = 12.0822; angle = 89.7152; height = -0.0999994; orient = 539.956; };
    };
};


class BWI_Construction_SquadTrench180DesertEast: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "180 Squad Position (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.50093; angle = 20.8022; timer = 7; };
        class Point2 { distance = 5.10194; angle = 159.273; timer = 7; };
        class Point3 { distance = 11.4609; angle = 146.629; timer = 7; };
        class Point4 { distance = 19.0157; angle = 113.965; timer = 7; };
        class Point5 { distance = 20.9434; angle = 89.8758; timer = 7; label = "To Front"; };
        class Point6 { distance = 18.7058; angle = 62.2102; timer = 7; };
        class Point7 { distance = 11.6686; angle = 32.3919; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 10.7068; angle = 90.1803; radius = 9.6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_rampart_EP1"; distance = 2.28675; angle = 85.4502; height = 0.1; orient = 359.956; };
        class Object2 { type = "Land_CamoNet_EAST_EP1"; distance = 5.15471; angle = 94.6564; height = 0; orient = 539.956; };
        class Object3 { type = "Land_CamoNetVar_EAST_EP1"; distance = 12.8412; angle = 91.8881; height = 0.302207; orient = 539.956; };
        class Object4 { type = "Land_fort_artillery_nest_EP1"; distance = 13.1216; angle = 89.2953; height = -0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_SquadTrench180DesertWest: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "180 Squad Position (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.50093; angle = 20.8022; timer = 7; };
        class Point2 { distance = 5.10194; angle = 159.273; timer = 7; };
        class Point3 { distance = 11.4609; angle = 146.629; timer = 7; };
        class Point4 { distance = 19.0157; angle = 113.965; timer = 7; };
        class Point5 { distance = 20.9434; angle = 89.8758; timer = 7; label = "To Front"; };
        class Point6 { distance = 18.7058; angle = 62.2102; timer = 7; };
        class Point7 { distance = 11.6686; angle = 32.3919; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 10.7068; angle = 90.1803; radius = 9.6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_rampart_EP1"; distance = 2.28675; angle = 85.4502; height = 0.1; orient = 359.956; };
        class Object2 { type = "Land_CamoNet_NATO_EP1"; distance = 5.15471; angle = 94.6564; height = 0; orient = 539.956; };
        class Object3 { type = "Land_CamoNetVar_NATO_EP1"; distance = 12.8412; angle = 91.8881; height = 0.302207; orient = 539.956; };
        class Object4 { type = "Land_fort_artillery_nest_EP1"; distance = 13.1216; angle = 89.2953; height = -0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_SquadTrench360DesertEast: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "360 Squad Position (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.25137; angle = 14.721; timer = 9; };
        class Point2 { distance = 4.10755; angle = 161.878; timer = 9; };
        class Point3 { distance = 12.1256; angle = 143.689; timer = 9; };
        class Point4 { distance = 19.9058; angle = 119.317; timer = 9; };
        class Point5 { distance = 23.8152; angle = 99.5751; timer = 9; };
        class Point6 { distance = 23.7829; angle = 79.853; timer = 9; };
        class Point7 { distance = 20.2077; angle = 59.477; timer = 9; };
        class Point8 { distance = 12.2986; angle = 35.6583; timer = 9; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 12.324; angle = 87.0203; radius = 10.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 8.42394; angle = 86.87; height = -0.0999999; orient = 539.956; };
        class Object2 { type = "Land_CamoNetVar_EAST_EP1"; distance = 11.9186; angle = 86.6908; height = 0.302207; orient = 449.956; };
        class Object3 { type = "Land_fort_artillery_nest_EP1"; distance = 16.4757; angle = 87.9721; height = -0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_SquadTrench360DesertWest: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "360 Squad Position (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.25137; angle = 14.721; timer = 9; };
        class Point2 { distance = 4.10755; angle = 161.878; timer = 9; };
        class Point3 { distance = 12.1256; angle = 143.689; timer = 9; };
        class Point4 { distance = 19.9058; angle = 119.317; timer = 9; };
        class Point5 { distance = 23.8152; angle = 99.5751; timer = 9; };
        class Point6 { distance = 23.7829; angle = 79.853; timer = 9; };
        class Point7 { distance = 20.2077; angle = 59.477; timer = 9; };
        class Point8 { distance = 12.2986; angle = 35.6583; timer = 9; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 12.324; angle = 87.0203; radius = 10.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 8.42394; angle = 86.87; height = -0.0999999; orient = 539.956; };
        class Object2 { type = "Land_CamoNetVar_NATO_EP1"; distance = 11.9186; angle = 86.6908; height = 0.302207; orient = 449.956; };
        class Object3 { type = "Land_fort_artillery_nest_EP1"; distance = 16.4757; angle = 87.9721; height = -0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_VehicleTrenchDesert: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Vehicle Position (Desert)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 10.6116; angle = 172.989; timer = 7; };
        class Point2 { distance = 15.0314; angle = 123.061; timer = 7; };
        class Point3 { distance = 16.4654; angle = 89.1122; timer = 7; label = "To Front"; };
        class Point4 { distance = 15.0565; angle = 56.9608; timer = 7; };
        class Point5 { distance = 10.3738; angle = 8.3084; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 8.5962; angle = 135.578; radius = 4; };
        class Area2 { distance = 8.63595; angle = 43.3883; radius = 4; };
        class Area3 { distance = 9.49485; angle = 90.5657; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 8.12989; angle = 90.9946; height = 0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_VehicleTrenchCamoDesertEast: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Camouflaged Vehicle Position (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.44695; angle = 21.4165; timer = 10; };
        class Point2 { distance = 4.24785; angle = 157.875; timer = 10; };
        class Point3 { distance = 15.2097; angle = 136.061; timer = 10; };
        class Point4 { distance = 26.5748; angle = 111.977; timer = 1; label = "Access"; };
        class Point5 { distance = 40.0471; angle = 105.218; timer = 12; };
        class Point6 { distance = 45.9208; angle = 90.1352; timer = 12; label = "To Front"; };
        class Point7 { distance = 40.1338; angle = 74.6174; timer = 12; };
        class Point8 { distance = 26.6316; angle = 67.5445; timer = 1; label = "Access"; };
        class Point9 { distance = 15.991; angle = 41.3134; timer = 10; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 13.5175; angle = 88.3596; radius = 11; };
        class Area2 { distance = 34.8399; angle = 89.8948; radius = 10.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 10.0577; angle = 87.8799; height = 0.3; orient = 539.956; };
        class Object2 { type = "Land_CamoNetB_EAST_EP1"; distance = 11.8457; angle = 89.4969; height = 0.302; orient = 539.956; };
        class Object3 { type = "Land_fort_artillery_nest_EP1"; distance = 37.6173; angle = 89.3589; height = 0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_VehicleTrenchCamoDesertWest: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Camouflaged Vehicle Position (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.44695; angle = 21.4165; timer = 10; };
        class Point2 { distance = 4.24785; angle = 157.875; timer = 10; };
        class Point3 { distance = 15.2097; angle = 136.061; timer = 10; };
        class Point4 { distance = 26.5748; angle = 111.977; timer = 1; label = "Access"; };
        class Point5 { distance = 40.0471; angle = 105.218; timer = 12; };
        class Point6 { distance = 45.9208; angle = 90.1352; timer = 12; label = "To Front"; };
        class Point7 { distance = 40.1338; angle = 74.6174; timer = 12; };
        class Point8 { distance = 26.6316; angle = 67.5445; timer = 1; label = "Access"; };
        class Point9 { distance = 15.991; angle = 41.3134; timer = 10; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 13.5175; angle = 88.3596; radius = 11; };
        class Area2 { distance = 34.8399; angle = 89.8948; radius = 10.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 10.0577; angle = 87.8799; height = 0.3; orient = 539.956; };
        class Object2 { type = "Land_CamoNetB_NATO_EP1"; distance = 11.8457; angle = 89.4969; height = 0.302; orient = 539.956; };
        class Object3 { type = "Land_fort_artillery_nest_EP1"; distance = 37.6173; angle = 89.3589; height = 0.0999999; orient = 359.956; };
    };
};


class BWI_Construction_VehicleTrenchLargeDesertEast: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Fortified Vehicle Position (Desert, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 6.28083; angle = 24.4351; timer = 10; };
        class Point2 { distance = 6.58751; angle = 159.49; timer = 10; };
        class Point3 { distance = 15.9883; angle = 139.512; timer = 10; };
        class Point4 { distance = 23.794; angle = 121.065; timer = 1; label = "Access"; };
        class Point5 { distance = 35.0075; angle = 125.697; timer = 12; };
        class Point6 { distance = 45.2519; angle = 114.586; timer = 12; };
        class Point7 { distance = 45.4929; angle = 100.263; timer = 12; };
        class Point8 { distance = 43.0954; angle = 90.1448; timer = 12; label = "To Front"; };
        class Point9 { distance = 45.569; angle = 80.0036; timer = 12; };
        class Point10 { distance = 45.3561; angle = 65.6399; timer = 12; };
        class Point11 { distance = 34.6488; angle = 54.9323; timer = 12; };
        class Point12 { distance = 24.5778; angle = 60.3002; timer = 1; label = "Access"; };
        class Point13 { distance = 15.9906; angle = 41.3158; timer = 10; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 13.8817; angle = 91.3363; radius = 11; };
        class Area2 { distance = 34.9551; angle = 73.4617; radius = 10.5; };
        class Area3 { distance = 35.0679; angle = 106.435; radius = 10.5; };
        class Area4 { distance = 36.3783; angle = 90.3853; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 10.8769; angle = 92.1585; height = 0.6; orient = 539.956; };
        class Object2 { type = "Land_CamoNetB_EAST_EP1"; distance = 11.7998; angle = 89.6017; height = 0.302; orient = 539.956; };
        class Object3 { type = "Land_fort_artillery_nest_EP1"; distance = 37.5156; angle = 71.2861; height = 0.0999999; orient = 404.956; };
        class Object4 { type = "Land_fort_artillery_nest_EP1"; distance = 37.7729; angle = 108.588; height = 0.0999999; orient = 674.956; };
        class Object5 { type = "Land_fort_rampart_EP1"; distance = 39.3682; angle = 89.9119; height = 0.900001; orient = 539.956; };
    };
};


class BWI_Construction_VehicleTrenchLargeDesertWest: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Fortified Vehicle Position (Desert, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionTrenchKits";
	ace_cargo_size = 6;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_3.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 6.28083; angle = 24.4351; timer = 10; };
        class Point2 { distance = 6.58751; angle = 159.49; timer = 10; };
        class Point3 { distance = 15.9883; angle = 139.512; timer = 10; };
        class Point4 { distance = 23.794; angle = 121.065; timer = 1; label = "Access"; };
        class Point5 { distance = 35.0075; angle = 125.697; timer = 12; };
        class Point6 { distance = 45.2519; angle = 114.586; timer = 12; };
        class Point7 { distance = 45.4929; angle = 100.263; timer = 12; };
        class Point8 { distance = 43.0954; angle = 90.1448; timer = 12; label = "To Front"; };
        class Point9 { distance = 45.569; angle = 80.0036; timer = 12; };
        class Point10 { distance = 45.3561; angle = 65.6399; timer = 12; };
        class Point11 { distance = 34.6488; angle = 54.9323; timer = 12; };
        class Point12 { distance = 24.5778; angle = 60.3002; timer = 1; label = "Access"; };
        class Point13 { distance = 15.9906; angle = 41.3158; timer = 10; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 13.8817; angle = 91.3363; radius = 11; };
        class Area2 { distance = 34.9551; angle = 73.4617; radius = 10.5; };
        class Area3 { distance = 35.0679; angle = 106.435; radius = 10.5; };
        class Area4 { distance = 36.3783; angle = 90.3853; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_fort_artillery_nest_EP1"; distance = 10.8769; angle = 92.1585; height = 0.6; orient = 539.956; };
        class Object2 { type = "Land_CamoNetB_NATO_EP1"; distance = 11.7998; angle = 89.6017; height = 0.302; orient = 539.956; };
        class Object3 { type = "Land_fort_artillery_nest_EP1"; distance = 37.5156; angle = 71.2861; height = 0.0999999; orient = 404.956; };
        class Object4 { type = "Land_fort_artillery_nest_EP1"; distance = 37.7729; angle = 108.588; height = 0.0999999; orient = 674.956; };
        class Object5 { type = "Land_fort_rampart_EP1"; distance = 39.3682; angle = 89.9119; height = 0.900001; orient = 539.956; };
    };
};