/**
 * Light Kits
 */
class BWI_Construction_PortableLightKit: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Portable Light Kit";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.72673; angle = 26.6466; timer = 3; };
        class Point2 { distance = 0.827117; angle = 116.648; timer = 2; };
        class Point3 { distance = 2.27824; angle = 130.062; timer = 3; label = "Generator"; };
        class Point4 { distance = 2.5118; angle = 98.1667; timer = 3; };
        class Point5 { distance = 2.89813; angle = 57.7552; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 1.73132; angle = 107.263; radius = 1; };
        class Area2 { distance = 1.79337; angle = 69.8036; radius = 1; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_PortableLight_single_F"; distance = 1.22695; angle = 88.569; height = 0; orient = 539.711; };
        class Object2 { type = "Land_PortableLight_single_F"; distance = 1.76501; angle = 43.9996; height = 0; orient = 539.711; };
        class Object3 { type = "Land_Portable_generator_F"; distance = 2.01978; angle = 119.025; height = -0.000807762; orient = 450.969; };
        class Object4 { type = "Land_PortableLight_single_F"; distance = 2.03174; angle = 90.0103; height = 0; orient = 539.711; };
        class Object5 { type = "Land_PortableLight_single_F"; distance = 2.37912; angle = 58.6253; height = 0; orient = 539.711; };
    };
};


class BWI_Construction_TallLightKit: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Tall Light Kit";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.30885; angle = 59.6851; timer = 5; label = "Rear"; };
        class Point2 { distance = 1.29886; angle = 121.585; timer = 5; label = "Rear"; };
        class Point3 { distance = 2.42677; angle = 105.725; timer = 5; label = "Front"; };
        class Point4 { distance = 2.45837; angle = 74.7299; timer = 5; label = "Front"; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 1.76044; angle = 89.1776; radius = 0.75; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_LampHalogen_F"; distance = 1.72591; angle = 88.8855; height = 4.76837e-007; orient = 90.0004; };
    };
};


/**
 * Shelter Kits
 */
class BWI_Construction_Shelter2: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Concrete Shelter x2";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.81054; angle = 135.391; timer = 2; label = "Front"; };
        class Point2 { distance = 4.56532; angle = 106.364; timer = 2; label = "Rear"; };
        class Point3 { distance = 4.55687; angle = 75.0599; timer = 2; label = "Rear"; };
        class Point4 { distance = 1.73619; angle = 46.168; timer = 2; label = "Front"; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 2.81837; angle = 89.8288; radius = 2; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_CncShelter_F"; distance = 2.05411; angle = 91.1237; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_CncShelter_F"; distance = 3.61363; angle = 90.8013; height = 0; orient = 360; disableDamage = 1; };
    };
};


class BWI_Construction_Shelter4: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Concrete Shelter x4";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.89369; angle = 147.605; timer = 4; label = "Front"; };
        class Point2 { distance = 5.25928; angle = 117.535; timer = 4; label = "Rear"; };
        class Point3 { distance = 5.3012; angle = 61.4095; timer = 4; label = "Rear"; };
        class Point4 { distance = 2.95775; angle = 31.0461; timer = 4; label = "Front"; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.32496; angle = 109.649; radius = 2; };
        class Area2 { distance = 3.36152; angle = 67.8648; radius = 2; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_CncShelter_F"; distance = 2.63755; angle = 116.975; height = 0; orient = 360; disableDamage = 1; };
        class Object2 { type = "Land_CncShelter_F"; distance = 2.67095; angle = 60.5224; height = 0; orient = 360; disableDamage = 1; };
        class Object3 { type = "Land_CncShelter_F"; distance = 4.08996; angle = 107.145; height = 0; orient = 360; disableDamage = 1; };
        class Object4 { type = "Land_CncShelter_F"; distance = 4.09412; angle = 71.3614; height = 0; orient = 360; disableDamage = 1; };
    };
};


/**
 * Tank Trap Kits
 */
class BWI_Construction_TankTrap4: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Tank Trap x4";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.12794; angle = 142.55; timer = 3; };
        class Point2 { distance = 6.1821; angle = 131.599; timer = 3; };
        class Point3 { distance = 5.22453; angle = 73.9932; timer = 3; };
        class Point4 { distance = 4.425; angle = 21.7967; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 2.11865; angle = 125.396; radius = 0.75; };
        class Area2 { distance = 4.14404; angle = 28.2802; radius = 0.75; };
        class Area3 { distance = 4.71899; angle = 77.1645; radius = 0.75; };
        class Area4 { distance = 5.50649; angle = 131.471; radius = 0.75; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_jezekbeton"; distance = 2.01593; angle = 132.458; height = 0; orient = 375; };
        class Object2 { type = "Land_jezekbeton"; distance = 4.03138; angle = 25.6122; height = 0; orient = 707.026; };
        class Object3 { type = "Land_jezekbeton"; distance = 4.61225; angle = 75.4032; height = 0; orient = 660; };
        class Object4 { type = "Land_jezekbeton"; distance = 5.37243; angle = 131.213; height = 0; orient = 660; };
    };
};


class BWI_Construction_TankTrap8: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Tank Trap x8";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 6.00641; angle = 162.158; timer = 3; };
        class Point2 { distance = 9.83238; angle = 149.903; timer = 3; };
        class Point3 { distance = 6.0158; angle = 125.437; timer = 3; };
        class Point4 { distance = 5.50788; angle = 86.2513; timer = 3; };
        class Point5 { distance = 7.6019; angle = 41.5043; timer = 3; };
        class Point6 { distance = 8.26832; angle = 8.37366; timer = 3; };
        class Point7 { distance = 3.57701; angle = 34.6869; timer = 3; };
        class Point8 { distance = 1.11344; angle = 105.158; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 1.78982; angle = 115.985; radius = 0.75; };
        class Area2 { distance = 3.78712; angle = 41.9127; radius = 0.75; };
        class Area3 { distance = 4.85101; angle = 85.1296; radius = 0.75; };
        class Area4 { distance = 5.52103; angle = 127.229; radius = 0.75; };
        class Area5 { distance = 6.27339; angle = 155.303; radius = 0.75; };
        class Area6 { distance = 6.48736; angle = 39.3158; radius = 0.75; };
        class Area7 { distance = 7.8412; angle = 11.7637; radius = 0.75; };
        class Area8 { distance = 9.30676; angle = 149.639; radius = 0.75; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_jezekbeton"; distance = 1.65146; angle = 116.342; height = 0; orient = 707.026; };
        class Object2 { type = "Land_jezekbeton"; distance = 3.6381; angle = 42.6911; height = 0; orient = 426.018; };
        class Object3 { type = "Land_jezekbeton"; distance = 4.74515; angle = 84.0093; height = 0; orient = 687.327; };
        class Object4 { type = "Land_jezekbeton"; distance = 5.37581; angle = 126.607; height = 0; orient = 660; };
        class Object5 { type = "Land_jezekbeton"; distance = 6.21276; angle = 157.237; height = 0; orient = 375; };
        class Object6 { type = "Land_jezekbeton"; distance = 6.6676; angle = 38.7869; height = 0; orient = 605.632; };
        class Object7 { type = "Land_jezekbeton"; distance = 7.80715; angle = 10.7488; height = 0; orient = 707.026; };
        class Object8 { type = "Land_jezekbeton"; distance = 9.13784; angle = 149.926; height = 0; orient = 660; };
    };
};


class BWI_Construction_DragonTooth4: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Dragon's Tooth x4";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.12794; angle = 142.55; timer = 3; };
        class Point2 { distance = 6.1821; angle = 131.599; timer = 3; };
        class Point3 { distance = 5.22453; angle = 73.9932; timer = 3; };
        class Point4 { distance = 4.425; angle = 21.7967; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 2.11865; angle = 125.396; radius = 0.75; };
        class Area2 { distance = 4.14404; angle = 28.2802; radius = 0.75; };
        class Area3 { distance = 4.71899; angle = 77.1645; radius = 0.75; };
        class Area4 { distance = 5.50649; angle = 131.471; radius = 0.75; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 2.01593; angle = 132.458; height = 0; orient = 375; };
        class Object2 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 4.03138; angle = 25.6122; height = 0; orient = 707.026; };
        class Object3 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 4.61225; angle = 75.4032; height = 0; orient = 660; };
        class Object4 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 5.37243; angle = 131.213; height = 0; orient = 660; };
    };
};


class BWI_Construction_DragonTooth8: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Dragon's Tooth x8";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionExtraKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_6.paa",
		"\bwi_construction_main\data\bwi_color_l.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 6.00641; angle = 162.158; timer = 3; };
        class Point2 { distance = 9.83238; angle = 149.903; timer = 3; };
        class Point3 { distance = 6.0158; angle = 125.437; timer = 3; };
        class Point4 { distance = 5.50788; angle = 86.2513; timer = 3; };
        class Point5 { distance = 7.6019; angle = 41.5043; timer = 3; };
        class Point6 { distance = 8.26832; angle = 8.37366; timer = 3; };
        class Point7 { distance = 3.57701; angle = 34.6869; timer = 3; };
        class Point8 { distance = 1.11344; angle = 105.158; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 1.78982; angle = 115.985; radius = 0.75; };
        class Area2 { distance = 3.78712; angle = 41.9127; radius = 0.75; };
        class Area3 { distance = 4.85101; angle = 85.1296; radius = 0.75; };
        class Area4 { distance = 5.52103; angle = 127.229; radius = 0.75; };
        class Area5 { distance = 6.27339; angle = 155.303; radius = 0.75; };
        class Area6 { distance = 6.48736; angle = 39.3158; radius = 0.75; };
        class Area7 { distance = 7.8412; angle = 11.7637; radius = 0.75; };
        class Area8 { distance = 9.30676; angle = 149.639; radius = 0.75; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 1.65146; angle = 116.342; height = 0; orient = 707.026; };
        class Object2 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 3.6381; angle = 42.6911; height = 0; orient = 426.018; };
        class Object3 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 4.74515; angle = 84.0093; height = 0; orient = 687.327; };
        class Object4 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 5.37581; angle = 126.607; height = 0; orient = 660; };
        class Object5 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 6.21276; angle = 157.237; height = 0; orient = 375; };
        class Object6 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 6.6676; angle = 38.7869; height = 0; orient = 605.632; };
        class Object7 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 7.80715; angle = 10.7488; height = 0; orient = 707.026; };
        class Object8 { type = "Land_DragonsTeeth_01_1x1_new_F"; distance = 9.13784; angle = 149.926; height = 0; orient = 660; };
    };
};