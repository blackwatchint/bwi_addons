/**
 * Sandbag Field Kits
 */
class BWI_Construction_SquadSandbagWoodEast: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.92455; angle = 18.0891; timer = 3; };
        class Point2 { distance = 4.69298; angle = 160.068; timer = 3; };
        class Point3 { distance = 8.74716; angle = 141.661; timer = 1; };
        class Point4 { distance = 10.9808; angle = 120.542; timer = 2; };
        class Point5 { distance = 9.4554; angle = 88.8134; timer = 2; label = "To Front"; };
        class Point6 { distance = 11.4773; angle = 55.3242; timer = 2; };
        class Point7 { distance = 8.71395; angle = 38.459; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.41782; angle = 114.816; radius = 4; };
        class Area2 { distance = 6.50298; angle = 62.5345; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_long_green_F"; distance = 3.3985; angle = 82.2465; height = 0; orient = 539.956; };
        class Object2 { type = "Land_BagFence_01_long_green_F"; distance = 4.11351; angle = 125.57; height = 0; orient = 539.956; };
        class Object3 { type = "Land_BagFence_01_long_green_F"; distance = 4.75059; angle = 45.2915; height = 0; orient = 359.956; };
        class Object4 { type = "Land_CamoNet_EAST"; distance = 5.56898; angle = 90.4019; height = 0; orient = 539.956; };
        class Object5 { type = "Land_BagFence_01_short_green_F"; distance = 5.73712; angle = 144.512; height = 0; orient = 539.956; };
        class Object6 { type = "Land_BagFence_01_short_green_F"; distance = 6.58646; angle = 30.4127; height = 0; orient = 359.956; };
        class Object7 { type = "Land_BagFence_01_end_green_F"; distance = 8.99053; angle = 128.462; height = 0; orient = 449.956; };
        class Object8 { type = "Land_BagFence_01_long_green_F"; distance = 9.11535; angle = 87.1721; height = 0; orient = 539.956; };
        class Object9 { type = "Land_BagFence_01_long_green_F"; distance = 9.39491; angle = 104.811; height = 0; orient = 539.956; };
        class Object10 { type = "Land_BagFence_01_end_green_F"; distance = 9.54369; angle = 47.206; height = 0; orient = 449.956; };
        class Object11 { type = "Land_BagFence_01_long_green_F"; distance = 9.70351; angle = 69.9091; height = 0; orient = 359.956; };
        class Object12 { type = "Land_BagFence_01_round_green_F"; distance = 9.74345; angle = 120.398; height = 0; orient = 494.956; };
        class Object13 { type = "Land_BagFence_01_round_green_F"; distance = 10.2514; angle = 55.3846; height = 0; orient = 584.956; };
    };
};


class BWI_Construction_SquadSandbagWoodWest: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.92455; angle = 18.0891; timer = 3; };
        class Point2 { distance = 4.69298; angle = 160.068; timer = 3; };
        class Point3 { distance = 8.74716; angle = 141.661; timer = 1; };
        class Point4 { distance = 10.9808; angle = 120.542; timer = 2; };
        class Point5 { distance = 9.4554; angle = 88.8134; timer = 2; label = "To Front"; };
        class Point6 { distance = 11.4773; angle = 55.3242; timer = 2; };
        class Point7 { distance = 8.71395; angle = 38.459; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.41782; angle = 114.816; radius = 4; };
        class Area2 { distance = 6.50298; angle = 62.5345; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_long_green_F"; distance = 3.3985; angle = 82.2465; height = 0; orient = 539.956; };
        class Object2 { type = "Land_BagFence_01_long_green_F"; distance = 4.11351; angle = 125.57; height = 0; orient = 539.956; };
        class Object3 { type = "Land_BagFence_01_long_green_F"; distance = 4.75059; angle = 45.2915; height = 0; orient = 359.956; };
        class Object4 { type = "Land_CamoNet_NATO"; distance = 5.56898; angle = 90.4019; height = 0; orient = 539.956; };
        class Object5 { type = "Land_BagFence_01_short_green_F"; distance = 5.73712; angle = 144.512; height = 0; orient = 539.956; };
        class Object6 { type = "Land_BagFence_01_short_green_F"; distance = 6.58646; angle = 30.4127; height = 0; orient = 359.956; };
        class Object7 { type = "Land_BagFence_01_end_green_F"; distance = 8.99053; angle = 128.462; height = 0; orient = 449.956; };
        class Object8 { type = "Land_BagFence_01_long_green_F"; distance = 9.11535; angle = 87.1721; height = 0; orient = 539.956; };
        class Object9 { type = "Land_BagFence_01_long_green_F"; distance = 9.39491; angle = 104.811; height = 0; orient = 539.956; };
        class Object10 { type = "Land_BagFence_01_end_green_F"; distance = 9.54369; angle = 47.206; height = 0; orient = 449.956; };
        class Object11 { type = "Land_BagFence_01_long_green_F"; distance = 9.70351; angle = 69.9091; height = 0; orient = 359.956; };
        class Object12 { type = "Land_BagFence_01_round_green_F"; distance = 9.74345; angle = 120.398; height = 0; orient = 494.956; };
        class Object13 { type = "Land_BagFence_01_round_green_F"; distance = 10.2514; angle = 55.3846; height = 0; orient = 584.956; };
    };
};


class BWI_Construction_SquadSandbagTallWoodEast: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Squad Position (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.92432; angle = 18.09; timer = 3; };
        class Point2 { distance = 4.69307; angle = 160.065; timer = 3; };
        class Point3 { distance = 8.74716; angle = 141.661; timer = 1; };
        class Point4 { distance = 10.9806; angle = 120.543; timer = 4; };
        class Point5 { distance = 9.4554; angle = 88.8134; timer = 2; label = "To Front"; };
        class Point6 { distance = 11.4776; angle = 55.3239; timer = 4; };
        class Point7 { distance = 8.71395; angle = 38.459; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.41782; angle = 114.816; radius = 4; };
        class Area2 { distance = 6.50298; angle = 62.5345; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_long_green_F"; distance = 3.3985; angle = 82.2465; height = 0; orient = 539.956; };
        class Object2 { type = "Land_BagFence_01_long_green_F"; distance = 4.11351; angle = 125.57; height = 0; orient = 539.956; };
        class Object3 { type = "Land_BagFence_01_long_green_F"; distance = 4.75059; angle = 45.2915; height = 0; orient = 359.956; };
        class Object4 { type = "Land_CamoNet_EAST"; distance = 5.56898; angle = 90.4019; height = 0; orient = 539.956; };
        class Object5 { type = "Land_BagFence_01_short_green_F"; distance = 5.73712; angle = 144.512; height = 0; orient = 539.956; };
        class Object6 { type = "Land_BagFence_01_short_green_F"; distance = 6.58646; angle = 30.4127; height = 0; orient = 359.956; };
        class Object7 { type = "Land_BagFence_01_end_green_F"; distance = 8.99053; angle = 128.462; height = 0; orient = 449.956; };
        class Object8 { type = "Land_BagFence_01_long_green_F"; distance = 9.11535; angle = 87.1721; height = 0; orient = 539.956; };
        class Object9 { type = "Land_BagFence_01_short_green_F"; distance = 9.348; angle = 105.313; height = 0.690024; orient = 359.956; };
        class Object10 { type = "Land_BagFence_01_long_green_F"; distance = 9.39491; angle = 104.811; height = 0; orient = 539.956; };
        class Object11 { type = "Land_BagFence_01_end_green_F"; distance = 9.54369; angle = 47.206; height = 0; orient = 449.956; };
        class Object12 { type = "Land_BagFence_01_end_green_F"; distance = 9.58456; angle = 71.7797; height = 0.690024; orient = 539.956; };
        class Object13 { type = "Land_BagFence_01_long_green_F"; distance = 9.70351; angle = 69.9091; height = 0; orient = 359.956; };
        class Object14 { type = "Land_BagFence_01_round_green_F"; distance = 9.69478; angle = 120.515; height = 0.653741; orient = 494.956; };
        class Object15 { type = "Land_BagFence_01_round_green_F"; distance = 9.74345; angle = 120.398; height = 0; orient = 494.956; };
        class Object16 { type = "Land_BagFence_01_end_green_F"; distance = 9.75985; angle = 112.162; height = 0.690024; orient = 539.956; };
        class Object17 { type = "Land_BagFence_01_short_green_F"; distance = 10.0096; angle = 65.1295; height = 0.690024; orient = 359.956; };
        class Object18 { type = "Land_BagFence_01_round_green_F"; distance = 10.2709; angle = 55.4596; height = 0; orient = 584.956; };
        class Object19 { type = "Land_BagFence_01_round_green_F"; distance = 10.2451; angle = 55.2124; height = 0.67585; orient = 584.956; };
    };
};


class BWI_Construction_SquadSandbagTallWoodWest: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Squad Position (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.92432; angle = 18.09; timer = 3; };
        class Point2 { distance = 4.69307; angle = 160.065; timer = 3; };
        class Point3 { distance = 8.74716; angle = 141.661; timer = 1; };
        class Point4 { distance = 10.9806; angle = 120.543; timer = 4; };
        class Point5 { distance = 9.4554; angle = 88.8134; timer = 2; label = "To Front"; };
        class Point6 { distance = 11.4776; angle = 55.3239; timer = 4; };
        class Point7 { distance = 8.71395; angle = 38.459; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.41782; angle = 114.816; radius = 4; };
        class Area2 { distance = 6.50298; angle = 62.5345; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_long_green_F"; distance = 3.3985; angle = 82.2465; height = 0; orient = 539.956; };
        class Object2 { type = "Land_BagFence_01_long_green_F"; distance = 4.11351; angle = 125.57; height = 0; orient = 539.956; };
        class Object3 { type = "Land_BagFence_01_long_green_F"; distance = 4.75059; angle = 45.2915; height = 0; orient = 359.956; };
        class Object4 { type = "Land_CamoNet_NATO"; distance = 5.56898; angle = 90.4019; height = 0; orient = 539.956; };
        class Object5 { type = "Land_BagFence_01_short_green_F"; distance = 5.73712; angle = 144.512; height = 0; orient = 539.956; };
        class Object6 { type = "Land_BagFence_01_short_green_F"; distance = 6.58646; angle = 30.4127; height = 0; orient = 359.956; };
        class Object7 { type = "Land_BagFence_01_end_green_F"; distance = 8.99053; angle = 128.462; height = 0; orient = 449.956; };
        class Object8 { type = "Land_BagFence_01_long_green_F"; distance = 9.11535; angle = 87.1721; height = 0; orient = 539.956; };
        class Object9 { type = "Land_BagFence_01_short_green_F"; distance = 9.348; angle = 105.313; height = 0.690024; orient = 359.956; };
        class Object10 { type = "Land_BagFence_01_long_green_F"; distance = 9.39491; angle = 104.811; height = 0; orient = 539.956; };
        class Object11 { type = "Land_BagFence_01_end_green_F"; distance = 9.54369; angle = 47.206; height = 0; orient = 449.956; };
        class Object12 { type = "Land_BagFence_01_end_green_F"; distance = 9.58456; angle = 71.7797; height = 0.690024; orient = 539.956; };
        class Object13 { type = "Land_BagFence_01_long_green_F"; distance = 9.70351; angle = 69.9091; height = 0; orient = 359.956; };
        class Object14 { type = "Land_BagFence_01_round_green_F"; distance = 9.69478; angle = 120.515; height = 0.653741; orient = 494.956; };
        class Object15 { type = "Land_BagFence_01_round_green_F"; distance = 9.74345; angle = 120.398; height = 0; orient = 494.956; };
        class Object16 { type = "Land_BagFence_01_end_green_F"; distance = 9.75985; angle = 112.162; height = 0.690024; orient = 539.956; };
        class Object17 { type = "Land_BagFence_01_short_green_F"; distance = 10.0096; angle = 65.1295; height = 0.690024; orient = 359.956; };
        class Object18 { type = "Land_BagFence_01_round_green_F"; distance = 10.2709; angle = 55.4596; height = 0; orient = 584.956; };
        class Object19 { type = "Land_BagFence_01_round_green_F"; distance = 10.2451; angle = 55.2124; height = 0.67585; orient = 584.956; };
    };
};


class BWI_Construction_MortarSandbagWood: BWI_Construction_CrateBaseSmall3
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Mortar Position (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 3.67299; angle = 159.124; timer = 2; };
        class Point2 { distance = 6.9106; angle = 142.164; timer = 2; };
        class Point3 { distance = 10.6068; angle = 98.5098; timer = 2; };
        class Point4 { distance = 11.0102; angle = 81.1438; timer = 2; };
        class Point5 { distance = 6.92617; angle = 37.4407; timer = 2; };
        class Point6 { distance = 4.00692; angle = 17.8874; timer = 2; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 4.76556; angle = 49.4461; radius = 2.5; };
        class Area2 { distance = 4.82856; angle = 129.143; radius = 2.5; };
        class Area3 { distance = 5.87942; angle = 89.4504; radius = 5; };
        class Area4 { distance = 9.01774; angle = 88.0839; radius = 2.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_round_green_F"; distance = 3.17452; angle = 45.9971; height = 0; orient = 390.081; };
        class Object2 { type = "Land_BagFence_01_round_green_F"; distance = 3.40705; angle = 135.955; height = 0; orient = 690.765; };
        class Object3 { type = "Land_BagFence_01_round_green_F"; distance = 5.16335; angle = 33.1694; height = 0; orient = 660.663; };
        class Object4 { type = "Land_BagFence_01_round_green_F"; distance = 5.46189; angle = 146.303; height = 0; orient = 421.536; };
        class Object5 { type = "Land_BagFence_01_round_green_F"; distance = 6.2582; angle = 52.1434; height = 0; orient = 569.894; };
        class Object6 { type = "Land_BagFence_01_round_green_F"; distance = 6.45406; angle = 127.2; height = 0; orient = 510.955; };
        class Object7 { type = "Land_BagFence_01_round_green_F"; distance = 9.11766; angle = 78.1124; height = 0; orient = 632.987; };
        class Object8 { type = "Land_BagFence_01_round_green_F"; distance = 9.28466; angle = 97.678; height = 0; orient = 452.796; };
        class Object9 { type = "Land_BagFence_01_round_green_F"; distance = 10.6107; angle = 87.7491; height = 0; orient = 543.572; };
    };
};


class BWI_Construction_BunkerSandbagWood: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Bunker (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.33217; angle = 166.358; timer = 1; };
        class Point2 { distance = 4.68665; angle = 121.601; timer = 2; };
        class Point3 { distance = 7.05669; angle = 107.959; timer = 3; label = "To Front"; };
        class Point4 { distance = 7.01395; angle = 71.7688; timer = 3; label = "To Front"; };
        class Point5 { distance = 4.63352; angle = 59.5788; timer = 2; };
        class Point6 { distance = 4.14702; angle = 12.9825; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.42757; angle = 42.5175; radius = 1.5; };
        class Area2 { distance = 3.47542; angle = 139.304; radius = 1.5; };
        class Area3 { distance = 4.47418; angle = 89.7155; radius = 2.5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_end_green_F"; distance = 4.05071; angle = 19.7881; height = 0; orient = 449.956; };
        class Object2 { type = "Land_BagFence_01_end_green_F"; distance = 4.20456; angle = 158.778; height = 0; orient = 449.956; };
        class Object3 { type = "Land_BagBunker_01_small_green_F"; distance = 4.19862; angle = 90.4498; height = 0; orient = 539.956; };
        class Object4 { type = "Land_BagFence_01_round_green_F"; distance = 4.32415; angle = 42.6456; height = 0; orient = 584.956; };
        class Object5 { type = "Land_BagFence_01_round_green_F"; distance = 4.39137; angle = 137.853; height = 0; orient = 494.956; };
    };
};


class BWI_Construction_BunkerSandbagLargeWood: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Large Bunker (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 3.27784; angle = 150.212; timer = 2; };
        class Point2 { distance = 6.23075; angle = 142.687; timer = 5; };
        class Point3 { distance = 13.8759; angle = 110.864; timer = 5; label = "To Front"; };
        class Point4 { distance = 13.6005; angle = 71.3201; timer = 5; label = "To Front"; };
        class Point5 { distance = 5.95015; angle = 40.9055; timer = 5; };
        class Point6 { distance = 3.53514; angle = 31.5012; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 4.54198; angle = 128.111; radius = 2; };
        class Area2 { distance = 4.59844; angle = 56.6744; radius = 2; };
        class Area3 { distance = 8.28864; angle = 92.1674; radius = 5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_BagFence_01_short_green_F"; distance = 2.6101; angle = 55.314; height = 0; orient = 539.956; };
        class Object2 { type = "Land_BagFence_01_end_green_F"; distance = 2.85461; angle = 47.9327; height = 0.618276; orient = 539.956; };
        class Object3 { type = "Land_BagFence_01_end_green_F"; distance = 3.13385; angle = 141.761; height = 0; orient = 359.956; };
        class Object4 { type = "Land_BagFence_01_round_green_F"; distance = 4.45693; angle = 38.2623; height = 0; orient = 674.956; };
        class Object5 { type = "Land_BagFence_01_round_green_F"; distance = 4.40162; angle = 38.5392; height = 0.721619; orient = 674.956; };
        class Object6 { type = "Land_BagFence_01_round_green_F"; distance = 4.73107; angle = 146.311; height = 0; orient = 404.956; };
        class Object7 { type = "Land_BagFence_01_round_green_F"; distance = 4.73034; angle = 145.752; height = 0.74296; orient = 404.956; };
        class Object8 { type = "Land_BagBunker_01_large_green_F"; distance = 7.75263; angle = 92.0213; height = 0; orient = 539.956; };
    };
};


class BWI_Construction_CheckpointSandbagWoodWest: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Checkpoint (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 8;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.46631; angle = 90.0227; timer = 5; label = "Entrance"; };
        class Point2 { distance = 10.8775; angle = 162.932; timer = 13; label = "Bunker"; };
        class Point3 { distance = 21.1239; angle = 130.187; timer = 1; };
        class Point4 { distance = 30.489; angle = 112.026; timer = 5; };
        class Point5 { distance = 30.4366; angle = 89.727; timer = 5; label = "Exit"; };
        class Point6 { distance = 31.3174; angle = 71.3622; timer = 13; label = "Bunker"; };
        class Point7 { distance = 21.2335; angle = 52.9309; timer = 1; };
        class Point8 { distance = 11.6578; angle = 25.0607; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.53029; angle = 90.1178; radius = 4; };
        class Area2 { distance = 9.90045; angle = 128.157; radius = 6; };
        class Area3 { distance = 10.2244; angle = 55.4015; radius = 5; };
        class Area4 { distance = 16.3704; angle = 91.2169; radius = 8; };
        class Area5 { distance = 25.1849; angle = 106.232; radius = 5; };
        class Area6 { distance = 25.2048; angle = 75.7561; radius = 6; };
        class Area7 { distance = 27.6042; angle = 90.75; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint_US_EP1"; distance = 5.59862; angle = 48.387; height = 0; orient = 11.2724; };
        class Object2 { type = "Sign_Checkpoint_US_EP1"; distance = 6.16811; angle = 130.967; height = 0; orient = 352.113; };
        class Object3 { type = "Land_BarGate_F"; distance = 5.90485; angle = 89.737; height = 0; orient = 0.000402094; disableDamage = 1; };
        class Object4 { type = "Land_BagFence_01_long_green_F"; distance = 7.42994; angle = 43.7166; height = 0; orient = 180; };
        class Object5 { type = "Fort_RazorWire"; distance = 8.56899; angle = 168.468; height = 0; orient = 184.91; };
        class Object6 { type = "Land_BagBunker_01_small_green_F"; distance = 8.71552; angle = 142.863; height = 0; orient = 358.948; };
        class Object7 { type = "Fort_RazorWire"; distance = 8.89674; angle = 20.4699; height = 0; orient = 349.91; };
        class Object8 { type = "Land_BagFence_01_short_green_F"; distance = 9.27404; angle = 33.9222; height = 0; orient = 180; };
        class Object9 { type = "Land_CncBarrier_stripes_F"; distance = 9.28818; angle = 63.0605; height = 0; orient = 269.361; };
        class Object10 { type = "Land_BagFence_01_short_green_F"; distance = 11.1025; angle = 141.21; height = 0; orient = 270; };
        class Object11 { type = "Land_BagFence_01_round_green_F"; distance = 11.264; angle = 30.8443; height = 0; orient = 315; };
        class Object12 { type = "Land_CncBarrier_stripes_F"; distance = 11.6875; angle = 115.421; height = 0; orient = 269.361; };
        class Object13 { type = "Land_BagFence_01_round_green_F"; distance = 11.8339; angle = 132.346; height = 0; orient = 135; };
        class Object14 { type = "Land_BagFence_01_short_green_F"; distance = 12.9405; angle = 36.5616; height = 0; orient = 270; };
        class Object15 { type = "FlagPole_EP1"; distance = 12.9374; angle = 42.2925; height = 0; orient = 270; };
        class Object16 { type = "Land_CncBarrier_stripes_F"; distance = 13.6238; angle = 72.5541; height = 0; orient = 272.574; };
        class Object17 { type = "Land_BagFence_01_end_green_F"; distance = 13.6884; angle = 40.5726; height = 0; orient = 270.001; };
        class Object18 { type = "Fort_RazorWire"; distance = 14.2376; angle = 154.57; height = 0; orient = 79.9096; };
        class Object19 { type = "Fort_RazorWire"; distance = 14.8787; angle = 31.5676; height = 0; orient = 274.91; };
        class Object20 { type = "Land_CncBarrier_stripes_F"; distance = 15.9077; angle = 108.5; height = 0; orient = 85.148; };
        class Object21 { type = "Land_CncBarrier_stripes_F"; distance = 18.2855; angle = 76.9358; height = 0; orient = 265.148; };
        class Object22 { type = "Fort_RazorWire"; distance = 19.9109; angle = 133.453; height = 0; orient = 270.867; };
        class Object23 { type = "Land_CncBarrier_stripes_F"; distance = 20.6775; angle = 103.793; height = 0; orient = 91.6108; };
        class Object24 { type = "Land_CncBarrier_stripes_F"; distance = 22.4216; angle = 79.6303; height = 0; orient = 269.361; };
        class Object25 { type = "Fort_RazorWire"; distance = 22.7546; angle = 54.02; height = 0; orient = 270.867; };
        class Object26 { type = "Land_CncBarrier_stripes_F"; distance = 25.0514; angle = 101.386; height = 0; orient = 94.1421; };
        class Object27 { type = "Land_BagFence_01_round_green_F"; distance = 25.2196; angle = 72.3433; height = 0; orient = 315; };
        class Object28 { type = "FlagPole_EP1"; distance = 26.1902; angle = 112.17; height = 0; orient = 90.0004; };
        class Object29 { type = "Land_BagFence_01_end_green_F"; distance = 26.7128; angle = 114.265; height = 0; orient = 90.0005; };
        class Object30 { type = "Land_BagFence_01_short_green_F"; distance = 27.1328; angle = 72.1176; height = 0; orient = 90.0005; };
        class Object31 { type = "Land_BarGate_F"; distance = 26.9986; angle = 90.9466; height = 0; orient = 180; disableDamage = 1; };
        class Object32 { type = "Land_BagFence_01_short_green_F"; distance = 27.8049; angle = 113.247; height = 0; orient = 90.0004; };
        class Object33 { type = "Sign_Checkpoint_US_EP1"; distance = 28.2046; angle = 82.8306; height = 0; orient = 352.113; };
        class Object34 { type = "Land_BagBunker_01_small_green_F"; distance = 28.2518; angle = 76.6686; height = 0; orient = 178.948; };
        class Object35 { type = "Fort_RazorWire"; distance = 28.5417; angle = 117.513; height = 0; orient = 94.9096; };
        class Object36 { type = "Land_BagFence_01_long_green_F"; distance = 28.7438; angle = 101.948; height = 0; orient = 0.000402094; };
        class Object37 { type = "Land_BagFence_01_short_green_F"; distance = 29.2747; angle = 106.423; height = 0; orient = 0.000402094; };
        class Object38 { type = "Land_BagFence_01_round_green_F"; distance = 29.331; angle = 110.458; height = 0; orient = 135; };
        class Object39 { type = "Fort_RazorWire"; distance = 29.4407; angle = 64.6262; height = 0; orient = 259.91; };
        class Object40 { type = "Sign_Checkpoint_US_EP1"; distance = 29.6333; angle = 97.903; height = 0; orient = 11.2724; };
        class Object41 { type = "Fort_RazorWire"; distance = 31.2682; angle = 106.428; height = 0; orient = 169.91; };
        class Object42 { type = "Fort_RazorWire"; distance = 32.2967; angle = 75.619; height = 0; orient = 4.90955; };
    };
};


class BWI_Construction_CheckpointSandbagWoodEast: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Checkpoint (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionSBFieldKits";
	ace_cargo_size = 8;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

   class BWI_Construction_Build {
        class Point1 { distance = 2.46631; angle = 90.0227; timer = 5; label = "Entrance"; };
        class Point2 { distance = 10.8775; angle = 162.932; timer = 13; label = "Bunker"; };
        class Point3 { distance = 21.1239; angle = 130.187; timer = 1; };
        class Point4 { distance = 30.489; angle = 112.026; timer = 5; };
        class Point5 { distance = 30.4366; angle = 89.727; timer = 5; label = "Exit"; };
        class Point6 { distance = 31.3174; angle = 71.3622; timer = 13; label = "Bunker"; };
        class Point7 { distance = 21.2335; angle = 52.9309; timer = 1; };
        class Point8 { distance = 11.6578; angle = 25.0607; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.53029; angle = 90.1178; radius = 4; };
        class Area2 { distance = 9.90045; angle = 128.157; radius = 6; };
        class Area3 { distance = 10.2244; angle = 55.4015; radius = 5; };
        class Area4 { distance = 16.3704; angle = 91.2169; radius = 8; };
        class Area5 { distance = 25.1849; angle = 106.232; radius = 5; };
        class Area6 { distance = 25.2048; angle = 75.7561; radius = 6; };
        class Area7 { distance = 27.6042; angle = 90.75; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint"; distance = 5.59862; angle = 48.387; height = 0; orient = 11.2724; };
        class Object2 { type = "Sign_Checkpoint"; distance = 6.16811; angle = 130.967; height = 0; orient = 352.113; };
        class Object3 { type = "Land_BarGate_F"; distance = 5.90485; angle = 89.737; height = 0; orient = 0.000402094; disableDamage = 1; };
        class Object4 { type = "Land_BagFence_01_long_green_F"; distance = 7.42994; angle = 43.7166; height = 0; orient = 180; };
        class Object5 { type = "Fort_RazorWire"; distance = 8.56899; angle = 168.468; height = 0; orient = 184.91; };
        class Object6 { type = "Land_BagBunker_01_small_green_F"; distance = 8.71552; angle = 142.863; height = 0; orient = 358.948; };
        class Object7 { type = "Fort_RazorWire"; distance = 8.89674; angle = 20.4699; height = 0; orient = 349.91; };
        class Object8 { type = "Land_BagFence_01_short_green_F"; distance = 9.27404; angle = 33.9222; height = 0; orient = 180; };
        class Object9 { type = "Land_CncBarrier_stripes_F"; distance = 9.28818; angle = 63.0605; height = 0; orient = 269.361; };
        class Object10 { type = "Land_BagFence_01_short_green_F"; distance = 11.1025; angle = 141.21; height = 0; orient = 270; };
        class Object11 { type = "Land_BagFence_01_round_green_F"; distance = 11.264; angle = 30.8443; height = 0; orient = 315; };
        class Object12 { type = "Land_CncBarrier_stripes_F"; distance = 11.6875; angle = 115.421; height = 0; orient = 269.361; };
        class Object13 { type = "Land_BagFence_01_round_green_F"; distance = 11.8339; angle = 132.346; height = 0; orient = 135; };
        class Object14 { type = "Land_BagFence_01_short_green_F"; distance = 12.9405; angle = 36.5616; height = 0; orient = 270; };
        class Object15 { type = "FlagPole_EP1"; distance = 12.9374; angle = 42.2925; height = 0; orient = 270; };
        class Object16 { type = "Land_CncBarrier_stripes_F"; distance = 13.6238; angle = 72.5541; height = 0; orient = 272.574; };
        class Object17 { type = "Land_BagFence_01_end_green_F"; distance = 13.6884; angle = 40.5726; height = 0; orient = 270.001; };
        class Object18 { type = "Fort_RazorWire"; distance = 14.2376; angle = 154.57; height = 0; orient = 79.9096; };
        class Object19 { type = "Fort_RazorWire"; distance = 14.8787; angle = 31.5676; height = 0; orient = 274.91; };
        class Object20 { type = "Land_CncBarrier_stripes_F"; distance = 15.9077; angle = 108.5; height = 0; orient = 85.148; };
        class Object21 { type = "Land_CncBarrier_stripes_F"; distance = 18.2855; angle = 76.9358; height = 0; orient = 265.148; };
        class Object22 { type = "Fort_RazorWire"; distance = 19.9109; angle = 133.453; height = 0; orient = 270.867; };
        class Object23 { type = "Land_CncBarrier_stripes_F"; distance = 20.6775; angle = 103.793; height = 0; orient = 91.6108; };
        class Object24 { type = "Land_CncBarrier_stripes_F"; distance = 22.4216; angle = 79.6303; height = 0; orient = 269.361; };
        class Object25 { type = "Fort_RazorWire"; distance = 22.7546; angle = 54.02; height = 0; orient = 270.867; };
        class Object26 { type = "Land_CncBarrier_stripes_F"; distance = 25.0514; angle = 101.386; height = 0; orient = 94.1421; };
        class Object27 { type = "Land_BagFence_01_round_green_F"; distance = 25.2196; angle = 72.3433; height = 0; orient = 315; };
        class Object28 { type = "FlagPole_EP1"; distance = 26.1902; angle = 112.17; height = 0; orient = 90.0004; };
        class Object29 { type = "Land_BagFence_01_end_green_F"; distance = 26.7128; angle = 114.265; height = 0; orient = 90.0005; };
        class Object30 { type = "Land_BagFence_01_short_green_F"; distance = 27.1328; angle = 72.1176; height = 0; orient = 90.0005; };
        class Object31 { type = "Land_BarGate_F"; distance = 26.9986; angle = 90.9466; height = 0; orient = 180; disableDamage = 1; };
        class Object32 { type = "Land_BagFence_01_short_green_F"; distance = 27.8049; angle = 113.247; height = 0; orient = 90.0004; };
        class Object33 { type = "Sign_Checkpoint"; distance = 28.2046; angle = 82.8306; height = 0; orient = 352.113; };
        class Object34 { type = "Land_BagBunker_01_small_green_F"; distance = 28.2518; angle = 76.6686; height = 0; orient = 178.948; };
        class Object35 { type = "Fort_RazorWire"; distance = 28.5417; angle = 117.513; height = 0; orient = 94.9096; };
        class Object36 { type = "Land_BagFence_01_long_green_F"; distance = 28.7438; angle = 101.948; height = 0; orient = 0.000402094; };
        class Object37 { type = "Land_BagFence_01_short_green_F"; distance = 29.2747; angle = 106.423; height = 0; orient = 0.000402094; };
        class Object38 { type = "Land_BagFence_01_round_green_F"; distance = 29.331; angle = 110.458; height = 0; orient = 135; };
        class Object39 { type = "Fort_RazorWire"; distance = 29.4407; angle = 64.6262; height = 0; orient = 259.91; };
        class Object40 { type = "Sign_Checkpoint"; distance = 29.6333; angle = 97.903; height = 0; orient = 11.2724; };
        class Object41 { type = "Fort_RazorWire"; distance = 31.2682; angle = 106.428; height = 0; orient = 169.91; };
        class Object42 { type = "Fort_RazorWire"; distance = 32.2967; angle = 75.619; height = 0; orient = 4.90955; };
    };
};


/**
 * Barricade Field Kits
 */
class BWI_Construction_SquadBarricadeWoodEast: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.82891; angle = 14.514; timer = 2; };
        class Point2 { distance = 4.89218; angle = 162.673; timer = 2; };
        class Point3 { distance = 8.84565; angle = 143.788; timer = 1; };
        class Point4 { distance = 10.425; angle = 118.986; timer = 2; };
        class Point5 { distance = 9.31952; angle = 87.4175; timer = 3; label = "To Front"; };
        class Point6 { distance = 10.5609; angle = 60.7447; timer = 2; };
        class Point7 { distance = 8.43503; angle = 37.6095; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.15695; angle = 64.7163; radius = 4; };
        class Area2 { distance = 6.52079; angle = 119.165; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_CamoNet_EAST"; distance = 5.3054; angle = 93.1048; height = 0; orient = 539.956; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.56286; angle = 89.1913; height = 0; orient = 359.956; };
        class Object3 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.75034; angle = 101.878; height = 0; orient = 359.956; };
        class Object4 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.79972; angle = 76.7699; height = 0; orient = 359.956; };
        class Object5 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.31656; angle = 122.868; height = 0; orient = 659.956; };
        class Object6 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.33675; angle = 113.52; height = 0; orient = 359.956; };
        class Object7 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.4053; angle = 56.128; height = 0; orient = 419.956; };
        class Object8 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.42628; angle = 65.3194; height = 0; orient = 359.956; };
    };
};


class BWI_Construction_SquadBarricadeWoodWest: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 2;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.82891; angle = 14.514; timer = 2; };
        class Point2 { distance = 4.89218; angle = 162.673; timer = 2; };
        class Point3 { distance = 8.84565; angle = 143.788; timer = 1; };
        class Point4 { distance = 10.425; angle = 118.986; timer = 2; };
        class Point5 { distance = 9.31952; angle = 87.4175; timer = 3; label = "To Front"; };
        class Point6 { distance = 10.5609; angle = 60.7447; timer = 2; };
        class Point7 { distance = 8.43503; angle = 37.6095; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.15695; angle = 64.7163; radius = 4; };
        class Area2 { distance = 6.52079; angle = 119.165; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_CamoNet_NATO"; distance = 5.3054; angle = 93.1048; height = 0; orient = 539.956; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.56286; angle = 89.1913; height = 0; orient = 359.956; };
        class Object3 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.75034; angle = 101.878; height = 0; orient = 359.956; };
        class Object4 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.79972; angle = 76.7699; height = 0; orient = 359.956; };
        class Object5 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.31656; angle = 122.868; height = 0; orient = 659.956; };
        class Object6 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.33675; angle = 113.52; height = 0; orient = 359.956; };
        class Object7 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.4053; angle = 56.128; height = 0; orient = 419.956; };
        class Object8 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.42628; angle = 65.3194; height = 0; orient = 359.956; };
    };
};


class BWI_Construction_SquadBarricadeTallWoodEast: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Squad Position (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.82939; angle = 14.5125; timer = 2; };
        class Point2 { distance = 4.89171; angle = 162.671; timer = 2; };
        class Point3 { distance = 8.84526; angle = 143.786; timer = 1; };
        class Point4 { distance = 10.4248; angle = 118.987; timer = 3; };
        class Point5 { distance = 9.31952; angle = 87.4175; timer = 5; label = "To Front"; };
        class Point6 { distance = 10.5614; angle = 60.7401; timer = 3; };
        class Point7 { distance = 8.43542; angle = 37.6075; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.15716; angle = 64.7122; radius = 4; };
        class Area2 { distance = 6.52055; angle = 119.161; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_CamoNet_EAST"; distance = 5.30537; angle = 93.0995; height = 0; orient = 539.956; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.56287; angle = 89.1881; height = 0; orient = 359.956; };
        class Object3 { type = "Land_SandbagBarricade_01_F"; distance = 8.75029; angle = 101.876; height = 0; orient = 359.956; };
        class Object4 { type = "Land_SandbagBarricade_01_F"; distance = 8.79953; angle = 76.768; height = 0; orient = 359.956; };
        class Object5 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.3163; angle = 122.865; height = 0; orient = 659.956; };
        class Object6 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.33666; angle = 113.519; height = 0; orient = 359.956; };
        class Object7 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.40558; angle = 56.1255; height = 0; orient = 419.956; };
        class Object8 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.42606; angle = 65.3188; height = 0; orient = 359.956; };
    };
};


class BWI_Construction_SquadBarricadeTallWoodWest: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Reinforced Squad Position (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionBBFieldKits";
	ace_cargo_size = 3;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.82939; angle = 14.5125; timer = 2; };
        class Point2 { distance = 4.89171; angle = 162.671; timer = 2; };
        class Point3 { distance = 8.84526; angle = 143.786; timer = 1; };
        class Point4 { distance = 10.4248; angle = 118.987; timer = 3; };
        class Point5 { distance = 9.31952; angle = 87.4175; timer = 5; label = "To Front"; };
        class Point6 { distance = 10.5614; angle = 60.7401; timer = 3; };
        class Point7 { distance = 8.43542; angle = 37.6075; timer = 1; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.15716; angle = 64.7122; radius = 4; };
        class Area2 { distance = 6.52055; angle = 119.161; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_CamoNet_NATO"; distance = 5.30537; angle = 93.0995; height = 0; orient = 539.956; };
        class Object2 { type = "Land_SandbagBarricade_01_half_F"; distance = 8.56287; angle = 89.1881; height = 0; orient = 359.956; };
        class Object3 { type = "Land_SandbagBarricade_01_F"; distance = 8.75029; angle = 101.876; height = 0; orient = 359.956; };
        class Object4 { type = "Land_SandbagBarricade_01_F"; distance = 8.79953; angle = 76.768; height = 0; orient = 359.956; };
        class Object5 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.3163; angle = 122.865; height = 0; orient = 659.956; };
        class Object6 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.33666; angle = 113.519; height = 0; orient = 359.956; };
        class Object7 { type = "Land_SandbagBarricade_01_half_F"; distance = 9.40558; angle = 56.1255; height = 0; orient = 419.956; };
        class Object8 { type = "Land_SandbagBarricade_01_hole_F"; distance = 9.42606; angle = 65.3188; height = 0; orient = 359.956; };
    };
};


/**
 * H-Barrier Field Kits
 */
class BWI_Construction_SquadHescoWoodEast: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.91185; angle = 15.7133; timer = 7; };
        class Point2 { distance = 5.8361; angle = 163.238; timer = 7; };
        class Point3 { distance = 11.3637; angle = 136.493; timer = 3; };
        class Point4 { distance = 13.1721; angle = 115.8; timer = 3; };
        class Point5 { distance = 11.9642; angle = 90.1964; timer = 4; label = "To Front"; };
        class Point6 { distance = 13.1191; angle = 65.1995; timer = 3; };
        class Point7 { distance = 11.2281; angle = 43.3119; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.41397; angle = 65.7636; radius = 5.2; };
        class Area2 { distance = 7.52451; angle = 114.977; radius = 5.2; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_5_green_F"; distance = 3.70663; angle = 137.771; height = 0; orient = 359.956; };
        class Object2 { type = "Land_HBarrier_01_line_5_green_F"; distance = 3.79256; angle = 40.4965; height = 0; orient = 359.956; };
        class Object3 { type = "Land_CamoNet_EAST"; distance = 6.51722; angle = 92.9138; height = 0; orient = 539.956; };
        class Object4 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.1167; angle = 127.11; height = 0; orient = 479.956; };
        class Object5 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.1175; angle = 53.6684; height = 0; orient = 599.956; };
        class Object6 { type = "Land_HBarrier_01_line_5_green_F"; distance = 11.4404; angle = 76.2449; height = 0; orient = 539.956; };
        class Object7 { type = "Land_HBarrier_01_line_5_green_F"; distance = 11.5146; angle = 104.628; height = 0; orient = 539.956; };
    };
};


class BWI_Construction_SquadHescoWoodWest: BWI_Construction_CrateBaseSmall2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Squad Position (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.91185; angle = 15.7133; timer = 7; };
        class Point2 { distance = 5.8361; angle = 163.238; timer = 7; };
        class Point3 { distance = 11.3637; angle = 136.493; timer = 3; };
        class Point4 { distance = 13.1721; angle = 115.8; timer = 3; };
        class Point5 { distance = 11.9642; angle = 90.1964; timer = 4; label = "To Front"; };
        class Point6 { distance = 13.1191; angle = 65.1995; timer = 3; };
        class Point7 { distance = 11.2281; angle = 43.3119; timer = 3; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 7.41397; angle = 65.7636; radius = 5.2; };
        class Area2 { distance = 7.52451; angle = 114.977; radius = 5.2; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_5_green_F"; distance = 3.70663; angle = 137.771; height = 0; orient = 359.956; };
        class Object2 { type = "Land_HBarrier_01_line_5_green_F"; distance = 3.79256; angle = 40.4965; height = 0; orient = 359.956; };
        class Object3 { type = "Land_CamoNet_NATO"; distance = 6.51722; angle = 92.9138; height = 0; orient = 539.956; };
        class Object4 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.1167; angle = 127.11; height = 0; orient = 479.956; };
        class Object5 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.1175; angle = 53.6684; height = 0; orient = 599.956; };
        class Object6 { type = "Land_HBarrier_01_line_5_green_F"; distance = 11.4404; angle = 76.2449; height = 0; orient = 539.956; };
        class Object7 { type = "Land_HBarrier_01_line_5_green_F"; distance = 11.5146; angle = 104.628; height = 0; orient = 539.956; };
    };
};


class BWI_Construction_VehicleHescoWood: BWI_Construction_CrateBaseSmall4
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Vehicle Position (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.71416; angle = 160.917; timer = 5; };
        class Point2 { distance = 12.1088; angle = 112.867; timer = 3; };
        class Point3 { distance = 11.3392; angle = 90.6785; timer = 5; label = "To Front"; };
        class Point4 { distance = 12.2174; angle = 68.7565; timer = 3; };
        class Point5 { distance = 5.69266; angle = 21.0234; timer = 5; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.6786; angle = 89.3947; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_3_green_F"; distance = 6.58004; angle = 48.8195; height = 0; orient = 449.956; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 6.60269; angle = 132.698; height = 0; orient = 449.956; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 10.2385; angle = 90.6695; height = 0; orient = 539.956; };
        class Object4 { type = "Land_HBarrier_01_line_3_green_F"; distance = 10.2885; angle = 68.1342; height = 0; orient = 614.956; };
        class Object5 { type = "Land_HBarrier_01_line_3_green_F"; distance = 10.3073; angle = 113.313; height = 0; orient = 644.956; };
    };
};


class BWI_Construction_VehicleHescoCamoWoodWest: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Camouflaged Vehicle Position (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.83526; angle = 9.95553; timer = 5; };
        class Point2 { distance = 4.95637; angle = 170.502; timer = 5; };
        class Point3 { distance = 13.681; angle = 135.706; timer = 9; };
        class Point4 { distance = 23.6411; angle = 104.263; timer = 7; label = "Access"; };
        class Point5 { distance = 35.7511; angle = 97.4724; timer = 4; };
        class Point6 { distance = 35.3316; angle = 89.9006; timer = 5; label = "To Front"; };
        class Point7 { distance = 35.761; angle = 83.2671; timer = 4; };
        class Point8 { distance = 23.6027; angle = 76.0789; timer = 7; label = "Access"; };
        class Point9 { distance = 13.5892; angle = 45.3938; timer = 9; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 10.3272; angle = 89.1182; radius = 8; };
        class Area2 { distance = 30.6061; angle = 90.2857; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_3_green_F"; distance = 4.38215; angle = 114.509; height = 0; orient = 359.956; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 4.76459; angle = 56.3042; height = 0; orient = 359.956; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 7.7019; angle = 56.1835; height = 0; orient = 449.956; };
        class Object4 { type = "Land_HBarrier_01_line_3_green_F"; distance = 7.80558; angle = 125.959; height = 0; orient = 449.956; };
        class Object5 { type = "Land_CamoNetB_NATO"; distance = 9.08301; angle = 90.0447; height = 0; orient = 539.956; };
        class Object6 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.6569; angle = 69.1129; height = 0; orient = 629.956; };
        class Object7 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.7522; angle = 113.331; height = 0; orient = 629.956; };
        class Object8 { type = "Land_HBarrier_01_line_3_green_F"; distance = 15.8954; angle = 74.6572; height = 0; orient = 449.956; };
        class Object9 { type = "Land_HBarrier_01_line_3_green_F"; distance = 15.9328; angle = 106.858; height = 0; orient = 449.956; };
        class Object10 { type = "Land_HBarrier_01_line_3_green_F"; distance = 29.3105; angle = 98.9319; height = 0; orient = 449.956; };
        class Object11 { type = "Land_HBarrier_01_line_3_green_F"; distance = 29.3654; angle = 81.6607; height = 0; orient = 449.956; };
        class Object12 { type = "Land_HBarrier_01_line_3_green_F"; distance = 33.8242; angle = 97.0516; height = 0; orient = 644.956; };
        class Object13 { type = "Land_HBarrier_01_line_3_green_F"; distance = 33.8604; angle = 83.627; height = 0; orient = 614.956; };
        class Object14 { type = "Land_HBarrier_01_line_3_green_F"; distance = 34.3406; angle = 90.3222; height = 0; orient = 539.956; };
    };
};


class BWI_Construction_VehicleHescoCamoWoodEast: BWI_Construction_CrateBaseSmall1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Camouflaged Vehicle Position (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 4.83526; angle = 9.95553; timer = 5; };
        class Point2 { distance = 4.95637; angle = 170.502; timer = 5; };
        class Point3 { distance = 13.681; angle = 135.706; timer = 9; };
        class Point4 { distance = 23.6411; angle = 104.263; timer = 7; label = "Access"; };
        class Point5 { distance = 35.7511; angle = 97.4724; timer = 4; };
        class Point6 { distance = 35.3316; angle = 89.9006; timer = 5; label = "To Front"; };
        class Point7 { distance = 35.761; angle = 83.2671; timer = 4; };
        class Point8 { distance = 23.6027; angle = 76.0789; timer = 7; label = "Access"; };
        class Point9 { distance = 13.5892; angle = 45.3938; timer = 9; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 10.3272; angle = 89.1182; radius = 8; };
        class Area2 { distance = 30.6061; angle = 90.2857; radius = 6; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_3_green_F"; distance = 4.38215; angle = 114.509; height = 0; orient = 359.956; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 4.76459; angle = 56.3042; height = 0; orient = 359.956; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 7.7019; angle = 56.1835; height = 0; orient = 449.956; };
        class Object4 { type = "Land_HBarrier_01_line_3_green_F"; distance = 7.80558; angle = 125.959; height = 0; orient = 449.956; };
        class Object5 { type = "Land_CamoNetB_EAST"; distance = 9.08301; angle = 90.0447; height = 0; orient = 539.956; };
        class Object6 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.6569; angle = 69.1129; height = 0; orient = 629.956; };
        class Object7 { type = "Land_HBarrier_01_line_3_green_F"; distance = 11.7522; angle = 113.331; height = 0; orient = 629.956; };
        class Object8 { type = "Land_HBarrier_01_line_3_green_F"; distance = 15.8954; angle = 74.6572; height = 0; orient = 449.956; };
        class Object9 { type = "Land_HBarrier_01_line_3_green_F"; distance = 15.9328; angle = 106.858; height = 0; orient = 449.956; };
        class Object10 { type = "Land_HBarrier_01_line_3_green_F"; distance = 29.3105; angle = 98.9319; height = 0; orient = 449.956; };
        class Object11 { type = "Land_HBarrier_01_line_3_green_F"; distance = 29.3654; angle = 81.6607; height = 0; orient = 449.956; };
        class Object12 { type = "Land_HBarrier_01_line_3_green_F"; distance = 33.8242; angle = 97.0516; height = 0; orient = 644.956; };
        class Object13 { type = "Land_HBarrier_01_line_3_green_F"; distance = 33.8604; angle = 83.627; height = 0; orient = 614.956; };
        class Object14 { type = "Land_HBarrier_01_line_3_green_F"; distance = 34.3406; angle = 90.3222; height = 0; orient = 539.956; };
    };
};


class BWI_Construction_MortarHescoWood: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Mortar Position (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 1.65733; angle = 89.3585; timer = 7; };
        class Point2 { distance = 11.3689; angle = 141.107; timer = 6; };
        class Point3 { distance = 19.3689; angle = 115.252; timer = 7; };
        class Point4 { distance = 21.6887; angle = 87.9125; timer = 6; };
        class Point5 { distance = 18.5991; angle = 60.7156; timer = 7; };
        class Point6 { distance = 10.8098; angle = 37.4641; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 11.796; angle = 89.5731; radius = 10; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_1_green_F"; distance = 2.43559; angle = 90.3216; height = 0; orient = 447.625; };
        class Object2 { type = "Land_HBarrier_01_line_5_green_F"; distance = 6.05362; angle = 43.0487; height = 0; orient = 689.896; };
        class Object3 { type = "Land_HBarrier_01_line_5_green_F"; distance = 6.07362; angle = 136.736; height = 0; orient = 389.668; };
        class Object4 { type = "Land_BagFence_01_round_green_F"; distance = 8.76683; angle = 75.8539; height = 0; orient = 390.08; };
        class Object5 { type = "Land_BagFence_01_round_green_F"; distance = 8.94602; angle = 106.306; height = 0; orient = 690.765; };
        class Object6 { type = "Land_BagFence_01_round_green_F"; distance = 9.99519; angle = 64.7807; height = 0; orient = 660.663; };
        class Object7 { type = "Land_BagFence_01_round_green_F"; distance = 10.332; angle = 116.483; height = 0; orient = 421.536; };
        class Object8 { type = "Land_BagFence_01_round_green_F"; distance = 11.7807; angle = 71.2985; height = 0; orient = 569.894; };
        class Object9 { type = "Land_BagFence_01_round_green_F"; distance = 12.0305; angle = 109.242; height = 0; orient = 510.955; };
        class Object10 { type = "Land_HBarrier_01_line_5_green_F"; distance = 14.4865; angle = 53.1085; height = 0; orient = 630.352; };
        class Object11 { type = "Land_BagFence_01_round_green_F"; distance = 15.2481; angle = 83.1622; height = 0; orient = 632.987; };
        class Object12 { type = "Land_HBarrier_01_line_5_green_F"; distance = 15.3069; angle = 124.742; height = 0; orient = 454.966; };
        class Object13 { type = "Land_BagFence_01_round_green_F"; distance = 15.474; angle = 94.833; height = 0; orient = 452.796; };
        class Object14 { type = "Land_BagFence_01_round_green_F"; distance = 16.8238; angle = 88.796; height = 0; orient = 543.571; };
        class Object15 { type = "Land_HBarrier_01_line_1_green_F"; distance = 17.8529; angle = 62.5897; height = 0; orient = 512.48; };
        class Object16 { type = "Land_HBarrier_01_line_1_green_F"; distance = 18.7195; angle = 114.022; height = 0; orient = 394.224; };
        class Object17 { type = "Land_HBarrier_01_line_5_green_F"; distance = 19.5351; angle = 75.7738; height = 0; orient = 392.908; };
        class Object18 { type = "Land_HBarrier_01_line_5_green_F"; distance = 19.9567; angle = 100.329; height = 0; orient = 516.99; };
    };
};


class BWI_Construction_MortarHescoWireWood: BWI_Construction_CrateBaseMedium1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Razorwire Mortar Position (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.13903; angle = 89.3984; timer = 8; };
        class Point2 { distance = 14.3502; angle = 141.512; timer = 7; };
        class Point3 { distance = 24.9015; angle = 115.526; timer = 8; };
        class Point4 { distance = 28.1603; angle = 88.3725; timer = 7; };
        class Point5 { distance = 24.1656; angle = 61.2376; timer = 8; };
        class Point6 { distance = 14.1815; angle = 37.3765; timer = 7; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 15.1431; angle = 89.6527; radius = 13; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_1_green_F"; distance = 5.78272; angle = 90.0967; height = 0; orient = 447.625; };
        class Object2 { type = "Fort_RazorWire"; distance = 6.51412; angle = 26.9511; height = 0; orient = 515.492; };
        class Object3 { type = "Fort_RazorWire"; distance = 6.71811; angle = 150.307; height = 0; orient = 384.089; };
        class Object4 { type = "Land_HBarrier_01_line_5_green_F"; distance = 8.69132; angle = 59.3807; height = 0; orient = 689.896; };
        class Object5 { type = "Land_HBarrier_01_line_5_green_F"; distance = 8.71391; angle = 120.479; height = 0; orient = 389.668; };
        class Object6 { type = "Land_BagFence_01_round_green_F"; distance = 12.0408; angle = 79.7359; height = 0; orient = 390.08; };
        class Object7 { type = "Land_BagFence_01_round_green_F"; distance = 12.1942; angle = 101.873; height = 0; orient = 690.765; };
        class Object8 { type = "Land_BagFence_01_round_green_F"; distance = 13.1024; angle = 71.0141; height = 0; orient = 660.663; };
        class Object9 { type = "Land_BagFence_01_round_green_F"; distance = 13.4099; angle = 110.078; height = 0; orient = 421.536; };
        class Object10 { type = "Fort_RazorWire"; distance = 14.2821; angle = 36.0519; height = 0; orient = 479.993; };
        class Object11 { type = "Fort_RazorWire"; distance = 14.5313; angle = 143.111; height = 0; orient = 421.233; };
        class Object12 { type = "Land_BagFence_01_round_green_F"; distance = 14.9906; angle = 75.3898; height = 0; orient = 569.894; };
        class Object13 { type = "Land_BagFence_01_round_green_F"; distance = 15.2299; angle = 105.078; height = 0; orient = 510.955; };
        class Object14 { type = "Land_HBarrier_01_line_5_green_F"; distance = 17.2827; angle = 59.7744; height = 0; orient = 630.352; };
        class Object15 { type = "Land_HBarrier_01_line_5_green_F"; distance = 18.1562; angle = 118.702; height = 0; orient = 454.966; };
        class Object16 { type = "Land_BagFence_01_round_green_F"; distance = 18.576; angle = 84.3825; height = 0; orient = 632.987; };
        class Object17 { type = "Land_BagFence_01_round_green_F"; distance = 18.8111; angle = 93.9622; height = 0; orient = 452.796; };
        class Object18 { type = "Land_BagFence_01_round_green_F"; distance = 20.1704; angle = 88.9847; height = 0; orient = 543.571; };
        class Object19 { type = "Land_HBarrier_01_line_1_green_F"; distance = 20.8824; angle = 66.8142; height = 0; orient = 512.48; };
        class Object20 { type = "Fort_RazorWire"; distance = 20.9946; angle = 49.944; height = 0; orient = 626.533; };
        class Object21 { type = "Fort_RazorWire"; distance = 21.3733; angle = 128.672; height = 0; orient = 456.22; };
        class Object22 { type = "Land_HBarrier_01_line_1_green_F"; distance = 21.8182; angle = 110.434; height = 0; orient = 394.224; };
        class Object23 { type = "Land_HBarrier_01_line_5_green_F"; distance = 22.7951; angle = 77.8346; height = 0; orient = 392.908; };
        class Object24 { type = "Land_HBarrier_01_line_5_green_F"; distance = 23.2569; angle = 98.8429; height = 0; orient = 516.99; };
        class Object25 { type = "Fort_RazorWire"; distance = 26.9355; angle = 71.9766; height = 0; orient = 398.103; };
        class Object26 { type = "Fort_RazorWire"; distance = 27.7168; angle = 103.453; height = 0; orient = 692.874; };
        class Object27 { type = "Fort_RazorWire"; distance = 28.3202; angle = 87.5098; height = 0; orient = 366.064; };
    };
};


class BWI_Construction_TowerHescoWood: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Tower (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 4;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 5.48344; angle = 163.925; timer = 5; };
        class Point2 { distance = 10.0908; angle = 121.689; timer = 3; };
        class Point3 { distance = 10.9964; angle = 100.5; timer = 8; label = "To Front"; };
        class Point4 { distance = 11.5636; angle = 69.7986; timer = 8; label = "To Front"; };
        class Point5 { distance = 4.20722; angle = 19.8587; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 4.34534; angle = 77.8371; radius = 3; };
        class Area2 { distance = 4.78018; angle = 133.477; radius = 2; };
        class Area3 { distance = 7.65442; angle = 115.793; radius = 2; };
        class Area4 { distance = 8.31831; angle = 83.8706; radius = 3; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_5_green_F"; distance = 6.29296; angle = 136.014; height = 0; orient = 629.956; };
        class Object2 { type = "Land_HBarrier_01_tower_green_F"; distance = 6.24741; angle = 80.6441; height = 0; orient = 539.956; };
        class Object3 { type = "Land_HBarrier_01_line_3_green_F"; distance = 8.71006; angle = 114.365; height = 0; orient = 359.956; };
    };
};


class BWI_Construction_BunkerHescoWood: BWI_Construction_CrateBaseMedium2
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Watchtower Bunker (Woodland)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 5;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.8583; angle = 155.015; timer = 4; };
        class Point2 { distance = 5.81823; angle = 125.032; timer = 7; };
        class Point3 { distance = 11.458; angle = 107.08; timer = 7; label = "To Front"; };
        class Point4 { distance = 11.4899; angle = 72.9692; timer = 7; label = "To Front"; };
        class Point5 { distance = 5.7725; angle = 53.5304; timer = 7; };
        class Point6 { distance = 2.95285; angle = 24.7258; timer = 4; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 3.82159; angle = 88.8359; radius = 3; };
        class Area2 { distance = 7.66589; angle = 89.6825; radius = 4; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Land_HBarrier_01_line_3_green_F"; distance = 3.45232; angle = 58.4928; height = 0; orient = 629.956; };
        class Object2 { type = "Land_HBarrier_01_line_3_green_F"; distance = 3.46406; angle = 120.559; height = 0; orient = 629.956; };
        class Object3 { type = "Land_HBarrier_01_line_1_green_F"; distance = 4.28055; angle = 64.1745; height = 1.236; orient = 367.429; };
        class Object4 { type = "Land_HBarrier_01_line_1_green_F"; distance = 4.31534; angle = 115.573; height = 1.26873; orient = 716.88; };
        class Object5 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 6.4203; angle = 89.6296; height = 0; orient = 539.956; };
    };
};


class BWI_Construction_CheckpointHescoWoodWest: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Checkpoint (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 8;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.18609; angle = 93.342; timer = 8; label = "Entrance"; };
        class Point2 { distance = 12.7045; angle = 166.682; timer = 28; label = "Tower"; };
        class Point3 { distance = 19.6676; angle = 144.276; timer = 1; };
        class Point4 { distance = 31.0429; angle = 121.219; timer = 1; };
        class Point5 { distance = 38.7478; angle = 111.689; timer = 6; };
        class Point6 { distance = 38.2818; angle = 90.2309; timer = 8; label = "Exit"; };
        class Point7 { distance = 40.3167; angle = 71.9731; timer = 28; label = "Tower"; };
        class Point8 { distance = 33.2597; angle = 61.4905; timer = 1; };
        class Point9 { distance = 20.8007; angle = 37.9282; timer = 6; };
        class Point10 { distance = 13.982; angle = 17.7367; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.39667; angle = 86.7268; radius = 5; };
        class Area2 { distance = 11.7822; angle = 135.069; radius = 5; };
        class Area3 { distance = 14.4846; angle = 67.1527; radius = 10; };
        class Area4 { distance = 28.4692; angle = 100.576; radius = 10; };
        class Area5 { distance = 33.7943; angle = 75.5539; radius = 5; };
        class Area6 { distance = 35.97; angle = 89.6936; radius = 5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint_US_EP1"; distance = 3.36513; angle = 32.0089; height = 0; orient = 371.272; };
        class Object2 { type = "Land_CncBarrier_stripes_F"; distance = 3.57124; angle = 43.4239; height = 0; orient = 360.663; };
        class Object3 { type = "Land_CncBarrier_stripes_F"; distance = 4.16484; angle = 80.7845; height = 0; orient = 429.948; };
        class Object4 { type = "Land_CncBarrier_stripes_F"; distance = 7.20622; angle = 88.7809; height = 0; orient = 630.047; };
        class Object5 { type = "Fort_RazorWire"; distance = 8.9474; angle = 162.571; height = 0; orient = 360; };
        class Object6 { type = "Fort_RazorWire"; distance = 9.28543; angle = 20.5539; height = 0; orient = 709.909; };
        class Object7 { type = "Land_HBarrier_01_line_5_green_F"; distance = 10.3856; angle = 32.0391; height = 0; orient = 705.456; };
        class Object8 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 11.6617; angle = 134.383; height = 0; orient = 360.868; };
        class Object9 { type = "Land_CncBarrier_stripes_F"; distance = 13.0242; angle = 112.615; height = 0; orient = 629.384; };
        class Object10 { type = "Sign_Checkpoint_US_EP1"; distance = 14.2729; angle = 105.605; height = 0; orient = 712.113; };
        class Object11 { type = "Fort_RazorWire"; distance = 15.5226; angle = 155.426; height = 0; orient = 428.371; };
        class Object12 { type = "Land_CncBarrier_stripes_F"; distance = 15.6506; angle = 101.304; height = 0; orient = 674.988; };
        class Object13 { type = "Land_CncBarrier_stripes_F"; distance = 16.1322; angle = 75.8083; height = 0; orient = 452.573; };
        class Object14 { type = "Land_HBarrier_01_line_5_green_F"; distance = 16.4459; angle = 36.4337; height = 0; orient = 466.958; };
        class Object15 { type = "Fort_RazorWire"; distance = 16.4877; angle = 27.2769; height = 0; orient = 651.882; };
        class Object16 { type = "Land_CncBarrier_stripes_F"; distance = 18.3273; angle = 98.8846; height = 0; orient = 419.625; };
        class Object17 { type = "Land_CncBarrier_stripes_F"; distance = 19.2778; angle = 77.894; height = 0; orient = 452.573; };
        class Object18 { type = "FlagPole_EP1"; distance = 20.7779; angle = 77.1925; height = 0; orient = 360; };
        class Object19 { type = "Fort_RazorWire"; distance = 21.6939; angle = 138.104; height = 0; orient = 444.786; };
        class Object20 { type = "Land_CncBarrier_stripes_F"; distance = 22.2706; angle = 100.966; height = 0; orient = 447.913; };
        class Object21 { type = "Fort_RazorWire"; distance = 22.4993; angle = 42.9193; height = 0; orient = 633.377; };
        class Object22 { type = "Land_CncBarrier_stripes_F"; distance = 23.5118; angle = 83.0363; height = 0; orient = 599.602; };
        class Object23 { type = "Land_CncBarrier_stripes_F"; distance = 25.5686; angle = 99.5714; height = 0; orient = 452.573; };
        class Object24 { type = "Land_CncBarrier_stripes_F"; distance = 26.1405; angle = 82.6639; height = 0; orient = 494.965; };
        class Object25 { type = "Sign_Checkpoint_US_EP1"; distance = 28.2412; angle = 81.3688; height = 0; orient = 711.616; };
        class Object26 { type = "Land_CncBarrier_stripes_F"; distance = 29.389; angle = 79.9571; height = 0; orient = 449.361; };
        class Object27 { type = "Fort_RazorWire"; distance = 29.8572; angle = 122.785; height = 0; orient = 453.63; };
        class Object28 { type = "Fort_RazorWire"; distance = 31.515; angle = 58.6493; height = 0; orient = 622.014; };
        class Object29 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 33.6784; angle = 75.582; height = 0; orient = 538.948; };
        class Object30 { type = "Land_HBarrier_01_line_5_green_F"; distance = 33.7685; angle = 112.932; height = 0; orient = 638.606; };
        class Object31 { type = "Land_CncBarrier_stripes_F"; distance = 34.1788; angle = 90.3225; height = 0; orient = 450.024; };
        class Object32 { type = "Fort_RazorWire"; distance = 36.4114; angle = 114.4; height = 0; orient = 641.439; };
        class Object33 { type = "Land_HBarrier_01_line_5_green_F"; distance = 36.7856; angle = 104.402; height = 0; orient = 702.922; };
        class Object34 { type = "Land_CncBarrier_stripes_F"; distance = 37.1153; angle = 91.0222; height = 0; orient = 609.925; };
        class Object35 { type = "Fort_RazorWire"; distance = 37.5088; angle = 67.6767; height = 0; orient = 607.232; };
        class Object36 { type = "Land_CncBarrier_stripes_F"; distance = 38.6587; angle = 93.9914; height = 0; orient = 540.64; };
        class Object37 { type = "Fort_RazorWire"; distance = 39.2384; angle = 104.712; height = 0; orient = 528.339; };
        class Object38 { type = "Sign_Checkpoint_US_EP1"; distance = 39.2917; angle = 93.5267; height = 0; orient = 369.573; };
        class Object39 { type = "Fort_RazorWire"; distance = 39.3069; angle = 77.3275; height = 0; orient = 539.977; };
    };
};


class BWI_Construction_CheckpointHescoWoodEast: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Checkpoint (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 8;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.18609; angle = 93.342; timer = 8; label = "Entrance"; };
        class Point2 { distance = 12.7045; angle = 166.682; timer = 28; label = "Tower"; };
        class Point3 { distance = 19.6676; angle = 144.276; timer = 1; };
        class Point4 { distance = 31.0429; angle = 121.219; timer = 1; };
        class Point5 { distance = 38.7478; angle = 111.689; timer = 6; };
        class Point6 { distance = 38.2818; angle = 90.2309; timer = 8; label = "Exit"; };
        class Point7 { distance = 40.3167; angle = 71.9731; timer = 28; label = "Tower"; };
        class Point8 { distance = 33.2597; angle = 61.4905; timer = 1; };
        class Point9 { distance = 20.8007; angle = 37.9282; timer = 6; };
        class Point10 { distance = 13.982; angle = 17.7367; timer = 6; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.39667; angle = 86.7268; radius = 5; };
        class Area2 { distance = 11.7822; angle = 135.069; radius = 5; };
        class Area3 { distance = 14.4846; angle = 67.1527; radius = 10; };
        class Area4 { distance = 28.4692; angle = 100.576; radius = 10; };
        class Area5 { distance = 33.7943; angle = 75.5539; radius = 5; };
        class Area6 { distance = 35.97; angle = 89.6936; radius = 5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint"; distance = 3.36513; angle = 32.0089; height = 0; orient = 371.272; };
        class Object2 { type = "Land_CncBarrier_stripes_F"; distance = 3.57124; angle = 43.4239; height = 0; orient = 360.663; };
        class Object3 { type = "Land_CncBarrier_stripes_F"; distance = 4.16484; angle = 80.7845; height = 0; orient = 429.948; };
        class Object4 { type = "Land_CncBarrier_stripes_F"; distance = 7.20622; angle = 88.7809; height = 0; orient = 630.047; };
        class Object5 { type = "Fort_RazorWire"; distance = 8.9474; angle = 162.571; height = 0; orient = 360; };
        class Object6 { type = "Fort_RazorWire"; distance = 9.28543; angle = 20.5539; height = 0; orient = 709.909; };
        class Object7 { type = "Land_HBarrier_01_line_5_green_F"; distance = 10.3856; angle = 32.0391; height = 0; orient = 705.456; };
        class Object8 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 11.6617; angle = 134.383; height = 0; orient = 360.868; };
        class Object9 { type = "Land_CncBarrier_stripes_F"; distance = 13.0242; angle = 112.615; height = 0; orient = 629.384; };
        class Object10 { type = "Sign_Checkpoint"; distance = 14.2729; angle = 105.605; height = 0; orient = 712.113; };
        class Object11 { type = "Fort_RazorWire"; distance = 15.5226; angle = 155.426; height = 0; orient = 428.371; };
        class Object12 { type = "Land_CncBarrier_stripes_F"; distance = 15.6506; angle = 101.304; height = 0; orient = 674.988; };
        class Object13 { type = "Land_CncBarrier_stripes_F"; distance = 16.1322; angle = 75.8083; height = 0; orient = 452.573; };
        class Object14 { type = "Land_HBarrier_01_line_5_green_F"; distance = 16.4459; angle = 36.4337; height = 0; orient = 466.958; };
        class Object15 { type = "Fort_RazorWire"; distance = 16.4877; angle = 27.2769; height = 0; orient = 651.882; };
        class Object16 { type = "Land_CncBarrier_stripes_F"; distance = 18.3273; angle = 98.8846; height = 0; orient = 419.625; };
        class Object17 { type = "Land_CncBarrier_stripes_F"; distance = 19.2778; angle = 77.894; height = 0; orient = 452.573; };
        class Object18 { type = "FlagPole_EP1"; distance = 20.7779; angle = 77.1925; height = 0; orient = 360; };
        class Object19 { type = "Fort_RazorWire"; distance = 21.6939; angle = 138.104; height = 0; orient = 444.786; };
        class Object20 { type = "Land_CncBarrier_stripes_F"; distance = 22.2706; angle = 100.966; height = 0; orient = 447.913; };
        class Object21 { type = "Fort_RazorWire"; distance = 22.4993; angle = 42.9193; height = 0; orient = 633.377; };
        class Object22 { type = "Land_CncBarrier_stripes_F"; distance = 23.5118; angle = 83.0363; height = 0; orient = 599.602; };
        class Object23 { type = "Land_CncBarrier_stripes_F"; distance = 25.5686; angle = 99.5714; height = 0; orient = 452.573; };
        class Object24 { type = "Land_CncBarrier_stripes_F"; distance = 26.1405; angle = 82.6639; height = 0; orient = 494.965; };
        class Object25 { type = "Sign_Checkpoint"; distance = 28.2412; angle = 81.3688; height = 0; orient = 711.616; };
        class Object26 { type = "Land_CncBarrier_stripes_F"; distance = 29.389; angle = 79.9571; height = 0; orient = 449.361; };
        class Object27 { type = "Fort_RazorWire"; distance = 29.8572; angle = 122.785; height = 0; orient = 453.63; };
        class Object28 { type = "Fort_RazorWire"; distance = 31.515; angle = 58.6493; height = 0; orient = 622.014; };
        class Object29 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 33.6784; angle = 75.582; height = 0; orient = 538.948; };
        class Object30 { type = "Land_HBarrier_01_line_5_green_F"; distance = 33.7685; angle = 112.932; height = 0; orient = 638.606; };
        class Object31 { type = "Land_CncBarrier_stripes_F"; distance = 34.1788; angle = 90.3225; height = 0; orient = 450.024; };
        class Object32 { type = "Fort_RazorWire"; distance = 36.4114; angle = 114.4; height = 0; orient = 641.439; };
        class Object33 { type = "Land_HBarrier_01_line_5_green_F"; distance = 36.7856; angle = 104.402; height = 0; orient = 702.922; };
        class Object34 { type = "Land_CncBarrier_stripes_F"; distance = 37.1153; angle = 91.0222; height = 0; orient = 609.925; };
        class Object35 { type = "Fort_RazorWire"; distance = 37.5088; angle = 67.6767; height = 0; orient = 607.232; };
        class Object36 { type = "Land_CncBarrier_stripes_F"; distance = 38.6587; angle = 93.9914; height = 0; orient = 540.64; };
        class Object37 { type = "Fort_RazorWire"; distance = 39.2384; angle = 104.712; height = 0; orient = 528.339; };
        class Object38 { type = "Sign_Checkpoint"; distance = 39.2917; angle = 93.5267; height = 0; orient = 369.573; };
        class Object39 { type = "Fort_RazorWire"; distance = 39.3069; angle = 77.3275; height = 0; orient = 539.977; };
    };
};


class BWI_Construction_CheckpointHescoHeavyWoodWest: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Fortified Checkpoint (Woodland, West)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 10;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.06837; angle = 89.8377; timer = 8; label = "Entrance"; };
        class Point2 { distance = 12.5478; angle = 167.047; timer = 28; label = "Tower"; };
        class Point3 { distance = 19.494; angle = 144.321; timer = 19; };
        class Point4 { distance = 30.8771; angle = 121.119; timer = 36; };
        class Point5 { distance = 38.5927; angle = 111.567; timer = 9; };
        class Point6 { distance = 38.1675; angle = 90.0301; timer = 8; label = "Exit"; };
        class Point7 { distance = 40.2501; angle = 71.7418; timer = 28; label = "Tower"; };
        class Point8 { distance = 33.2229; angle = 61.1969; timer = 19; };
        class Point9 { distance = 20.8353; angle = 37.458; timer = 36; };
        class Point10 { distance = 14.0746; angle = 17.1299; timer = 9; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.73934; angle = 85.0164; radius = 5; };
        class Area2 { distance = 11.6067; angle = 134.999; radius = 5; };
        class Area3 { distance = 14.432; angle = 66.491; radius = 10; };
        class Area4 { distance = 18.0653; angle = 114.604; radius = 8; };
        class Area5 { distance = 24.263; angle = 71.5135; radius = 8; };
        class Area6 { distance = 28.3327; angle = 100.351; radius = 10; };
        class Area7 { distance = 33.7177; angle = 75.2854; radius = 5; };
        class Area8 { distance = 35.857; angle = 89.4788; radius = 5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint_US_EP1"; distance = 3.42225; angle = 29.2019; height = 0; orient = 371.272; };
        class Object2 { type = "Land_CncBarrier_stripes_F"; distance = 3.59429; angle = 40.6367; height = 0; orient = 360.663; };
        class Object3 { type = "Land_CncBarrier_stripes_F"; distance = 4.0765; angle = 78.6712; height = 0; orient = 429.948; };
        class Object4 { type = "Land_CncBarrier_stripes_F"; distance = 7.09639; angle = 87.6813; height = 0; orient = 630.047; };
        class Object5 { type = "Fort_RazorWire"; distance = 8.78634; angle = 163.02; height = 0; orient = 360; };
        class Object6 { type = "Fort_RazorWire"; distance = 9.37193; angle = 19.6141; height = 0; orient = 709.909; };
        class Object7 { type = "Land_HBarrier_01_big_4_green_F"; distance = 10.44; angle = 31.1202; height = 0; orient = 705.456; };
        class Object8 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 11.487; angle = 134.305; height = 0; orient = 360.868; };
        class Object9 { type = "Land_CncBarrier_stripes_F"; distance = 12.8675; angle = 112.258; height = 0; orient = 629.384; };
        class Object10 { type = "Sign_Checkpoint_US_EP1"; distance = 14.1273; angle = 105.204; height = 0; orient = 712.113; };
        class Object11 { type = "Fort_RazorWire"; distance = 15.3532; angle = 155.605; height = 0; orient = 428.371; };
        class Object12 { type = "Land_CncBarrier_stripes_F"; distance = 15.5128; angle = 100.901; height = 0; orient = 674.988; };
        class Object13 { type = "Land_CncBarrier_stripes_F"; distance = 16.0553; angle = 75.2457; height = 0; orient = 452.573; };
        class Object14 { type = "Fort_RazorWire"; distance = 16.5551; angle = 26.7139; height = 0; orient = 651.882; };
        class Object15 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 17.1398; angle = 42.8135; height = 0; orient = 466.958; };
        class Object16 { type = "Land_CncBarrier_stripes_F"; distance = 18.1943; angle = 98.5223; height = 0; orient = 419.625; };
        class Object17 { type = "Land_HBarrier_01_big_4_green_F"; distance = 18.3511; angle = 137.155; height = 0; orient = 617.083; };
        class Object18 { type = "Land_CncBarrier_stripes_F"; distance = 19.195; angle = 77.4321; height = 0; orient = 452.573; };
        class Object19 { type = "FlagPole_EP1"; distance = 20.6968; angle = 76.764; height = 0; orient = 360; };
        class Object20 { type = "Fort_RazorWire"; distance = 21.5337; angle = 138.508; height = 0; orient = 442.619; };
        class Object21 { type = "Land_CncBarrier_stripes_F"; distance = 22.1335; angle = 100.682; height = 0; orient = 447.913; };
        class Object22 { type = "Fort_RazorWire"; distance = 22.5203; angle = 42.475; height = 0; orient = 633.377; };
        class Object23 { type = "Land_CncBarrier_stripes_F"; distance = 23.4153; angle = 82.6775; height = 0; orient = 599.602; };
        class Object24 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 23.7497; angle = 55.448; height = 0; orient = 451.306; };
        class Object25 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 24.4715; angle = 121.224; height = 0; orient = 634.199; };
        class Object26 { type = "Land_CncBarrier_stripes_F"; distance = 25.4341; angle = 99.3158; height = 0; orient = 452.573; };
        class Object27 { type = "Land_CncBarrier_stripes_F"; distance = 26.0449; angle = 82.3399; height = 0; orient = 494.965; };
        class Object28 { type = "Sign_Checkpoint_US_EP1"; distance = 28.149; angle = 81.0647; height = 0; orient = 711.616; };
        class Object29 { type = "Fort_RazorWire"; distance = 29.2172; angle = 123.319; height = 0; orient = 453.63; };
        class Object30 { type = "Land_CncBarrier_stripes_F"; distance = 29.3005; angle = 79.6607; height = 0; orient = 449.361; };
        class Object31 { type = "Land_HBarrier_01_big_4_green_F"; distance = 31.2645; angle = 63.7835; height = 0; orient = 441.467; };
        class Object32 { type = "Fort_RazorWire"; distance = 31.4878; angle = 58.3335; height = 0; orient = 622.014; };
        class Object33 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 31.6969; angle = 112.731; height = 0; orient = 638.606; };
        class Object34 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 33.6016; angle = 75.3126; height = 0; orient = 538.948; };
        class Object35 { type = "Land_CncBarrier_stripes_F"; distance = 34.0643; angle = 90.0985; height = 0; orient = 450.024; };
        class Object36 { type = "Fort_RazorWire"; distance = 35.9292; angle = 114.673; height = 0; orient = 641.439; };
        class Object37 { type = "Land_HBarrier_01_big_4_green_F"; distance = 36.6423; angle = 104.244; height = 0; orient = 702.922; };
        class Object38 { type = "Land_CncBarrier_stripes_F"; distance = 36.9991; angle = 90.8182; height = 0; orient = 609.925; };
        class Object39 { type = "Fort_RazorWire"; distance = 37.4546; angle = 67.4211; height = 0; orient = 607.232; };
        class Object40 { type = "Land_CncBarrier_stripes_F"; distance = 38.5358; angle = 93.804; height = 0; orient = 540.64; };
        class Object41 { type = "Fort_RazorWire"; distance = 39.0944; angle = 104.566; height = 0; orient = 528.339; };
        class Object42 { type = "Sign_Checkpoint_US_EP1"; distance = 39.1699; angle = 93.3417; height = 0; orient = 369.573; };
        class Object43 { type = "Fort_RazorWire"; distance = 39.2253; angle = 77.1003; height = 0; orient = 539.977; };
    };
};



class BWI_Construction_CheckpointHescoHeavyWoodEast: BWI_Construction_CrateBaseLong1
{
	scope = 2;
    scopeCurator = 2;
	displayName = "Fortified Checkpoint (Woodland, East)";
    editorCategory = "EdCat_BwiConstruction";
    editorSubcategory = "EdSubcat_BwiConstructionHBFieldKits";
	ace_cargo_size = 10;
	
	hiddenSelectionsTextures[] = {
		"\bwi_construction_main\data\con_signs_4.paa",
		"\bwi_construction_main\data\bwi_color_d.paa"
	};

    class BWI_Construction_Build {
        class Point1 { distance = 2.06837; angle = 89.8377; timer = 8; label = "Entrance"; };
        class Point2 { distance = 12.5478; angle = 167.047; timer = 28; label = "Tower"; };
        class Point3 { distance = 19.494; angle = 144.321; timer = 19; };
        class Point4 { distance = 30.8771; angle = 121.119; timer = 36; };
        class Point5 { distance = 38.5927; angle = 111.567; timer = 9; };
        class Point6 { distance = 38.1675; angle = 90.0301; timer = 8; label = "Exit"; };
        class Point7 { distance = 40.2501; angle = 71.7418; timer = 28; label = "Tower"; };
        class Point8 { distance = 33.2229; angle = 61.1969; timer = 19; };
        class Point9 { distance = 20.8353; angle = 37.458; timer = 36; };
        class Point10 { distance = 14.0746; angle = 17.1299; timer = 9; };
    };

    class BWI_Construction_Area {
        class Area1 { distance = 6.73934; angle = 85.0164; radius = 5; };
        class Area2 { distance = 11.6067; angle = 134.999; radius = 5; };
        class Area3 { distance = 14.432; angle = 66.491; radius = 10; };
        class Area4 { distance = 18.0653; angle = 114.604; radius = 8; };
        class Area5 { distance = 24.263; angle = 71.5135; radius = 8; };
        class Area6 { distance = 28.3327; angle = 100.351; radius = 10; };
        class Area7 { distance = 33.7177; angle = 75.2854; radius = 5; };
        class Area8 { distance = 35.857; angle = 89.4788; radius = 5; };
    };

    class BWI_Construction_Composition {
        class Object1 { type = "Sign_Checkpoint"; distance = 3.42225; angle = 29.2019; height = 0; orient = 371.272; };
        class Object2 { type = "Land_CncBarrier_stripes_F"; distance = 3.59429; angle = 40.6367; height = 0; orient = 360.663; };
        class Object3 { type = "Land_CncBarrier_stripes_F"; distance = 4.0765; angle = 78.6712; height = 0; orient = 429.948; };
        class Object4 { type = "Land_CncBarrier_stripes_F"; distance = 7.09639; angle = 87.6813; height = 0; orient = 630.047; };
        class Object5 { type = "Fort_RazorWire"; distance = 8.78634; angle = 163.02; height = 0; orient = 360; };
        class Object6 { type = "Fort_RazorWire"; distance = 9.37193; angle = 19.6141; height = 0; orient = 709.909; };
        class Object7 { type = "Land_HBarrier_01_big_4_green_F"; distance = 10.44; angle = 31.1202; height = 0; orient = 705.456; };
        class Object8 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 11.487; angle = 134.305; height = 0; orient = 360.868; };
        class Object9 { type = "Land_CncBarrier_stripes_F"; distance = 12.8675; angle = 112.258; height = 0; orient = 629.384; };
        class Object10 { type = "Sign_Checkpoint"; distance = 14.1273; angle = 105.204; height = 0; orient = 712.113; };
        class Object11 { type = "Fort_RazorWire"; distance = 15.3532; angle = 155.605; height = 0; orient = 428.371; };
        class Object12 { type = "Land_CncBarrier_stripes_F"; distance = 15.5128; angle = 100.901; height = 0; orient = 674.988; };
        class Object13 { type = "Land_CncBarrier_stripes_F"; distance = 16.0553; angle = 75.2457; height = 0; orient = 452.573; };
        class Object14 { type = "Fort_RazorWire"; distance = 16.5551; angle = 26.7139; height = 0; orient = 651.882; };
        class Object15 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 17.1398; angle = 42.8135; height = 0; orient = 466.958; };
        class Object16 { type = "Land_CncBarrier_stripes_F"; distance = 18.1943; angle = 98.5223; height = 0; orient = 419.625; };
        class Object17 { type = "Land_HBarrier_01_big_4_green_F"; distance = 18.3511; angle = 137.155; height = 0; orient = 617.083; };
        class Object18 { type = "Land_CncBarrier_stripes_F"; distance = 19.195; angle = 77.4321; height = 0; orient = 452.573; };
        class Object19 { type = "FlagPole_EP1"; distance = 20.6968; angle = 76.764; height = 0; orient = 360; };
        class Object20 { type = "Fort_RazorWire"; distance = 21.5337; angle = 138.508; height = 0; orient = 442.619; };
        class Object21 { type = "Land_CncBarrier_stripes_F"; distance = 22.1335; angle = 100.682; height = 0; orient = 447.913; };
        class Object22 { type = "Fort_RazorWire"; distance = 22.5203; angle = 42.475; height = 0; orient = 633.377; };
        class Object23 { type = "Land_CncBarrier_stripes_F"; distance = 23.4153; angle = 82.6775; height = 0; orient = 599.602; };
        class Object24 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 23.7497; angle = 55.448; height = 0; orient = 451.306; };
        class Object25 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 24.4715; angle = 121.224; height = 0; orient = 634.199; };
        class Object26 { type = "Land_CncBarrier_stripes_F"; distance = 25.4341; angle = 99.3158; height = 0; orient = 452.573; };
        class Object27 { type = "Land_CncBarrier_stripes_F"; distance = 26.0449; angle = 82.3399; height = 0; orient = 494.965; };
        class Object28 { type = "Sign_Checkpoint"; distance = 28.149; angle = 81.0647; height = 0; orient = 711.616; };
        class Object29 { type = "Fort_RazorWire"; distance = 29.2172; angle = 123.319; height = 0; orient = 453.63; };
        class Object30 { type = "Land_CncBarrier_stripes_F"; distance = 29.3005; angle = 79.6607; height = 0; orient = 449.361; };
        class Object31 { type = "Land_HBarrier_01_big_4_green_F"; distance = 31.2645; angle = 63.7835; height = 0; orient = 441.467; };
        class Object32 { type = "Fort_RazorWire"; distance = 31.4878; angle = 58.3335; height = 0; orient = 622.014; };
        class Object33 { type = "Land_HBarrier_01_wall_6_green_F"; distance = 31.6969; angle = 112.731; height = 0; orient = 638.606; };
        class Object34 { type = "Land_HBarrier_01_big_tower_green_F"; distance = 33.6016; angle = 75.3126; height = 0; orient = 538.948; };
        class Object35 { type = "Land_CncBarrier_stripes_F"; distance = 34.0643; angle = 90.0985; height = 0; orient = 450.024; };
        class Object36 { type = "Fort_RazorWire"; distance = 35.9292; angle = 114.673; height = 0; orient = 641.439; };
        class Object37 { type = "Land_HBarrier_01_big_4_green_F"; distance = 36.6423; angle = 104.244; height = 0; orient = 702.922; };
        class Object38 { type = "Land_CncBarrier_stripes_F"; distance = 36.9991; angle = 90.8182; height = 0; orient = 609.925; };
        class Object39 { type = "Fort_RazorWire"; distance = 37.4546; angle = 67.4211; height = 0; orient = 607.232; };
        class Object40 { type = "Land_CncBarrier_stripes_F"; distance = 38.5358; angle = 93.804; height = 0; orient = 540.64; };
        class Object41 { type = "Fort_RazorWire"; distance = 39.0944; angle = 104.566; height = 0; orient = 528.339; };
        class Object42 { type = "Sign_Checkpoint"; distance = 39.1699; angle = 93.3417; height = 0; orient = 369.573; };
        class Object43 { type = "Fort_RazorWire"; distance = 39.2253; angle = 77.1003; height = 0; orient = 539.977; };
    };
};