Following is a list of all construction crate classnames defined within `CfgVehicles`. Formatted as a codeblock for easy copy-pasting.

#### Base Kits - DESERT
```
    "BaseWallDesert",
    "BaseRampartDesert",
    "BaseRampartCornerDesert",
    "BaseRampartInvCornerDesert",
    "BaseRampartBunkerDesert",
    "BaseWallCargoDesert",
    "BaseWallCornerBunkerDesert",
    "BaseWallCornerCargoDesert",
    "BaseSideEntranceDesert",
    "BaseMainEntranceBunkerDesert",
    "BaseMainEntranceCargoDesert",
    "BaseFuelBladderDesert",
    "BaseHelipadDesert",
    "BaseCargoHouseDesert",
    "BaseCargoHQDesert",
    "BaseCargoTowerDesert",
```

#### Base Kits - DESERT - WEST
```
    "BaseAmmoDumpDesertWest",
    "BaseRampartAmmoDumpDesertWest",
    "BaseVehicleResupplyDesertWest",
```

#### Base Kits - DESERT - EAST
```
    "BaseAmmoDumpDesertEast",
    "BaseRampartAmmoDumpDesertEast",
    "BaseVehicleResupplyDesertEast",
```

#### Base Kits - WOODLAND
```
    "BaseWallWood",
    "BaseRampartWood",
    "BaseRampartCornerWood",
    "BaseRampartInvCornerWood",
    "BaseRampartBunkerWood",
    "BaseWallCargoWood",
    "BaseWallCornerBunkerWood",
    "BaseWallCornerCargoWood",
    "BaseSideEntranceWood",
    "BaseMainEntranceBunkerWood",
    "BaseMainEntranceCargoWood",
    "BaseFuelBladderWood",
    "BaseHelipadWood",
    "BaseCargoHouseWood",
    "BaseCargoHQWood",
    "BaseCargoTowerWood",
```

#### Base Kits - WOODLAND - WEST
```
    "BaseAmmoDumpWoodWest",
    "BaseRampartAmmoDumpWoodWest",
    "BaseVehicleResupplyWoodWest",
```

#### Base Kits - WOODLAND - EAST
```
    "BaseAmmoDumpWoodEast",
    "BaseRampartAmmoDumpWoodEast",
    "BaseVehicleResupplyWoodEast",
```

#### Sandbag Defence Kits - DESERT
```
    "MortarSandbagDesert",
    "BunkerSandbagDesert",
    "BunkerSandbagLargeDesert",
```

#### Sandbag Defence Kits - DESERT - WEST
```
    "SquadSandbagDesertWest",
    "SquadSandbagTallDesertWest",
    "CheckpointSandbagDesertWest",
```

#### Sandbag Defence Kits - DESERT - EAST
```
    "SquadSandbagDesertEast",
    "SquadSandbagTallDesertEast",
    "CheckpointSandbagDesertEast",
```

#### Sandbag Defence Kits - WOODLAND
```
    "MortarSandbagWood",
    "BunkerSandbagWood",
    "BunkerSandbagLargeWood",
```

#### Sandbag Defence Kits - WOODLAND - WEST
```
    "SquadSandbagWoodWest",
    "SquadSandbagTallWoodWest",
    "CheckpointSandbagWoodWest",
```

#### Sandbag Defence Kits - WOODLAND - EAST
```
    "SquadSandbagWoodEast",
    "SquadSandbagTallWoodEast",
    "CheckpointSandbagWoodEast",
```

#### Hesco Defence Kits - DESERT
```
    "VehicleHescoDesert",
    "MortarHescoDesert",
    "MortarHescoWireDesert",
    "TowerHescoDesert",
    "BunkerHescoDesert",
```

#### Hesco Defence Kits - DESERT - WEST
```
    "SquadHescoDesertWest",
    "VehicleHescoCamoDesertWest",
    "CheckpointHescoDesertWest",
    "CheckpointHescoHeavyDesertWest",
```

#### Hesco Defence Kits - DESERT - EAST
```
    "SquadHescoDesertEast",
    "VehicleHescoCamoDesertEast",
    "CheckpointHescoDesertEast",
    "CheckpointHescoHeavyDesertEast",
```

#### Hesco Defence Kits - WOODLAND
```
    "VehicleHescoWood",
    "MortarHescoWood",
    "MortarHescoWireWood",
    "TowerHescoWood",
    "BunkerHescoWood",
```
    
#### Hesco Defence Kits - WOODLAND - WEST
```
    "SquadHescoWoodWest",
    "VehicleHescoCamoWoodWest",
    "CheckpointHescoWoodWest",
    "CheckpointHescoHeavyWoodWest",
```

#### Hesco Defence Kits - WOODLAND - EAST
```
    "SquadHescoWoodEast",
    "VehicleHescoCamoWoodEast",
    "CheckpointHescoWoodEast",
    "CheckpointHescoHeavyWoodEast",
```

#### Barricade Defence Kits
```
    "BunkerBarricadeSmall",
    "BunkerBarricadeMedium",
    "BunkerBarricadeLarge",
```

#### Barricade Defence Kits - EAST
```
    "CheckpointBarricadeEast",
```

#### Barricade Defence Kits - DESERT - WEST
```
    "SquadBarricadeDesertWest",
    "SquadBarricadeTallDesertWest",
```

#### Barricade Defence Kits - DESERT - EAST
```
    "SquadBarricadeDesertEast",
    "SquadBarricadeTallDesertEast",
```

#### Barricade Defence Kits - WOODLAND - WEST
```
    "SquadBarricadeWoodWest",
    "SquadBarricadeTallWoodWest",
```

#### Barricade Defence Kits - WOODLAND - EAST
```
    "SquadBarricadeWoodEast",
    "SquadBarricadeTallWoodEast",
```

#### Trench Kits - DESERT
```
    "VehicleTrenchDesert",
```

#### Trench Kits - DESERT - WEST
```
    "SquadTrench90DesertWest",
    "SquadTrench180DesertWest",
    "SquadTrench360DesertWest",
    "VehicleTrenchCamoDesertWest",
    "VehicleTrenchLargeDesertWest",
```

#### Trench Kits - DESERT - EAST
```
    "SquadTrench90DesertEast",
    "SquadTrench180DesertEast",
    "SquadTrench360DesertEast",
    "VehicleTrenchCamoDesertEast",
    "VehicleTrenchLargeDesertEast",
```

#### Trench Kits - WOODLAND
```
    "VehicleTrenchWoodland",
```

#### Trench Kits - WOODLAND - WEST
```
    "SquadTrench90WoodWest",
    "SquadTrench180WoodWest",
    "SquadTrench360WoodWest",
    "VehicleTrenchCamoWoodlandWest",
    "VehicleTrenchLargeWoodlandWest",
```

#### Trench Kits - WOODLAND - EAST
```
    "SquadTrench90WoodEast",
    "SquadTrench180WoodEast",
    "SquadTrench360WoodEast",
    "VehicleTrenchCamoWoodlandEast",
    "VehicleTrenchLargeWoodlandEast",
```

#### Extra Kits
```
    "PortableLightKit",
    "TallLightKit",
    "Shelter2",
    "Shelter4",
    "TankTrap4",
    "TankTrap8",
```

#### Tent COPs
```
    "COPTents",
```

#### Sandbag COPs - DESERT - WEST
```
    "COPSandbagDesertWest",
    "COPSandbagTallDesertWest",
```

#### Sandbag COPs - DESERT - EAST
```
    "COPSandbagDesertEast",
    "COPSandbagTallDesertEast",
```

#### Sandbag COPs - WOODLAND - WEST
```
    "COPSandbagWoodWest",
    "COPSandbagTallWoodWest",
```

#### Sandbag COPs - WOODLAND - EAST
```
    "COPSandbagWoodEast",
    "COPSandbagTallWoodEast",
```

#### Hesco COPs - DESERT - WEST
```
    "COPHescoDesertWest",
    "COPHescoHeavyDesertWest",
```

#### Hesco COPs - WOODLAND - WEST
```
    "COPHescoWoodWest",
    "COPHescoHeavyWoodWest",
```

#### Barricade COPs - DESERT - EAST
```
    "COPBarricadeDesertEast",
    "COPBarricadeTallDesertEast",
```

#### Barricade COPs - WOODLAND - EAST
```
    "COPBarricadeWoodEast",
    "COPBarricadeTallWoodEast",
```