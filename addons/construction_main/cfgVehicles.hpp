class BWI_Construction_CrateBaseSmall1;
class BWI_Construction_CrateBaseSmall2;
class BWI_Construction_CrateBaseSmall3;
class BWI_Construction_CrateBaseSmall4;
class BWI_Construction_CrateBaseMedium1;
class BWI_Construction_CrateBaseMedium2;
class BWI_Construction_CrateBaseLong1;

#include <configs\modularbase_desert.hpp>
#include <configs\modularbase_woodland.hpp>
#include <configs\fieldkits_desert.hpp>
#include <configs\fieldkits_woodland.hpp>
#include <configs\fieldkits_neutral.hpp>
#include <configs\trenchkits_desert.hpp>
#include <configs\trenchkits_woodland.hpp>
#include <configs\extras_neutral.hpp>