class CfgPatches
{
    class bwi_menu
    {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.0; 
		authors[] = {"Abel", "Dhorkiy", "Andrew"};
		author = "Black Watch International";
		authorURL = "http://blackwatch-int.com";
        version = 1.2;
        versionStr = "1.2.0";
        versionAr[] = {1,2,0};
    };
};
 
 
class RscStandardDisplay;
class RscDisplayMain: RscStandardDisplay
{
    class Spotlight
    {
        class BlackWatchInternational
        {
            text = "Black Watch International";
            textIsQuote = 0;
            picture = "\bwi_menu\data\main.paa"; // Square picture, ideally 512x512
            video = "\bwi_menu\data\menuvideo.ogv"; // Video played on mouse hover
            action = "connectToServer ['144.76.74.130', 2302, ''];"; //
            actionText =  "Join the server!";
            condition = "true";
        };
		
		class Bootcamp
        {
            text = "What Training badges do you have?";
            textIsQuote = 0;
            picture = "\bwi_menu\data\main.paa"; // Square picture, ideally 512x512
            video = "\bwi_menu\data\menuvideo.ogv"; // Video played on mouse hover
            action = "connectToServer ['144.76.74.130', 2302, ''];"; //
            actionText =  "Speak to Sentinel about future sessions.";
            condition = "true";
        };
       
    };
};
