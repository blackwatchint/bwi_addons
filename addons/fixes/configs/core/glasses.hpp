class G_AirPurifyingRespirator_01_base_F;
class G_AirPurifyingRespirator_02_base_F;
class G_RegulatorMask_base_F;
class G_Blindfold_01_base_F;
class G_Respirator_base_F;
class None;


/**
 * Hides specific facewear from player selection.
 *
 * Date Added: 2020-09-08
 * BWI Issue:  #481
 * Mod Issue:  N/A
 */
class G_AirPurifyingRespirator_01_F: G_AirPurifyingRespirator_01_base_F
{
	scope = 1;
};
class G_AirPurifyingRespirator_02_black_F: G_AirPurifyingRespirator_01_base_F
{
	scope = 1;
};
class G_AirPurifyingRespirator_02_olive_F: G_AirPurifyingRespirator_02_base_F
{
	scope = 1;
};
class G_AirPurifyingRespirator_02_sand_F: G_AirPurifyingRespirator_02_base_F
{
	scope = 1;
};
class G_RegulatorMask_F : G_RegulatorMask_base_F
{
	scope = 1;
};
class G_Blindfold_01_black_F: G_Blindfold_01_base_F
{
	scope = 1;
};
class G_Blindfold_01_white_F: G_Blindfold_01_base_F
{
	scope = 1;
};
class G_Respirator_blue_F: G_Respirator_base_F
{
	scope = 1;
};
class G_Respirator_white_F: G_Respirator_base_F
{
	scope = 1;
};
class G_Respirator_yellow_F: G_Respirator_base_F
{
	scope = 1;
};
class G_Goggles_VR: None
{
	scope = 1;
};