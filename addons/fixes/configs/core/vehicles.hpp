/**
 * Disables the ACE Zeus Suicide Bomber module that would
 * result in low value gameplay.
 *
 * Date Added: 2019-01-30
 * BWI Issue:  #209
 * Mod Issue:  N/A
 */
class ace_zeus_moduleBase;
class ace_zeus_moduleSuicideBomber: ace_zeus_moduleBase
{
    scope = 0;
    scopeCurator = 0;
};


/**
 * Disables the ACE Arsenal modules to prevent confusion 
 * with our module that works in both 3DEN and Zeus.
 *
 * Date Added: 2019-03-25
 * BWI Issue: (Undocumented)
 * Mod Issue: N/A
 */
class ace_zeus_AddFullArsenal: ace_zeus_moduleBase
{
    scope = 0;
    scopeCurator = 0;
};

class ace_zeus_RemoveFullArsenal: ace_zeus_moduleBase
{
    scope = 0;
    scopeCurator = 0;
};

class ace_zeus_AddFullAceArsenal: ace_zeus_moduleBase
{
    scope = 0;
    scopeCurator = 0;
};

class ace_zeus_RemoveFullAceArsenal: ace_zeus_moduleBase
{
    scope = 0;
    scopeCurator = 0;
};


/**
 * Disables the ACE Update Editable Objects module
 * that permits adding of critical logic objects to
 * curator, opening them up for accidental deletion.
 * BWI Utilities Add/Remove Objects/Vehicles modules 
 * provide the same functionality without exposing 
 * critical elements.
 *
 * Date Added: 2019-03-29
 * BWI Issue: (Undocumented)
 * Mod Issue: N/A
 */
class ace_zeus_moduleEditableObjects: ace_zeus_moduleBase
{
    scope = 0;
    scopeCurator = 0;
};


/**
 * Creates a duplicate of the vanilla Truck Boxer and 
 * enables ACE ammo on it.
 *
 * Date Added: 2020-03-07
 * BWI Issue: #369
 * Mod Issue: N/A
 */
class C_Van_01_box_F;
class C_Van_01_box_ammo_F: C_Van_01_box_F
{
    displayName = "Truck Boxer (Ammo)";
    transportAmmo = 0;
    ace_rearm_defaultSupply = 1200;
};