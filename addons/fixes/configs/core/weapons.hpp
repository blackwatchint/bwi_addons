/**
 * Reverts the mass (size) of ACE3 Flashlights to the values
 * before the ACE3 v3.13.0 update.
 *
 * Date Added: 2020-01-28
 * BWI Issue:  #347
 * Mod Issue:  N/A
 */
class ACE_ItemCore;
class CBA_MiscItem_ItemInfo;


/* KSF-1*/
class ACE_Flashlight_KSF1: ACE_ItemCore
{
	class ItemInfo: CBA_MiscItem_ItemInfo
	{
		mass = 1;
	};
};


/* 	Fulton MX-991 */
class ACE_Flashlight_MX991: ACE_ItemCore
{
	class ItemInfo: CBA_MiscItem_ItemInfo
	{
		mass = 1;
	};
};


/* Maglite XL50 */
class ACE_Flashlight_XL50: ACE_ItemCore
{
	class ItemInfo: CBA_MiscItem_ItemInfo
	{
		mass = 1;
	};
};


/**
 * Reduces the size of the DAGR to a sane size.
 *
 * Date Added: 2020-03-01
 * BWI Issue: #255
 * Mod Issue: N/A
 */
class ACE_DAGR: ACE_ItemCore
{
	class ItemInfo: CBA_MiscItem_ItemInfo
	{
		mass = 1;
	};
};