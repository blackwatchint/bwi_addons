class CfgPatches {
    class bwi_fixes {
        requiredVersion = 1;
        authors[] = {"Fourjays", "Redbery", "0mega", "Tebro", "Andrew"};
        authorURL = "http://blackwatch-int.com";
        author = "Black Watch International";
        version = 2.6;
		versionStr = "2.6.0";
		versionAr[] = {2,6,0};
        requiredAddons[] = {
            "ace_flashlights",
			"ace_zeus",
            "ace_medical_gui"
        };
        units[] = {};
    };
};

/**
 * No code should be included in the config.cpp, or any of the files linked directly (e.g. CfgVehicles.hpp).
 * All custom fixes should be located in a configs\mod\file.hpp.
 * The main fix should be documented with a comment, including description, date and issue numbers.
 * Additional code snippets should be documented with relevant issue number for tracking.
 *
 *  When in doubt look at the other fixes as examples.    
 */

class CfgVehicles {
	#include <cfgVehicles.hpp>
};

class CfgWeapons {
	#include <cfgWeapons.hpp>
};

class CfgGlasses {
	#include <cfgGlasses.hpp>
};