/// ACE Advanced Ballistics
force force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force force ace_advanced_ballistics_bulletTraceEnabled = false;
force force ace_advanced_ballistics_enabled = false;
force force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Fatigue
force force ace_advanced_fatigue_enabled = true;
force force ace_advanced_fatigue_enableStaminaBar = true;
ace_advanced_fatigue_fadeStaminaBar = true;
force force ace_advanced_fatigue_loadFactor = 1;
force force ace_advanced_fatigue_performanceFactor = 1;
force force ace_advanced_fatigue_recoveryFactor = 1;
force force ace_advanced_fatigue_swayFactor = 1;
force force ace_advanced_fatigue_terrainGradientFactor = 1;

// ACE Advanced Throwing
force force ace_advanced_throwing_enabled = true;
force force ace_advanced_throwing_enablePickUp = true;
force force ace_advanced_throwing_enablePickUpAttached = true;
//ace_advanced_throwing_showMouseControls = true;
force force ace_advanced_throwing_showThrowArc = true;

// ACE Advanced Vehicle Damage
force force ace_vehicle_damage_enableCarDamage = false;
force force ace_vehicle_damage_enabled = true;
force force ace_vehicle_damage_removeAmmoDuringCookoff = true;

// ACE Arsenal
force force ace_arsenal_allowDefaultLoadouts = true;
force force ace_arsenal_allowSharedLoadouts = true;
//ace_arsenal_camInverted = false;
force force ace_arsenal_enableIdentityTabs = true;
//ace_arsenal_enableModIcons = true;
//ace_arsenal_EnableRPTLog = true;
//ace_arsenal_fontHeight = 4.5;

// ACE Artillery
force force ace_artillerytables_advancedCorrections = false;
force force ace_artillerytables_disableArtilleryComputer = false;
force force ace_mk6mortar_airResistanceEnabled = true;
force force ace_mk6mortar_allowCompass = true;
force force ace_mk6mortar_allowComputerRangefinder = false;
force force ace_mk6mortar_useAmmoHandling = false;

// ACE Captives
force force ace_captives_allowHandcuffOwnSide = true;
force force ace_captives_allowSurrender = true;
force force ace_captives_requireSurrender = 0;
force force ace_captives_requireSurrenderAi = false;

// ACE Common
force force ace_common_allowFadeMusic = true;
force force ace_common_checkPBOsAction = 0;
force force ace_common_checkPBOsCheckAll = false;
force force ace_common_checkPBOsWhitelist = "[]";
//ace_common_displayTextColor = [0,0,0,0.1];
//ace_common_displayTextFontColor = [1,1,1,1];
//ace_common_epilepsyFriendlyMode = false;
//ace_common_progressBarInfo = 2;
//ace_common_settingFeedbackIcons = 1;
//ace_common_settingProgressBarLocation = 0;
force force ace_noradio_enabled = true;

// ACE Cook off
force force ace_cookoff_ammoCookoffDuration = 1;
force force ace_cookoff_enable = 2;
force force ace_cookoff_enableAmmobox = true;
force force ace_cookoff_enableAmmoCookoff = true;
force force ace_cookoff_enableFire = true;
force force ace_cookoff_probabilityCoef = 1;

// ACE Crew Served Weapons
force force ace_csw_ammoHandling = 0;
force force ace_csw_defaultAssemblyMode = false;
force force ace_csw_dragAfterDeploy = false;
force force ace_csw_handleExtraMagazines = false;
force force ace_csw_progressBarTimeCoefficent = 1;

// ACE Dragging
force force ace_dragging_dragAndFire = true;

// ACE Explosives
//ace_explosives_customTimerDefault = 60;
force force ace_explosives_customTimerMax = 900;
force force ace_explosives_customTimerMin = 5;
force force ace_explosives_explodeOnDefuse = true;
force force ace_explosives_punishNonSpecialists = true;
force force ace_explosives_requireSpecialist = true;

// ACE Field Rations
force force acex_field_rations_affectAdvancedFatigue = false;
force force acex_field_rations_enabled = false;
force force acex_field_rations_hudShowLevel = 0;
force force acex_field_rations_hudTransparency = -1;
force force acex_field_rations_hudType = 0;
force force acex_field_rations_hungerSatiated = 10;
force force acex_field_rations_terrainObjectActions = false;
force force acex_field_rations_thirstQuenched = 10;
force force acex_field_rations_timeWithoutFood = 504;
force force acex_field_rations_timeWithoutWater = 168;
force force acex_field_rations_waterSourceActions = 0;

// ACE Fire
force force ace_fire_dropWeapon = 1;
force force ace_fire_enabled = true;
force force ace_fire_enableFlare = false;
//ace_fire_enableScreams = true;

// ACE Fortify
force force ace_fortify_markObjectsOnMap = 1;
force force ace_fortify_timeCostCoefficient = 1;
force force ace_fortify_timeMin = 1.5;
force force acex_fortify_settingHint = 2;

// ACE Fragmentation Simulation
force force ace_frag_enabled = true;
force force ace_frag_maxTrack = 10;
force force ace_frag_maxTrackPerFrame = 10;
force force ace_frag_reflectionsEnabled = false;
force force ace_frag_spallEnabled = false;

// ACE G-Forces
force force ace_gforces_coef = 1;
force force ace_gforces_enabledFor = 1;

// ACE Goggles
force force ace_goggles_effects = 2;
ace_goggles_showClearGlasses = true;
force force ace_goggles_showInThirdPerson = false;

// ACE Grenades
force force ace_grenades_convertExplosives = true;

// ACE Headless
force force acex_headless_delay = 60;
force force acex_headless_enabled = true;
force force acex_headless_endMission = 2;
//force acex_headless_log = false;
force force acex_headless_transferLoadout = 1;

// ACE Hearing
force force ace_hearing_autoAddEarplugsToUnits = false;
force force ace_hearing_disableEarRinging = false;
force force ace_hearing_earplugsVolume = 0.5;
force force ace_hearing_enableCombatDeafness = true;
force force ace_hearing_enabledForZeusUnits = true;
force force ace_hearing_unconsciousnessVolume = 0.4;

// ACE Interaction
force force ace_interaction_disableNegativeRating = true;
force force ace_interaction_enableGroupRenaming = false;
force force ace_interaction_enableMagazinePassing = true;
force force ace_interaction_enableTeamManagement = true;
force force ace_interaction_enableWeaponAttachments = true;
force force ace_interaction_interactWithTerrainObjects = false;

// ACE Interaction Menu
//ace_gestures_showOnInteractionMenu = 1;
//ace_interact_menu_actionOnKeyRelease = true;
//ace_interact_menu_addBuildingActions = false;
//ace_interact_menu_alwaysUseCursorInteraction = true;
//ace_interact_menu_alwaysUseCursorSelfInteraction = true;
//ace_interact_menu_colorShadowMax = [0,0,0,1];
//ace_interact_menu_colorShadowMin = [0,0,0,0.25];
//ace_interact_menu_colorTextMax = [1,1,1,1];
//ace_interact_menu_colorTextMin = [1,1,1,0.25];
//ace_interact_menu_consolidateSingleChild = false;
//ace_interact_menu_cursorKeepCentered = false;
//ace_interact_menu_cursorKeepCenteredSelfInteraction = false;
//ace_interact_menu_menuAnimationSpeed = 2;
//ace_interact_menu_menuBackground = 0;
//ace_interact_menu_menuBackgroundSelf = 0;
//ace_interact_menu_selectorColor = [1,0,0];
//ace_interact_menu_shadowSetting = 2;
//ace_interact_menu_textSize = 2;
//ace_interact_menu_useListMenu = true;
//ace_interact_menu_useListMenuSelf = false;

// ACE Logistics
force force ace_cargo_enable = true;
force force ace_cargo_enableRename = true;
force force ace_cargo_loadTimeCoefficient = 1;
//ace_cargo_openAfterUnload = 1;
force force ace_cargo_paradropTimeCoefficent = 2.5;
force force ace_rearm_distance = 20;
force force ace_rearm_level = 2;
force force ace_rearm_supply = 0;
force force ace_refuel_hoseLength = 20;
force force ace_refuel_rate = 1;
force force ace_repair_addSpareParts = true;
force force ace_repair_autoShutOffEngineWhenStartingRepair = false;
force force ace_repair_consumeItem_toolKit = 0;
//ace_repair_displayTextOnRepair = true;
force force ace_repair_engineerSetting_fullRepair = 2;
force force ace_repair_engineerSetting_repair = 1;
force force ace_repair_engineerSetting_wheel = 0;
force force ace_repair_fullRepairLocation = 4;
force force ace_repair_fullRepairRequiredItems = ["ace_repair_anyToolKit"];
force force ace_repair_locationsBoostTraining = false;
force force ace_repair_miscRepairRequiredItems = ["ace_repair_anyToolKit"];
force force ace_repair_repairDamageThreshold = 0;
force force ace_repair_repairDamageThreshold_engineer = 0;
force force ace_repair_wheelRepairRequiredItems = [];

// ACE Magazine Repack
//ace_magazinerepack_repackLoadedMagazines = true;
force force ace_magazinerepack_timePerAmmo = 1.5;
force force ace_magazinerepack_timePerBeltLink = 8;
force force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force force ace_map_BFT_Enabled = true;
force force ace_map_BFT_HideAiGroups = true;
force force ace_map_BFT_Interval = 3;
force force ace_map_BFT_ShowPlayerNames = false;
force force ace_map_DefaultChannel = 1;
force force ace_map_mapGlow = true;
force force ace_map_mapIllumination = true;
force force ace_map_mapLimitZoom = false;
force force ace_map_mapShake = true;
force force ace_map_mapShowCursorCoordinates = false;
force force ace_markers_moveRestriction = -1;
force force ace_markers_timestampEnabled = true;
force force ace_markers_timestampFormat = "HH:MM";
force force ace_markers_timestampHourFormat = 24;

// ACE Map Gestures
force force ace_map_gestures_allowCurator = true;
force force ace_map_gestures_allowSpectator = true;
force force ace_map_gestures_briefingMode = 0;
//ace_map_gestures_defaultColor = [1,0.88,0,0.7];
//ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force force ace_map_gestures_enabled = true;
force force ace_map_gestures_interval = 0.03;
force force ace_map_gestures_maxRange = 7;
force force ace_map_gestures_maxRangeCamera = 14;
//ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];
force force ace_map_gestures_onlyShowFriendlys = false;

// ACE Map Tools
force force ace_maptools_drawStraightLines = true;
//ace_maptools_rotateModifierKey = 1;

// ACE Medical
force force ace_medical_ai_enabledFor = 2;
force force ace_medical_AIDamageThreshold = 1;
force force ace_medical_bleedingCoefficient = 0.75;
force force ace_medical_blood_bloodLifetime = 900;
force force ace_medical_blood_enabledFor = 2;
force force ace_medical_blood_maxBloodObjects = 500;
force force ace_medical_deathChance = 1;
force force ace_medical_enableVehicleCrashes = true;
force force ace_medical_fatalDamageSource = 2;
//ace_medical_feedback_bloodVolumeEffectType = 2;
//ace_medical_feedback_enableHUDIndicators = true;
//ace_medical_feedback_painEffectType = 0;
force force ace_medical_fractureChance = 0.75;
force force ace_medical_fractures = 2;
//ace_medical_gui_bloodLossColor_0 = [1,1,1,1];
//ace_medical_gui_bloodLossColor_1 = [1,0.95,0.64,1];
//ace_medical_gui_bloodLossColor_2 = [1,0.87,0.46,1];
//ace_medical_gui_bloodLossColor_3 = [1,0.8,0.33,1];
//ace_medical_gui_bloodLossColor_4 = [1,0.72,0.24,1];
//ace_medical_gui_bloodLossColor_5 = [1,0.63,0.15,1];
//ace_medical_gui_bloodLossColor_6 = [1,0.54,0.08,1];
//ace_medical_gui_bloodLossColor_7 = [1,0.43,0.02,1];
//ace_medical_gui_bloodLossColor_8 = [1,0.3,0,1];
//ace_medical_gui_bloodLossColor_9 = [1,0,0,1];
//ace_medical_gui_damageColor_0 = [1,1,1,1];
//ace_medical_gui_damageColor_1 = [0.75,0.95,1,1];
//ace_medical_gui_damageColor_2 = [0.62,0.86,1,1];
//ace_medical_gui_damageColor_3 = [0.54,0.77,1,1];
//ace_medical_gui_damageColor_4 = [0.48,0.67,1,1];
//ace_medical_gui_damageColor_5 = [0.42,0.57,1,1];
//ace_medical_gui_damageColor_6 = [0.37,0.47,1,1];
//ace_medical_gui_damageColor_7 = [0.31,0.36,1,1];
//ace_medical_gui_damageColor_8 = [0.22,0.23,1,1];
//ace_medical_gui_damageColor_9 = [0,0,1,1];
//ace_medical_gui_enableActions = 0;
//ace_medical_gui_enableMedicalMenu = 1;
//ace_medical_gui_enableSelfActions = true;
force force ace_medical_gui_interactionMenuShowTriage = 1;
force force ace_medical_gui_maxDistance = 3;
//ace_medical_gui_openAfterTreatment = true;
force force ace_medical_ivFlowRate = 1;
force force ace_medical_limping = 1;
force force ace_medical_painCoefficient = 1;
force force ace_medical_painUnconsciousChance = 0.1;
force force ace_medical_playerDamageThreshold = 1;
force force ace_medical_spontaneousWakeUpChance = 0.1;
force force ace_medical_spontaneousWakeUpEpinephrineBoost = 5;
force force ace_medical_statemachine_AIUnconsciousness = true;
force force ace_medical_statemachine_cardiacArrestBleedoutEnabled = true;
force force ace_medical_statemachine_cardiacArrestTime = 150;
force force ace_medical_statemachine_fatalInjuriesAI = 0;
force force ace_medical_statemachine_fatalInjuriesPlayer = 0;
force force ace_medical_treatment_advancedBandages = 2;
force force ace_medical_treatment_advancedDiagnose = 1;
force force ace_medical_treatment_advancedMedication = true;
force force ace_medical_treatment_allowBodyBagUnconscious = true;
force force ace_medical_treatment_allowLitterCreation = true;
force force ace_medical_treatment_allowSelfIV = 1;
force force ace_medical_treatment_allowSelfPAK = 0;
force force ace_medical_treatment_allowSelfStitch = 0;
force force ace_medical_treatment_allowSharedEquipment = 0;
force force ace_medical_treatment_clearTrauma = 1;
force force ace_medical_treatment_consumePAK = 0;
force force ace_medical_treatment_consumeSurgicalKit = 0;
force force ace_medical_treatment_convertItems = 0;
force force ace_medical_treatment_cprSuccessChanceMax = 1;
force force ace_medical_treatment_cprSuccessChanceMin = 1;
force force ace_medical_treatment_holsterRequired = 0;
force force ace_medical_treatment_litterCleanupDelay = 600;
force force ace_medical_treatment_locationEpinephrine = 0;
force force ace_medical_treatment_locationIV = 0;
force force ace_medical_treatment_locationPAK = 3;
force force ace_medical_treatment_locationsBoostTraining = false;
force force ace_medical_treatment_locationSurgicalKit = 0;
force force ace_medical_treatment_maxLitterObjects = 500;
force force ace_medical_treatment_medicEpinephrine = 0;
force force ace_medical_treatment_medicIV = 1;
force force ace_medical_treatment_medicPAK = 1;
force force ace_medical_treatment_medicSurgicalKit = 1;
force force ace_medical_treatment_timeCoefficientPAK = 1;
force force ace_medical_treatment_treatmentTimeAutoinjector = 5;
force force ace_medical_treatment_treatmentTimeBodyBag = 15;
force force ace_medical_treatment_treatmentTimeCPR = 30;
force force ace_medical_treatment_treatmentTimeIV = 12;
force force ace_medical_treatment_treatmentTimeSplint = 15;
force force ace_medical_treatment_treatmentTimeTourniquet = 5;
force force ace_medical_treatment_woundReopenChance = 1;
force force ace_medical_treatment_woundStitchTime = 5;

// ACE Name Tags
force force ace_nametags_ambientBrightnessAffectViewDist = 1;
//ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
//ace_nametags_nametagColorBlue = [0.5,0.5,1,1];
//ace_nametags_nametagColorGreen = [0.5,1,0.5,1];
//ace_nametags_nametagColorMain = [1,1,1,1];
//ace_nametags_nametagColorRed = [1,0.5,0.5,1];
//ace_nametags_nametagColorYellow = [1,1,0.5,1];
force force ace_nametags_playerNamesMaxAlpha = 0.8;
force force ace_nametags_playerNamesViewDistance = 5;
force force ace_nametags_showCursorTagForVehicles = false;
force force ace_nametags_showNamesForAI = false;
force force ace_nametags_showPlayerNames = 1;
force force ace_nametags_showPlayerRanks = true;
force force ace_nametags_showSoundWaves = 1;
force force ace_nametags_showVehicleCrewInfo = false;
//ace_nametags_tagSize = 2;

// ACE Nightvision
force force ace_nightvision_aimDownSightsBlur = 1;
force force ace_nightvision_disableNVGsWithSights = false;
force force ace_nightvision_effectScaling = 0.5;
force force ace_nightvision_fogScaling = 0.8;
force force ace_nightvision_noiseScaling = 0.7;
force force ace_nightvision_shutterEffects = true;

// ACE Overheating
force force ace_overheating_cookoffCoef = 0;
force force ace_overheating_coolingCoef = 1;
//ace_overheating_displayTextOnJam = true;
force force ace_overheating_enabled = true;
force force ace_overheating_heatCoef = 1;
force force ace_overheating_jamChanceCoef = 1.5;
force force ace_overheating_overheatingDispersion = true;
force force ace_overheating_overheatingRateOfFire = true;
//ace_overheating_particleEffectsAndDispersionDistance = 3000;
//ace_overheating_showParticleEffects = true;
//ace_overheating_showParticleEffectsForEveryone = true;
force force ace_overheating_suppressorCoef = 1;
force force ace_overheating_unJamFailChance = 0.1;
force force ace_overheating_unJamOnreload = false;
force force ace_overheating_unJamOnSwapBarrel = false;

// ACE Pointing
force force ace_finger_enabled = true;
//ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
//ace_finger_indicatorForSelf = true;
force force ace_finger_maxRange = 4;

// ACE Pylons
force force ace_pylons_enabledForZeus = true;
force force ace_pylons_enabledFromAmmoTrucks = false;
force force ace_pylons_rearmNewPylons = false;
force force ace_pylons_requireEngineer = false;
force force ace_pylons_requireToolkit = false;
force force ace_pylons_searchDistance = 15;
force force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force force ace_quickmount_distance = 3;
force force ace_quickmount_enabled = true;
//ace_quickmount_enableMenu = 3;
//ace_quickmount_priority = 3;
force force ace_quickmount_speed = 5;

// ACE Respawn
force force ace_respawn_removeDeadBodiesDisconnected = false;
force force ace_respawn_savePreDeathGear = false;

// ACE Scopes
force force ace_scopes_correctZeroing = true;
force force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force force ace_scopes_defaultZeroRange = 100;
force force ace_scopes_enabled = true;
force force ace_scopes_forceUseOfAdjustmentTurrets = true;
force force ace_scopes_overwriteZeroRange = false;
force force ace_scopes_simplifiedZeroing = false;
force force ace_scopes_useLegacyUI = false;
force force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force force ace_scopes_zeroReferenceHumidity = 0.5;
force force ace_scopes_zeroReferenceTemperature = 15;

// ACE Sitting
force force acex_sitting_enable = true;

// ACE Spectator
force force ace_spectator_enableAI = false;
force force ace_spectator_maxFollowDistance = 5;
force force ace_spectator_restrictModes = 3;
force force ace_spectator_restrictVisions = 1;

// ACE Switch Units
force force ace_switchunits_enableSafeZone = true;
force force ace_switchunits_enableSwitchUnits = false;
force force ace_switchunits_safeZoneRadius = 100;
force force ace_switchunits_switchToCivilian = false;
force force ace_switchunits_switchToEast = false;
force force ace_switchunits_switchToIndependent = false;
force force ace_switchunits_switchToWest = false;

// ACE Trenches
force force ace_trenches_bigEnvelopeDigDuration = 25;
force force ace_trenches_bigEnvelopeRemoveDuration = 15;
force force ace_trenches_smallEnvelopeDigDuration = 20;
force force ace_trenches_smallEnvelopeRemoveDuration = 12;

// ACE Uncategorized
force force ace_fastroping_requireRopeItems = false;
force force ace_gunbag_swapGunbagEnabled = true;
force force ace_hitreactions_minDamageToTrigger = 0.1;
//ace_inventory_inventoryDisplaySize = 0;
force force ace_laser_dispersionCount = 2;
force force ace_microdagr_mapDataAvailable = 2;
force force ace_microdagr_waypointPrecision = 3;
//ace_optionsmenu_showNewsOnMainMenu = true;
force force ace_overpressure_distanceCoefficient = 1;
force force ace_parachute_failureChance = 0;
force force ace_parachute_hideAltimeter = true;
force force ace_tagging_quickTag = 1;

// ACE User Interface
force force ace_ui_allowSelectiveUI = false;
force force ace_ui_ammoCount = false;
force force ace_ui_ammoType = true;
force force ace_ui_commandMenu = false;
force force ace_ui_enableSpeedIndicator = true;
force force ace_ui_firingMode = true;
force force ace_ui_groupBar = false;
force force ace_ui_gunnerAmmoCount = false;
force force ace_ui_gunnerAmmoType = true;
force force ace_ui_gunnerFiringMode = true;
force force ace_ui_gunnerLaunchableCount = true;
force force ace_ui_gunnerLaunchableName = true;
force force ace_ui_gunnerMagCount = true;
force force ace_ui_gunnerWeaponLowerInfoBackground = true;
force force ace_ui_gunnerWeaponName = true;
force force ace_ui_gunnerWeaponNameBackground = true;
force force ace_ui_gunnerZeroing = true;
force force ace_ui_magCount = true;
force force ace_ui_soldierVehicleWeaponInfo = true;
force force ace_ui_staminaBar = true;
force force ace_ui_stance = false;
force force ace_ui_throwableCount = true;
force force ace_ui_throwableName = true;
force force ace_ui_vehicleAltitude = true;
force force ace_ui_vehicleCompass = true;
force force ace_ui_vehicleDamage = true;
force force ace_ui_vehicleFuelBar = true;
force force ace_ui_vehicleInfoBackground = true;
force force ace_ui_vehicleName = true;
force force ace_ui_vehicleNameBackground = true;
force force ace_ui_vehicleRadar = true;
force force ace_ui_vehicleSpeed = true;
force force ace_ui_weaponLowerInfoBackground = true;
force force ace_ui_weaponName = true;
force force ace_ui_weaponNameBackground = true;
force force ace_ui_zeroing = true;

// ACE Vehicle Lock
force force ace_vehiclelock_defaultLockpickStrength = 10;
force force ace_vehiclelock_lockVehicleInventory = false;
force force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE Vehicles
//ace_vehicles_hideEjectAction = true;
force force ace_vehicles_keepEngineRunning = false;
//ace_vehicles_speedLimiterStep = 5;

// ACE View Distance Limiter
force force ace_viewdistance_enabled = true;
force force ace_viewdistance_limitViewDistance = 7500;
//ace_viewdistance_objectViewDistanceCoeff = 5;
//ace_viewdistance_viewDistanceAirVehicle = 8000;
//ace_viewdistance_viewDistanceLandVehicle = 5000;
//ace_viewdistance_viewDistanceOnFoot = 2000;

// ACE View Restriction
force force acex_viewrestriction_mode = 1;
force force acex_viewrestriction_modeSelectiveAir = 0;
force force acex_viewrestriction_modeSelectiveFoot = 0;
force force acex_viewrestriction_modeSelectiveLand = 0;
force force acex_viewrestriction_modeSelectiveSea = 0;
force force acex_viewrestriction_preserveView = false;

// ACE Volume
//acex_volume_fadeDelay = 3;
//acex_volume_enabled = false;
//acex_volume_lowerInVehicles = false;
//acex_volume_reduction = 7;
//acex_volume_remindIfLowered = false;
//acex_volume_showNotification = false;

// ACE Weapons
force force ace_common_persistentLaserEnabled = true;
force force ace_laserpointer_enabled = true;
//ace_reload_displayText = true;
//ace_reload_showCheckAmmoSelf = false;
//ace_weaponselect_displayText = true;

// ACE Weather
force force ace_weather_enabled = true;
//ace_weather_showCheckAirTemperature = true;
force force ace_weather_updateInterval = 300;
force force ace_weather_windSimulation = true;

// ACE Wind Deflection
force force ace_winddeflection_enabled = false;
force force ace_winddeflection_simulationInterval = 0.05;
force force ace_winddeflection_vehicleEnabled = false;

// ACE Zeus
force force ace_zeus_autoAddObjects = true;
force force ace_zeus_canCreateZeus = -1;
force force ace_zeus_radioOrdnance = false;
force force ace_zeus_remoteWind = false;
force force ace_zeus_revealMines = 0;
force force ace_zeus_zeusAscension = false;
force force ace_zeus_zeusBird = false;

// ACRE2
force force acre_sys_core_automaticAntennaDirection = true;
//acre_sys_core_defaultRadioVolume = 0.8;
force force acre_sys_core_fullDuplex = true;
acre_sys_core_godVolume = 1;
force force acre_sys_core_ignoreAntennaDirection = false;
force force acre_sys_core_interference = false;
//acre_sys_core_postmixGlobalVolume = 2;
//acre_sys_core_premixGlobalVolume = 3;
force force acre_sys_core_revealToAI = 1;
//acre_sys_core_spectatorVolume = 0.8;
force force acre_sys_core_terrainLoss = 0.5;
force force acre_sys_core_ts3ChannelName = "ACRE Operations";
force force acre_sys_core_ts3ChannelPassword = "";
force force acre_sys_core_ts3ChannelSwitch = true;
//acre_sys_core_unmuteClients = true;
force force acre_sys_signal_signalModel = 1;

// ACRE2 UI
//acre_sys_godmode_rxNotification = true;
//acre_sys_godmode_rxNotificationColor = [0.8,0.8,0.8,1];
//acre_sys_godmode_txNotification = true;
//acre_sys_godmode_txNotificationCurrentChatColor = [0.8,0.8,0.8,1];
//acre_sys_godmode_txNotificationGroup1Color = [0.8,0.8,0.8,1];
//acre_sys_godmode_txNotificationGroup2Color = [0.8,0.8,0.8,1];
//acre_sys_godmode_txNotificationGroup3Color = [0.8,0.8,0.8,1];
//acre_sys_gui_volumeColorScale = [[1,1,0,0.5],[1,0.83,0,0.5],[1,0.65,0,0.5],[1,0.44,0,0.5],[1,0,0,0.5]];
//acre_sys_list_CycleRadiosColor = [0.66,0.05,1,1];
//acre_sys_list_DefaultPTTColor = [1,0.8,0,1];
//acre_sys_list_HintBackgroundColor = [0,0,0,0.8];
//acre_sys_list_HintTextFont = "RobotoCondensed";
//acre_sys_list_LanguageColor = [1,0.29,0.16,1];
//acre_sys_list_PTT1Color = [1,0.8,0,1];
//acre_sys_list_PTT2Color = [1,0.8,0,1];
//acre_sys_list_PTT3Color = [1,0.8,0,1];
//acre_sys_list_SwitchChannelColor = [0.66,0.05,1,1];
//acre_sys_list_ToggleHeadsetColor = [0.66,0.05,1,1];

// ACRE2 Zeus
force force acre_sys_zeus_zeusCanSpectate = true;
force force acre_sys_zeus_zeusCommunicateViaCamera = true;
force force acre_sys_zeus_zeusDefaultVoiceSource = false;

// AWESome Aerodynamics
force force orbis_aerodynamics_enabled = true;
force force orbis_aerodynamics_pylonDragMultiplierGlobal = 1;
force force orbis_aerodynamics_pylonMassMultiplierGlobal = 1;
force force orbis_aerodynamics_windMultiplier = 1;

// AWESome ATC
//orbis_atc_ATISupdateInterval = 15;
//orbis_atc_displayCallsign = 0;
//orbis_atc_displayProjectileTrails = false;
//orbis_atc_radarTrailLength = 5;
//orbis_atc_radarUpdateInterval = 0.5;
//orbis_atc_unitSettingAlt = false;
//orbis_atc_unitSettingSpd = false;

// AWESome Cockpit
//orbis_cockpit_checklistUnits = "KIAS";
//orbis_cockpit_groundMultiplier = 1;
//orbis_cockpit_shakeEnabled = true;
//orbis_cockpit_speedMultiplier = 1;

// AWESome GPWS
//orbis_gpws_automaticTransponder = true;
//orbis_gpws_defaultVolumeLow = false;
//orbis_gpws_personalDefault = "none";

// Bundeswehr
//BWA3_Leopard_ClocknumbersDir_Commander = false;
force force BWA3_NaviPad_showMembers = true;
//BWA3_Puma_ClocknumbersDir_Commander = false;
//BWA3_Puma_ClocknumbersDir_Gunner = false;

// BWI Armory
//bwi_armory_babelLanguageBlufor = "BLUFOR";
//bwi_armory_babelLanguageCivil = "CIVILIAN";
//bwi_armory_babelLanguageIndep = "INDEPENDENT";
//bwi_armory_babelLanguageOpfor = "OPFOR";
//bwi_armory_playerFactionBlufor = "CDF_Airborne/Woodland_2008";
//bwi_armory_playerFactionCivil = "Civilians/Afghan";
//bwi_armory_playerFactionIndep = "TSA_Infantry/Desert_1986";
//bwi_armory_playerFactionOpfor = "CHE_Armored/Woodland_2009";
force force bwi_armory_radioChannelNames = "PLTNET-1,CMDNET-1,AIRNET-1,TACNET-1,MEDNET-1,PLTNET-2,CMDNET-2,AIRNET-2,TACNET-2,MEDNET-2,PLTNET-3,CMDNET-3,AIRNET-3,TACNET-3,MEDNET-3,GODNET-1";
force force bwi_armory_radioFrequencySpacing = 0.05;
force force bwi_armory_radioFrequencyStart = 60;
//bwi_armory_unlockAllFactions = false;
//bwi_armory_unlockReconElements = false;
//bwi_armory_unlockSpecialElements = false;
force force bwi_armory_unlockWeaponsElements = true;

// BWI Common
//bwi_common_displayDebug = false;
//bwi_common_notificationDisplayTime = 5;
//bwi_common_useMultipleSideCallsigns = false;

// BWI Construction
force force bwi_construction_exclusionRadiusTier1 = 1000;
force force bwi_construction_exclusionRadiusTier2 = 1500;
force force bwi_construction_exclusionRadiusTier3 = 2000;
//bwi_construction_previewColorError = [1,0,0,0.7];
//bwi_construction_previewColorLine = [0,0,1,0.8];
//bwi_construction_previewColorOK = [0,1,0,0.7];
//bwi_construction_previewColorWarning = [1,0.5,0,0.7];
//bwi_construction_previewViewDistance = 250;

// BWI Markers
force force bwi_markers_allowedMarkerCharacters = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
//bwi_markers_hideShortcutHint = false;
force force bwi_markers_minUniqueLocationDistance = 500;

// BWI Medical
force force bwi_medical_emergencyMedicMaxRange = 25;
force force bwi_medical_emergencyMedicTreatmentTime = 30;
force force bwi_medical_surgeryFractureHeal = true;
force force bwi_medical_surgeryTraumaHeal = true;

// BWI Motorpool
//bwi_motorpool_maxLevelBlufor = 1;
//bwi_motorpool_maxLevelCivil = 1;
//bwi_motorpool_maxLevelIndep = 1;
//bwi_motorpool_maxLevelOpfor = 1;
force force bwi_motorpool_rearmRate = 0.06;
force force bwi_motorpool_refuelRate = 0.08;
//bwi_motorpool_refundTimeLimit = 180;
force force bwi_motorpool_repairRate = 0.04;
//bwi_motorpool_restrictAccessTo = "";
force force bwi_motorpool_servicingInterval = 5;
force force bwi_motorpool_servicingRange = 50;
//bwi_motorpool_totalFundsBlufor = 85;
//bwi_motorpool_totalFundsCivil = 85;
//bwi_motorpool_totalFundsIndep = 85;
//bwi_motorpool_totalFundsOpfor = 85;

// BWI Resupply
//bwi_resupply_restrictAccessTo = "";

// BWI Staging
//bwi_staging_notificationFrequency = 120;
//bwi_staging_notificationSubscribers = "";
//bwi_staging_primaryAreaBlufor = "NONE";
//bwi_staging_primaryAreaIndep = "NONE";
//bwi_staging_primaryAreaOpfor = "NONE";
force force bwi_staging_reinforcementCheckRadius = 25;
force force bwi_staging_returnTimeLimit = -1;
//bwi_staging_secondaryAreaBlufor = "NONE";
//bwi_staging_secondaryAreaIndep = "NONE";
//bwi_staging_secondaryAreaOpfor = "NONE";
force force bwi_staging_showActiveMarkers = true;
//bwi_staging_showAllMarkers = false;
//bwi_staging_showMarkersIn3DEN = true;

// BWI Utilities
force force bwi_utilities_clearVehicleCargo = true;
force force bwi_utilities_corpseCleanupRadius = 75;
force force bwi_utilities_ignoreCargoItems = "rhsusf_ihadss,UK3CB_BAF_32Rnd_40mm_G_Box,UK3CB_BAF_762_200Rnd_T,UK3CB_BAF_127_100Rnd,UK3CB_BAF_1Rnd_Milan,UK3CB_BAF_6Rnd_30mm_L21A1_APDS,UK3CB_BAF_6Rnd_30mm_L21A1_HE";

// Community Base Addons
//cba_diagnostic_ConsoleIndentType = 0;
force force cba_disposable_dropUsedLauncher = 2;
force force cba_disposable_replaceDisposableLauncher = true;
//cba_events_repetitionMode = 1;
force force cba_network_loadoutValidation = 0;
//cba_optics_usePipOptics = true;
//cba_ui_notifyLifetime = 4;
//cba_ui_StorePasswords = 1;

// DUI - Squad Radar - Indicators
force force diwako_dui_indicators_crew_range_enabled = false;
//diwako_dui_indicators_fov_scale = false;
force force diwako_dui_indicators_icon_buddy = true;
force force diwako_dui_indicators_icon_leader = true;
force force diwako_dui_indicators_icon_medic = true;
force force diwako_dui_indicators_range = 20;
force force diwako_dui_indicators_range_crew = 300;
//diwako_dui_indicators_range_scale = false;
force force diwako_dui_indicators_show = false;
//diwako_dui_indicators_size = 1;
//diwako_dui_indicators_style = "standard";
force force diwako_dui_indicators_useACENametagsRange = false;

// DUI - Squad Radar - Main
//diwako_dui_ace_hide_interaction = true;
force force diwako_dui_colors = "ace";
//diwako_dui_font = "RobotoCondensed";
//diwako_dui_icon_style = "standard";
//diwako_dui_main_hide_ui_by_default = false;
force force diwako_dui_main_squadBlue = [0,0,1,1];
force force diwako_dui_main_squadGreen = [0,1,0,1];
force force diwako_dui_main_squadMain = [1,1,1,1];
force force diwako_dui_main_squadRed = [1,0,0,1];
force force diwako_dui_main_squadYellow = [1,1,0,1];
//diwako_dui_main_trackingColor = [0.93,0.26,0.93,1];
//diwako_dui_reset_ui_pos = false;

// DUI - Squad Radar - Nametags
//diwako_dui_nametags_deadColor = [0.2,0.2,0.2,1];
force force diwako_dui_nametags_deadRenderDistance = 3.5;
force force diwako_dui_nametags_drawRank = true;
force force diwako_dui_nametags_enabled = false;
force force diwako_dui_nametags_enableFOVBoost = true;
force force diwako_dui_nametags_enableOcclusion = true;
//diwako_dui_nametags_fadeInTime = 0.05;
//diwako_dui_nametags_fadeOutTime = 0.5;
//diwako_dui_nametags_fontGroup = "RobotoCondensedLight";
//diwako_dui_nametags_fontGroupNameSize = 8;
//diwako_dui_nametags_fontName = "RobotoCondensedBold";
//diwako_dui_nametags_fontNameSize = 10;
//diwako_dui_nametags_groupColor = [1,1,1,1];
//diwako_dui_nametags_groupFontShadow = 1;
//diwako_dui_nametags_groupNameOtherGroupColor = [0.6,0.85,0.6,1];
//diwako_dui_nametags_nameFontShadow = 1;
//diwako_dui_nametags_nameOtherGroupColor = [0.2,1,0,1];
force force diwako_dui_nametags_renderDistance = 40;
force force diwako_dui_nametags_showUnconAsDead = true;
force force diwako_dui_nametags_useSideIsFriendly = true;

// DUI - Squad Radar - Radar
force force diwako_dui_compass_hide_alone_group = false;
force force diwako_dui_compass_hide_blip_alone_group = false;
//diwako_dui_compass_icon_scale = 1;
//diwako_dui_compass_opacity = 1;
//diwako_dui_compass_style = ["\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass_limited.paa","\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass.paa"];
force force diwako_dui_compassRange = 35;
//diwako_dui_compassRefreshrate = 0;
//diwako_dui_dir_showMildot = false;
//diwako_dui_dir_size = 1.25;
force force diwako_dui_distanceWarning = 5;
force force diwako_dui_enable_compass = true;
force force diwako_dui_enable_compass_dir = 2;
force force diwako_dui_enable_occlusion = true;
force force diwako_dui_enable_occlusion_cone = 360;
//diwako_dui_hudScaling = 1;
force force diwako_dui_namelist = true;
//diwako_dui_namelist_bg = 0;
//diwako_dui_namelist_only_buddy_icon = false;
//diwako_dui_namelist_size = 1;
//diwako_dui_namelist_text_shadow = 2;
//diwako_dui_namelist_width = 215;
force force diwako_dui_radar_ace_finger = true;
force force diwako_dui_radar_ace_medic = true;
force force diwako_dui_radar_compassRangeCrew = 175;
//diwako_dui_radar_dir_padding = 25;
force force diwako_dui_radar_group_by_vehicle = true;
//diwako_dui_radar_icon_opacity = 1;
//diwako_dui_radar_icon_opacity_no_player = true;
//diwako_dui_radar_icon_scale_crew = 6;
force force diwako_dui_radar_leadingZeroes = true;
force force diwako_dui_radar_namelist_hideWhenLeader = false;
//diwako_dui_radar_namelist_vertical_spacing = 1;
force force diwako_dui_radar_occlusion_fade_in_time = 1;
force force diwako_dui_radar_occlusion_fade_time = 10;
//diwako_dui_radar_pointer_color = [1,0.5,0,1];
//diwako_dui_radar_pointer_style = "standard";
force force diwako_dui_radar_show_cardinal_points = true;
force force diwako_dui_radar_showSpeaking = false;
force force diwako_dui_radar_showSpeaking_radioOnly = false;
force force diwako_dui_radar_showSpeaking_replaceIcon = false;
force force diwako_dui_radar_sortType = "fireteam";
force force diwako_dui_radar_sqlFirst = true;
force force diwako_dui_radar_vehicleCompassEnabled = true;
//diwako_dui_use_layout_editor = false;

// GRAD Trenches
force force grad_trenches_functions_allowBigEnvelope = true;
force force grad_trenches_functions_allowCamouflage = true;
force force grad_trenches_functions_allowDigging = true;
force force grad_trenches_functions_allowEffects = true;
force force grad_trenches_functions_allowGiantEnvelope = true;
force force grad_trenches_functions_allowHitDecay = true;
force force grad_trenches_functions_allowLongEnvelope = true;
force force grad_trenches_functions_allowShortEnvelope = true;
force force grad_trenches_functions_allowSmallEnvelope = true;
force force grad_trenches_functions_allowTrenchDecay = false;
force force grad_trenches_functions_allowVehicleEnvelope = true;
force force grad_trenches_functions_bigEnvelopeDamageMultiplier = 2;
force force grad_trenches_functions_bigEnvelopeDigTime = 40;
force force grad_trenches_functions_bigEnvelopeRemovalTime = -1;
force force grad_trenches_functions_buildFatigueFactor = 1;
force force grad_trenches_functions_camouflageRequireEntrenchmentTool = true;
force force grad_trenches_functions_createTrenchMarker = false;
force force grad_trenches_functions_decayTime = 1800;
force force grad_trenches_functions_giantEnvelopeDamageMultiplier = 1;
force force grad_trenches_functions_giantEnvelopeDigTime = 90;
force force grad_trenches_functions_giantEnvelopeRemovalTime = -1;
force force grad_trenches_functions_hitDecayMultiplier = 1;
force force grad_trenches_functions_LongEnvelopeDigTime = 100;
force force grad_trenches_functions_LongEnvelopeRemovalTime = -1;
force force grad_trenches_functions_shortEnvelopeDamageMultiplier = 2;
force force grad_trenches_functions_shortEnvelopeDigTime = 15;
force force grad_trenches_functions_shortEnvelopeRemovalTime = -1;
force force grad_trenches_functions_smallEnvelopeDamageMultiplier = 3;
force force grad_trenches_functions_smallEnvelopeDigTime = 30;
force force grad_trenches_functions_smallEnvelopeRemovalTime = -1;
force force grad_trenches_functions_stopBuildingAtFatigueMax = true;
force force grad_trenches_functions_timeoutToDecay = 7200;
force force grad_trenches_functions_vehicleEnvelopeDamageMultiplier = 1;
force force grad_trenches_functions_vehicleEnvelopeDigTime = 120;
force force grad_trenches_functions_vehicleEnvelopeRemovalTime = -1;

// USAF
force force usaf_afterburner_setting_allow_ai = true;
force force USAF_allowNuke = false;
force usaf_debug_setting_enabled_clients = false;
force usaf_debug_setting_enabled_server = false;
force force usaf_f35a_allow_das_coverage = true;
force force usaf_f35a_allow_sar_imagery = true;
force force usaf_serviceMenu_setting_allowHangarRearm = false;
force force usaf_serviceMenu_setting_allowHangarRefuel = false;
force force usaf_serviceMenu_setting_allowHangarRepair = false;
force force usaf_serviceMenu_setting_allowLoadoutModification = false;
force force usaf_serviceMenu_setting_enabled = false;
force force usaf_serviceMenu_setting_refuelTime = "100";
force force usaf_serviceMenu_setting_reloadTime = "2";
force force usaf_serviceMenu_setting_repairTime = "100";
force force usaf_serviceMenu_setting_replaceSources = false;
force force usaf_serviceMenu_setting_selectorSearchRadius = "15";
force force usaf_setting_allow_aiFormlights = true;
force force usaf_utility_core_allow_move_in_cargo = false;

// VCOM AI East Skill
force force Vcm_AISkills_East_AimingAccuracy = 0.25;
force force Vcm_AISkills_East_aimingShake = 0.15;
force force Vcm_AISkills_East_aimingSpeed = 0.15;
force force Vcm_AISkills_East_commanding = 0.85;
force force Vcm_AISkills_East_courage = 0.5;
force force Vcm_AISkills_East_general = 0.5;
force force Vcm_AISkills_East_reloadSpeed = 1;
force force Vcm_AISkills_East_spotDistance = 0.85;
force force Vcm_AISkills_East_spotTime = 0.85;

// VCOM AI General Skill
force force Vcm_AISkills_General_AimingAccuracy = 0.25;
force force Vcm_AISkills_General_aimingShake = 0.15;
force force Vcm_AISkills_General_aimingSpeed = 0.35;
force force Vcm_AISkills_General_commanding = 0.85;
force force Vcm_AISkills_General_courage = 0.5;
force force Vcm_AISkills_General_general = 0.5;
force force Vcm_AISkills_General_reloadSpeed = 1;
force force Vcm_AISkills_General_spotDistance = 0.85;
force force Vcm_AISkills_General_spotTime = 0.85;

// VCOM AI Resistance Skill
force force Vcm_AISkills_Resistance_AimingAccuracy = 0.25;
force force Vcm_AISkills_Resistance_aimingShake = 0.15;
force force Vcm_AISkills_Resistance_aimingSpeed = 0.15;
force force Vcm_AISkills_Resistance_commanding = 0.85;
force force Vcm_AISkills_Resistance_courage = 0.5;
force force Vcm_AISkills_Resistance_general = 0.5;
force force Vcm_AISkills_Resistance_reloadSpeed = 1;
force force Vcm_AISkills_Resistance_spotDistance = 0.85;
force force Vcm_AISkills_Resistance_spotTime = 0.85;

// VCOM AI West Skill
force force Vcm_AISkills_West_AimingAccuracy = 0.25;
force force Vcm_AISkills_West_aimingShake = 0.15;
force force Vcm_AISkills_West_aimingSpeed = 0.15;
force force Vcm_AISkills_West_commanding = 0.85;
force force Vcm_AISkills_West_courage = 0.5;
force force Vcm_AISkills_West_general = 0.5;
force force Vcm_AISkills_West_reloadSpeed = 1;
force force Vcm_AISkills_West_spotDistance = 0.85;
force force Vcm_AISkills_West_spotTime = 0.85;

// VCOM SETTINGS
force force VCM_ActivateAI = true;
force force VCM_ADVANCEDMOVEMENT = true;
force force VCM_AIDISTANCEVEHPATH = 100;
force force VCM_AIMagLimit = 3;
force force VCM_AISkills_General_EM = false;
force force VCM_AISkills_General_EM_CHN = 10;
force force VCM_AISkills_General_EM_CLDWN = 10;
force force Vcm_AISkills_SideSpecific = false;
force force VCM_AISNIPERS = true;
force force VCM_AISUPPRESS = true;
force force VCM_ARTYDELAY = 10;
force force VCM_ARTYENABLE = true;
force force VCM_ARTYSIDES = ["WEST","EAST","RESISTANCE"];
force force VCM_CARGOCHNG = true;
force force VCM_ClassSteal = true;
force force VCM_Debug = false;
force force VCM_DISEMBARKRANGE = 200;
force force Vcm_DrivingActivated = false;
force force VCM_ForceSpeed = true;
force force VCM_FRMCHANGE = true;
force force Vcm_GrenadeChance = 10;
force force VCM_HEARINGDISTANCE = 900;
force force VCM_MEDICALACTIVE = true;
force force VCM_MINECHANCE = 75;
force force VCM_MINEENABLED = true;
force force Vcm_PlayerAISkills = true;
force force VCM_RAGDOLL = true;
force force VCM_RAGDOLLCHC = 50;
force force VCM_SIDEENABLED = ["WEST","EAST","RESISTANCE"];
force force VCM_SKILLCHANGE = true;
force force Vcm_SmokeGrenadeChance = 3;
force force VCM_STATICARMT = 300;
force force VCM_StealVeh = true;
force force VCM_SUPDIST = 150;
force force VCM_TURRETUNLOAD = true;
force force VCM_USECBASETTINGS = true;
force force VCM_WARNDELAY = 30;
force force VCM_WARNDIST = 1000;
