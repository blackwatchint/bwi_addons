When using Addon Builder to build:
1. Go to **Options**.
2. Enter `cba_settings_userconfig` in the **Addon prefix** field.
3. Click **OK** and build the PBO.
4. Reverse the setting before building other addons.