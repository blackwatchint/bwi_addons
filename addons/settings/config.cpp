class CfgPatches {
    class cba_settings_userconfig {
        requiredVersion = 1.0;
        version = 1.5;
		versionStr = "1.5.0";
        versionAr[] = {1,5,0};
        author = "Black Watch International";
		url = "http://blackwatch-int.com";
        authors[] = {"RedBery"};
        units[] = {};
        weapons[] = {};
        requiredAddons[] = {"cba_settings", "ace_common"};
        cba_settings_whitelist[] = {"admin"};
    };
};