// Wolf Command / Medic
class Redd_Tank_LKW_leicht_gl_Wolf_Base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		delete Intercom_1;
	};
};


// LKW KAT I Trucks
// Co-driver is 'Commander', when they turn out to use the MG3 they become 'Gunner'
class rnt_lkw_10t_mil_gl_kat_i_base: Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};
class rnt_lkw_7t_mil_gl_kat_i_base: Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};
class rnt_lkw_5t_mil_gl_kat_i_base: Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// Fuchs APC (Infantry, Medical, Engineer)
// Turret 0,3 is the co-driver when turned in/out. Co-driver becomes 'gunner' when they mount the MG3.
// 'Middle Hatch' is labeled as a Commander seat, but it is unarmed and in the passenger area, so we treat it as a passenger seat.
class Redd_Tank_Fuchs_1A4_Base: Wheeled_APC_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {0,3}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", {"turret", {0,3}}};
			disabledPositions[] = {};
			limitedPositions[] = {"commander", {"cargo","all"}, {"ffv", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", {"turret", {0,3}}, "commander", {"cargo","all"}, {"ffv", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// Fuchs APC (Milan)
// Turret 0,3 is the co-driver when turned in/out. Co-driver becomes 'gunner' when they mount the MG3.
// For this vehicle, the Commander seat has a Milan launcher, and we treat it as a crew seat.
class Redd_Tank_Fuchs_1A4_Jg_Milan_Flecktarn: Redd_Tank_Fuchs_1A4_Base {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,3}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,3}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", {"turret", {0,3}}, "commander", {"cargo","all"}, {"ffv", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// Luchs APC
// Driver / Gunner / Commander / Radio Operatior as {"turret", {1}}}
// Use the "inside" ACRE2 wildcard because there's no one that shouldn't be on rack/intercom.
class rnt_sppz_2a2_luchs_Base: Wheeled_APC_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
	};
};


// Wiesel Tankette (TOW)
// Commander seat operates the TOW.
// {"turret", {0,1}} is the Loader seat when turned in/out.
// {"turret", {1} is the Loader seat when they mount the MG3.
// Use the "inside" ACRE2 wildcard because there's no one that shouldn't be on rack/intercom.
class Redd_Tank_Wiesel_1A2_TOW_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
	};
};


// Wiesel Tankette (mk20)
// 'Commander' seat is actually labeled 'gunner' internally. Gunner operates the 20mm gun.
// Gunner becomes commander when using the 'Climb Up' action. It's a mess.
// Use the "inside" ACRE2 wildcard because there's no one that shouldn't be on rack/intercom.
class Redd_Tank_Wiesel_1A4_MK20_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
	};
};


// Gepard SPAA
class Redd_Tank_Gepard_1A2_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
	};
};


// Marder IFV
class Redd_Marder_1A5_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};