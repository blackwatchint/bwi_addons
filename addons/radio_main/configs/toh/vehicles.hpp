class JK_UH1H_base;
class JK_Heli_Medium_01_base_F;

// UH-1H - Left door gun is 'gunner', Right door gun is 'turret 0'
class JK_UH1H_Armed_M240_Base : JK_UH1H_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner", {"turret", {0}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 0; // Don't automatically connect door gunners to intercom.
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", "gunner", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// UH-146 - Middle FLIR turret seat is 'commander 1'
class JK_Heli_Medium_Mil_Utility_Base_F : JK_Heli_Medium_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 0; // Don't automatically connect door gunners to intercom.
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// UH-146 - Middle FLIR turret seat is 'commander 1'
class JK_Heli_Medium_GUE_Base_F : JK_Heli_Medium_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 0; // Don't automatically connect door gunners to intercom.
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// UH-146 /w Radar - Left door gunner is 'commander 1', Right door gunner is 'commander 2', Middle FLIR turret seat is 'commander 3'
class JK_Heli_Medium_Military_Armed_Base_F : JK_Heli_Medium_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 0; // Don't automatically connect door gunners to intercom.
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};