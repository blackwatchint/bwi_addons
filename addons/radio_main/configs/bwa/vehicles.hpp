// Eagle IV
class BWA3_Eagle_base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", {"cargo", 1}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// Eagle IV FLW
class BWA3_Eagle_flw100_base: BWA3_Eagle_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// Dingo 2
class BWA3_Dingo2_base: car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// Puma IFV
class BWA3_Puma_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};


// Leopard MBT
class BWA3_Leopard_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,1}}}; // Turret 0,1 is loader.
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,1}}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
	};
};


// UH Tiger CAS Helicopter
class BWA3_Tiger_base: Helicopter_Base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
	};
};