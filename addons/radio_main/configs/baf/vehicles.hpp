// Jackal / Coyote (Logistics) / Coyote (Passenger) inherits racks / intercom from this.
class UK3CB_BAF_Jackal_Base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		delete Intercom_1;
	};
};


// Land Rover Snatch / Soft-top / Hard-top / Ambulance / WMIK inherits racks / intercom from this.
// Requires "uk3cb_baf_vehicles_landrover" in requiredAddons[], or your configs will not work.
class UK3CB_BAF_LandRover_Base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "commander", "gunner", {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		delete Intercom_1;
	};
};


// MAN HX60 4x4
class UK3CB_BAF_MAN_HX60_Base: Truck_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", {"cargo", 0}, {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// MAN HX58 6x6 
class UK3CB_BAF_MAN_HX58_Base: UK3CB_BAF_MAN_HX60_Base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", {"cargo", 0}, {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// Panther MRAP
class UK3CB_BAF_Panther_Base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
		delete Rack_3;
	};
	class AcreIntercoms {
		delete Intercom_1;
	};
};


// Husky TSV
class UK3CB_BAF_Husky_Base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
		delete Rack_3;
	};
	class AcreIntercoms {
		delete Intercom_1;
	};
};


// Warrior IFV
class UK3CB_BAF_Warrior_A3_Base: APC_Tracked_03_base_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
		delete Rack_2;
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};