class rhsusf_m998_w_4dr_fulltop;
class rhsusf_M1151_GPK_M2_base;
class rhsusf_M1220_M2_usarmy_d;
class rhsusf_RG33_base;
class rhsusf_RG33_CROWS_base;
class rhsusf_RG33_CROWSMK19_base;
class rhsusf_M1239_base;
class rhsusf_M1239_CROWS_base;
class rhsusf_M1239_CROWSMK19_base;
class rhsusf_M1239_Deploy_base;
class rhsusf_M1239_DeployMK19_base;
class rhs_Ural_BaseTurret;
class RHS_Ural_Base;
class rhs_gaz66_vmf;
class rhs_bmp1tank_base;
class rhsusf_stryker_m1126_m2_base;

/** RHS USF Assets */

// Base Humvees - Front passenger is cargo 0
class rhsusf_hmmwe_base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// Unarmed M1025 Humvee - Front passenger is turret 0
class rhsusf_m1025_w: rhsusf_m998_w_4dr_fulltop {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}, {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// Armed Humvees - Front passenger is turret 1
class rhsusf_m1025_w_m2: rhsusf_m1025_w {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}, {"turret", {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// TOW Humvees - Front passener in M966 is turret 1, M1045 inherits from M996 now
class rhsusf_m966_w: rhsusf_m1025_w {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}, {"turret", {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// M998 Humvees with FFV seats
class rhsusf_m998_w_2dr: rhsusf_hmmwe_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}, {"turret", {2}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// Unarmed M1151 / M1165 Humvee - Co-driver is turret 0
class rhsusf_m1151_base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// Armed M1151 Humvee - Co-driver is turret 1
class rhsusf_m1151_GPK_base: rhsusf_m1151_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};
class rhsusf_m1151_CROWS_base: rhsusf_m1151_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// LRAS3 Humvee - Co-driver is turret 3
class rhsusf_M1151_M2_LRAS3_base: rhsusf_M1151_GPK_M2_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {3}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// M1220 CROWS M2 - M1220 CROWS Mk19 inherits from this
class rhsusf_M1220_M153_M2_usarmy_d: rhsusf_M1220_M2_usarmy_d {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1240 MATV
class rhsusf_MATV_base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1238A1 ASV
class rhsusf_M1238A1_socom_d: rhsusf_RG33_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1238A1 ASV CROWS M2 - doesn't inherit configs from M1238A1 ASV
class rhsusf_M1238A1_M2_socom_d: rhsusf_RG33_CROWS_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1238A1 ASV CROWS Mk19 - doesn't inherit configs from M1238A1 ASV
class rhsusf_M1238A1_Mk19_socom_d: rhsusf_RG33_CROWSMK19_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1239 AUV
class rhsusf_M1239_socom_d: rhsusf_M1239_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1239 AUV CROWS M2 - doesn't inherit configs from M1239 AUV
class rhsusf_M1239_M2_socom_d: rhsusf_M1239_CROWS_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1239 AUV CROWS Mk19 - doesn't inherit configs from M1239 AUV
class rhsusf_M1239_MK19_socom_d: rhsusf_M1239_CROWSMK19_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1239 AUV Deploy CROWS M2 - doesn't inherit configs from M1239 AUV
class rhsusf_M1239_M2_Deploy_socom_d: rhsusf_M1239_Deploy_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1239 AUV Deploy CROWS Mk19 - doesn't inherit configs from M1239 AUV
class rhsusf_M1239_MK19_Deploy_socom_d: rhsusf_M1239_DeployMK19_base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// CGR CAT1A2 'Cougar'
class rhsusf_Cougar_base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		delete Rack_2;
	};
};


// M1117 ASV
class rhsusf_M1117_base: Wheeled_APC_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"gunner"};
			connectedByDefault = 0;
		};
	};
};


// Stryker - All Stryker variants inherit from this
class rhsusf_stryker_base: Wheeled_APC_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"gunner"};
			connectedByDefault = 0;
		};
	};
};


// M1134 Stryker - Has special seats (driver, gunner, commander, loader) and no passenger seats (doesn't need PAX intercom)
class rhsusf_stryker_m1134_base : rhsusf_stryker_m1126_m2_base {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
		delete Intercom_2;
	};
};


// HIMARS
class rhsusf_himars_base: Truck_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Little Bird AH-6, MH-6, OH-6
class RHS_MELB_base: Helicopter_Base_H {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		// No PAX for bench seats.
	};
};


// Bell UH-1Y Venom
class RHS_UH1_Base: Heli_light_03_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", {"turret", {1}}, {"turret", {2}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 0; // Don't automatically connect door gunners to intercom.
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// Black Hawk UH-60M, UH-60M_ESSS, UH-60M MEV
class RHS_UH60_Base: Heli_Transport_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner", {"turret", {2}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 0; // Don't automatically connect door gunners to intercom.
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", "gunner", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// C-130J
class RHS_C130J_Base: Plane_Base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


/** RHS RUS Assets */

//Ural-4320 BM-21
class RHS_BM21_MSV_01: rhs_Ural_BaseTurret {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Ural-4320 ZU-23
class RHS_Ural_Zu23_Base: rhs_Ural_BaseTurret {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo", 3, 4}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// GAZ-66 ZU-23
class rhs_gaz66_zu23_base: rhs_gaz66_vmf {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// BRDM-2
class rhsgref_BRDM2 : Wheeled_APC_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"gunner"};
			connectedByDefault = 0;
		};
	};
};


// BTR-60, BTR-70, BTR-80, BTR-80A
class rhs_btr_base: Wheeled_APC_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"crew"}; // {"turret", {1}} is commander seat
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"crew"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"gunner"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"crew", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"gunner"};
			connectedByDefault = 0;
		};
	};
};


// BMP-1, BMP-2
// {"turret", {1}} is commander of BMP-1, {"turret", {0,0}} is commander of BMP-2.
class rhs_bmp_base: rhs_bmp1tank_base {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,0}}, {"turret", {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,0}}, {"turret", {1}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,0}}, {"turret", {1}}, {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {};
			connectedByDefault = 0;
		};
	};
};


// BMP-3
class rhs_bmp3tank_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 3;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};


// BMD-1, BMD-2
// All cargo seats are labeled as turret seats. Because the commander is also a turret seat, you can't use {"turret", "all"}
class rhs_bmd_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}}; // {"turret", {1}} is commander seat
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"turret", {2}}, {"turret", {3}}, {"turret", {4}}, {"turret", {5}}};
			numLimitedPositions = 2;
			masterPosition[] = {{"turret", {1}}};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}, {"turret", {2}}, {"turret", {3}}, {"turret", {4}}, {"turret", {5}}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {{"turret", {1}}};
			connectedByDefault = 0;
		};
	};
};


// BMD-4, BMD-4M, BMD-4MA
// All cargo seats are labeled as turret seats. Because the commander is also a turret seat, you can't use {"turret", "all"}
class rhs_a3spruttank_base: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,0}}}; // {"turret", {0,0}} is commander seat
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,0}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"turret", {1}}, {"turret", {2}}, {"turret", {3}}, {"turret", {4}}};
			numLimitedPositions = 2;
			masterPosition[] = {{"turret", {0,0}}};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,0}}, {"turret", {1}}, {"turret", {2}}, {"turret", {3}}, {"turret", {4}}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {{"turret", {0,0}}};
			connectedByDefault = 0;
		};
	};
};


// An-2 Antonov
class RHS_AN2_Base: Plane_Base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};