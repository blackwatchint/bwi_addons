class UK3CB_Hilux_Open;
class UK3CB_V3S_Base;


// Hilux (Rocket)
class UK3CB_Hilux_Rocket: UK3CB_Hilux_Open {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Hilux (Artillery)
class UK3CB_Hilux_Rocket_Arty: UK3CB_Hilux_Open {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Hilux ZU-23
class UK3CB_Hilux_ZU23: UK3CB_Hilux_Open {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}, {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Praga VS3 ZU-23
class UK3CB_V3S_Zu23: UK3CB_V3S_Base {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"inside"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Navistar MaxxPro
class UK3CB_MaxxPro_Base: MRAP_01_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// BTR-40
class UK3CB_BTR40: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};

// BMP-1. Doesn't inherit from RHS BMP-1 due to having a second layer of base classes.
// Config Viewer seems sketchy on this. Remove with caution.
class UK3CB_BMP1Tank_Base: rhs_bmp1tank_base {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}, {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {};
			connectedByDefault = 0;
		};
	};
};


// GAZ Vodnik
class UK3CB_GAZ_Vodnik_Base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0}}, {"turret", {3}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// LAZ 543 SCUD Launcher
class UK3CB_MAZ_543_SCUD: Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commmander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Willys Jeep
class UK3CB_Willys_Jeep_Base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// M151 'Mutt' Jeep
class UK3CB_M151_Jeep_Base: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// M939 5 Ton Truck
class UK3CB_M939_Base: Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// MTVR
class UK3CB_MTVR_Base: Truck_02_base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", {"turret", {0}, {1}}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// T-34-85M
class UK3CB_T34: MBT_02_base_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}}; // Turret 1 is Hull Gunner
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {1}}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
	};
};