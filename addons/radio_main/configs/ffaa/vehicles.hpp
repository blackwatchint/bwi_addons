//Hercules c-130
//the position copilot does not work. 
//trying individual positions under crew did not work
//workaround: add crew and disable driver.
class ffaa_ea_hercules_base: Plane_Base_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.3";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"crew"}; 
			disabledPositions[] = {"driver"};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.4";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"crew"}; 
			disabledPositions[] = {"driver"};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"crew"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"crew", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};