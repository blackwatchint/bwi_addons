// Create base classes for structure.
class AccessoryGroup
{
    name = "";          // Name of the group as it will appear in the customization UI.
    slot = -1;          // Attachment slot that the group belongs to (0 = top, 1 = right, 3 = bottom, 4 = barrel, -1 = hidden).
    level = 0;          // Arbitary number denoting quality level. Filtered based on allowedAccessories in CfgPlayableRoles. 0 is always none.
    
    /**
     * Name of an SQF function that can be run against "player" and return a classname.
     * For example, setting this to "primaryWeapon" returns the player's equipped
     * weapon, which will be compared against classnames in compatibleClasses.
     */
    compatibleAgainst = "primaryWeapon"; 

    /**
     * Top-level parent class tree that the items in this category belong to. For
     * example, CfgWeapons or CfgVehicles.
     */
    parentClassTree = "CfgWeapons";

    class Accessory     // Classname will match accessory classname.
    {
        year = 0;       // Approximate year the accessory is available from.

        /**
         * Define compatible classnames.
         *
         * Any classnames in this array that match the player's selected weapon/helmet
         * will result in this accessory being displayed in the accessories list.
         *
         * An empty array disables the filter, making the accessory available for all
         * weapons/helmets.
         */ 
        compatibleClasses[] = {};
    };
};