class CfgPatches
{
	class bwi_data
	{
		requiredVersion = 1.0;
		authors[] = {"Fourjays"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
	    version = 1.1;
	    versionStr = "1.1.0";
	    versionAr[] = {1,1,0};
		requiredAddons[] = {};
		units[] = {};
	};
};

class CfgPlayableFactions // Custom
{
	#include <cfgPlayableFactions.hpp>
};

class CfgPlayableRoles // Custom
{
	#include <cfgPlayableRoles.hpp>
};

class CfgPlayableAccessories // Custom
{
	#include <cfgPlayableAccessories.hpp>
};

class CfgPlayableSupplies // Custom
{
	#include <cfgPlayableSupplies.hpp>
};

class CfgPlayableVehicles // Custom
{
	#include <cfgPlayableVehicles.hpp>
};