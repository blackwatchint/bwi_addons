// Create base classes for structure.
class Category
{
    name = ""; // Name of the category.

    class Supply 
    {
        classname = ""; // Classname of a CfgVehicles resupply crate to spawn.

        compatible[] = {}; // List of compatible weapon, displayed in the GUI for user reference only.

        /**
        * Used for assigning faction-specific textures to spawned 
        * crates. This array is iterated with setObjectTexture 
        * called on each entry. The texture index is set to the
        * index in textureIndexes, while the texture itself is 
        * selected from a faction's resupplyTextures array at the
        * index specified in textureIndexes.
        *
        * E.g. If a faction defines {"tex1", "tex2", "tex3", "tex4"}
        * Then this parameter defines {0,3}, setObjectTexture will 
        * set slot 0 to "tex1", and slot 1 to "tex4".
        */
        textureIndexes[] = {};
    };
};