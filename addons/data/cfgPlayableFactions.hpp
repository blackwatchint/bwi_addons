// Create base classes for structure.
class FactionGroup
{
    name = ""; // Name of the group of factions.
    side = -1; // Side the faction group belongs to (0 = blufor, 1 = opfor, 2 = indep, 3 = civil, -1 = hidden).

    flag = "";  // Path to an image file used for flag textures.
    icon = "";  // Path to an image file used for the faction tree icons.
    image = ""; // Path to an image file used for the large icon in the top right.

    class Faction
    {
        name = "";       // Name of the faction.
        year = 0;        // Approximate year the faction represents. Affects standard gear.
        type = "";       // Type of faction. Affects standard gear.

        /**
         * Define the paths to the faction loadout scripts.
         * 
         * The scripts that will be used to generate the loadout for each faction.
         */
        uniformScript = "";
        weaponScript  = "";
        grenadeScript = "";
        ammoScript    = "";
        gearScript    = "";

        /**
         * Define roles and elements to hide.
         *
         * Any roles or elements in this array that match the 3-letter IDs defined in
         * CfgPlayableRoles will be skipped when generating the roles tree for this faction.
         */ 
        hiddenElements[] = {};
        hiddenRoles[] = {};

        /**
         * Defines ACRE radio devices.
         *
         * First and second slots are for personal radios, such as the 343 and 148.
         * Third and fourth slots are for element radios, such as the 152 and 117.
         *
         * If a slot is set to "NONE", no radio will be issued. If a slot is set to
         * an ACRE radio classname (e.g. ACRE_PRC148), the device will be issued with
         * the corresponding channel defined by acreRadioDevices in CfgPlayableRoles.
         */
        acreRadioDevices[] = {"NONE", "NONE", "NONE", "NONE"};

        /**
         * Defines the vehicles available through BWI Motorpool.
         * 
         * Classnames defined in these arrays are those listed in CfgPlayableVehicles.
         * Vehicles defined in allowedVehicles are available regardless of the presence
         * of recon and special forces elements. The arrays allowedVehiclesRecon and 
         * allowedVehiclesSpecial are only included when the recon and special forces
         * elements are unlocked in bwi_armory.
         */
        allowedVehicles[] = {};
        allowedVehiclesRecon[] = {};
        allowedVehiclesSpecial[] = {};
        allowedVehiclesWeapons[] = {};

        /**
         * Defines the resupply available through BWI Resupply.
         * 
         * Classnames defined in these arrays are those listed in CfgPlayableSupplies.
         * Crates defined in allowedSupplies are available regardless of the presence
         * of recon and special forces elements. The arrays allowedSuppliesRecon and 
         * allowedSuppliesSpecial are only included when the recon and special forces
         * elements are unlocked in bwi_armory.
         */
        allowedSupplies[] = {};
        allowedSuppliesRecon[] = {};
        allowedSuppliesSpecial[] = {};
        allowedSuppliesWeapons[] = {};

        /**
         * Defines an array of textures for BWI Resupply crates.
         *
         * The textureIndexes parameter in CfgPlayableSupplies maps each texture index 
         * on a crate to a texture in this array. Can be used with any crate texture
         * setup if desired, as long as it matches between the two. This will typically
         * be a background texture and three signs textures, for the base game crates.
         */
        resupplyTextures[] = {};
    };
};