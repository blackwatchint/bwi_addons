// Create base classes for structure.
class Category
{
    name = ""; // Name of the category.
    type = 0;  // Types of vehicles, used for location compatibility filters (1 = ground, 2 = helipad, 3 = hangar, 4 = apron, 5 = carrier, 6 = sea).

    class Vehicle 
    {
        classname = ""; // Classname of a CfgVehicles vehicle to spawn.
        name = "";      // Optional vehicle name, read from CfgVehicles if left empty.

        crew = -1;       // Optional crew count, read from CfgVehicles if left at -1.
        passengers = -1; // Optional passenger count, read from CfgVehicles if left at -1.
        
        level = 1; // Capability level of the vehicle (1 = utility, 2 = anti-car, 3 = anti-apc/spaa, 4 = anti-ifv, 5 = anti-mbt).
        cost = 0;  // Funds that this vehicle costs to spawn.

        textureData[] = {};   // Texture data array for BIS_fnc_initVehicle. Obtained through "Edit Vehicle Appearance" export. Reference BI Wiki for more information.
        animationData[] = {}; // Animation data array for BIS_fnc_initVehicle. Obtained through "Edit Vehicle Appearance" export. Reference BI Wiki for more information.

        pylonLoadout[] = {};  // Munition classnames, in the same order as the pylons on the aircraft. Obtained through "Export to SQF" on a mission.
        pylonTurrets[] = {};  // Optional turret settings for each pylon position (-1 no change, 0 sets turret to copilot). Obtained through "Export to SQF" on a mission.

        loadInCargo[] = {};   // Optional array of crate or object classnames that are to be loaded into the vehicle's ACE Cargo space.
        isOutpost = 0;        // When set to 1 the vehicle will act as a mobile outpost for bwi_staging.
    };
};