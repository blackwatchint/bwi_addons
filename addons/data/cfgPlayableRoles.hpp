// Create base classes for structure.
class Element
{
    name = "";        // Name of the element.
    sides[] = {};     // Side this element belongs to (0 = blufor, 1 = opfor, 2 = indep, 3 = civil, -1 = hidden).
    callsigns[] = {}; // List of callsigns used for this element. Each additional element of this type will used the next callsign.

    isReconElement = 0;   // Flag identifying this faction as recon. Used for armory filters.
    isSpecialElement = 0; // Flag identifying this faction as special forces. Used for armory filters.
    isWeaponsElement = 0; // Flag identifying this faction as a weapons team. Used for armory filters.

    class Role {
        name = "";          // Name of the role.
        callsign = "";      // Callsign used for this role specifically. Requires this element to be unique.
        showOnCommcard = 0; // Whether to show this role on the Commcard.

        isHidden = 0; // Hides or unhides the role in the armory interface.
        isZeus   = 0; // Used for quick identification of Zeus units for Zeus-only features.

        /**
         * ACE feature flags.
         *
         * Used to identify roles that require specific ACE features be enabled.
         * Reference ACE documentation for information on the numbers used.
         */
        aceIsMedic    = 0;
        aceIsEngineer = 0;
        aceIsEOD      = 0;

        /**
         * ACE GForce coefficient.
         *
         * Used to define the GForce tolerance of a unit. Default value is 1. Lower
         * values will result in an improved tolerance of GForces.
         */
        aceGForceCoef = 1;

        /**
         * ACE Stamina factor.
         *
         * Used to define the stamina of a unit. Default value is 1. Higher values will
         * result in an improved level of stamina.
         */
        aceStaminaFactor = 1;

        /**
         * Enables ACRE Babel translation.
         *
         * When set to 1, the unit will be able to interpret all Babel languages.
         */
        acreBabelInterpreter = 0;

        /**
         * Defines ACRE radio channels.
         *
         * First and second slots are for personal radios, such as the 343 and 148.
         * Third and fourth slots are for element radios, such as the 152 and 117.
         *
         * If a slot is set to zero, no radio will be issued. If a slot is not zero,
         * a device corresponding to acreRadioDevices in CfgPlayableFactions will be
         * issued with the defined channel number. At least one slot should be zero.
         */
        acreRadioChannels[] = {0,0,0,0};

        /**
         * Defines allowed accessory levels.
         *
         * Each parameter of the array corresponds to a weapon's attachment slots, 
         * in the order of Top Rail, Side Rail, Bottom Rail and Barrel. It additionally
         * accepts options for the non-weapon Chestpack and Head Mount slots. These
         * can be omitted, and will default to 0.
         *
         * The number defined sets the maximum level of accessories that can be 
         * equipped by this role for the given slot. The number is entirely arbitary,
         * but is always treated as "this level and all below" then filtering. For
         * example, defining all 4X scopes with a level of 4 on the top rail.
         *
         * Refer to the accessory definitions for details of the accessory types defined
         * at each level.
         */
        allowedAccessories[] = {0,0,0,0,0,0};
    };
};