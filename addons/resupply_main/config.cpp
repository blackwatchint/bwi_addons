#include <\resupply\defines.hpp> // Absolute path

class CfgPatches
{
	class bwi_resupply_main
	{
		requiredVersion = 1.0;
		authors[] = {"Fourjays"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
	    version = 1.1;
	    versionStr = "1.1.0";
	    versionAr[] = {1,1,0};
		requiredAddons[] = {
            "bwi_resupply"
		};
		units[] = {};
	};
};

class CfgVehicles
{
	#include <cfgVehicles.hpp>
};