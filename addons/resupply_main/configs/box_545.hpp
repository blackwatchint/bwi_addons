/**
 * RPK-74
 */
class BWI_Resupply_Box_60Rnd_RPK: BWI_Resupply_CrateBaseSmall2 {
	scope = 2;
	displayName = "5.45mm 60Rnd RPK";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(22,UK3CB_RPK74_60rnd_545x39_GM);
		MACRO_ADDMAGAZINE(16,UK3CB_RPK74_60rnd_545x39_GT);
	};

	class TransportItems {
		MACRO_ADDITEM(2,ACE_SpareBarrel);
	};
};