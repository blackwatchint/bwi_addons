/**
 * ACE ropes
 */
class BWI_Resupply_Veh_Rope: BWI_Resupply_CrateBaseSmall1 {
	scope = 2;
	displayName = "Towing Ropes";

 	hiddenSelectionsTextures[] = {
 		"",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportItems {
		MACRO_ADDITEM(1,ACE_rope6);
		MACRO_ADDITEM(1,ACE_rope12);
		MACRO_ADDITEM(1,ACE_rope18);
		MACRO_ADDITEM(1,ACE_rope36);
	};
};
