/**
 * Corpsman Usage
 */
class BWI_Resupply_Medical_Bandages: BWI_Resupply_CrateBaseSmall3 {
	scope = 2;
	displayName = "Assorted Bandages";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_3.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportItems {
		MACRO_ADDITEM(30,ACE_fieldDressing);
		MACRO_ADDITEM(30,ACE_packingBandage);
		MACRO_ADDITEM(30,ACE_elasticBandage);
		MACRO_ADDITEM(30,ACE_quikClot);
		MACRO_ADDITEM(20,ACE_splint);
		MACRO_ADDITEM(10,ACE_tourniquet);
	};
};

class BWI_Resupply_Medical_Medication: BWI_Resupply_Medical_Bandages {
	displayName = "Assorted Medication";

	class TransportItems {
		MACRO_ADDITEM(20,ACE_morphine);
		MACRO_ADDITEM(20,ACE_epinephrine);
		MACRO_ADDITEM(10,ACE_adenosine);
		MACRO_ADDITEM(12,ACE_plasmaIV_500);
		MACRO_ADDITEM( 8,ACE_plasmaIV_250);
		MACRO_ADDITEM(12,ACE_salineIV_500);
		MACRO_ADDITEM( 8,ACE_salineIV_250);
	};
};

class BWI_Resupply_Medical_Bodybags: BWI_Resupply_Medical_Bandages {
	displayName = "Bodybags";

	class TransportItems {
		MACRO_ADDITEM(24,ACE_bodyBag);
	};
};