/**
 * FIM-92F Stinger
 */
class BWI_Resupply_AA_1Rnd_FIM92: BWI_Resupply_CrateBaseLong1 {
	scope = 2;
	displayName = "FIM-92F Missiles";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 2,rhs_fim92_mag);
	};
};


/**
 * Fliegerfaust
 */
class BWI_Resupply_AA_1Rnd_Fliegerfaust: BWI_Resupply_AA_1Rnd_FIM92 {
	displayName = "Fliegerfaust Missiles";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_1.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 2,BWA3_Fliegerfaust_Mag);
	};
};


/**
 * 9K38 Igla
 */
class BWI_Resupply_AA_1Rnd_9K38: BWI_Resupply_AA_1Rnd_FIM92 {
	displayName = "9K38 Missiles";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE( 2,rhs_mag_9k38_rocket);
	};
};
