/**
 * M240B, M240G
 */
class BWI_Resupply_Box_100Rnd_M80: BWI_Resupply_CrateBaseSmall3 {
	scope = 2;
	displayName = "7.62mm 100Rnd M80";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_1.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,rhsusf_100Rnd_762x51);
		MACRO_ADDMAGAZINE( 6,rhsusf_100Rnd_762x51_m62_tracer);
	};

	class TransportItems {
		MACRO_ADDITEM(2,ACE_SpareBarrel);
	};
};

class BWI_Resupply_Box_100Rnd_M80A1: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 100Rnd M80A1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,rhsusf_100Rnd_762x51_m80a1epr);
		MACRO_ADDMAGAZINE( 6,rhsusf_100Rnd_762x51_m62_tracer);
	};
};

class BWI_Resupply_Box_100Rnd_M61AP: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 100Rnd M61 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,rhsusf_100Rnd_762x51_m61_ap);
	};
};

class BWI_Resupply_Box_50Rnd_M80_SP: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 50Rnd M80 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_50Rnd_762x51);
		MACRO_ADDMAGAZINE(12,rhsusf_50Rnd_762x51_m62_tracer);
	};
};

class BWI_Resupply_Box_50Rnd_M80A1_SP: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 50Rnd M80A1 (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_50Rnd_762x51_m80a1epr);
		MACRO_ADDMAGAZINE(12,rhsusf_50Rnd_762x51_m62_tracer);
	};
};

class BWI_Resupply_Box_50Rnd_M61AP_SP: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 50Rnd M61 AP (Softpack)";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_50Rnd_762x51_m61_ap);
	};
};


/**
 * MG3, MG5
 */
class BWI_Resupply_Box_120Rnd_MG5: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 120Rnd MG5";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ger_signs_1.paa",
 		"\bwi_resupply_main\data\ger_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(8,BWA3_120Rnd_762x51_soft);
		MACRO_ADDMAGAZINE(5,BWA3_120Rnd_762x51_Tracer_soft);
	};
};

class BWI_Resupply_Box_120Rnd_MG3: BWI_Resupply_Box_120Rnd_MG5 {
	displayName = "7.62mm 120Rnd MG3";
};

class BWI_Resupply_Box_250Rnd_MG3: BWI_Resupply_Box_120Rnd_MG5 {
	displayName = "7.62mm 250Rnd MG3";
	
	class TransportMagazines {
		MACRO_ADDMAGAZINE(5,UK3CB_MG3_250rnd_762x51_GM);
		MACRO_ADDMAGAZINE(5,UK3CB_MG3_250rnd_762x51_GT);
	};
};


/**
 * RPK
 */
class BWI_Resupply_Box_75Rnd_RPK: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 75Rnd 57-N-231 RPK";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(24,rhs_75Rnd_762x39mm);
		MACRO_ADDMAGAZINE(18,rhs_75Rnd_762x39mm_tracer);
	};
};


/**
 * M84
 */
class BWI_Resupply_Box_250Rnd_M30: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 250Rnd M30";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\ser_signs_1.paa",
 		"\bwi_resupply_main\data\ser_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(6,rhssaf_250Rnd_762x54R);
	};
};


/**
 * PKP, PKM
 */
class BWI_Resupply_Box_100Rnd_57N323S: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 100Rnd 57-N-323S";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\rus_signs_1.paa",
 		"\bwi_resupply_main\data\rus_color_k.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,rhs_100Rnd_762x54mmR);
		MACRO_ADDMAGAZINE( 6,rhs_100Rnd_762x54mmR_green);
	};
};

class BWI_Resupply_Box_100Rnd_7N13: BWI_Resupply_Box_100Rnd_57N323S {
	displayName = "7.62mm 100Rnd 7N13 AP";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,rhs_100Rnd_762x54mmR_7N13);
	};
};


/**
 * M60 
 */
class BWI_Resupply_Box_100Rnd_M13: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm EPR 100Rnd M13-Linked Belt";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,UK3CB_100Rnd_762x51_B_M60);
		MACRO_ADDMAGAZINE( 6,UK3CB_100Rnd_762x51_T_M60);
	};
};



/**
 * Negev
 */
class BWI_Resupply_Box_150Rnd_Negev: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 150Rnd Negev";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(10,150Rnd_762x54_Box);
		MACRO_ADDMAGAZINE( 6,150Rnd_762x54_Box_Tracer);
	};
};


/**
 * HK G3/SG1
 */
class BWI_Resupply_Box_50Rnd_G3SG1: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 50Rnd G3/SG1";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(12,UK3CB_G3_50rnd_762x51_G);
		MACRO_ADDMAGAZINE( 8,UK3CB_G3_50rnd_762x51_GT);
	};
};


/**
 * Bren Light Machine Gun
 */
class BWI_Resupply_Box_30Rnd_BrenLMG: BWI_Resupply_Box_100Rnd_M80 {
	displayName = "7.62mm 30Rnd Bren LMG";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(24,UK3CB_Bren_30Rnd_762x51_Magazine_R);
		MACRO_ADDMAGAZINE(18,UK3CB_Bren_30Rnd_762x51_Magazine_RT);
	};
};