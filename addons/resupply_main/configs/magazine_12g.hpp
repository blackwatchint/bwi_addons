/**
 * M590 (Long)
 */
class BWI_Resupply_Mag_8Rnd_Buck: BWI_Resupply_CrateBaseSmall1 {
	scope = 2;
	displayName = "12g 8Rnd Buckshot";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\usa_signs_2.paa",
 		"\bwi_resupply_main\data\usa_color_g.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_8Rnd_00Buck);
	};	
};

class BWI_Resupply_Mag_8Rnd_Slug: BWI_Resupply_Mag_8Rnd_Buck {
	displayName = "12g 8Rnd Slug";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,rhsusf_8Rnd_Slug);
	};	
};


/**
 * M590 (Short)
 */
class BWI_Resupply_Mag_5Rnd_Buck: BWI_Resupply_Mag_8Rnd_Buck {
	displayName = "12g 5Rnd Buck";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhsusf_5Rnd_00Buck);
	};	
};

class BWI_Resupply_Mag_5Rnd_Slug: BWI_Resupply_Mag_8Rnd_Buck {
	displayName = "12g 5Rnd Slug";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(30,rhsusf_5Rnd_Slug);
	};	
};


/**
 * L128A1
 */
class BWI_Resupply_Mag_8Rnd_L128_Buck: BWI_Resupply_Mag_8Rnd_Buck {
	displayName = "12g 8Rnd Buckshot";

 	hiddenSelectionsTextures[] = {
 		"\bwi_resupply_main\data\gbr_signs_2.paa",
 		"\bwi_resupply_main\data\gbr_color_b.paa"
 	};

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,UK3CB_BAF_12G_Pellets);
	};	
};

class BWI_Resupply_Mag_8Rnd_L128_Slug: BWI_Resupply_Mag_8Rnd_L128_Buck {
	displayName = "12g 8Rnd Slug";

	class TransportMagazines {
		MACRO_ADDMAGAZINE(20,UK3CB_BAF_12G_Slugs);
	};	
};