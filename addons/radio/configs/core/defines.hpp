/** Adds infantry phone. */
#define MACRO_ADD_PHONE \
	acre_infantryPhoneControlActions[] = {"intercom_1"}; \
	acre_infantryPhoneIntercom[] = {"all"}; \
	acre_hasInfantryPhone = 1; \
	delete acre_infantryPhonePosition;