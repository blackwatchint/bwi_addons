class Car_F;
class Truck_F;
class Tank;
class Helicopter;
class Plane;


/** Set up broad inherited radios for other vehicles.
 *
 * Modded vehicles may inherit from here, but consider
 * writing configs for a specific modded vehicle instead
 * of changing these core radio rack / intercom configs.
 */


// Cars
/* No broad inheritance. Define radio configs in their modName.hpp */


// Trucks
class Truck_01_base_F : Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};
class Truck_02_base_F : Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};
class Truck_03_base_F : Truck_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// MRAPs
class MRAP_01_base_F: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};
class MRAP_02_base_F: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};
class MRAP_03_base_F: Car_F {
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", {"cargo", 0}};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
};


// Tank-like vehicles (MBTs, but also used by other tracked vehicles)
class Tank_F: Tank {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,1}}}; // Turret 0,1 is loader.
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander", {"turret", {0,1}}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
	};
};


// Wheeled / Tracked APCs and IFVs
class Wheeled_APC_F: Car_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};
class APC_Tracked_01_base_F: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};
class APC_Tracked_02_base_F: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};
class APC_Tracked_03_base_F: Tank_F {
	MACRO_ADD_PHONE
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {"Intercom_1"};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"commander"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "gunner", "commander", {"cargo","all"}, {"ffv", "all"}, {"turret", "all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"commander"};
			connectedByDefault = 0;
		};
	};
};



/* Rotary - do not define {turret} seats in limitedPositions[] here. Co-pilots are often turret seats.
 *
 * Example for any Intercom (Crew, PAX, etc)
 *  allowedPositions[] = {"driver", "copilot"}    where "copilot" could be resolved to {"turret", {0}} by ACRE2
 *  limitedPositions[] = {"turret", "all"}        where "turret" is some door gunner seat you wanted to give e.g. PAX Intercom access to
 *
 * This will result in some seats (either co-pilot or ALL OTHER TURRET SEATS) not getting Intercom access.
 * If a helicopter needs Intercom access for door guns or any other "turret" seat, make a separate radio config for that helicopter.
 */
class Helicopter_Base_F: Helicopter {
	acre_hasInfantryPhone = 0;
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
	};
};
class Helicopter_Base_H: Helicopter_Base_F {
	acre_hasInfantryPhone = 0;
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};
class Heli_light_03_base_F: Helicopter_Base_F {
	acre_hasInfantryPhone = 0;
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner"};
			disabledPositions[] = {};
			limitedPositions[] = {{"cargo","all"}};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
		class Intercom_2 {
			displayName = "PAX Intercom";
			shortName = "PAX";
			allowedPositions[] = {"driver", "copilot", {"cargo","all"}};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 0;
			masterPosition[] = {"driver"};
			connectedByDefault = 0;
		};
	};
};


// Fixed Wing
class Plane_Base_F: Plane {
	acre_hasInfantryPhone = 0;
	class AcreRacks {
		class Rack_1 {
			displayName = "Radio Rack 1";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_2 {
			displayName = "Radio Rack 2";
			shortName = "R.2";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"driver"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_3 {
			displayName = "Radio Rack 3";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
		class Rack_4 {
			displayName = "Radio Rack 4";
			shortName = "R.1";
			componentName = "ACRE_VRC103";
			allowedPositions[] = {"copilot", "gunner", "commander"};
			disabledPositions[] = {};
			mountedRadio = "ACRE_PRC117F";
			isRadioRemovable = 0;
			intercom[] = {};
		};
	};
	class AcreIntercoms {
		class Intercom_1 {
			displayName = "Crew Intercom";
			shortName = "Crew";
			allowedPositions[] = {"driver", "copilot", "gunner", "commander"};
			disabledPositions[] = {};
			limitedPositions[] = {};
			numLimitedPositions = 1;
			masterPosition[] = {"driver"};
			connectedByDefault = 1;
		};
	};
};