#include <defines.hpp>
class CfgPatches {
    class bwi_radio {
        requiredVersion = 1;
        authors[] = { "Dhorkiy", "Andrew" };
        authorURL = "http://blackwatch-int.com";
        author = "Black Watch International";
        version = 1.2;
        versionStr = "1.2.0";
        versionAr[] = {1,2,0};
        requiredAddons[] = {
            "acre_main",
            "acre_sys_rack"
        };
        units[] = {};
    };
};

class CfgVehicles {
	#include <cfgVehicles.hpp>
};