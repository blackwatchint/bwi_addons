/**
 * ctrlIDCs: _colCallsign, _colPrimaryNet, _colPrimaryBlk, _colPrimaryChn, _colSecondaryNet, _colSecondaryBlk, _colSecondaryChn
 */
params ["_display", "_ctrlIDCs"];

private _colCallsign     = _display displayctrl (_ctrlIDCs select 0);
private _colPrimaryNet   = _display displayctrl (_ctrlIDCs select 1); 
private _colPrimaryBlk   = _display displayctrl (_ctrlIDCs select 2);
private _colPrimaryChn   = _display displayctrl (_ctrlIDCs select 3);
private _colSecondaryNet = _display displayctrl (_ctrlIDCs select 4);
private _colSecondaryBlk = _display displayctrl (_ctrlIDCs select 5);
private _colSecondaryChn = _display displayctrl (_ctrlIDCs select 6);

// Initialize strings for building each column.
private _rowsCallsign = "";
private _rowsPrimaryNet = "";
private _rowsPrimaryBlk = "";
private _rowsPrimaryChn = "";
private _rowsSecondaryNet = "";
private _rowsSecondaryBlk = "";
private _rowsSecondaryChn = "";

// Only process if bwi_armory faction is specified.
if ( !isNil "bwi_armory_selectedFaction" ) then {
	// Get the faction and role data from armory.
	private _factionData = bwi_armory_selectedFaction;
	private _roleData = bwi_armory_selectedRole;

	// Pointer to the faction configs.
	private _factionClasses = _factionData splitString "/";
	private _factionCfg = configFile >> "CfgPlayableFactions" >> _factionClasses select 0 >> _factionClasses select 1;

	// Find all player groups.
	private _playerGroups = [];
	{
		if ( side _x == playerSide ) then {
			_playerGroups pushBackUnique group _x;
		};
	} forEach allPlayers;

	// Iterate over the groups, building each column a row at a time.
	{
		// Use the group's leader when loading the element's data.
		private _playerSlotIdArr = [player] call bwi_common_fnc_getSlotIdArray;
		private _groupSlotIdArr  = [leader _x] call bwi_common_fnc_getSlotIdArray;
		private _groupCallsign = groupId _x;

		// Load all role's that should be displayed on the commcard for the group's element.
		private _roleCfgs = "getNumber (_x >> 'showOnCommcard') == 1" configClasses ( configFile >> "CfgPlayableRoles" >> _groupSlotIdArr select 1 );

		// Iterate over the roles and display them on the commcard.
		{
			// Get the radio configurations.
			private _radioDevices = getArray (_factionCfg >> "acreRadioDevices");
			private _radioChannels = getArray (_x >> "acreRadioChannels");

			// Iterate over the configuration data to identify the radio nets.
			private _hasPrimary   = false;
			private _hasSecondary = false;
			private _primaryRadio   = ["","",""];
			private _secondaryRadio = ["","",""];

			{
				private _device  = _radioDevices select _forEachIndex;
				private _channel = _radioChannels select _forEachIndex;
				private _block   = "-";
				private _net     = "N/A";

				// Device and channel are specified.
				if ( _device != "NONE" && _channel > 0 ) then {
					// For PRC343 radios make channel block and channel the element number.
					if ( _device in ["ACRE_PRC343"] ) then {
						_block   = _channel;
						_channel = _groupSlotIdArr select 2;
					};

					// For PRC148, PRC152 and PRC117 radios, find the net name.
					if ( _device in ["ACRE_PRC148", "ACRE_PRC152", "ACRE_PRC117F"] ) then {
						_net = [_device, "bwi", _channel, "label"] call acre_api_fnc_getPresetChannelField;
					};

					// For the SEM52, SEM70 and PRC77 radios, use the frequency as the net name.
					if ( _device in ["ACRE_SEM52SL", "ACRE_SEM70", "ACRE_PRC77"] ) then {
						// Note these radios have unreliable presets so we use the ACRE_PRC148's.
						private _freq = ["ACRE_PRC148", "bwi", _channel, "frequencyRX"] call acre_api_fnc_getPresetChannelField;
						_net = format["%1 MHz", _freq toFixed 2];
					};

					// Add the data to the primary or secondary radio slot.
					if ( !_hasPrimary ) then {
						_hasPrimary = true;
						_primaryRadio = [_net, _block, _channel];
					} else {
						if ( !_hasSecondary ) then {
							_hasSecondary = true;
							_secondaryRadio = [_net, _block, _channel];
						};
					};
				};
			} forEach _radioDevices;

			// Append the role ID if the group displays multiple roles.
			private _appendRoleId = "";
			if ( count _roleCfgs > 1 ) then {
				_appendRoleId = format [" (%1)", configName(_x)];
			};

			// Highlight the entry if it matches the player's group, group number...
			private _rowColor = "#000000";
			if ( _playerSlotIdArr select 1 == _groupSlotIdArr select 1 && _playerSlotIdArr select 2 == _groupSlotIdArr select 2 ) then { 
				// ...and is either the only role displayed on the commcard or it is their role.
				if ( count _roleCfgs == 1 || _playerSlotIdArr select 3 == toLower configName(_x) ) then {
					_rowColor = "#d50000";
				};
			};

			// Add the data for this row to each column of rows.
			// Important that empty columns are processed for the correct number of lines.
			_rowsCallsign     = format["<t color='%3'>%1%2</t><br/>", _rowsCallsign, _groupCallsign + _appendRoleId, _rowColor];
			_rowsPrimaryNet   = format["<t color='%3'>%1%2</t><br/>", _rowsPrimaryNet, _primaryRadio select 0, _rowColor];
			_rowsPrimaryBlk   = format["<t color='%3'>%1%2</t><br/>", _rowsPrimaryBlk, _primaryRadio select 1, _rowColor];
			_rowsPrimaryChn   = format["<t color='%3'>%1%2</t><br/>", _rowsPrimaryChn, _primaryRadio select 2, _rowColor];
			_rowsSecondaryNet = format["<t color='%3'>%1%2</t><br/>", _rowsSecondaryNet, _secondaryRadio select 0, _rowColor];
			_rowsSecondaryBlk = format["<t color='%3'>%1%2</t><br/>", _rowsSecondaryBlk, _secondaryRadio select 1, _rowColor];
			_rowsSecondaryChn = format["<t color='%3'>%1%2</t><br/>", _rowsSecondaryChn, _secondaryRadio select 2, _rowColor];
		} forEach _roleCfgs;
	} forEach _playerGroups;
};

// Assign columns of text to the display elements.
_colCallsign ctrlSetStructuredText parseText _rowsCallsign;
_colPrimaryNet ctrlSetStructuredText parseText _rowsPrimaryNet;
_colPrimaryBlk ctrlSetStructuredText parseText _rowsPrimaryBlk;
_colPrimaryChn ctrlSetStructuredText parseText _rowsPrimaryChn;
_colSecondaryNet ctrlSetStructuredText parseText _rowsSecondaryNet;
_colSecondaryBlk ctrlSetStructuredText parseText _rowsSecondaryBlk;
_colSecondaryChn ctrlSetStructuredText parseText _rowsSecondaryChn;