class BwiGui_RscStructuredText;
class BwiGui_RscPicture;

class BwiDialogCommcard
{
	idd = 6500;
	movingenable = 0;
	onLoad = "[_this select 0, [6502, 6503, 6504, 6505, 6506, 6507, 6508]] call bwi_commcard_fnc_refreshCommcard;";

	class ControlsBackground {
		class CommcardBackground: BwiGui_RscPicture {
			idc = 6501;
			text = "\bwi_commcard\gui\data\commcard_bg.paa";
			x = 0.350 * safezoneW + safezoneX;
			y = 0.192 * safezoneH + safezoneY;
			w = 0.299 * safezoneW;
			h = 0.606 * safezoneH;
		};
	};


	class Controls
	{
		class colCallsign: BwiGui_RscStructuredText {
			idc = 6502;
			//colorBackground[] = {0,0,1,0.3};
			x = 0.3541 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0996 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};

		class colPrimaryNet: BwiGui_RscStructuredText {
			idc = 6503;
			//colorBackground[] = {0,1,0,0.3};
			x = 0.4542 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0472 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};

		class colPrimaryBlk: BwiGui_RscStructuredText {
			idc = 6504;
			//colorBackground[] = {1,0,0,0.3};
			x = 0.5022 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0234 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};

		class colPrimaryChn: BwiGui_RscStructuredText {
			idc = 6505;
			//colorBackground[] = {1,1,0,0.3};
			x = 0.5268 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0234 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};

		class colSecondaryNet: BwiGui_RscStructuredText {
			idc = 6506;
			//colorBackground[] = {0,1,0,0.3};
			x = 0.5502 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0472 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};

		class colSecondaryBlk: BwiGui_RscStructuredText {
			idc = 6507;
			//colorBackground[] = {1,0,0,0.3};
			x = 0.5982 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0234 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};

		class colSecondaryChn: BwiGui_RscStructuredText {
			idc = 6508;
			//colorBackground[] = {1,1,0,0.3};
			x = 0.6210 * safezoneW + safezoneX;
			y = 0.2628 * safezoneH + safezoneY;
			w = 0.0234 * safezoneW;
			h = 0.5270 * safezoneH;
			size = 0.022 * safezoneH;
			shadow = 0;

		  	class Attributes {
		   		color = "#000000";
		  	};
		};
	};
};