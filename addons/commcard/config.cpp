class CfgPatches
{
	class bwi_commcard
	{
		requiredVersion = 1.0;
		authors[] = {"Fourjays", "Tebro", "Abel", "Ifu"};
		author = "Black Watch International";
		url = "http://blackwatch-int.com";
	    version = 4.1;
	    versionStr = "4.1.0";
	    versionAr[] = {4,1,0};
		requiredAddons[] = {
			"acre_main",
			"bwi_common",
			"bwi_data",
			"bwi_armory"
		};
		units[] = {};
	};
};

class CfgFunctions 
{
	#include <cfgFunctions.hpp>
};

class CfgVehicles
{
	#include <cfgVehicles.hpp>
};

// Common GUI Definitions
#include <\common\gui\macros.hpp> // Absolute path

// GUI Definitions
#include <gui\commcard.hpp>