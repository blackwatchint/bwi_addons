class Man;
class CAManBase: Man {
    class ACE_SelfActions {
        class ACE_Equipment {
            /**
             * Interaction for opening the Commcard GUI to 
             * see a rendered list of elements and their 
             * radio channels.
             */
            class BWI_CommCard_OpenCommCard {
                displayName = "Open CommCard";
                condition = "true";
                statement = "createDialog 'BwiDialogCommCard';";
                showDisabled = 0;
                priority = 0.1;
                icon = "\bwi_commcard\data\icon.paa";
                exceptions[] = {"notOnMap"};
            };
        };
        
        class ACE_TeamManagement {
            /**
             * Interaction for resetting the player's callsign 
			 * to its original value.
             */
            class BWI_CommCard_ResetCallsign {
                displayName = "Reset Callsign";
                condition = "leader group _player == _player";
                statement = "[_player] call bwi_commcard_fnc_resetCallsign;";
                showDisabled = 1;
                priority = 0.1;
                icon = "\bwi_commcard\data\icon.paa";
                exceptions[] = {"notOnMap"};
            };
        };
    };
};