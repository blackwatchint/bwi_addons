class bwi_commcard
{
	class functions
	{
		file = "\bwi_commcard\functions";

		class resetCallsign{};
	};

	class gui_functions
	{
		file = "\bwi_commcard\gui\functions";

		class refreshCommcard{};
	};
};
