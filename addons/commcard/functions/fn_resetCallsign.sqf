params ["_unit"];

// Only run for group leader.
if ( leader group _unit == _unit ) then {
	// Only run if using an Armory loadout.
	if ( !isNil "bwi_armory_selectedFaction" && !isNil "bwi_armory_selectedRole" ) then {
		// Get the faction and role data from armory.
		private _factionData = bwi_armory_selectedFaction;
		private _roleData = bwi_armory_selectedRole;

		// Call setUnitCallsign to reset the callsign.
		[_unit, _factionData, _roleData] call bwi_armory_fnc_setUnitCallsign;

		// Output a notification.
		private _message = parseText format["Callsign reset to %1.", groupId (group _unit)];
		["Callsign Reset", _message, 2] call bwi_common_fnc_notification;
	};
};
