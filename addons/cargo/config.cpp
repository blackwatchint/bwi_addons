class CfgPatches {
	class bwi_cargo {
		requiredVersion = 1;
		authors[] = { "Fourjays", "RedBery", "Tebro" };
		authorURL = "http://blackwatch-int.com";
		author = "Black Watch International";
		version = 2.5;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"ace_cargo"
		};
		units[] = {};
	};
};

class CfgFunctions {
	#include <cfgFunctions.hpp>
};

class Cfg3DEN {
	#include <cfgEden.hpp>
};

class CfgVehicles {
	#include <cfgVehicles.hpp>
};

// CBA Extended Event Handlers
#include <cfgExtendedEventHandlers.hpp>