class Extended_PostInit_EventHandlers {
    class bwi_cargo_postInit {
        init = "call bwi_cargo_fnc_initCargo;";
    };
};

class Extended_Init_EventHandlers{
    class AllVehicles{
        class bwi_cargo_init {
            init = "_this call bwi_cargo_fnc_initVehicleCargo";
        };
    };
};