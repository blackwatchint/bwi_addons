params ["_vehicle"];

private _cargoLeft = [_vehicle] call ace_cargo_fnc_getCargoSpaceLeft; 
private _maxCargo = getNumber(configfile >> "cfgVehicles" >> (typeOf _vehicle) >> "ace_cargo_space");
private _secondaryCargo = getNumber(configfile >> "cfgVehicles" >> (typeOf _vehicle) >> "bwi_secondary_cargo_space");

(_maxCargo - _cargoLeft) > _secondaryCargo