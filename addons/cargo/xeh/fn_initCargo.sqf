["ace_cargoLoaded", {
	params ["_item", "_vehicle"];

	if (local _vehicle) then {
		if([_vehicle] call bwi_cargo_fnc_isAboveSecondaryCargoCap) then {
			if(count (getVehicleCargo _vehicle) == 0) then {
				_vehicle enableVehicleCargo false;
			}
			else {
				[_item,_vehicle] call ACE_cargo_fnc_unloadItem;
			}; 
		};
		
	};

}] call CBA_fnc_addEventHandler;

["ace_cargoUnloaded", {
	params ["_item", "_vehicle"];

	if (local _vehicle) then {
		if(!([_vehicle] call bwi_cargo_fnc_isAboveSecondaryCargoCap)) then {
			_vehicle enableVehicleCargo true;
		};
	};

}] call CBA_fnc_addEventHandler;