params ["_vehicle"];

if (local _vehicle) then { 
	if([_vehicle] call bwi_cargo_fnc_isAboveSecondaryCargoCap) then { 
		if(count (getVehicleCargo _vehicle) == 0) then { 
			_vehicle enableVehicleCargo false; 
		}; 
	}; 
};