class Car;
class Tank;
class Van_02_base_F;
class Offroad_01_military_base_F;
class Heli_Transport_04_base_F;
class Helicopter;
class Plane;
class Ship;
class Boat_F;
class NATO_Box_Base;
class EAST_Box_Base;
class IND_Box_Base;

/**
 * CARS
 * Standard = 4 / 100
 * Armored Cars = 1 / 100
 * Rear Mounted Weapon = 1 / 100
 * Support = 1 / 100
**/
class Car_F: Car {
	maximumLoad = 100;
};
class Truck_F: Car_F {
	maximumLoad = 50;
};
class Wheeled_APC_F: Car_F {
	maximumload = 150;
};
class Hatchback_01_base_F: Car_F {
	maximumLoad = 100;
};
class MRAP_01_base_F: Car_F {
	maximumLoad = 100;
};
class MRAP_02_base_F: Car_F {
	maximumLoad = 100;
};
class MRAP_03_base_F: Car_F {
	maximumLoad = 100;
};
class Offroad_01_base_F: Car_F {
	maximumLoad = 100;
};
class Offroad_01_armed_base_F: Offroad_01_military_base_F {
	ace_cargo_space = 1;
};
class Offroad_01_AT_base_F: Offroad_01_military_base_F {
	ace_cargo_space = 1;
};
class Offroad_02_base_F: Car_F {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 1;
	maximumLoad = 100;
};
class Quadbike_01_base_F: Car_F {
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 1;
	maximumLoad = 0;
};
class LSV_01_base_F: Car_F {
	maximumLoad = 100;
};
class LSV_02_base_F: Car_F {
	maximumLoad = 100;
};


/**
 * TRUCKS
 * Standard = 8 / 50
 * Flatbed = 16 / 50
 * Container = 32 / 100
 * Rear Mounted Weapon = 1 / 50
 * Ammo = 3 / 50
 * Repair = 6 / 50
**/
class Truck_01_base_F: Truck_F {  // HEMETT
	maximumLoad = 50;
};
class B_Truck_01_transport_F: Truck_01_base_F {
	ace_cargo_space = 18;
	maximumLoad = 100;
};
class B_Truck_01_covered_F: B_Truck_01_transport_F {
	ace_cargo_space = 18;
	maximumLoad = 100;
};
class B_Truck_01_mover_F: B_Truck_01_transport_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class B_Truck_01_box_F: B_Truck_01_mover_F {
	ace_cargo_space = 32;
	maximumLoad = 100;
};
class B_Truck_01_Repair_F: B_Truck_01_mover_F {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class B_Truck_01_ammo_F: B_Truck_01_mover_F {
	ace_cargo_space = 3;
	maximumLoad = 50;
};
class B_Truck_01_fuel_F: B_Truck_01_mover_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class B_Truck_01_medical_F: B_Truck_01_transport_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Truck_02_base_F: Truck_F { // Kamaz
	ace_cargo_space = 16;
	maximumLoad = 100;
};
class Truck_02_transport_base_F: Truck_02_base_F {
	ace_cargo_space = 16;
	maximumLoad = 100;
};
class Truck_02_box_base_F: Truck_02_base_F {
	ace_cargo_space = 4;
	maximumLoad = 50;
};
class Truck_02_medical_base_F: Truck_02_box_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Truck_02_Ammo_base_F: Truck_02_base_F {
	ace_cargo_space = 3; // Space wheel.
	maximumLoad = 50;
};
class Truck_02_fuel_base_F: Truck_02_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Truck_03_base_F: Truck_F { // Typhoon
	ace_cargo_space = 16;
	maximumLoad = 100;
};
class O_Truck_03_transport_F: Truck_03_base_F { // Typhoon
	ace_cargo_space = 18;
	maximumLoad = 100;
};
class O_Truck_03_covered_F: Truck_03_base_F {
	ace_cargo_space = 18;
	maximumLoad = 100;
};
class O_Truck_03_repair_F: Truck_03_base_F {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class O_Truck_03_ammo_F: Truck_03_base_F {
	ace_cargo_space = 3; // Space wheel.
	maximumLoad = 50;
};
class O_Truck_03_fuel_F: Truck_03_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class O_Truck_03_medical_F: Truck_03_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class O_Truck_03_device_F: Truck_03_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Van_01_base_F: Truck_F { // Civilian
	maximumLoad = 50;
};
class Van_01_box_base_F: Van_01_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Van_01_fuel_base_F: Van_01_base_F {
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Truck_02_MRL_base_F: Truck_02_base_F
{
	ace_cargo_space = 1; // Space wheel.
	maximumLoad = 50;
};
class Van_02_transport_base_F: Van_02_base_F {
	ace_cargo_space = 4;
};
class Van_02_medevac_base_F: Van_02_base_F {
	ace_cargo_space = 1;
};


/**
 * ARMOR
 * Standard = 4 / 150
 * Light APC = 2 / 50
**/
class Tank_F: Tank {
	maximumLoad = 150;
};
class LT_01_base_F: Tank_F
{
	ace_cargo_space = 2;
	maximumLoad = 50;
};
class AFV_Wheeled_01_base_F: Wheeled_APC_F
{
	ace_cargo_space = 2;
	maximumLoad = 150;
};


/**
 * HELICOPTERS
 * Standard = 4 / 25
 * Light = 2 / 25
 * Medium = 6 / 50
 * Large = 8 / 75
 * Gunship = 0 / 25
 * Light Attack = 0 / 25
**/
class Helicopter_Base_F: Helicopter {
	ace_cargo_space = 4;
	maximumLoad = 25;
};
class Helicopter_Base_H: Helicopter_Base_F {
	maximumLoad = 25;
};
class Heli_Light_01_base_F: Helicopter_Base_H {
	ace_cargo_space = 2;
	ace_cargo_hasCargo = 1;
	maximumLoad = 25;
};
class Heli_Light_01_armed_F: Heli_Light_01_base_F {
	ace_cargo_space = 0;
	ace_cargo_hasCargo = 0;
	maximumLoad = 25;	
};
class Heli_Light_02_base_F: Helicopter_Base_H {
	ace_cargo_space = 3;
	maximumLoad = 25;
};
class Heli_light_03_base_F: Helicopter_Base_F {
	ace_cargo_space = 3;
	maximumLoad = 25;
};
class Heli_Transport_01_base_F: Helicopter_Base_H {
	ace_cargo_space = 6;
	maximumLoad = 50;
};
class Heli_Transport_02_base_F: Helicopter_Base_H {
	ace_cargo_space = 8;
	maximumLoad = 75;
};
class Heli_Transport_03_base_F: Helicopter_Base_H {
	ace_cargo_space = 8;
	maximumLoad = 75;
};
class O_Heli_Transport_04_box_F: Heli_Transport_04_base_F {
	ace_cargo_space = 16;
	ace_cargo_hasCargo = 1;
	maximumLoad = 75;
};
class O_Heli_Transport_04_repair_F: Heli_Transport_04_base_F {
	ace_cargo_space = 0;
	ace_cargo_hasCargo = 0;
	maximumLoad = 0;
};
class O_Heli_Transport_04_ammo_F: Heli_Transport_04_base_F {
	ace_cargo_space = 0;
	ace_cargo_hasCargo = 0;
	maximumLoad = 0;
};
class O_Heli_Transport_04_fuel_F: Heli_Transport_04_base_F {
	ace_cargo_space = 0;
	ace_cargo_hasCargo = 0;
	maximumLoad = 0;
};
class O_Heli_Transport_04_medevac_F: Heli_Transport_04_base_F {
	ace_cargo_space = 4;
	ace_cargo_hasCargo = 1;
	maximumLoad = 25;
};
class Heli_Attack_01_base_F: Helicopter_Base_F {
	ace_cargo_space = 0;
	ace_cargo_hasCargo = 0;
	maximumLoad = 25;
};
class Heli_Attack_02_base_F: Helicopter_Base_F {
	ace_cargo_space = 2;
	maximumLoad = 25;
};


/**
 * PLANES
 * Standard = 8 / 25
 * Transport = 24 / 150
 * Fighter = 0 / 25
**/
class Plane_Base_F: Plane {
	maximumLoad = 25;
};
class VTOL_Base_F: Plane_Base_F {
	maximumLoad = 150;
	ace_cargo_hasCargo = 1;
	ace_cargo_space = 24;
};
class VTOL_01_Base_F: VTOL_Base_F {
	maximumLoad = 150;
};
class VTOL_02_base_F: VTOL_Base_F {
	maximumLoad = 150;
};
class VTOL_01_armed_base_F: VTOL_01_Base_F {
	maximumLoad = 150;
	ace_cargo_space = 12;
};


/**
 * BOATS
 * Standard = 4 / 150
 * Inflatable = 1
**/
class Ship_F: Ship {
    maximumLoad = 150;
};
class Boat_Civil_01_base_F: Ship_F {
   ace_cargo_hasCargo = 1;
   ace_cargo_space = 1;
   maximumLoad = 0;
};
class Rubber_duck_base_F: Boat_F {
   ace_cargo_hasCargo = 1;
   ace_cargo_space = 1;
   maximumLoad = 0;
};
class Scooter_Transport_01_base_F: Rubber_duck_base_F {
   ace_cargo_hasCargo = 0;
   ace_cargo_space = 0;
};
class Boat_Armed_01_base_F: Boat_F {
   ace_cargo_space = 4;
   maximumLoad = 150;
};


/**
 * SPECIAL
 *  CASES
**/
class Box_NATO_AmmoVeh_F: NATO_Box_Base {
	ace_cargo_canLoad = 0;
};
class Box_East_AmmoVeh_F: East_Box_Base {
	ace_cargo_canLoad = 0;
};
class Box_IND_AmmoVeh_F: IND_Box_Base {
	ace_cargo_canLoad = 0;
};
