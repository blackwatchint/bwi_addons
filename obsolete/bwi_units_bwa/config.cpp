#include "defines.hpp"

class CfgPatches {
	class bwi_units_bwa {
		requiredVersion = 1;
		author = "Black Watch International";
		authors[] = { "Fourjays" };
		authorURL = "http://blackwatch-int.com";
		version = 2.5.0;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"rhs_main",
			"rhsusf_main",
			"rhsgref_main",
			"bwi_units",
			"bwa3_weapons",
			"bwa3_leopard2a6m",
			"bwa3_puma"
		};
		units[] = {			
			#include "configs\b_danish_army\classes.hpp"
			#include "configs\b_norwegian_army_d\classes.hpp"
			#include "configs\b_norwegian_army_w\classes.hpp"
			#include "configs\b_finnish_defence_forces_w\classes.hpp"
			#include "configs\b_finnish_defence_forces_s\classes.hpp"
			#include "configs\b_swedish_army_w\classes.hpp"
			#include "configs\b_swedish_army_d\classes.hpp"
			#include "configs\b_swedish_army_a\classes.hpp"
			#include "configs\b_dutch_army_w\classes.hpp"
			#include "configs\b_dutch_army_d\classes.hpp"
			#include "configs\b_belgian_army_w\classes.hpp"
			#include "configs\b_belgian_army_d\classes.hpp"
		};
	};
};


class CfgFactionClasses {	
	#include "cfgFactionClasses.hpp"
};

class CfgVehicles {
	#include "cfgVehicles.hpp"
};

class CfgWeapons {
	#include "cfgWeapons.hpp"
};

class CfgGroups {
	#include "cfgGroups.hpp"
};

class CfgMarkers 
{
	#include "cfgMarkers.hpp"
};