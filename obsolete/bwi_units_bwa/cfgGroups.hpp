class West
{
	name="$STR_A3_CfgGroups_West0";
	#include "configs/b_danish_army/groups.hpp"
	#include "configs/b_norwegian_army_d/groups.hpp"
	#include "configs/b_norwegian_army_w/groups.hpp"
	#include "configs/b_finnish_defence_forces_w/groups.hpp"
	#include "configs/b_finnish_defence_forces_s/groups.hpp"
	#include "configs/b_swedish_army_w/groups.hpp"
	#include "configs/b_swedish_army_d/groups.hpp"
	#include "configs/b_swedish_army_a/groups.hpp"
	#include "configs/b_dutch_army_w/groups.hpp"
	#include "configs/b_dutch_army_d/groups.hpp"
	#include "configs/b_belgian_army_w/groups.hpp"
	#include "configs/b_belgian_army_d/groups.hpp"
};