// Shared base classes.
class B_Soldier_base_F;
class O_Soldier_base_F;
class C_man_1;

class B_APC_Wheeled_01_cannon_F;

class rhs_bmp2_chdkz;
class rhsusf_rg33_wd;
class rhsusf_rg33_m2_wd;
class rhsusf_m113d_usarmy;
class rhsusf_m113_usarmy;
class rhsusf_m1025_w;
class rhsusf_m1025_d;
class rhsusf_m1025_w_m2;
class rhsusf_m1025_d_m2;

class BWA3_Leopard2A6M_Fleck;
class BWA3_Leopard2A6M_Tropen;
class BWA3_Eagle_Fleck;
class BWA3_Eagle_Tropen;
class BWA3_Eagle_FLW100_Fleck;
class BWA3_Eagle_FLW100_Tropen;

// Add ammo to backpacks.
#include "configs/backpack_ammo.hpp"
	
// Include faction configs.
#include "configs/b_danish_army/units.hpp"
#include "configs/b_norwegian_army_d/units.hpp"
#include "configs/b_norwegian_army_w/units.hpp"
#include "configs/b_finnish_defence_forces_w/units.hpp"
#include "configs/b_finnish_defence_forces_s/units.hpp"
#include "configs/b_swedish_army_w/units.hpp"
#include "configs/b_swedish_army_d/units.hpp"
#include "configs/b_swedish_army_a/units.hpp"
#include "configs/b_dutch_army_w/units.hpp"
#include "configs/b_dutch_army_d/units.hpp"
#include "configs/b_belgian_army_w/units.hpp"
#include "configs/b_belgian_army_d/units.hpp"