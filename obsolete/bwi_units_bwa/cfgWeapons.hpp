// Shared base classes.
class UniformItem;
class Uniform_Base;
class U_B_CombatUniform_mcam: Uniform_Base {
	class ItemInfo;
};

class V_PlateCarrier1_rgr;
class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr {
	class ItemInfo;
};

class H_HelmetB;
class H_HelmetIA: H_HelmetB {
	class ItemInfo;
};


// Add scopes to weapons.
#include "configs/weapon_scopes.hpp"

// Include faction configs.
#include "configs/b_danish_army/gear.hpp"
#include "configs/b_norwegian_army_d/gear.hpp"
#include "configs/b_norwegian_army_w/gear.hpp"
#include "configs/b_finnish_defence_forces_w/gear.hpp"
#include "configs/b_finnish_defence_forces_s/gear.hpp"
#include "configs/b_swedish_army_w/gear.hpp"
#include "configs/b_swedish_army_d/gear.hpp"
#include "configs/b_swedish_army_a/gear.hpp"
#include "configs/b_dutch_army_w/gear.hpp"
#include "configs/b_dutch_army_d/gear.hpp"
#include "configs/b_belgian_army_w/gear.hpp"
#include "configs/b_belgian_army_d/gear.hpp"