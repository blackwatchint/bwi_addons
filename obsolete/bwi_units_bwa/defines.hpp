#define MACRO_ADDWEAPON(WEAPON,COUNT) class _xx_##WEAPON { weapon = #WEAPON; count = COUNT; }
#define MACRO_ADDITEM(ITEM,COUNT) class _xx_##ITEM { name = #ITEM; count = COUNT; }
#define MACRO_ADDMAGAZINE(MAGAZINE,COUNT) class _xx_##MAGAZINE { magazine = #MAGAZINE; count = COUNT; }
#define MACRO_ADDBACKPACK(BACKPACK,COUNT) class _xx_##BACKPACK { backpack = #BACKPACK; count = COUNT; }

#define NO_SIDE -1
#define EAST 0
#define WEST 1
#define RESISTANCE 2
#define CIVILIAN 3
#define NEUTRAL 4
#define ENEMY 5
#define FRIENDLY 6
#define LOGIC 7