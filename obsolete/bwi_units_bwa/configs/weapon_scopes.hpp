class BWA3_MG5;
class BWA3_MG5_w_EotechMag: BWA3_MG5 {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "BWA3_optic_EOTech_Mag_Off";
		};
	};		
};

class BWA3_G27;
class BWA3_G27_w_ACOG2: BWA3_G27 {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG2_USMC";
		};
	};		
};

class BWA3_G38;
class BWA3_G38_w_EOTech: BWA3_G38 {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "BWA3_optic_EOTech";
		};
	};		
};

class BWA3_G38_AG;
class BWA3_G38_AG_w_ACOG2: BWA3_G38_AG {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG2";
		};
	};		
};

class BWA3_G38;
class BWA3_G38_w_ACOG3: BWA3_G38 {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG3";
		};
	};		
};