class BWI_Flag_SWE {
	scope = 1;
	name = "Sweden";
	markerClass = "Flags";
	icon = "\bwi_units_bwa\data\markers\mkr_swe.paa";
	texture = "\bwi_units_bwa\data\markers\mkr_swe.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};