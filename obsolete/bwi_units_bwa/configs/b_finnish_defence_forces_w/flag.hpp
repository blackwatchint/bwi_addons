class BWI_Flag_FIN {
	scope = 1;
	name = "Finland";
	markerClass = "Flags";
	icon = "\bwi_units_bwa\data\markers\mkr_fin.paa";
	texture = "\bwi_units_bwa\data\markers\mkr_fin.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};