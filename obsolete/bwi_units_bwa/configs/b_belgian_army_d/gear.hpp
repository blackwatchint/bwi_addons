class BWI_Uniform_BEL_JIGA: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (Jigsaw Arid - Belgium)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_JigsawArid_Belgium.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_BEL_JIGA_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_BEL_JIGA: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (Jigsaw Arid - Belgium)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\N_Vests_JigsawArid_Belgium.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_BEL_JIGA: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (Jigsaw Arid - Belgium)";
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Helmet_JigsawArid_Belgium.paa"
	};
};