class BWI_Soldier_SWE_M90AW_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_SWE_M90AW_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_SWE_A";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F", 
		"Head_Euro",  
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_SWE_M90AW",
		"BWI_Vest_SWE_M90AW",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_SWE_M90AW",
		"BWI_Vest_SWE_M90AW",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_SWE_M90AW";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_M90AW_Sweden.paa"
	};
};


class BWI_Soldier_SWE_M90AW_AT: BWI_Soldier_SWE_M90AW_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"rhs_weap_M136",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"rhs_weap_M136",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_SWE_M90AW_AR: BWI_Soldier_SWE_M90AW_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_CarryAll_oli_f_m249";
};


class BWI_Soldier_SWE_M90AW_DMR: BWI_Soldier_SWE_M90AW_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"BWA3_G38_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"BWA3_G38_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36_Tracer",
		"BWA3_30Rnd_556x45_G36_Tracer",
		"BWA3_30Rnd_556x45_G36_Tracer",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36",
		"BWA3_30Rnd_556x45_G36_Tracer",
		"BWA3_30Rnd_556x45_G36_Tracer",
		"BWA3_30Rnd_556x45_G36_Tracer",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_SWE_M90AW_TL: BWI_Soldier_SWE_M90AW_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_osw_GL_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_osw_GL_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
	};
};


class BWI_Soldier_SWE_M90AW_MAT: BWI_Soldier_SWE_M90AW_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_oli_f_MAAWS";
};


class BWI_Soldier_SWE_M90AW_MMG: BWI_Soldier_SWE_M90AW_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_oli_f_mg3";
};


class BWI_Soldier_SWE_M90AW_CRW: BWI_Soldier_SWE_M90AW_R {
	displayName = "Crew";
	weapons[] = {
		"SMG_05_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"SMG_05_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_SWE_M90AW",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_SWE_M90AW",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_SWE_Leopard2A6M_A: BWA3_Leopard2A6M_Fleck {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_SWE_A";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_SWE_M90AW_CRW";
};

class BWI_Vehicle_SWE_PAMV_A: B_APC_Wheeled_01_cannon_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_SWE_A";
	vehicleClass = "rhs_vehclass_apc";
	displayName = "Patria AMV";
	crew = "BWI_Soldier_SWE_M90AW_CRW";
};

class rhsusf_rg33_d;
class BWI_Vehicle_SWE_RG33_A: rhsusf_rg33_wd {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_SWE_A";
	crew = "BWI_Soldier_SWE_M90AW_R";
};

class rhsusf_rg33_m2_d;
class BWI_Vehicle_SWE_RG33_M2_A: rhsusf_rg33_m2_wd {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_SWE_A";
	crew = "BWI_Soldier_SWE_M90AW_R";
};