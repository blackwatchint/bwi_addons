class BWI_Uniform_SWE_M90AW: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (M90AW - Sweden)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_M90AW_Sweden.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_SWE_M90AW_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_SWE_M90AW: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (M90AW - Sweden)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\N_Vests_M90AW_Sweden.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_SWE_M90AW: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (M90AW - Sweden)";
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Helmet_M90AW_Sweden.paa"
	};
};