class BWI_FACTION_NOR_D
{
	name = "Norwegian Army (Desert)";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_NOR_D
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_NOR_D";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_NOR_D
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_NOR_D";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_NOR_D
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_NOR_D";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_NOR_D
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_NOR_D";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_NOR_D
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_NOR_D";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NOR_M03_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};