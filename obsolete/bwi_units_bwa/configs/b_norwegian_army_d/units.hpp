class BWI_Soldier_NOR_M03_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_NOR_M03_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NOR_D";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENGB_F", 
		"Head_Euro", 
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_hk416d10_w_Holo",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_hk416d10_w_Holo",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_NOR_M03",
		"BWI_Vest_NOR_M03",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_NOR_M03",
		"BWI_Vest_NOR_M03",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_NOR_M03";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_M03_Norway.paa"
	};
};


class BWI_Soldier_NOR_M03_AT: BWI_Soldier_NOR_M03_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_hk416d10_w_Holo",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_hk416d10_w_Holo",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_NOR_M03_AR: BWI_Soldier_NOR_M03_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_CarryAll_cbr_f_m249";
};


class BWI_Soldier_NOR_M03_DMR: BWI_Soldier_NOR_M03_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_NOR_M03_TL: BWI_Soldier_NOR_M03_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_hk416d10_m320_w_ACOG",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_hk416d10_m320_w_ACOG",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_NOR_M03_MAT: BWI_Soldier_NOR_M03_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_hk416d10_w_Holo",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_hk416d10_w_Holo",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_cbr_f_MAAWS";
};


class BWI_Soldier_NOR_M03_MMG: BWI_Soldier_NOR_M03_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_cbr_f_mg3";
};


class BWI_Soldier_NOR_M03_CRW: BWI_Soldier_NOR_M03_R {
	displayName = "Crew";
	weapons[] = {
		"SMG_05_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"SMG_05_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	linkedItems[] = {
		"rhsusf_cvc_helmet",
		"BWI_Vest_NOR_M03",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_helmet",
		"BWI_Vest_NOR_M03",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWA3_Leopard2A6M_Tropen;
class BWI_Vehicle_NOR_Leopard2A6M_D: BWA3_Leopard2A6M_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NOR_D";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_NOR_M03_CRW";
};

class rhsusf_m113d_usarmy;
class BWI_Vehicle_NOR_M113_D: rhsusf_m113d_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NOR_D";
	crew = "BWI_Soldier_NOR_M03_CRW";
};

class rhsusf_m1025_d;
class BWI_Vehicle_NOR_M025_D: rhsusf_m1025_d {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NOR_D";
	crew = "BWI_Soldier_NOR_M03_R";
};

class rhsusf_m1025_d_m2;
class BWI_Vehicle_NOR_M025_M2_D: rhsusf_m1025_d_m2 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NOR_D";
	crew = "BWI_Soldier_NOR_M03_R";
};