class BWI_FACTION_NED
{
	name = "Royal Netherlands Army (Woodland)";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_NED
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_NED";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_NED
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_NED";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_NED
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_NED";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_NED
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_NED";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_NED
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_NED";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_NED_DPM_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};