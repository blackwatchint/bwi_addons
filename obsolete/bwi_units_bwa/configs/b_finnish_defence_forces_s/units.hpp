class BWI_Soldier_FIN_M05L_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_FIN_M05L_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_FIN_S";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F", 
		"Head_Euro", 
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_RK62",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_RK62",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_FIN_M05L",
		"BWI_Vest_FIN_M05L",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_FIN_M05L",
		"BWI_Vest_FIN_M05L",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_FIN_M05L";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_M05L_Finland.paa"
	};
};


class BWI_Soldier_FIN_M05L_AT: BWI_Soldier_FIN_M05L_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_RK62",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_RK62",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_FIN_M05L_AR: BWI_Soldier_FIN_M05L_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_cbr_f_pkm";
};


class BWI_Soldier_FIN_M05L_DMR: BWI_Soldier_FIN_M05L_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_FIN_M05L_TL: BWI_Soldier_FIN_M05L_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_RK62",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_RK62",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_FIN_M05L_CRW: BWI_Soldier_FIN_M05L_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_akms",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akms",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	linkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_FIN_M05L",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_FIN_M05L",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};