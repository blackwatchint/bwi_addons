class BWI_Soldier_BEL_JIG_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_BEL_JIG_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_BEL";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENGB_F", 
		"Head_Euro", 
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_BEL_JIG",
		"BWI_Vest_BEL_JIG",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_BEL_JIG",
		"BWI_Vest_BEL_JIG",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_BEL_JIG";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_Jigsaw_Belgium.paa"
	};
};


class BWI_Soldier_BEL_JIG_AT: BWI_Soldier_BEL_JIG_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_BEL_JIG_AR: BWI_Soldier_BEL_JIG_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_CarryAll_oli_f_m249";
};


class BWI_Soldier_BEL_JIG_DMR: BWI_Soldier_BEL_JIG_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_BEL_JIG_TL: BWI_Soldier_BEL_JIG_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_osw_GL_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_osw_GL_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_BEL_JIG_MAT: BWI_Soldier_BEL_JIG_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw_w_Eotech",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_oli_f_Titan";
};


class BWI_Soldier_BEL_JIG_MMG: BWI_Soldier_BEL_JIG_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m240B_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m240B_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_oli_f_M240";
};


class BWI_Soldier_BEL_JIG_CRW: BWI_Soldier_BEL_JIG_R {
	displayName = "Crew";
	weapons[] = {
		"hlc_rifle_falosw",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_falosw",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_BEL_JIG",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_BEL_JIG",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};

class BWI_Vehicle_BEL_Eagle_W: BWA3_Eagle_Fleck {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_BEL";
	crew = "BWI_Soldier_BEL_JIG_R";
};

class BWI_Vehicle_BEL_Eagle_FLW100_W: BWA3_Eagle_FLW100_Fleck {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_BEL";
	crew = "BWI_Soldier_BEL_JIG_R";
};