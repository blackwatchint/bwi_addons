class BWI_Uniform_NED_DCU: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (DCU - Netherlands)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_3CO_Netherlands.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_NED_DCU_R";
		containerClass = "Supply40";
		mass = 40;
	};
};