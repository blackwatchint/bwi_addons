class BWI_Soldier_NED_DCU_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_NED_DCU_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NED_D";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENGB_F", 
		"Head_Euro", 
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_USA_DCU",
		"BWI_Vest_USA_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_USA_DCU",
		"BWI_Vest_USA_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_NED_DCU";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_3CO_Netherlands.paa"
	};
};


class BWI_Soldier_NED_DCU_AT: BWI_Soldier_NED_DCU_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_NED_DCU_AR: BWI_Soldier_NED_DCU_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_CarryAll_cbr_f_m249";
};


class BWI_Soldier_NED_DCU_DMR: BWI_Soldier_NED_DCU_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_NED_DCU_TL: BWI_Soldier_NED_DCU_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_m203_w_ACOG",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_m203_w_ACOG",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
};


class BWI_Soldier_NED_DCU_MAT: BWI_Soldier_NED_DCU_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_cbr_f_Titan";
};


class BWI_Soldier_NED_DCU_MMG: BWI_Soldier_NED_DCU_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m240B_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m240B_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"rhs_mag_m67",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc",
		"rhs_mag_an_m8hc"
	};
	backpack = "B_Carryall_cbr_f_M240";
};


class BWI_Soldier_NED_DCU_CRW: BWI_Soldier_NED_DCU_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"rhs_mag_m67",
		"rhs_mag_an_m8hc"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_NED_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_NED_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWA3_Leopard2A6M_Tropen;
class BWI_Vehicle_NED_Leopard2A6M_D: BWA3_Leopard2A6M_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NED_D";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_NED_DCU_CRW";
};

class BWI_Vehicle_NED_Eagle_D: BWA3_Eagle_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NED_D";
	crew = "BWI_Soldier_NED_DCU_R";
};

class BWI_Vehicle_NED_Eagle_FLW100_D: BWA3_Eagle_FLW100_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_NED_D";
	crew = "BWI_Soldier_NED_DCU_R";
};