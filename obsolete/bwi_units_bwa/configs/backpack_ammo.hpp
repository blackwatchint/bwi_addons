class B_Carryall_khk;
class B_Carryall_khk_f_MG5: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(BWA3_120Rnd_762x51, 4);
		MACRO_ADDMAGAZINE(BWA3_120Rnd_762x51_Tracer, 2);
	};
};

class B_Carryall_cbr;
class B_Carryall_cbr_f_MG5: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(BWA3_120Rnd_762x51, 4);
		MACRO_ADDMAGAZINE(BWA3_120Rnd_762x51_Tracer, 2);
	};
};

class B_Carryall_oli;
class B_Carryall_oli_f_MG5: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(BWA3_120Rnd_762x51, 4);
		MACRO_ADDMAGAZINE(BWA3_120Rnd_762x51_Tracer, 2);
	};
};