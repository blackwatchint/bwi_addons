class BWI_FACTION_DEN
{
	name = "Royal Danish Army";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_DEN
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_DEN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_DEN
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_DEN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_DEN
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_DEN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_DEN
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_DEN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_DEN
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_DEN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_DEN_M11_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};