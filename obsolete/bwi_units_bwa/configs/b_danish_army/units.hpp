class BWI_Soldier_DEN_M11_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_DEN_M11_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENGB_F", 
		"Head_Euro", 
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_DEN_M11",
		"BWI_Vest_DEN_M11",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_DEN_M11",
		"BWI_Vest_DEN_M11",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_DEN_M11";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_bwa\data\I_Clothing_MultiCam_Denmark.paa"
	};
};


class BWI_Soldier_DEN_M11_AT: BWI_Soldier_DEN_M11_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"rhs_weap_M136",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"rhs_weap_M136",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_DEN_M11_AR: BWI_Soldier_DEN_M11_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_M60E4_w_ACOG2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_M60E4_w_ACOG2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_khk_f_m60";
};


class BWI_Soldier_DEN_M11_DMR: BWI_Soldier_DEN_M11_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"BWA3_G27_w_ACOG2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"BWA3_20Rnd_762x51_G28_Tracer",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_DEN_M11_TL: BWI_Soldier_DEN_M11_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_m203_w_ACOG",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_m203_w_ACOG",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_DEN_M11_MAT: BWI_Soldier_DEN_M11_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_w_CompM4",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_khk_f_MAAWS";
};


class BWI_Soldier_DEN_M11_MMG: BWI_Soldier_DEN_M11_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	
	weapons[] = {
		"hlc_lmg_M60E4_w_ACOG2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_M60E4_w_ACOG2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_khk_f_m60";
};


class BWI_Soldier_DEN_M11_CRW: BWI_Soldier_DEN_M11_R {
	displayName = "Crew";
	weapons[] = {
		"SMG_05_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"SMG_05_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"30Rnd_9x21_Mag_SMG_02",
		"HandGrenade",
		"SmokeShell"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_DEN_M11",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_DEN_M11",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_DEN_Leopard2A6M_W: BWA3_Leopard2A6M_Fleck {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_DEN_M11_CRW";
};

class BWI_Vehicle_DEN_Leopard2A6M_D: BWA3_Leopard2A6M_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_DEN_M11_CRW";
};

class BWI_Vehicle_DEN_M113_W: rhsusf_m113_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_CRW";
};

class BWI_Vehicle_DEN_M113_D: rhsusf_m113d_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_CRW";
};

class BWI_Vehicle_DEN_M025_W: rhsusf_m1025_w {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_M025_D: rhsusf_m1025_d {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_M025_M2_W: rhsusf_m1025_w_m2 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_M025_M2_D: rhsusf_m1025_d_m2 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_Eagle_W: BWA3_Eagle_Fleck {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_Eagle_D: BWA3_Eagle_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_Eagle_FLW100_W: BWA3_Eagle_FLW100_Fleck {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};

class BWI_Vehicle_DEN_Eagle_FLW100_D: BWA3_Eagle_FLW100_Tropen {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_DEN";
	crew = "BWI_Soldier_DEN_M11_R";
};