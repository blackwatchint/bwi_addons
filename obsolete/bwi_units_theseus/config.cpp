#include "defines.hpp"

class CfgPatches {
	class bwi_units_theseus {
		requiredVersion = 1;
		author = "Black Watch International";
		authors[] = { "Fourjays", "Tebro" };
		authorURL = "http://blackwatch-int.com";
		version = 2.5.0;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"rhs_main",
			"rhsusf_main",
			"bwi_units",
			"tacs_main",
			"Taliban_Fighters"
		};
		units[] = {			
			#include "configs\i_islamic_state\classes.hpp"
			#include "configs\i_al_shabaab\classes.hpp"
			#include "configs\o_iraqi_army_desert_storm\classes.hpp"
			#include "configs\o_vietcong\classes.hpp"
			#include "configs\o_islamic_state\classes.hpp"
			#include "configs\o_al_shabaab\classes.hpp"
		};
	};
};


class CfgFactionClasses {	
	#include "cfgFactionClasses.hpp"
};

class CfgVehicles {
	#include "cfgVehicles.hpp"
};

class CfgGroups {
	#include "cfgGroups.hpp"
};

class CfgMarkers 
{
	#include "cfgMarkers.hpp"
};