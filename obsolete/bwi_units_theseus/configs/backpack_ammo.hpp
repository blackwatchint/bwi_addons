class tacs_Backpack_Carryall_DarkBlack;
class tacs_Backpack_Carryall_DarkBlack_f_rpg7: tacs_Backpack_Carryall_DarkBlack {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_rpg7_PG7VL_mag, 2);
		MACRO_ADDMAGAZINE(rhs_rpg7_OG7V_mag, 2);
	};
};

class tacs_Backpack_Carryall_DarkBlack_f_rpk74: tacs_Backpack_Carryall_DarkBlack {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_545x39_t_rpk, 18);
	};
};

class tacs_Backpack_Carryall_DarkBlack_f_pkm: tacs_Backpack_Carryall_DarkBlack {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR, 4);
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR_green, 2);
	};
};