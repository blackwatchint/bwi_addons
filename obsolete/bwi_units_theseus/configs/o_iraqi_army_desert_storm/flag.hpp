class BWI_Flag_IRQ_DS {
	scope = 1;
	name = "Iraq (1991-2004)";
	markerClass = "Flags";
	icon = "\bwi_units_theseus\data\markers\mkr_irq.paa";
	texture = "\bwi_units_theseus\data\markers\mkr_irq.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};