class BWI_Soldier_IRQ_DS_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_IRQ_DS_GEN_R";
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_TK", 
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhsgref_helmet_M1_bare",
		"rhsgref_otv_khaki",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsgref_helmet_M1_bare",
		"rhsgref_otv_khaki",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "tacs_Uniform_Garment_LS_GS_GP_BB";
};


class BWI_Soldier_IRQ_DS_GEN_AT: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akms",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akms",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_IRQ_DS_GEN_AR: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpk";
};


class BWI_Soldier_IRQ_DS_GEN_DMR: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_IRQ_DS_GEN_TL: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"H_Beret_blk",
		"rhsgref_otv_khaki",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Beret_blk",
		"rhsgref_otv_khaki",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Soldier_IRQ_DS_GEN_MAT: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_IRQ_DS_GEN_MMG: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_pkm";
};


class BWI_Soldier_IRQ_DS_GEN_CRW: BWI_Soldier_IRQ_DS_GEN_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"rhs_tsh4",
		"rhsgref_otv_khaki",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"rhsgref_otv_khaki",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_IRQ_DS_T72_D: rhs_t72ba_tv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	crew = "BWI_Soldier_IRQ_DS_GEN_CRW";
};

class BWI_Vehicle_IRQ_DS_BMP1_D: rhs_bmp1_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	crew = "BWI_Soldier_IRQ_DS_GEN_CRW";
};

class BWI_Vehicle_IRQ_DS_BRDM_D: rhsgref_BRDM2_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	crew = "BWI_Soldier_IRQ_DS_GEN_CRW";
};

class BWI_Vehicle_IRQ_DS_BRDM_ATGM_D: rhsgref_BRDM2_ATGM_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	crew = "BWI_Soldier_IRQ_DS_GEN_CRW";
};

class BWI_Vehicle_IRQ_DS_BTR60_D: rhs_btr60_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	crew = "BWI_Soldier_IRQ_DS_GEN_CRW";
};

class BWI_Vehicle_IRQ_DS_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRQ_DS";
	crew = "BWI_Soldier_IRQ_DS_GEN_R";
};