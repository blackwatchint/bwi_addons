class BWI_Flag_SOMJIH {
	scope = 1;
	name = "Al-Shabaab";
	markerClass = "Flags";
	icon = "\bwi_units_theseus\data\markers\mkr_isis.paa";
	texture = "\bwi_units_theseus\data\markers\mkr_isis.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};