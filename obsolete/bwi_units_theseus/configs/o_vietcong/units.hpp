class BWI_Soldier_VC_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_VC_GEN_R";
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_VC";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageCHI_F",
		"Head_Asian",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_BS_BP_BB";
};


class BWI_Soldier_VC_GEN_AT: BWI_Soldier_VC_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_ak47",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ak47",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_BS_BP_BB";
};


class BWI_Soldier_VC_GEN_AR: BWI_Soldier_VC_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_oli_f_rpk";
	linkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_RS_BS_BP_BB";
};


class BWI_Soldier_VC_GEN_DMR: BWI_Soldier_VC_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_kar98k",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_kar98k",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_RS_GS_GP_BB";
};


class BWI_Soldier_VC_GEN_TL: BWI_Soldier_VC_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_m38",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m38",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	linkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_cbr_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_cbr_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_GS_GP_BB";
};


class BWI_Soldier_VC_GEN_MMG: BWI_Soldier_VC_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_BS_GP_BB";
};


class BWI_Soldier_VC_GEN_R2: BWI_Soldier_VC_GEN_R {
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_cbr_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_cbr_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_GS_BP_BB";
};
class BWI_Soldier_VC_GEN_AT2: BWI_Soldier_VC_GEN_AT {
	linkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_cbr_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_cbr_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_GS_GP_BB";
};
class BWI_Soldier_VC_GEN_AR2: BWI_Soldier_VC_GEN_AR {
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_BS_BP_BB";
};
class BWI_Soldier_VC_GEN_DMR2: BWI_Soldier_VC_GEN_DMR {
	linkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_cbr_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_cbr_F"
	};
	uniformClass = "tacs_Uniform_Garment_LS_BS_GP_BB";
};
class BWI_Soldier_VC_GEN_TL2: BWI_Soldier_VC_GEN_TL {
	linkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_cbr_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"V_TacChestrig_cbr_F"
	};
	uniformClass = "tacs_Uniform_Garment_RS_BS_BP_BB";
};
class BWI_Soldier_VC_GEN_MMG2: BWI_Soldier_VC_GEN_MMG {
	linkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_grn_F"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_oli",
		"V_TacChestrig_grn_F"
	};
	uniformClass = "tacs_Uniform_Garment_RS_GS_GP_BB";
};