class BWI_Flag_VC {
	scope = 1;
	name = "Vietcong";
	markerClass = "Flags";
	icon = "\bwi_units_theseus\data\markers\mkr_vc.paa";
	texture = "\bwi_units_theseus\data\markers\mkr_vc.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};