class BWI_Soldier_JIH_O_GEN_R : BWI_Soldier_JIH_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_AT: BWI_Soldier_JIH_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_AR: BWI_Soldier_JIH_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_DMR: BWI_Soldier_JIH_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_TL: BWI_Soldier_JIH_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_MAT: BWI_Soldier_JIH_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_MMG: BWI_Soldier_JIH_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};
class BWI_Soldier_JIH_O_GEN_CRW: BWI_Soldier_JIH_GEN_CRW {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
};


class BWI_Vehicle_JIH_O_T72: BWI_Vehicle_JIH_T72 {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	crew = "BWI_Soldier_JIH_O_GEN_CRW";
};
class BWI_Vehicle_JIH_O_BMP1: BWI_Vehicle_JIH_BMP1 {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	crew = "BWI_Soldier_JIH_O_GEN_CRW";
};
class BWI_Vehicle_JIH_O_BTR60: BWI_Vehicle_JIH_BTR60 {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	crew = "BWI_Soldier_JIH_O_GEN_CRW";
};
class BWI_Vehicle_JIH_O_M025: BWI_Vehicle_JIH_M025 {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	crew = "BWI_Soldier_JIH_O_GEN_R";
};
class BWI_Vehicle_JIH_O_M025_M2: BWI_Vehicle_JIH_M025_M2 {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	crew = "BWI_Soldier_JIH_O_GEN_R";
};
class BWI_Vehicle_JIH_O_Tech_M2: BWI_Vehicle_JIH_Tech_M2 {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_JIH_O_GEN_R";
};
class BWI_Vehicle_JIH_O_Tech: BWI_Vehicle_JIH_Tech {
	side = EAST;
	faction = "BWI_FACTION_JIH_O";
	crew = "BWI_Soldier_JIH_O_GEN_R";
};