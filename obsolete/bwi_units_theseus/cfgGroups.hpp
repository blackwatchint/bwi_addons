class Indep
{
	name="$STR_A3_CfgGroups_Indep0";
	#include "configs/i_islamic_state/groups.hpp"	
	#include "configs/i_al_shabaab/groups.hpp"	
};

class East
{
	name="$STR_A3_CfgGroups_East0";
	#include "configs/o_iraqi_army_desert_storm/groups.hpp"	
	#include "configs/o_vietcong/groups.hpp"	
	#include "configs/o_islamic_state/groups.hpp"	
	#include "configs/o_al_shabaab/groups.hpp"	
};