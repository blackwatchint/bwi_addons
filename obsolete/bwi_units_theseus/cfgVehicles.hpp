// Shared base classes.
class B_Soldier_base_F;

class rhs_t72ba_tv;
class rhs_btr60_msv;
class rhs_bmp1_msv;
class rhs_bmp2_msv;
class rhs_btr60_msv;
class rhsgref_ins_g_btr60;
class rhsgref_BRDM2_msv;
class rhsgref_BRDM2_ATGM_msv;
class rhs_uaz_open_chdkz;
class rhsusf_m1025_d;
class rhsusf_m1025_d_m2;
class B_G_Offroad_01_armed_F;
class I_G_Offroad_01_F;


// Add ammo to backpacks.
#include "configs/backpack_ammo.hpp"

// Include faction configs.
#include "configs/i_islamic_state/units.hpp"
#include "configs/i_al_shabaab/units.hpp"
#include "configs/o_iraqi_army_desert_storm/units.hpp"
#include "configs/o_vietcong/units.hpp"
#include "configs/o_islamic_state/units.hpp"
#include "configs/o_al_shabaab/units.hpp"