// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2003;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_cu_ucp"; };

	case "af_fwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};

	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_cu_ucp";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsusf_ach_helmet_ESS_ucp"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": { _unit addHeadgear "rhsusf_ach_helmet_headset_ess_ucp"; };	

	case "re_sni";
	case "re_spo": { _unit addHeadgear "rhs_Booniehat_ucp"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "rhsusf_cvc_helmet"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "rhsusf_patrolcap_ucp"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhsusf_iotv_ucp_rifleman"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs": { _unit addVest "rhsusf_iotv_ucp_squadleader"; };

	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": { _unit addVest "rhsusf_iotv_ucp_Teamleader"; };	

	case "ft_ammg";
	case "ft_lmg";
	case "ft_mmg": { _unit addVest "rhsusf_iotv_ucp_saw"; };

	case "pm_cpm": { _unit addVest "rhsusf_iotv_ucp_Medic"; };	

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "rhsusf_assault_eagleaiii_ucp"; };

	case "pl_ptl";
	case "pl_pts";
	case "re_fac": { _unit addBackpack "B_Kitbag_cbr"; };

	case "pe_dem";
	case "ft_ammg";
	case "ft_amat";
	case "ft_hat";
	case "ft_ahat";
	case "ft_aaag": { _unit addBackpack "B_Carryall_cbr"; };

	case "re_mtg": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_htg": { _unit addBackpack "rhs_Tow_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "rhs_TOW_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_cbr"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"