// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2007;
_equipment = "IN";
_side = resistance;


// Uniform.
[_unit, ["rhsgref_uniform_para_ttsko_mountain", "rhsgref_uniform_flecktarn", "rhsgref_uniform_woodland_olive",
		 "rhsgref_uniform_ttsko_mountain","rhsgref_uniform_ttsko_forest", "rhsgref_uniform_para_ttsko_oxblood",
		 "rhs_uniform_mvd_izlom", "tacs_Uniform_Combat_LS_GS_BP_BB", "tacs_Uniform_Combat_LS_CLBS_GP_BB",
		 "tacs_Uniform_Combat_LS_GS_TP_BB", "tacs_Uniform_Combat_LS_CPS_BP_BB"]
] call BWI_fnc_AddRandomUniform;


// Helmet.
[_unit, ["rhs_fieldcap_vsr", "rhs_6b26_bala_green", "rhs_6b27m_green_bala", "rhs_6b7_1m_ess_bala",
		 "H_Watchcap_cbr", "H_Watchcap_camo", "H_Watchcap_khk", "rhs_6b28_green", "rhs_6b27m_green",
		 "rhs_6b7_1m", "rhs_6b7_1m_bala2", "rhs_6b7_1m_bala1", "rds_Woodlander_cap3", "H_Cap_oli"]
] call BWI_fnc_AddRandomHeadgear;


// Vest.
[_unit, ["V_TacVest_camo", "V_TacVest_oli", "V_TacVest_khk", "V_TacVest_blk",
		 "V_I_G_resistanceLeader_F", "rhs_vydra_3m"]
] call BWI_fnc_AddRandomVest;


// Backpack.
switch ( _role ) do {
	default {
		[_unit, ["B_FieldPack_cbr", "B_FieldPack_khk", "B_FieldPack_oli"]] call BWI_fnc_AddRandomBackpack;
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mat";
	case "ft_amat": {
		[_unit, ["B_Kitbag_cbr", "B_Kitbag_rgr"]] call BWI_fnc_AddRandomBackpack;
	};

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg";
	case "ft_aaag": {
		[_unit, ["B_CarryAll_cbr", "B_CarryAll_khk", "B_CarryAll_oli"]] call BWI_fnc_AddRandomBackpack;
	};

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_htg": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_DShkM_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_DShkM_TripodLow_Bag"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"