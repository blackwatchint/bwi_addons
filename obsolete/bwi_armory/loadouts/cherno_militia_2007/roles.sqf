// Parameters passed.
params["_unit", "_faction", "_role"];
private["_unit", "_faction", "_role", "_canSelectRole", "_canSelectSide"];

// Define available roles.
switch ( _role ) do {
	default {
		_canSelectRole = true;
	};

	case "ft_hat";
	case "ft_ahat";
	case "pe_eod";
	case "re_gml";
	case "re_gmg";
	case "re_gma";
	case "re_fac";
	case "re_jtac";
	case "af_fwp";
	case "ar_rwp": {
		_canSelectRole = false;
	};
};


// Define available sides.
switch ( side _unit ) do {
	default {
		_canSelectSide = false;
	};

	case resistance: {
		_canSelectSide = true;
	};
};

// Return var.
[_canSelectRole, _canSelectSide];