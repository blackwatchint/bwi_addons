// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2018;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_FROG01_wd"; };

	case "af_fwp";
	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsusf_lwh_helmet_marpatwd_ess"; };

	case "re_sni";
	case "re_spo": { _unit addHeadgear "rhs_Booniehat_marpatwd"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "rhsusf_cvc_green_helmet"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "rhs_8point_marpatwd"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhsusf_spc_rifleman"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs": { _unit addVest "rhsusf_spc_squadleader"; };

	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": { _unit addVest "rhsusf_spc_teamleader"; };	

	case "ft_lmg";
	case "ft_mmg";
	case "ft_ammg": { _unit addVest "rhsusf_spc_mg"; };

	case "re_sni": { _unit addVest "rhsusf_spc_marksman"; };

	case "pm_cpm": { _unit addVest "rhsusf_spc_corpsman"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addVest "rhsusf_spc_crewman"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "rhsusf_assault_eagleaiii_coy"; };

	case "pl_ptl";
	case "pl_pts";
	case "re_fac": { _unit addBackpack "B_Kitbag_rgr"; };

	case "pe_dem";
	case "ft_ammg";
	case "ft_mat";
	case "ft_amat";
	case "ft_hat";
	case "ft_ahat";
	case "ft_aaag": { _unit addBackpack "B_Carryall_khk"; };

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_htg": { _unit addBackpack "rhs_Tow_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "rhs_TOW_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_oli"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;


// Weapons script to run.
"weapons"