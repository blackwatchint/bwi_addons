// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_G36A1";
		_unit addPrimaryWeaponItem "HLC_Optic_G36Export15x2d";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 6] call BWI_fnc_AddToVest;

		if ( _role in ["ft_amat"] ) then {
			[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToVest;
		} else {
			[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToBackpack;
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "hlc_rifle_G36A1AG36";

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "HLC_Optic_G36Export15x2d";
		} else {
			_unit addPrimaryWeaponItem "HLC_Optic_G36Export35x2d";
		};		

		[_unit, "hlc_30rnd_556x45_EPR_G36", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_MG36";
		_unit addPrimaryWeaponItem "HLC_Optic_G36Dualoptic15x2d";

		[_unit, "hlc_100rnd_556x45_EPR_G36", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100rnd_556x45_M_G36", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_MG3KWS_b";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";

		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_psg1A1";
		_unit addPrimaryWeaponItem "hlc_optic_accupoint_g3";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_T_G3", 2] call BWI_fnc_AddToVest;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_G36A1";
		_unit addPrimaryWeaponItem "HLC_Optic_G36Export15x2d";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 1] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a4";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_Siglite";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_100rnd_556x45_EPR_G36", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100rnd_556x45_M_G36", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.