Following is a list of valid equipment type values used by the standard gear functions.

**Equipment Types:**
```RI``` Regular Infantry
```SF``` Special Forces 
```SC``` Scuba
```PM``` PMC/Paramilitary 
```IN``` Insurgents
```PO``` Police
```CV``` Civilian


Following is a list of all valid role ID values used for determining the correct loadout.

**Role IDs:**
- ```pl_ptl```  Platoon Leader
- ```pl_pts```  Platoon Sergeant
- ```pm_cpm```  Corpsman
- ```pe_dem```  Demolitions Engineer
- ```pe_eod```  Explosive Ordnance Disposal
- ```pe_rep```  Vehicle Engineer
- ```sq_sql```  Squad Leader
- ```sq_sqs```  Assistant Squad Leader
- ```ft_ftl```  Fireteam Leader
- ```ft_gre```  Grenadier
- ```ft_lmg```  Automatic Rifleman
- ```ft_lat```  Anti-Tank Rifleman
- ```ft_mmg```  MMG Gunner
- ```ft_ammg``` MMG Assistant
- ```ft_mat```  MAT Gunner
- ```ft_amat``` MAT Assistant
- ```ft_hat```  HAT Gunner
- ```ft_ahat``` HAT Assistant
- ```ft_aag```  Anti-Air Gunner
- ```ft_aaag``` Anti-Air Assistant
- ```re_mtl```  Mortar Team Leader
- ```re_mtg```  Mortar Team Gunner
- ```re_mta```  Mortar Team Assistant
- ```re_gml```  GMG Team Leader
- ```re_gmg```  GMG Team Gunner
- ```re_gma```  GMG Team Assistant
- ```re_hml```  HMG Team Leader
- ```re_hmg```  HMG Team Gunner
- ```re_hma```  HMG Team Assistant
- ```re_htl```  HAT Team Leader
- ```re_htg```  HAT Team Gunner
- ```re_hta```  HAT Team Assistant
- ```re_sni```  Sniper
- ```re_spo```  Spotter
- ```re_fac```  Forward Air Controller
- ```re_jtac``` Joint Terminal Attack Controller
- ```re_int```  Interpreter
- ```af_fwp```  Fixed-Wing Pilot
- ```ar_rwp```  Rotary-Wing Plot
- ```ac_cmd```  Armor Commander
- ```ac_gun```  Armor Gunner
- ```ac_drv```  Armor Driver


The following code snippets can be run to reset the seed for testing purposes.

**Reset the seed once:**
```bwi_armory_RandomSeed = nil;```

**Reset the seed continually:**
```[{bwi_armory_RandomSeed = nil;}, 0.5] call CBA_fnc_addPerFrameHandler;```