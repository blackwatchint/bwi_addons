// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 1993;
_equipment = "SF";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWI_Uniform_USA_DCU"; };

	case "af_fwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};

	case "ar_rwp": { _unit forceAddUniform "BWI_Uniform_USA_DCU";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsusf_protech_helmet_ess"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "H_Beret_02"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "tacs_Vest_PlateCarrierFull_Black"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_blk"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "re_fac": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg";
	case "ft_mat";
	case "ft_amat": { _unit addBackpack "tacs_Backpack_Carryall_DarkBlack"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_blk"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"