// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_Colt727";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";

		[_unit, "hlc_30rnd_556x45_EPR", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_MDim", 2] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "hlc_rifle_Colt727_GL";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";

		[_unit, "hlc_30rnd_556x45_EPR", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_MDim", 2] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249";

		[_unit, "rhs_200rnd_556x45_T_SAW", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_m60";

		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_M21";
		_unit addPrimaryWeaponItem "hlc_optic_artel_m14";

		[_unit, "hlc_20Rnd_762x51_B_M14", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_M14", 2] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a2";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_m9";

		if ( _role in ["pl_pts"] ) then {
			[_unit, "rhsusf_mag_15Rnd_9x19_JHP", 3] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "rhsusf_mag_15Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
		};
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_smaw_green";
		_unit addSecondaryWeaponItem "rhs_weap_optic_smaw";

		[_unit, "rhs_mag_smaw_SR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_smaw_HEAA", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_gre";
	case "ft_lat": { // Bonus SF ammo.
		[_unit, "hlc_30rnd_556x45_EPR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_M", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_MDim", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_smaw_SR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_smaw_HEAA", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.