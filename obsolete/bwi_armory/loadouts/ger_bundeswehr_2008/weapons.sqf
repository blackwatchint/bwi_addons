// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_G36E1";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";
		_unit addPrimaryWeaponItem "HLC_Optic_G36Dualoptic15x2d";

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_MDIM_G36", 2] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "hlc_rifle_G36E1AG36";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";
		_unit addPrimaryWeaponItem "HLC_Optic_G36dualoptic35x2d";	

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToVest;

		if ( _role in ["pl_ptl", "sq_sql", "ft_ftl", "ft_gre"] ) then {
			[_unit, "hlc_30rnd_556x45_MDIM_G36", 3] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "hlc_30rnd_556x45_MDIM_G36", 3] call BWI_fnc_AddToVest;
		};

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_MG36";
		_unit addPrimaryWeaponItem "HLC_Optic_G36Dualoptic15x2d";

		[_unit, "hlc_100rnd_556x45_EPR_G36", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100rnd_556x45_EPR_G36", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100rnd_556x45_M_G36", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_MG3KWS_b";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";

		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "BWA3_G82";
		_unit addPrimaryWeaponItem "BWA3_optic_24x72";

		[_unit, "BWA3_10Rnd_127x99_G82", 4] call BWI_fnc_AddToVest;
		[_unit, "BWA3_10Rnd_127x99_G82_Tracer", 3] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_10Rnd_127x99_G82_AP", 2] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_10Rnd_127x99_G82_AP_Tracer", 1] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_G36E1";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";
		_unit addPrimaryWeaponItem "HLC_Optic_G36Dualoptic15x2d";

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToVest;

		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_MDIM_G36", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "hlc_rifle_G36C";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_MDIM_G36", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a3";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "BWA3_P8";

		[_unit, "BWA3_15Rnd_9x19_P8", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "BWA3_RGW90";

		[_unit, "BWA3_RGW90_HH", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_mat": {
		_unit addWeapon "BWA3_Pzf3";

		[_unit, "BWA3_Pzf3_IT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_hat": {
		_unit addWeapon "launch_I_Titan_short_F";

		[_unit, "Titan_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "BWA3_Fliegerfaust";

		[_unit, "BWA3_Fliegerfaust_Mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_100rnd_556x45_EPR_G36", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100rnd_556x45_M_G36", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "BWA3_Pzf3_IT", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_ahat": {
		[_unit, "Titan_AT", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "BWA3_Fliegerfaust_Mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.