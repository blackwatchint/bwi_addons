// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_G36C";
		_unit addPrimaryWeaponItem "optic_ACO_grn_smg";
		_unit addPrimaryWeaponItem "acc_flashlight";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_SPR_G36", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_SOST_G36", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_G36KV";
		_unit addPrimaryWeaponItem "optic_ACO_grn_smg";
		_unit addPrimaryWeaponItem "acc_flashlight";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_SPR_G36", 6] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_SOST_G36", 6] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		_unit addWeapon "rhs_weap_M590_5RD";

		[_unit, "rhsusf_5Rnd_00Buck", 16] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 12] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_HE", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_psg1A1";
		_unit addPrimaryWeaponItem "hlc_optic_accupoint_g3";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";

		[_unit, "hlc_20rnd_762x51_b_G3", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_b_G3", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_T_G3", 2] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5k_PDW";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhsusf_weap_glock17g4";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 2] call BWI_fnc_AddToVest;
	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_m4009", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "rhs_mag_m4009", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "rhs_mag_m4009", 15] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": { /* No launcher */ };
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": { /* No assistant */	};
};


// Return nothing.