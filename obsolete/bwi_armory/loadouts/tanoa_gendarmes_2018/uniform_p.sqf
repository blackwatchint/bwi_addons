// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2018;
_equipment = "PO";
_side = resistance;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "U_B_GEN_Soldier_F"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs": { _unit forceAddUniform "U_B_GEN_Commander_F"; };
	

	case "ar_rwp": { _unit forceAddUniform "U_B_GEN_Soldier_F";	};
};


// Helmet.
switch ( _role ) do {
	default { 
		_unit addHeadgear "H_MilCap_gen_F";
	};
	
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs": {
		_unit addHeadgear "H_Beret_gen_F";
	};	

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "H_Beret_gen_F"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "V_TacVest_gen_F"; };

	case "af_rwp": { _unit addVest "V_TacVest_gen_F"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_blk"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "re_fac": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "pe_dem": { _unit addBackpack "tacs_Backpack_Carryall_DarkBlack"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"