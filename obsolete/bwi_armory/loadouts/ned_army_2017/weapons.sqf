// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_m4a1_carryhandle_grip";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";
		_unit addPrimaryWeaponItem "rhsusf_acc_compm4";

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "rhs_weap_m4a1_carryhandle_m203";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "rhsusf_acc_compm4";
		} else {
			_unit addPrimaryWeaponItem "rhsusf_acc_ACOG3";
		};		

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip_S";
		_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";

		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "UK3CB_BAF_L7A2";
		_unit addPrimaryWeaponItem "UK3CB_BAF_SpecterLDS_3D";

		[_unit, "UK3CB_BAF_762_100Rnd", 3] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "UK3CB_BAF_L115A3_BL";
		_unit addPrimaryWeaponItem "RKSL_optic_PMII_525";

		[_unit, "UK3CB_BAF_338_5Rnd", 5] call BWI_fnc_AddToVest;
		[_unit, "UK3CB_BAF_338_5Rnd", 10] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_338_5Rnd_Tracer", 5] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "rhs_weap_m4a1_carryhandle";
		_unit addPrimaryWeaponItem "rhsusf_acc_M952V";
		_unit addPrimaryWeaponItem "rhsusf_acc_compm4";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "rhs_weap_m4a1_carryhandle";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5k_PDW";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_glock17g4";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_hat": {
		_unit addWeapon "launch_I_Titan_short_F";

		[_unit, "Titan_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "UK3CB_BAF_762_100Rnd", 2] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ahat": {
		[_unit, "Titan_AT", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.