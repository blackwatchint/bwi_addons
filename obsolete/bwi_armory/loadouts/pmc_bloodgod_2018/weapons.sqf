// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_bcmjack";
		_unit addPrimaryWeaponItem "rhs_acc_perst3";
		_unit addPrimaryWeaponItem "rhsusf_acc_T1_high";

		[_unit, "rhs_acc_2dpZenit_ris", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_30rnd_556x45_EPR", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_TDim", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_M", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_lmg_mk46mod1";
		_unit addPrimaryWeaponItem "rhs_acc_perst3";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN_ard";

		[_unit, "rhs_acc_2dpZenit_ris", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_mk48mod1";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_MDO";

		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_Mdim_M60E4", 1] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_m24sws_blk";
		_unit addPrimaryWeaponItem "rhsusf_acc_M8541";
		_unit addPrimaryWeaponItem "rhsusf_acc_harris_swivel";

		[_unit, "rhsusf_5Rnd_762x51_m118_special_Mag", 10] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_762x51_m993_Mag", 4] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_762x51_m62_Mag", 6] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_bcmjack";

		[_unit, "hlc_30rnd_556x45_EPR", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a4";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "rhsusf_weap_m1911a1";

		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call BWI_fnc_AddToVest;
	};


	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136_hedp";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.