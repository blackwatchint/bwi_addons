// Parameters passed.
params["_unit", "_faction", "_role"];
private["_unit", "_faction", "_role", "_canSelectRole", "_canSelectSide"];

// Define available roles.
switch ( _role ) do {
	default {
		_canSelectRole = true;
	};
};

// Define available sides.
switch ( side _unit ) do {
	default {
		_canSelectSide = false;
	};

	case west: {
		_canSelectSide = true;
	};
};

// Return var.
[_canSelectRole, _canSelectSide];