// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role", "_randomWeapon"];

// Primary weapon.
switch ( _role ) do {
	default {
		_randomWeapon = [_unit, ["rhs_weap_kar98k", "rhs_weap_m38"]] call BWI_fnc_AddRandomWeapon;

		if ( _randomWeapon == "rhs_weap_kar98k" ) then {
			[_unit, "rhsgref_5Rnd_792x57_kar98k", 10] call BWI_fnc_AddToVest;
			[_unit, "rhsgref_5Rnd_792x57_kar98k", 10] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "rhsgref_5Rnd_762x54_m38", 10] call BWI_fnc_AddToVest;
			[_unit, "rhsgref_5Rnd_762x54_m38", 10] call BWI_fnc_AddToBackpack;
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		[_unit, ["hlc_rifle_ak47", "hlc_rifle_akm"]] call BWI_fnc_AddRandomWeapon;

		[_unit, "hlc_30Rnd_762x39_t_ak", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30Rnd_762x39_b_ak", 6] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_rpk";

		[_unit, "hlc_45Rnd_762x39_t_rpk", 6] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_45Rnd_762x39_m_rpk", 10] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_pkm";

		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR", 3] call BWI_fnc_AddToBackpack;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pm_cpm": {
		_unit addWeapon "rhs_weap_makarov_pm";

		[_unit, "rhs_mag_9x18_8_57N181S", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_rpg26";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v2";

		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7V_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_45Rnd_762x39_t_rpk", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_45Rnd_762x39_m_rpk", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhs_100Rnd_762x54mmR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.