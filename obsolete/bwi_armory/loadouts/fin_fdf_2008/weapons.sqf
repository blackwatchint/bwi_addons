// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_RK62";

		[_unit, "hlc_30Rnd_762x39_b_ak", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_762x39_t_ak", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_pkp";
		_unit addPrimaryWeaponItem "rhs_acc_1p63";

		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_svdp_npz";
		_unit addPrimaryWeaponItem "rhsusf_acc_LEUPOLDMK4_2";

		[_unit, "rhs_10Rnd_762x54mmR_7N1", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_10Rnd_762x54mmR_7N1", 8] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_RK62";

		[_unit, "hlc_30Rnd_762x39_b_ak", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_762x39_t_ak", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "rhs_weap_akms";

		[_unit, "rhs_30Rnd_762x39mm", 5] call BWI_fnc_AddToVest;
		[_unit, "rhs_30Rnd_762x39mm_tracer", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "rhs_weap_aks74u_folded";

		[_unit, "rhs_30Rnd_545x39_AK", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "hlc_pistol_P226R";
		_unit addHandgunItem "HLC_optic_stavenhagen";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "rhs_100Rnd_762x54mmR", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.