// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2018;
_equipment = "CV";
_side = civilian;


// Uniform.
[_unit, ["U_C_Man_casual_6_F", "U_C_Man_casual_4_F", "U_C_Man_casual_5_F"]] call BWI_fnc_AddRandomUniform;


// Helmet.
[_unit, ["H_Bandanna_surfer", "H_Bandanna_surfer_blk", "H_Bandanna_surfer_grn"]] call BWI_fnc_AddRandomHeadgear;


// Vest.
_unit addVest "V_BandollierB_blk";


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"