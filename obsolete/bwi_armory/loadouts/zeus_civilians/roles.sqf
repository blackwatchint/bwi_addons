// Parameters passed.
params["_unit", "_faction", "_role"];
private["_unit", "_faction", "_role", "_canSelectRole", "_canSelectSide"];

// Define available roles.
switch ( _role ) do {
	default {
		_canSelectRole = false;
	};

	case "zeus": {
		_canSelectRole = true;
	};	
};


// Define available sides.
switch ( side _unit ) do {
	default {
		_canSelectSide = false;
	};

	case civilian: {
		_canSelectSide = true;
	};
};

// Return var.
[_canSelectRole, _canSelectSide];