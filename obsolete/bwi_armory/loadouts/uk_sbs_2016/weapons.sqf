// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_g3ka4";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "muzzle_snds_B";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG";

		[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToUniform;
		[_unit, "hlc_20rnd_762x51_b_G3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_S_G3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_MDim_G3", 1] call BWI_fnc_AddToBackpack;

		if ( !(_role in ["re_fac"]) ) then {
			[_unit, "hlc_20rnd_762x51_MDim_G3", 1] call BWI_fnc_AddToBackpack;
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "HLC_Rifle_g3ka4_GL";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "muzzle_snds_B";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG";

		[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToUniform;
		[_unit, "hlc_20rnd_762x51_b_G3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_S_G3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_MDim_G3", 2] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip_S_para";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG";
		_unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";

		[_unit, "rhs_200rnd_556x45_T_SAW", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 4] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "UK3CB_BAF_L115A3_BL_Ghillie";
		_unit addPrimaryWeaponItem "UK3CB_BAF_Silencer_L115A3";
		_unit addPrimaryWeaponItem "RKSL_optic_PMII_525";

		[_unit, "UK3CB_BAF_338_5Rnd", 5] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_338_5Rnd", 10] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_338_5Rnd_Tracer", 5] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "UK3CB_BAF_L22A2";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "hlc_muzzle_TiRant9S";
		_unit addHandgunItem "acc_flashlight_pistol";
		_unit addHandgunItem "HLC_optic228_XS";

		[_unit, "hlc_15Rnd_9x19_SD_P226", 3] call BWI_fnc_AddToUniform;
	};

	case "af_fwp": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_XS";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_B_G3", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_M_SAW", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.