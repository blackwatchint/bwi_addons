// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2016;
_equipment = "SC";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "U_B_Wetsuit"; };

	case "af_fwp": { _unit forceAddUniform "UK3CB_BAF_U_HeliPilotCoveralls_RAF"; };

	case "ar_rwp": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_MTP_ShortSleeve";	};

	case "zeus": { _unit forceAddUniform "UK3CB_BAF_U_Smock_MTP"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addGoggles "G_B_Diving"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "UK3CB_BAF_H_PilotHelmetHeli_A"; };

	case "zeus": { _unit addHeadgear "UK3CB_BAF_H_Beret_SBS"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "V_RebreatherB"; };

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };

	case "ar_rwp": { _unit addVest "UK3CB_BAF_V_Pilot_A"; };

	case "zeus": { _unit addVest "UK3CB_BAF_V_Osprey_Rifleman_B"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "pe_dem": { _unit addBackpack "tacs_Backpack_Carryall_DarkBlack"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_blk"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"