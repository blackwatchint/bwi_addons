// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 1991;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWI_Uniform_USA_DBDU"; };

	case "af_fwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};

	case "ar_rwp": { _unit forceAddUniform "BWI_Uniform_USA_DBDU";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "BWI_Helmet_USA_DBDU"; };

	case "re_sni";
	case "re_spo": { _unit addHeadgear "H_Booniehat_tan"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "rhsusf_cvc_green_helmet"; };

	case "af_fwp": { _unit addHeadgear "rhs_zsh7a_alt"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "H_Beret_02"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "BWI_Vest_USA_DBDU"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_cbr"; };

	case "pl_ptl";
	case "pl_pts";
	case "re_fac": { _unit addBackpack "B_Kitbag_cbr"; };

	case "pe_dem";
	case "ft_ammg";
	case "ft_mat";
	case "ft_amat";
	case "ft_hat";
	case "ft_ahat";
	case "ft_aaag": { _unit addBackpack "B_Carryall_cbr"; };

	case "re_mtg": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_htg": { _unit addBackpack "rhs_Tow_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "rhs_TOW_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_cbr"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"