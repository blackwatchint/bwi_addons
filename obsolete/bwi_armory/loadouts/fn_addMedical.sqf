params ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Add medical according to unit class
switch ( _role ) do {
	case "pm_cpm": {
		[_unit, "ACE_fieldDressing",  12] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_packingBandage", 12] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_elasticBandage", 16] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_quikclot", 	  10] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_tourniquet", 	   6] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_morphine", 	   8] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_epinephrine", 	   6] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_adenosine", 	   2] call BWI_fnc_AddToBackpack;

		if ( _equipment in ["SF", "SC"] ) then {
			[_unit, "ACE_plasmaIV", 	2] call BWI_fnc_AddToBackpack;
			[_unit, "ACE_plasmaIV_500", 8] call BWI_fnc_AddToBackpack;

			if ( _equipment == "SC" ) then {
				[_unit, "ACE_plasmaIV", 2] call BWI_fnc_AddToUniform;
			} else {
				[_unit, "ACE_plasmaIV", 2] call BWI_fnc_AddToBackpack; // Additive
			};

			[_unit, "BWI_Support_AidStationItem", 1] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "ACE_plasmaIV_500", 6] call BWI_fnc_AddToBackpack;
		};
		
		[_unit, "ACE_salineIV_500", 2] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_surgicalKit",  1] call BWI_fnc_AddToBackpack;
	};
	
	case "af_fwp";
	case "ar_rwp": {
		[_unit, "ACE_fieldDressing",  3] call BWI_fnc_AddToVest;
		[_unit, "ACE_packingBandage", 3] call BWI_fnc_AddToVest;
		[_unit, "ACE_elasticBandage", 3] call BWI_fnc_AddToVest;
		[_unit, "ACE_quikclot", 	  3] call BWI_fnc_AddToVest;
		[_unit, "ACE_tourniquet", 	  2] call BWI_fnc_AddToVest;
		[_unit, "ACE_morphine", 	  1] call BWI_fnc_AddToVest;
	};

	case "zeus": {
		[_unit, "ACE_fieldDressing",  1] call BWI_fnc_AddToUniform;
		[_unit, "ACE_packingBandage", 1] call BWI_fnc_AddToUniform;
		[_unit, "ACE_elasticBandage", 1] call BWI_fnc_AddToUniform;
		[_unit, "ACE_quikclot", 	  1] call BWI_fnc_AddToUniform;
	};
	
	default {
		if ( _equipment in ["IN", "PO"] ) then {
			[_unit, "ACE_fieldDressing",  3] call BWI_fnc_AddToVest;
			[_unit, "ACE_packingBandage", 3] call BWI_fnc_AddToVest;
			[_unit, "ACE_elasticBandage", 3] call BWI_fnc_AddToVest;
			[_unit, "ACE_quikclot", 	  3] call BWI_fnc_AddToVest;
			[_unit, "ACE_tourniquet", 	  2] call BWI_fnc_AddToVest;
		} else {
			[_unit, "ACE_fieldDressing",  3] call BWI_fnc_AddToUniform;
			[_unit, "ACE_packingBandage", 3] call BWI_fnc_AddToUniform;
			[_unit, "ACE_elasticBandage", 3] call BWI_fnc_AddToUniform;
			[_unit, "ACE_quikclot", 	  3] call BWI_fnc_AddToUniform;
			[_unit, "ACE_tourniquet", 	  2] call BWI_fnc_AddToUniform;
		};

		// Extra in packpack for assistant leader.
		if ( _role == "sq_sqs" ) then {
			[_unit, "ACE_fieldDressing",  3] call BWI_fnc_AddToBackpack;
			[_unit, "ACE_packingBandage", 3] call BWI_fnc_AddToBackpack;
			[_unit, "ACE_elasticBandage", 3] call BWI_fnc_AddToBackpack;
			[_unit, "ACE_quikclot", 	  3] call BWI_fnc_AddToBackpack;
			[_unit, "ACE_tourniquet", 	  2] call BWI_fnc_AddToBackpack;
		};
	};
};