// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_auga2_b";
		_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";

		[_unit, "hlc_30Rnd_556x45_B_AUG", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_556x45_B_AUG", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30Rnd_556x45_T_AUG", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_auga2lsw_b";
		_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";

		[_unit, "hlc_40Rnd_556x45_B_AUG", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_40Rnd_556x45_B_AUG", 10] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_40Rnd_556x45_SOST_AUG", 10] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_MG3KWS_b";

		[_unit, "hlc_100Rnd_762x51_B_MG3", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_B_MG3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_MG3", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_m14dmr";
		_unit addPrimaryWeaponItem "hlc_optic_LRT_m14";

		[_unit, "hlc_20Rnd_762x51_mk316_M14", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_M14", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_auga2_b";

		[_unit, "hlc_30Rnd_556x45_B_AUG", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_556x45_T_AUG", 1] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_rifle_auga2para_b";

		[_unit, "hlc_25Rnd_9x19mm_M882_AUG", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_Siglite";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_40Rnd_556x45_B_AUG", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_40Rnd_556x45_B_AUG", 4] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_40Rnd_556x45_SOST_AUG", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_B_MG3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_MG3", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.