// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_g3a3";

		[_unit, "hlc_20rnd_762x51_b_G3", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_t_G3", 4] call BWI_fnc_AddToBackpack;
	};

	case "pl_pts";
	case "sq_sqs";
	case "ft_gre";
	case "ft_lat": {
		_unit addWeapon "hlc_rifle_FAL5061";

		[_unit, "hlc_20Rnd_762x51_B_fal", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_B_fal", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_fal", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249";

		[_unit, "rhs_200rnd_556x45_T_SAW", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "UK3CB_BAF_L7A2";

		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToVest;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_g3a3";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_t_G3", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a2";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default { /* No pistol */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_maaws";

		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_ammg": {
		[_unit, "UK3CB_BAF_762_100Rnd_T", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEDP", 3] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.