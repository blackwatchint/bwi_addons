// Parameters passed.
params["_unit", "_faction", "_role"];
private["_unit", "_faction", "_role", "_canSelectRole", "_canSelectSide"];

// Define available roles.
switch ( _role ) do {
	default {
		_canSelectRole = true;
	};

	case "ft_mat";
	case "ft_amat";
	case "re_htl";
	case "re_htg";
	case "re_hta": {
		_canSelectRole = false;
	};
};

// Define available sides.
switch ( side _unit ) do {
	default {
		_canSelectSide = false;
	};

	case west: {
		_canSelectSide = true;
	};
};

// Return var.
[_canSelectRole, _canSelectSide];