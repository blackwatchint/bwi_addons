// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "UK3CB_BAF_L85A2";

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "UK3CB_BAF_L85A2_UGL";

		if ( _role != "ft_gre" ) then {
			_unit addPrimaryWeaponItem "UK3CB_BAF_SUSAT_3D";
		};		

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "UK3CB_BAF_L86A2";

		[_unit, "UK3CB_BAF_556_30Rnd", 8] call BWI_fnc_AddToVest;
		[_unit, "UK3CB_BAF_556_30Rnd", 10] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_556_30Rnd_T", 10] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "UK3CB_BAF_L7A2";
		_unit addPrimaryWeaponItem "UK3CB_BAF_SpecterLDS_3D";

		[_unit, "UK3CB_BAF_762_100Rnd", 3] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "UK3CB_BAF_L118A1_Covert";
		_unit addPrimaryWeaponItem "RKSL_optic_PMII_312";

		[_unit, "UK3CB_BAF_762_L42A1_10Rnd", 3] call BWI_fnc_AddToVest;
		[_unit, "UK3CB_BAF_762_L42A1_10Rnd_T", 2] call BWI_fnc_AddToVest;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "UK3CB_BAF_L85A2";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "UK3CB_BAF_L22A2";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "UK3CB_BAF_L22A2";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "UK3CB_BAF_L131A1";

		[_unit, "UK3CB_BAF_9_17Rnd", 3] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "af_fwp": {
		_unit addWeapon "UK3CB_BAF_L131A1";

		[_unit, "UK3CB_BAF_9_17Rnd", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};

	case "ft_hat": {
		_unit addWeapon "rhs_weap_fgm148";

		[_unit, "rhs_fgm148_magazine_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "UK3CB_BAF_556_30Rnd", 8] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_556_30Rnd_T", 6] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "UK3CB_BAF_762_100Rnd", 2] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ahat": {
		[_unit, "rhs_fgm148_magazine_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.