params ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Add specialty gear for specific roles.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		switch ( _side ) do {
			case west: { 	   [_unit, "ACE_SpraypaintBlue", 1] call BWI_fnc_AddToBackpack; };
			case east: { 	   [_unit, "ACE_SpraypaintRed", 1] call BWI_fnc_AddToBackpack; };
			case resistance: { [_unit, "ACE_SpraypaintGreen", 1] call BWI_fnc_AddToBackpack; };
		};

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "DemoCharge_Remote_Mag", 1] call BWI_fnc_AddToBackpack;
		};

		if ( _equipment in ["SF", "SC", "PO"] && _role == "ft_ftl" ) then {
			[_unit, "ACE_wirecutter", 1] call BWI_fnc_AddToBackpack;
		};
	};

	case "pe_dem": {
		if ( _equipment == "IN" && _era >= 1990 ) then {
			if ( _era >= 2000 ) then {
				[_unit, "ACE_Cellphone", 1] call BWI_fnc_AddToBackpack;
			};

			[_unit, "ACE_DeadManSwitch", 2] call BWI_fnc_AddToBackpack;

			[_unit, "DemoCharge_Remote_Mag", 2] call BWI_fnc_AddToBackpack;
			[_unit, "IEDUrbanSmall_Remote_Mag", 2] call BWI_fnc_AddToBackpack;
			[_unit, "IEDLandSmall_Remote_Mag", 2] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "ACE_Clacker", 1] call BWI_fnc_AddToBackpack;

			[_unit, "DemoCharge_Remote_Mag", 6] call BWI_fnc_AddToBackpack;

			if ( _equipment in ["SF", "SC"] ) then {
				if ( _equipment in ["SC"] ) then {
					[_unit, "DemoCharge_Remote_Mag", 2] call BWI_fnc_AddToBackpack; // Additive
				} else {
					[_unit, "SatchelCharge_Remote_Mag", 2] call BWI_fnc_AddToBackpack;
				};
			} else {
				[_unit, "SatchelCharge_Remote_Mag", 1] call BWI_fnc_AddToBackpack;
			};
		};
	};

	case "pe_eod": {
		_unit addWeapon "ACE_VMM3";

		[_unit, "ACE_Clacker", 1] call BWI_fnc_AddToBackpack;
		[_unit, "ACE_DefusalKit", 1] call BWI_fnc_AddToBackpack;
		[_unit, "DemoCharge_Remote_Mag", 4] call BWI_fnc_AddToBackpack;
		
		switch ( _side ) do {
			case west: { 	   [_unit, "ACE_SpraypaintBlue", 1] call BWI_fnc_AddToBackpack; };
			case east: { 	   [_unit, "ACE_SpraypaintRed", 1] call BWI_fnc_AddToBackpack; };
			case resistance: { [_unit, "ACE_SpraypaintGreen", 1] call BWI_fnc_AddToBackpack; };
		};
	};

	case "pe_rep": {
		[_unit, "ToolKit", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		[_unit, "ACE_RangeCard", 1] call BWI_fnc_AddToUniform;
	};

	case "ft_ammg": {
		[_unit, "ACE_RangeCard", 1] call BWI_fnc_AddToUniform;
		[_unit, "ACE_SpareBarrel", 1] call BWI_fnc_AddToBackpack;
	};

	case "re_mtl";
	case "re_mtg";
	case "re_mta": {
		if ( _role == "re_mta" && _era >= 2000 ) then {
			[_unit, "ACE_Kestrel4500", 1] call BWI_fnc_AddToUniform;
		};

		[_unit, "ACE_RangeTable_82mm", 1] call BWI_fnc_AddToUniform;
	};

	case "re_sni";
	case "re_spo": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 2000 ) then {
			[_unit, "ACE_ATragMX", 1] call BWI_fnc_AddToBackpack;
			[_unit, "ACE_Kestrel4500", 1] call BWI_fnc_AddToBackpack;
		};

		[_unit, "ACE_RangeCard", 1] call BWI_fnc_AddToBackpack;
	};

	case "re_fac": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 2000 ) then {
			switch ( side _unit ) do { // Unit side not loadout side.
				case west: { 	   [_unit, "B_UavTerminal", 1] call BWI_fnc_AddToBackpack; };
				case east: { 	   [_unit, "O_UavTerminal", 1] call BWI_fnc_AddToBackpack; };
				case resistance: { [_unit, "I_UavTerminal", 1] call BWI_fnc_AddToBackpack; };
			};
		};
	};
};