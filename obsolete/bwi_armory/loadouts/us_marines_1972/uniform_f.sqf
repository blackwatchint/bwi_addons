// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 1972;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_g3_rgr"; };

	case "af_fwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};

	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhsgref_helmet_M1_bare"; };

	case "re_sni";
	case "re_spo": { _unit addHeadgear "rhs_Booniehat_m81"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "af_fwp": { _unit addHeadgear "rhs_zsh7a_alt"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "rhs_Booniehat_m81"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "V_TacChestrig_grn_F"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "sq_sql";
	case "sq_sqs";
	case "re_hml";
	case "re_htl";
	case "re_gml";
	case "re_mtl";
	case "re_jtac";
	case "re_spo";
	case "re_fac";
	case "ar_rwp";
	case "af_fwp": { _unit addBackpack "B_Kitbag_rgr"; };

	case "pe_dem";
	case "ft_lmg";
	case "ft_lat": { _unit addBackpack "B_Carryall_oli"; };

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_oli"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };


	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"