// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2019;
_equipment = "RI";
_side = east;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWI_Uniform_DPRK_M81"; };

	case "af_fwp";
	case "ar_rwp": { _unit forceAddUniform "rhsgref_uniform_olive";	};
};


// Helmet.
switch ( _role ) do {
	default {
		_unit addHeadgear "BWI_Helmet_DPRK_M81";
		_unit addGoggles "rhs_googles_black";
	};

	case "re_sni";
	case "re_spo": {
		_unit addHeadgear "rhs_Booniehat_m81";
		_unit addGoggles "rhs_googles_black";
	};	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addHeadgear "rhs_tsh4";
		_unit addGoggles "rhs_googles_black";
	};

	case "af_fwp": { _unit addHeadgear "rhs_zsh7a"; };

	case "ar_rwp": { _unit addHeadgear "rhs_zsh7a_mike"; };

	case "zeus": {
		_unit addHeadgear "rds_Woodlander_cap3";
		_unit addGoggles "rhs_googles_black";
	};
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "BWI_Vest_DPRK_M81"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addVest "BWI_Vest_DPRK_M81"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "rhsgref_otv_khaki"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mat";
	case "ft_amat";
	case "re_fac": { _unit addBackpack "B_Kitbag_rgr"; };

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg";
	case "ft_aaag": { _unit addBackpack "B_CarryAll_oli"; };

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_AGS30_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_AGS30_Tripod_Bag"; };
	case "re_htg": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_DShkM_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_DShkM_TripodLow_Bag"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"