// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2006;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_DPMT"; };

	case "pe_dem";
	case "pe_eod";
	case "pe_rep": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_DPMT_ShortSleeve"; };

	case "af_fwp": { _unit forceAddUniform "UK3CB_BAF_U_HeliPilotCoveralls_RAF";	};

	case "ar_rwp": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_DPMT_ShortSleeve";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "UK3CB_BAF_H_Mk6_DPMT_B"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": { _unit addHeadgear "UK3CB_BAF_H_Mk6_DPMT_A"; };	
	
	case "re_sni";
	case "re_spo": { _unit addHeadgear "UK3CB_BAF_H_Mk6_DPMT_B"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "UK3CB_BAF_H_CrewHelmet_DPMT_A"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "UK3CB_BAF_H_PilotHelmetHeli_A"; };

	case "zeus": { _unit addHeadgear "UK3CB_BAF_H_Beret_SR"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_BAF_V_Osprey_DPMT4"; };

	case "pm_cpm": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT2"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT1"; };

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };

	case "ar_rwp": { _unit addVest "UK3CB_BAF_V_Pilot_DPMT"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_Rifleman_A"; };

	case "pl_ptl";
	case "pl_pts";
	case "re_fac": { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_SL_A"; };

	case "pm_cpm";
	case "pe_dem";
	case "pe_eod";
	case "pe_rep";
	case "pm_gre":  { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_Rifleman_B"; };

	case "ft_ammg";
	case "ft_hat";
	case "ft_ahat";
	case "ft_aaag": { _unit addBackpack "UK3CB_BAF_B_Carryall_DPMT"; };

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"