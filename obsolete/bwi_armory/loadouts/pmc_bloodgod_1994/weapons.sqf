// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_g3a3ris";

		[_unit, "hlc_20rnd_762x51_b_G3", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_t_G3", 4] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl", "ft_ammg"] ) then {
			[_unit, "hlc_20rnd_762x51_b_G3", 2] call BWI_fnc_AddToBackpack; // Additive
		} else {
			[_unit, "hlc_20rnd_762x51_b_G3", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "hlc_20rnd_762x51_b_G3", 1] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249";

		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_m60";

		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_M21";
		_unit addPrimaryWeaponItem "hlc_optic_LRT_m14";

		[_unit, "hlc_20Rnd_762x51_B_M14", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_M14", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_g3a3ris";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_t_G3", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a2";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "rhsusf_weap_m1911a1";

		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 1] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.