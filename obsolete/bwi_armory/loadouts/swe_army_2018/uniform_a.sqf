// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2018;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWI_Uniform_SWE_M90AW"; };

	case "af_fwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_GS_GP_BB";	};

	case "ar_rwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_GS_GP_BB";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "BWI_Helmet_SWE_M90AW"; };
	
	case "re_sni";
	case "re_spo": { _unit addHeadgear "H_Booniehat_oli"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "rhsusf_cvc_green_helmet"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "H_Beret_02"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "BWI_Vest_SWE_M90AW"; };


	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "pl_pts";
	case "pm_cpm";
	case "pe_dem";
	case "ft_mat";
	case "re_fac": { _unit addBackpack "B_Kitbag_sgg"; };

	case "pl_ptl";
	case "pe_dem";
	case "pe_eod";
	case "pe_rep";
	case "ft_ammg";
	case "ft_amat": { _unit addBackpack "B_Carryall_oli"; };

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_htg": { _unit addBackpack "rhs_Tow_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "rhs_TOW_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_oli"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"