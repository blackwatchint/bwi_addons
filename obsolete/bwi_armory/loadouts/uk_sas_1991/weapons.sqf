// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_Colt727";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";

		[_unit, "hlc_30rnd_556x45_EPR", 6] call BWI_fnc_AddToVest;

		if ( !(_role in ["pe_dem", "sq_sqs", "re_jtac"]) ) then {
			[_unit, "hlc_30rnd_556x45_SPR", 3] call BWI_fnc_AddToVest; // Additive
		};

		if ( !(_role in ["pe_dem", "ft_amat", "ft_aaag"]) ) then {
			[_unit, "hlc_30rnd_556x45_SPR", 3] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "hlc_rifle_Colt727_GL";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";

		[_unit, "hlc_30rnd_556x45_EPR", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_SPR", 6] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip_S_para";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";

		[_unit, "rhs_200rnd_556x45_T_SAW", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "UK3CB_BAF_L7A2";

		[_unit, "UK3CB_BAF_762_100Rnd", 3] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_psg1A1";
		_unit addPrimaryWeaponItem "HLC_Optic_ZFSG1";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_T_G3", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "UK3CB_BAF_L22A2";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "UK3CB_BAF_L131A1";
		_unit addHandgunItem "muzzle_snds_L";

		if ( _role in ["pl_ptl", "pe_dem"] ) then {
			[_unit, "UK3CB_BAF_9_17Rnd", 3] call BWI_fnc_AddToVest;
		} else {
			[_unit, "UK3CB_BAF_9_17Rnd", 3] call BWI_fnc_AddToBackpack;
		};
	};

	case "af_fwp": {
		_unit addWeapon "UK3CB_BAF_L131A1";

		[_unit, "UK3CB_BAF_9_17Rnd", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "hlc_30rnd_556x45_EPR", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_SPR", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_M_SAW", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "UK3CB_BAF_762_100Rnd", 1] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.