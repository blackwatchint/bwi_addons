// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 1991;
_equipment = "SF";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "UK3CB_BAF_U_Smock_DPMT"; };

	case "re_sni";
	case "re_spo": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_DPMT_Ghillie_RM"; };

	case "af_fwp": { _unit forceAddUniform "UK3CB_BAF_U_HeliPilotCoveralls_RAF"; };

	case "ar_rwp": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_DPMT_ShortSleeve";	};
};


// Helmet.
switch ( _role ) do {
	default {
		_unit addHeadgear "UK3CB_BAF_H_Wool_Hat";
	};
	
	case "re_sni";
	case "re_spo": { /* No helmet */ };	

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "UK3CB_BAF_H_PilotHelmetHeli_A"; };

	case "zeus": { _unit addHeadgear "UK3CB_BAF_H_Beret_SAS"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_BAF_V_Osprey_DPMT3"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT3"; };

	case "pm_cpm": { _unit addVest "UK3CB_BAF_V_Osprey_DPMT2"; };	

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };

	case "ar_rwp": { _unit addVest "UK3CB_BAF_V_Pilot_A"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_Rifleman_A"; };

	case "pl_ptl";
	case "pl_pts";
	case "re_fac": { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_SL_A"; };

	case "pm_cpm";
	case "pe_dem";
	case "ft_ammg";
	case "ft_amat";
	case "ft_aaag": { _unit addBackpack "UK3CB_BAF_B_Bergen_DPMT_Rifleman_B"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"