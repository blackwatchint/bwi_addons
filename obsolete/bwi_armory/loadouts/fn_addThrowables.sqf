params ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Add chemlights for relevant eras and roles.
if ( _role in ["pl_ptl", "pl_pts", "sq_sql", "sq_sqs", "ft_ftl"] && _equipment in ["RI", "SF", "SC", "PM"] ) then {
	if ( _era >= 2000 ) then {
		if ( _equipment in ["SF", "SC"] ) then {
			[_unit, "ACE_Chemlight_IR", 1] call BWI_fnc_AddToUniform;
		} else {
			switch ( _role ) do {
				case "pl_ptl";
				case "pl_pts": { [_unit, "ACE_Chemlight_HiYellow", 1] call BWI_fnc_AddToUniform; };
				case "sq_sql";
				case "sq_sqs": { [_unit, "ACE_Chemlight_HiOrange", 1] call BWI_fnc_AddToUniform; };
				case "ft_ftl": { [_unit, "ACE_Chemlight_HiRed", 1] call BWI_fnc_AddToUniform; };
			};
		};
	};

	if ( _era >= 1990 && _era < 2000 ) then {
		[_unit, "Chemlight_green", 1] call BWI_fnc_AddToUniform;
	};
};

if ( _equipment in ["PO"] ) then {
	[_unit, "Chemlight_green", 2] call BWI_fnc_AddToUniform;
};

if ( _role == "zeus" && _era >= 1990 ) then {
	[_unit, "ACE_Chemlight_HiWhite", 2] call BWI_fnc_AddToVest;
	[_unit, "ACE_Chemlight_HiYellow", 1] call BWI_fnc_AddToVest;
	[_unit, "ACE_Chemlight_HiOrange", 1] call BWI_fnc_AddToVest;
	[_unit, "ACE_Chemlight_HiRed", 1] call BWI_fnc_AddToVest;
};


// Add IR grenades to relevant roles.
if ( _role in ["sq_sql", "sq_sqs", "ft_ftl", "re_jtac"] && _equipment in ["RI", "SF", "SC", "PM"] && _era >= 1990 ) then {
	if ( _equipment == "SC" ) then {
		switch ( side _unit ) do { // Unit side not loadout side.
			case west: { 	   [_unit, "B_IR_Grenade",  1] call BWI_fnc_AddToBackpack; };
			case east: { 	   [_unit, "O_IR_Grenade",  1] call BWI_fnc_AddToBackpack; };
			case resistance: { [_unit, "I_IR_Grenade",  1] call BWI_fnc_AddToBackpack; };
		};
	} else {
		switch ( side _unit ) do { // Unit side not loadout side.
			case west: { 	   [_unit, "B_IR_Grenade",  1] call BWI_fnc_AddToVest; };
			case east: { 	   [_unit, "O_IR_Grenade",  1] call BWI_fnc_AddToVest; };
			case resistance: { [_unit, "I_IR_Grenade",  1] call BWI_fnc_AddToVest; };
		};
	};
};


// Add grenades and smokes according to side and role.
switch ( _side ) do {
	case west: {
		switch ( _role ) do {
			default {
				if ( _equipment == "SC" ) then {
					[_unit, "SmokeShell",  1] call BWI_fnc_AddToBackpack;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToBackpack;
					[_unit, "HandGrenade",  2] call BWI_fnc_AddToBackpack;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
					[_unit, "HandGrenade",  2] call BWI_fnc_AddToVest;
				};

				if ( _role == "sq_sqs" ) then {
					[_unit, "SmokeShellRed",  2] call BWI_fnc_AddToBackpack;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToBackpack;
					[_unit, "SmokeShellBlue",  1] call BWI_fnc_AddToBackpack;
				};

				if ( _equipment in ["SF", "SC", "PO"] && _role in ["sq_sql", "sq_sqs", "ft_ftl", "ft_gre", "ft_lmg", "ft_lat"] ) then {
					[_unit, "ACE_M84",  1] call BWI_fnc_AddToBackpack;
				};
			};

			case "pe_dem";
			case "pe_eod";
			case "pe_rep";
			case "pm_cpm": {
				if ( _equipment == "SC" ) then {
					[_unit, "SmokeShell",  1] call BWI_fnc_AddToBackpack;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToBackpack;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
				};
			};

			case "ft_mmg";
			case "ft_ammg";
			case "ft_mat";
			case "ft_amat";
			case "ft_hat";
			case "ft_ahat";
			case "ft_aag";
			case "ft_aaag";
			case "re_mtg";
			case "re_mta";
			case "re_gmg";
			case "re_gma";
			case "re_hmg";
			case "re_hma";
			case "re_htg";
			case "re_hta";
			case "ac_cmd";
			case "ac_gun";
			case "ac_drv": {
				[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
				[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
			};

			case "ar_rwp": {
				[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
				[_unit, "ACE_HandFlare_Red",  1] call BWI_fnc_AddToVest;
				[_unit, "ACE_HandFlare_Green",  1] call BWI_fnc_AddToVest;
			};

			case "af_fwp": {
				[_unit, "ACE_HandFlare_Red",  1] call BWI_fnc_AddToVest;
				[_unit, "ACE_HandFlare_Green",  1] call BWI_fnc_AddToVest;
			};

			case "zeus": {
				[_unit, "SmokeShellPurple",  1] call BWI_fnc_AddToVest;
				[_unit, "HandGrenade",  2] call BWI_fnc_AddToVest;
			};
		};
	};

	case east: {
		switch ( _role ) do {
			default {
				if ( _equipment == "SC" ) then {
					[_unit, "SmokeShell",  1] call BWI_fnc_AddToUniform;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToUniform;
					[_unit, "rhs_mag_rgd5",  2] call BWI_fnc_AddToUniform;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
					[_unit, "rhs_mag_rgd5",  2] call BWI_fnc_AddToVest;
				};

				if ( _role == "sq_sqs" ) then {
					[_unit, "SmokeShellRed",  2] call BWI_fnc_AddToBackpack;	
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToBackpack;
					[_unit, "SmokeShellBlue",  1] call BWI_fnc_AddToBackpack;
				};

				if ( _equipment in ["SF", "SC", "PO"] && _role in ["sq_sql", "sq_sqs", "ft_ftl", "ft_gre", "ft_lmg", "ft_lat"] ) then {
					[_unit, "ACE_M84",  1] call BWI_fnc_AddToBackpack;
				};
			};

			case "pe_dem";
			case "pe_eod";
			case "pe_rep";
			case "pm_cpm": {
				if ( _equipment == "SC" ) then {
					[_unit, "SmokeShell",  1] call BWI_fnc_AddToBackpack;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToBackpack;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
				};
			};

			case "ft_mmg";
			case "ft_ammg";
			case "ft_mat";
			case "ft_amat";
			case "ft_hat";
			case "ft_ahat";
			case "ft_aag";
			case "ft_aaag";
			case "re_mtg";
			case "re_mta";
			case "re_gmg";
			case "re_gma";
			case "re_hmg";
			case "re_hma";
			case "re_htg";
			case "re_hta";
			case "ac_cmd";
			case "ac_gun";
			case "ac_drv": {
				[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
				[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
			};

			case "ar_rwp": {
				[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
				[_unit, "ACE_HandFlare_Red",  1] call BWI_fnc_AddToVest;
				[_unit, "ACE_HandFlare_Green",  1] call BWI_fnc_AddToVest;
			};

			case "af_fwp": {
				[_unit, "ACE_HandFlare_Red",  1] call BWI_fnc_AddToVest;
				[_unit, "ACE_HandFlare_Green",  1] call BWI_fnc_AddToVest;
			};

			case "zeus": {
				[_unit, "SmokeShellPurple",  1] call BWI_fnc_AddToVest;
				[_unit, "rhs_mag_rgd5",  2] call BWI_fnc_AddToVest;
			};
		};
	};

	case resistance: {
		switch ( _role ) do {
			default {
				if ( _equipment == "IN" ) then {
					[_unit, "rhs_mag_rdg2_white",  2] call BWI_fnc_AddToVest;
					[_unit, "rhs_mag_rgd5",  2] call BWI_fnc_AddToVest;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
					[_unit, "HandGrenade",  2] call BWI_fnc_AddToVest;
				};

				if ( _role == "sq_sqs" ) then {
					if ( _equipment == "IN" ) then {
						[_unit, "rhs_mag_rdg2_white",  2] call BWI_fnc_AddToBackpack;
						[_unit, "rhs_mag_rdg2_black",  2] call BWI_fnc_AddToBackpack;
					} else {
						[_unit, "SmokeShellRed",  2] call BWI_fnc_AddToBackpack;
						[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToBackpack;
						[_unit, "SmokeShellBlue",  1] call BWI_fnc_AddToBackpack;
					};
				};

				if ( _equipment in ["PO"] && _role in ["sq_sql", "sq_sqs", "ft_ftl", "ft_gre", "ft_lmg", "ft_lat"] ) then {
					[_unit, "ACE_M84",  1] call BWI_fnc_AddToBackpack;
				};
			};

			case "pe_dem";
			case "pe_eod";
			case "pe_rep";
			case "pm_cpm": {
				if ( _equipment == "IN" ) then {
					[_unit, "rhs_mag_rdg2_white",  2] call BWI_fnc_AddToVest;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
				};
			};

			case "ft_mmg";
			case "ft_ammg";
			case "ft_mat";
			case "ft_amat";
			case "ft_hat";
			case "ft_ahat";
			case "ft_aag";
			case "ft_aaag";
			case "re_mtg";
			case "re_mta";
			case "re_gmg";
			case "re_gma";
			case "re_hmg";
			case "re_hma";
			case "re_htg";
			case "re_hta";
			case "ac_cmd";
			case "ac_gun";
			case "ac_drv": {
				if ( _equipment == "IN" ) then {
					[_unit, "rhs_mag_rdg2_white",  2] call BWI_fnc_AddToVest;
				} else {
					[_unit, "SmokeShell",  2] call BWI_fnc_AddToVest;
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
				};
			};

			case "ar_rwp": {
				if ( _equipment != "IN" ) then {
					[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
					[_unit, "ACE_HandFlare_Red",  1] call BWI_fnc_AddToVest;
					[_unit, "ACE_HandFlare_Green",  1] call BWI_fnc_AddToVest;
				};
			};

			case "af_fwp": {
				if ( _equipment != "IN" ) then {
					[_unit, "ACE_HandFlare_Red",  1] call BWI_fnc_AddToVest;
					[_unit, "ACE_HandFlare_Green",  1] call BWI_fnc_AddToVest;
				};
			};

			case "zeus": {
				[_unit, "SmokeShellPurple",  1] call BWI_fnc_AddToVest;
				[_unit, "HandGrenade",  2] call BWI_fnc_AddToVest;
			};
		};
	};

	case civilian: {
		[_unit, "SmokeShell",  1] call BWI_fnc_AddToVest;
		[_unit, "SmokeShellGreen",  1] call BWI_fnc_AddToVest;
	};
};