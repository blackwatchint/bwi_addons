// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_m4a1_blockII_d";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_d";

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 6] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_m4a1_blockII_M203_d";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_d";

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;

		if ( _role != "pl_ptl" ) then {
			[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest; // Additive
		};

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip_S_para";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG";
		_unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";

		[_unit, "rhs_200rnd_556x45_T_SAW", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "UK3CB_BAF_L7A2";
		_unit addPrimaryWeaponItem "UK3CB_BAF_SpecterLDS_3D";

		[_unit, "UK3CB_BAF_762_100Rnd", 3] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "UK3CB_BAF_L115A3_DE_Ghillie";
		_unit addPrimaryWeaponItem "UK3CB_BAF_Silencer_L115A3";
		_unit addPrimaryWeaponItem "RKSL_optic_PMII_525_des";

		[_unit, "UK3CB_BAF_338_5Rnd", 5] call BWI_fnc_AddToVest;
		[_unit, "UK3CB_BAF_338_5Rnd", 10] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_338_5Rnd_Tracer", 5] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "UK3CB_BAF_L22A2";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "hlc_muzzle_TiRant9S";
		_unit addHandgunItem "acc_flashlight_pistol";
		_unit addHandgunItem "HLC_optic228_XS";

		if ( _role in ["pl_ptl", "pe_dem"] ) then {
			[_unit, "hlc_15Rnd_9x19_SD_P226", 3] call BWI_fnc_AddToVest;
		} else {
			[_unit, "hlc_15Rnd_9x19_SD_P226", 3] call BWI_fnc_AddToBackpack;
		};
	};

	case "af_fwp": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_XS";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "30Rnd_556x45_Stanag", 3] call BWI_fnc_AddToBackpack;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_M_SAW", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "UK3CB_BAF_762_100Rnd", 2] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.