// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_amt";

		[_unit, "hlc_20Rnd_762x51_b_amt", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_b_amt", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_amt", 5] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_STGW57";
		_unit addPrimaryWeaponItem "hlc_optic_Kern2d";

		[_unit, "hlc_24Rnd_75x55_B_stgw", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_24Rnd_75x55_B_stgw", 8] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_24Rnd_75x55_T_stgw", 8] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_mg42_bakelite";

		[_unit, "hlc_100Rnd_792x57_B_MG42", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_792x57_AP_MG42", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_792x57_B_MG42", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_792x57_T_MG42", 1] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_M21";
		_unit addPrimaryWeaponItem "hlc_optic_LRT_m14";

		[_unit, "hlc_20Rnd_762x51_B_M14", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_M14", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_amt";

		[_unit, "hlc_20Rnd_762x51_b_amt", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_amt", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a2";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "rhsusf_weap_m1911a1";

		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_gre";
	case "ft_lat": {
		[_unit, "hlc_24Rnd_75x55_B_stgw", 4] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_24Rnd_75x55_T_stgw", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_792x57_AP_MG42", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_792x57_B_MG42", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_792x57_T_MG42", 1] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.