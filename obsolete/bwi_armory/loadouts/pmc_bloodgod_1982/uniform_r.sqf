// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 1982;
_equipment = "PM";
_side = resistance;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhssaf_uniform_m93_oakleaf_summer"; };
};


// Helmet.
switch ( _role ) do {
	default {
		_unit addHeadgear "H_Watchcap_khk";
		_unit addGoggles "G_Bandanna_beast";
	};

	case "ar_rwp": {
		_unit addHeadgear "rhsusf_hgu56p_visor_mask_black_skull";
	};

	case "zeus": {
		_unit addHeadgear "H_Beret_Colonel";
		_unit addGoggles "G_Aviator";
	};
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "tacs_Vest_PlateCarrierFull_Green"; };

	case "ar_rwp": { _unit addVest "tacs_Vest_Tactical_DarkBlack"; };
};


// Backpack.
switch ( _role ) do {
	default {
		_unit addBackpack "B_FieldPack_oli";
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mmg";
	case "re_fac": {
		_unit addBackpack "B_Kitbag_rgr";
	};

	case "pe_dem";
	case "ft_ammg": {
		_unit addBackpack "B_Carryall_oli";
	};

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_hmg": { _unit addBackpack "RHS_DShkM_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_DShkM_TripodLow_Bag"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"