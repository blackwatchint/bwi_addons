// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 1972;
_equipment = "IN";
_side = east;


// Uniform.
[_unit, ["tacs_Uniform_Garment_LS_BS_BP_BB", "tacs_Uniform_Garment_RS_BS_BP_BB", "tacs_Uniform_Garment_RS_GS_GP_BB",
		 "tacs_Uniform_Garment_LS_GS_GP_BB", "tacs_Uniform_Garment_LS_BS_GP_BB", "tacs_Uniform_Garment_LS_GS_BP_BB",
		 "tacs_Uniform_Garment_LS_BS_BP_BB", "tacs_Uniform_Garment_RS_BS_BP_BB"]
] call BWI_fnc_AddRandomUniform;


// Helmet.
[_unit, ["H_Booniehat_khk", "H_Booniehat_oli"]] call BWI_fnc_AddRandomHeadgear;


// Vest.
[_unit, ["V_TacChestrig_grn_F", "V_TacChestrig_cbr_F"]] call BWI_fnc_AddRandomVest;


// Backpack.
switch ( _role ) do {
	default {
		[_unit, ["B_FieldPack_cbr", "B_FieldPack_khk", "B_FieldPack_oli"]] call BWI_fnc_AddRandomBackpack;
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "sq_sql";
	case "sq_sqs";
	case "re_hml";
	case "re_htl";
	case "re_gml";
	case "re_mtl";
	case "ft_lmg";
	case "ft_mat";
	case "ft_amat": {
		[_unit, ["B_Kitbag_cbr", "B_Kitbag_rgr"]] call BWI_fnc_AddRandomBackpack;
	};

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg": {
		[_unit, ["B_CarryAll_cbr", "B_CarryAll_khk", "B_CarryAll_oli"]] call BWI_fnc_AddRandomBackpack;
	};

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_htg": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_DShkM_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_DShkM_TripodLow_Bag"; };

	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"