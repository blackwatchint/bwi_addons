params ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Add binocular optics according to role and era.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 1990 ) then {
			if ( _era >= 2000 ) then {
				_unit addWeapon "ACE_Vector";
			} else  {
				_unit addWeapon "ACE_VectorDay";
			};
		} else {
			_unit addWeapon "Binocular";
		};
	};

	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 1990 ) then {
			if ( _era >= 2000 ) then {
				if ( _role in ["sq_sql", "sq_sqs"] && _era >= 2010 ) then {
					_unit addWeapon "ACE_Vector";
				} else {
					_unit addWeapon "ACE_VectorDay";
				};
			} else  {
				_unit addWeapon "ACE_VectorDay";
			};
		} else {
			_unit addWeapon "Binocular";
		};
	};

	case "ft_ammg";
	case "ft_amat";
	case "ft_ahat";
	case "re_mta";
	case "re_gma";
	case "re_hma";
	case "re_hta": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 1990 ) then {
			_unit addWeapon "ACE_VectorDay";
		} else {
			_unit addWeapon "Binocular";
		};
	};

	case "re_spo": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 1990 ) then {
			if ( _era >= 2000 ) then {
				_unit addWeapon "ACE_Vector";
			} else {
				_unit addWeapon "ACE_VectorDay";
			};
		} else {
			if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] ) then {
				_unit addWeapon "Leupold_Mk4";
			} else {
				_unit addWeapon "Binocular";
			};
		};
	};

	case "re_jtac": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 1990 ) then {
			_unit addWeapon "Laserdesignator";
			[_unit, "Laserbatteries", 4] call BWI_fnc_AddToBackpack;
		} else {
			_unit addWeapon "Binocular";
		};
	};

	default {
		_unit addWeapon "Binocular";
	};
};