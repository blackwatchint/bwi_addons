// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_ak74";
		_unit addPrimaryWeaponItem "rhs_acc_2dpZenit";

		[_unit, "rhs_30Rnd_545x39_AK", 3] call BWI_fnc_AddToVest;
		[_unit, "rhs_30Rnd_545x39_AK", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_30Rnd_545x39_AK_Green", 3] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "rhs_weap_ak74_gp25";
		_unit addPrimaryWeaponItem "rhs_acc_2dpZenit";

		[_unit, "rhs_30Rnd_545x39_AK", 3] call BWI_fnc_AddToVest;
		[_unit, "rhs_30Rnd_545x39_AK", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_30Rnd_545x39_AK_Green", 3] call BWI_fnc_AddToBackpack;

		[_unit, "rhs_VG40OP_white", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_GRD40_Red", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_GRD40_White", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_VOG25P", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "rhs_VOG25P", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "rhs_GRD40_White", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "rhs_GRD40_Green", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "rhs_VOG25P", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_rpk74n";

		[_unit, "hlc_45Rnd_545x39_t_rpk", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_45Rnd_545x39_t_rpk", 14] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_pkm";

		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_svdp";
		_unit addPrimaryWeaponItem "rhs_acc_pso1m21";

		[_unit, "rhs_10Rnd_762x54mmR_7N1", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_10Rnd_762x54mmR_7N1", 8] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "rhs_weap_ak74";
		_unit addPrimaryWeaponItem "rhs_acc_2dpZenit";

		[_unit, "rhs_30Rnd_545x39_AK", 4] call BWI_fnc_AddToVest;
		[_unit, "rhs_30Rnd_545X39_AK_Green", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "rhs_weap_aks74u_folded";

		[_unit, "rhs_30Rnd_545x39_AK", 5] call BWI_fnc_AddToVest;
		[_unit, "rhs_30Rnd_545X39_AK_Green", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "rhs_weap_aks74u_folded";

		[_unit, "rhs_30Rnd_545x39_AK", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "rhs_weap_makarov_pm";

		[_unit, "rhs_mag_9x18_8_57N181S", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_rpg26";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v2";

		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7V_mag", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_igla";

		[_unit, "rhs_mag_9k38_rocket", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_45Rnd_545x39_t_rpk", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_45Rnd_545x39_t_rpk", 7] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhs_100Rnd_762x54mmR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_mag_9k38_rocket", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.