// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_m4a1_blockII_bk";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";
		_unit addPrimaryWeaponItem "rhsusf_acc_compm4";

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "rhs_weap_m4a1_blockII_M203_bk";
		_unit addPrimaryWeaponItem "rhsusf_acc_anpeq15side_bk";

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "rhsusf_acc_compm4";
		} else {
			_unit addPrimaryWeaponItem "rhsusf_acc_ACOG";
		};		

		[_unit, "rhsusf_acc_M952V", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m27iar";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";
		_unit addPrimaryWeaponItem "rhsusf_acc_harris_bipod";

		[_unit, "30Rnd_556x45_Stanag", 8] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag", 8] call BWI_fnc_AddToBackpack;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 8] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_m240B_CAP";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_MDO";

		[_unit, "rhsusf_100Rnd_762x51", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 2] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "srifle_EBR_F";
		_unit addPrimaryWeaponItem "optic_DMS";

		[_unit, "20Rnd_762x51_Mag", 5] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "rhs_weap_m4a1_blockII_bk";
		_unit addPrimaryWeaponItem "rhsusf_acc_M952V";
		_unit addPrimaryWeaponItem "rhsusf_acc_compm4";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5k_PDW";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "rhsusf_weap_glock17g4";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_smaw_green";
		_unit addSecondaryWeaponItem "rhs_weap_optic_smaw";

		[_unit, "rhs_mag_smaw_SR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_smaw_HEAA", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "30Rnd_556x45_Stanag", 8] call BWI_fnc_AddToBackpack;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhsusf_100Rnd_762x51", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_smaw_SR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_smaw_HEAA", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.