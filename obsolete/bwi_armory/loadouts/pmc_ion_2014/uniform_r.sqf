// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2014;
_equipment = "PM";
_side = resistance;


// Uniform.
[_unit, ["tacs_Uniform_Combat_LS_CDBS_GP_TB", "tacs_Uniform_Combat_LS_CLRS_TP_BB",
		 "tacs_Uniform_Combat_RS_CLBS_GP_BB", "tacs_Uniform_Combat_LS_CLBS_GP_BB", 
		 "tacs_Uniform_Combat_RS_CDBS_GP_TB", "tacs_Uniform_Combat_RS_CLRS_TP_BB",
		 "tacs_Uniform_Combat_LS_CPS_BP_BB", "tacs_Uniform_Combat_RS_CPS_BP_BB"]
] call BWI_fnc_AddRandomUniform;


// Helmet.
if ( _role == "ar_rwp" ) then {
	[_unit, ["rhsusf_hgu56p_visor_black", "rhsusf_hgu56p_visor_green", "rhsusf_hgu56p_visor", "rhsusf_hgu56p_visor_usa"]] call BWI_fnc_AddRandomHeadgear;
	
} else {
	[_unit, ["H_Cap_blk_ION", "H_Cap_blk", "H_Cap_usblack"]] call BWI_fnc_AddRandomHeadgear;
	_unit addGoggles "rhs_googles_black";
};


// Vest.
_unit addVest "tacs_Vest_PlateCarrier_Black";


// Backpack.
switch ( _role ) do {
	default {
		_unit addBackpack "B_FieldPack_blk";
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mmg";
	case "re_fac": {
		_unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";
	};

	case "pe_dem";
	case "ft_ammg";
	case "ft_amat";
	case "ft_aaag": {
		_unit addBackpack "tacs_Backpack_Carryall_DarkBlack";
	};

	case "re_mtg": { _unit addBackpack "I_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "I_Mortar_01_support_F"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"