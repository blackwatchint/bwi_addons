// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2016;
_equipment = "SF";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "UK3CB_BAF_U_Smock_Arctic"; };

	case "af_fwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_GS_EP_TB";	};

	case "ar_rwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_GS_GP_BB";	};
};


// Helmet.
switch ( _role ) do {
	default { 
		_unit addHeadgear "UK3CB_BAF_H_Mk7_Win_ESS_A";
		_unit addGoggles "UK3CB_BAF_G_Balaclava_Win";
	};

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "H_Beret_02"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_BAF_V_Osprey_Winter"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_oli"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "ft_aag";
	case "re_fac": { _unit addBackpack "B_Kitbag_rgr"; };

	case "pe_dem";
	case "ft_ammg";
	case "ft_amat";
	case "ft_aaag": { _unit addBackpack "B_Carryall_khk"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_oli"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"