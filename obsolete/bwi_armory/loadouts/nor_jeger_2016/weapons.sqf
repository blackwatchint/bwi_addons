// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_g3ka4";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_IR_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";
		_unit addPrimaryWeaponItem "muzzle_snds_B";

		[_unit, "hlc_20rnd_762x51_b_G3", 4] call BWI_fnc_AddToVest;

		if ( _role in ["pm_cpm", "pe_dem", "pe_eod", "ft_amat", "ft_aaag"] ) then {
			[_unit, "hlc_20rnd_762x51_S_G3", 2] call BWI_fnc_AddToVest;
		} else {
			[_unit, "hlc_20rnd_762x51_S_G3", 2] call BWI_fnc_AddToBackpack;	
		};

		if ( !(_role in ["pe_dem"]) ) then {
			[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToBackpack;
			[_unit, "hlc_20rnd_762x51_MDim_G3", 2] call BWI_fnc_AddToBackpack;
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "HLC_Rifle_g3ka4_GL";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_IR_Black";
		_unit addPrimaryWeaponItem "muzzle_snds_B";

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";
		} else {
			_unit addPrimaryWeaponItem "rhsusf_acc_ACOG3";
		};

		[_unit, "hlc_20rnd_762x51_b_G3", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_S_G3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_MDim_G3", 2] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip_S";
		_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";
		_unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";

		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_MG3KWS_b";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";

		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_psg1A1";
		_unit addPrimaryWeaponItem "HLC_Optic_ZFSG1";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_T_G3", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "rhsusf_weap_MP7A2";

		[_unit, "rhsusf_mag_40Rnd_46x30_FMJ", 4] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhsusf_weap_glock17g4";
		_unit addSecondaryWeaponItem "rhsusf_acc_omega9k";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 2] call BWI_fnc_AddToVest;
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 1] call BWI_fnc_AddToVest;
	};

	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_glock17g4";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "hlc_20rnd_762x51_S_G3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_Barrier_MG3", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.