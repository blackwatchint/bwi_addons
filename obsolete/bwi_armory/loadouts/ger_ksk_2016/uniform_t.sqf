// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2016;
_equipment = "SF";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWA3_Uniform_Tropen"; };

	case "re_sni";
	case "re_spo": { _unit forceAddUniform "BWA3_Uniform_Ghillie_Tropen"; };

	case "af_fwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_ES_EP_TB"; };

	case "ar_rwp": { _unit forceAddUniform "BWA3_Uniform_Crew_Tropen";	};
};


// Helmet.
switch ( _role ) do {
	default {
		_unit addHeadgear "BWA3_OpsCore_Tropen_Camera";
		_unit addGoggles "rhsusf_shemagh2_tan";
	};
	
	case "re_sni";
	case "re_spo": { /* No helmet */ };	

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p_visor"; };

	case "zeus": { _unit addHeadgear "BWA3_Beret_HFlieger"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "BWA3_Vest_Rifleman1_Tropen"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl": { _unit addVest "BWA3_Vest_Leader_Tropen"; };	
	
	case "ft_gre": { _unit addVest "BWA3_Vest_Grenadier_Tropen"; };
	
	case "ft_ammg";
	case "ft_lmg";
	case "ft_mmg": { _unit addVest "BWA3_Vest_Autorifleman_Tropen"; };
	
	case "re_sni": { _unit addVest "BWA3_Vest_Marksman_Tropen"; };

	case "pm_cpm": { _unit addVest "BWA3_Vest_Medic_Tropen"; };	

	case "af_fwp": { _unit addVest "V_TacVest_blk"; };

	case "ar_rwp": { _unit addVest "BWA3_Vest_Tropen"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "BWA3_Kitbag_Tropen"; };
	
	case "pm_cpm": { _unit addBackpack "BWA3_Kitbag_Tropen_Medic"; };

	case "pe_dem";
	case "ft_ammg";
	case "pe_dem": { _unit addBackpack "BWA3_Carryall_Tropen"; };

	case "ft_mat";
	case "ft_amat": { _unit addBackpack "BWA3_Kitbag_Tropen"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "BWA3_FieldPack_Tropen"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons_t"