// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_G36KA1KSK";
		_unit addPrimaryWeaponItem "BWA3_acc_VarioRay_irlaser";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";
		_unit addPrimaryWeaponItem "ACE_optic_MRCO_2D";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToVest;
	};
	
	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "HLC_Rifle_G36KSKAG36";
		_unit addPrimaryWeaponItem "BWA3_acc_VarioRay_irlaser";
		_unit addPrimaryWeaponItem "hlc_muzzle_556NATO_KAC";
		_unit addPrimaryWeaponItem "ACE_optic_MRCO_2D";

		[_unit, "hlc_30rnd_556x45_EPR_G36", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 3] call BWI_fnc_AddToVest;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "BWA3_MG4";
		_unit addPrimaryWeaponItem "ACE_optic_MRCO_2D";
		_unit addPrimaryWeaponItem "BWA3_muzzle_snds_G36";

		[_unit, "BWA3_200Rnd_556x45_Tracer", 2] call BWI_fnc_AddToVest;
		[_unit, "BWA3_200Rnd_556x45", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_MG3KWS_b";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";

		[_unit, "hlc_100Rnd_762x51_M_MG3", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_MG3", 2] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_PSG1A1_RIS";
		_unit addPrimaryWeaponItem "RKSL_optic_PMII_312";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_T_G3", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20rnd_762x51_IRDim_G3", 1] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5k_PDW";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhsusf_weap_glock17g4";
		_unit addHandgunItem "rhsusf_acc_omega9k";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 2] call BWI_fnc_AddToVest;
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 1] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre";
	case "ft_lat";
	case "re_spo";
	case "re_fac";
	case "re_jtac";
	case "re_int": {
		_unit addWeapon "rhsusf_weap_glock17g4";
		_unit addHandgunItem "rhsusf_acc_omega9k";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_mag_17Rnd_9x19_FMJ", 1] call BWI_fnc_AddToBackpack;
	};

	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_glock17g4";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "BWA3_RGW90";
		
		[_unit, "BWA3_RGW90_HH", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_mat": {
		_unit addWeapon "BWA3_Pzf3";
		_unit addSecondaryWeaponItem "BWA3_optic_NSA80";

		[_unit, "BWA3_Pzf3_IT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "BWA3_Fliegerfaust";

		[_unit, "BWA3_Fliegerfaust_Mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "hlc_30rnd_556x45_EPR_G36", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30rnd_556x45_Tracers_G36", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "BWA3_200Rnd_556x45", 1] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_200Rnd_556x45_Tracer", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "BWA3_Pzf3_IT", 2] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_Pzf3_IT_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_M_MG3", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_MG3", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "BWA3_Fliegerfaust_Mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.