params ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Define roles that have no backpack.
private _noBackpackRoles = ["re_mtg", "re_mta", "re_gmg", "re_gma", "re_hmg", "re_hma", "re_htg", "re_hta", "ac_cmd", "ac_gun", "ac_drv", "af_fwp", "ar_rwp", "zeus"];


// Add standard gear for all.
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";

[_unit, "ACE_MapTools", 1] call BWI_fnc_AddToUniform;
[_unit, "ACE_EarPlugs", 1] call BWI_fnc_AddToUniform;
[_unit, "ACE_CableTie", 2] call BWI_fnc_AddToUniform;


// Add night vision equipment according to era.
if ( _equipment in ["RI", "SF", "SC", "PM"] ) then {
	private _nvgClass = nil;

	if ( _era >= 2020 ) then {
		_nvgClass = "ACE_NVG_Gen4";
	};

	if ( _era >= 2000 && _era < 2020 ) then {
		if ( _equipment in ["SF", "SC"] ) then {
			_nvgClass = "NVGoggles_OPFOR"; // Gen 3
		} else {
			_nvgClass = "rhsusf_ANPVS_15";
		};
	};

	if ( _era >= 1990 && _era < 2000 ) then {
		_nvgClass = "ACE_NVG_Gen2";
	};

	if ( _era >= 1980 && _era < 1990 && _equipment in ["SF", "SC"] ) then {
		_nvgClass = "rhsusf_ANPVS_14";
	};


	// Add to vest for some roles.
	if ( !isNil "_nvgClass" ) then {
		if ( _role in _noBackpackRoles ) then {
			[_unit, _nvgClass, 1] call BWI_fnc_AddToVest;
		} else {
			[_unit, _nvgClass, 1] call BWI_fnc_AddToBackpack;
		};
	};
};


// Add IR strobes according to type and era.
if ( _equipment in ["RI", "SF", "SC", "PM"] && _era >= 1990 ) then {
	if ( _role in _noBackpackRoles ) then {
		[_unit, "ACE_IR_Strobe_Item", 1] call BWI_fnc_AddToUniform;
	} else {
		[_unit, "ACE_IR_Strobe_Item", 1] call BWI_fnc_AddToBackpack;
	};
};


// Add flashlight equipment according to type.
if ( _equipment in ["RI", "SF", "SC", "PM"] ) then {
	[_unit, "ACE_Flashlight_MX991", 1] call BWI_fnc_AddToUniform;
} else {
	[_unit, "ACE_Flashlight_XL50", 1] call BWI_fnc_AddToUniform;
};


// Add GPS devices according to role and era.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl";
	case "re_fac";
	case "re_jtac";
	case "re_spo";
	case "ar_rwp";
	case "af_fwp";
	case "ac_cmd": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 2000 ) then {
			[_unit, "ACE_microDAGR", 1] call BWI_fnc_AddToUniform;
			_unit linkItem "ItemGPS";
		};

		if ( _era >= 1990 && _era < 2000 ) then {
			if ( _role in ["ac_cmd", "ar_rwp", "af_fwp"] ) then {
				[_unit, "ACE_DAGR", 1] call BWI_fnc_AddToUniform;
			} else {
				[_unit, "ACE_DAGR", 1] call BWI_fnc_AddToBackpack;
			};
		};
	};

	case "pm_cpm";
	case "pe_dem";
	case "pe_eod";
	case "pe_rep": {
		if ( _equipment in ["RI", "SF", "SC", "PM", "PO"] && _era >= 2000 ) then {
			[_unit, "ACE_microDAGR", 1] call BWI_fnc_AddToUniform;
			_unit linkItem "ItemGPS";
		};

		if ( _equipment in ["SF", "SC"] && _era >= 1990 && _era < 2000 ) then {
			[_unit, "ACE_DAGR", 1] call BWI_fnc_AddToBackpack;
		};
	};

	default {
		if ( _equipment in ["RI", "SF", "SC", "PM"] && _era >= 2010 ) then {
			[_unit, "ACE_microDAGR", 1] call BWI_fnc_AddToUniform;
			_unit linkItem "ItemGPS";
		};

		if ( _equipment in ["SF", "SC"] && _era >= 1990 && _era < 2000 ) then {
			if ( _role in _noBackpackRoles ) then {
				[_unit, "ACE_DAGR", 1] call BWI_fnc_AddToVest;
			} else {
				[_unit, "ACE_DAGR", 1] call BWI_fnc_AddToBackpack;
			};
		};
	};
};