// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_ACR_Carb_black";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_T1_high";

		[_unit, "hlc_30rnd_556x45_EPR", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 3] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl": {
		_unit addWeapon "hlc_rifle_ACR_Carb_black";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "optic_ERCO_blk_F";

		[_unit, "hlc_30rnd_556x45_EPR", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 3] call BWI_fnc_AddToBackpack;
	};


	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN";

		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_M60E4";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG2_USMC";

		[_unit, "hlc_100Rnd_762x51_M_M60E4", 4] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 2] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_awmagnum";
		_unit addPrimaryWeaponItem "rhsusf_acc_M8541_low";

		[_unit, "hlc_5rnd_300WM_FMJ_AWM", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_5rnd_300WM_FMJ_AWM", 4] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_5rnd_300WM_T_AWM", 7] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_5rnd_300WM_AP_AWM", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_ACR_Carb_black";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_T1_high";

		[_unit, "hlc_30rnd_556x45_EPR", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "hlc_rifle_ACR_SBR_black";

		[_unit, "hlc_30rnd_556x45_EPR", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30rnd_556x45_M", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "hlc_rifle_ACR_SBR_black";

		[_unit, "hlc_30rnd_556x45_EPR", 4] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_m9";

		[_unit, "rhsusf_mag_15Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_mat": {
		_unit addWeapon "BWA3_Pzf3";

		[_unit, "BWA3_Pzf3_IT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "BWA3_Pzf3_IT", 2] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_Pzf3_IT_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.