// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_mk18_KAC_d";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_d";

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;

		if ( _role in ["sq_sqs", "re_jtac"] ) then {
			[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 2] call BWI_fnc_AddToBackpack; // Additive
		} else {
			[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 2] call BWI_fnc_AddToVest; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_lmg_mk46";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_ELCAN_ard";
		_unit addPrimaryWeaponItem "muzzle_snds_H_MG_blk_F";

		[_unit, "hlc_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_200rnd_556x45_Mdim_SAW", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_mk48mod1";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_MDO";
		_unit addPrimaryWeaponItem "muzzle_snds_H_MG_blk_F";

		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_Mdim_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 1] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_sr25_d";
		_unit addPrimaryWeaponItem "rhsusf_acc_SR25S";
		_unit addPrimaryWeaponItem "rhsusf_acc_harris_bipod";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_M8541";

		[_unit, "rhsusf_20Rnd_762x51_m62_Mag", 3] call BWI_fnc_AddToVest;
		[_unit, "rhsusf_20Rnd_762x51_m993_Mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_20Rnd_762x51_m118_special_Mag", 1] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "rhsusf_weap_MP7A2";

		[_unit, "rhsusf_mag_40Rnd_46x30_FMJ", 4] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_pistol_P229R_357Elite";
		_unit addHandgunItem "hlc_muzzle_Octane45";
		_unit addHandgunItem "acc_flashlight_pistol";
		_unit addHandgunItem "HLC_Optic228_Romeo1_RX";

		[_unit, "hlc_10Rnd_357SIG_JHP_P229", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_10Rnd_357SIG_B_P229", 1] call BWI_fnc_AddToVest;
	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "af_fwp": {
		_unit addWeapon "hlc_pistol_P229R_357Elite";
		_unit addHandgunItem "HLC_optic228_XS";

		[_unit, "hlc_10Rnd_357SIG_JHP_P229", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_hat": {
		_unit addWeapon "rhs_weap_fgm148";

		[_unit, "rhs_fgm148_magazine_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "30Rnd_556x45_Stanag", 3] call BWI_fnc_AddToBackpack;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "hlc_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_200rnd_556x45_Mdim_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_100Rnd_762x51_M_M60E4", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_T_M60E4", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_100Rnd_762x51_Mdim_M60E4", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HE", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ahat": {
		[_unit, "rhs_fgm148_magazine_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.