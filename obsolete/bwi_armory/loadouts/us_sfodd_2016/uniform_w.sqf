// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2016;
_equipment = "SF";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_g3_m81"; };

	case "af_fwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};

	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_cu_ocp";	};
};


// Helmet.
switch ( _role ) do {
	default { 
		_unit addHeadgear "rhsusf_ach_bare_wood_headset_ess";
		_unit addGoggles "G_Bandanna_oli";
	};
	
	case "re_sni";
	case "re_spo": {
		_unit addHeadgear "rhs_Booniehat_m81";
		_unit addGoggles "G_Bandanna_oli";
	};	

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "rhs_Booniehat_m81"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "tacs_Vest_PlateCarrier_Green"; };

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };

	case "af_rwp": { _unit addVest "rhsusf_iotv_ocp_Rifleman"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_khk"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mmg";
	case "re_fac": { _unit addBackpack "B_Kitbag_rgr"; };

	case "pe_dem";
	case "ft_ammg";
	case "ft_amat";
	case "ft_aaag": { _unit addBackpack "B_Carryall_khk"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons_w"