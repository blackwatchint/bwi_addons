// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_smg_mp5sd6";
		_unit addPrimaryWeaponItem "optic_ACO_grn_smg";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_9x19_GD_MP5", 2] call BWI_fnc_AddToVest;

		if ( _role in ["pe_dem"] ) then {
			[_unit, "hlc_30Rnd_9x19_SD_MP5", 2] call BWI_fnc_AddToVest;
		} else {
			[_unit, "hlc_30Rnd_9x19_SD_MP5", 2] call BWI_fnc_AddToBackpack;	
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_M590_5RD";

		[_unit, "rhsusf_5Rnd_00Buck", 16] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 12] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_HE", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_psg1A1";
		_unit addPrimaryWeaponItem "hlc_optic_accupoint_g3";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_T_G3", 2] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "rhsusf_weap_MP7A2";

		[_unit, "rhsusf_mag_40Rnd_46x30_FMJ", 4] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_pistol_P229R_357Elite";
		_unit addHandgunItem "hlc_muzzle_Octane45";
		_unit addHandgunItem "acc_flashlight_pistol";
		_unit addHandgunItem "HLC_Optic228_Romeo1_RX";

		[_unit, "hlc_10Rnd_357SIG_JHP_P229", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_10Rnd_357SIG_B_P229", 1] call BWI_fnc_AddToVest;

		if ( _role in ["sq_sqs"] ) then {
			[_unit, "hlc_10Rnd_357SIG_JHP_P229", 2] call BWI_fnc_AddToBackpack;
			[_unit, "hlc_10Rnd_357SIG_B_P229", 1] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "hlc_10Rnd_357SIG_JHP_P229", 2] call BWI_fnc_AddToVest;
			[_unit, "hlc_10Rnd_357SIG_B_P229", 1] call BWI_fnc_AddToVest;
		};

	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive

			[_unit, "rhs_mag_m4009", 5] call BWI_fnc_AddToBackpack;
			[_unit, "rhs_mag_m576", 5] call BWI_fnc_AddToBackpack;
		};
	};

	case "af_fwp": {
		_unit addWeapon "hlc_pistol_P229R_357Elite";
		_unit addHandgunItem "HLC_optic228_XS";

		[_unit, "hlc_10Rnd_357SIG_JHP_P229", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "hlc_30Rnd_9x19_B_MP5", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30Rnd_9x19_GD_MP5", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhsusf_5Rnd_00Buck", 8] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 6] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.