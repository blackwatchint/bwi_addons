// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2016;
_equipment = "SF";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "tacs_Uniform_Garment_LS_ES_BP_BB"; };

	case "af_fwp": { _unit forceAddUniform "rhs_uniform_g3_rgr";	};

	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_cu_ocp";	};
};


// Helmet.
switch ( _role ) do {
	default { 
		_unit addHeadgear "rhsusf_opscore_bk_pelt";
		_unit addGoggles "G_Bandanna_blk";
	};
	
	case "re_sni";
	case "re_spo": {
		_unit addHeadgear "H_Watchcap_blk";
		_unit addGoggles "G_Bandanna_blk";
	};	

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "H_Beret_02"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "tacs_Vest_PlateCarrier_Black"; };

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };

	case "af_rwp": { _unit addVest "rhsusf_iotv_ocp_Rifleman"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_blk"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "re_fac": { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "pe_dem": { _unit addBackpack "tacs_Backpack_Carryall_DarkBlack"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons_u"