// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_randomUni", "_randomHat", "_randomVest", "_randomBackpack"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2011;
_equipment = "IN";
_side = resistance;


// Uniform.
[_unit, ["rhs_chdkz_uniform_5", "rhs_chdkz_uniform_4", "rhs_chdkz_uniform_3", "rhs_chdkz_uniform_2",
		 "rhs_chdkz_uniform_1", "tacs_Uniform_TShirt_JP_GS_LP_BB", "tacs_Uniform_TShirt_JP_BS_LP_BB",
		 "tacs_Uniform_Polo_TP_TS_GP_BB"]
] call BWI_fnc_AddRandomUniform;


// Helmet.
[_unit, ["rhs_6b28_green", "rhs_6b27m_green", "H_ShemagOpen_khk", "H_Shemag_olive", "H_ShemagOpen_tan"]] call BWI_fnc_AddRandomHeadgear;


// Vest.
[_unit, ["V_TacVest_camo", "V_TacVest_oli", "V_TacVest_khk", "V_I_G_resistanceLeader_F", "rhs_vydra_3m"]] call BWI_fnc_AddRandomVest;


// Backpack.
switch ( _role ) do {
	default {
		[_unit, ["B_FieldPack_cbr", "B_FieldPack_khk", "B_FieldPack_oli"]] call BWI_fnc_AddRandomBackpack;
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mat";
	case "ft_amat": {
		[_unit, ["B_Kitbag_cbr", "B_Kitbag_rgr"]] call BWI_fnc_AddRandomBackpack;
	};

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg";
	case "ft_aaag": {
		[_unit, ["B_CarryAll_cbr", "B_CarryAll_khk", "B_CarryAll_oli"]] call BWI_fnc_AddRandomBackpack;
	};

	case "re_mtg": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "re_htg": { _unit addBackpack "RHS_SPG9_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "RHS_SPG9_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_DShkM_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_DShkM_TripodLow_Bag"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"