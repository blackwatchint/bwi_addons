// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		[_unit, ["rhs_weap_akms", "rhs_weap_akm"]] call BWI_fnc_AddRandomWeapon;

		[_unit, "rhs_30Rnd_762x39mm", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_30Rnd_762x39mm", 7] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		[_unit, ["rhs_weap_akms_gp25", "rhs_weap_akm_gp25"]] call BWI_fnc_AddRandomWeapon;

		[_unit, "rhs_30Rnd_762x39mm", 9] call BWI_fnc_AddToBackpack;

		[_unit, "rhs_VG40OP_white", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_GRD40_White", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_VOG25P", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "rhs_VOG25P", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "rhs_VOG25P", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_rpk";

		[_unit, "hlc_45Rnd_762x39_t_rpk", 6] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_45Rnd_762x39_m_rpk", 10] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_pkm";

		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_svdp_wd";
		_unit addPrimaryWeaponItem "rhs_acc_pso1m2";

		[_unit, "rhs_10Rnd_762x54mmR_7N1", 10] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		[_unit, ["rhs_weap_akms", "rhs_weap_akm"]] call BWI_fnc_AddRandomWeapon;

		[_unit, "rhs_30Rnd_762x39mm", 1] call BWI_fnc_AddToUniform;
		[_unit, "rhs_30Rnd_762x39mm", 2] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		[_unit, ["rhs_weap_akms", "rhs_weap_akm"]] call BWI_fnc_AddRandomWeapon;

		[_unit, "rhs_30Rnd_762x39mm", 3] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pm_cpm": {
		_unit addWeapon "rhs_weap_makarov_pmm";

		[_unit, "rhs_mag_9x18_12_57N181S", 1] call BWI_fnc_AddToUniform;
		[_unit, "rhs_mag_9x18_12_57N181S", 2] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_rpg26";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";

		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_OG7V_mag", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_igla";

		[_unit, "rhs_mag_9k38_rocket", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_45Rnd_762x39_t_rpk", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_45Rnd_762x39_m_rpk", 4] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhs_100Rnd_762x54mmR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_mag_9k38_rocket", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.