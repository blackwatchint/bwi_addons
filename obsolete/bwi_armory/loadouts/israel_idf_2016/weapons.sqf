// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "arifle_TRG21_F";
		_unit addPrimaryWeaponItem "acc_pointer_IR";
		_unit addPrimaryWeaponItem "optic_Aco";

		[_unit, "acc_flashlight", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "arifle_TRG21_GL_F";
		_unit addPrimaryWeaponItem "acc_pointer_IR";

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "optic_Aco";
		} else {
			_unit addPrimaryWeaponItem "optic_MRCO";
		};		

		[_unit, "acc_flashlight", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_556x45_Stanag", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 3] call BWI_fnc_AddToVest;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_m249_pip_S";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG";

		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_200rnd_556x45_M_SAW", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_m240B_CAP";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG_MDO";

		[_unit, "rhsusf_100Rnd_762x51", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 2] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "UK3CB_BAF_L135A1";
		_unit addPrimaryWeaponItem "RKSL_optic_PMII_525";

		[_unit, "UK3CB_BAF_127_10Rnd", 3] call BWI_fnc_AddToVest;
		[_unit, "UK3CB_BAF_127_10Rnd", 4] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_127_10Rnd_AP", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "arifle_TRG21_F";
		_unit addPrimaryWeaponItem "acc_flashlight";
		_unit addPrimaryWeaponItem "optic_Aco";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "arifle_TRG20_F";

		[_unit, "30Rnd_556x45_Stanag", 5] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_556x45_Stanag_Tracer_Red", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "SMG_05_F";

		[_unit, "30Rnd_9x21_Mag_SMG_02", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_glock17g4";

		[_unit, "rhsusf_mag_17Rnd_9x19_JHP", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_hat": {
		_unit addWeapon "launch_I_Titan_short_F";

		[_unit, "Titan_AT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "rhs_200rnd_556x45_T_SAW", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_200rnd_556x45_M_SAW", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhsusf_100Rnd_762x51", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_100Rnd_762x51_m62_tracer", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_ahat": {
		[_unit, "Titan_AT", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.