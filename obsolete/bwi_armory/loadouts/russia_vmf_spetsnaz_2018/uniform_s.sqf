// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2018;
_equipment = "SC";
_side = east;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "U_B_Wetsuit"; };

	case "af_fwp";
	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_df15"; };

	case "zeus": { _unit forceAddUniform "rhs_uniform_emr_patchless"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addGoggles "G_B_Diving"; };

	case "af_fwp": { _unit addHeadgear "rhs_zsh7a"; };

	case "ar_rwp": { _unit addHeadgear "rhs_zsh7a_mike"; };

	case "zeus": { _unit addHeadgear "rhs_beret_milp"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "V_RebreatherB"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_blk"; };

	case "zeus": { _unit addVest "rhs_6b23_digi_6sh92_Vog_Radio_Spetsnaz"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "pe_dem": { _unit addBackpack "tacs_Backpack_Carryall_DarkBlack"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_blk"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons_s"