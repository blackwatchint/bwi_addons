// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_ak105_zenitco01_b33";
		_unit addPrimaryWeaponItem "hlc_muzzle_545SUP_AK";
		_unit addPrimaryWeaponItem "rhs_acc_perst3_2dp_h";
		_unit addPrimaryWeaponItem "rhsusf_acc_ACOG2";

		[_unit, "rhs_30Rnd_545x39_AK", 6] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_30Rnd_545x39_7U1_AK", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_RPK12";
		_unit addPrimaryWeaponItem "hlc_muzzle_545SUP_AK";
		_unit addPrimaryWeaponItem "rhs_acc_rakursPM";
		_unit addPrimaryWeaponItem "rhs_acc_perst1ik_ris";

		[_unit, "rhs_acc_2dpZenit_ris", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_60Rnd_545x39_t_rpk", 10] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_svdp_npz";
		_unit addPrimaryWeaponItem "rhsusf_acc_LEUPOLDMK4";
		_unit addPrimaryWeaponItem "rhs_acc_tgpv2";

		[_unit, "rhs_10Rnd_762x54mmR_7N1", 10] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "rhs_weap_aks74u";

		[_unit, "rhs_30Rnd_545x39_AK", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_pb_6p9";
		_unit addHandgunItem "rhs_acc_6p9_suppressor";

		[_unit, "rhs_mag_9x18_8_57N181S", 2] call BWI_fnc_AddToUniform;
	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "af_fwp": {
		_unit addWeapon "hgun_Rook40_F";
		
		[_unit, "16Rnd_9x21_Mag", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_rpg26";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "rhs_30Rnd_545x39_AK", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_30Rnd_545x39_7U1_AK", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "hlc_60Rnd_545x39_t_rpk", 6] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.