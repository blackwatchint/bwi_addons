// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_auga3_b";
		_unit addPrimaryWeaponItem "acc_flashlight";
		_unit addPrimaryWeaponItem "rhsusf_acc_T1_low";

		[_unit, "acc_pointer_IR", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_30Rnd_556x45_B_AUG", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_556x45_B_AUG", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30Rnd_556x45_T_AUG", 3] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "hlc_rifle_auga3_GL_B";
		_unit addPrimaryWeaponItem "acc_flashlight";

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "rhsusf_acc_T1_low";
		} else {
			_unit addPrimaryWeaponItem "ACE_optic_MRCO_2D";
		};

		[_unit, "acc_pointer_IR", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_30Rnd_556x45_B_AUG", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_556x45_B_AUG", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_30Rnd_556x45_T_AUG", 3] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "BWA3_MG4";
		_unit addPrimaryWeaponItem "BWA3_optic_ZO4x30_Single";

		[_unit, "BWA3_200Rnd_556x45_Tracer", 1] call BWI_fnc_AddToVest;
		[_unit, "BWA3_200Rnd_556x45", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "BWA3_MG5";
		_unit addPrimaryWeaponItem "BWA3_optic_ZO4x30_Single";

		[_unit, "BWA3_120Rnd_762x51", 3] call BWI_fnc_AddToVest;
		[_unit, "BWA3_120Rnd_762x51_Tracer", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "srifle_EBR_F";
		_unit addPrimaryWeaponItem "optic_DMS";

		[_unit, "20Rnd_762x51_Mag", 5] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_auga2_b";

		[_unit, "hlc_30Rnd_556x45_B_AUG", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_556x45_T_AUG", 1] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_rifle_auga2para_b";

		[_unit, "hlc_25Rnd_9x19mm_M882_AUG", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_Siglite";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "BWA3_RGW90";

		[_unit, "BWA3_RGW90_HH", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_mat": {
		_unit addWeapon "BWA3_Pzf3";

		[_unit, "BWA3_Pzf3_IT", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "BWA3_200Rnd_556x45_Tracer", 1] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_200Rnd_556x45", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "BWA3_Pzf3_IT", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "BWA3_120Rnd_762x51", 3] call BWI_fnc_AddToBackpack;
		[_unit, "BWA3_120Rnd_762x51_Tracer", 1] call BWI_fnc_AddToVest;
		[_unit, "BWA3_120Rnd_762x51_Tracer", 1] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.