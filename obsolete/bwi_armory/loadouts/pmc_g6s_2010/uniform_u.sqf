// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2004;
_equipment = "PM";
_side = resistance;


// Uniform.
_unit forceAddUniform "tacs_Uniform_Garment_LS_GS_BP_BB";


// Helmet.
switch ( _role ) do {
	default {
		_unit addHeadgear "rhs_altyn_novisor_ess";
		_unit addGoggles "rhsusf_shemagh2_grn";
	};

	case "ar_rwp": {
		_unit addHeadgear "rhsusf_hgu56p_visor_mask_Empire_black";
	};

	case "zeus": {
		_unit addHeadgear "H_Beret_Colonel";
		_unit addGoggles "G_Aviator";
	};
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "tacs_Vest_PlateCarrier_Black"; };

	case "ar_rwp": { _unit addVest "tacs_Vest_Tactical_DarkBlack"; };
};


// Backpack.
switch ( _role ) do {
	default {
		_unit addBackpack "B_FieldPack_blk";
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mmg";
	case "ft_amat";
	case "re_fac": {
		_unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";
	};

	case "pe_dem";
	case "ft_ammg";
	case "ft_aaag": {
		_unit addBackpack "tacs_Backpack_Carryall_DarkBlack";
	};

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"