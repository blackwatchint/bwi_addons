// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_smg_MP5N";
		_unit addPrimaryWeaponItem "rhs_acc_2dpZenit_ris";
		_unit addPrimaryWeaponItem "rhsusf_acc_T1_low";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_9x19_GD_MP5", 3] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "hlc_smg_9mmar";
		_unit addPrimaryWeaponItem "rhsusf_acc_T1_low";


		[_unit, "hlc_30Rnd_9x19_B_MP5", 6] call BWI_fnc_AddToVest;
		[_unit, "hlc_30Rnd_9x19_GD_MP5", 3] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "rhs_mag_m4009", 1] call BWI_fnc_AddToBackpack;
			[_unit, "rhs_mag_m576", 1] call BWI_fnc_AddToBackpack;
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "rhs_mag_m4009", 2] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "rhs_mag_m576", 2] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_M590_5RD";

		[_unit, "rhsusf_5Rnd_00Buck", 16] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 12] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_HE", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_PSG1A1_RIS";
		_unit addPrimaryWeaponItem "rhs_acc_dh520x56";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";

		[_unit, "hlc_20rnd_762x51_b_G3", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20rnd_762x51_T_G3", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_smg_mp5a4";

		[_unit, "hlc_30Rnd_9x19_B_MP5", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_Siglite";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_M136";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "rhsusf_5Rnd_00Buck", 8] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 6] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.