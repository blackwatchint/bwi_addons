// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "arifle_AK12_F";
		_unit addPrimaryWeaponItem "muzzle_snds_B";
		_unit addPrimaryWeaponItem "optic_Arco_blk_F";
		_unit addPrimaryWeaponItem "acc_pointer_IR";

		[_unit, "acc_flashlight", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_762x39_Mag_F", 6] call BWI_fnc_AddToVest;

		if ( _role == "pe_dem" ) then {
			[_unit, "30Rnd_762x39_Mag_Tracer_Green_F", 2] call BWI_fnc_AddToVest;
			[_unit, "30Rnd_762x39_Mag_Tracer_Green_F", 1] call BWI_fnc_AddToBackpack;
		} else {
			[_unit, "30Rnd_762x39_Mag_Tracer_Green_F", 3] call BWI_fnc_AddToBackpack;
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "arifle_AK12_GL_F";
		_unit addPrimaryWeaponItem "muzzle_snds_B";
		_unit addPrimaryWeaponItem "acc_pointer_IR";
		_unit addPrimaryWeaponItem "optic_Arco_blk_F";

		[_unit, "acc_flashlight", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_762x39_Mag_F", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_762x39_Mag_Tracer_Green_F", 3] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_RPK12";
		_unit addPrimaryWeaponItem "hlc_muzzle_545SUP_AK";
		_unit addPrimaryWeaponItem "rhs_acc_rakursPM";
		_unit addPrimaryWeaponItem "rhs_acc_perst1ik_ris";

		[_unit, "rhs_acc_2dpZenit_ris", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_60Rnd_545x39_t_rpk", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_60Rnd_545x39_t_rpk", 10] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_pkp";
		_unit addPrimaryWeaponItem "rhs_acc_1p63";

		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_svds_npz";
		_unit addPrimaryWeaponItem "rhsusf_acc_LEUPOLDMK4";
		_unit addPrimaryWeaponItem "rhs_acc_tgpv2";

		[_unit, "rhs_10Rnd_762x54mmR_7N1", 2] call BWI_fnc_AddToVest;
		[_unit, "rhs_10Rnd_762x54mmR_7N1", 8] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "rhs_weap_aks74u";

		[_unit, "rhs_30Rnd_545x39_AK", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhs_weap_pb_6p9";
		_unit addHandgunItem "rhs_acc_6p9_suppressor";
		
		[_unit, "rhs_mag_9x18_8_57N181S", 3] call BWI_fnc_AddToVest;
	};

	case "fw_fwp": {
		_unit addWeapon "hgun_Rook40_F";
		
		[_unit, "16Rnd_9x21_Mag", 3] call BWI_fnc_AddToVest;
	};

	case "pe_eod";
	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_rpg26";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";

		[_unit, "rhs_rpg7_OG7V_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VR_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_igla";

		[_unit, "rhs_mag_9k38_rocket", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "30Rnd_762x39_Mag_F", 1] call BWI_fnc_AddToBackpack;
		[_unit, "30Rnd_762x39_Mag_Tracer_Green_F", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "hlc_60Rnd_545x39_t_rpk", 5] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhs_100Rnd_762x54mmR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_rpg7_OG7V_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VR_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_TBG7V_mag", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_mag_9k38_rocket", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.