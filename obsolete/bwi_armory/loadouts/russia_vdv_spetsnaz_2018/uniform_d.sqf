// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2018;
_equipment = "SF";
_side = east;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "rhs_uniform_emr_des_patchless"; };

	case "af_fwp";
	case "ar_rwp": { _unit forceAddUniform "rhs_uniform_df15";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "rhs_altyn_novisor_ess_bala"; };

	case "re_sni";
	case "re_spo": { _unit addHeadgear "rhs_Booniehat_digi"; };	

	case "af_fwp": { _unit addHeadgear "rhs_zsh7a"; };

	case "ar_rwp": { _unit addHeadgear "rhs_zsh7a_mike"; };

	case "zeus": { _unit addHeadgear "rhs_beret_milp"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "rhs_6b23_digi_6sh92_Vog_Radio_Spetsnaz"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_oli"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mat";
	case "ft_amat";
	case "re_fac": { _unit addBackpack "B_Kitbag_rgr"; };

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg";
	case "ft_aaag": { _unit addBackpack "B_CarryAll_oli"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"