// Parameters passed.
params["_unit", "_faction", "_role"];
private["_unit", "_faction", "_role", "_canSelectRole", "_canSelectSide"];

// Define available roles.
switch ( _role ) do {
	default {
		_canSelectRole = true;
	};

	case "ft_mmg";
	case "ft_ammg";
	case "ft_mat";
	case "ft_amat";
	case "ft_aag";
	case "ft_aaag": {
		if ( _faction == "russia_gru_2018\uniform_u" ) then {
			_canSelectRole = false;
		} else {
			_canSelectRole = true;
		};
	};

	case "pe_rep";
	case "ft_hat";
	case "ft_ahat";
	case "re_mtl";
	case "re_mtg";
	case "re_mta";
	case "re_gml";
	case "re_gmg";
	case "re_gma";
	case "re_htl";
	case "re_htg";
	case "re_hta";
	case "re_hml";
	case "re_hmg";
	case "re_hma";
	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_canSelectRole = false;
	};
};

// Define available sides.
switch ( side _unit ) do {
	default {
		_canSelectSide = false;
	};

	case east: {
		_canSelectSide = true;
	};
};

// Return var.
[_canSelectRole, _canSelectSide];