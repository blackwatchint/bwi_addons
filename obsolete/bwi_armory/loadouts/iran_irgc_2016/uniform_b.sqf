// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2016;
_equipment = "RI";
_side = east;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "BWI_Uniform_IRN_BAS"; };

	case "af_fwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_TS_BP_BB";	};

	case "ar_rwp": { _unit forceAddUniform "tacs_Uniform_Garment_LS_TS_TP_TB";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "BWI_Helmet_IRN_BAS"; };

	case "re_sni";
	case "re_spo": { _unit addHeadgear "H_Booniehat_oli"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "rhs_tsh4"; };

	case "af_fwp": { _unit addHeadgear "rhs_zsh7a"; };

	case "ar_rwp": { _unit addHeadgear "rhs_zsh7a_mike"; };

	case "zeus": { _unit addHeadgear "H_Beret_Colonel"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "BWI_Vest_IRN_BAS"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_blk"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "B_FieldPack_cbr"; };

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_lmg";
	case "ft_mat";
	case "ft_amat";
	case "re_fac": { _unit addBackpack "B_Kitbag_cbr"; };

	case "pe_dem";
	case "ft_mmg";
	case "ft_ammg";
	case "ft_aaag": { _unit addBackpack "B_CarryAll_cbr"; };

	case "re_mtg": { _unit addBackpack "O_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "O_Mortar_01_support_F"; };
	case "re_htg": { _unit addBackpack "RHS_Metis_Gun_Bag"; };
	case "re_hta": { _unit addBackpack "RHS_Metis_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_NSV_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_NSV_Tripod_Bag"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"