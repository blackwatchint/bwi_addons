// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "arifle_Katiba_F";
		_unit addPrimaryWeaponItem "acc_pointer_IR";
		_unit addPrimaryWeaponItem "optic_Aco";

		[_unit, "acc_flashlight", 1] call BWI_fnc_AddToBackpack;

		[_unit, "30Rnd_65x39_caseless_green", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_65x39_caseless_green_mag_Tracer", 3] call BWI_fnc_AddToVest;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "arifle_Katiba_GL_F";
		_unit addPrimaryWeaponItem "acc_pointer_IR";

		[_unit, "acc_flashlight", 1] call BWI_fnc_AddToBackpack;

		if ( _role == "ft_gre" ) then {
			_unit addPrimaryWeaponItem "optic_Aco";
		} else {
			_unit addPrimaryWeaponItem "optic_MRCO";
		};		

		[_unit, "30Rnd_65x39_caseless_green", 6] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_65x39_caseless_green_mag_Tracer", 3] call BWI_fnc_AddToBackpack;

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToVest; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_RPK12";
		_unit addPrimaryWeaponItem "rhs_acc_rakursPM";
		_unit addPrimaryWeaponItem "rhs_acc_perst1ik_ris";

		[_unit, "rhs_acc_2dpZenit_ris", 1] call BWI_fnc_AddToBackpack;

		[_unit, "hlc_60Rnd_545x39_t_rpk", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_60Rnd_545x39_t_rpk", 10] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "rhs_weap_pkp";
		_unit addPrimaryWeaponItem "rhs_acc_1p63";

		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToVest;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR", 3] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "srifle_LRR_F";
		_unit addPrimaryWeaponItem "optic_LRPS";

		[_unit, "7Rnd_408_Mag", 4] call BWI_fnc_AddToVest;
		[_unit, "7Rnd_408_Mag", 10] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "arifle_Katiba_F";
		_unit addPrimaryWeaponItem "acc_flashlight";
		_unit addPrimaryWeaponItem "optic_Aco";

		[_unit, "30Rnd_65x39_caseless_green", 4] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_65x39_caseless_green_mag_Tracer", 1] call BWI_fnc_AddToVest;
	};

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": {
		_unit addWeapon "arifle_Katiba_F";

		[_unit, "30Rnd_65x39_caseless_green", 4] call BWI_fnc_AddToVest;
		[_unit, "30Rnd_65x39_caseless_green_mag_Tracer", 1] call BWI_fnc_AddToVest;
	};	

	case "ar_rwp": {
		_unit addWeapon "arifle_Katiba_C_F";

		[_unit, "30Rnd_65x39_caseless_green", 5] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni";
	case "af_fwp": {
		_unit addWeapon "hgun_Rook40_F";

		[_unit, "16Rnd_9x21_Mag", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_rpg26";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_rpg7";
		_unit addSecondaryWeaponItem "rhs_acc_pgo7v3";

		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_OG7V_mag", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_igla";

		[_unit, "rhs_mag_9k38_rocket", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_60Rnd_545x39_t_rpk", 6] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "rhs_100Rnd_762x54mmR", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_100Rnd_762x54mmR_green", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_rpg7_PG7VL_mag", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_rpg7_PG7VL_mag", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_mag_9k38_rocket", 2] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.