// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_L1A1SLR";

		[_unit, "hlc_20Rnd_762x51_B_fal", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_fal", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_fal", 3] call BWI_fnc_AddToBackpack;

		if ( _role != "pe_dem" ) then {
			[_unit, "hlc_20Rnd_762x51_B_fal", 4] call BWI_fnc_AddToBackpack;
		};
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "hlc_rifle_L1A1SLR";
		_unit addPrimaryWeaponItem "hlc_optic_suit";	

		[_unit, "hlc_20Rnd_762x51_B_fal", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_fal", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_B_fal", 4] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_fal", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_gre": {
		_unit addWeapon "hlc_rifle_LAR";

		[_unit, "hlc_50rnd_762x51_M_FAL", 2] call BWI_fnc_AddToVest;
		[_unit, "hlc_50rnd_762x51_M_FAL", 5] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "UK3CB_BAF_L7A2";

		[_unit, "UK3CB_BAF_762_100Rnd", 3] call BWI_fnc_AddToBackpack;
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToVest;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_M14";
		_unit addPrimaryWeaponItem "hlc_optic_artel_m14";

		[_unit, "hlc_20Rnd_762x51_B_M14", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_M14", 2] call BWI_fnc_AddToVest;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_L1A1SLR";

		[_unit, "hlc_20Rnd_762x51_B_fal", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_fal", 2] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_rifle_L1A1SLR";

		[_unit, "hlc_20Rnd_762x51_B_fal", 3] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_fal", 1] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "rhsusf_weap_m1911a1";

		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "af_fwp": {
		_unit addWeapon "rhsusf_weap_m1911a1";

		[_unit, "rhsusf_mag_7x45acp_MHP", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};

	case "ft_mat": {
		_unit addWeapon "rhs_weap_maaws";
		_unit addSecondaryWeaponItem "rhs_optic_maaws";

		[_unit, "rhs_mag_maaws_HEAT", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aag": {
		_unit addWeapon "rhs_weap_fim92";

		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_ftl": {
		[_unit, "UK3CB_BAF_762_100Rnd", 1] call BWI_fnc_AddToBackpack;
	};
	
	case "ft_lat": {
		[_unit, "UK3CB_BAF_762_100Rnd_T", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_amat": {
		[_unit, "rhs_mag_maaws_HEAT", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhs_mag_maaws_HEDP", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_aaag": {
		[_unit, "rhs_fim92_mag", 1] call BWI_fnc_AddToBackpack;
	};
};

// Return nothing.