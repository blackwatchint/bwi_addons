params ["_unit", "_role", "_side", "_equipment", "_era"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Pre-1980 only the PRC77 is used for leadership roles.
if ( _era < 1980 ) then {
	if ( _role in ["pl_ptl", "pl_pts", "sq_sql", "sq_sqs",
				   "re_hml", "re_htl", "re_gml", "re_mtl",
				   "re_spo", "re_jtac", "re_fac",
				   "af_fwp", "ar_rwp"] ) then {
		[_unit, "ACRE_PRC77", 1] call BWI_fnc_AddToBackpack;
	};
// Post-1980 it varies by role.
} else {
	switch ( _role ) do {
		default {
			if ( _era >= 2000 ) then {
				[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
			};
		};

		case "ft_ftl": {
			if ( _era >= 2000 ) then {
				[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
			};

			[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
		};

		case "sq_sql";
		case "sq_sqs": {
			[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;

			if ( _era >= 2000 ) then {
				[_unit, "ACRE_PRC148", 1] call BWI_fnc_AddToUniform;
			} else {
				[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
			};
		};

		case "pl_ptl";
		case "pl_pts": {
			[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
			[_unit, "ACRE_PRC117F", 1] call BWI_fnc_AddToBackpack;
		};

		case "pm_cpm";
		case "pe_dem";
		case "pe_eod";
		case "pe_rep": {
			[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
		};

		case "re_mtl";
		case "re_gml";
		case "re_hml";
		case "re_htl";
		case "re_spo";
		case "re_int";
		case "re_jtac": {
			if ( _era >= 2000 ) then {
				[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
			};

			[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
		};

		case "re_fac": {
			if ( _era >= 2000 ) then {
				[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
			};
			
			[_unit, "ACRE_PRC117F", 1] call BWI_fnc_AddToBackpack;
		};

		case "ac_cmd": {
			[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
			[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
		};	

		case "ac_gun";
		case "ac_drv": {
			[_unit, "ACRE_PRC343", 1] call BWI_fnc_AddToUniform;
		};

		case "af_fwp";
		case "ar_rwp": {
			[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
		};

		case "zeus": {
			[_unit, "ACRE_PRC152", 1] call BWI_fnc_AddToUniform;
		};
	};
};