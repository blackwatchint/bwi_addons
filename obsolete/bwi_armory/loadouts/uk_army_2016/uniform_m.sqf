// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;


// Era and type.
_era = 2016;
_equipment = "RI";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_MTP"; };

	case "pe_dem";
	case "pe_eod";
	case "pe_rep": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_MTP_ShortSleeve"; };

	case "af_fwp": { _unit forceAddUniform "UK3CB_BAF_U_HeliPilotCoveralls_RAF"; };

	case "ar_rwp": { _unit forceAddUniform "UK3CB_BAF_U_CombatUniform_MTP_ShortSleeve";	};
};


// Helmet.
switch ( _role ) do {
	default { _unit addHeadgear "UK3CB_BAF_H_Mk7_Scrim_B"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": { _unit addHeadgear "UK3CB_BAF_H_Mk7_Scrim_ESS_B"; };	
	
	case "re_sni";
	case "re_spo": { _unit addHeadgear "UK3CB_BAF_H_Mk7_Scrim_C"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addHeadgear "UK3CB_BAF_H_CrewHelmet_A"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "UK3CB_BAF_H_PilotHelmetHeli_A"; };

	case "zeus": { _unit addHeadgear "UK3CB_BAF_H_Beret_PR"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "UK3CB_BAF_V_Osprey_MG_B"; };

	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": { _unit addVest "UK3CB_BAF_V_Osprey_SL_D"; };	

	case "pm_cpm": { _unit addVest "UK3CB_BAF_V_Osprey_Medic_C"; };	

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { _unit addVest "UK3CB_BAF_V_Osprey_Belt_A"; };

	case "af_fwp": { _unit addVest "V_TacVest_oli"; };

	case "ar_rwp": { _unit addVest "UK3CB_BAF_V_Pilot_A"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Rifleman_L_A"; };

	case "pl_ptl";
	case "pl_pts": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Radio_L_A"; };

	case "re_fac": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_JTAC_H_A"; };

	case "pm_cpm": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Medic_L_A"; };

	case "pe_dem";
	case "pe_rep": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Engineer_L_A"; };

	case "pe_eod": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Sapper_H_A"; };

	case "ft_lmg";
	case "ft_ammg";
	case "ft_hat";
	case "ft_ahat";
	case "ft_aaag": { _unit addBackpack "UK3CB_BAF_B_Bergen_MTP_Rifleman_L_D"; };

	case "re_mtg": { _unit addBackpack "B_Mortar_01_weapon_F"; };
	case "re_mta": { _unit addBackpack "B_Mortar_01_support_F"; };
	case "re_gmg": { _unit addBackpack "RHS_Mk19_Gun_Bag"; };
	case "re_gma": { _unit addBackpack "RHS_Mk19_Tripod_Bag"; };
	case "re_hmg": { _unit addBackpack "RHS_M2_Gun_Bag"; };
	case "re_hma": { _unit addBackpack "RHS_M2_MiniTripod_Bag"; };

	case "ac_cmd";
	case "ac_gun";
	case "ac_drv": { /* No backpack */ };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"