// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "rhsusf_weap_MP7A2";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_rotex_mp7";
		_unit addPrimaryWeaponItem "rhsusf_acc_eotech_552";

		[_unit, "rhsusf_mag_40Rnd_46x30_FMJ", 5] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_mag_40Rnd_46x30_JHP", 2] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_mag_40Rnd_46x30_AP", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "rhs_weap_M590_5RD";

		[_unit, "rhsusf_5Rnd_00Buck", 16] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 12] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_HE", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "rhs_weap_m14ebrri";
		_unit addPrimaryWeaponItem "rhs_acc_harris_swivel";
		_unit addPrimaryWeaponItem "UK3CB_BAF_LLM_Flashlight_Black";
		_unit addPrimaryWeaponItem "rhsusf_acc_M8541_low";

		[_unit, "rhsusf_20Rnd_762x51_m118_special_Mag", 3] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_20Rnd_762x51_m62_Mag", 2] call BWI_fnc_AddToBackpack;
	};

	case "ar_rwp": {
		_unit addWeapon "rhsusf_weap_MP7A2";

		[_unit, "rhsusf_mag_40Rnd_46x30_FMJ", 4] call BWI_fnc_AddToVest;
	};

	case "af_fwp";
	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "hlc_muzzle_TiRant9S";
		_unit addHandgunItem "acc_flashlight_pistol";
		_unit addHandgunItem "HLC_optic228_HP";

		[_unit, "hlc_15Rnd_9x19_SD_P226", 2] call BWI_fnc_AddToUniform;
		[_unit, "hlc_15Rnd_9x19_JHP_P226", 1] call BWI_fnc_AddToUniform;
	};

	case "sq_sql";
	case "ft_ftl";
	case "ft_gre": {
		_unit addWeapon "rhs_weap_M320";

		[_unit, "UGL_FlareWhite_F", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_SmokeRed_Grenade_shell", 2] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack;
		[_unit, "1Rnd_HE_Grenade_shell", 3] call BWI_fnc_AddToBackpack;

		if ( _role in ["sq_sql", "ft_ftl"] ) then {
			[_unit, "1Rnd_HE_Grenade_shell", 2] call BWI_fnc_AddToBackpack; // Additive
		};

		if ( _role == "ft_gre" ) then {
			[_unit, "1Rnd_Smoke_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_SmokeGreen_Grenade_shell", 1] call BWI_fnc_AddToBackpack; // Additive
			[_unit, "1Rnd_HE_Grenade_shell", 5] call BWI_fnc_AddToBackpack; // Additive
		};
	};

	case "af_fwp": {
		_unit addWeapon "hlc_pistol_P229R_Combat";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};

	case "ar_rwp";
	case "zeus": { /* No secondary */ };
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "sq_sql";
	case "sq_sqs";
	case "ft_ftl";
	case "ft_gre": { // Bonus SF ammo.
		[_unit, "rhsusf_mag_40Rnd_46x30_FMJ", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_mag_40Rnd_46x30_JHP", 1] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_mag_40Rnd_46x30_AP", 1] call BWI_fnc_AddToBackpack;
	};

	case "ft_lat": {
		[_unit, "rhsusf_5Rnd_00Buck", 8] call BWI_fnc_AddToBackpack;
		[_unit, "rhsusf_5Rnd_Slug", 6] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.