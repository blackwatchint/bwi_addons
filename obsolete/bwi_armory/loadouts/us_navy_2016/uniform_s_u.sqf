// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 2016;
_equipment = "SC";
_side = west;


// Uniform.
switch ( _role ) do {
	default { _unit forceAddUniform "U_B_Wetsuit"; };

	case "af_fwp";
	case "ar_rwp";
	case "zeus": { _unit forceAddUniform "rhs_uniform_g3_rgr"; };
};


// Helmet.
switch ( _role ) do {
	default { _unit addGoggles "G_B_Diving"; };

	case "af_fwp": { _unit addHeadgear "RHS_jetpilot_usaf"; };

	case "ar_rwp": { _unit addHeadgear "rhsusf_hgu56p"; };

	case "zeus": { _unit addHeadgear "rhs_Booniehat_m81"; };
};


// Vest.
switch ( _role ) do {
	default { _unit addVest "V_RebreatherB"; };

	case "af_fwp";
	case "ar_rwp": { _unit addVest "V_TacVest_oli"; };

	case "zeus": { _unit addVest "tacs_Vest_PlateCarrier_Green"; };
};


// Backpack.
switch ( _role ) do {
	default { _unit addBackpack "tacs_Backpack_Kitbag_DarkBlack"; };

	case "pe_dem": { _unit addBackpack "tacs_Backpack_Carryall_DarkBlack"; };

	case "re_sni";
	case "re_spo": { _unit addBackpack "B_FieldPack_blk"; };

	case "af_fwp": { _unit addBackpack "B_Parachute"; };

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons_s_u"