// Parameters passed.
params["_unit", "_role"];
private["_unit", "_role"];

// Primary weapon.
switch ( _role ) do {
	default {
		_unit addWeapon "hlc_rifle_L1A1SLR";

		[_unit, "hlc_20Rnd_762x51_B_fal", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_B_fal", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_fal", 3] call BWI_fnc_AddToBackpack;
	};

	case "pl_ptl";
	case "sq_sql";
	case "ft_ftl";
	case "ft_gre";
	case "re_mtl";
	case "re_gml";
	case "re_hml";
	case "re_htl": {
		_unit addWeapon "hlc_rifle_L1A1SLR";
		_unit addPrimaryWeaponItem "hlc_optic_suit";

		[_unit, "hlc_20Rnd_762x51_B_fal", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_B_fal", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_fal", 3] call BWI_fnc_AddToBackpack;
	};

	case "ft_lmg": {
		_unit addWeapon "hlc_rifle_LAR";
		_unit addPrimaryWeaponItem "hlc_optic_suit";

		[_unit, "hlc_20Rnd_762x51_B_fal", 4] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_B_fal", 6] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_fal", 6] call BWI_fnc_AddToBackpack;
	};

	case "ft_mmg": {
		_unit addWeapon "hlc_lmg_MG42";

		[_unit, "hlc_200Rnd_792x57_B_MG42", 1] call BWI_fnc_AddToVest;
		[_unit, "hlc_200Rnd_792x57_B_MG42", 1] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_200Rnd_792x57_AP_MG42", 1] call BWI_fnc_AddToBackpack;
	};

	case "re_sni": {
		_unit addWeapon "hlc_rifle_M21";
		_unit addPrimaryWeaponItem "hlc_optic_LRT_m14";

		[_unit, "hlc_20Rnd_762x51_B_M14", 3] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_M14", 2] call BWI_fnc_AddToBackpack;
	};

	case "re_mtg";
	case "re_mta";
	case "re_gmg";
	case "re_gma";
	case "re_htg";
	case "re_hta";
	case "re_hmg";
	case "re_hma": {
		_unit addWeapon "hlc_rifle_L1A1SLR";

		[_unit, "hlc_20Rnd_762x51_B_fal", 5] call BWI_fnc_AddToVest;
		[_unit, "hlc_20Rnd_762x51_T_fal", 1] call BWI_fnc_AddToVest;
	};

	case "ar_rwp": {
		_unit addWeapon "hlc_rifle_L1A1SLR";

		[_unit, "hlc_20Rnd_762x51_B_fal", 5] call BWI_fnc_AddToVest;
	};

	case "zeus": { /* No primary */ };
};


// Secondary weapon.
switch ( _role ) do {
	case "pl_ptl";
	case "pl_pts";
	case "sq_sql";
	case "ft_ftl";
	case "pm_cpm";
	case "re_sni": {
		_unit addWeapon "hlc_pistol_P229R_Combat";
		_unit addHandgunItem "HLC_optic228_Siglite";

		[_unit, "hlc_15Rnd_9x19_JHP_P226", 3] call BWI_fnc_AddToVest;
	};
};


// Launcher.
switch ( _role ) do {
	case "ft_lat": {
		_unit addWeapon "rhs_weap_m72a7";
	};
};


// Assistant ammo.
switch ( _role ) do {
	case "ft_lat": {
		[_unit, "hlc_20Rnd_762x51_B_fal", 2] call BWI_fnc_AddToBackpack;
		[_unit, "hlc_20Rnd_762x51_T_fal", 2] call BWI_fnc_AddToBackpack;
	};

	case "ft_ammg": {
		[_unit, "hlc_200Rnd_792x57_B_MG42", 1] call BWI_fnc_AddToBackpack;
	};
};


// Return nothing.