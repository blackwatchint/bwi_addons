// Parameters passed.
params ["_unit", "_role"];
private ["_unit", "_role", "_side", "_equipment", "_era"];


// Remove existing items.
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;


// Era and type.
_era = 1984;
_equipment = "PM";
_side = resistance;


// Uniform.
if ( _role == "ar_rwp" ) then {
	_unit forceAddUniform "tacs_Uniform_Garment_LS_GS_GP_BB";
} else {
	[_unit, ["tacs_Uniform_Polo_CP_RS_LP_BB", "tacs_Uniform_Polo_CP_RS_LP_BB", "tacs_Uniform_TShirt_JP_BS_TP_BB",
			 "tacs_Uniform_TShirt_JP_LS_LP_BB", "tacs_Uniform_TShirt_JP_GS_LP_BB", "tacs_Uniform_TShirt_JP_WS_LP_BB"]
	] call BWI_fnc_AddRandomUniform;
};


// Helmet.
if ( _role == "ar_rwp" ) then {
	_unit addHeadgear "rhs_zsh7a_mike_green";
} else {
	[_unit, ["H_Bandanna_gry", "H_Bandanna_blu", "H_Bandanna_khk",
			 "H_Cap_blk", "H_Cap_blu", "H_Cap_red", "H_Cap_oli"]
	] call BWI_fnc_AddRandomHeadgear;
};
_unit addGoggles "G_Aviator";


// Vest.
_unit addVest "tacs_Vest_PlateCarrier_Black";


// Backpack.
switch ( _role ) do {
	default {
		_unit addBackpack "B_FieldPack_blk";
	};

	case "pl_ptl";
	case "pl_pts";
	case "pm_cpm";
	case "ft_mmg";
	case "ft_amat";
	case "re_fac": {
		_unit addBackpack "tacs_Backpack_Kitbag_DarkBlack";
	};

	case "pe_dem";
	case "ft_lmg";
	case "ft_ammg";
	case "ft_aaag": {
		_unit addBackpack "tacs_Backpack_Carryall_DarkBlack";
	};

	case "ar_rwp";
	case "zeus": { /* No backpack */ };
};


// Call standard gear functions.
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddStandardGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRoleGear;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddMedical;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddThrowables;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddBinoculars;
[_unit, _role, _side, _equipment, _era] call BWI_fnc_AddRadio;

// Weapons script to run.
"weapons"