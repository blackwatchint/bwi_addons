class Indep
{
	name="$STR_A3_CfgGroups_Indep0";
	#include "configs/i_nigeria_army/groups.hpp"
	#include "configs/i_kenya_kdf/groups.hpp"
	#include "configs/i_argentina_army/groups.hpp"
	#include "configs/i_argentina_army_falklands/groups.hpp"
};

class East
{
	name="$STR_A3_CfgGroups_East0";
	#include "configs/o_argentina_army/groups.hpp"
	#include "configs/o_argentina_army_falklands/groups.hpp"
};

class West
{
	name="$STR_A3_CfgGroups_West0";
	#include "configs/b_nigeria_army/groups.hpp"
	#include "configs/b_kenya_kdf/groups.hpp"
};