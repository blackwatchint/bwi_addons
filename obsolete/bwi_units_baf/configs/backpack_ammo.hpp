class B_Carryall_khk;
class B_Carryall_khk_f_L7A2: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(UK3CB_BAF_762_100Rnd_T, 4);
	};
};

class B_Carryall_oli;
class B_Carryall_oli_f_L7A2: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(UK3CB_BAF_762_100Rnd_T, 4);
	};
};

class B_Carryall_cbr;
class B_Carryall_cbr_f_L7A2: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(UK3CB_BAF_762_100Rnd_T, 4);
	};
};