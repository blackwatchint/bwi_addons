class BWI_Soldier_ARG_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_ARG_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageGRE_F",
		"Head_Euro",
		"Head_Greek",
		"Head_African",
		"Head_Asian",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhsgref_helmet_pasgt_woodland",
		"rhsgref_TacVest_ERDL",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsgref_helmet_pasgt_woodland",
		"rhsgref_TacVest_ERDL",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_woodland";
};


class BWI_Soldier_ARG_GEN_AT: BWI_Soldier_ARG_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"rhs_weap_M136",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"rhs_weap_M136",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_ARG_GEN_AR: BWI_Soldier_ARG_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_LAR",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_LAR",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_LAR";
};


class BWI_Soldier_ARG_GEN_DMR: BWI_Soldier_ARG_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_m24sws_blk_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m24sws_blk_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsusf_5Rnd_762x51_m62_Mag",
		"rhsusf_5Rnd_762x51_m62_Mag",
		"rhsusf_5Rnd_762x51_m62_Mag",
		"rhsusf_5Rnd_762x51_m993_Mag",
		"rhsusf_5Rnd_762x51_m993_Mag",
		"rhsusf_5Rnd_762x51_m993_Mag",
		"rhsusf_5Rnd_762x51_m118_special_Mag",
		"rhsusf_5Rnd_762x51_m118_special_Mag",
		"rhsusf_5Rnd_762x51_m118_special_Mag",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhsusf_5Rnd_762x51_m62_Mag",
		"rhsusf_5Rnd_762x51_m62_Mag",
		"rhsusf_5Rnd_762x51_m62_Mag",
		"rhsusf_5Rnd_762x51_m993_Mag",
		"rhsusf_5Rnd_762x51_m993_Mag",
		"rhsusf_5Rnd_762x51_m993_Mag",
		"rhsusf_5Rnd_762x51_m118_special_Mag",
		"rhsusf_5Rnd_762x51_m118_special_Mag",
		"rhsusf_5Rnd_762x51_m118_special_Mag",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_ARG_GEN_TL: BWI_Soldier_ARG_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_ARG_GEN_HAT: BWI_Soldier_ARG_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_Titan";
};


class BWI_Soldier_ARG_GEN_MMG: BWI_Soldier_ARG_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_L7A2";
};


class BWI_Soldier_ARG_GEN_CRW: BWI_Soldier_ARG_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"rhsgref_TacVest_ERDL",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"rhsgref_TacVest_ERDL",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};

class BWI_Vehicle_ARG_M113_M240: rhsusf_m113_usarmy_m240 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG";
	crew = "BWI_Soldier_ARG_GEN_CRW";
};

class BWI_Vehicle_ARG_M113: rhsusf_m113_usarmy_unarmed {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG";
	crew = "BWI_Soldier_ARG_GEN_CRW";
};

class BWI_Vehicle_ARG_M113_M2: rhsgref_hidf_m113a3_m2 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG";
	crew = "BWI_Soldier_ARG_GEN_CRW";
};

class BWI_Vehicle_ARG_M1025: rhsusf_m1025_w {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG";
	crew = "BWI_Soldier_ARG_GEN_R";
};

class BWI_Vehicle_ARG_M1025_M2: rhsusf_m1025_w_m2 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG";
	crew = "BWI_Soldier_ARG_GEN_R";
};