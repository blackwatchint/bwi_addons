class BWI_Flag_ARG {
	scope = 1;
	name = "Argentina";
	markerClass = "Flags";
	icon = "\bwi_units_baf\data\markers\mkr_arg.paa";
	texture = "\bwi_units_baf\data\markers\mkr_arg.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};