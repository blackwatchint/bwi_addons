class BWI_Soldier_ARG_O_GEN_R : BWI_Soldier_ARG_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_AT: BWI_Soldier_ARG_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_AR: BWI_Soldier_ARG_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_DMR: BWI_Soldier_ARG_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_TL: BWI_Soldier_ARG_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_HAT: BWI_Soldier_ARG_GEN_HAT {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_MMG: BWI_Soldier_ARG_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};
class BWI_Soldier_ARG_O_GEN_CRW: BWI_Soldier_ARG_GEN_CRW {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
};


class BWI_Vehicle_ARG_O_M113_M240: BWI_Vehicle_ARG_M113_M240 {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
	crew = "BWI_Soldier_ARG_O_GEN_CRW";
};

class BWI_Vehicle_ARG_O_M113: BWI_Vehicle_ARG_M113 {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
	crew = "BWI_Soldier_ARG_O_GEN_CRW";
};

class BWI_Vehicle_ARG_O_M113_M2: BWI_Vehicle_ARG_M113_M2 {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
	crew = "BWI_Soldier_ARG_O_GEN_CRW";
};

class BWI_Vehicle_ARG_O_M1025: BWI_Vehicle_ARG_M1025 {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
	crew = "BWI_Soldier_ARG_O_GEN_R";
};

class BWI_Vehicle_ARG_O_M1025_M2: BWI_Vehicle_ARG_M1025_M2 {
	side = EAST;
	faction = "BWI_FACTION_ARG_O";
	crew = "BWI_Soldier_ARG_O_GEN_R";
};