class BWI_Soldier_ARG82_O_GEN_R : BWI_Soldier_ARG82_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_AT: BWI_Soldier_ARG82_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_AR: BWI_Soldier_ARG82_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_DMR: BWI_Soldier_ARG82_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_TL: BWI_Soldier_ARG82_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_MAT: BWI_Soldier_ARG82_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_MMG: BWI_Soldier_ARG82_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};
class BWI_Soldier_ARG82_O_GEN_CRW: BWI_Soldier_ARG82_GEN_CRW {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
};


class BWI_Vehicle_ARG82_O_BRDM2: BWI_Vehicle_ARG82_BRDM2 {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
	crew = "BWI_Soldier_ARG82_O_GEN_CRW";
};

class BWI_Vehicle_ARG82_O_BRDM2UM: BWI_Vehicle_ARG82_BRDM2UM {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
	crew = "BWI_Soldier_ARG82_O_GEN_CRW";
};

class BWI_Vehicle_ARG82_O_BRDM2_HQ: BWI_Vehicle_ARG82_BRDM2_HQ {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
	crew = "BWI_Soldier_ARG82_O_GEN_CRW";
};

class BWI_Vehicle_ARG82_O_BRDM2_ATGM: BWI_Vehicle_ARG82_BRDM2_ATGM {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
	crew = "BWI_Soldier_ARG82_O_GEN_CRW";
};

class BWI_Vehicle_ARG82_O_UAZ: BWI_Vehicle_ARG82_UAZ {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
	crew = "BWI_Soldier_ARG82_O_GEN_R";
};

class BWI_Vehicle_ARG82_O_UAZ_OPEN: BWI_Vehicle_ARG82_UAZ_OPEN {
	side = EAST;
	faction = "BWI_FACTION_ARG82_O";
	crew = "BWI_Soldier_ARG82_O_GEN_R";
};