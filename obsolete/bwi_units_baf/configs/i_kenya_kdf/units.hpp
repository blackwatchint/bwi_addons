class BWI_Soldier_KDF_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_KDF_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_KDF";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhs_6b7_1m",
		"UK3CB_BAF_V_Osprey_DPMT2",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_6b7_1m",
		"UK3CB_BAF_V_Osprey_DPMT2",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "UK3CB_BAF_U_JumperUniform_DPMT";
};


class BWI_Soldier_KDF_GEN_AT: BWI_Soldier_KDF_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_KDF_GEN_AR: BWI_Soldier_KDF_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_m249";
};


class BWI_Soldier_KDF_GEN_DMR: BWI_Soldier_KDF_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_KDF_GEN_TL: BWI_Soldier_KDF_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_KDF_GEN_MAT: BWI_Soldier_KDF_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_maaws";
};


class BWI_Soldier_KDF_GEN_MMG: BWI_Soldier_KDF_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_L7A2";
};

class BWI_Soldier_KDF_GEN_CRW: BWI_Soldier_KDF_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"UK3CB_BAF_V_Osprey_DPMT2",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"UK3CB_BAF_V_Osprey_DPMT2",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_KDF_T72: rhs_t72ba_tv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_KDF";
	crew = "BWI_Soldier_KDF_GEN_CRW";
};


class BWI_Vehicle_KDF_M1025_W: rhsusf_m1025_w {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_KDF";
	crew = "BWI_Soldier_KDF_GEN_R";
};

class BWI_Vehicle_KDF_M1025_M2_W: rhsusf_m1025_w_m2 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_KDF";
	crew = "BWI_Soldier_KDF_GEN_R";
};