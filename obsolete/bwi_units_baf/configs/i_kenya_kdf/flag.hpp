class BWI_Flag_KDF {
	scope = 1;
	name = "Kenya";
	markerClass = "Flags";
	icon = "\bwi_units_baf\data\markers\mkr_ken.paa";
	texture = "\bwi_units_baf\data\markers\mkr_ken.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};