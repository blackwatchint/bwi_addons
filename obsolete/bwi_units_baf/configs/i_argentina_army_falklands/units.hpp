class BWI_Soldier_ARG82_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_ARG82_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageGRE_F",
		"Head_Euro",
		"Head_Greek",
		"Head_African",
		"Head_Asian",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhsgref_helmet_M1_bare",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsgref_helmet_M1_bare",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "tacs_Uniform_Garment_RS_GS_BP_BB";
};


class BWI_Soldier_ARG82_GEN_AT: BWI_Soldier_ARG82_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_ARG82_GEN_AR: BWI_Soldier_ARG82_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_LAR",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_LAR",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"hlc_50rnd_762x51_M_FAL",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_LAR";
};


class BWI_Soldier_ARG82_GEN_DMR: BWI_Soldier_ARG82_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_M14_w_artel",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_M14_w_artel",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_T_M14",
		"hlc_20Rnd_762x51_T_M14",
		"hlc_20Rnd_762x51_T_M14",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_T_M14",
		"hlc_20Rnd_762x51_T_M14",
		"hlc_20Rnd_762x51_T_M14",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_ARG82_GEN_TL: BWI_Soldier_ARG82_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_ARG82_GEN_MAT: BWI_Soldier_ARG82_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_FAL5000",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_FAL5000",
		"rhs_weap_maaws",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"hlc_20Rnd_762x51_T_fal",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_CarryAll_oli_f_maaws";
};


class BWI_Soldier_ARG82_GEN_MMG: BWI_Soldier_ARG82_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_L7A2";
};


class BWI_Soldier_ARG82_GEN_CRW: BWI_Soldier_ARG82_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_ARG82_BRDM2: rhsgref_BRDM2_vdv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	crew = "BWI_Soldier_ARG82_GEN_CRW";
};

class BWI_Vehicle_ARG82_BRDM2UM: rhsgref_BRDM2UM_vdv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	crew = "BWI_Soldier_ARG82_GEN_CRW";
};

class BWI_Vehicle_ARG82_BRDM2_HQ: rhsgref_BRDM2_HQ_vdv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	crew = "BWI_Soldier_ARG82_GEN_CRW";
};

class BWI_Vehicle_ARG82_BRDM2_ATGM: rhsgref_BRDM2_ATGM_vdv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	crew = "BWI_Soldier_ARG82_GEN_CRW";
};

class BWI_Vehicle_ARG82_UAZ: rhs_uaz_vdv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	crew = "BWI_Soldier_ARG82_GEN_R";
};

class BWI_Vehicle_ARG82_UAZ_OPEN: rhs_uaz_open_vdv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ARG82";
	crew = "BWI_Soldier_ARG82_GEN_R";
};