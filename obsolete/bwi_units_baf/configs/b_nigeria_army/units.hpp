class BWI_Soldier_NIG_B_GEN_R : BWI_Soldier_NIG_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_AT: BWI_Soldier_NIG_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_AR: BWI_Soldier_NIG_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_DMR: BWI_Soldier_NIG_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_TL: BWI_Soldier_NIG_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_MAT: BWI_Soldier_NIG_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_MMG: BWI_Soldier_NIG_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};
class BWI_Soldier_NIG_B_GEN_CRW: BWI_Soldier_NIG_GEN_CRW {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
};


class BWI_Vehicle_NIG_B_T72: BWI_Vehicle_NIG_T72 {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
	crew = "BWI_Soldier_NIG_GEN_CRW";
};

class BWI_Vehicle_NIG_B_BMP1: BWI_Vehicle_NIG_BMP1 {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
	crew = "BWI_Soldier_NIG_B_GEN_CRW";
};

class BWI_Vehicle_NIG_B_BTR70: BWI_Vehicle_NIG_BTR70 {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
	crew = "BWI_Soldier_NIG_B_GEN_CRW";
};

class BWI_Vehicle_NIG_B_RG33_M2: BWI_Vehicle_NIG_RG33_M2 {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
	crew = "BWI_Soldier_NIG_B_GEN_R";
};

class BWI_Vehicle_NIG_B_RG33: BWI_Vehicle_NIG_RG33 {
	side = WEST;
	faction = "BWI_FACTION_NIG_B";
	crew = "BWI_Soldier_NIG_B_GEN_R";
};