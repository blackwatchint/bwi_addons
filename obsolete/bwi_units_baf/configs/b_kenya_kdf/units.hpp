class BWI_Soldier_KDF_B_GEN_R : BWI_Soldier_KDF_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_AT: BWI_Soldier_KDF_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_AR: BWI_Soldier_KDF_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_DMR: BWI_Soldier_KDF_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_TL: BWI_Soldier_KDF_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_MAT: BWI_Soldier_KDF_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_MMG: BWI_Soldier_KDF_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};
class BWI_Soldier_KDF_B_GEN_CRW: BWI_Soldier_KDF_GEN_CRW {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
};


class BWI_Vehicle_KDF_B_T72: BWI_Vehicle_KDF_T72 {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
	crew = "BWI_Soldier_KDF_GEN_CRW";
};

class BWI_Vehicle_KDF_B_M1025_W: BWI_Vehicle_KDF_M1025_W {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
	crew = "BWI_Soldier_KDF_B_GEN_R";
};

class BWI_Vehicle_KDF_B_M1025_M2_W: BWI_Vehicle_KDF_M1025_M2_W {
	side = WEST;
	faction = "BWI_FACTION_KDF_B";
	crew = "BWI_Soldier_KDF_B_GEN_R";
};