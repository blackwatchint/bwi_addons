class BWI_Flag_NIG {
	scope = 1;
	name = "Nigeria";
	markerClass = "Flags";
	icon = "\bwi_units_baf\data\markers\mkr_nig.paa";
	texture = "\bwi_units_baf\data\markers\mkr_nig.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};