class BWI_Soldier_NIG_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_NIG_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_NIG";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhs_6b28_green_ess",
		"V_TacVest_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_6b28_green_ess",
		"V_TacVest_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_USA_M81";
};


class BWI_Soldier_NIG_GEN_AT: BWI_Soldier_NIG_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_NIG_GEN_AR: BWI_Soldier_NIG_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpk";
};


class BWI_Soldier_NIG_GEN_DMR: BWI_Soldier_NIG_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_t5000_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_t5000_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_5Rnd_338lapua_t5000",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_NIG_GEN_TL: BWI_Soldier_NIG_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_NIG_GEN_MAT: BWI_Soldier_NIG_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3a3",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_NIG_GEN_MMG: BWI_Soldier_NIG_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"UK3CB_BAF_L7A2",
		"Throw",
		"Put"
	};
	magazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"UK3CB_BAF_762_100Rnd_T",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_L7A2";
};

class BWI_Soldier_NIG_GEN_CRW: BWI_Soldier_NIG_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"V_TacVest_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"V_TacVest_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_NIG_T72: rhs_t72ba_tv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_NIG";
	crew = "BWI_Soldier_NIG_GEN_CRW";
};

class BWI_Vehicle_NIG_BMP1: rhs_bmp1_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_NIG";
	crew = "BWI_Soldier_NIG_GEN_CRW";
};

class BWI_Vehicle_NIG_BTR70: rhs_btr70_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_NIG";
	crew = "BWI_Soldier_NIG_GEN_CRW";
};

class BWI_Vehicle_NIG_RG33_M2: rhsusf_rg33_m2_usmc_wd {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_NIG";
	crew = "BWI_Soldier_NIG_GEN_R";
};

class BWI_Vehicle_NIG_RG33: rhsusf_rg33_usmc_wd {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_NIG";
	crew = "BWI_Soldier_NIG_GEN_R";
};