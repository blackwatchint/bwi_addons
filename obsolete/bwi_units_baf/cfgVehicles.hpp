// Shared base classes.
class B_Soldier_base_F;
class O_Soldier_base_F;
class C_man_1;

class rhsusf_m1025_w;
class rhsusf_m1025_w_m2;
class rhsusf_rg33_m2_usmc_wd;
class rhsusf_rg33_usmc_wd;
class rhsusf_m113_usarmy_m240;
class rhsusf_m113_usarmy_unarmed;
class rhsusf_m113_usarmy;
class rhsgref_hidf_m113a3_m2;
class rhs_t72ba_tv;
class rhs_btr70_msv;
class rhs_bmp1_msv;
class rhsgref_BRDM2_vdv;
class rhsgref_BRDM2UM_vdv;
class rhsgref_BRDM2_HQ_vdv;
class rhsgref_BRDM2_ATGM_vdv;
class rhs_uaz_vdv;
class rhs_uaz_open_vdv;

// Add ammo to backpacks.
#include "configs/backpack_ammo.hpp"
	
// Include faction configs.
#include "configs/i_nigeria_army/units.hpp"
#include "configs/b_nigeria_army/units.hpp"
#include "configs/i_kenya_kdf/units.hpp"
#include "configs/b_kenya_kdf/units.hpp"
#include "configs/i_argentina_army/units.hpp"
#include "configs/o_argentina_army/units.hpp"
#include "configs/i_argentina_army_falklands/units.hpp"
#include "configs/o_argentina_army_falklands/units.hpp"