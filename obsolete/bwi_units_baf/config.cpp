#include "defines.hpp"

class CfgPatches {
	class bwi_units_baf {
		requiredVersion = 1;
		author = "Black Watch International";
		authors[] = { "Fourjays" };
		authorURL = "http://blackwatch-int.com";
		version = 2.5.0;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"ace_nouniformrestrictions",
			"rhs_main",
			"rhsusf_main",
			"rhsgref_main",
			"bwi_units",
			"uk3cb_baf_weapons_SmallArms",
			"uk3cb_baf_units_temperate"
		};
		units[] = {			
			#include "configs\i_nigeria_army\classes.hpp"
			#include "configs\b_nigeria_army\classes.hpp"
			#include "configs\i_kenya_kdf\classes.hpp"
			#include "configs\b_kenya_kdf\classes.hpp"
			#include "configs\i_argentina_army\classes.hpp"
			#include "configs\o_argentina_army\classes.hpp"
			#include "configs\i_argentina_army_falklands\classes.hpp"
			#include "configs\o_argentina_army_falklands\classes.hpp"
		};
	};
};


class CfgFactionClasses {	
	#include "cfgFactionClasses.hpp"
};

class CfgVehicles {
	#include "cfgVehicles.hpp"
};

class CfgWeapons {
	#include "cfgWeapons.hpp"
};

class CfgGroups {
	#include "cfgGroups.hpp"
};

class CfgMarkers 
{
	#include "cfgMarkers.hpp"
};