class Indep
{
	name="$STR_A3_CfgGroups_Indep0";
	#include "configs/i_taliban/groups.hpp"
	#include "configs/i_mujahideen/groups.hpp"
	#include "configs/i_free_syrian_army/groups.hpp"
	#include "configs/i_syrian_army/groups.hpp"
	#include "configs/i_iraqi_army/groups.hpp"
	#include "configs/i_hezbollah/groups.hpp"
	#include "configs/i_egypt_army/groups.hpp"
	#include "configs/i_turkey_army/groups.hpp"
	#include "configs/i_saudi_army/groups.hpp"
};
class East
{
	name="$STR_A3_CfgGroups_East0";
	#include "configs/o_syrian_army/groups.hpp"
	#include "configs/o_iran_rev_guards_b/groups.hpp"	
	#include "configs/o_iran_rev_guards_d/groups.hpp"	
	#include "configs/o_taliban/groups.hpp"
	#include "configs/o_free_syrian_army/groups.hpp"
	#include "configs/o_hezbollah/groups.hpp"
};
class West
{
	name="$STR_A3_CfgGroups_West0";
	#include "configs/b_mujahideen/groups.hpp"
	#include "configs/b_iraqi_army/groups.hpp"
	#include "configs/b_egypt_army/groups.hpp"
	#include "configs/b_turkey_army/groups.hpp"
	#include "configs/b_saudi_army/groups.hpp"
};