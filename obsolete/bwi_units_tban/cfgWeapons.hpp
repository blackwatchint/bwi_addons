// Shared base classes.
class UniformItem;
class Uniform_Base;
class U_B_CombatUniform_mcam: Uniform_Base {
	class ItemInfo;
};

class V_PlateCarrier1_rgr;
class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr {
	class ItemInfo;
};

class H_HelmetB;
class H_HelmetIA: H_HelmetB {
	class ItemInfo;
};
	

// Include faction configs.
#include "configs/i_taliban/gear.hpp"
#include "configs/o_taliban/gear.hpp"
#include "configs/b_mujahideen/gear.hpp"
#include "configs/o_iran_rev_guards_b/gear.hpp"
#include "configs/o_iran_rev_guards_d/gear.hpp"
#include "configs/b_iraqi_army/gear.hpp"
#include "configs/i_egypt_army/gear.hpp"
#include "configs/i_turkey_army/gear.hpp"
#include "configs/i_saudi_army/gear.hpp"