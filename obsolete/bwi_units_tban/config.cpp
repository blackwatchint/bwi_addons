#include "defines.hpp"

class CfgPatches {
	class bwi_units_tban {
		requiredVersion = 1;
		author = "Black Watch International";
		authors[] = { "Fourjays", "Tebro" };
		authorURL = "http://blackwatch-int.com";
		version = 2.5.0;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"rhs_main",
			"rhsusf_main",
			"bwi_units",
			"Taliban_fighters"
		};
		units[] = {			
			#include "configs\i_taliban\classes.hpp"
			#include "configs\i_mujahideen\classes.hpp"
			#include "configs\i_free_syrian_army\classes.hpp"
			#include "configs\i_syrian_army\classes.hpp"
			#include "configs\o_iran_rev_guards_b\classes.hpp"
			#include "configs\o_iran_rev_guards_d\classes.hpp"
			#include "configs\o_taliban\classes.hpp"
			#include "configs\o_free_syrian_army\classes.hpp"
			#include "configs\o_syrian_army\classes.hpp"
			#include "configs\b_mujahideen\classes.hpp"
			#include "configs\b_iraqi_army\classes.hpp"
			#include "configs\i_iraqi_army\classes.hpp"
			#include "configs\i_hezbollah\classes.hpp"
			#include "configs\o_hezbollah\classes.hpp"
			#include "configs\i_egypt_army\classes.hpp"
			#include "configs\b_egypt_army\classes.hpp"
			#include "configs\i_turkey_army\classes.hpp"
			#include "configs\b_turkey_army\classes.hpp"
			#include "configs\i_saudi_army\classes.hpp"
			#include "configs\b_saudi_army\classes.hpp"
		};
	};
};


class CfgFactionClasses {	
	#include "cfgFactionClasses.hpp"
};


class CfgVehicles {
	#include "cfgVehicles.hpp"
};


class CfgWeapons {
	#include "cfgWeapons.hpp"
};


class CfgGroups
{
	#include "cfgGroups.hpp"
};

class CfgMarkers 
{
	#include "cfgMarkers.hpp"
};