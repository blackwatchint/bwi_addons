class BWI_Flag_SYR {
	scope = 1;
	name = "Syria";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_syr.paa";
	texture = "\bwi_units_tban\data\markers\mkr_syr.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};