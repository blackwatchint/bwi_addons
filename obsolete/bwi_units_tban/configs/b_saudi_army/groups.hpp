class BWI_FACTION_SAU_B
{
	name = "Royal Saudi Land Forces";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_SAU_B
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_SAU_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_AT";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_TL";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_TL";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_DMR";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_SAU_B
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_SAU_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_SAU_B
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_SAU_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_HAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_SAU_B
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_SAU_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_SAU_B
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_SAU_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_SAU_B_MAR_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};