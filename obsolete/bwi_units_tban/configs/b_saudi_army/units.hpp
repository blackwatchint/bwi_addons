class BWI_Soldier_SAU_B_MAR_R : BWI_Soldier_SAU_MAR_R {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_AT: BWI_Soldier_SAU_MAR_AT {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_AR: BWI_Soldier_SAU_MAR_AR {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_DMR: BWI_Soldier_SAU_MAR_DMR {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_TL: BWI_Soldier_SAU_MAR_TL {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_HAT: BWI_Soldier_SAU_MAR_HAT {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_MMG: BWI_Soldier_SAU_MAR_MMG {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};
class BWI_Soldier_SAU_B_MAR_CRW: BWI_Soldier_SAU_MAR_CRW {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
};


class BWI_Vehicle_SAU_B_M1A1: BWI_Vehicle_SAU_M1A1 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_CRW";
};

class BWI_Vehicle_SAU_B_M2A3: BWI_Vehicle_SAU_M2A3 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_CRW";
};

class BWI_Vehicle_SAU_B_M113_M240: BWI_Vehicle_SAU_M113_M240 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_CRW";
};

class BWI_Vehicle_SAU_B_M113: BWI_Vehicle_SAU_M113 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_CRW";
};

class BWI_Vehicle_SAU_B_M113_M2: BWI_Vehicle_SAU_M113_M2 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_CRW";
};

class BWI_Vehicle_SAU_B_RG33_M2: BWI_Vehicle_SAU_RG33_M2 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_R";
};

class BWI_Vehicle_SAU_B_RG33: BWI_Vehicle_SAU_RG33 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_R";
};

class BWI_Vehicle_SAU_B_M1025_M2: BWI_Vehicle_SAU_M1025_M2 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_R";
};

class BWI_Vehicle_SAU_B_M1025: BWI_Vehicle_SAU_M1025 {
	side = WEST;
	faction = "BWI_FACTION_SAU_B";
	crew = "BWI_Soldier_SAU_B_MAR_R";
};