class BWI_Uniform_IRQ_DCU: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (DCU - Iraq)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_3CO_Iraq.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_IRQ_GEN_R";
		containerClass = "Supply40";
		mass = 40;
	};
};