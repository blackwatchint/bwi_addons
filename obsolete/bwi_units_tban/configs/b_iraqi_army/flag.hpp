class BWI_Flag_IRQ {
	scope = 1;
	name = "Iraq (2008-)";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_irq2.paa";
	texture = "\bwi_units_tban\data\markers\mkr_irq2.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};