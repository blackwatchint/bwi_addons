class BWI_Soldier_SYR_O_GEN_R : BWI_Soldier_SYR_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_AT: BWI_Soldier_SYR_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_AR: BWI_Soldier_SYR_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_DMR: BWI_Soldier_SYR_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_TL: BWI_Soldier_SYR_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_MAT: BWI_Soldier_SYR_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_MMG: BWI_Soldier_SYR_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Soldier_SYR_O_GEN_CRW: BWI_Soldier_SYR_GEN_CRW {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
};
class BWI_Vehicle_SYR_O_T72_D: BWI_Vehicle_SYR_T72_D {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
	crew = "BWI_Soldier_SYR_O_GEN_CRW";
};


class BWI_Vehicle_SYR_O_BMP1_D: BWI_Vehicle_SYR_BMP1_D {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
	crew = "BWI_Soldier_SYR_O_GEN_CRW";
};

class BWI_Vehicle_SYR_O_BMP2_D: BWI_Vehicle_SYR_BMP2_D {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
	crew = "BWI_Soldier_SYR_O_GEN_CRW";
};

class BWI_Vehicle_SYR_O_BTR80_D: BWI_Vehicle_SYR_BTR80_D {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
	crew = "BWI_Soldier_SYR_O_GEN_CRW";
};

class BWI_Vehicle_SYR_O_BTR60_D: BWI_Vehicle_SYR_BTR60_D {
	side = EAST;
	faction = "BWI_FACTION_SYR_O";
	crew = "BWI_Soldier_SYR_O_GEN_CRW";
};