class BWI_Soldier_TUR_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_TUR_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_Greek",
		"Head_TK",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_g3ka4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3ka4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_TUR_TUB",
		"BWI_Vest_TUR_TUB",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_TUR_TUB",
		"BWI_Vest_TUR_TUB",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_TUR_TUB";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_TUBITAK_Turkey.paa"
	};
};


class BWI_Soldier_TUR_GEN_AT: BWI_Soldier_TUR_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_g3ka4",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3ka4",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_TUR_GEN_AR: BWI_Soldier_TUR_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_cbr_f_m249";
};


class BWI_Soldier_TUR_GEN_DMR: BWI_Soldier_TUR_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_TUR_GEN_TL: BWI_Soldier_TUR_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_g3ka4_GL_w_ACOG",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3ka4_GL_w_ACOG",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_TUR_GEN_MAT: BWI_Soldier_TUR_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_g3ka4",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_g3ka4",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_cbr_f_rpg7";
};


class BWI_Soldier_TUR_GEN_MMG: BWI_Soldier_TUR_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_cbr_f_mg3";
};


class BWI_Soldier_TUR_GEN_CRW: BWI_Soldier_TUR_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_TUR_TUB",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_TUR_TUB",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_TUR_Leopard: I_MBT_03_cannon_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_CRW";
};

class BWI_Vehicle_TUR_BTR80: rhs_btr80_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_CRW";
};

class BWI_Vehicle_TUR_M113_M240: rhsusf_m113_usarmy_m240 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_CRW";
};

class BWI_Vehicle_TUR_M113: rhsusf_m113_usarmy_unarmed {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_CRW";
};

class BWI_Vehicle_TUR_M113_M2: rhsusf_m113_usarmy {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_CRW";
};

class BWI_Vehicle_TUR_RG33_M2: rhsusf_rg33_m2_usmc_wd {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_R";
};

class BWI_Vehicle_TUR_RG33: rhsusf_rg33_usmc_wd {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_TUR";
	crew = "BWI_Soldier_TUR_GEN_R";
};