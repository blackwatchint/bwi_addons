class BWI_Flag_TUR {
	scope = 1;
	name = "Turkey";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_tur.paa";
	texture = "\bwi_units_tban\data\markers\mkr_tur.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};