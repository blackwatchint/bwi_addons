class BWI_Soldier_AFGMUJ_B_GEN_R : BWI_Soldier_AFGMUJ_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD1";
};
class BWI_Soldier_AFGMUJ_B_GEN_AT: BWI_Soldier_AFGMUJ_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD8";
};
class BWI_Soldier_AFGMUJ_B_GEN_AR: BWI_Soldier_AFGMUJ_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD12";
};
class BWI_Soldier_AFGMUJ_B_GEN_DMR: BWI_Soldier_AFGMUJ_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD4";
};
class BWI_Soldier_AFGMUJ_B_GEN_TL: BWI_Soldier_AFGMUJ_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD5";
};
class BWI_Soldier_AFGMUJ_B_GEN_MAT: BWI_Soldier_AFGMUJ_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD6";
};
class BWI_Soldier_AFGMUJ_B_GEN_MMG: BWI_Soldier_AFGMUJ_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD7";
};
class BWI_Soldier_AFGMUJ_B_GEN_R2: BWI_Soldier_AFGMUJ_GEN_R2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD8";
};
class BWI_Soldier_AFGMUJ_B_GEN_AT2: BWI_Soldier_AFGMUJ_GEN_AT2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD13";
};
class BWI_Soldier_AFGMUJ_B_GEN_AR2: BWI_Soldier_AFGMUJ_GEN_AR2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD14";
};
class BWI_Soldier_AFGMUJ_B_GEN_DMR2: BWI_Soldier_AFGMUJ_GEN_DMR2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD11";
};
class BWI_Soldier_AFGMUJ_B_GEN_TL2: BWI_Soldier_AFGMUJ_GEN_TL2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD12";
};
class BWI_Soldier_AFGMUJ_B_GEN_MAT2: BWI_Soldier_AFGMUJ_GEN_MAT2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD13";
};
class BWI_Soldier_AFGMUJ_B_GEN_MMG2: BWI_Soldier_AFGMUJ_GEN_MMG2 {
	side = WEST;
	faction = "BWI_FACTION_AFGMUJ_B";
	uniformClass = "BWI_Uniform_AFGMIL_B_DD14";
};