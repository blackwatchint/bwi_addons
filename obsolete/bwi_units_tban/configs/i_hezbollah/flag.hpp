class BWI_Flag_HEZ {
	scope = 1;
	name = "Hezbollah";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_hez.paa";
	texture = "\bwi_units_tban\data\markers\mkr_hez.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};