class BWI_Soldier_HEZ_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_HEZ_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_TK", 
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhs_beret_milp",
		"V_I_G_resistanceLeader_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_beret_milp",
		"V_I_G_resistanceLeader_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_woodland";
};


class BWI_Soldier_HEZ_GEN_AT: BWI_Soldier_HEZ_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akms",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akms",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	uniformClass = "rhsgref_uniform_woodland_olive";
};


class BWI_Soldier_HEZ_GEN_AR: BWI_Soldier_HEZ_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhsgref_uniform_woodland";
	backpack = "B_Carryall_khk_f_rpk74";
};


class BWI_Soldier_HEZ_GEN_DMR: BWI_Soldier_HEZ_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhsgref_uniform_woodland_olive";
};


class BWI_Soldier_HEZ_GEN_TL: BWI_Soldier_HEZ_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhsgref_uniform_woodland";
};


class BWI_Soldier_HEZ_GEN_MAT: BWI_Soldier_HEZ_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akms",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akms",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	uniformClass = "rhsgref_uniform_woodland_olive";
	backpack = "B_Carryall_khk_f_rpg7";
};


class BWI_Soldier_HEZ_GEN_MMG: BWI_Soldier_HEZ_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_100Rnd_762x54mmR",
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_100Rnd_762x54mmR",
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhsgref_uniform_woodland";
	backpack = "B_Carryall_khk_f_pkm";
};


class BWI_Soldier_HEZ_GEN_CRW: BWI_Soldier_HEZ_GEN_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"rhs_tsh4",
		"V_I_G_resistanceLeader_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"V_I_G_resistanceLeader_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_woodland_olive";
};


class BWI_Vehicle_HEZ_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_R";
};

class BWI_Vehicle_HEZ_UAZ_DSHKM: rhs_uaz_dshkm_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_R";
};

class BWI_Vehicle_HEZ_UAZ_SPG9: rhs_uaz_spg9_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_R";
};

class BWI_Vehicle_HEZ_Tech_M2: I_G_Offroad_01_armed_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_HEZ_GEN_R";
};

class BWI_Vehicle_HEZ_Tech: I_G_Offroad_01_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	displayName = "Technical";
	crew = "BWI_Soldier_HEZ_GEN_R";
};

class BWI_Vehicle_HEZ_T72_D: rhs_t72ba_tv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_CRW";
};

class BWI_Vehicle_HEZ_BMP1_D: rhsgref_ins_g_bmp1 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_CRW";
};

class BWI_Vehicle_HEZ_BTR60_D: rhsgref_ins_g_btr60 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_CRW";
};

class BWI_Vehicle_HEZ_BRDM_D: rhsgref_BRDM2_ins_g {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_CRW";
};

class BWI_Vehicle_HEZ_BRDM_ATGM_D: rhsgref_BRDM2_ATGM_ins_g {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_HEZ";
	crew = "BWI_Soldier_HEZ_GEN_CRW";
};