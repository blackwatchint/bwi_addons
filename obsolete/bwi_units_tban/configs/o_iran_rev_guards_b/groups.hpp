class BWI_FACTION_IRN
{
	name = "Iranian Revolutionary Guards (Basij)";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_IRN
		{
			name = "Squad";
			side = EAST;
			faction = "BWI_FACTION_IRN";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_IRN
		{
			name = "Team";
			side = EAST;
			faction = "BWI_FACTION_IRN";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_IRN
		{
			name = "Team (AT)";
			side = EAST;
			faction = "BWI_FACTION_IRN";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_IRN
		{
			name = "Team (MG)";
			side = EAST;
			faction = "BWI_FACTION_IRN";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_IRN
		{
			name = "Patrol";
			side = EAST;
			faction = "BWI_FACTION_IRN";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_IRN_BAS_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};