class BWI_Uniform_IRN_BAS: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (Basij - Iran)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_BAS_Iran.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_IRN_BAS_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_IRN_BAS: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (Basij - Iran)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\N_Vests_BAS_Iran.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_IRN_BAS: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (Basij - Iran)";
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Helmet_BAS_Iran.paa"
	};
};