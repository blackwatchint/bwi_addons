class BWI_Flag_IRN {
	scope = 1;
	name = "Iran";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_irn.paa";
	texture = "\bwi_units_tban\data\markers\mkr_irn.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};