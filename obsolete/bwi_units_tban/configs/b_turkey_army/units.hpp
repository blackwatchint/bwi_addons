class BWI_Soldier_TUR_B_GEN_R : BWI_Soldier_TUR_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_AT: BWI_Soldier_TUR_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_AR: BWI_Soldier_TUR_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_DMR: BWI_Soldier_TUR_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_TL: BWI_Soldier_TUR_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_MAT: BWI_Soldier_TUR_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_MMG: BWI_Soldier_TUR_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};
class BWI_Soldier_TUR_B_GEN_CRW: BWI_Soldier_TUR_GEN_CRW {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
};


class BWI_Vehicle_TUR_B_Leopard: BWI_Vehicle_TUR_Leopard {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_CRW";
};

class BWI_Vehicle_TUR_B_BTR80: BWI_Vehicle_TUR_BTR80 {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_CRW";
};

class BWI_Vehicle_TUR_B_M113_M240: BWI_Vehicle_TUR_M113_M240 {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_CRW";
};

class BWI_Vehicle_TUR_B_M113: BWI_Vehicle_TUR_M113 {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_CRW";
};

class BWI_Vehicle_TUR_B_M113_M2: BWI_Vehicle_TUR_M113_M2 {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_CRW";
};

class BWI_Vehicle_TUR_B_RG33_M2: BWI_Vehicle_TUR_RG33_M2 {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_R";
};

class BWI_Vehicle_TUR_B_RG33: BWI_Vehicle_TUR_RG33 {
	side = WEST;
	faction = "BWI_FACTION_TUR_B";
	crew = "BWI_Soldier_TUR_B_GEN_R";
};