class BWI_Flag_FSA {
	scope = 1;
	name = "Free Syrian Army";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_fsa.paa";
	texture = "\bwi_units_tban\data\markers\mkr_fsa.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};