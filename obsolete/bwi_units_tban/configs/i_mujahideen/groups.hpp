class BWI_FACTION_AFGMUJ
{
	name = "Mujahideen";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_AFGMUJ
		{
			name = "Squad";
			side = RESISTANCE;
			faction = "BWI_FACTION_AFGMUJ";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_MAT2";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_AR2";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_TL";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_TL2";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_DMR";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_AFGMUJ
		{
			name = "Team";
			side = RESISTANCE;
			faction = "BWI_FACTION_AFGMUJ";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_TL2";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_AT2";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R2";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_AFGMUJ
		{
			name = "Team (AT)";
			side = RESISTANCE;
			faction = "BWI_FACTION_AFGMUJ";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R2";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_AFGMUJ
		{
			name = "Team (MG)";
			side = RESISTANCE;
			faction = "BWI_FACTION_AFGMUJ";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_TL2";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R2";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_AFGMUJ
		{
			name = "Patrol";
			side = RESISTANCE;
			faction = "BWI_FACTION_AFGMUJ";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R2";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_AFGMUJ_GEN_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};