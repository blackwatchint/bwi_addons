class BWI_Soldier_AFGMUJ_GEN_R : TBan_Fighter1 {
	_generalMacro = "BWI_Soldier_AFGMUJ_GEN_R";
	scope = 2;
	scopeCurator = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFGMUJ";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_TK",
		"G_GUERIL_default",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD1";
};


class BWI_Soldier_AFGMUJ_GEN_AT: BWI_Soldier_AFGMUJ_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_kar98k",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_kar98k",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD8";
};


class BWI_Soldier_AFGMUJ_GEN_AR: BWI_Soldier_AFGMUJ_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_khk_f_rpk";
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD12";
};


class BWI_Soldier_AFGMUJ_GEN_DMR: BWI_Soldier_AFGMUJ_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_m38",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m38",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhsgref_5Rnd_762x54_m38",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD4";
};


class BWI_Soldier_AFGMUJ_GEN_TL: BWI_Soldier_AFGMUJ_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD5";
};


class BWI_Soldier_AFGMUJ_GEN_MAT: BWI_Soldier_AFGMUJ_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak",
		"hlc_30Rnd_762x39_b_ak"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	linkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD6";
};


class BWI_Soldier_AFGMUJ_GEN_MMG: BWI_Soldier_AFGMUJ_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD7";
};


class BWI_Soldier_AFGMUJ_GEN_R2: BWI_Soldier_AFGMUJ_GEN_R {
	weapons[] = {
		"rhs_weap_kar98k",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_kar98k",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhsgref_5Rnd_792x57_kar98k",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD8";
};

class BWI_Soldier_AFGMUJ_GEN_AT2: BWI_Soldier_AFGMUJ_GEN_AT {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD13";
};

class BWI_Soldier_AFGMUJ_GEN_AR2: BWI_Soldier_AFGMUJ_GEN_AR {
	linkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD14";
};

class BWI_Soldier_AFGMUJ_GEN_DMR2: BWI_Soldier_AFGMUJ_GEN_DMR {
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD11";
};
class BWI_Soldier_AFGMUJ_GEN_TL2: BWI_Soldier_AFGMUJ_GEN_TL {
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD12";
};
class BWI_Soldier_AFGMUJ_GEN_MAT2: BWI_Soldier_AFGMUJ_GEN_MAT {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD13";
};
class BWI_Soldier_AFGMUJ_GEN_MMG2: BWI_Soldier_AFGMUJ_GEN_MMG {
	linkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_AFGMIL_DD14";
};