class BWI_Flag_AFGMUJ {
	scope = 1;
	name = "Mujahideen";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_muj.paa";
	texture = "\bwi_units_tban\data\markers\mkr_muj.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};