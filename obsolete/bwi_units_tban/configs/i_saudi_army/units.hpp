class BWI_Soldier_SAU_MAR_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_SAU_MAR_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_TK",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_auga1carb_t",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_auga1carb_t",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_SAU_MAR",
		"BWI_Vest_SAU_MAR",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_SAU_MAR",
		"BWI_Vest_SAU_MAR",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_SAU_MAR";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_MARPAT_Saudi.paa"
	};
};


class BWI_Soldier_SAU_MAR_AT: BWI_Soldier_SAU_MAR_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_auga1carb_t",
		"launch_NLAW_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_auga1carb_t",
		"launch_NLAW_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"ACE_PreloadedMissileDummy",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"ACE_PreloadedMissileDummy",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_SAU_MAR_AR: BWI_Soldier_SAU_MAR_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_cbr_f_m249";
};


class BWI_Soldier_SAU_MAR_DMR: BWI_Soldier_SAU_MAR_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_PSG1A1_RIS_w_LRPS",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_PSG1A1_RIS_w_LRPS",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_b_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"hlc_20rnd_762x51_T_G3",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_SAU_MAR_TL: BWI_Soldier_SAU_MAR_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_m4a1_carryhandle_m203_w_ACOG",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m4a1_carryhandle_m203_w_ACOG",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_SAU_MAR_HAT: BWI_Soldier_SAU_MAR_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_auga1carb_t",
		"rhs_weap_fgm148",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_auga1carb_t",
		"rhs_weap_fgm148",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_B_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"hlc_30Rnd_556x45_T_AUG",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_cbr_f_Javelin";
};


class BWI_Soldier_SAU_MAR_MMG: BWI_Soldier_SAU_MAR_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_MG3KWS_b_w_ELCAN",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_Barrier_MG3",
		"hlc_100Rnd_762x51_Barrier_MG3",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_cbr_f_mg3";
};


class BWI_Soldier_SAU_MAR_CRW: BWI_Soldier_SAU_MAR_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhsusf_cvc_helmet",
		"BWI_Vest_SAU_MAR",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_helmet",
		"BWI_Vest_SAU_MAR",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_SAU_M1A1: rhsusf_m1a1aimd_usarmy {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_CRW";
};

class BWI_Vehicle_SAU_M2A3: RHS_M2A3 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_CRW";
};

class BWI_Vehicle_SAU_M113_M240: rhsusf_m113d_usarmy_m240 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_CRW";
};

class BWI_Vehicle_SAU_M113: rhsusf_m113d_usarmy_unarmed {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_CRW";
};

class BWI_Vehicle_SAU_M113_M2: rhsusf_m113d_usarmy {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_CRW";
};

class BWI_Vehicle_SAU_RG33_M2: rhsusf_rg33_m2_usmc_d {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_R";
};

class BWI_Vehicle_SAU_RG33: rhsusf_rg33_usmc_d {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_R";
};

class BWI_Vehicle_SAU_M1025_M2: rhsusf_m1025_d_m2 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_R";
};

class BWI_Vehicle_SAU_M1025: rhsusf_m1025_d {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SAU";
	crew = "BWI_Soldier_SAU_MAR_R";
};