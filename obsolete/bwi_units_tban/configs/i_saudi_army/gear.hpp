class BWI_Uniform_SAU_MAR: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (MARPAT - Saudi Arabia)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_MARPAT_Saudi.paa"
	};
	class ItemInfo: ItemInfo {
		modelSides[] = {0,1,2,3};
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_SAU_MAR_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_SAU_MAR: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (MARPAT - Saudi Arabia)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\N_Vests_MARPAT_Saudi.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_SAU_MAR: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (MARPAT - Saudi Arabia)";
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Helmet_MARPAT_Saudi.paa"
	};
};