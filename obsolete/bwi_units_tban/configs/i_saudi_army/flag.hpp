class BWI_Flag_SAU {
	scope = 1;
	name = "Saudi Arabia";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_saudi.paa";
	texture = "\bwi_units_tban\data\markers\mkr_saudi.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};