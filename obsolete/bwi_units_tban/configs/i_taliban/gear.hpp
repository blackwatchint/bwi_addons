class U_Afghan02NH;
class BWI_Uniform_AFGMIL_DD1: U_Afghan02NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_02NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_R";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan01NH;
class BWI_Uniform_AFGMIL_DD2: U_Afghan01NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_01NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_AT";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan03NH;
class BWI_Uniform_AFGMIL_DD3: U_Afghan03NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_03NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_AR";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_DD4: U_Afghan02NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_02NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_DMR";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan05;
class BWI_Uniform_AFGMIL_DD5: U_Afghan05 {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_05.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_TL";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan06;
class BWI_Uniform_AFGMIL_DD6: U_Afghan06 {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_06.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_MAT";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan04;
class BWI_Uniform_AFGMIL_DD7: U_Afghan04 {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_04.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_MMG";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan06NH;
class BWI_Uniform_AFGMIL_DD8: U_Afghan06NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_06NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_R2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_DD9: U_Afghan02NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_02NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_AT2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_DD10: U_Afghan01NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_01NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_AR2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_DD11: U_Afghan06NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_06NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_DMR2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_DD12: U_Afghan01NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_01NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_TL2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_Afghan03;
class BWI_Uniform_AFGMIL_DD13: U_Afghan03 {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_03.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_MAT2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_DD14: U_Afghan03NH {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_03NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_GEN_MMG2";
		containerClass = "Supply40";
		mass = 40;
	};
};