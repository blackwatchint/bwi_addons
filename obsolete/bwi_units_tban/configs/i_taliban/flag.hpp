class BWI_Flag_AFGMIL {
	scope = 1;
	name = "Taliban";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_tali.paa";
	texture = "\bwi_units_tban\data\markers\mkr_tali.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};