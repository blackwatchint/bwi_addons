class BWI_Soldier_AFGMIL_GEN_R : TBan_Fighter1 {
	_generalMacro = "BWI_Soldier_AFGMIL_GEN_R";
	scope = 2;
	scopeCurator = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFGMIL";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_TK",
		"G_GUERIL_default",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"V_BandollierB_rgr",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_02NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD1";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil02_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_AT: BWI_Soldier_AFGMIL_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_06NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD8";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil06_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_AR: BWI_Soldier_AFGMIL_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk74";
	linkedItems[] = {
		"V_BandollierB_blk",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_01NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD12";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil01_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_DMR: BWI_Soldier_AFGMIL_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"H_Shemag_olive",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Shemag_olive",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_02NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD4";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil02_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_TL: BWI_Soldier_AFGMIL_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_05";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD5";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil05_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_MAT: BWI_Soldier_AFGMIL_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	linkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_06";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD6";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil06_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_MMG: BWI_Soldier_AFGMIL_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_04";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD7";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil04_1_co.paa"
	};
};


class BWI_Soldier_AFGMIL_GEN_R2: BWI_Soldier_AFGMIL_GEN_R {
	linkedItems[] = {
		"V_BandollierB_oli",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_06NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD8";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil06_1_co.paa"
	};
};

class BWI_Soldier_AFGMIL_GEN_AT2: BWI_Soldier_AFGMIL_GEN_AT {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_03";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD13";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil03_1_co.paa"
	};
};

class BWI_Soldier_AFGMIL_GEN_AR2: BWI_Soldier_AFGMIL_GEN_AR {
	linkedItems[] = {
		"H_ShemagOpen_khk",
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_ShemagOpen_khk",
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_03NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD14";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil03_1_co.paa"
	};
};

class BWI_Soldier_AFGMIL_GEN_DMR2: BWI_Soldier_AFGMIL_GEN_DMR {
	linkedItems[] = {
		"V_BandollierB_oli",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_06NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD11";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil06_1_co.paa"
	};
};
class BWI_Soldier_AFGMIL_GEN_TL2: BWI_Soldier_AFGMIL_GEN_TL {
	linkedItems[] = {
		"V_BandollierB_blk",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"H_Shemag_olive",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_01NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD12";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil01_1_co.paa"
	};
};
class BWI_Soldier_AFGMIL_GEN_MAT2: BWI_Soldier_AFGMIL_GEN_MAT {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_03";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD13";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil03_1_co.paa"
	};
};
class BWI_Soldier_AFGMIL_GEN_MMG2: BWI_Soldier_AFGMIL_GEN_MMG {
	linkedItems[] = {
		"H_ShemagOpen_khk",
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_ShemagOpen_khk",
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\Taliban_Fighters\Uniforms\Afghan_03NH";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_AFGMIL_DD14";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"Taliban_Fighters\data\tak_civil03_1_co.paa"
	};
};


class BWI_Vehicle_AFGMIL_Tech_M2: B_G_Offroad_01_armed_F
{
	scope = 2;
	scopeCurator = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFGMIL";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_AFGMIL_GEN_R";
};

class BWI_Vehicle_AFGMIL_Tech: B_G_Offroad_01_F
{
	scope = 2;
	scopeCurator = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFGMIL";
	displayName = "Technical";
	crew = "BWI_Soldier_AFGMIL_GEN_R2";
};