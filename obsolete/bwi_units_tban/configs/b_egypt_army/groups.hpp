class BWI_FACTION_EGY_B
{
	name = "Egyptian Army";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_EGY_B
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_EGY_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_AT";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_TL";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_TL";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_DMR";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_EGY_B
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_EGY_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_EGY_B
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_EGY_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_EGY_B
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_EGY_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_EGY_B
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_EGY_B";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_EGY_B_GEN_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};