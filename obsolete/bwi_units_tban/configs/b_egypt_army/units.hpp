class BWI_Soldier_EGY_B_GEN_R : BWI_Soldier_EGY_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_AT: BWI_Soldier_EGY_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_AR: BWI_Soldier_EGY_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_DMR: BWI_Soldier_EGY_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_TL: BWI_Soldier_EGY_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_MAT: BWI_Soldier_EGY_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_MMG: BWI_Soldier_EGY_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};
class BWI_Soldier_EGY_B_GEN_CRW: BWI_Soldier_EGY_GEN_CRW {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
};


class BWI_Vehicle_EGY_B_M1A1_D: BWI_Vehicle_EGY_M1A1_D {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_BMP1: BWI_Vehicle_EGY_BMP1 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_BTR60: BWI_Vehicle_EGY_BTR60 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_BRDM2: BWI_Vehicle_EGY_BRDM2 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_BRDM2_ATGM: BWI_Vehicle_EGY_BRDM2_ATGM {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_M113_M240: BWI_Vehicle_EGY_M113_M240 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_M113: BWI_Vehicle_EGY_M113 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_M113_M2: BWI_Vehicle_EGY_M113_M2 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_CRW";
};

class BWI_Vehicle_EGY_B_RG33_M2: BWI_Vehicle_EGY_RG33_M2 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_R";
};

class BWI_Vehicle_EGY_B_RG33: BWI_Vehicle_EGY_RG33 {
	side = WEST;
	faction = "BWI_FACTION_EGY_B";
	crew = "BWI_Soldier_EGY_B_GEN_R";
};