class BWI_Soldier_IRQ_I_GEN_R : BWI_Soldier_IRQ_GEN_R {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_AT: BWI_Soldier_IRQ_GEN_AT {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_AR: BWI_Soldier_IRQ_GEN_AR {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_DMR: BWI_Soldier_IRQ_GEN_DMR {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_TL: BWI_Soldier_IRQ_GEN_TL {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_MAT: BWI_Soldier_IRQ_GEN_MAT {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_MMG: BWI_Soldier_IRQ_GEN_MMG {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};
class BWI_Soldier_IRQ_I_GEN_CRW: BWI_Soldier_IRQ_GEN_CRW {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
};


class BWI_Vehicle_IRQ_I_T72_D: BWI_Vehicle_IRQ_T72_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_BMP1_D: BWI_Vehicle_IRQ_BMP1_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_BRDM2_D: BWI_Vehicle_IRQ_BRDM2_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_BRDM2_ATGM_D: BWI_Vehicle_IRQ_BRDM2_ATGM_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_BTR80_D: BWI_Vehicle_IRQ_BTR80_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_M1A1_D: BWI_Vehicle_IRQ_M1A1_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_M113_M2_D: BWI_Vehicle_IRQ_M113_M2_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_M113_M240_D: BWI_Vehicle_IRQ_M113_M240_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_M113_D: BWI_Vehicle_IRQ_M113_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_CRW";
};

class BWI_Vehicle_IRQ_I_M1025_D: BWI_Vehicle_IRQ_M1025_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_R";
};

class BWI_Vehicle_IRQ_I_M1025_M2_D: BWI_Vehicle_IRQ_M1025_M2_D {
	side = RESISTANCE;
	faction = "BWI_FACTION_IRQ_I";
	crew = "BWI_Soldier_IRQ_I_GEN_R";
};