class BWI_Soldier_EGY_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_EGY_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_TK",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_USA_DCU",
		"BWI_Vest_USA_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_USA_DCU",
		"BWI_Vest_USA_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_EGY_DCU";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_3CO_Egypt.paa"
	};
};


class BWI_Soldier_EGY_GEN_AT: BWI_Soldier_EGY_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_EGY_GEN_AR: BWI_Soldier_EGY_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_cbr_f_m249";
};


class BWI_Soldier_EGY_GEN_DMR: BWI_Soldier_EGY_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_EGY_GEN_TL: BWI_Soldier_EGY_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_Colt727_GL",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727_GL",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_EGY_GEN_MAT: BWI_Soldier_EGY_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_cbr_f_rpg7";
};


class BWI_Soldier_EGY_GEN_MMG: BWI_Soldier_EGY_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_cbr_f_pkm";
};

class BWI_Soldier_EGY_GEN_CRW: BWI_Soldier_EGY_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_USA_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_USA_DCU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_EGY_M1A1_D: rhsusf_m1a1aimd_usarmy {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_BMP1: rhs_bmp1_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_BTR60: rhs_btr60_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_BRDM2: rhsgref_BRDM2_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_BRDM2_ATGM: rhsgref_BRDM2_ATGM_msv {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_M113_M240: rhsusf_m113d_usarmy_m240 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_M113: rhsusf_m113d_usarmy_unarmed {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_M113_M2: rhsusf_m113d_usarmy {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_CRW";
};

class BWI_Vehicle_EGY_RG33_M2: rhsusf_rg33_m2_usmc_d {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_R";
};

class BWI_Vehicle_EGY_RG33: rhsusf_rg33_usmc_d {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_EGY";
	crew = "BWI_Soldier_EGY_GEN_R";
};