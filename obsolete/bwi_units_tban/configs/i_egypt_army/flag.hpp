class BWI_Flag_EGY {
	scope = 1;
	name = "Egypt";
	markerClass = "Flags";
	icon = "\bwi_units_tban\data\markers\mkr_egy.paa";
	texture = "\bwi_units_tban\data\markers\mkr_egy.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};