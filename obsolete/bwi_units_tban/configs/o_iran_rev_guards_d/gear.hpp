class BWI_Uniform_IRN_DES: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (Desert - Iran)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_DES_Iran.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_IRN_DES_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_IRN_DES: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (Desert - Iran)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\N_Vests_DES_Iran.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_IRN_DES: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (Desert - Iran)";
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Helmet_DES_Iran.paa"
	};
};