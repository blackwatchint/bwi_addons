class BWI_Soldier_IRN_DES_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_IRN_DES_R";
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F", 
		"Head_TK", 
		"NoGlasses"
	};
	weapons[] = {
		"arifle_Katiba_F_w_Aco",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_Katiba_F_w_Aco",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_IRN_DES",
		"BWI_Vest_IRN_DES",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_IRN_DES",
		"BWI_Vest_IRN_DES",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_IRN_DES";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units_tban\data\I_Clothing_DES_Iran.paa"
	};
};


class BWI_Soldier_IRN_DES_AT: BWI_Soldier_IRN_DES_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"arifle_Katiba_F_w_Aco",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_Katiba_F_w_Aco",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_IRN_DES_AR: BWI_Soldier_IRN_DES_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_cbr_f_rpk74";
};


class BWI_Soldier_IRN_DES_DMR: BWI_Soldier_IRN_DES_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_IRN_DES_TL: BWI_Soldier_IRN_DES_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"arifle_Katiba_GL_F_w_MRCO",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_Katiba_GL_F_w_MRCO",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_IRN_DES_MAT: BWI_Soldier_IRN_DES_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"arifle_Katiba_F_w_Aco",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_Katiba_F_w_Aco",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"30Rnd_65x39_caseless_green_mag_Tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_cbr_f_rpg7";
};


class BWI_Soldier_IRN_DES_MMG: BWI_Soldier_IRN_DES_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkp",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkp",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_cbr_f_pkm";
};


class BWI_Soldier_IRN_DES_CRW: BWI_Soldier_IRN_DES_R {
	displayName = "Crew";
	weapons[] = {
		"arifle_Katiba_C_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_Katiba_C_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"30Rnd_65x39_caseless_green",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"rhsusf_cvc_helmet",
		"BWI_Vest_IRN_DES",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_helmet",
		"BWI_Vest_IRN_DES",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_IRN_Zulfiqar_D: O_MBT_02_cannon_F {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	vehicleClass = "rhs_vehclass_tank";
	displayName = "Zulfiqar 3";
	crew = "BWI_Soldier_IRN_DES_CRW";
};

class BWI_Vehicle_IRN_T72_D: rhs_t72bc_tv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	crew = "BWI_Soldier_IRN_DES_CRW";
};

class BWI_Vehicle_IRN_BMP1_D: rhs_bmp1_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	crew = "BWI_Soldier_IRN_DES_CRW";
};

class BWI_Vehicle_IRN_BMP2_D: rhs_bmp2_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	crew = "BWI_Soldier_IRN_DES_CRW";
};

class BWI_Vehicle_IRN_BTR60_D: rhs_btr60_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	crew = "BWI_Soldier_IRN_DES_CRW";
};

class BWI_Vehicle_IRN_M113_D: rhsusf_m113d_usarmy {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_IRN_D";
	crew = "BWI_Soldier_IRN_DES_CRW";
};