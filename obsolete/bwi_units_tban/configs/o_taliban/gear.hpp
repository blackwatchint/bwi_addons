class BWI_Uniform_AFGMIL_O_DD1: BWI_Uniform_AFGMIL_DD1 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_02NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_R";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD2: BWI_Uniform_AFGMIL_DD2 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_01NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_AT";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD3: BWI_Uniform_AFGMIL_DD3 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_03NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_AR";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD4: BWI_Uniform_AFGMIL_DD4 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_02NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_DMR";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD5: BWI_Uniform_AFGMIL_DD5 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_05.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_TL";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD6: BWI_Uniform_AFGMIL_DD6 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_06.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_MAT";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD7: BWI_Uniform_AFGMIL_DD7 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_04.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_MMG";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD8: BWI_Uniform_AFGMIL_DD8 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_06NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_R2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD9: BWI_Uniform_AFGMIL_DD9 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_02NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_AT2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD10: BWI_Uniform_AFGMIL_DD10 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_01NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_AR2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD11: BWI_Uniform_AFGMIL_DD11 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_06NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_DMR2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD12: BWI_Uniform_AFGMIL_DD12 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_01NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_TL2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD13: BWI_Uniform_AFGMIL_DD13 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_03.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_MAT2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_AFGMIL_O_DD14: BWI_Uniform_AFGMIL_DD14 {

	class ItemInfo: UniformItem {
		uniformModel = "\Taliban_Fighters\Uniforms\Afghan_03NH.p3d";
		uniformClass = "BWI_Soldier_AFGMIL_O_GEN_MMG2";
		containerClass = "Supply40";
		mass = 40;
	};
};