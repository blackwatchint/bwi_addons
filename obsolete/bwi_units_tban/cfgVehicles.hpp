// Shared base classes.
class B_Soldier_base_F;
class B_G_Offroad_01_F;
class B_G_Offroad_01_armed_F;
class I_G_Offroad_01_armed_F;
class I_G_Offroad_01_F;
class O_MBT_02_cannon_F;
class I_MBT_03_cannon_F;

class Tban_O_Offroad_01_F: B_G_Offroad_01_armed_F {
	scope = 1;
};

class rhs_t72ba_tv;
class rhs_t72bc_tv;
class rhs_bmp1_msv;
class rhs_bmp2_msv;
class rhs_btr80_msv;
class rhs_btr60_msv;
class rhs_uaz_open_chdkz;
class rhs_uaz_dshkm_chdkz;
class rhs_uaz_spg9_chdkz;
class rhsgref_ins_g_bmp1;
class rhsgref_ins_g_btr60;
class rhsgref_BRDM2_ins_g;
class rhsgref_BRDM2_ATGM_ins_g;
class rhsgref_BRDM2_msv;
class rhsgref_BRDM2_ATGM_msv;
class rhsusf_m1a1aimd_usarmy;
class RHS_M2A3;
class rhsusf_m113_usarmy;
class rhsusf_m113_usarmy_M240;
class rhsusf_m113_usarmy_unarmed;
class rhsusf_m113d_usarmy;
class rhsusf_m113d_usarmy_M240;
class rhsusf_m113d_usarmy_unarmed;
class rhsusf_m1025_d;
class rhsusf_m1025_d_m2;
class rhsusf_rg33_m2_usmc_d;
class rhsusf_rg33_usmc_d;
class rhsusf_rg33_m2_usmc_wd;
class rhsusf_rg33_usmc_wd;


// Hide TBan default units.
#include "configs/hide_tban.hpp"
	
// Include faction configs.
#include "configs/i_taliban/units.hpp"
#include "configs/i_mujahideen/units.hpp"
#include "configs/i_free_syrian_army/units.hpp"
#include "configs/i_syrian_army/units.hpp"
#include "configs/o_iran_rev_guards_b/units.hpp"
#include "configs/o_iran_rev_guards_d/units.hpp"
#include "configs/o_taliban/units.hpp"
#include "configs/o_free_syrian_army/units.hpp"
#include "configs/o_syrian_army/units.hpp"
#include "configs/b_mujahideen/units.hpp"
#include "configs/b_iraqi_army/units.hpp"
#include "configs/i_iraqi_army/units.hpp"
#include "configs/i_hezbollah/units.hpp"
#include "configs/o_hezbollah/units.hpp"
#include "configs/i_egypt_army/units.hpp"
#include "configs/b_egypt_army/units.hpp"
#include "configs/i_turkey_army/units.hpp"
#include "configs/b_turkey_army/units.hpp"
#include "configs/i_saudi_army/units.hpp"
#include "configs/b_saudi_army/units.hpp"