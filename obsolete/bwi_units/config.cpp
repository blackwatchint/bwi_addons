#include "defines.hpp"

class CfgPatches {
	class bwi_units {
		requiredVersion = 1;
		author = "Black Watch International";
		authors[] = { "Fourjays" };
		authorURL = "http://blackwatch-int.com";
		version = 2.5.0;
		versionStr = "2.5.0";
		versionAr[] = {2,5,0};
		requiredAddons[] = {
			"ace_nouniformrestrictions",
			"rhs_main",
			"rhsusf_main",
			"rhsgref_main"
		};
		units[] = {			
			#include "configs\i_jemaah_islamiyah\classes.hpp"
			#include "configs\i_boko_haram\classes.hpp"
			#include "configs\i_somali_pirates\classes.hpp"
			#include "configs\i_somali_usc\classes.hpp"
			#include "configs\i_farc\classes.hpp"
			#include "configs\i_sierra_leone_ruf\classes.hpp"
			#include "configs\i_sierra_leone_army\classes.hpp"
			#include "configs\i_liberia_lurd\classes.hpp"
			#include "configs\i_liberia_army\classes.hpp"
			#include "configs\i_pmc_exec_decisions\classes.hpp"
			#include "configs\i_pmc_pp_solutions\classes.hpp"
			#include "configs\o_ussr_msv_cold_war\classes.hpp"
			#include "configs\o_russia_gru\classes.hpp"
			#include "configs\o_jemaah_islamiyah\classes.hpp"
			#include "configs\o_boko_haram\classes.hpp"
			#include "configs\o_somali_pirates\classes.hpp"
			#include "configs\o_somali_usc\classes.hpp"
			#include "configs\o_farc\classes.hpp"
			#include "configs\o_sierra_leone_ruf\classes.hpp"
			#include "configs\o_liberia_army\classes.hpp"
			#include "configs\o_pmc_exec_decisions\classes.hpp"
			#include "configs\o_pmc_pp_solutions\classes.hpp"
			#include "configs\o_dprk_army\classes.hpp"
			#include "configs\b_israel_defence_forces\classes.hpp"
			#include "configs\b_us_army_desert_storm\classes.hpp"
			#include "configs\b_us_army_just_cause\classes.hpp"
			#include "configs\b_us_army_battle_mog\classes.hpp"
			#include "configs\b_us_army_cold_war\classes.hpp"
			#include "configs\b_us_army_vietnam\classes.hpp"
			#include "configs\b_sierra_leone_army\classes.hpp"
			#include "configs\b_liberia_lurd\classes.hpp"
			#include "configs\b_rok_army\classes.hpp"
		};
	};
};


class CfgFactionClasses {	
	#include "cfgFactionClasses.hpp"
};

class CfgVehicles {
	#include "cfgVehicles.hpp"
};

class CfgWeapons {
	#include "cfgWeapons.hpp"
};

class CfgGroups {
	#include "cfgGroups.hpp"
};

class CfgMarkers 
{
	#include "cfgMarkers.hpp"
};