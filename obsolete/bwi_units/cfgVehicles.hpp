// Shared base classes.
class B_Soldier_base_F;
class O_Soldier_base_F;
class C_man_1;

class B_G_Offroad_01_F;
class B_G_Offroad_01_armed_F;
class I_G_Offroad_01_armed_F;
class I_G_Offroad_01_F;

class B_MBT_01_TUSK_F;
class B_MBT_01_cannon_F;
class B_MBT_01_arty_F;
class B_MBT_01_mlrs_F;
class B_APC_Tracked_01_rcws_F;
class B_APC_Tracked_01_AA_F;

class B_Boat_Transport_01_F;

class rhs_bmd2_chdkz;
class rhs_bmd1_chdkz;
class rhs_uaz_open_chdkz;
class rhs_uaz_dshkm_chdkz;
class rhs_uaz_spg9_chdkz;
class rhsusf_m1025_d;
class rhsusf_m1025_w;
class rhsusf_m1025_d_m2;
class rhsusf_m1025_w_m2;
class rhsusf_m113_usarmy;
class rhsusf_m113d_usarmy;
class rhsusf_m113_usarmy_M240;
class rhsusf_m113d_usarmy_M240;
class rhsusf_m113_usarmy_unarmed;
class rhsusf_m113d_usarmy_unarmed;
class rhsusf_m113_usarmy_medical;
class rhsusf_m113d_usarmy_medical;
class rhsusf_m1a1aimd_usarmy;
class rhsusf_m1a1aimwd_usarmy;
class RHS_M2A2;
class RHS_M2A2_wd;
class rhs_t72ba_tv;
class rhs_t72bd_tv;
class rhs_t80;
class rhsgref_BRDM2_msv;
class rhsgref_BRDM2_ATGM_vdv;
class rhsgref_ins_g_btr60;
class rhs_btr60_msv;
class rhs_btr70_msv;
class rhs_btr80a_vdv;
class rhs_bmp1_msv;
class rhs_bmp2_msv;
class rhs_bmp2e_msv;
class RHS_UAZ_MSV_01;
class rhsusf_m113_usarmy_M2_90;

// Add ammo to backpacks.
#include "configs/backpack_ammo.hpp"
	
// Include faction configs.
#include "configs/i_jemaah_islamiyah/units.hpp"
#include "configs/i_boko_haram/units.hpp"
#include "configs/i_somali_pirates/units.hpp"
#include "configs/i_somali_usc/units.hpp"
#include "configs/i_farc/units.hpp"
#include "configs/i_sierra_leone_ruf/units.hpp"
#include "configs/i_sierra_leone_army/units.hpp"
#include "configs/i_liberia_lurd/units.hpp"
#include "configs/i_liberia_army/units.hpp"
#include "configs/i_pmc_exec_decisions/units.hpp"
#include "configs/i_pmc_pp_solutions/units.hpp"
#include "configs/o_ussr_msv_cold_war/units.hpp"
#include "configs/o_russia_gru/units.hpp"
#include "configs/o_jemaah_islamiyah/units.hpp"
#include "configs/o_boko_haram/units.hpp"
#include "configs/o_somali_pirates/units.hpp"
#include "configs/o_somali_usc/units.hpp"
#include "configs/o_farc/units.hpp"
#include "configs/o_sierra_leone_ruf/units.hpp"
#include "configs/o_liberia_army/units.hpp"
#include "configs/o_pmc_exec_decisions/units.hpp"
#include "configs/o_pmc_pp_solutions/units.hpp"
#include "configs/o_dprk_army/units.hpp"
#include "configs/b_rok_army/units.hpp"
#include "configs/b_israel_defence_forces/units.hpp"
#include "configs/b_us_army_desert_storm/units.hpp"
#include "configs/b_us_army_just_cause/units.hpp"
#include "configs/b_us_army_battle_mog/units.hpp"
#include "configs/b_us_army_cold_war/units.hpp"
#include "configs/b_us_army_vietnam/units.hpp"
#include "configs/b_sierra_leone_army/units.hpp"
#include "configs/b_liberia_lurd/units.hpp"