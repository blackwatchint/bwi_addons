class BWI_Soldier_FARC_O_GEN_R : BWI_Soldier_FARC_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_AT: BWI_Soldier_FARC_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_AR: BWI_Soldier_FARC_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_DMR: BWI_Soldier_FARC_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_TL: BWI_Soldier_FARC_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_MAT: BWI_Soldier_FARC_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_MMG: BWI_Soldier_FARC_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_R2: BWI_Soldier_FARC_GEN_R2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_AT2: BWI_Soldier_FARC_GEN_AT2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_AR2: BWI_Soldier_FARC_GEN_AR2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_DMR2: BWI_Soldier_FARC_GEN_DMR2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_TL2: BWI_Soldier_FARC_GEN_TL2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_MAT2: BWI_Soldier_FARC_GEN_MAT2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};
class BWI_Soldier_FARC_O_GEN_MMG2: BWI_Soldier_FARC_GEN_MMG2 {
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
};


class BWI_Vehicle_FARC_O_Tech_M2: BWI_Vehicle_FARC_Tech_M2
{
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
	crew = "BWI_Soldier_FARC_O_GEN_R";
};
class BWI_Vehicle_FARC_O_Tech: BWI_Vehicle_FARC_Tech
{
	side = EAST;
	faction = "BWI_FACTION_FARC_O";
	crew = "BWI_Soldier_FARC_O_GEN_R";
};