class BWI_Flag_RSLAF {
	scope = 1;
	name = "Sierra Leone";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_sl.paa";
	texture = "\bwi_units\data\markers\mkr_sl.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};