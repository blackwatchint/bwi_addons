class BWI_FACTION_USA_VN
{
	name = "US Army (Vietnam War)";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_USA_VN
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_USA_VN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_USA_VN
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_USA_VN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_USA_VN
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_USA_VN";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_VN_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};