class BWI_Soldier_USA_VN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_USA_VN_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_VN";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F", 
		"Head_NATO", 
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhsgref_helmet_M1_bare",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsgref_helmet_M1_bare",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};

	uniformClass = "rhs_uniform_g3_rgr";
};


class BWI_Soldier_USA_VN_AT: BWI_Soldier_USA_VN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_M14",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_M14",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_T_M14",
		"hlc_20Rnd_762x51_T_M14",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_T_M14",
		"hlc_20Rnd_762x51_T_M14",
		"HandGrenade",
		"SmokeShell"
	};
};


class BWI_Soldier_USA_VN_AR: BWI_Soldier_USA_VN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_m60",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_m60",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_T_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_T_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_m60";
};


class BWI_Soldier_USA_VN_DMR: BWI_Soldier_USA_VN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_USA_VN_TL: BWI_Soldier_USA_VN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_Colt727_GL",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727_GL",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_USA_VN_CRW: BWI_Soldier_USA_VN_R {
	displayName = "Crew";
	weapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"HandGrenade",
		"SmokeShell"
	};
	linkedItems[] = {
		"rhs_tsh4",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_USA_VN_M113_W: rhsusf_m113_usarmy_M2_90 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_VN";
	crew = "BWI_Soldier_USA_VN_CRW";
	hiddenSelections[] = {"camo1", "camo3", "camo4", "camo5","camo6"};
};

class BWI_Vehicle_USA_VN_M113_M240_W: rhsusf_m113_usarmy_M240 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_VN";
	crew = "BWI_Soldier_USA_VN_CRW";
};

class BWI_Vehicle_USA_VN_M113_UNARM_W: rhsusf_m113_usarmy_unarmed {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_VN";
	crew = "BWI_Soldier_USA_VN_CRW";
};

class BWI_Vehicle_USA_VN_M113_MED_W: rhsusf_m113_usarmy_medical {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_VN";
	crew = "BWI_Soldier_USA_VN_CRW";
};