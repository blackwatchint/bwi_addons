class BWI_Soldier_IDF_OLI_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_IDF_OLI_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageGRE_F", 
		"Head_Greek",
		"Head_Euro",
		"NoGlasses"
	};
	weapons[] = {
		"arifle_TRG21_F_w_Aco",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_TRG21_F_w_Aco",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_IDF_OLI",
		"BWI_Vest_IDF_OLI",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_IDF_OLI",
		"BWI_Vest_IDF_OLI",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_IDF_OLI";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_Olive_Israel.paa"
	};
};


class BWI_Soldier_IDF_OLI_AT: BWI_Soldier_IDF_OLI_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"arifle_TRG21_F_w_Aco",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_TRG21_F_w_Aco",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_IDF_OLI_AR: BWI_Soldier_IDF_OLI_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249_pip_L_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_oli_f_m249";
};


class BWI_Soldier_IDF_OLI_DMR: BWI_Soldier_IDF_OLI_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"arifle_TRG21_F_w_MOS",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_TRG21_F_w_MOS",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_IDF_OLI_TL: BWI_Soldier_IDF_OLI_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"arifle_TRG21_GL_F_w_MRCO",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_TRG21_GL_F_w_MRCO",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_IDF_OLI_HAT: BWI_Soldier_IDF_OLI_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"arifle_TRG21_F_w_Aco",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_TRG21_F_w_Aco",
		"launch_I_Titan_short_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"SmokeShell",
	};
	backpack = "B_Carryall_oli_f_Titan";
};


class BWI_Soldier_IDF_OLI_MMG: BWI_Soldier_IDF_OLI_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m240B_w_ACOG3",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m240B_w_ACOG3",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_M240";
};


class BWI_Soldier_IDF_OLI_CRW: BWI_Soldier_IDF_OLI_R {
	displayName = "Crew";
	weapons[] = {
		"arifle_TRG20_F",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_TRG20_F",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"HandGrenade",
		"SmokeShell"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_IDF_OLI",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_IDF_OLI",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_IDF_MerkavaTUSK_D: B_MBT_01_TUSK_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_Merkava_D: B_MBT_01_cannon_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_tank";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_Sholef_D: B_MBT_01_arty_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_artillery";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_Seara_D: B_MBT_01_mlrs_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_artillery";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_Namer_D: B_APC_Tracked_01_rcws_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_apc";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_Bardelas_D: B_APC_Tracked_01_AA_F {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	vehicleClass = "rhs_vehclass_aa";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_M113_D: rhsusf_m113d_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	crew = "BWI_Soldier_IDF_OLI_CRW";
};

class BWI_Vehicle_IDF_M025_D: rhsusf_m1025_d {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	crew = "BWI_Soldier_IDF_OLI_R";
};

class BWI_Vehicle_IDF_M025_M2_D: rhsusf_m1025_d_m2 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_IDF";
	crew = "BWI_Soldier_IDF_OLI_R";
};