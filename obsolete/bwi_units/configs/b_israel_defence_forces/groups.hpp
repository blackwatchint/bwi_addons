class BWI_FACTION_IDF
{
	name = "Israel Defence Forces";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_IDF
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_IDF";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_IDF
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_IDF";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_IDF
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_IDF";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_HAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_IDF
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_IDF";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_IDF
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_IDF";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_IDF_OLI_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};