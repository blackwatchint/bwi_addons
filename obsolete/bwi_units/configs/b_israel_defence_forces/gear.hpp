class BWI_Uniform_IDF_OLI: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (Olive Drab - Israel)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_Olive_Israel.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_IDF_OLI_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_IDF_OLI: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (Olive Drab - Israel)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\N_Vests_Olive_Israel.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_IDF_OLI: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (Olive Drab - Israel)";
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Helmet_Olive_Israel.paa"
	};
};