class BWI_Soldier_SOMUSC_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_SOMUSC_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SOMUSC";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"H_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_1_F";
};


class BWI_Soldier_SOMUSC_GEN_AT: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_4";
};


class BWI_Soldier_SOMUSC_GEN_AR: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk";
	linkedItems[] = {
		"H_Bandanna_blu",
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_blu",
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_2_F";
	
};


class BWI_Soldier_SOMUSC_GEN_DMR: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_2";
};


class BWI_Soldier_SOMUSC_GEN_TL: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"H_Bandanna_cbr",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_cbr",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_3_F";
};


class BWI_Soldier_SOMUSC_GEN_MAT: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_5";
};


class BWI_Soldier_SOMUSC_GEN_MMG: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_cbr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_4_F";
};


class BWI_Soldier_SOMUSC_GEN_CRW: BWI_Soldier_SOMUSC_GEN_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
		linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch"
	};
	uniformClass = "rhs_chdkz_uniform_4";
};

class BWI_Soldier_SOMUSC_GEN_R2: BWI_Soldier_SOMUSC_GEN_R {
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_3";
};
class BWI_Soldier_SOMUSC_GEN_AT2: BWI_Soldier_SOMUSC_GEN_AT {
	uniformClass = "U_I_C_Soldier_Para_1_F";
};
class BWI_Soldier_SOMUSC_GEN_AR2: BWI_Soldier_SOMUSC_GEN_AR {
	linkedItems[] = {
		"H_Bandanna_sgg",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_sgg",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_4_F";
};
class BWI_Soldier_SOMUSC_GEN_DMR2: BWI_Soldier_SOMUSC_GEN_DMR {
	uniformClass = "rhs_chdkz_uniform_3";
};
class BWI_Soldier_SOMUSC_GEN_TL2: BWI_Soldier_SOMUSC_GEN_TL {
	linkedItems[] = {
		"H_Bandanna_khk",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_khk",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_5";
};
class BWI_Soldier_SOMUSC_GEN_MAT2: BWI_Soldier_SOMUSC_GEN_MAT {
	uniformClass = "rhs_chdkz_uniform_2";
};
class BWI_Soldier_SOMUSC_GEN_MMG2: BWI_Soldier_SOMUSC_GEN_MMG {
	uniformClass = "U_I_C_Soldier_Para_3_F";
};


class BWI_Vehicle_SOMUSC_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SOMUSC";
	crew = "BWI_Soldier_SOMUSC_GEN_R";
};

class BWI_Vehicle_SOMUSC_UAZ_DSHKM: rhs_uaz_dshkm_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SOMUSC";
	crew = "BWI_Soldier_SOMUSC_GEN_R";
};

class BWI_Vehicle_SOMUSC_UAZ_SPG9: rhs_uaz_spg9_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_SOMUSC";
	crew = "BWI_Soldier_SOMUSC_GEN_R";
};