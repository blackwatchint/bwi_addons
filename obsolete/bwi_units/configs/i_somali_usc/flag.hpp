class BWI_Flag_SOMUSC {
	scope = 1;
	name = "Somalia";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_som.paa";
	texture = "\bwi_units\data\markers\mkr_som.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};