class BWI_FACTION_SLRUF_O
{
	name = "Revolutionary United Front";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_SLRUF_O
		{
			name = "Squad";
			side = EAST;
			faction = "BWI_FACTION_SLRUF_O";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_AT";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_AR2";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_TL";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_AT2";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_TL2";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_DMR";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_SLRUF_O
		{
			name = "Team";
			side = EAST;
			faction = "BWI_FACTION_SLRUF_O";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_TL2";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_AT2";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R2";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_SLRUF_O
		{
			name = "Team (AT)";
			side = EAST;
			faction = "BWI_FACTION_SLRUF_O";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_MAT2";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R2";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_SLRUF_O
		{
			name = "Team (MG)";
			side = EAST;
			faction = "BWI_FACTION_SLRUF_O";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_MMG2";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R2";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_SLRUF_O
		{
			name = "Patrol";
			side = EAST;
			faction = "BWI_FACTION_SLRUF_O";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R2";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_SLRUF_O_GEN_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};