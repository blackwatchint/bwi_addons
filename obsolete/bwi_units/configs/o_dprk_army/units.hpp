class BWI_Soldier_DPRK_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_DPRK_R";
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageCHI_F",
		"Head_Asian",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_ak74m",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74m",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhs_googles_black",
		"BWI_Vest_DPRK_M81",
		"BWI_Helmet_DPRK_M81",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_googles_black",
		"BWI_Vest_DPRK_M81",
		"BWI_Helmet_DPRK_M81",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_DPRK_M81";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_M81_DPRK.paa"
	};
};


class BWI_Soldier_DPRK_AT: BWI_Soldier_DPRK_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_ak74m",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74m",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_DPRK_AR: BWI_Soldier_DPRK_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_CarryAll_oli_f_rpk74";
};


class BWI_Soldier_DPRK_DMR: BWI_Soldier_DPRK_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_DPRK_TL: BWI_Soldier_DPRK_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_ak74m_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74m_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_DPRK_MAT: BWI_Soldier_DPRK_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_ak74m",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74m",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_DPRK_MMG: BWI_Soldier_DPRK_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR"
	};
	backpack = "B_Carryall_oli_f_pkm";
};


class BWI_Soldier_DPRK_CRW: BWI_Soldier_DPRK_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_ak74m",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74m",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_DPRK_M81",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"BWI_Vest_DPRK_M81",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_DPRK_T72_W: rhs_t72ba_tv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_T72B3_W: rhs_t72bd_tv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_BRDM_W: rhsgref_BRDM2_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_BRDM_ATGM_W: rhsgref_BRDM2_ATGM_vdv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_BTR60_W: rhs_btr60_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_BTR80A_W: rhs_btr80a_vdv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_BMP1_W: rhs_bmp1_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_CRW";
};

class BWI_Vehicle_DPRK_UAZ_W: RHS_UAZ_MSV_01 {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_DPRK";
	crew = "BWI_Soldier_DPRK_R";
};