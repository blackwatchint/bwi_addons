class BWI_FACTION_DPRK
{
	name = "Korean People's Army";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_DPRK
		{
			name = "Squad";
			side = EAST;
			faction = "BWI_FACTION_DPRK";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_DPRK
		{
			name = "Team";
			side = EAST;
			faction = "BWI_FACTION_DPRK";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};

		class BWI_Group_TeamAT_DPRK
		{
			name = "Team (AT)";
			side = EAST;
			faction = "BWI_FACTION_DPRK";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_DPRK
		{
			name = "Team (MG)";
			side = EAST;
			faction = "BWI_FACTION_DPRK";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_DPRK
		{
			name = "Patrol";
			side = EAST;
			faction = "BWI_FACTION_DPRK";
			class Unit0
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = EAST;
				vehicle = "BWI_Soldier_DPRK_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};