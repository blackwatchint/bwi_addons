class BWI_Uniform_DPRK_M81: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (M81 - DPRK)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_M81_DPRK.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_DPRK_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_DPRK_M81: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (M81 - DPRK)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\N_Vests_M81_DPRK.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_DPRK_M81: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (M81 - DPRK)";
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Helmet_M81_DPRK.paa"
	};
};