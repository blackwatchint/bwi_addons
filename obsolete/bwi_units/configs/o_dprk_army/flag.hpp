class BWI_Flag_DPRK {
	scope = 1;
	name = "Democratic People's Republic of Korea";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_dprk.paa";
	texture = "\bwi_units\data\markers\mkr_dprk.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};