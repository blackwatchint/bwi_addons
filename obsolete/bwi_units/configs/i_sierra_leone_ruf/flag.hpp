class BWI_Flag_SLRUF {
	scope = 1;
	name = "Revolutionary United Front";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_ruf.paa";
	texture = "\bwi_units\data\markers\mkr_ruf.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};