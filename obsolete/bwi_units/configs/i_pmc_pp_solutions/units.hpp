class BWI_Soldier_PMCPPS_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_PMCPPS_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PMCPPS";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F",
		"Head_NATO", 
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_G36A1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_G36A1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"tacs_Cap_Backwards_BlackLogo",
		"G_Bandanna_shades",
		"tacs_Vest_PlateCarrier_Black",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"tacs_Cap_Backwards_BlackLogo",
		"G_Bandanna_shades",
		"tacs_Vest_PlateCarrier_Black",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "tacs_Uniform_Polo_TP_BS_LP_BB";
};


class BWI_Soldier_PMCPPS_GEN_AT: BWI_Soldier_PMCPPS_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_G36A1",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_G36A1",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
};


class BWI_Soldier_PMCPPS_GEN_AR: BWI_Soldier_PMCPPS_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"rhs_200rnd_556x45_M_SAW",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	backpack = "B_Carryall_oli_f_m249";
};


class BWI_Soldier_PMCPPS_GEN_DMR: BWI_Soldier_PMCPPS_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14"
	};
};


class BWI_Soldier_PMCPPS_GEN_TL: BWI_Soldier_PMCPPS_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_G36A1AG36",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_G36A1AG36",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
};


class BWI_Soldier_PMCPPS_GEN_MAT: BWI_Soldier_PMCPPS_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_G36A1",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_G36A1",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_EPR_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"hlc_30rnd_556x45_Tracers_G36",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_PMCPPS_GEN_MMG: BWI_Soldier_PMCPPS_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_m60",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_m60",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"rhs_mag_m67",
		"SmokeShellOrange"
	};
	backpack = "B_Carryall_oli_f_m60";
};


class BWI_Vehicle_PMCPPS_Tech_M2: I_G_Offroad_01_armed_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PMCPPS";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_PMCPPS_GEN_R";
};

class BWI_Vehicle_PMCPPS_Tech: I_G_Offroad_01_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PMCPPS";
	displayName = "Technical";
	crew = "BWI_Soldier_PMCPPS_GEN_R";
};