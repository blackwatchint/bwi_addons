class BWI_FACTION_PMCPPS
{
	name = "Pinpoint Solutions Intl.";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_PMCPPS
		{
			name = "Squad";
			side = RESISTANCE;
			faction = "BWI_FACTION_PMCPPS";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_AT";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_DMR";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_PMCPPS
		{
			name = "Team";
			side = RESISTANCE;
			faction = "BWI_FACTION_PMCPPS";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_PMCPPS
		{
			name = "Team (AT)";
			side = RESISTANCE;
			faction = "BWI_FACTION_PMCPPS";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_PMCPPS
		{
			name = "Team (MG)";
			side = RESISTANCE;
			faction = "BWI_FACTION_PMCPPS";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_PMCPPS
		{
			name = "Patrol";
			side = RESISTANCE;
			faction = "BWI_FACTION_PMCPPS";
			class Unit0
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = RESISTANCE;
				vehicle = "BWI_Soldier_PMCPPS_GEN_TL";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};