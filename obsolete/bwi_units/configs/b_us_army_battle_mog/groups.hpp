class BWI_FACTION_USA_BM
{
	name = "US Army (Battle of Mogadishu)";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_USA_BM
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_USA_BM";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_USA_BM
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_USA_BM";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_USA_BM
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_USA_BM";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_USA_BM
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_USA_BM";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_USA_BM
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_USA_BM";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_BM_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};