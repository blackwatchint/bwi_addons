class BWI_Uniform_USA_DCU: U_B_CombatUniform_mcam {
	scope = 2;
	displayName = "Combat Fatigues (DCU - USA)";
	model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_3CO_USA.paa"
	};
	class ItemInfo: ItemInfo {
		uniformmodel = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
		uniformClass = "BWI_Soldier_USA_BM_R";
		containerClass = "Supply40";
		mass = 40;
	};
};


class BWI_Vest_USA_DCU: V_PlateCarrier2_rgr {
	scope = 2;
	displayName = "Plate Carrier (DCU - USA)";
	model = "\A3\Characters_F\BLUFOR\equip_b_vest02";
	hiddenSelections[] = {
		"Camo",
		"insignia",
		"clan"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\N_Vests_3CO_USA.paa"
	};
	class ItemInfo: ItemInfo {
		uniformModel = "\A3\Characters_F\BLUFOR\equip_b_vest02";
		containerclass = "Supply140";
		mass = 80;
		hiddenSelections[] = {
			"Camo",
			"insignia",
			"clan"
		};
	};
};


class BWI_Helmet_USA_DCU: H_HelmetIA
{
	scope = 2;
	displayName = "MICH (DCU - USA)";
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Helmet_3CO_USA.paa"
	};
};