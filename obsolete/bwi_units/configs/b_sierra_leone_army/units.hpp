class BWI_Soldier_RSLAF_B_GEN_R : BWI_Soldier_RSLAF_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_AT: BWI_Soldier_RSLAF_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_AR: BWI_Soldier_RSLAF_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_DMR: BWI_Soldier_RSLAF_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_TL: BWI_Soldier_RSLAF_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_MAT: BWI_Soldier_RSLAF_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_MMG: BWI_Soldier_RSLAF_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};
class BWI_Soldier_RSLAF_B_GEN_CRW: BWI_Soldier_RSLAF_GEN_CRW {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
};


class BWI_Vehicle_RSLAF_B_BTR60: BWI_Vehicle_RSLAF_BTR60 {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
	crew = "BWI_Soldier_RSLAF_B_GEN_CRW";
};

class BWI_Vehicle_RSLAF_B_UAZ: BWI_Vehicle_RSLAF_UAZ {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
	crew = "BWI_Soldier_RSLAF_B_GEN_R";
};

class BWI_Vehicle_RSLAF_B_UAZ_DSHKM: BWI_Vehicle_RSLAF_UAZ_DSHKM {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
	crew = "BWI_Soldier_RSLAF_B_GEN_R";
};

class BWI_Vehicle_RSLAF_B_UAZ_SPG9: BWI_Vehicle_RSLAF_UAZ_SPG9 {
	side = WEST;
	faction = "BWI_FACTION_RSLAF_B";
	crew = "BWI_Soldier_RSLAF_B_GEN_AT";
};