class BWI_Flag_PMCED {
	scope = 1;
	name = "Executive Decisions Inc.";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_ed.paa";
	texture = "\bwi_units\data\markers\mkr_ed.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};