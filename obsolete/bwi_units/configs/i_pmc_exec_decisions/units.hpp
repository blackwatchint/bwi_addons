class BWI_Soldier_PMCED_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_PMCED_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PMCED";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F",
		"Head_NATO", 
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_L1A1SLR",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_L1A1SLR",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"H_Watchcap_blk",
		"G_Bandanna_aviator",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Watchcap_blk",
		"G_Bandanna_aviator",
		"V_TacChestrig_grn_F",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_ERDL";
};


class BWI_Soldier_PMCED_GEN_AT: BWI_Soldier_PMCED_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_L1A1SLR",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_L1A1SLR",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
};


class BWI_Soldier_PMCED_GEN_AR: BWI_Soldier_PMCED_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_LAR",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_LAR",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_oli_f_LAR";
};


class BWI_Soldier_PMCED_GEN_DMR: BWI_Soldier_PMCED_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14"
	};
};


class BWI_Soldier_PMCED_GEN_TL: BWI_Soldier_PMCED_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_L1A1SLR",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_L1A1SLR",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
};


class BWI_Soldier_PMCED_GEN_MAT: BWI_Soldier_PMCED_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_L1A1SLR",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_L1A1SLR",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"hlc_20Rnd_762x51_B_fal",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_PMCED_GEN_MMG: BWI_Soldier_PMCED_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_MG42",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_MG42",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_black",
		"rhs_mag_rdg2_black"
	};
	backpack = "B_Carryall_oli_f_mg42";
};


class BWI_Vehicle_PMCED_Tech_M2: I_G_Offroad_01_armed_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PMCED";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_PMCED_GEN_R";
};

class BWI_Vehicle_PMCED_Tech: I_G_Offroad_01_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PMCED";
	displayName = "Technical";
	crew = "BWI_Soldier_PMCED_GEN_R";
};