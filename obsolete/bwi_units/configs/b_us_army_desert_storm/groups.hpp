class BWI_FACTION_USA_DS
{
	name = "US Army (Gulf War)";
	class Infantry
	{
		name = "Infantry";
		class BWI_Group_Squad_USA_DS
		{
			name = "Squad";
			side = WEST;
			faction = "BWI_FACTION_USA_DS";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_TL";
				rank = "SERGEANT";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_TL";
				rank = "CORPORAL";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_TL";
				rank = "CORPORAL";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_AR";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
			class Unit4
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_AR";
				rank = "PRIVATE";
				position[] = {-10,-8,0};
			};
			class Unit5
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_AT";
				rank = "PRIVATE";
				position[] = {15,-12,0};
			};
			class Unit6
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_AT";
				rank = "PRIVATE";
				position[] = {-15,-12,0};
			};
			class Unit7
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_DMR";
				rank = "PRIVATE";
				position[] = {20,-16,0};
			};
			class Unit8
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {-20,-16,0};
			};
		};
		
		class BWI_Group_Team_USA_DS
		{
			name = "Team";
			side = WEST;
			faction = "BWI_FACTION_USA_DS";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_AR";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_AT";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamAT_USA_DS
		{
			name = "Team (AT)";
			side = WEST;
			faction = "BWI_FACTION_USA_DS";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_MAT";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_TeamMG_USA_DS
		{
			name = "Team (MG)";
			side = WEST;
			faction = "BWI_FACTION_USA_DS";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_TL";
				rank = "CORPORAL";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_MMG";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
			class Unit2
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {-5,-4,0};
			};
			class Unit3
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {10,-8,0};
			};
		};
		
		class BWI_Group_Patrol_USA_DS
		{
			name = "Patrol";
			side = WEST;
			faction = "BWI_FACTION_USA_DS";
			class Unit0
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {0,0,0};
			};
			class Unit1
			{
				side = WEST;
				vehicle = "BWI_Soldier_USA_DS_R";
				rank = "PRIVATE";
				position[] = {5,-4,0};
			};
		};
	};
};