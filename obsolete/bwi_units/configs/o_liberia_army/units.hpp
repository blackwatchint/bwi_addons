class BWI_Soldier_AFL_O_GEN_R : BWI_Soldier_AFL_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_AT: BWI_Soldier_AFL_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_AR: BWI_Soldier_AFL_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_DMR: BWI_Soldier_AFL_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_TL: BWI_Soldier_AFL_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_MAT: BWI_Soldier_AFL_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_MMG: BWI_Soldier_AFL_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};
class BWI_Soldier_AFL_O_GEN_CRW: BWI_Soldier_AFL_GEN_CRW {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
};


class BWI_Vehicle_AFL_O_BTR60: BWI_Vehicle_AFL_BTR60 {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
	crew = "BWI_Soldier_AFL_O_GEN_CRW";
};

class BWI_Vehicle_AFL_O_UAZ: BWI_Vehicle_AFL_UAZ {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
	crew = "BWI_Soldier_AFL_O_GEN_R";
};

class BWI_Vehicle_AFL_O_UAZ_DSHKM: BWI_Vehicle_AFL_UAZ_DSHKM {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
	crew = "BWI_Soldier_AFL_O_GEN_R";
};

class BWI_Vehicle_AFL_O_UAZ_SPG9: BWI_Vehicle_AFL_UAZ_SPG9 {
	side = EAST;
	faction = "BWI_FACTION_AFL_O";
	crew = "BWI_Soldier_AFL_O_GEN_AT";
};