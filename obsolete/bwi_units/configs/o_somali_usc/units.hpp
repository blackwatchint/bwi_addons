class BWI_Soldier_SOMUSC_O_GEN_R : BWI_Soldier_SOMUSC_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_AT: BWI_Soldier_SOMUSC_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_AR: BWI_Soldier_SOMUSC_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_DMR: BWI_Soldier_SOMUSC_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_TL: BWI_Soldier_SOMUSC_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_MAT: BWI_Soldier_SOMUSC_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_MMG: BWI_Soldier_SOMUSC_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_CRW: BWI_Soldier_SOMUSC_GEN_CRW {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_R2: BWI_Soldier_SOMUSC_GEN_R2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_AT2: BWI_Soldier_SOMUSC_GEN_AT2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_AR2: BWI_Soldier_SOMUSC_GEN_AR2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_DMR2: BWI_Soldier_SOMUSC_GEN_DMR2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_TL2: BWI_Soldier_SOMUSC_GEN_TL2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_MAT2: BWI_Soldier_SOMUSC_GEN_MAT2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};
class BWI_Soldier_SOMUSC_O_GEN_MMG2: BWI_Soldier_SOMUSC_GEN_MMG2 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
};


class BWI_Vehicle_SOMUSC_O_UAZ: BWI_Vehicle_SOMUSC_UAZ {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
	crew = "BWI_Soldier_SOMUSC_O_GEN_R";
};

class BWI_Vehicle_SOMUSC_O_UAZ_DSHKM: BWI_Vehicle_SOMUSC_UAZ_DSHKM {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
	crew = "BWI_Soldier_SOMUSC_O_GEN_R";
};

class BWI_Vehicle_SOMUSC_O_UAZ_SPG9: BWI_Vehicle_SOMUSC_UAZ_SPG9 {
	side = EAST;
	faction = "BWI_FACTION_SOMUSC_O";
	crew = "BWI_Soldier_SOMUSC_O_GEN_R";
};