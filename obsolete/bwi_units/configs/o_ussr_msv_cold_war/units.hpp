class BWI_Soldier_USSR_CW_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_USSR_CW_R";
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageRUS",
		"Head_Russian",
		"Head_Euro",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_ak74",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhsgref_ssh68_ttsko_forest",
		"rhsgref_6b23_ttsko_forest_rifleman",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsgref_ssh68_ttsko_forest",
		"rhsgref_6b23_ttsko_forest_rifleman",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_ttsko_forest";
};


class BWI_Soldier_USSR_CW_AT: BWI_Soldier_USSR_CW_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_ak74",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_USSR_CW_AR: BWI_Soldier_USSR_CW_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_CarryAll_oli_f_rpk74";
};


class BWI_Soldier_USSR_CW_DMR: BWI_Soldier_USSR_CW_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_USSR_CW_TL: BWI_Soldier_USSR_CW_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_ak74_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_USSR_CW_MAT: BWI_Soldier_USSR_CW_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_ak74",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_USSR_CW_MMG: BWI_Soldier_USSR_CW_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR"
	};
	backpack = "B_Carryall_oli_f_pkm";
};


class BWI_Soldier_USSR_CW_CRW: BWI_Soldier_USSR_CW_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_ak74",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_ak74",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"rhs_tsh4",
		"rhsgref_6b23_ttsko_forest_rifleman",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"rhsgref_6b23_ttsko_forest_rifleman",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_USSR_CW_T72_W: rhs_t72ba_tv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_T80_W: rhs_t80 {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_BRDM_W: rhsgref_BRDM2_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_BTR60_W: rhs_btr60_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_BTR70_W: rhs_btr70_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_BMP1_W: rhs_bmp1_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_BMP2_W: rhs_bmp2_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_BMP2E_W: rhs_bmp2e_msv {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_CRW";
};

class BWI_Vehicle_USSR_CW_UAZ_W: RHS_UAZ_MSV_01 {
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_USSR_CW";
	crew = "BWI_Soldier_USSR_CW_R";
};