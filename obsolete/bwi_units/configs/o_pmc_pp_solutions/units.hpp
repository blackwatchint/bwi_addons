class BWI_Soldier_PMCPPS_O_GEN_R : BWI_Soldier_PMCPPS_GEN_R {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};
class BWI_Soldier_PMCPPS_O_GEN_AT: BWI_Soldier_PMCPPS_GEN_AT {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};
class BWI_Soldier_PMCPPS_O_GEN_AR: BWI_Soldier_PMCPPS_GEN_AR {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};
class BWI_Soldier_PMCPPS_O_GEN_DMR: BWI_Soldier_PMCPPS_GEN_DMR {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};
class BWI_Soldier_PMCPPS_O_GEN_TL: BWI_Soldier_PMCPPS_GEN_TL {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};
class BWI_Soldier_PMCPPS_O_GEN_MAT: BWI_Soldier_PMCPPS_GEN_MAT {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};
class BWI_Soldier_PMCPPS_O_GEN_MMG: BWI_Soldier_PMCPPS_GEN_MMG {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
};


class BWI_Vehicle_PMCPPS_O_Tech_M2: BWI_Vehicle_PMCPPS_Tech_M2 {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
	crew = "BWI_Soldier_PMCPPS_O_GEN_R";
};

class BWI_Vehicle_PMCPPS_O_Tech: BWI_Vehicle_PMCPPS_Tech {
	side = EAST;
	faction = "BWI_FACTION_PMCPPS_O";
	crew = "BWI_Soldier_PMCPPS_O_GEN_R";
};