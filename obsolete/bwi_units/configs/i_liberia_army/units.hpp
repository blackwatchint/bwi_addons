class BWI_Soldier_AFL_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_AFL_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFL";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhs_6b7_1m_olive",
		"rhs_vydra_3m",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_6b7_1m_olive",
		"rhs_vydra_3m",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_alpenflage";
};


class BWI_Soldier_AFL_GEN_AT: BWI_Soldier_AFL_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_AFL_GEN_AR: BWI_Soldier_AFL_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk";
};


class BWI_Soldier_AFL_GEN_DMR: BWI_Soldier_AFL_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1", 
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_AFL_GEN_TL: BWI_Soldier_AFL_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_AFL_GEN_MAT: BWI_Soldier_AFL_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpg7";
};


class BWI_Soldier_AFL_GEN_MMG: BWI_Soldier_AFL_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
};

class BWI_Soldier_AFL_GEN_CRW: BWI_Soldier_AFL_GEN_R {
	displayName = "Crew";
	linkedItems[] = {
		"rhs_tsh4",
		"rhs_vydra_3m",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_tsh4",
		"rhs_vydra_3m",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_AFL_BTR60: rhsgref_ins_g_btr60 {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFL";
	crew = "BWI_Soldier_AFL_GEN_CRW";
};

class BWI_Vehicle_AFL_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFL";
	crew = "BWI_Soldier_AFL_GEN_R";
};

class BWI_Vehicle_AFL_UAZ_DSHKM: rhs_uaz_dshkm_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFL";
	crew = "BWI_Soldier_AFL_GEN_R";
};

class BWI_Vehicle_AFL_UAZ_SPG9: rhs_uaz_spg9_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFL";
	crew = "BWI_Soldier_AFL_GEN_AT";
};