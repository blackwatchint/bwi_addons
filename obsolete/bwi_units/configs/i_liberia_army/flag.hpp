class BWI_Flag_AFL {
	scope = 1;
	name = "Liberia";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_lib.paa";
	texture = "\bwi_units\data\markers\mkr_lib.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};