class BWI_Flag_LURD {
	scope = 1;
	name = "Liberians United for Reconciliation and Democracy";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_lurd.paa";
	texture = "\bwi_units\data\markers\mkr_lurd.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};