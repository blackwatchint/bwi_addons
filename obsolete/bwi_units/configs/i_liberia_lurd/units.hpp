class BWI_Soldier_LURD_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_LURD_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_LURD";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_4_F";
};


class BWI_Soldier_LURD_GEN_AT: BWI_Soldier_LURD_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	linkedItems[] = {
		"H_Beret_blk",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Beret_blk",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_C_Man_casual_6_F";
};


class BWI_Soldier_LURD_GEN_AR: BWI_Soldier_LURD_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"hlc_45Rnd_762x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk";
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_2_F";
};


class BWI_Soldier_LURD_GEN_DMR: BWI_Soldier_LURD_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1", 
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"H_Cap_red",
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Cap_red",
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_3_F";
};


class BWI_Soldier_LURD_GEN_TL: BWI_Soldier_LURD_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ak47",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak"
	};
	linkedItems[] = {
		"H_Cap_blu",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Cap_blu",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_C_Man_casual_4_F";
};


class BWI_Soldier_LURD_GEN_MAT: BWI_Soldier_LURD_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_ak47",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ak47",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak"
	};
	respawnMagazines[] = {
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak",
		"hlc_30Rnd_762x39_t_ak"
	};
	linkedItems[] = {
		"H_Cap_oli",
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Cap_oli",
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	uniformClass = "U_I_C_Soldier_Bandit_1_F";
};


class BWI_Soldier_LURD_GEN_MMG: BWI_Soldier_LURD_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_2_F";
};


class BWI_Soldier_LURD_GEN_R2: BWI_Soldier_LURD_GEN_R {
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_3_F";
};
class BWI_Soldier_LURD_GEN_AT2: BWI_Soldier_LURD_GEN_AT {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_C_Man_casual_5_F";
};
class BWI_Soldier_LURD_GEN_AR2: BWI_Soldier_LURD_GEN_AR {
	linkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_5_F";
};
class BWI_Soldier_LURD_GEN_DMR2: BWI_Soldier_LURD_GEN_DMR {
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_3_F";
};
class BWI_Soldier_LURD_GEN_TL2: BWI_Soldier_LURD_GEN_TL {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_2_F";
};
class BWI_Soldier_LURD_GEN_MAT2: BWI_Soldier_LURD_GEN_MAT {
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Bandit_4_F";
};
class BWI_Soldier_LURD_GEN_MMG2: BWI_Soldier_LURD_GEN_MMG {
	linkedItems[] = {
		"H_Cap_red",
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Cap_red",
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_1_F";
};


class BWI_Vehicle_LURD_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_LURD";
	crew = "BWI_Soldier_LURD_GEN_R2";
};

class BWI_Vehicle_LURD_UAZ_DSHKM: rhs_uaz_dshkm_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_LURD";
	crew = "BWI_Soldier_LURD_GEN_R";
};

class BWI_Vehicle_LURD_UAZ_SPG9: rhs_uaz_spg9_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_LURD";
	crew = "BWI_Soldier_LURD_GEN_AT2";
};

class BWI_Vehicle_LURD_Tech_M2: I_G_Offroad_01_armed_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_LURD";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_LURD_GEN_R";
};

class BWI_Vehicle_LURD_Tech: I_G_Offroad_01_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_LURD";
	displayName = "Technical";
	crew = "BWI_Soldier_LURD_GEN_R2";
};