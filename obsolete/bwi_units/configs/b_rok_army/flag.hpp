class BWI_Flag_ROK {
	scope = 1;
	name = "Republic of Korea";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_rok.paa";
	texture = "\bwi_units\data\markers\mkr_rok.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};