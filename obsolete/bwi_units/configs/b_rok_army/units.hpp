class BWI_Soldier_ROK_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_ROK_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageCHI_F",
		"Head_Asian",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_ROK_GRN",
		"BWI_Vest_ROK_GRN",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_ROK_GRN",
		"BWI_Vest_ROK_GRN",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_ROK_GRN";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_Granite_ROK.paa"
	};
};


class BWI_Soldier_ROK_AT: BWI_Soldier_ROK_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"rhs_weap_m72a7",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_ROK_AR: BWI_Soldier_ROK_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_oli_f_m249";
};


class BWI_Soldier_ROK_DMR: BWI_Soldier_ROK_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_ACR_full_black_w_ACOG",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ACR_full_black_w_ACOG",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_ROK_TL: BWI_Soldier_ROK_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_ROK_MAT: BWI_Soldier_ROK_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"rhs_weap_smaw",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"rhs_weap_smaw",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"SmokeShell",
	};
	backpack = "B_Carryall_oli_f_smaw";
};


class BWI_Soldier_ROK_MMG: BWI_Soldier_ROK_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"hlc_lmg_m60",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_lmg_m60",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_100Rnd_762x51_M_M60E4",
		"hlc_100Rnd_762x51_M_M60E4",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_m60";
};


class BWI_Soldier_ROK_CRW: BWI_Soldier_ROK_R {
	displayName = "Crew";
	weapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_ACR_Carb_black",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"HandGrenade",
		"SmokeShell"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_ROK_GRN",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_ROK_GRN",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_ROK_M1A1_W: rhsusf_m1a1aimwd_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_CRW";
};

class BWI_Vehicle_ROK_M2A2_W: RHS_M2A2_wd {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_CRW";
};

class BWI_Vehicle_ROK_M113_W: rhsusf_m113_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_CRW";
};

class BWI_Vehicle_ROK_M113_M240_W: rhsusf_m113_usarmy_M240 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_CRW";
};

class BWI_Vehicle_ROK_M113_UNARM_W: rhsusf_m113_usarmy_unarmed {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_CRW";
};

class BWI_Vehicle_ROK_M113_MED_W: rhsusf_m113_usarmy_medical {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_CRW";
};

class BWI_Vehicle_ROK_M025_W: rhsusf_m1025_w {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_R";
};

class BWI_Vehicle_ROK_M025_M2_W: rhsusf_m1025_w_m2 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_ROK";
	crew = "BWI_Soldier_ROK_R";
};