class BWI_Soldier_USA_JC_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_USA_JC_R";
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageENG_F", 
		"Head_NATO", 
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"BWI_Helmet_USA_BDU",
		"BWI_Vest_USA_BDU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"BWI_Helmet_USA_BDU",
		"BWI_Vest_USA_BDU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F_beta\indep\ia_soldier_01.p3d";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_USA_BDU";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"\bwi_units\data\I_Clothing_M81_USA.paa"
	};
};


class BWI_Soldier_USA_JC_AT: BWI_Soldier_USA_JC_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_M136_hedp",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_M136_hedp",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_USA_JC_AR: BWI_Soldier_USA_JC_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m249",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhs_200rnd_556x45_M_SAW",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_CarryAll_oli_f_m249";
};


class BWI_Soldier_USA_JC_DMR: BWI_Soldier_USA_JC_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_M21_Rail_w_optic_KHS_old",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"hlc_20Rnd_762x51_B_M14",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_USA_JC_TL: BWI_Soldier_USA_JC_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"hlc_rifle_Colt727_GL",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727_GL",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
};


class BWI_Soldier_USA_JC_MAT: BWI_Soldier_USA_JC_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_smaw",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"rhs_weap_smaw",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"hlc_30rnd_556x45_SPR",
		"HandGrenade",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_smaw";
};


class BWI_Soldier_USA_JC_MMG: BWI_Soldier_USA_JC_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_m240B",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m240B",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"rhsusf_100Rnd_762x51",
		"rhsusf_100Rnd_762x51_m62_tracer",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_oli_f_M240";
};


class BWI_Soldier_USA_JC_CRW: BWI_Soldier_USA_JC_R {
	displayName = "Crew";
	weapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_Colt727",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"HandGrenade",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"hlc_30rnd_556x45_EPR",
		"HandGrenade",
		"SmokeShell"
	};
	linkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_USA_BDU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhsusf_cvc_green_helmet",
		"BWI_Vest_USA_BDU",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
};


class BWI_Vehicle_USA_JC_M1A1_W: rhsusf_m1a1aimwd_usarmy {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_CRW";
};

class BWI_Vehicle_USA_JC_M2A2_W: RHS_M2A2_wd {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_CRW";
};

class BWI_Vehicle_USA_JC_M113_W: rhsusf_m113_usarmy_M2_90 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_CRW";
};

class BWI_Vehicle_USA_JC_M113_M240_W: rhsusf_m113_usarmy_M240 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_CRW";
};

class BWI_Vehicle_USA_JC_M113_UNARM_W: rhsusf_m113_usarmy_unarmed {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_CRW";
};

class BWI_Vehicle_USA_JC_M113_MED_W: rhsusf_m113_usarmy_medical {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_CRW";
};

class BWI_Vehicle_USA_JC_M025_W: rhsusf_m1025_w {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_R";
};

class BWI_Vehicle_USA_JC_M025_M2_W: rhsusf_m1025_w_m2 {
	scope = 2;
	side = WEST;
	faction = "BWI_FACTION_USA_JC";
	crew = "BWI_Soldier_USA_JC_R";
};