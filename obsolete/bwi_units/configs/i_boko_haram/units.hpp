class BWI_Soldier_AFRJIH_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_AFRJIH_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageGRE_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"G_Balaclava_blk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"G_Balaclava_blk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_5";
};


class BWI_Soldier_AFRJIH_GEN_AT: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	uniformClass = "rhs_chdkz_uniform_4";
};


class BWI_Soldier_AFRJIH_GEN_AR: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk74";
	uniformClass = "rhs_chdkz_uniform_4";
	
};


class BWI_Soldier_AFRJIH_GEN_DMR: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhs_chdkz_uniform_2";
};


class BWI_Soldier_AFRJIH_GEN_TL: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhs_chdkz_uniform_2";
};


class BWI_Soldier_AFRJIH_GEN_MAT: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	uniformClass = "rhs_chdkz_uniform_5";
};


class BWI_Soldier_AFRJIH_GEN_MMG: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
	uniformClass = "rhs_chdkz_uniform_4";
};


class BWI_Soldier_AFRJIH_GEN_CRW: BWI_Soldier_AFRJIH_GEN_R {
	displayName = "Crew";
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	uniformClass = "rhs_chdkz_uniform_4";
};

class BWI_Soldier_AFRJIH_GEN_R2: BWI_Soldier_AFRJIH_GEN_R {
	uniformClass = "rhs_chdkz_uniform_3";
};
class BWI_Soldier_AFRJIH_GEN_AT2: BWI_Soldier_AFRJIH_GEN_AT {
	uniformClass = "rhs_chdkz_uniform_5";
};
class BWI_Soldier_AFRJIH_GEN_AR2: BWI_Soldier_AFRJIH_GEN_AR {
	uniformClass = "rhs_chdkz_uniform_3";
};
class BWI_Soldier_AFRJIH_GEN_DMR2: BWI_Soldier_AFRJIH_GEN_DMR {
	uniformClass = "rhs_chdkz_uniform_3";
};
class BWI_Soldier_AFRJIH_GEN_TL2: BWI_Soldier_AFRJIH_GEN_TL {
	uniformClass = "rhs_chdkz_uniform_5";
};
class BWI_Soldier_AFRJIH_GEN_MAT2: BWI_Soldier_AFRJIH_GEN_MAT {
	uniformClass = "rhs_chdkz_uniform_2";
};
class BWI_Soldier_AFRJIH_GEN_MMG2: BWI_Soldier_AFRJIH_GEN_MMG {
	uniformClass = "rhs_chdkz_uniform_2";
};


class BWI_Vehicle_AFRJIH_BMD2: rhs_bmd2_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	crew = "BWI_Soldier_AFRJIH_GEN_CRW";
};

class BWI_Vehicle_AFRJIH_BMD1: rhs_bmd1_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	crew = "BWI_Soldier_AFRJIH_GEN_CRW";
};

class BWI_Vehicle_AFRJIH_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	crew = "BWI_Soldier_AFRJIH_GEN_R";
};

class BWI_Vehicle_AFRJIH_UAZ_DSHKM: rhs_uaz_dshkm_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	crew = "BWI_Soldier_AFRJIH_GEN_R";
};

class BWI_Vehicle_AFRJIH_UAZ_SPG9: rhs_uaz_spg9_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	crew = "BWI_Soldier_AFRJIH_GEN_R";
};

class BWI_Vehicle_AFRJIH_Tech_M2: I_G_Offroad_01_armed_F
{
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_AFRJIH_GEN_R";
};

class BWI_Vehicle_AFRJIH_Tech: I_G_Offroad_01_F
{
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_AFRJIH";
	displayName = "Technical";
	crew = "BWI_Soldier_AFRJIH_GEN_R";
};