class BWI_FACTION_FARC {
	displayName = "Revolutionary Armed Forces of Colombia";
	flag = "\bwi_units\data\flags\flag_farc.paa";
	icon = "\bwi_units\data\icon\icon_farc.paa";
	priority = 1;
	side = RESISTANCE; 
};