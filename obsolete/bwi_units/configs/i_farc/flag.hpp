class BWI_Flag_FARC {
	scope = 1;
	name = "Revolutionary Armed Forces of Colombia";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_farc.paa";
	texture = "\bwi_units\data\markers\mkr_farc.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};