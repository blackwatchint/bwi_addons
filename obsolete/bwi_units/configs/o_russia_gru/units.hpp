class BWI_Soldier_RUS_GRU_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_RUS_GRU_R";
	scope = 2;
	side = EAST;
	faction = "BWI_FACTION_RUS_GRU";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageRUS",
		"Head_Russian",
		"Head_Euro",
		"NoGlasses"
	};
	weapons[] = {
		"arifle_AK12_F_GRU",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_AK12_F_GRU",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"rhs_6b23_digi_6sh92_Vog_Radio_Spetsnaz",
		"rhs_altyn_novisor_ess_bala",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"rhs_6b23_digi_6sh92_Vog_Radio_Spetsnaz",
		"rhs_altyn_novisor_ess_bala",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_uniform_emr_patchless";
};


class BWI_Soldier_RUS_GRU_AT: BWI_Soldier_RUS_GRU_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"arifle_AK12_F_GRU",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_AK12_F_GRU",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
};


class BWI_Soldier_RUS_GRU_AR: BWI_Soldier_RUS_GRU_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_RPK12",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_RPK12",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpk12";
};


class BWI_Soldier_RUS_GRU_DMR: BWI_Soldier_RUS_GRU_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_wd_npz_w_LEUPOLDMK4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_RUS_GRU_TL: BWI_Soldier_RUS_GRU_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"arifle_AK12_GL_F_GRU",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_AK12_GL_F_GRU",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_VOG25P",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
};


class BWI_Soldier_RUS_GRU_MAT: BWI_Soldier_RUS_GRU_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"arifle_AK12_F_GRU",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_AK12_F_GRU",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_30Rnd_545x39_AK",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_oli_f_rpg7";
};


class BWI_Soldier_RUS_GRU_MMG: BWI_Soldier_RUS_GRU_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkp",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkp",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR"
	};
	backpack = "B_Carryall_oli_f_pkm";
};