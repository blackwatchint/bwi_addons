class rhsgref_uniform_woodland;
class BWI_Uniform_USA_M81: rhsgref_uniform_woodland {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_USA_CW_R";
		containerClass = "Supply40";
		mass = 40;
	};
};