class B_Carryall_khk;
class B_Carryall_khk_f_rpg7: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_rpg7_PG7VL_mag, 2);
		MACRO_ADDMAGAZINE(rhs_rpg7_OG7V_mag, 2);
	};
};

class B_Carryall_khk_f_rpk: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_762x39_t_rpk, 18);
	};
};

class B_Carryall_khk_f_rpk74: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_545x39_t_rpk, 18);
	};
};

class B_Carryall_khk_f_pkm: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR, 4);
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR_green, 2);
	};
};

class B_CarryAll_khk_f_m249: B_CarryAll_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_200rnd_556x45_M_SAW, 4);
		MACRO_ADDMAGAZINE(rhs_200rnd_556x45_T_SAW, 2);
	};
};

class B_CarryAll_khk_f_m60: B_CarryAll_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_762x51_M_M60E4, 6);
		MACRO_ADDMAGAZINE(hlc_100Rnd_762x51_T_M60E4, 4);
	};
};

class B_Carryall_khk_f_mg42: B_Carryall_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_792x57_B_MG42, 4);
	};
};

class B_CarryAll_khk_f_smaw: B_CarryAll_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_mag_smaw_HEAA, 1);
		MACRO_ADDMAGAZINE(rhs_mag_smaw_HEDP, 1);
	};
};

class B_CarryAll_khk_f_maaws: B_CarryAll_khk {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HEAT, 1);
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HEDP, 1);
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HE, 1);
	};
};

class B_CarryAll_oli;
class B_CarryAll_oli_f_m249: B_CarryAll_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_200rnd_556x45_M_SAW, 4);
		MACRO_ADDMAGAZINE(rhs_200rnd_556x45_T_SAW, 2);
	};
};

class B_CarryAll_oli_f_LAR: B_CarryAll_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_50rnd_762x51_M_FAL, 8);
	};
};

class B_Carryall_oli_f_m240: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhsusf_100Rnd_762x51, 6);
		MACRO_ADDMAGAZINE(rhsusf_100Rnd_762x51_m62_tracer, 4);
	};
};

class B_Carryall_oli_f_m60: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_762x51_M_M60E4, 6);
		MACRO_ADDMAGAZINE(hlc_100Rnd_762x51_T_M60E4, 4);
	};
};

class B_Carryall_oli_f_mg3: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_762x51_Barrier_MG3, 4);
	};
};

class B_Carryall_oli_f_rpg7: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_rpg7_PG7VL_mag, 2);
		MACRO_ADDMAGAZINE(rhs_rpg7_OG7V_mag, 2);
	};
};

class B_CarryAll_oli_f_maaws: B_CarryAll_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HEAT, 1);
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HEDP, 1);
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HE, 1);
	};
};

class B_Carryall_oli_f_Titan: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(Titan_AT, 2);
	};
};

class B_Carryall_oli_f_rpk: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_762x39_t_rpk, 18);
	};
};

class B_Carryall_oli_f_rpk74: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_545x39_t_rpk, 18);
	};
};

class B_Carryall_oli_f_rpk12: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_60Rnd_545x39_t_rpk, 18);
	};
};

class B_Carryall_oli_f_pkm: B_CarryAll_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR, 4);
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR_green, 2);
	};
};

class B_Carryall_oli_f_mg42: B_Carryall_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_792x57_B_MG42, 4);
	};
};

class B_CarryAll_oli_f_smaw: B_CarryAll_oli {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_mag_smaw_HEAA, 1);
		MACRO_ADDMAGAZINE(rhs_mag_smaw_HEDP, 1);
	};
};



class B_Carryall_cbr;
class B_Carryall_cbr_f_rpg7: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_rpg7_PG7VL_mag, 2);
		MACRO_ADDMAGAZINE(rhs_rpg7_OG7V_mag, 2);
	};
};

class B_Carryall_cbr_f_Titan: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(Titan_AT, 2);
	};
};

class B_Carryall_cbr_f_Javelin: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_fgm148_magazine_AT, 1);
	};
};

class B_Carryall_cbr_f_rpk: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_762x39_t_rpk, 18);
	};
};

class B_Carryall_cbr_f_rpk74: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_45Rnd_545x39_t_rpk, 18);
	};
};

class B_Carryall_cbr_f_pkm: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR, 4);
		MACRO_ADDMAGAZINE(rhs_100Rnd_762x54mmR_green, 2);
	};
};

class B_CarryAll_cbr_f_m249: B_CarryAll_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_200rnd_556x45_M_SAW, 4);
		MACRO_ADDMAGAZINE(rhs_200rnd_556x45_T_SAW, 2);
	};
};

class B_CarryAll_cbr_f_smaw: B_CarryAll_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_mag_smaw_HEAA, 1);
		MACRO_ADDMAGAZINE(rhs_mag_smaw_HEDP, 1);
	};
};

class B_CarryAll_cbr_f_maaws: B_CarryAll_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HEAT, 1);
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HEDP, 1);
		MACRO_ADDMAGAZINE(rhs_mag_maaws_HE, 1);
	};
};

class B_Carryall_cbr_f_m240: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(rhsusf_100Rnd_762x51, 6);
		MACRO_ADDMAGAZINE(rhsusf_100Rnd_762x51_m62_tracer, 4);
	};
};

class B_Carryall_cbr_f_mg3: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_762x51_Barrier_MG3, 4);
	};
};

class B_Carryall_cbr_f_mg42: B_Carryall_cbr {
	scope = 1;
	class TransportMagazines {
		MACRO_ADDMAGAZINE(hlc_100Rnd_792x57_B_MG42, 4);
	};
};