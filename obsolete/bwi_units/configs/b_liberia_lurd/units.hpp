class BWI_Soldier_LURD_B_GEN_R : BWI_Soldier_LURD_GEN_R {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_AT: BWI_Soldier_LURD_GEN_AT {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_AR: BWI_Soldier_LURD_GEN_AR {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_DMR: BWI_Soldier_LURD_GEN_DMR {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_TL: BWI_Soldier_LURD_GEN_TL {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_MAT: BWI_Soldier_LURD_GEN_MAT {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_MMG: BWI_Soldier_LURD_GEN_MMG {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_R2: BWI_Soldier_LURD_GEN_R2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_AT2: BWI_Soldier_LURD_GEN_AT2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_AR2: BWI_Soldier_LURD_GEN_AR2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_DMR2: BWI_Soldier_LURD_GEN_DMR2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_TL2: BWI_Soldier_LURD_GEN_TL2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_MAT2: BWI_Soldier_LURD_GEN_MAT2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};
class BWI_Soldier_LURD_B_GEN_MMG2: BWI_Soldier_LURD_GEN_MMG2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
};


class BWI_Vehicle_LURD_B_UAZ: BWI_Vehicle_LURD_UAZ {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
	crew = "BWI_Soldier_LURD_B_GEN_R2";
};

class BWI_Vehicle_LURD_B_UAZ_DSHKM: BWI_Vehicle_LURD_UAZ_DSHKM {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
	crew = "BWI_Soldier_LURD_B_GEN_R";
};

class BWI_Vehicle_LURD_B_UAZ_SPG9: BWI_Vehicle_LURD_UAZ_SPG9 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
	crew = "BWI_Soldier_LURD_B_GEN_AT2";
};

class BWI_Vehicle_LURD_B_Tech_M2: BWI_Vehicle_LURD_Tech_M2 {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
	crew = "BWI_Soldier_LURD_B_GEN_R";
};

class BWI_Vehicle_LURD_B_Tech: BWI_Vehicle_LURD_Tech {
	side = WEST;
	faction = "BWI_FACTION_LURD_B";
	crew = "BWI_Soldier_LURD_B_GEN_R2";
};