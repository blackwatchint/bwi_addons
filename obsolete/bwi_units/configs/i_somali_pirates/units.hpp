class BWI_Soldier_PIR_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_PIR_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguagePER_F",
		"Head_African",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	model = "\A3\Characters_F\Civil\c_poloshirt";
	nakedUniform = "U_BasicBody";
	uniformClass = "BWI_Uniform_PIR_PS1";
	hiddenSelections[] = {
		"Camo"
	};
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_1_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_AT: BWI_Soldier_PIR_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS2";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_2_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_AR: BWI_Soldier_PIR_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk74";
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS3";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_3_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_DMR: BWI_Soldier_PIR_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1", 
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS4";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_4_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_TL: BWI_Soldier_PIR_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	identityTypes[] = {
		"LanguageGRE_F",
		"Head_African",
		"G_Aviator"
	};
	weapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm_gp25",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_VOG25",
		"rhs_VOG25",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS5";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_5_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_MAT: BWI_Soldier_PIR_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_akm",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_30Rnd_762x39mm_tracer",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	uniformClass = "BWI_Uniform_PIR_PS6";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_6_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_MMG: BWI_Soldier_PIR_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS7";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_1_co.paa"
	};
};


class BWI_Soldier_PIR_GEN_R2: BWI_Soldier_PIR_GEN_R {
	linkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS8";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_4_co.paa"
	};
};
class BWI_Soldier_PIR_GEN_AT2: BWI_Soldier_PIR_GEN_AT {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS9";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_3_co.paa"
	};
};
class BWI_Soldier_PIR_GEN_AR2: BWI_Soldier_PIR_GEN_AR {
	linkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_rgr",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS10";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_4_co.paa"
	};
};
class BWI_Soldier_PIR_GEN_DMR2: BWI_Soldier_PIR_GEN_DMR {
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS11";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_3_co.paa"
	};
};
class BWI_Soldier_PIR_GEN_TL2: BWI_Soldier_PIR_GEN_TL {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS12";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_6_co.paa"
	};
};
class BWI_Soldier_PIR_GEN_MAT2: BWI_Soldier_PIR_GEN_MAT {
	linkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS13";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_2_co.paa"
	};
};
class BWI_Soldier_PIR_GEN_MMG2: BWI_Soldier_PIR_GEN_MMG {
	linkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"V_BandollierB_khk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "BWI_Uniform_PIR_PS14";
	hiddenSelectionsTextures[] = {
		"A3\Characters_F\Civil\Data\c_poloshirt_3_co.paa"
	};
};


class BWI_Vehicle_PIR_UAZ: rhs_uaz_open_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	crew = "BWI_Soldier_PIR_GEN_R2";
};

class BWI_Vehicle_PIR_UAZ_DSHKM: rhs_uaz_dshkm_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	crew = "BWI_Soldier_PIR_GEN_R";
};

class BWI_Vehicle_PIR_UAZ_SPG9: rhs_uaz_spg9_chdkz {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	crew = "BWI_Soldier_PIR_GEN_R2";
};

class BWI_Vehicle_PIR_Tech_M2: I_G_Offroad_01_armed_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_PIR_GEN_R";
};

class BWI_Vehicle_PIR_Tech: I_G_Offroad_01_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	displayName = "Technical";
	crew = "BWI_Soldier_PIR_GEN_R2";
};

class BWI_Vehicle_PIR_Boat: B_Boat_Transport_01_F {
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_PIR";
	crew = "BWI_Soldier_PIR_GEN_R";
};