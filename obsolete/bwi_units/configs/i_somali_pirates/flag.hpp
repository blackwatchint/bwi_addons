class BWI_Flag_PIR {
	scope = 1;
	name = "Pirates";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_pir.paa";
	texture = "\bwi_units\data\markers\mkr_pir.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};