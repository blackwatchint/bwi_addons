class U_C_Poloshirt_stripped;
class BWI_Uniform_PIR_PS1: U_C_Poloshirt_stripped {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "\A3\Characters_F\Civil\c_poloshirt";
		uniformClass = "BWI_Soldier_PIR_GEN_R";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_C_Poloshirt_tricolour;
class BWI_Uniform_PIR_PS2: U_C_Poloshirt_tricolour {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_AT";
		containerClass = "Supply40";
		mass = 40;
	};
};

class U_C_Poloshirt_salmon;
class BWI_Uniform_PIR_PS3: U_C_Poloshirt_salmon {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_AR";
		containerClass = "Supply40";
		mass = 40;
	};
};
class U_C_Poloshirt_redwhite;
class BWI_Uniform_PIR_PS4: U_C_Poloshirt_redwhite {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_DMR";
		containerClass = "Supply40";
		mass = 40;
	};
};
class U_C_Poloshirt_burgundy;
class BWI_Uniform_PIR_PS5: U_C_Poloshirt_burgundy {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_TL";
		containerClass = "Supply40";
		mass = 40;
	};
};
class U_C_Poloshirt_blue;
class BWI_Uniform_PIR_PS6: U_C_Poloshirt_blue {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_MAT";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS7: U_C_Poloshirt_stripped {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_MMG";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS8: U_C_Poloshirt_redwhite {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_R2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS9: U_C_Poloshirt_salmon {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_AT2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS10: U_C_Poloshirt_redwhite {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_AR2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS11: U_C_Poloshirt_salmon {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_DMR2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS12: U_C_Poloshirt_blue {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_TL2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS13: U_C_Poloshirt_tricolour {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_MAT2";
		containerClass = "Supply40";
		mass = 40;
	};
};

class BWI_Uniform_PIR_PS14: U_C_Poloshirt_salmon {
	scope = 1;
	modelSides[] = { 3, 2, 1, 0 };

	class ItemInfo: UniformItem {
		uniformModel = "-";
		uniformClass = "BWI_Soldier_PIR_GEN_MMG2";
		containerClass = "Supply40";
		mass = 40;
	};
};
