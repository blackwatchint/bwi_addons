class arifle_TRG21_F;
class arifle_TRG21_F_w_Aco: arifle_TRG21_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Aco";
		};
	};		
};

class arifle_TRG21_F_w_MOS: arifle_TRG21_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_SOS";
		};
	};		
};

class arifle_TRG21_GL_F;
class arifle_TRG21_GL_F_w_MRCO: arifle_TRG21_GL_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_MRCO";
		};
	};		
};

class rhs_weap_m249_pip_L;
class rhs_weap_m249_pip_L_w_ACOG3: rhs_weap_m249_pip_L {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG3_USMC";
		};
	};		
};

class rhs_weap_m240B;
class rhs_weap_m240B_w_ACOG3: rhs_weap_m240B {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG3_USMC";
		};
	};		
};

class arifle_Katiba_F;
class arifle_Katiba_F_w_Aco: arifle_Katiba_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Aco";
		};
	};		
};

class arifle_Katiba_GL_F;
class arifle_Katiba_GL_F_w_MRCO: arifle_Katiba_GL_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_MRCO";
		};
	};		
};

class rhs_weap_svdp_wd_npz;
class rhs_weap_svdp_wd_npz_w_LEUPOLDMK4: rhs_weap_svdp_wd_npz {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_LEUPOLDMK4";
		};
	};		
};

class rhs_weap_m4a1_carryhandle;
class rhs_weap_m4a1_carryhandle_w_CompM4: rhs_weap_m4a1_carryhandle  {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_compm4";
		};
	};
};

class rhs_weap_m4a1_carryhandle_m203;
class rhs_weap_m4a1_carryhandle_m203_w_ACOG: rhs_weap_m4a1_carryhandle_m203  {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG_USMC";
		};
	};
};

class rhs_weap_hk416d10;
class rhs_weap_hk416d10_w_Holo: rhs_weap_hk416d10  {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Holosight_blk_F";
		};
	};
};

class rhs_weap_hk416d10_m320;
class rhs_weap_hk416d10_m320_w_ACOG: rhs_weap_hk416d10_m320  {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG";
		};
	};
};

class rhs_weap_ak103_gp25_npz;
class rhs_weap_ak103_gp25_npz_w_ACOG2: rhs_weap_ak103_gp25_npz  {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG2_USMC";
		};
	};
};

class rhs_weap_ak103_1_npz;
class rhs_weap_ak103_1_npz_w_ACOG2: rhs_weap_ak103_1_npz  {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Holosight_blk_F";
		};
	};
};

class rhs_weap_ak74m_plummag_npz;
class rhs_weap_ak74m_plummag_npz_w_ARCO: rhs_weap_ak74m_plummag_npz {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Arco_blk_F";
		};
	};
};

class hlc_rifle_M21_Rail;
class hlc_rifle_M21_Rail_w_optic_KHS_old: hlc_rifle_M21_Rail {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_KHS_old";
		};
	};
};

class hlc_lmg_M60E4;
class hlc_lmg_M60E4_w_ACOG2: hlc_lmg_M60E4 {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG2_USMC";
		};
	};
};

class hlc_lmg_MG3KWS_b;
class hlc_lmg_MG3KWS_b_w_ELCAN: hlc_lmg_MG3KWS_b {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ELCAN";
		};
	};
};

class hlc_rifle_falosw;
class hlc_rifle_falosw_w_Eotech: hlc_rifle_falosw {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_eotech_552";
		};
	};
};

class hlc_rifle_osw_GL;
class hlc_rifle_osw_GL_w_ACOG3: hlc_rifle_osw_GL {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG3";
		};
	};
};

class hlc_rifle_M14;
class hlc_rifle_M14_w_artel: hlc_rifle_M14 {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "hlc_optic_artel_m14";
		};
	};
};

class rhs_weap_m24sws_blk;
class rhs_weap_m24sws_blk_w_LEUPOLDMK4: rhs_weap_m24sws_blk {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_LEUPOLDMK4";
		};
	};
};

class HLC_Rifle_g3ka4_GL;
class hlc_rifle_g3ka4_GL_w_ACOG: HLC_Rifle_g3ka4_GL {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG";
		};
	};
};

class hlc_rifle_PSG1A1_RIS;
class hlc_rifle_PSG1A1_RIS_w_LRPS: hlc_rifle_PSG1A1_RIS {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "ACE_optic_LRPS_2D";
		};
	};
};

class hlc_rifle_ACR_full_black;
class hlc_rifle_ACR_full_black_w_ACOG: hlc_rifle_ACR_full_black {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "rhsusf_acc_ACOG_RMR";
		};
	};
};

class arifle_AK12_F;
class arifle_AK12_F_GRU: arifle_AK12_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Arco_blk_F";
		};

		class LinkedItemsMuzzle {
			slot = "MuzzleSlot";
			item = "muzzle_snds_B";
		};

		class LinkedItemsAcc {
			slot = "PointerSlot";
			item = "acc_pointer_IR";
		};
	};
};

class arifle_AK12_GL_F;
class arifle_AK12_GL_F_GRU: arifle_AK12_GL_F {
	class LinkedItems {
		class LinkedItemsOptic {
			slot = "CowsSlot";
			item = "optic_Arco_blk_F";
		};

		class LinkedItemsMuzzle {
			slot = "MuzzleSlot";
			item = "muzzle_snds_B";
		};

		class LinkedItemsAcc {
			slot = "PointerSlot";
			item = "acc_pointer_IR";
		};
	};
};