class BWI_Flag_ASNJIH {
	scope = 1;
	name = "Jemaah Islamiyah";
	markerClass = "Flags";
	icon = "\bwi_units\data\markers\mkr_ji.paa";
	texture = "\bwi_units\data\markers\mkr_ji.paa";
	color[] = {1, 1, 1, 1};
	shadow = 0;
	size = 32;
};