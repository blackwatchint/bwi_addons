class BWI_Soldier_ASNJIH_GEN_R : B_Soldier_base_F {
	_generalMacro = "BWI_Soldier_ASNJIH_GEN_R";
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ASNJIH";
	vehicleClass = "rhs_vehclass_infantry";
	displayName = "Rifleman";
	icon = "iconMan";
	identityTypes[] = {
		"LanguageCHI_F",
		"Head_Asian",
		"NoGlasses"
	};
	weapons[] = {
		"rhs_weap_m16a4",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m16a4",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	Items[] = {
		"FirstAidKit"
	};
	RespawnItems[] = {
		"FirstAidKit"
	};
	linkedItems[] = {
		"H_Bandanna_gry",
		"G_Bandanna_blk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_gry",
		"G_Bandanna_blk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_5";
};


class BWI_Soldier_ASNJIH_GEN_AT: BWI_Soldier_ASNJIH_GEN_R {
	displayName = "Rifleman (AT)";
	icon = "iconManAT";
	weapons[] = {
		"arifle_AKM_F",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_AKM_F",
		"rhs_weap_rpg26",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	linkedItems[] = {
		"H_Bandanna_khk",
		"G_Bandanna_oli",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_khk",
		"G_Bandanna_oli",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhsgref_uniform_tigerstripe";
};


class BWI_Soldier_ASNJIH_GEN_AR: BWI_Soldier_ASNJIH_GEN_R {
	displayName = "Automatic Rifleman";
	icon = "iconManMG";
	weapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"hlc_rifle_rpk74n",
		"Throw",
		"Put"
	};
	magazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"hlc_45Rnd_545x39_t_rpk",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_rpk74";
	linkedItems[] = {
		"H_Booniehat_oli",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_oli",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "U_I_C_Soldier_Para_4_F";
	
};


class BWI_Soldier_ASNJIH_GEN_DMR: BWI_Soldier_ASNJIH_GEN_R {
	displayName = "Marksman";
	icon = "iconManRecon";
	weapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_svdp_pso1",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_10Rnd_762x54mmR_7N1",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	linkedItems[] = {
		"H_Cap_oli",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Cap_oli",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_4";
};


class BWI_Soldier_ASNJIH_GEN_TL: BWI_Soldier_ASNJIH_GEN_R {
	displayName = "Team Leader";
	icon = "iconManLeader";
	weapons[] = {
		"rhs_weap_m16a4_carryhandle_M203",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_m16a4_carryhandle_M203",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"30Rnd_556x45_Stanag_Tracer_Red",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"1Rnd_HE_Grenade_shell",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	linkedItems[] = {
		"H_Bandanna_blu",
		"G_Bandanna_blk",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Bandanna_blu",
		"G_Bandanna_blk",
		"V_BandollierB_blk",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_3";
};


class BWI_Soldier_ASNJIH_GEN_MAT: BWI_Soldier_ASNJIH_GEN_R {
	displayName = "Anti-Tank";
	icon = "iconManAT";
	weapons[] = {
		"arifle_AKM_F",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"arifle_AKM_F",
		"rhs_weap_rpg7",
		"Throw",
		"Put"
	};
	magazines[] = {
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	respawnMagazines[] = {
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"30Rnd_762x39_Mag_Tracer_F",
		"HandGrenade",
		"HandGrenade",
		"SmokeShell",
		"SmokeShell"
	};
	backpack = "B_Carryall_khk_f_rpg7";
	linkedItems[] = {
		"H_Cap_oli",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Cap_oli",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_4";
};


class BWI_Soldier_ASNJIH_GEN_MMG: BWI_Soldier_ASNJIH_GEN_R {
	displayName = "Machine Gunner";
	icon = "iconManMG";
	weapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	respawnWeapons[] = {
		"rhs_weap_pkm",
		"Throw",
		"Put"
	};
	magazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	respawnMagazines[] = {
		"rhs_100Rnd_762x54mmR",
		"rhs_mag_rgd5",
		"rhs_mag_rgd5",
		"rhs_mag_rdg2_white",
		"rhs_mag_rdg2_white"
	};
	backpack = "B_Carryall_khk_f_pkm";
	linkedItems[] = {
		"H_Booniehat_khk",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	respawnLinkedItems[] = {
		"H_Booniehat_khk",
		"G_Bandanna_khk",
		"V_BandollierB_oli",
		"ItemMap",
		"ItemCompass",
		"ItemWatch",
		"ItemRadio"
	};
	uniformClass = "rhs_chdkz_uniform_3";
};


class BWI_Soldier_ASNJIH_GEN_R2: BWI_Soldier_ASNJIH_GEN_R {
	uniformClass = "rhsgref_uniform_ttsko_forest";
};
class BWI_Soldier_ASNJIH_GEN_AT2: BWI_Soldier_ASNJIH_GEN_AT {
	uniformClass = "rhs_chdkz_uniform_4";
};
class BWI_Soldier_ASNJIH_GEN_AR2: BWI_Soldier_ASNJIH_GEN_AR {
	uniformClass = "rhs_chdkz_uniform_5";
};
class BWI_Soldier_ASNJIH_GEN_DMR2: BWI_Soldier_ASNJIH_GEN_DMR {
	uniformClass = "rhsgref_uniform_ttsko_forest";
};
class BWI_Soldier_ASNJIH_GEN_TL2: BWI_Soldier_ASNJIH_GEN_TL {
	uniformClass = "rhsgref_uniform_tigerstripe";
};
class BWI_Soldier_ASNJIH_GEN_MAT2: BWI_Soldier_ASNJIH_GEN_MAT {
	uniformClass = "rhs_chdkz_uniform_3";
};
class BWI_Soldier_ASNJIH_GEN_MMG2: BWI_Soldier_ASNJIH_GEN_MMG {
	uniformClass = "U_I_C_Soldier_Para_4_F";
};


class BWI_Vehicle_ASNJIH_Tech_M2: B_G_Offroad_01_armed_F
{
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ASNJIH";
	displayName = "Technical (M2)";
	crew = "BWI_Soldier_ASNJIH_GEN_R";
};


class BWI_Vehicle_ASNJIH_Tech: B_G_Offroad_01_F
{
	scope = 2;
	side = RESISTANCE;
	faction = "BWI_FACTION_ASNJIH";
	displayName = "Technical";
	crew = "BWI_Soldier_ASNJIH_GEN_R";
};