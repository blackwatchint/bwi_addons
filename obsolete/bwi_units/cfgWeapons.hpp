// Shared base classes.
class UniformItem;
class Uniform_Base;
class U_B_CombatUniform_mcam: Uniform_Base {
	class ItemInfo;
};

class V_PlateCarrier1_rgr;
class V_PlateCarrier2_rgr: V_PlateCarrier1_rgr {
	class ItemInfo;
};

class H_HelmetB;
class H_HelmetIA: H_HelmetB {
	class ItemInfo;
};


// Add scopes to weapons.
#include "configs/weapon_scopes.hpp"

// Include faction configs.
#include "configs/i_somali_pirates/gear.hpp"
#include "configs/b_israel_defence_forces/gear.hpp"
#include "configs/b_us_army_desert_storm/gear.hpp"
#include "configs/b_us_army_just_cause/gear.hpp"
#include "configs/b_us_army_battle_mog/gear.hpp"
#include "configs/b_us_army_cold_war/gear.hpp"
#include "configs/b_rok_army/gear.hpp"
#include "configs/o_dprk_army/gear.hpp"