class Indep
{
	name="$STR_A3_CfgGroups_Indep0";
	#include "configs/i_jemaah_islamiyah/groups.hpp"
	#include "configs/i_boko_haram/groups.hpp"
	#include "configs/i_somali_pirates/groups.hpp"
	#include "configs/i_somali_usc/groups.hpp"
	#include "configs/i_farc/groups.hpp"
	#include "configs/i_sierra_leone_ruf/groups.hpp"
	#include "configs/i_sierra_leone_army/groups.hpp"	
	#include "configs/i_liberia_lurd/groups.hpp"	
	#include "configs/i_liberia_army/groups.hpp"
	#include "configs/i_pmc_exec_decisions/groups.hpp"
	#include "configs/i_pmc_pp_solutions/groups.hpp"
};

class East
{
	name="$STR_A3_CfgGroups_East0";
	#include "configs/o_ussr_msv_cold_war/groups.hpp"
	#include "configs/o_russia_gru/groups.hpp"
	#include "configs/o_jemaah_islamiyah/groups.hpp"
	#include "configs/o_boko_haram/groups.hpp"
	#include "configs/o_somali_pirates/groups.hpp"
	#include "configs/o_somali_usc/groups.hpp"
	#include "configs/o_farc/groups.hpp"
	#include "configs/o_sierra_leone_ruf/groups.hpp"	
	#include "configs/o_liberia_army/groups.hpp"
	#include "configs/o_pmc_exec_decisions/groups.hpp"
	#include "configs/o_pmc_pp_solutions/groups.hpp"
	#include "configs/o_dprk_army/groups.hpp"
};

class West
{
	name="$STR_A3_CfgGroups_West0";
	#include "configs/b_israel_defence_forces/groups.hpp"
	#include "configs/b_us_army_desert_storm/groups.hpp"
	#include "configs/b_us_army_just_cause/groups.hpp"		
	#include "configs/b_us_army_battle_mog/groups.hpp"
	#include "configs/b_us_army_cold_war/groups.hpp"	
	#include "configs/b_us_army_vietnam/groups.hpp"
	#include "configs/b_sierra_leone_army/groups.hpp"
	#include "configs/b_liberia_lurd/groups.hpp"
	#include "configs/b_rok_army/groups.hpp"
};